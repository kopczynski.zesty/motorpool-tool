// General config ripper
_configs = "getNumber (_x >> 'scope') >= 1 &&   
(configName _x isKindOf 'Car'  
|| configName _x isKindOf 'Air'
|| configName _x isKindOf 'Tank'  
|| configName _x isKindOf 'Ship') " configClasses (configFile >> "CfgVehicles") apply  
{  
 configName(_x) + ":"  
 + getText (_x >> "displayName") + ":"  
 + str(getNumber (_x >> "ace_cargo_space")) + ":" 
 + getText (_x >> "textSingular");  
};  
copyToClipBoard str _configs;

// Get configName, displayName, ace_cargo_space, textSingular, weapons, and magazines.
_newLine = toString [0x0D, 0x0A]; 
_configs = "getNumber (_x >> 'scope') >= 1 &&    
(configName _x isKindOf 'Car'   
|| configName _x isKindOf 'Air' 
|| configName _x isKindOf 'Tank'   
|| configName _x isKindOf 'Ship') " configClasses (configFile >> "CfgVehicles") apply   
{ 
 _crewCount = [configName (_x),false] call BIS_fnc_crewCount; 
 _passengerCount = [configName (_x),true] call BIS_fnc_crewCount; 
 _passengerCount = _passengerCount - _crewCount; 
 _turrets = []; 
 _turretConfigs = configProperties[_x >> "Turrets"]; 
 { 
  if (getText (_x >> "gun") != "") then 
  { 
  _turrets pushBack configName(_x);
  _turrets pushBack getNumber(_x >> "primaryGunner"); 
  _turrets pushBack ((getArray (_x >> "weapons")) + (getArray (_x >> "magazines"))); 
  }; 
 } forEach _turretConfigs; 
 
 _newLine 
 + configName(_x) + ";" 
 + getText (_x >> "displayName") + ";" 
 + str(getNumber (_x >> "ace_cargo_space")) + ";" 
 + getText (_x >> "textSingular") + ";" 
 + str _crewCount + ";" 
 + str _passengerCount + ";" 
 + str(getNumber (_x >> "maximumLoad")) + ";" 
 + str _turrets + ";" 
 + str(getArray (_x >> "weapons") + getArray (_x >> "magazines")) + ";";   
};   
copyToClipBoard str _configs;

// CfgWeapons
// Dev Note: You can't use colons as seperators because some weapon descriptions use colons.
// configName : ARMA 3 clasName
// displayName : Human readble name
// magazines : array of compatible magazines from CfgMagazines
// magazineWells : array of compatible magazines from CfgMagazineWells
// magazineReloadtime : delay between reloading a fresh magazineCargo
// reloadTime : delay between each fired shot
// canLock : 0 never locks. 1 use to lock on lower difficulties (removed in ARMA 3 1.58). 2 can lock on
// weaponLockSystem : do later. has to do with vehicle lock warnings.
_newLine = toString [0x0D, 0x0A];
_configs = "getNumber (_x >> 'scope') >= 1 &&
(getNumber (_x >> 'type') == 1
|| getNumber (_x >> 'type') == 2
|| getNumber (_x >> 'type') == 4
|| getNumber (_x >> 'type') == 256
|| getNumber (_x >> 'type') == 4096
|| getNumber (_x >> 'type') == 65536)" configClasses (configFile >> "CfgWeapons") apply  
{
	_newLine
	+ configName(_x) + ";"
	+ getText (_x >> "displayName") + ";"
	+ getText (_x >> "descriptionShort") + ";"
	+ str(getArray (_x >> "magazines")) + ";"
	+ str(getArray (_x >> "magazineWell")) + ";"
	+ str(getNumber (_x >> "magazineReloadTime")) + ";"
	+ str(getNumber (_x >> "reloadTime")) + ";"
	+ str(getNumber (_x >> "canLock")) + ";"
	+ str(getNumber (_x >> "weaponLockSystem"));  
};  
copyToClipBoard str _configs;

// CfgMagazines
_newLine = toString [0x0D, 0x0A];
_configs = "getNumber (_x >> 'scope') >= 1" configClasses (configFile >> "CfgMagazines") apply  
{
	_newLine
	+ configName(_x) + ";"
	+ getText (_x >> "displayName") + ";"
	+ getText (_x >> "displayNameShort") + ";"
	+ getText (_x >> "descriptionShort") + ";"
	+ getText (_x >> "pylonWeapon") + ";"
	+ getText (_x >> "ammo") + ";"
	+ str(getNumber (_x >> "count"));
};  
copyToClipBoard str _configs;



// Thumbnail generator.
_className = copyFromClipboard;
_camera = "camera" camCreate [30,0,2];
_camera camSetFov 0.2;
showCinemaBorder false;
_camera camSetTarget (invisibleTarget); 
_camera cameraEffect ["internal", "back"]; 
_camera camCommit 0;

_target = nearestObjects [player, ["Car", "Truck", "Helicopter", "Tank", "Plane"], 100];
deleteVehicle (_target select 0);
_target = copyFromClipboard createVehicle [0,0,0];