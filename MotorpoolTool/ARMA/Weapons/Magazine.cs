﻿using MotorpoolTool.GUI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;


namespace MotorpoolTool.ARMA.Weapons
{
    public class Magazine : ARMAEntity, ICloneable
    {
        public static Dictionary<string, Magazine> MagazineDictionary = new Dictionary<string, Magazine>();

        public static Magazine Lookup(string className)
        {
            return MagazineDictionary[className.ToLowerInvariant()];
        }

        public Turret Parent { get; set; }
        public string DisplayNameShort { get; set; }
        public string Description { get; set; }
        public string PylonWeapon { get; set; }
        public string CfgAmmoReference { get; set; }
        public int Ammo { get; set; }
        public string AmmoString
        {
            get
            {
                return $"{this} ({this.Ammo} round{(this.Ammo == 1 ? string.Empty : "s")})";
            }
        }
        public int Control { get; set; }
        public bool Suffix { get; set; }
        public bool Filter { get; set; }

        // Prefix filters cases like "SomePylon x1" and Suffix filters cases like "SomePylon 1x"
        private static readonly Regex pylonAmmoCountFilterPrefix = new Regex(@"(x+\s*\d+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex pylonAmmoCountFilterSuffix = new Regex(@"\s(\d+\s*x)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public Magazine(Magazine magazine)
        {
            this.Parent = magazine.Parent; // ok to use reference here for clone.
            this.ClassName = magazine.ClassName;
            this.DisplayName = magazine.DisplayName;
            this.DisplayNameShort = magazine.DisplayNameShort;
            this.Description = magazine.Description;
            this.PylonWeapon = magazine.PylonWeapon;
            this.CfgAmmoReference = magazine.CfgAmmoReference;
            this.Ammo = magazine.Ammo;
            this.Indent = magazine.Indent;
            this.Control = magazine.Control;
            this.Mass = magazine.Mass;
        }

        public Magazine(
            string className = null,
            string[] inheritance = null,
            string displayName = null,
            string displayNameShort = null,
            string description = null,
            string pylonWeapon = null,
            string cfgAmmoReference = null,
            string ammoCount = null,
            string mass = null
            ) : base(className, inheritance, displayName)
        {
            this.DisplayNameShort = displayNameShort;
            this.Description = !string.IsNullOrEmpty(description) ? description : "None provided.";
            this.PylonWeapon = pylonWeapon;
            this.CfgAmmoReference = cfgAmmoReference;
            this.Control = Constants.PILOT; // assume all weapons are pilot controlled unless assigned

            if (int.TryParse(ammoCount, out int tempAmmoCount))
            {
                this.Ammo = tempAmmoCount;
            }
            else
            {
                this.Ammo = -1;
            }

            this.Mass = float.Parse(mass, NumberStyles.Float, Program.MPTCulture);
        }


        /// <summary>
        /// Converts a string array to a magazine list using ARMAMagazineLookup
        /// </summary>
        /// <param name="magazines"></param>
        /// <returns></returns>
        public static List<Magazine> StringToMagazine(string[] magazines)
        {
            List<Magazine> magazineList = new List<Magazine>();
            foreach (string entry in magazines)
            {
                if (MagazineDictionary.TryGetValue(entry.ToLowerInvariant(), out Magazine magazine))
                {
                    magazineList.Add(magazine);
                }
            }
            return magazineList;
        }

        public new object Clone()
        {
            return new Magazine(this);
        }

        public override string ToString()
        {
            if (!this.Filter)
            {
                return base.ToString();
            }

            string filteredName = this.Name;

            MatchCollection matchesPrefix = pylonAmmoCountFilterPrefix.Matches(this.DisplayName);
            MatchCollection matchesSuffix = pylonAmmoCountFilterSuffix.Matches(this.DisplayName);

            if (matchesPrefix.Count > 0)
            {
                filteredName = filteredName.Replace(matchesPrefix[0].Groups[1].Value.ToString(), string.Empty);
            }
            if (matchesSuffix.Count > 0)
            {
                filteredName = filteredName.Replace(matchesSuffix[0].Groups[1].Value.ToString(), string.Empty);
            }

            if (this.Suffix)
            {
                filteredName = filteredName.Trim() + string.Format(" ( x{0})", this.Ammo);
            }
            else
            {
                filteredName = filteredName.Insert(0, string.Format("( x{0} ) ", this.Ammo));
            }

            return filteredName;
        }
    }
}
