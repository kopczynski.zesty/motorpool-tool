﻿using ExtensionMethods.ARMA;
using System;
using System.Collections.Generic;
using MotorpoolTool.ARMA.Weapons;

namespace MotorpoolTool.ARMA.Weapons
{
    public class Weapon : ARMAEntity, ICloneable
    {
        public static Dictionary<string, Weapon> WeaponDictionary = new Dictionary<string, Weapon>();

        public static Weapon Lookup(string className)
        {
            return WeaponDictionary[className.ToLowerInvariant()];
        }

        public Turret Parent { get; set; }
        public string[] ItemType { get; set; }
        public string Description { get; set; }
        public List<string> CompatibleMagazines { get; set; }
        public List<string> CompatibleMagazineWells { get; set; }

        // Delay for loading new magazine
        public float MagazineReloadTime { get; set; }

        // Delay between shots
        public float ReloadTime { get; set; }

        public bool CanLock { get; set; }

        public string ContainerClass { get; private set; }

        // for ARMA reasons, a CfgWeapons maxmiumLoad is looked up from CfgVehicles.
        public int GetMaxmimumLoad()
        {
            return -1;
        }

        // TODO weaponLockSystem bitmask or something

        public Weapon(Weapon weapon)
        {
            this.Parent = weapon.Parent; // ok to use reference here for clone
            this.ClassName = weapon.ClassName;
            this.ItemType = weapon.ItemType;
            this.DisplayName = weapon.DisplayName;
            this.Description = weapon.Description;
            this.CompatibleMagazines = weapon.CompatibleMagazines.Clone();
            this.CompatibleMagazineWells = weapon.CompatibleMagazineWells.Clone();
            this.MagazineReloadTime = weapon.MagazineReloadTime;
            this.ReloadTime = weapon.ReloadTime;
            this.CanLock = weapon.CanLock;
            this.Mass = weapon.Mass;
            this.ContainerClass = weapon.ContainerClass;
            this.Indent = weapon.Indent;
        }

        public Weapon(string className,
            string[] inheritance,
            string[] itemType = null,
            string displayName = null,
            string description = null,
            string[] magazines = null,
            string[] magazineWell = null,
            float magazineReloadTime = -1,
            float reloadTime = -1,
            bool canLock = false,
            string weaponLockSystem = null,
            float mass = -1,
            string containerClass = null
            ) : base(className, inheritance, displayName)
        {
            this.ItemType = itemType;

            // Patches
            if (this.ClassName.ToLowerInvariant().Contains("rhs_weap_fcs"))
            {
                //Console.WriteLine($"Patched {this.ClassName} '{this.DisplayName}' to '{Patches.MG251_FCS_DISPLAY_NAME}'");
                this.DisplayName = Patches.MG251_FCS_DISPLAY_NAME;
            }

            this.Description = !string.IsNullOrEmpty(description) ? description : "None provided.";

            this.CompatibleMagazines = new List<string>();
            if (magazines != null)
            {
                this.CompatibleMagazines.AddRange(magazines);
            }

            // Add compatible magazineWells to compatibleMagazines for simplicity.
            this.CompatibleMagazineWells = new List<string>();
            if (magazineWell != null)
            {
                this.CompatibleMagazineWells.AddRange(magazineWell);

                if (this.CompatibleMagazines == null) { this.CompatibleMagazines = new List<string>(); }
                var lookup = MagazineWell.StringToMagazine(magazineWell);
                foreach (Magazine mag in lookup)
                {
                    this.CompatibleMagazines.Add(mag.ClassName);
                }
            }

            this.MagazineReloadTime = magazineReloadTime;
            this.ReloadTime = reloadTime;
            this.CanLock = canLock;

            // TODO handle weapon lock

            this.Mass = mass;
            this.ContainerClass = containerClass;
        }

        /// <summary>
        /// Convert a string array to a weapons list using ARMAWeaponLookup
        /// </summary>
        /// <param name="weapons">String of potential weapons</param>
        /// <returns>List of weapons</returns>
        public static List<Weapon> StringToWeapon(string[] weapons)
        {
            List<Weapon> weaponList = new List<Weapon>();
            foreach (string entry in weapons)
            {
                if (Weapon.WeaponDictionary.TryGetValue(entry.ToLowerInvariant(), out Weapon weapon))
                {
                    weaponList.Add(weapon);
                }
            }
            return weaponList;
        }

        public new object Clone()
        {
            return new Weapon(this);
        }
    }
}
