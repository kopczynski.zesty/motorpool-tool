﻿using ExtensionMethods.ARMA;
using MotorpoolTool.Data;
using System;
using System.Collections.Generic;

namespace MotorpoolTool.ARMA.Weapons
{
    public class MagazineWell : ARMAEntity, ICloneable
    {
        public Turret Parent { get; set; }
        public List<Magazine> Magazines { get; set; }

        public MagazineWell(MagazineWell magazineWell)
        {
            this.Parent = magazineWell.Parent;
            this.ClassName = magazineWell.ClassName;
            this.Magazines = magazineWell.Magazines.Clone();
        }

        public MagazineWell(
            string className, 
            string[] inheritance,
            string[] magazines
            ) : base(className, inheritance, string.Empty)
        {
            if (magazines == null)
            {
                return;
            }

            this.Magazines = Magazine.StringToMagazine(magazines).Clone();
        }

        public static List<Magazine> StringToMagazine(string[] magazineWells)
        {
            List<Magazine> magazineList = new List<Magazine>();
            foreach (string entry in magazineWells)
            {
                if (MagazineWellFactory.MagazineWellsLookup.TryGetValue(entry.ToLowerInvariant(), out MagazineWell magazineWell))
                {
                    magazineList.AddRange(magazineWell.Magazines);
                }
            }
            return magazineList;
        }

        public new object Clone()
        {
            return new MagazineWell(this);
        }

        public override string ToString()
        {
            return this.ClassName;
        }
    }
}
