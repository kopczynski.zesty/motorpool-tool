﻿namespace MotorpoolTool.ARMA
{
    /// <summary>
    /// Sometimes life is difficult, and content creators use scripts to make data scraping harder for no reason. Patches solves that.
    /// </summary>
    public class Patches
    {
        /// <summary>
        /// The MG251 is actually a fire control system. This renames it so it's less confusing when it shows up.
        /// </summary>
        public static readonly string MG251_FCS_DISPLAY_NAME = "FCS";

        /// <summary>
        /// RHS uses scripts to assign BM-21 weapons for some dumb reason. This overrides the scraped turret data.
        /// </summary>
        public static readonly string BM21_TURRET_DATA = "[MainTurret,1,[rhs_weap_bm21,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1,rhs_mag_m21of_1]]";


        /// <summary>
        /// RHS uses scripts to assign HIMARS weapons for some dumb reason. This overrides the scraped turret data.
        /// </summary>
        public static readonly string HIMARS_TURRET_DATA = "[MainTurret,1,[rhs_weap_mlrs,rhs_mag_m26a1_6]]";

        /// <summary>
        /// Abramia reports its world size incorrectly.
        /// </summary>
        public const int ABRAMIA_WORLD_SIZE = 10240;
    }
}
