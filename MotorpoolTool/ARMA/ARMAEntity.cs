﻿using MotorpoolTool.ARMA.Weapons;
using System;
using System.Collections.Generic;

namespace MotorpoolTool.ARMA
{
    public class ARMAEntity : ICloneable, IEquatable<ARMAEntity>
    {
        public string Name
        {
            get
            {
                return !string.IsNullOrEmpty(this.DisplayName) ? this.DisplayName : this.ClassName;
            }
        }
        public string ClassName { get; protected set; }
        public string[] Inhertiance { get; protected set; }
        public string DisplayName { get; protected set; }
        public float Mass { get; protected set; }
        public bool Indent { get; set; }

        public Dictionary<ARMAEntity, int> Inventory;
        public ARMAEntity()
        {
            this.Inventory = new Dictionary<ARMAEntity, int>();
        }

        public ARMAEntity(string className, string[] inhertiance, string displayName, float mass = -1f)
        {
            this.ClassName = className;
            this.Inhertiance = inhertiance;
            this.DisplayName = displayName;
            this.Mass = mass;
            
            this.Inventory = new Dictionary<ARMAEntity, int>();
        }

        public ARMAEntity(ARMAEntity armaBase)
        {
            this.ClassName = armaBase.ClassName;
            this.Inhertiance = armaBase.Inhertiance;
            this.DisplayName = armaBase.DisplayName;
            this.Mass = armaBase.Mass;

            this.Inventory = (Dictionary<ARMAEntity, int>)armaBase.Clone();
        }

        public void AddToInventory(ARMAEntity item, int count = 1)
        {
            if (this.Inventory.ContainsKey(item))
            {
                this.Inventory[item] += count;
            }
            else
            {
                this.Inventory.Add(item, count);
            }
        }

        public void AddToInventory(string item, int count = 1)
        {
            if (string.IsNullOrEmpty(item)) { return; }

            Magazine.MagazineDictionary.TryGetValue(item.ToLowerInvariant(), out Magazine magazine);
            Weapon.WeaponDictionary.TryGetValue(item.ToLowerInvariant(), out Weapon weapon);

            if (magazine != null && weapon != null)
            {
                throw new InvalidOperationException();
            }

            if (magazine != null && this.Inventory.ContainsKey(magazine))
            {
                this.Inventory[magazine] += count;
                return;
            }
            else if (weapon != null && this.Inventory.ContainsKey(weapon))
            {
                this.Inventory[weapon] += count;
                return;
            }
            else if (magazine != null)
            {
                this.Inventory.Add(magazine, count);
            }
            else if (weapon != null)
            {
                this.Inventory.Add(weapon, count);
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public bool SearchInventory(params string[] classNames)
        {
            foreach (var item in this.Inventory)
            {
                foreach (string s in classNames)
                {
                    if (item.Key.ClassName.Equals(s))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public object Clone()
        {
            return new ARMAEntity(this);
        }

        public override string ToString()
        {
            return $"{(this.Indent ? Constants.TAB : string.Empty)}{this.Name}";
        }

        bool IEquatable<ARMAEntity>.Equals(ARMAEntity other)
            => this.ClassName.Equals(other.ClassName, StringComparison.OrdinalIgnoreCase);
    }
}
