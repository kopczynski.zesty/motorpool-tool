﻿using MotorpoolTool.GUI;
using System.Collections.Generic;
using System.Globalization;

namespace MotorpoolTool.ARMA
{
    public class Grid
    {
        private enum ZoomInfo
        {
            format,
            formatX,
            formatY,
            stepX,
            stepY,
            zoomMax
        }
        public Grid(string[][] zooms)
        {
            this.Zooms = new Dictionary<string, Zoom>();

            foreach (var array in zooms)
            {
                var split = array[1].Split(',');

                string className = array[0].Replace(",", string.Empty);
                string format = split[(int)ZoomInfo.format];
                string formatX = split[(int)ZoomInfo.formatX];
                string formatY = split[(int)ZoomInfo.formatY];
                float stepX = float.Parse(split[(int)ZoomInfo.stepX], NumberStyles.Float, Program.MPTCulture);
                float stepY = float.Parse(split[(int)ZoomInfo.stepY], NumberStyles.Float, Program.MPTCulture);
                float zoomMax = float.Parse(split[(int)ZoomInfo.zoomMax], NumberStyles.Float, Program.MPTCulture);

                this.Zooms.Add(className.ToLowerInvariant(), new Zoom(className, format, formatX, formatY, stepX, stepY, zoomMax));

            }
        }

        public Dictionary<string, Zoom> Zooms { get; private set; }
    }
}
