﻿using System;
using System.Collections.Generic;

namespace MotorpoolTool.ARMA
{
    public class World : ARMAEntity
    {
        public static Dictionary<string, World> WorldDictionary = new Dictionary<string, World>();
        public static World Lookup(string className)
        {
            return WorldDictionary[className.ToLowerInvariant()];
        }

        public World(
            string className,
            string[] inheritance,
            string description, 
            float mapSize, 
            float latitute, 
            float longitude, 
            string[][] grids
            ) : base(className, inheritance, description)
        {
            this.Size = mapSize;
            this.Latitude = latitute;
            this.Longitude = longitude;

            if (grids != null && grids[0].Length > 1)
            {
                this.Grids = new Grid(grids);

            }

            // Patches
            if (this.ClassName.Equals("abramia", StringComparison.InvariantCultureIgnoreCase))
            {
                this.Size = Patches.ABRAMIA_WORLD_SIZE;
            }
        }

        /// <summary>
        /// Size in meters.
        /// </summary>
        public float Size { get; set; }

        public float Latitude { get; private set; }
        public float Longitude { get; private set; }

        public Grid Grids { get; private set; }

        public override string ToString()
        {
            return !string.IsNullOrEmpty(this.DisplayName) ? this.DisplayName : this.ClassName;
        }
    }


}
