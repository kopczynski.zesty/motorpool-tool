﻿namespace MotorpoolTool.ARMA
{
    public class Zoom
    {
        public Zoom(string className, string format, string formatX, string formatY, float stepX, float stepY, float zoomMax)
        {
            this.ClassName = className;
            this.Format = format;
            this.FormatX = formatX;
            this.FormatY = formatY;
            this.StepX = stepX;
            this.StepY = stepY;
            this.ZoomMax = zoomMax;
        }

        public string ClassName { get; private set; }
        public string Format { get; private set; }
        public string FormatX { get; private set; }
        public string FormatY { get; private set; }
        public float StepX { get; private set; }
        public float StepY { get; private set; }
        public float ZoomMax { get; private set; }
    }
}
