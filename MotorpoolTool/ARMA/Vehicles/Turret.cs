﻿using ExtensionMethods.ARMA;
using MotorpoolTool.ARMA.Weapons;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MotorpoolTool.ARMA
{

    public class Turret
    {
        public string ClassName { get; set; }

        public bool IsPrimaryTurret { get; set; }

        /// <summary>
        /// Some 'turret' positions are just seats without any weapons.
        /// </summary>
        public bool HasWeapons
        {
            get { return this.Weapons != null && this.Weapons.Count > 0; }
        }
        public List<Weapon> Weapons { get; set; }
        public List<Magazine> Magazines { get; set; }

        public Turret(string className)
        {
            this.ClassName = className;
            this.Weapons = new List<Weapon>();
            this.Magazines = new List<Magazine>();
        }

        public Turret(string className, string primaryTurret, string[] weaponsAndMagazines)
        {
            this.ClassName = className;
            this.IsPrimaryTurret = int.Parse(primaryTurret) > 0 ? true : false;

            if (weaponsAndMagazines == null) { return; } // probably a commander turret
            this.Magazines = Magazine.StringToMagazine(weaponsAndMagazines).Clone();
            this.Weapons = Weapon.StringToWeapon(weaponsAndMagazines).Clone();

            foreach (Weapon weapon in this.Weapons)
            {
                if (weapon.Parent != null) { throw new DataException(string.Format("{0} tried to become child of {1}, but was already child of {2}", weapon.ToString(), this.ToString(), weapon.Parent.ToString())); };
                weapon.Parent = this;
            }
            foreach (Magazine mag in this.Magazines)
            {
                mag.Parent = this;
            }
        }

        public override string ToString()
        {
            return this.IsPrimaryTurret ? "Gunner Weapons" : this.ClassName;
        }
    }
}
