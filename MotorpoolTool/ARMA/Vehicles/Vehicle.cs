﻿using ExtensionMethods.ARMA;
using MotorpoolTool.ARMA.Weapons;
using MotorpoolTool.GUI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MotorpoolTool.ARMA
{
    /// <summary>
    /// Defines and contains multiple properties of an ARMA vehicle.
    /// <code>TODO</code>
    /// </summary>
    public class Vehicle : ARMAEntity, ICloneable
    {
        /// <summary>
        /// Dictionary of all CfgVehicles loaded.
        /// </summary>
        public static Dictionary<string, Vehicle> VehicleDictionary = new Dictionary<string, Vehicle>(StringComparer.OrdinalIgnoreCase);

        public static Vehicle Lookup(string className)
        {
            VehicleDictionary.TryGetValue(className, out Vehicle vehicle);
            return vehicle;
        }

        // TextSingular
        public ushort Category { get; private set; }

        /// <summary>
        /// ACE3 variable that enables cargo. Cargo space can be >0,
        /// but if this is false then you can't use the vehicle's cargo.
        /// </summary>
        public bool HasCargo { get; private set; }

        /// <summary>
        /// ACE3 cargo capacity.
        /// </summary>
        public float CargoSpace { get; private set; }

        /// <summary>
        /// ACE3 cargo size.
        /// </summary>
        public float CargoSize { get; private set; }

        /// <summary>
        /// ACE3 ammo supply.
        /// </summary>
        public int Reammo { get; private set; }

        /// <summary>
        /// ACE3 fuel supply.
        /// </summary>
        public int Refuel { get; private set; }

        /// <summary>
        /// ACE3 variable that marks this vehicle as a repair vehicle.
        /// </summary>
        public bool Repair { get; private set; }

        /// <summary>
        /// ARMA 3 vanilla inventory capacity.
        /// </summary>
        public float MaxmiumLoad { get; private set; }

        /// <summary>
        /// ARMA 3 vanilla area of effect reammo.
        /// </summary>
        public float TransportAmmo { get; private set; }

        /// <summary>
        /// ARMA 3 vanilla area of effect refuel.
        /// </summary>
        public float TransportFuel { get; private set; }

        /// <summary>
        /// Crew slots in a vehicle. Usually includes FFV seats.
        /// </summary>
        public int Crew { get; private set; }

        /// <summary>
        /// Passenger slots in a vehicle. Only includes 'cargo' seats.
        /// </summary>
        public int Passengers { get; set; }

        public List<Turret> Turrets { get; set; }
        public List<Weapon> Weapons { get; set; }
        public List<Magazine> Magazines { get; set; }
        public int Slingpoints { get; private set; }
        public bool CanFloat { get; private set; }
        public bool IsBackpack { get; private set; }

        // The only boat in the game that has canFloat = 0 is the RHS Mk. 5 SOC. Just list all boats as amphibious.
        public string Floats { get { return this.CanFloat || this.Category == Constants.SHIP ? "Yes" : "No"; } }

        public Vehicle(Vehicle vehicle)
        {
            this.ClassName = vehicle.ClassName;
            this.Inhertiance = vehicle.Inhertiance;
            this.DisplayName = vehicle.DisplayName;
            this.Category = vehicle.Category;
            this.HasCargo = vehicle.HasCargo;
            this.CargoSpace = vehicle.CargoSpace;
            this.CargoSize = vehicle.CargoSize;
            this.Reammo = vehicle.Reammo;
            this.Refuel = vehicle.Refuel;
            this.Repair = vehicle.Repair;
            this.Crew = vehicle.Crew;
            this.Passengers = vehicle.Passengers;
            this.MaxmiumLoad = vehicle.MaxmiumLoad;
            this.TransportAmmo = vehicle.TransportAmmo;
            this.TransportFuel = vehicle.TransportFuel;
            this.Turrets = vehicle.Turrets; // TODO needs clone?
            this.Magazines = vehicle.Magazines;
            this.Weapons = vehicle.Weapons;
            this.Slingpoints = vehicle.Slingpoints;
            this.CanFloat = vehicle.CanFloat;
            this.IsBackpack = vehicle.IsBackpack;
        }

        public Vehicle(string className) : this(Lookup(className))
        {

        }

        public Vehicle(
            string className,
            string[] inhertiance,
            string displayName,
            bool aceCargoHasCargo,
            float aceCargoSpace,
            float aceCargoSize,
            int aceRearmSupply,
            int aceRefuelCargo,
            bool aceCanRepair,
            ushort category,
            int crewCount,
            int passengerCount,
            float maxmiumLoad,
            float transportAmmo,
            float transportFuel,
            string turrets,
            string[] weaponsAndMagazines,
            string slingLoadCargoMemoryPoints,
            int canFloat,
            int isBackpack) : base(className, inhertiance, displayName)
        {
            this.HasCargo = aceCargoHasCargo;
            this.CargoSpace = aceCargoSpace;
            this.CargoSize = aceCargoSize;
            this.Reammo = aceRearmSupply;
            this.Refuel = aceRefuelCargo;
            this.Repair = aceCanRepair;
            this.Category = category;
            this.Crew = crewCount;
            this.Passengers = passengerCount;
            this.MaxmiumLoad = maxmiumLoad;
            this.TransportAmmo = transportAmmo;
            this.TransportFuel = transportFuel;
            this.Turrets = GetTurretsFromString(turrets);

            if (weaponsAndMagazines != null && weaponsAndMagazines.Length > 0)
            {
                this.Magazines = Magazine.StringToMagazine(weaponsAndMagazines).Clone();
                this.Weapons = Weapon.StringToWeapon(weaponsAndMagazines).Clone();
            }

            this.Slingpoints = (slingLoadCargoMemoryPoints != null && slingLoadCargoMemoryPoints.Length > 0) ? int.Parse(slingLoadCargoMemoryPoints) : -1;
            this.CanFloat = canFloat > 0;
            this.IsBackpack = isBackpack > 0;
        }

        private List<Turret> GetTurretsFromString(string turretData)
        {
            // Patch for BM-21 and M142 HIMARS due to RHS jank code that doesn't correctly assign weapons at config time.
            if (this.ClassName.ToLowerInvariant().Contains("bm21"))
            {
                turretData = Patches.BM21_TURRET_DATA;
            }
            else if (this.ClassName.ToLowerInvariant().Contains("m142"))
            {
                turretData = Patches.HIMARS_TURRET_DATA;
            }

            var turretArray = Cfg.StringToArray(turretData);
            if (turretArray == null) { return null; }

            List<Turret> createdTurrets = new List<Turret>();
            foreach (string[] turret in turretArray)
            {
                var turretClassName = turret[0].Split(',');
                createdTurrets.Add(new Turret(turretClassName[0], turretClassName[1], turret.Length > 1 ? turret[1].Split(',') : null));
            }

            return createdTurrets;
        }



        public string CategoryString
        {
            get { return Constants.BWIVehicleCategory[this.Category]; }
        }

        /// <summary>
        /// Returns true if this vehicle has fixed weapons (e.g. autocannons or weapons like 'car horns')
        /// </summary>
        public bool HasTurrets
        {
            get { return this.Turrets != null && this.Turrets.Count != 0; }
        }

        /// <summary>
        /// Returns true if this vehicle has ANY weapons.
        /// </summary>
        public bool HasWeapons
        {
            get { return this.Weapons != null && this.Weapons.Count != 0; }
        }

        public virtual string ToDebugString()
        {
            var debugString = string.Format(
                                 "ClassName:    {0}\n" +
                                 "DisplayName:  {1}\n" +
                                 "Cargo:        {2}\n" +
                                 "MaxmiumLoad:  {3}\n" +
                                 "Crew:         {4}\n" +
                                 "Passengers:   {5}\n" +
                                 "Slingpoints:  {6}\n" +
                                 "CanFloat:     {7}\n",
                                 this.ClassName,
                                 this.DisplayName,
                                 this.CargoSpace,
                                 this.MaxmiumLoad,
                                 this.Crew,
                                 this.Passengers,
                                 this.Slingpoints,
                                 this.Floats);

            if (this.Weapons != null)
            {
                foreach (Weapon weapon in this.Weapons)
                {
                    debugString += string.Format("FWeapon:      {0}\n", weapon.DisplayName);
                }
            }
            if (this.Magazines != null)
            {
                foreach (Magazine magazine in this.Magazines)
                {
                    debugString += string.Format("FMagazine:    {0} ({1} rnds)\n", magazine.ToString(), magazine.Ammo);
                }
            }

            if (this.Turrets != null)
            {
                foreach (Turret turret in this.Turrets)
                {
                    debugString += string.Format("Turret:       {0} {1}\n", turret.ToString(), turret.IsPrimaryTurret ? "(primary turret)" : string.Empty);

                    // Commander turrets sometimes don't have weapons.
                    if (turret.Weapons != null)
                    {
                        foreach (Weapon weapon in turret.Weapons)
                        {
                            debugString += string.Format("    Wep:       {0}\n", weapon.DisplayName);
                        }
                    }

                    if (turret.Magazines != null)
                    {
                        foreach (Magazine magazine in turret.Magazines)
                        {
                            debugString += string.Format("    Mag:       {0} ({1} rnds)\n", magazine.ToString(), magazine.Ammo);
                        }
                    }

                }
            }

            string indent = " ";
            foreach(var inherited in this.Inhertiance)
            {
                debugString += $"Inhertiance: {indent}{inherited}\n";
                indent += "  ";
            }
            return debugString;
        }

        public new object Clone()
        {
            return new Vehicle(this);
        }

        public string FilteredName
        {
            get
            {
                if (this.DisplayName != null)
                {
                    foreach (String filter in Constants.FilterVehicleDescriptors)
                    {
                        // String.Contains doesn't have a case insentiive method. Alternative method.
                        if (this.DisplayName.IndexOf(filter, 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                        {
                            return this.DisplayName.Replace(filter, "").Trim();
                        }
                    }
                    return this.DisplayName;
                }
                else
                {
                    return this.ClassName;
                }
            }
        }
    }
}
