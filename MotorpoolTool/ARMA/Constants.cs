﻿using System;
using System.Collections.Generic;

namespace MotorpoolTool.ARMA
{
    // todo: move more ARMA constants here
    public static class Constants
    {
        public enum Side
        {
            BLUFOR,
            OPFOR,
            INDEP,
            CIVIL
        }

        public static Side StringToSide(string s)
        {
            switch (s.ToUpper())
            {
                case "BLUFOR":
                    return Side.BLUFOR;
                case "OPFOR":
                    return Side.OPFOR;
                case "INDEP":
                    return Side.INDEP;
                case "CIVIL":
                    return Side.CIVIL;
                default:
                    throw new ArgumentException();
            }
        }

        public const string TAB = "    ";

        public const int PILOT = -1;
        public const int GUNNER = 0;

        // Vehicle Regulars / Recon Locked / Special Forces Locked
        public const ushort REGULARS = 0;
        public const ushort RECON = 1;
        public const ushort SPECIAL_FORCES = 2;

        // Vehicle Escalations / Levels / Classes
        public const ushort UTILITY = 1;
        public const ushort ANTI_CAR = 2;
        public const ushort ANTI_APC = 3;
        public const ushort ANTI_IFV = 4;
        public const ushort ANTI_MBT = 5;

        // Vehicle Categories
        public const ushort MISC = 0; // cargo containers
        public const ushort TRUCK = 1;
        public const ushort CAR = 2;
        public const ushort MRAP = 3; //unreliable, RHS UAZs will show as MRAPs in game
        public const ushort APC = 4;
        public const ushort IFV = 5;
        public const ushort MBT = 6;
        public const ushort ROTARY = 7;
        public const ushort FIXED = 8;
        public const ushort UNMANNED = 9;
        public const ushort SHIP = 10;

        // Accessory types. Flat offests are applied to make life easier.
        public const ushort OFFSET_OPTIC = 0;
        public const ushort OFFSET_RAIL = 32;
        public const ushort OFFSET_REST = 64;
        public const ushort OFFSET_BARREL = 128;
        public const ushort OFFSET_PARACHUTE = 256;
        public const ushort OFFSET_NIGHTVISION = 512;
        public enum AccessoryLevel
        {
            OPTIC_NONE = 0 + OFFSET_OPTIC,
            OPTIC_1X   = 1 + OFFSET_OPTIC,
            OPTIC_2X   = 2 + OFFSET_OPTIC,
            OPTIC_3X   = 3 + OFFSET_OPTIC,
            OPTIC_4X   = 4 + OFFSET_OPTIC,
            OPTIC_6X   = 6 + OFFSET_OPTIC,
            OPTIC_8X   = 8 + OFFSET_OPTIC,

            RAIL_NONE  = 0 + OFFSET_RAIL,
            RAIL_LIGHT = 1 + OFFSET_RAIL,
            RAIL_LASER = 2 + OFFSET_RAIL,
            RAIL_COMBO = 3 + OFFSET_RAIL,

            REST_NONE  = 0 + OFFSET_REST,
            REST_GRIP  = 1 + OFFSET_REST,
            REST_BIPOD = 2 + OFFSET_REST,

            BARREL_NONE  = 0 + OFFSET_BARREL,
            BARREL_FLASH = 1 + OFFSET_BARREL,
            BARREL_SOUND = 2 + OFFSET_BARREL,

            PARACHUTE   = 0 + OFFSET_PARACHUTE,
            NIGHTVISION = 0 + OFFSET_NIGHTVISION
        }

        public enum AccessorySlot
        {
            SLOT_TOP,       // Scopes
            SLOT_SIDE,      // Flashlights, lasers
            SLOT_BOTTOM,    // Bipods, grips
            SLOT_BARREL,    // Flashhiders, suppressors
            CHESTPACK,      // Extra backpack worn on front of chest
            HEADMOUNT       // Nightvision
        }

        /// <summary>
        /// Dataset used to change the textSingular of vehicles to a constant number.
        /// Not a perfect system as ARMA tragically doesn't distinguish between "vehicles with tracks", IFVs, MBTs, etc, in most cases.
        /// </summary>
        public static Dictionary<string, ushort> ARMAVehicleCategory = new Dictionary<string, ushort>
        {
            { "truck",                  TRUCK },
            { "PTS",                    TRUCK }, // RHS
            { "car",                    CAR },
            { "Tigr",                   CAR }, // RHS
            { "MRAP",                   CAR },
            { "alpha victor (MRAP)",    CAR },
            { "APC",                    APC },
            { "armor",                  APC },
            { "IFV",                    IFV },
            { "BMD",                    IFV }, // RHS
            { "BMP",                    IFV }, // RHS
            { "tank",                   MBT },
            { "helicopter",             ROTARY },
            { "gunship",                ROTARY },
            { "UAV",                    UNMANNED },
            { "UGV",                    UNMANNED },
            { "fast mover",             FIXED },
            { "Su25",                   FIXED }, // RHS
            { "ship",                   SHIP },
            { "boat",                   SHIP },
            { "attack boat",            SHIP },
            { "SDV",                    SHIP },
            { "parachute",              MISC },
            { "unknown",                MISC }
        };

        /// <summary>
        /// Not an official list, but used to roughly vehicles into a similar order as they have been sorted historically on the Google Sheet.
        /// TODO Get categories from BWI Addons
        /// </summary>
        public static Dictionary<ushort, string> BWIVehicleCategory = new Dictionary<ushort, string>
        {
            { TRUCK,    "Truck" },
            { CAR,      "Car" },
            { MRAP,     "MRAP" },
            { APC,      "APC" },
            { IFV,      "IFV" },
            { MBT,      "Tracked" }, // Some non-MBTs are labeled as 'tank' in the arma configs
            { ROTARY,   "Rotary" },
            { FIXED,    "Fixed" },
            { UNMANNED, "Unmanned" },
            { SHIP,     "Ship" },
            { MISC,     "Miscellaneous" },
        };

        /// <summary>
        /// Hard-coded dictionary of utility vehicles that should be grouped together when putting stuff on the Google Sheet.
        /// TODO: Solve these dynamically.
        /// </summary>
        public static Dictionary<string, string> GrouppedUtilityVehicles = new Dictionary<string, string>
        {
            { "GAZ-66",     "GAZ-66"},
            { "Ural",       "Ural-4320"},
            { "ZiL",        "ZiL-131"},
            { "KamAZ",      "KamAZ-5350"},
            { "Iveco",      "Iveco Truck"},
            { "Praga",      "Praga V3S"},
            { "MAN HX60",   "MAN HX60 4x4"},
            { "MAN HX58",   "MAN HX58 6x6"},
            { "LKW",        "LKW KAT I"},
            { "Land Rover", "Land Rover"},
            { "UAZ-3151",   "UAZ-3151"},
            { "M151",       "M151 Mutt"},
            { "M939",       "M939 5-ton Truck"},
            { "M97",        "HEMTT"},
            { "A1P2",       "FMTV" },
            { "MTVR",       "MTVR"},
            { "Offroad",    "Offroad" },
            { "HMMW",       "Humvee" },
            { "M998",       "M998 2D Humvee" },
            { "M1123",      "M1123 2D Humvee" },
            { "M1025",      "M1025 4D Humvee" },
            { "M1043",      "M1043 4D Humvee" },
            { "M113",       "M113" },
            { "C-130",      "C-130" },
            { "GAZ Vodnik", "GAZ Vodnik" },
            { "gl Wolf",    "Wolf" }
        };

        /// <summary>
        /// Some vehicle displayNames specific their color, but we don't want that in the Google Sheet.
        /// </summary>
        public static List<string> FilterVehicleDescriptors = new List<string>
        {
            "(Black)",
            "(Sand)",
            "(Desert)",
            "Desert",
            "(Green)",
            "(Open)",
            "(Unarmed)",
            "(Small)",
            "(Camo)",
            "(Generic)",
            "Woodland",
            "(Woodland)"
        };

        /// <summary>
        /// Escalation, AKA 'Class' or 'Level'.
        /// </summary>
        public static Dictionary<string, ushort> EscalationLookup = new Dictionary<string, ushort>()
        {
            { "UTILITY", UTILITY },
            { "ANTI_CAR", ANTI_CAR },
            { "ANTI_APC", ANTI_APC },
            { "ANTI_IFV", ANTI_IFV },
            { "ANTI_MBT", ANTI_MBT }
        };

    }
}
