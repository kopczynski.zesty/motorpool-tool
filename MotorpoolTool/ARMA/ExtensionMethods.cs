﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExtensionMethods.ARMA
{
    public static class ListExtension
    {
        public static List<T> Clone<T>(this List<T> listToClone) where T : ICloneable
        {
            if (listToClone == null) { return null; }
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }
    }
}
