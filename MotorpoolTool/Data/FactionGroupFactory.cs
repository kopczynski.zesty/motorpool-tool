﻿using MotorpoolTool.BWI;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace MotorpoolTool.Data
{
    public class FactionGroupFactory : Factory
    {
        private static readonly Dictionary<string, Regex> rxKeywords = new Dictionary<string, Regex>()
        {
            { "class",  new Regex(@"(?:class\s+)(.*?)(?:\:)",       RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "name",   new Regex(@"(?:name\s*=\s*.)(.*?)(?:.;)",   RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "side",   new Regex(@"(?:side\s*=\s*)(.*?)(?:;)",     RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "flag",   new Regex(@"(?:flag\s*=\s*.)(.*?)(?:.;)",   RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "icon",   new Regex(@"(?:icon\s*=\s*.)(.*?)(?:.;)",   RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "image",  new Regex(@"(?:image\s*=\s*.)(.*?)(?:.;)",  RegexOptions.Compiled | RegexOptions.IgnoreCase) }
        };
        private static readonly Regex rxInclude = new Regex(@"(?:\#include\s*<)(\d+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);


        public static List<FactionGroup> StringToList(string text, string path = null)
        {
            List<FactionGroup> factionGroup = new List<FactionGroup>();
            var entries = GetClass(text);

            foreach (string factionGroupEntry in entries)
            {
                // Used for the Vehicle() constructor at the end.
                string[] capturedFactionGroup = new string[rxKeywords.Count];
                int index = 0;

                foreach (KeyValuePair<string, Regex> rx in rxKeywords)
                {
                    // The BWI Classname is 'terminated' by a colon instead of a semi-colon.
                    if (index == 0)
                        capturedFactionGroup[index] = CaptureProperty(factionGroupEntry, rx.Key, ':', true);
                    else
                        capturedFactionGroup[index] = CaptureProperty(factionGroupEntry, rx.Key, ';', true);

                    if (capturedFactionGroup[index] != null)
                    {
                        // Matches[0] is the first match, Groups[1] is the first capture group, Value gets the string.
                        MatchCollection matches = rxKeywords[rx.Key].Matches(capturedFactionGroup[index]);
                        capturedFactionGroup[index] = matches[0].Groups[1].Value;
                    }
                    index++;
                }

                // Create faction. Saving the directroy makes searching through files a little easier.
                var group = new FactionGroup(capturedFactionGroup[0], capturedFactionGroup[1], capturedFactionGroup[2], path);
                MatchCollection years = rxInclude.Matches(factionGroupEntry);
                foreach (Match year in years)
                {
                    // Some factionGroups declare multiple factions and years. Keep track of the years to make life easier.
                    // TODO deprecated?
                    group.Years.Add(ushort.Parse(year.Groups[1].Value));

                    string subFactionPath = Path.Combine(path, year.Groups[1].Value);

                    group.SubFactions.AddRange(FactionFactory.GetFaction(group,
                        System.IO.File.ReadAllText(Directory.GetFiles(
                            subFactionPath, "faction.hpp", SearchOption.AllDirectories)[0]), subFactionPath));
                }
                factionGroup.Add(group);
            }

            // Return list of processed vehicles.
            return factionGroup;
        }
    }
}
