﻿using MotorpoolTool.BWI;
using MotorpoolTool.BWI.Accessories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MotorpoolTool.Data
{
    public class AccessoryFactory : Factory
    {
        private enum Property
        {
            className,
            year,
            compatiableClasses
        }

        /// <summary>
        /// A dictonary listing of every keyword and a regular expression used to extract that keyword.
        /// </summary>
        private static readonly Dictionary<string, Regex> rxKeywords = new Dictionary<string, Regex>()
        {
            { "class",              new Regex(@"(?:class\s+)(.*?)(?:\:)",                       RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "year",               new Regex(@"(?:year\s*=\s*)(\d*?)(?:;)",                    RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "compatibleClasses",  new Regex(@"(?:compatibleClasses\[\]\s*=\s*{)(.*?)(?:};)",  RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline) }
        };

        public static List<Accessory> FileToList(string filePath)
        {
            if(!File.Exists(filePath))
            {
                System.Console.WriteLine("File {0} not found", filePath);
                return null;
            }

            var accessories = new List<Accessory>();
            var data = File.ReadAllText(filePath);

            var entries = GetClass(data);

            foreach(string accessory in entries)
            {
                string[] capturedAccessory = new string[rxKeywords.Count];
                int index = 0;

                foreach(KeyValuePair<string, Regex> rx in rxKeywords)
                {
                    if (index == 0)
                        capturedAccessory[index] = CaptureProperty(accessory, rx.Key, ':', true);
                    else
                        capturedAccessory[index] = CaptureProperty(accessory, rx.Key, ';', true);

                    if(capturedAccessory[index] != null)
                    {
                        var match = rxKeywords[rx.Key].Match(capturedAccessory[index]);
                        capturedAccessory[index] = match.Groups[1].Value;
                    }
                    index++;
                }

                // if compatiableClasses is null, that means it is available for everyone
                // e.g. parachutes are not limited to certain vests or anything like that
                var className = capturedAccessory[(int)Property.className].Trim();
                var year = int.Parse(capturedAccessory[(int)Property.year]);
                var compatibleClasses = !string.IsNullOrEmpty(capturedAccessory[(int)Property.compatiableClasses]) ? Regex.Replace(capturedAccessory[(int)Property.compatiableClasses], "[\"\t\r\n ]", string.Empty).Trim().Split(',') : null;

                accessories.Add(new Accessory(className, year, compatibleClasses));
            }

            return accessories;
        }
    }
}
