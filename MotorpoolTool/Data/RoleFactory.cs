﻿using MotorpoolTool.BWI;
using MotorpoolTool.BWI.Accessories;
using MotorpoolTool.BWI.Roles;
using MotorpoolTool.GUI;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using static MotorpoolTool.ARMA.Constants;

namespace MotorpoolTool.Data
{
    public class RoleFactory : Factory
    {
        private static int CLASSNAME = 0;
        private static int NAME = 1;
        private static int CALLSIGN = 2;
        private static int ALLOWED_ACCESSORIES = 3;
        private static int IS_MEDIC = 4;
        private static int IS_ENGINEER = 5;
        private static int IS_EOD = 6;
        private static int G_FORCE_COEF = 7;
        private static int STAMINA_FACTOR = 8;
        private static int ACRE_INTERPRETER = 9;
        private static readonly Dictionary<string, Regex> rxKeywords = new Dictionary<string, Regex>()
        {
            { "class",                  new Regex(@"(?:class\s+)(.*?)(?:\:)",                       RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "name",                   new Regex(@"(?:name\s*=\s*)(.*?)(?:;)",                     RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "callsign",               new Regex("(?:callsign\\s*=\\s*)\"(.*?)\"(?:;)",                 RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "allowedAccessories",     new Regex(@"(?:allowedAccessories\[\]\s*=\s*{)(.*?)(?:};)", RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "aceIsMedic",             new Regex(@"(?:aceIsMedic\s*=\s*)(\d+)(?:;)",               RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "aceIsEngineer",          new Regex(@"(?:aceIsEngineer\s*=\s*)(\d+)(?:;)",            RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "aceIsEOD",               new Regex(@"(?:aceIsEOD\s*=\s*)(\d+)(?:;)",                 RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "aceGForceCoef",          new Regex(@"(?:aceGForceCoef\s*=\s*)(.*?)(?:;)",            RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "aceStaminaFactor",       new Regex(@"(?:aceStaminaFactor\s*=\s*)(.*?)(?:;)",         RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "acreBabelInterpreter",   new Regex(@"(?:acreBabelInterpreter\s*=\s*)(\d+)(?:;)",     RegexOptions.Compiled | RegexOptions.IgnoreCase) }
        };

        public static List<Role> StringToList(string text, string path = null)
        {
            List<Role> roles = new List<Role>();
            var entries = GetClass(text);

            foreach (string role in entries)
            {
                string[] capturedRole = new string[rxKeywords.Count];
                int index = 0;

                foreach (KeyValuePair<string, Regex> rx in rxKeywords)
                {
                    if (index == 0)
                        capturedRole[index] = CaptureProperty(role, rx.Key, ':', true);
                    else
                        capturedRole[index] = CaptureProperty(role, rx.Key, ';', true);

                    if (capturedRole[index] != null)
                    {
                        MatchCollection matches = rxKeywords[rx.Key].Matches(capturedRole[index]);
                        capturedRole[index] = matches[0].Groups[1].Value;
                    }
                    index++;
                }

                string className = capturedRole[CLASSNAME];
                string name = capturedRole[NAME].Replace("\"", string.Empty);
                string callsign = !string.IsNullOrEmpty(capturedRole[CALLSIGN]) ? capturedRole[CALLSIGN] : null;

                // Order: Optic, Side Rail, Bottom Rail, Barrel
                AllowedAccessory allowedAccessories;
                if (capturedRole[3] != null)
                {
                    var accessories = capturedRole[ALLOWED_ACCESSORIES].Split(',');

                    // Arrry order: Top Rail, Side Rail, Bottom Rail and Barrel
                    allowedAccessories.TopRail = (AccessoryLevel)(int.Parse(accessories[0]) + OFFSET_OPTIC);
                    allowedAccessories.SideRail = (AccessoryLevel)(int.Parse(accessories[1]) + OFFSET_RAIL);
                    allowedAccessories.BottomRail = (AccessoryLevel)(int.Parse(accessories[2]) + OFFSET_REST);
                    allowedAccessories.Barrel = (AccessoryLevel)(int.Parse(accessories[3]) + OFFSET_BARREL);
                }
                else
                {
                    allowedAccessories.TopRail = AccessoryLevel.OPTIC_NONE;
                    allowedAccessories.SideRail = AccessoryLevel.RAIL_NONE;
                    allowedAccessories.BottomRail = AccessoryLevel.REST_NONE;
                    allowedAccessories.Barrel = AccessoryLevel.BARREL_NONE;
                }

                _ = int.TryParse(capturedRole[IS_MEDIC], out int isMedic);
                _ = int.TryParse(capturedRole[IS_ENGINEER], out int isEngineer);
                _ = int.TryParse(capturedRole[IS_EOD], out int isEOD);
                _ = float.TryParse(capturedRole[G_FORCE_COEF], NumberStyles.Any, Program.MPTCulture, out float aceGForceCoef);
                _ = float.TryParse(capturedRole[STAMINA_FACTOR], NumberStyles.Any, Program.MPTCulture, out float aceStaminiaFactor);
                bool arceBabelInterpreter = capturedRole[ACRE_INTERPRETER] != null ? !capturedRole[ACRE_INTERPRETER].Equals("0") : false;

                roles.Add(new Role(className, name, callsign, allowedAccessories, isMedic, isEngineer, isEOD, aceGForceCoef, aceStaminiaFactor, arceBabelInterpreter));

            }

            return roles;
        }
    }
}
