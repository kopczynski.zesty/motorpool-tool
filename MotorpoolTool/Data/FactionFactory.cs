﻿using MotorpoolTool.ARMA;
using MotorpoolTool.BWI;
using MotorpoolTool.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace MotorpoolTool.Data
{
    public class FactionFactory : Factory
    {
        private static readonly int CLASS = 0;
        private static readonly int NAME = 1;
        private static readonly int YEAR = 2;
        private static readonly int TYPE = 3;
        private static readonly int UNIFORM_SCRIPT = 4;
        private static readonly int WEAPON_SCRIPT = 5;
        private static readonly int GRENADE_SCRIPT = 6;
        private static readonly int AMMO_SCRIPT = 7;
        private static readonly int HIDDEN_ELEMENTS = 8;
        private static readonly int HIDDEN_ROLES = 9;
        private static readonly int INCLUDE_MOTORPOOL = 10;
        private static readonly int INCLUDE_RESUPPLY = 11;

        private static readonly Dictionary<string, Regex> rxKeywords = new Dictionary<string, Regex>()
        {
            { "class",                  new Regex(@"(?:class\s+)(.*?)(?:\:)",                   RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "name",                   new Regex(@"(?:name\s*=\s*.)(.*)(?:.;)",                RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "year",                   new Regex(@"(?:year\s*=\s*)(\d*)(?:;)",                 RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "type",                   new Regex(@"(?:type\s*=\s*)(\w*)(?:;)",                 RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "uniformScript",          new Regex(@"(?:uniformScript.*?\\)(\w+.sqf)",           RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "weaponScript",           new Regex(@"(?:weaponScript.*?\\)(\w+.sqf)",            RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "grenadeScript",          new Regex(@"(?:grenadeScript.*?\\)(\w+.sqf)",           RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "ammoScript",             new Regex(@"(?:ammoScript.*?\\)(\w+.sqf)",              RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "hiddenElements",         new Regex(@"hiddenElements\[\]\s+=\s+(\{.*\};)",        RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "hiddenRoles",            new Regex(@"hiddenRoles\[\]\s+=\s+(\{.*\};)",           RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "#include <motorpool",    new Regex(@"(?:\#include\s*<)(motorpool\w*\.hpp)(?:>)", RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "#include <resupply",     new Regex(@"(?:\#include\s*<)(resupply\w*\.hpp)(?:>)",  RegexOptions.Compiled | RegexOptions.IgnoreCase) }
        };

        private static readonly Regex rxAllowedSuppliesRegular  = new Regex(@"(?:allowedSupplies\[\].*?{)(.*?)(?:};)",          RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex rxAllowedSuppliesWeapons  = new Regex(@"(?:allowedSuppliesWeapons\[\].*?{)(.*?)(?:};)",   RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex rxAllowedSuppliesRecon    = new Regex(@"(?:allowedSuppliesRecon\[\].*?{)(.*?)(?:};)",     RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex rxAllowedSuppliesSpecial  = new Regex(@"(?:allowedSuppliesSpecial\[\].*?{)(.*?)(?:};)",   RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);

        public static List<Faction> GetFaction(FactionGroup group, string text, string path = null)
        {
            List<Faction> subFaction = new List<Faction>();
            var entries = GetClass(text);

            foreach (string factionEntry in entries)
            {
                // Used for the Vehicle() constructor at the end.
                string[] capturedFaction = new string[rxKeywords.Count];
                int index = 0;

                foreach (KeyValuePair<string, Regex> rx in rxKeywords)
                {
                    // The BWI Classname is 'terminated' by a colon instead of a semi-colon.
                    if (index == 0)
                        capturedFaction[index] = CaptureProperty(factionEntry, rx.Key, ':', true);
                    else
                        capturedFaction[index] = CaptureProperty(factionEntry, rx.Key, ';', true);

                    if (capturedFaction[index] != null)
                    {
                        // Matches[0] is the first match, Groups[1] is the first capture group, Value gets the string.
                        MatchCollection matches = rxKeywords[rx.Key].Matches(capturedFaction[index]);
                        capturedFaction[index] = matches[0].Groups[1].Value;
                    }
                    index++;
                }
                //const string gearScript = @"\bwi_data_main\configs\factions\gear.sqf";

                string bwiName = capturedFaction[CLASS].Trim();
                string name = capturedFaction[NAME].Trim();
                ushort year = ushort.Parse(capturedFaction[YEAR]);
                string type = capturedFaction[TYPE].Trim();
                string uniformScript = string.Format("{0}\\{1}", path, capturedFaction[UNIFORM_SCRIPT].Trim());
                string weaponScript = string.Format("{0}\\{1}", path, capturedFaction[WEAPON_SCRIPT].Trim());
                string grenadeScript = string.Format("{0}\\{1}", path, capturedFaction[GRENADE_SCRIPT].Trim());
                string ammoScript = string.Format("{0}\\{1}", path, capturedFaction[AMMO_SCRIPT].Trim());
                string[] hiddenElements = !string.IsNullOrEmpty(capturedFaction[HIDDEN_ELEMENTS]) ? Regex.Replace(capturedFaction[HIDDEN_ELEMENTS], "[ \"{};]", string.Empty).Split(',') : null;
                string[] hiddenRoles = !string.IsNullOrEmpty(capturedFaction[HIDDEN_ROLES]) ? Regex.Replace(capturedFaction[HIDDEN_ROLES], "[ \"{};]", string.Empty).Split(',') : null;
                string motorpoolPath = capturedFaction[INCLUDE_MOTORPOOL].Trim();
                string resupplyPath = capturedFaction[INCLUDE_RESUPPLY].Trim();

                // Get resupply location, read it, and then interpret it.
                string factionResupplyPath = Path.Combine(path, resupplyPath);
                string factionResupplyText = File.ReadAllText(factionResupplyPath);

                var resupplyRegular = ResupplyStringToArrayHelper(factionResupplyText, rxAllowedSuppliesRegular);
                var resupplyWeapons = ResupplyStringToArrayHelper(factionResupplyText, rxAllowedSuppliesWeapons);
                var resupplyRecon   = ResupplyStringToArrayHelper(factionResupplyText, rxAllowedSuppliesRecon);
                var resupplySpecial = ResupplyStringToArrayHelper(factionResupplyText, rxAllowedSuppliesSpecial);

                // Create faction. Saving the directroy makes searching through files a little easier.
                Faction faction = new Faction(group, bwiName, name, year, type,
                    uniformScript, weaponScript, grenadeScript, ammoScript,
                    resupplyRegular, resupplyWeapons, resupplyRecon, resupplySpecial,
                    hiddenElements, hiddenRoles);

                // Get motorpool location, read it, and then interpret it.
                string factionMotorpoolPath = Path.Combine(path, motorpoolPath);
                string factionMotorpoolText = File.ReadAllText(factionMotorpoolPath);

                // Remove quotes to make splitting up the entries later a little easier.
                factionMotorpoolText = factionMotorpoolText.Replace("\"", "");

                Regex rxAllowedVehicles = new Regex(@"(?:allowed.*?{)(.*?)(?:};)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
                var factionMotorpoolEntries = GetClass(factionMotorpoolText, "allowed");

                int vehicleAssignmentIndex = 0;
                foreach (String motorpoolVehicle in factionMotorpoolEntries)
                {
                    // Regulars 0, Recon 1, Special Forces 2

                    Match strippedVehicles = rxAllowedVehicles.Match(motorpoolVehicle);
                    var splitVehicles = strippedVehicles.Groups[1].Value.Split(',');

                    for (int i = 0; i < splitVehicles.Length; i++)
                    {
                        splitVehicles[i] = splitVehicles[i].Trim();
                    }

                    foreach (String factionVehicle in splitVehicles)
                    {
                        factionVehicle.Trim();

                        // Look-up vehicle, don't add duplicates.
                        MotorpoolVehicle.Lookup.TryGetValue(factionVehicle.ToLowerInvariant(), out MotorpoolVehicle vehicle);

                        if (vehicle != null && !faction.MotorpoolRegulars.Contains(vehicle))
                        {
                            // Need to clone vehicle, as you can have multiple entries of MotorpoolVehicle in the same Faction.
                            // e.g. Quadbikes are usually available in both Recon and Special Forces.
                            var clonedVehicle = (MotorpoolVehicle)vehicle.Clone();

                            // Add the vehicle to the appropriate motorpool.
                            switch (vehicleAssignmentIndex)
                            {
                                case Constants.REGULARS:
                                    faction.MotorpoolRegulars.Add(clonedVehicle);
                                    break;
                                case Constants.RECON:
                                    faction.MotorpoolRecon.Add(clonedVehicle);
                                    break;
                                case Constants.SPECIAL_FORCES:
                                    faction.MotorpoolSpecialForces.Add(clonedVehicle);
                                    break;
                                default:
                                    throw new IndexOutOfRangeException("Error when trying to assign motorpool vehicles to regulars / recon / special forces.");
                            }
                        }
                    }

                    vehicleAssignmentIndex++;
                }

                

                // Sub-Faction done 
                faction.SortMotorpool();
                subFaction.Add(new Faction(faction));
            }

            return subFaction;
        }

        private static string[] ResupplyStringToArrayHelper(string data, Regex rx)
        {
            string resupply = Helper.StripComments(rx.Match(data).Groups[1].Value);
            return Regex.Replace(resupply, "[\t\r\n\" ]", string.Empty).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
