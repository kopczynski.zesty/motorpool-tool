﻿using MotorpoolTool.BWI;
using MotorpoolTool.BWI.Accessories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using static MotorpoolTool.ARMA.Constants;

namespace MotorpoolTool.Data
{
    public class AccessoryGroupFactory : Factory
    {
        private enum Property
        {
            className,
            displayName,
            slot,
            level,
            include
        }


        /// <summary>
        /// A dictonary listing of every keyword and a regular expression used to extract that keyword.
        /// </summary>
        private static readonly Dictionary<string, Regex> rxKeywords = new Dictionary<string, Regex>()
        {
            { "class",              new Regex(@"(?:class\s+)(.*?)(?:\:)",                   RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "name",               new Regex(@"(?:name\s*=\s*.)(.*?)(?:.;)",               RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "slot",               new Regex(@"(?:slot\s*=\s*)(.*?)(?:;)",                 RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "level",              new Regex(@"(?:level\s*=\s*)(\w*?)(?:;)",               RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "#include",           new Regex(@"(?:#include\s*<)(.*?)(?:>)",                RegexOptions.Compiled | RegexOptions.IgnoreCase) }
        };

        public static List<AccessoryGroup> FileToList(string filePath)
        {
            string groupsFile = $@"{filePath}\groups.hpp";
            if (!System.IO.File.Exists(groupsFile))
            {
                System.Console.WriteLine("File {0} not found", filePath);
                return null;
            }

            var accessoryGroups = new List<AccessoryGroup>();
            var data = File.ReadAllText(groupsFile);

            var entries = GetClass(data);

            foreach (string group in entries)
            {
                string[] capturedAccessoryGroup = new string[rxKeywords.Count];
                int index = 0;

                foreach (KeyValuePair<string, Regex> rx in rxKeywords)
                {
                    if (index == 0)
                        capturedAccessoryGroup[index] = CaptureProperty(group, rx.Key, ':', true);
                    else
                        capturedAccessoryGroup[index] = CaptureProperty(group, rx.Key, ';', true);

                    if (capturedAccessoryGroup[index] != null)
                    {
                        var match = rxKeywords[rx.Key].Match(capturedAccessoryGroup[index]);
                        capturedAccessoryGroup[index] = match.Groups[1].Value;
                    }
                    index++;
                }

                var className = capturedAccessoryGroup[(int)Property.className].Trim();
                var displayName = capturedAccessoryGroup[(int)Property.displayName].Trim();
                AccessorySlot slot = (AccessorySlot)Enum.Parse(typeof(AccessorySlot), capturedAccessoryGroup[(int)Property.slot]);
                AccessoryLevel level = (AccessoryLevel)Enum.Parse(typeof(AccessoryLevel), capturedAccessoryGroup[(int)Property.level]);
                var accessoryList = AccessoryFactory.FileToList($@"{filePath}\{capturedAccessoryGroup[(int)Property.include]}");

                accessoryGroups.Add(new AccessoryGroup(className, displayName, slot, level, accessoryList));
            }

            return accessoryGroups;

        }
    }
}
