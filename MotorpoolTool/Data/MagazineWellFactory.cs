﻿using MotorpoolTool.ARMA;
using MotorpoolTool.ARMA.Weapons;
using System;
using System.Collections.Generic;
using System.IO;

namespace MotorpoolTool.Data
{
    public class MagazineWellFactory : Factory
    {
        public static Dictionary<string, MagazineWell> MagazineWellsLookup = new Dictionary<string, MagazineWell>();

        private enum Wells
        {
            className,
            inheritance,
            magazines
        }

        public static bool InitializeLookupFromDataset(string filepath)
        {
            if (!File.Exists(filepath))
            {
                System.Console.WriteLine("File {0} not found", filepath);
                return false;
            }
            else
            {
                MagazineWellFactory.MagazineWellsLookup.Clear();
                string[] externalDatasetLines = File.ReadAllLines(filepath);
                Console.Write("{0,-20} [", "CfgMagazineWells");

                int index = 0;
                foreach (string line in externalDatasetLines)
                {
                    if (line == string.Empty) { continue; }
                    var split = line.Split(';');

                    string className = split[(int)Wells.className];
                    var inhertiance = split[(int)Wells.inheritance] != string.Empty ? split[(int)Wells.inheritance].Split(new string[] { "[", ",", "]" }, StringSplitOptions.RemoveEmptyEntries) : null;
                    var magazines = split[(int)Wells.magazines] != string.Empty ? split[(int)Wells.magazines].Split(new string[] { "[", ",", "]" }, StringSplitOptions.RemoveEmptyEntries) : null;

                    /* Index values from external dataset
                     * 0 - ARMA 3 className     Config name / className for the ARMA 3 MagazineWell (referenced in CfgWeapons)
                     * 1 - magazineArray        Named array that contains a bunch of ARMA 3 Magazines (referenced in CfgMagazines)
                     */

                    MagazineWellsLookup.Add(className.ToLowerInvariant(), new MagazineWell(className, inhertiance, magazines));

                    if (index % (Math.Max(externalDatasetLines.Length, 20) / 20) == 0)
                    {
                        Console.Write("|");
                    }
                    index++;
                }
                Console.WriteLine("] {0}/{1}", MagazineWellsLookup.Count, MagazineWellsLookup.Count);
                return true;
            }
        }
    }
}
