﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MotorpoolTool.Data
{
    public class Factory
    {
        protected static List<string> GetClass(string text, string keyword = "class")
        {
            List<string> entries = new List<string>();

            // Regex cannot keep track of how deep it is.
            // Use regular tracking of { and } to track how 'deep' we are in a class.
            for (int index = 0; (index + keyword.Length) < text.Length; index++)
            {
                if (text.Substring(index, keyword.Length).Equals(keyword))
                {
                    int subIndex;
                    int bracketDepth = 0;

                    // Look for semi-colon and bracket depth is 0.
                    // e.g. class Example { content }; <--- Capture everything from class to ;
                    for (subIndex = index; !(text[index].Equals(';') && bracketDepth == 0)
                        && index < text.Length; index++)
                    {
                        if (text[index].Equals('{')) { bracketDepth++; }
                        else if (text[index].Equals('}')) { bracketDepth--; }
                    }
                    entries.Add(text.Substring(subIndex, index - subIndex + 1));
                }
            }
            return entries;
        }

        public static List<string> CaptureProperties(string text, string keyword, char terminator)
        {
            var entries = new List<string>();

            for (int index = text.IndexOf(keyword); index < (text.Length - keyword.Length); index++)
            {
                // Look for opening switch statement.
                if (text.Substring(index, keyword.Length).Equals(keyword))
                {
                    int subIndex;
                    int bracketDepth = 0;

                    // Stop reading when bracket depth is 0 and we hit a semi-colon.
                    for (subIndex = index; !(text[index].Equals(terminator) && bracketDepth == 0) && index < text.Length; index++)
                    {
                        if (text[index].Equals('{'))
                            bracketDepth++;
                        else if (text[index].Equals('}'))
                            bracketDepth--;
                    }

                    // We've exited the loop.
                    entries.Add(text.Substring(subIndex, index - subIndex + 1));
                }
            }

            return entries;
        }

        protected static string CaptureProperty(string text, string keyword, char terminator, bool getFirstMatch = false)
        {
            // TODO I rewrote this because it sucked. It should have used IndexOf from the start.
            // Review this again and determine if it could be faster.

            int startIndex = text.IndexOf(keyword);

            if(startIndex == - 1)
            {
                return null; // Keyword not in text
            }

            int subLength = 0;
            int bracketDepth = 0;

            // Look for keyword e.g. classname
            for (int index = startIndex; index < text.Length; index++)
            {
                // Track bracket depth to find correct terminator.
                if (text[index].Equals('{'))
                    bracketDepth++;
                else if (text[index].Equals('}'))
                    bracketDepth--;

                // Look for the terminating character
                
                if (text[index].Equals(terminator) && bracketDepth == 0)
                {
                    // Found terminator.
                    subLength++;

                    // If getFirstMatch, exit. If not, find the next keyword. If not found, exit.
                    if (!getFirstMatch && text.IndexOf(keyword, index) != -1)
                    {
                        startIndex = text.IndexOf(keyword, index);
                        index = startIndex;
                        subLength = 0;
                    }
                    else
                    {
                        break;
                    }
                }
                subLength++;
            }

            // Finished searching, take the last match found.
            string debug = text.Substring(startIndex, subLength);
            return debug;
        }
    }
}
