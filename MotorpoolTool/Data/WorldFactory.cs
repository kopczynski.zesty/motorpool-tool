﻿using MotorpoolTool.ARMA;
using MotorpoolTool.GUI;
using System;
using System.Globalization;

namespace MotorpoolTool.Data
{
    public class WorldFactory : Factory
    {
        /* Index values from external dataset. */
        private enum Worlds
        {
            className,      // Config name / className of the ARMA 3 map
            inheritance,
            description,    // Gets the human readable name of the map
            mapSize,        // From BIS_fnc_mapSize
            latitude,       // Latitude for the map, should be used to figure out when the sun rises / sets.
            longitude,      // Longitude for map, should be used to figure out when the sun rises / sets.
            grid            // Array in format: [[Zoom1, format, formatX, formatY, stepX, stepY, zoomMax], [Zoom2, ...], [Zoom3, ...]]
        }
        /* Children of Grid for reference: 
         *  format              States the grid format (e.g. XY means X coord first, then Y)
         *  formatX             Numerical format for X (e.g. 000 would mean grids down to 100m of detail)
         *  formatY             Numerical format for Y (e.g. 0000 would mean grids down to 10m of detail)
         *  stepX               Defines the grid granularity for X (100 means 100m grid segments, 1000 means 1000m grid segments, etc)
         *  stepY               Defines the grid granularity for y (10 means 10m grid segments, 10000 means 10000m grid segments, etc)
         *  zoomMax             TODO Figure this out, didn't have documentation for this.
         */

        public static bool InitializeLookupFromDataset(string filePath)
        {
            if (!System.IO.File.Exists(filePath))
            {
                System.Console.WriteLine("File {0} not found", filePath);
                return false;
            }
            else
            {
                World.WorldDictionary.Clear();

                string[] externalDatasetLines = System.IO.File.ReadAllLines(filePath);
                Console.Write("{0,-20} [", "CfgWorlds");

                int index = 0;
                foreach (string line in externalDatasetLines)
                {
                    if (line == string.Empty) { continue; }
                    var split = line.Split(';');

                    // Use constants because this gets really hard to reorganize later without them.
                    string className = split[(int)Worlds.className].Trim();
                    var inheritance = split[(int)Worlds.inheritance] != string.Empty ? split[(int)Worlds.inheritance].Split(new string[] { "[", ",", "]" }, StringSplitOptions.RemoveEmptyEntries) : null;
                    string description = !string.IsNullOrEmpty(split[(int)Worlds.description]) ? split[(int)Worlds.description].Trim() : null;
                    float mapSize = !string.IsNullOrEmpty(split[(int)Worlds.mapSize]) ? float.Parse(split[(int)Worlds.mapSize], NumberStyles.Float, Program.MPTCulture) : 0;
                    float latitude = !string.IsNullOrEmpty(split[(int)Worlds.latitude]) ? float.Parse(split[(int)Worlds.latitude], NumberStyles.Float, Program.MPTCulture) : 0;
                    float longitude = !string.IsNullOrEmpty(split[(int)Worlds.longitude]) ? float.Parse(split[(int)Worlds.longitude], NumberStyles.Float, Program.MPTCulture) : 0;
                    var grids = split[(int)Worlds.grid] != string.Empty ? Cfg.StringToArray(split[(int)Worlds.grid]) : null;

                    World.WorldDictionary.Add(className.ToLowerInvariant(), new World(
                        className, inheritance, description, mapSize, latitude, longitude, grids));

                    if (index % (Math.Max(externalDatasetLines.Length, 20) / 20) == 0)
                    {
                        Console.Write("|");
                    }
                    index++;
                }
                Console.WriteLine("] {0}/{1}", World.WorldDictionary.Count, World.WorldDictionary.Count);
                return true;
            }
        }

    }
}
