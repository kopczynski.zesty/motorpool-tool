﻿using MotorpoolTool.ARMA;
using System;
using System.IO;
using MotorpoolTool.ARMA.Weapons;
using System.Text;

namespace MotorpoolTool.Data
{
    public class MagazineFactory : Factory
    {
        //public static Dictionary<string, Magazine> MagazinesLookup = new Dictionary<string, Magazine>();

        private enum Magazines
        {
            className,          // Config name / className fo the ARMA 3 magazine
            inheritance,        // List of inhertiance, e.g. [BLUFOR_OFFROAD_AT, OFFROAD_BASE, LAND_VEHICLE]
            displayName,        // Gets the displayName in ARMA 3 (Human readable)
            displayNameShort,   // Shorter version of displayName, shown on the Weapon HUD
            descriptionShort,   // Short description of the magazine, usually shown in invetory or arsenal
            pylonWeapon,        // If this is a pylon magazine, this points at the CfgWeapons entry this pylon belongs to
            ammo,               // Points to the CfgAmmo entry that this magazine uses
            count,              // The amount of bullets / missiles / rockets / etc that this magazine contains
            mass
        }

        public static bool InitializeLookupFromDataset(string filepath)
        {
            if (!File.Exists(filepath))
            {
                System.Console.WriteLine("File {0} not found", filepath);
                return false;
            }
            else
            {
                Magazine.MagazineDictionary.Clear();
                string[] externalDatasetLines = File.ReadAllLines(filepath);
                Console.Write("{0,-20} [", "CfgMagazines");

                int index = 0;
                foreach (string line in externalDatasetLines)
                {
                    if (line == string.Empty) { continue; }
                    var split = line.Split(';');

                    string className = split[(int)Magazines.className];
                    var inhertiance = split[(int)Magazines.inheritance] != string.Empty ? split[(int)Magazines.inheritance].Split(new string[] { "[", ",", "]" }, StringSplitOptions.RemoveEmptyEntries) : null;
                    string displayName = split[(int)Magazines.displayName];
                    string displayNameShort = split[(int)Magazines.displayNameShort];
                    string descriptionShort = split[(int)Magazines.descriptionShort];

                    // Fix ARMA 3's bad UTF-8 encoding, Ã— to ×
                    var bytes = Encoding.GetEncoding(1252).GetBytes(descriptionShort);
                    descriptionShort = Encoding.UTF8.GetString(bytes);

                    string pylon = split[(int)Magazines.pylonWeapon];
                    string ammo = split[(int)Magazines.ammo];
                    string count = split[(int)Magazines.count];
                    string mass = split[(int)Magazines.mass];

                    Magazine.MagazineDictionary.Add(split[0].ToLowerInvariant(),
                        //new Magazine(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7]));
                        new Magazine(className, inhertiance, displayName, displayNameShort, descriptionShort, pylon, ammo, count, mass));

                    if (index % (Math.Max(externalDatasetLines.Length, 20) / 20) == 0)
                    {
                        Console.Write("|");
                    }
                    index++;
                }
                Console.WriteLine("] {0}/{1}", Magazine.MagazineDictionary.Count, Magazine.MagazineDictionary.Count);
                return true;
            }
        }
    }
}
