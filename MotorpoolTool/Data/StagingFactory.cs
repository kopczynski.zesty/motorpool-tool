﻿using MotorpoolTool.BWI.Stagings;
using MotorpoolTool.GUI;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace MotorpoolTool.Data
{
    public class StagingFactory : Factory
    {
        private static Regex rxClassName = new Regex(@"(?:bwi.*?\.)(.*?)(?:\\description.ext)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Dictionary<string, Regex> rxKeywordsMission = new Dictionary<string, Regex>()
        {
                { "author",             new Regex("(?:author.*?\")(.*?)(?:\";)",                RegexOptions.Compiled | RegexOptions.IgnoreCase) },
                { "OnLoadName",         new Regex("(?:OnLoadName.*?\")(.*?)(?:\";)",            RegexOptions.Compiled | RegexOptions.IgnoreCase) },
                { "OnLoadMission",      new Regex("(?:OnLoadMission.*?\")(.*?)(?:\";)",         RegexOptions.Compiled | RegexOptions.IgnoreCase) },
                { "enableDebugConsole", new Regex("(?:enableDebugConsole.*?=\\s*)(.*?)(?:;)",   RegexOptions.Compiled | RegexOptions.IgnoreCase) }
        };

        private static readonly Dictionary<string, Regex> rxKeywordsStaging = new Dictionary<string, Regex>()
        {
            { "class",                  new Regex( "(?:class.*?)(\\w+)",                        RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "name",                   new Regex("(?:name\\s*=\\s*\")(.*?)(?:\";)",            RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "position",               new Regex("(?:position.*?{)(.*?)(?:};)",                RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "teleporter",             new Regex("(?:teleporter.*?\")(.*?)(\";)",              RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "motorpoolLocations[]",   new Regex("(?:motorpoolLocations.*?{)(.*?)(?:};)",      RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline) },
            { "motorpoolLabels[]",      new Regex("(?:motorpoolLabels.*?{)(.*?)(?:};)",         RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline) },
            { "motorpoolTypes[]",       new Regex("(?:motorpoolTypes.*?{)(.*?)(?:};)",          RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline) },
            { "resupplyLocations[]",    new Regex("(?:resupplyLocations.*?{)(.*?)(?:};)",       RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline) }
        };

        public static void InitalizeStagings(string filePath)
        {
            var data = File.ReadAllText(filePath);

            string[] capturedStaging = new string[rxKeywordsMission.Count];
            int index = 0;
            foreach (var rx in rxKeywordsMission)
            {
                capturedStaging[index] = CaptureProperty(data, rx.Key, ';');

                if (capturedStaging[index] != null)
                {
                    var match = rxKeywordsMission[rx.Key].Match(capturedStaging[index]);
                    capturedStaging[index] = match.Groups[1].Value;
                }
                index++;
            }

            string terrainClassName = rxClassName.Match(filePath).Groups[1].Value;
            string author = capturedStaging[0].Trim();
            string onLoadName = capturedStaging[1].Trim();
            string onLoadMission = capturedStaging[2].Trim();
            bool enableDebugConsole = capturedStaging[3].Trim().Equals("0");

            List<string> entries = GetClass(data, "class Staging");

            // No we've seperated each Staging into a list of Stagings
            // class Staging_1 { ... };
            // class Staging_2 { ... };
            Staging.StagingListDictionary.Add(terrainClassName.ToLowerInvariant(), new List<Staging>());

            foreach (string stagingEntry in entries)
            {
                capturedStaging = new string[rxKeywordsStaging.Count];
                index = 0;
                foreach (var rx in rxKeywordsStaging)
                {
                    capturedStaging[index] = CaptureProperty(stagingEntry, rx.Key, ';');

                    if (capturedStaging[index] != null)
                    {
                        var match = rxKeywordsStaging[rx.Key].Match(capturedStaging[index]);
                        capturedStaging[index] = match.Groups[1].Value;
                    }

                    index++;
                }

                string stagingClassName = capturedStaging[0].Trim();
                string name = capturedStaging[1].Trim();
                string[] positionXYZ = capturedStaging[2].Split(',');

                var x = float.Parse(positionXYZ[0], NumberStyles.Float, Program.MPTCulture);
                var y = float.Parse(positionXYZ[1], NumberStyles.Float, Program.MPTCulture);
                //_ = float.TryParse(positionXYZ[0], out float x);
                //_ = float.TryParse(positionXYZ[1], out float y);
                Vector2f position = new Vector2f(x, y);

                string teleporter = capturedStaging[3].Trim();
                char[] charSeperators = new char[] { ',' };
                string[] motorpoolLocations = Regex.Replace(capturedStaging[4], "[\"\t\r\n]", string.Empty).Trim().Split(charSeperators, StringSplitOptions.RemoveEmptyEntries);
                string[] motorpoolLabels = Regex.Replace(capturedStaging[5], "[\"\t\r\n]", string.Empty).Trim().Split(charSeperators, StringSplitOptions.RemoveEmptyEntries);
                int[] motorpoolTypes = Array.ConvertAll(Regex.Replace(capturedStaging[6], "[\"\t\r\n]", string.Empty).Trim().Split(charSeperators, StringSplitOptions.RemoveEmptyEntries), s => int.Parse(s));
                string[] resupplyLocations = Regex.Replace(capturedStaging[7], "[\"\t\r\n]", string.Empty).Trim().Split(charSeperators, StringSplitOptions.RemoveEmptyEntries);

                Staging.StagingListDictionary[terrainClassName.ToLowerInvariant()].Add(new Staging(stagingClassName, name, position, teleporter, motorpoolLocations, motorpoolLabels, motorpoolTypes, resupplyLocations));
            }
        }
    }
}
