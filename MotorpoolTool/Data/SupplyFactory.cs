﻿using MotorpoolTool.BWI.Supplies;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MotorpoolTool.Data
{
    public class SupplyFactory : Factory
    {
        private enum Property
        {
            className,
            classNameOfResupply,
            compatible
        }

        /// <summary>
        /// A dictonary listing of every keyword and a regular expression used to extract that keyword.
        /// </summary>
        private static readonly Dictionary<string, Regex> rxKeywords = new Dictionary<string, Regex>()
        {
            { "class",              new Regex(@"(?:class\s+)(.*?)(?:\:)",               RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "classname",          new Regex(@"(?:classname\s*=\s*.)(.*?)(?:.;)",      RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "compatible[]",       new Regex(@"(?:compatible.*?{)(.*?)(?:};)",         RegexOptions.Compiled | RegexOptions.IgnoreCase) }
        };

        public static List<Supply> FileToList(string filepath)
        {
            if(!File.Exists(filepath))
            {
                Console.WriteLine($"Filepath {filepath} not found");
                return null;
            }

            Supply.SupplyDictionary.Clear();
            var supplies = new List<Supply>();
            var data = File.ReadAllText(filepath);

            var entries = GetClass(data);

            foreach(string entry in entries)
            {
                string[] capturedSupply = new string[rxKeywords.Count];
                int index = 0;

                foreach(var rx in rxKeywords)
                {
                    if (index == 0)
                        capturedSupply[index] = CaptureProperty(entry, rx.Key, ':', true);
                    else
                        capturedSupply[index] = CaptureProperty(entry, rx.Key, ';', true);

                    if(capturedSupply[index] != null)
                    {
                        capturedSupply[index] = rxKeywords[rx.Key].Match(capturedSupply[index]).Groups[1].Value;
                    }
                    index++;
                }

                var className = capturedSupply[(int)Property.className].Trim();
                var classNameOfResupplyCrate = capturedSupply[(int)Property.classNameOfResupply].Trim();
                string[] compatibleWith = !string.IsNullOrEmpty(capturedSupply[(int)Property.compatible]) ? Regex.Replace(capturedSupply[(int)Property.compatible], "[ \"]", string.Empty).Split(',') : Array.Empty<string>();

                Supply supply = new Supply(className, classNameOfResupplyCrate, compatibleWith);
                supplies.Add(supply);


                Supply.SupplyDictionary.Add(supply.ClassName, supply);
            }

            return supplies;
        }
    }
}
