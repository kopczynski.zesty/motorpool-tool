﻿using MotorpoolTool.BWI.Supplies;
using MotorpoolTool.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MotorpoolTool.Data
{
    public class SupplyGroupFactory : Factory
    {
        private enum Property
        {
            className,
            displayName,
            include
        }

        /// <summary>
        /// A dictonary listing of every keyword and a regular expression used to extract that keyword.
        /// </summary>
        private static readonly Dictionary<string, Regex> rxKeywords = new Dictionary<string, Regex>()
        {
            { "class",              new Regex(@"(?:class\s+)(.*?)(?:\:)",                   RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "name",               new Regex(@"(?:name\s*=\s*.)(.*?)(?:.;)",               RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "#include",           new Regex(@"(?:#include\s*<)(.*?)(?:>)",                RegexOptions.Compiled | RegexOptions.IgnoreCase) }
        };

        public static List<SupplyGroup> DirectoryToList(string directory)
        {
            if(!Directory.Exists(directory))
            {
                Console.WriteLine($"Directory {directory} not found");
                return null;
            }

            var categories = Directory.GetFiles(directory, "categories.hpp", SearchOption.AllDirectories);
            var supplyGroups = new List<SupplyGroup>();

            foreach (var filepath in categories)
            {
                
                var data = Helper.StripComments(File.ReadAllText(filepath));

                var entries = GetClass(data);

                foreach (string group in entries)
                {
                    var supplies = new List<Supply>();
                    string[] capturedSupplyGroup = new string[rxKeywords.Count];
                    int index = 0;

                    foreach (var rx in rxKeywords)
                    {
                        if (index == 0)
                            capturedSupplyGroup[index] = CaptureProperty(group, rx.Key, ':', true);
                        else
                            capturedSupplyGroup[index] = CaptureProperty(group, rx.Key, ';', true);

                        if (capturedSupplyGroup[index] != null)
                        {
                            var subCategories = rxKeywords[rx.Key].Matches(capturedSupplyGroup[index]);

                            switch(index)
                            {
                                case (int)Property.className:
                                case (int)Property.displayName:
                                    capturedSupplyGroup[index] = subCategories[0].Groups[1].Value;
                                    break;
                                case (int)Property.include:
                                    foreach (Match subCategory in subCategories)
                                    {
                                        // Pull file and start making supplies.
                                        var supplyPath = Path.Combine(Path.GetDirectoryName(filepath), subCategory.Groups[1].Value);
                                        supplies.AddRange(SupplyFactory.FileToList(supplyPath));
                                    }
                                    break;
                            }
                        }
                        index++;
                    }

                    var className = capturedSupplyGroup[(int)Property.className].Trim();
                    var displayName = capturedSupplyGroup[(int)Property.displayName].Trim();

                    SupplyGroup supplyGroup = new SupplyGroup(className, displayName, supplies);
                    supplyGroups.Add(supplyGroup);
                }
            }

            return supplyGroups;
        }
    }
}
