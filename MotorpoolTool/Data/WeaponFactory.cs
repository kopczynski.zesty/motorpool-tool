﻿using MotorpoolTool.ARMA;
using MotorpoolTool.ARMA.Weapons;
using MotorpoolTool.GUI;
using System;
using System.Globalization;
using System.IO;

namespace MotorpoolTool.Data
{
    public class WeaponFactory : Factory
    {
        private enum Weapons
        {
            className,          // Config name / className of the ARMA 3 weapon
            inheritance,        // List of inhertiance, e.g. [BLUFOR_OFFROAD_AT, OFFROAD_BASE, LAND_VEHICLE]
            itemType,           // Result from BIS_fnc_itemType, returns an array like [Equipment, Uniform], describes the kind of item.
            displayName,        // Gets the displayName in ARMA 3 (Human readable)
            descriptionShort,   // A short description of the weapon, usually just its name
            magazines,          // A list of magazines this weapon *could* be loaded with
            magazineWell,       // Special array of codes that let weapons share magazines from different mods
            magazineReloadTime, // Time it takes to load a new magazine
            reloadTime,         // Time between shots (fire rate)
            canLock,            // 0 never locks. 1 use to lock on lower difficulties (removed in ARMA 3 1.58). 2 can lock on
            weaponLockSystem,   // TODO, bitmask dealio that determines what kind of lock-on the weapon uses
            mass,               // A 'kind of' inventory size for items as well as 'weight' of items
            containerClass      // maximumLoad for stuff like vests gets looked up through a CfgVehicles class (are you okay Bohemia?)
        }

        //public static Dictionary<string, Weapon> WeaponsLookup = new Dictionary<string, Weapon>();

        public static bool InitializeLookupFromDataset(string filepath)
        {
            if (!File.Exists(filepath))
            {
                System.Console.WriteLine("File {0} not found", filepath);
                return false;
            }
            else
            {
                Weapon.WeaponDictionary.Clear();
                string[] externalDatasetLines = File.ReadAllLines(filepath);
                Console.Write("{0,-20} [", "CfgWeapons");

                int datasetProgress = 0;
                foreach (string line in externalDatasetLines)
                {
                    if (line == string.Empty) { continue; }
                    var split = line.Split(';');

                    // Use constants because this gets really hard to reorganize later without them.
                    string className = split[(int)Weapons.className];
                    var inhertiance = split[(int)Weapons.inheritance] != string.Empty ? split[(int)Weapons.inheritance].Split(new string[] { "[", ",", "]" }, StringSplitOptions.RemoveEmptyEntries) : null;
                    string[] itemType = split[(int)Weapons.itemType] != string.Empty ? split[(int)Weapons.itemType].Split(new string[] { "[", ",", "]" }, StringSplitOptions.RemoveEmptyEntries) : null;
                    string displayName = split[(int)Weapons.displayName];
                    string description = split[(int)Weapons.descriptionShort].Trim();
                    string[] magazines = split[(int)Weapons.magazines] != string.Empty ? split[(int)Weapons.magazines].Split(new string[] { "[", ",", "]" }, StringSplitOptions.RemoveEmptyEntries) : null;
                    string[] magazineWell = split[(int)Weapons.magazineWell] != string.Empty ? split[(int)Weapons.magazineWell].Split(new string[] { "[", ",", "]" }, StringSplitOptions.RemoveEmptyEntries) : null;
                    float magazineReloadTime = float.Parse(split[(int)Weapons.magazineReloadTime], NumberStyles.Float, Program.MPTCulture);
                    float reloadTime = float.Parse(split[(int)Weapons.reloadTime], NumberStyles.Float, Program.MPTCulture);
                    bool canLock = !split[(int)Weapons.canLock].Equals("0");
                    string weaponLockSystem = split[(int)Weapons.weaponLockSystem];
                    float mass = float.Parse(split[(int)Weapons.mass], NumberStyles.Float, Program.MPTCulture);
                    string containerClass = split[(int)Weapons.containerClass];

                    if (magazines.Length == 0) { magazines = null; }
                    if (magazineWell.Length == 0) { magazineWell = null; }

                    Weapon.WeaponDictionary.Add(className.ToLowerInvariant(), new Weapon(className, inhertiance, itemType, displayName, description,
                        magazines, magazineWell, magazineReloadTime, reloadTime, canLock, weaponLockSystem, mass, containerClass));

                    if (datasetProgress % (Math.Max(externalDatasetLines.Length, 20) / 20) == 0)
                    {
                        Console.Write("|");
                    }
                    datasetProgress++;
                }
                Console.WriteLine("] {0}/{1}", Weapon.WeaponDictionary.Count, Weapon.WeaponDictionary.Count);
                return true;
            }
        }
    }
}
