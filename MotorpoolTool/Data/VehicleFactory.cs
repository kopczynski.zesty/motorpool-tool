﻿using MotorpoolTool.ARMA;
using MotorpoolTool.BWI;
using MotorpoolTool.GUI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace MotorpoolTool.Data
{
    public class VehicleFactory : Factory
    {
        /* Index values from external dataset. Use this enum because reorganizing these is a pain. */
        private enum Vehicles
        {
            className,                  // Config name / className of the ARMA 3 vehicle
            inheritance,                // List of inhertiance, e.g. [BLUFOR_OFFROAD_AT, OFFROAD_BASE, LAND_VEHICLE]
            displayName,                // Gets the displayName used in ARMA 3 (Human Readable)
            ace_cargo_hasCargo,         // If 0, cannot interact with cargo. If 1, can interact with cargo via ACE interact.
            ace_cargo_space,            // Gets the ACE3 Cargo space for the object
            ace_cargo_size,             // Gets the ACE3 Cargo size for this object
            ace_rearm_defaultSupply,    // Used for ACE3 rearm, values >0 mean this vehicle can reammo other vehicles.
            ace_refuel_fuelCargo,       // Used for ACE3 refuel, values >0 mean this vehicle can refuel other vehicles.
            ace_repair_canRepair,       // Is this vehicle a repair vehicle?
            textSingular,               // Used for vehicle categorization
            crewCount,                  // Calculated from BIS_fnc_crewCount, all the 'crew' seats, usually includes FFV seats too.
            paxCount,                   // Calculated from BIS_fnc_crewCount, all of the 'cargo' (passenger) seats.
            maxmiumLoad,                // Gets the maxmimunLoad (vanilla vehicle inventory space) for a vehicle.
            transportAmmo,              // Vanilla ARMA3 area of effect reammo. BWI Modpack intentionally sets this to 0 where applicable.
            transportFuel,              // Vanilla ARMA3 area of effect refuel. BWI Modpack intentionally sets this to 0 where applicable.
            turrets,                    // Array of WEAPONS and MAGAZINES for TURRETS e.g. [[TankGun, CoaxialGun, 120mm_5rnd, 762mm_100rnd, 762mm_100rnd], [HullGun, 762mm_100rnd]]
            weaponsAndMagazines,        // Array of WEAPONS and MAGAZINES for NON-TURRETS e.g. [FIR_MasterArm, GAU8, 30mm_1000rnd]
            slingLoadCargoMemoryPoints, // Number of slingpoints vehicle has.
            canFloat,                   // Vehicle floats on water e.g. boats, amphibious land vehicles
            isBackpack                  // Is this vehicle a backpack that the player can wear?

        }

        /// <summary>
        /// A dictonary listing of every keyword to extract from the motorpool,
        /// and a regular expression used to extract that keyword.
        /// </summary>
        private static readonly Dictionary<string, Regex> rxKeywords = new Dictionary<string, Regex>()
        {
            { "class",              new Regex(@"(?:class\s+)(.*?)(?:\:)",                   RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "classname",          new Regex(@"(?:classname\s*=\s*.)(\w+)(?:.;)",          RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "name",               new Regex(@"(?<!class)(?:name\s*=\s*.)(.*?)(?:.;)",     RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "level",              new Regex(@"(?:level\s*=\s*)(.*?)(?:;)",                RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "cost",               new Regex(@"(?:cost\s*=\s*)(\d+)(?:;)",                 RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "pylonLoadout",       new Regex(@"(?:pylonLoadout\[\]\s*=\s{)(.*?)(?:};)",    RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline) },
            { "pylonTurrets",       new Regex(@"(?:pylonTurrets\[\]\s*=\s*{)(.*?)(?:};)",   RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "isOutpost",          new Regex(@"(?:isOutpost\s*=\s*)(\d+)(?:;)",            RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "textureData[]",      new Regex(@"(?:textureData\[\]\s*=\s*{)(.*?)(?:};)",    RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "animationData[]",    new Regex(@"(?:animationData\[\]\s*=\s*{)(.*?)(?:};)",  RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "crew",               new Regex(@"(?:crew\s*=\s*)(\d+)(?:;)",                 RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "passengers",         new Regex(@"(?:passengers\s*=\s*)(\d+)(?:;)",           RegexOptions.Compiled | RegexOptions.IgnoreCase) }
        };

        public static Dictionary<string, Vehicle> InitializeLookupFromDataset(string filePath)
        {
            var dictionary = new Dictionary<string, Vehicle>();
            if (!System.IO.File.Exists(filePath))
            {
                System.Console.WriteLine("File {0} not found", filePath);
                return null;
            }
            else
            {

                MotorpoolVehicle.Lookup.Clear();
                Vehicle.VehicleDictionary.Clear();

                string[] externalDatasetLines = System.IO.File.ReadAllLines(filePath);
                Console.Write("{0,-20} [", "CfgVehicles");

                int index = 0;
                foreach (string line in externalDatasetLines)
                {
                    if (line == string.Empty) { continue; }
                    var split = line.Split(';');

                    // Use constants because this gets really hard to reorganize later without them.
                    string className = split[(int)Vehicles.className].Trim();
                    var inhertiance = split[(int)Vehicles.inheritance] != string.Empty ? split[(int)Vehicles.inheritance].Split(new string[] { "[", ",", "]" }, StringSplitOptions.RemoveEmptyEntries) : null;
                    string displayName = split[(int)Vehicles.displayName].Trim();
                    bool aceCargoHasCargo = !split[(int)Vehicles.ace_cargo_hasCargo].Equals("0");
                    float aceCargoSpace = float.Parse(split[(int)Vehicles.ace_cargo_space], NumberStyles.Float, Program.MPTCulture);
                    float aceCargoSize = float.Parse(split[(int)Vehicles.ace_cargo_size], NumberStyles.Float, Program.MPTCulture);
                    int aceRearmDefaultSupply = int.Parse(split[(int)Vehicles.ace_rearm_defaultSupply]);
                    int aceRefuelCargo = int.Parse(split[(int)Vehicles.ace_refuel_fuelCargo]);
                    bool aceCanRepair = !split[(int)Vehicles.ace_repair_canRepair].Equals("0");
                    ushort category = Constants.ARMAVehicleCategory[split[(int)Vehicles.textSingular]];
                    int crewCount = int.Parse(split[(int)Vehicles.crewCount]);
                    int paxCount = int.Parse(split[(int)Vehicles.paxCount]);
                    float maxmiumLoad = float.Parse(split[(int)Vehicles.maxmiumLoad], NumberStyles.Float, Program.MPTCulture);
                    float transportAmmo = float.Parse(split[(int)Vehicles.transportAmmo], NumberStyles.Float, Program.MPTCulture);
                    float transportFuel = float.Parse(split[(int)Vehicles.transportFuel], NumberStyles.Float, Program.MPTCulture);
                    var turrets = split[(int)Vehicles.turrets] != string.Empty ? split[(int)Vehicles.turrets] : null;
                    var weaponsAndMagazines = split[(int)Vehicles.weaponsAndMagazines] != string.Empty ? split[(int)Vehicles.weaponsAndMagazines].Split(new string[] { "[", ",", "]" }, StringSplitOptions.RemoveEmptyEntries) : null;
                    string slingLoadCargoMemoryPoints = split[(int)Vehicles.slingLoadCargoMemoryPoints].Trim();
                    int canFloat = int.Parse(split[(int)Vehicles.canFloat]);
                    int isBackpack = int.Parse(split[(int)Vehicles.isBackpack]);

                    if (turrets.Equals("[]")) { turrets = null; }
                    if (weaponsAndMagazines.Length == 0) { weaponsAndMagazines = null; }

                    dictionary.Add(className.ToLowerInvariant(), new Vehicle(
                        className, inhertiance, displayName,
                        aceCargoHasCargo, aceCargoSpace, aceCargoSize, aceRearmDefaultSupply, aceRefuelCargo, aceCanRepair,
                        category, crewCount, paxCount, maxmiumLoad, transportAmmo, transportFuel,
                        turrets, weaponsAndMagazines, slingLoadCargoMemoryPoints, canFloat, isBackpack));

                    //VehiclesLookup.Add(new Vehicle());

                    if (index % (Math.Max(externalDatasetLines.Length, 20) / 20) == 0)
                    {
                        Console.Write("|");
                    }
                    index++;
                }
                Console.WriteLine("] {0}/{1}", dictionary.Count, dictionary.Count);


                foreach(var vehicle in dictionary)
                {
                    Vehicle.VehicleDictionary.Add(vehicle.Key, vehicle.Value);
                }

                return dictionary;
            }
        }

        /// <summary>
        /// Pulls every motorpool entry, uses helper functions to get properties of motorpool entries.
        /// </summary>
        /// <param name="text">Raw text from the vehicle.hpp file.</param>
        /// <param name="keyword">Starting keyword to look for. Defaults to "class"</param>
        /// <returns></returns>
        public static void InitializeMotorpool(string filePath)
        {
            var text = File.ReadAllText(filePath);
            List<string> entries = GetClass(text);

            // Now we've seperated each motorpool entry into a List of entries.
            // class A10A_CAS90: Vehicle { ... };
            // class A10C_CAS00: Vehicle { ... };
            // etc.
            foreach (string motorpoolEntry in entries)
            {
                // Used for the Vehicle() constructor at the end.
                string[] capturedVehicle = new string[rxKeywords.Count];
                int index = 0;

                foreach (KeyValuePair<string, Regex> rx in rxKeywords)
                {
                    // The BWI Classname is 'terminated' by a colon instead of a semi-colon.
                    if (index == 0)
                        capturedVehicle[index] = CaptureProperty(motorpoolEntry, rx.Key, ':', true);
                    else
                        capturedVehicle[index] = CaptureProperty(motorpoolEntry, rx.Key, ';');

                    if (capturedVehicle[index] != null)
                    {
                        var match = rxKeywords[rx.Key].Match(capturedVehicle[index]);
                        capturedVehicle[index] = match.Groups[1].Value;
                    }
                    index++;
                }

                // Rewrite

                string bwiClass = capturedVehicle[0].Trim();
                string className = capturedVehicle[1].Trim();
                string overrideName = capturedVehicle[2].Trim();

                // awful hack, have to fix Factory CaptureProperty later.
                if(className.Equals(overrideName, StringComparison.OrdinalIgnoreCase))
                {
                    overrideName = null;
                }

                ushort escalation = Constants.EscalationLookup[capturedVehicle[3]];
                int cost = int.Parse(capturedVehicle[4]);
                string pylonLoadout = capturedVehicle[5] != null ? Regex.Replace(capturedVehicle[5], "[\"\t\r\n]", string.Empty).Trim() : null;
                string pylonTurrets = capturedVehicle[6];
                bool isOutpost = capturedVehicle[7] != null ? !capturedVehicle[7].Equals("0") : false;
                string textureData = capturedVehicle[8];
                string animationData = capturedVehicle[9];
                int crewOverride = capturedVehicle[10] != null ? int.Parse(capturedVehicle[10]) : -1;
                int passengerOverride = capturedVehicle[11] != null ? int.Parse(capturedVehicle[11]) : -1;

                Vehicle baseVehicle = Vehicle.Lookup(className);

                var motorpoolVehicle = new MotorpoolVehicle(baseVehicle, bwiClass, overrideName, crewOverride, passengerOverride, cost, escalation, pylonLoadout, pylonTurrets, isOutpost)
                {
                    IsCarrierAirWing = filePath.Contains("armed_cvw.hpp") // todo lazy way of getting carrier aircraft
                };

                // A MotorpoolVehicle is just used to describe a vehicle in the BWI Motorpool.
                // Vehicles are purely based on ARMA configs. A Motorpool Vehicle has added attributes like "Cost" and "Escalation".
                MotorpoolVehicle.Lookup.Add(bwiClass.ToLowerInvariant(), motorpoolVehicle);
            }
            return;
        }
    }
}
