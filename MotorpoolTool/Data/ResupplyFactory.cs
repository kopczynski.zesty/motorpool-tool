﻿using MotorpoolTool.ARMA;
using MotorpoolTool.ARMA.Weapons;
using MotorpoolTool.BWI.Supplies;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MotorpoolTool.Data
{
    public class ResupplyFactory : Factory
    {
        private enum Resupplies
        {
            className,
            hiddenSelectionsTextures,
            transportMagazines,
            transportWeapons,
            transportItems
        }

        /// <summary>
        /// A dictonary listing of every keyword to extract from the motorpool,
        /// and a regular expression used to extract that keyword.
        /// </summary>
        private static readonly Dictionary<string, Regex> rxKeywords = new Dictionary<string, Regex>()
        {
            { "class",                      new Regex(@"(?:class\s+)(.*?)(?:\:)",                       RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "hiddenSelectionsTextures",   new Regex(@"(?:hiddenSelectionsTextures.*?{)(.*?)(?:};)",   RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline) },
            { "TransportMagazines",         new Regex(@"(?:TransportMagazines\s*{)(.*?)(?:};)",         RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline) },
            { "TransportWeapons",           new Regex(@"(?:TransportWeapons\s*{)(.*?)(?:};)",           RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline) },
            { "TransportItems",             new Regex(@"(?:TransportItems\s*{)(.*?)(?:};)",             RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline) }
        };

        private static readonly Regex rxMacro = new Regex(@"(?:MACRO_\w+\()(.*?)(?:\))", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public static bool InitializeLookupFromDirectory(string path)
        {
            if(!Directory.Exists(path))
            {
                System.Console.WriteLine($"Directory {path} not found");
                return false;
            }

            var allFiles = Directory.GetFiles(path, "*.hpp", SearchOption.AllDirectories);
            if (allFiles.Length == 0)
            {
                System.Console.WriteLine($"Directory {path} has no resuppply entries");
                return false;
            }

            foreach (string filePath in allFiles)
            {
                var data = File.ReadAllText(filePath);
                var entries = GetClass(data);

                foreach(var resupplyEntry in entries)
                {
                    string[] capturedResupply = new string[rxKeywords.Count];
                    int index = 0;

                    foreach(var rx in rxKeywords)
                    {
                        if (index == 0)
                            capturedResupply[index] = CaptureProperty(resupplyEntry, rx.Key, ':', true);
                        else
                            capturedResupply[index] = CaptureProperty(resupplyEntry, rx.Key, ';');

                        if(capturedResupply[index] != null)
                        {
                            capturedResupply[index] = rxKeywords[rx.Key].Match(capturedResupply[index]).Groups[1].Value;
                        }
                        index++;
                    }

                    string className = capturedResupply[(int)Resupplies.className].Trim();
                    Vehicle supply = Vehicle.Lookup(className);

                    string transportMagazines = !string.IsNullOrEmpty(capturedResupply[(int)Resupplies.transportMagazines]) ? Regex.Replace(capturedResupply[(int)Resupplies.transportMagazines], "[\"\t\r\n]", string.Empty).Trim() : null;
                    string transportWeapons = !string.IsNullOrEmpty(capturedResupply[(int)Resupplies.transportWeapons]) ? Regex.Replace(capturedResupply[(int)Resupplies.transportWeapons], "[\"\t\r\n]", string.Empty).Trim() : null;
                    string transportItems = !string.IsNullOrEmpty(capturedResupply[(int)Resupplies.transportItems]) ? Regex.Replace(capturedResupply[(int)Resupplies.transportItems], "[\"\t\r\n]", string.Empty).Trim() : null;

                    AddItemsToSupplyBoxHelper(className, supply, transportMagazines);
                    AddItemsToSupplyBoxHelper(className, supply, transportMagazines);
                    AddItemsToSupplyBoxHelper(className, supply, transportMagazines);

                }
            }

            return true;
        }

        private static void AddItemsToSupplyBoxHelper(string className, Vehicle supply, string transportX)
        {
            if (!string.IsNullOrEmpty(transportX))
            {
                foreach (string entry in transportX.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    string matchedEntry = rxMacro.Match(entry).Groups[1].Value;
                    var quantityAndClass = matchedEntry.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);

                    // 0 is quanity
                    // 1 is is className
                    if (supply == null)
                        Console.WriteLine($"Skipping vehicle {className} (className not found)");
                    else
                        supply.AddToInventory(quantityAndClass[1], int.Parse(quantityAndClass[0]));
                }
            }
        }
    }
}
