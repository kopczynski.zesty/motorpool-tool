﻿using MotorpoolTool.BWI;
using MotorpoolTool.BWI.Roles;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace MotorpoolTool.Data
{
    public class ElementFactory : Factory
    {
        private static readonly Dictionary<string, Regex> rxKeywords = new Dictionary<string, Regex>()
        {
            { "class",              new Regex(@"(?:class\s+)(.*?)(?:\:)",               RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "name",               new Regex(@"(?:name\s*=\s*)(.*?)(?:;)",             RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "sides",              new Regex(@"(?:sides\[\]\s*=\s*{)(.*?)(?:};)",      RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "callsigns",          new Regex(@"(?:callsigns\[\]\s*=\s*{)(.*?)(?:};)",  RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "#include",           new Regex(@"(?:\#include\s*<)(roles\w*\.hpp)(?:>)", RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "isWeaponsElement",   new Regex(@"(?:isWeaponsElement\s*=\s*)(\d+)(?:;)", RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "isReconElement",     new Regex(@"(?:isReconElement\s*=\s*)(\d+)(?:;)",   RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { "isSpecialElement",   new Regex(@"(?:isSpecialElement\s*=\s*)(\d+)(?:;)", RegexOptions.Compiled | RegexOptions.IgnoreCase) }
        };

        public static List<Element> StringToList(string text, string path = null)
        {
            List<Element> elements = new List<Element>();
            var entries = GetClass(text);

            foreach (string element in entries)
            {
                string[] capturedElement = new string[rxKeywords.Count];
                int index = 0;

                foreach (KeyValuePair<string, Regex> rx in rxKeywords)
                {
                    if (index == 0)
                        capturedElement[index] = CaptureProperty(element, rx.Key, ':', true);
                    else
                        capturedElement[index] = CaptureProperty(element, rx.Key, ';', true);

                    if (capturedElement[index] != null)
                    {
                        MatchCollection matches = rxKeywords[rx.Key].Matches(capturedElement[index]);
                        capturedElement[index] = matches[0].Groups[1].Value;
                    }
                    index++;
                }

                string className = capturedElement[0];
                string name = capturedElement[1].Replace("\"", string.Empty);
                string[] sides = capturedElement[2].Split(',');
                string[] callsigns = capturedElement[3].Replace("\"", string.Empty).Split(',');
                for (int i = 0; i < callsigns.Length; i++)
                {
                    callsigns[i] = callsigns[i].Trim();
                }

                var rolesDirectory = Directory.GetFiles(path, capturedElement[4]).First();
                var roleContents = File.ReadAllText(rolesDirectory);
                var roles = RoleFactory.StringToList(roleContents, rolesDirectory);

                bool isWeapons = capturedElement[5] != null ? !capturedElement.Equals("0") : false;
                bool isRecon = capturedElement[6] != null ? !capturedElement.Equals("0") : false;
                bool isSpecial = capturedElement[7] != null ? !capturedElement.Equals("0") : false;

                elements.Add(new Element(className, name, sides, callsigns, roles, isWeapons, isRecon, isSpecial));
            }

            return elements;
        }
    }
}
