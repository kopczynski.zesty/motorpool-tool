﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MotorpoolTool.ARMA.Constants;

namespace MotorpoolTool.BWI.Accessories
{
    public struct AllowedAccessory
    {
        public AllowedAccessory(AccessoryLevel topRail, AccessoryLevel sideRail, AccessoryLevel bottomRail, AccessoryLevel barrel)
        {
            this.TopRail = topRail;
            this.SideRail = sideRail;
            this.BottomRail = bottomRail;
            this.Barrel = barrel;
        }
        public AccessoryLevel TopRail;
        public AccessoryLevel SideRail;
        public AccessoryLevel BottomRail;
        public AccessoryLevel Barrel;
    }
}
