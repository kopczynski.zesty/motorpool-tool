﻿using MotorpoolTool.ARMA;
using MotorpoolTool.ARMA.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using static MotorpoolTool.ARMA.Constants;

namespace MotorpoolTool.BWI.Accessories
{
    public class Accessory : ARMAEntity
    {
        public Accessory(string className, int year, string[] compatibleClasses)
        {
            this.ClassName = className;
            this.Year = year;
            this.CompatibleClasses = compatibleClasses;
        }
        public int Year { get; private set; }
        public string[] CompatibleClasses { get; private set; }

        public static List<AccessoryGroup> GetCompatibleAccessories(List<AccessoryGroup> list, AccessorySlot accessorySlot, AccessoryLevel accessoryLevel, int year, ARMAEntity armaObject)
        {
            if (armaObject == null) { return new List<AccessoryGroup>(); }

            //var groups = list.Where(grp =>
            //    //grp.Slot == accessorySlot
            //    //&& grp.Level <= accessoryLevel
            //    //&& !grp.Accessories.Any(x => x.Year >= year)
            //    grp.Accessories.All(
            //        x => 
            //        x.CompatibleClasses != null ? 
            //        !x.CompatibleClasses.Contains(armaObject.ClassName, StringComparer.InvariantCultureIgnoreCase) : false)).ToList();

            // TODO Code analysis. This could probably be a LINQ expression and I'm not sure how much performance this eats.
            var newGroup = new List<AccessoryGroup>();
            foreach (var group in list)
            {
                if (group.Slot == accessorySlot && group.Level <= accessoryLevel)
                {
                    var newList = new List<Accessory>();
                    foreach (var accessory in group.Accessories)
                    {
                        if (accessory.Year <= year
                            && accessory.CompatibleClasses != null
                            && accessory.CompatibleClasses.Contains(armaObject.ClassName, StringComparer.InvariantCultureIgnoreCase))
                        {
                            newList.Add(accessory);
                        }
                    }
                    if (newList.Count > 0)
                    {
                        newGroup.Add(new AccessoryGroup(group.ClassName, group.DisplayName, group.Slot, group.Level, newList));
                    }

                }
            }

            return newGroup;
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(this.DisplayName))
            {
                _ = Weapon.WeaponDictionary.TryGetValue(this.ClassName.ToLowerInvariant(), out Weapon weapon);
                this.DisplayName = weapon.DisplayName;
            }
            return base.ToString();
        }
    }
}
