﻿using MotorpoolTool.ARMA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MotorpoolTool.ARMA.Constants;

namespace MotorpoolTool.BWI.Accessories
{
    public class AccessoryGroup : ARMAEntity
    {
        public AccessoryGroup(string className, string displayName, AccessorySlot slot, AccessoryLevel level,  List<Accessory> accessories)
        {
            this.ClassName = className;
            this.DisplayName = displayName;
            this.Slot = slot;
            this.Level = level;
            this.Accessories = accessories;
        }

        public AccessorySlot Slot { get; private set; }
        public AccessoryLevel Level { get; private set; }
        public List<Accessory> Accessories { get; private set; }
    }
}
