﻿using MotorpoolTool.ARMA;
using MotorpoolTool.ARMA.Weapons;
using MotorpoolTool.BWI.Accessories;
using System;
using System.Collections.Generic;

namespace MotorpoolTool.BWI.Roles
{
    public class Role : Hideable, ICloneable
    {

        public static Dictionary<string, Role> RoleDictionary = new Dictionary<string, Role>();
        public static Dictionary<string, Role> GetCopyOfRoles()
        {
            Dictionary<string, Role> copy = new Dictionary<string, Role>();

            foreach (var kv in RoleDictionary)
            {
                copy.Add(kv.Key, (Role)kv.Value.Clone());
            }

            return copy;
        }

        public static Role Lookup(string roleCode)
        {
            return RoleDictionary[roleCode.ToLowerInvariant()];
        }

        private Element m_parent = null;

        /// <summary>
        /// Sets up a parent for this element. This is super permanent and can only be done once for simplicity.
        /// </summary>
        public Element Parent
        {
            get
            {
                return m_parent;
            }
            set
            {
                if (m_parent != null)
                {
                    throw new ArgumentException("Tried to change parents of " + value.ClassName + "_" + this.ClassName);
                }
                else
                {
                    m_parent = value;
                    var key = string.Format("{0}_{1}", value.ClassName.ToLowerInvariant(), this.ClassName.ToLowerInvariant());
                    if (!Role.RoleDictionary.ContainsKey(key))
                    {
                        Role.RoleDictionary.Add(key, this);
                    }
                }
            }
        }

        public string Callsign { get; set; }

        public string ClassName { get; private set; }
        public string Code
        {
            get { return string.Format("{0}_{1}", this.Parent.ClassName, this.ClassName); }
        }
        public string Name { get; private set; }
        public AllowedAccessory AllowedAccessories { get; private set; }

        // ACE
        public int IsMedic { get; private set; }
        public int IsEngineer { get; private set; }
        public int IsEOD { get; private set; }
        public float GForceCoef { get; private set; }
        public float StaminiaFactor { get; private set; }

        /// <summary>
        /// Primary weapon. Usually a rifle. Set this using SetWeapon().
        /// </summary>
        public Weapon Primary { get; private set; }

        /// <summary>
        /// Secondary weapon. Usually a pistol. Set this using SetWeapon()
        /// </summary>
        public Weapon Secondary { get; private set; }

        /// <summary>
        /// Tertiary weapon. Usually a rocket launcher. Set this using SetWeapon()
        /// </summary>
        public Weapon Tertiary { get; private set; }

        public Weapon Binocular { get; private set; }

        // Gear that this role is wearing.
        // Helmets / Vests / Uniforms are part of CfgWeapons
        // Backpacks are part of CfgVehicles
        public Weapon Headgear { get; private set; }
        public Weapon Uniform { get; private set; }
        public Weapon Vest { get; private set; }
        public Vehicle Backpack { get; private set; }

        // Abstractions for map / compass / gps / watch, no need to be exact.
        public bool HasMap { get; set; }
        public bool HasCompass { get; set; }
        public bool HasGPS { get; set; }
        public bool HasUAVTerminal { get; set; }

        public bool HasBFT
        {
            get
            {
                bool uniform = false;
                bool vest = false;
                bool backpack = false;

                if (this.Uniform != null)
                    uniform = this.Uniform.SearchInventory("ACE_microDAGR", "ACE_DAGR");

                if (this.Vest != null)
                    vest = this.Vest.SearchInventory("ACE_microDAGR", "ACE_DAGR");

                if (this.Backpack != null)
                    backpack = this.Backpack.SearchInventory("ACE_microDAGR", "ACE_DAGR");

                return uniform || vest || backpack;
            }
        }

        public int Wrist { get; set; }

        // ACRE
        public bool Interpreter { get; private set; }

        public Role(Role role)
        {
            this.ClassName = role.ClassName;
            this.Name = role.Name;
            this.Callsign = role.Callsign;
            this.AllowedAccessories = role.AllowedAccessories;
            this.IsMedic = role.IsMedic;
            this.IsEngineer = role.IsEngineer;
            this.IsEOD = role.IsEOD;
            this.GForceCoef = role.GForceCoef;
            this.StaminiaFactor = role.StaminiaFactor;
            this.Interpreter = role.Interpreter;

            this.HasMap = role.HasMap;
            this.HasCompass = role.HasCompass;
            this.HasGPS = role.HasGPS;
            this.HasUAVTerminal = role.HasUAVTerminal;

            SetEquipment(role.Headgear);
            SetEquipment(role.Uniform);
            SetEquipment(role.Vest);
            SetEquipment(role.Backpack);

            SetWeapon(role.Primary);
            SetWeapon(role.Secondary);
            SetWeapon(role.Tertiary);
            SetWeapon(role.Binocular);
            this.Parent = role.Parent;
        }

        public Role(
            string className,
            string name,
            string callsign,
            AllowedAccessory allowedAccesories,
            int isMedic = 0,
            int isEngineer = 0,
            int isEOD = 0,
            float gForceCoef = 1.0f,
            float staminaFactor = 1.0f,
            bool interpreter = false)
        {
            this.ClassName = className;
            this.Name = name;
            this.Callsign = callsign;
            this.AllowedAccessories = allowedAccesories;
            this.IsMedic = isMedic;
            this.IsEngineer = isEngineer;
            this.IsEOD = isEOD;
            if (gForceCoef == 0f)
                gForceCoef = 1.0f;
            this.GForceCoef = gForceCoef;
            if (staminaFactor == 0f)
                staminaFactor = 1.0f;
            this.StaminiaFactor = staminaFactor;
            this.Interpreter = interpreter;
        }

        public bool SetEquipment(Object equipment)
        {
            if (equipment == null)
                return true; // TODO re-eval
            else if (equipment is Weapon)
            {
                var cfgWeapon = equipment as Weapon;

                if (cfgWeapon.ItemType[0].Equals("Weapon"))
                    return this.SetWeapon(cfgWeapon);

                switch (cfgWeapon.ItemType[1])
                {
                    case "Headgear":
                        this.Headgear = (Weapon)cfgWeapon.Clone();
                        return true;
                    case "Vest":
                        this.Vest = (Weapon)cfgWeapon.Clone();
                        return true;
                    case "Uniform":
                        this.Uniform = (Weapon)cfgWeapon.Clone();
                        return true;
                }
            }
            else if (equipment is Vehicle)
            {
                var cfgVehicle = equipment as Vehicle;

                if (cfgVehicle.IsBackpack)
                {
                    this.Backpack = (Vehicle)cfgVehicle.Clone();
                    return true;
                }
                else
                    throw new ArgumentException();
            }
            else if (equipment is List<string[]>)
            {
                // Items to be added to uniform / vest / backpack
                foreach (var item in equipment as List<string[]>)
                {
                    var magazine = Magazine.Lookup(item[0]);
                    int count = int.Parse(item[1]);

                    switch (item[2])
                    {
                        case "bwi_armory_fnc_addToUniform":
                            this.Uniform.AddToInventory(magazine, count);
                            break;
                        case "bwi_armory_fnc_addToVest":
                            this.Vest.AddToInventory(magazine, count);
                            break;
                        case "bwi_armory_fnc_addToBackpack":
                            this.Backpack.AddToInventory(magazine, count);
                            break;
                    }
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Handles setting Primary / Secondary / Tertiary weapons.
        /// </summary>
        /// <param name="weapon">Weapon to be assigned. The ItemType of this weapon will be compared.</param>
        /// <returns>True if a weapon was set, false if weapon was not set.</returns>
        public bool SetWeapon(Weapon weapon)
        {
            if (weapon == null)
                return false;
            else
                weapon = (Weapon)weapon.Clone();

            if (weapon.ItemType[0].Equals("Weapon"))
            {
                switch (weapon.ItemType[1])
                {
                    case "Rifle":
                    case "AssaultRifle":
                    case "SubmachineGun":
                    case "Shotgun":
                    case "SniperRifle":
                    case "MachineGun":
                        this.Primary = weapon;
                        return true;
                    case "Handgun":
                        this.Secondary = weapon;
                        return true;
                    case "Launcher":
                    case "MissileLauncher":
                    case "RocketLauncher":
                        this.Tertiary = weapon;
                        return true;
                    default:
                        return false;
                }
            }
            else if (weapon.ItemType[1].Equals("Binocular") || weapon.ItemType[1].Equals("LaserDesignator"))
            {
                this.Binocular = weapon;
            }

            return false;
        }

        public object Clone()
        {
            return new Role(this);
        }

        public override string ToString()
        {
            return this.Name;
        }

    }
}
