﻿using MotorpoolTool.ARMA;
using System.Collections.Generic;
using System.Linq;
using static MotorpoolTool.ARMA.Constants;

namespace MotorpoolTool.BWI.Roles
{
    public class Element : Hideable
    {
        public string ClassName { get; private set; }
        public string Name { get; private set; }
        public List<Side> Sides { get; private set; }
        public List<string> Callsigns { get; private set; }
        public List<Role> Roles { get; private set; }
        public bool IsWeaponsElement { get; private set; }
        public bool IsReconElement { get; private set; }
        public bool IsSpecialElement { get; private set; }


        public Element(Element element)
        {
            this.ClassName = element.ClassName;
            this.Name = element.Name;
            this.Sides = element.Sides;
            this.Callsigns = element.Callsigns;
            this.Roles = element.Roles;
            this.IsWeaponsElement = element.IsWeaponsElement;
            this.IsReconElement = element.IsReconElement;
            this.IsSpecialElement = element.IsSpecialElement;
        }

        public Element(string className, string name, string[] sides, string[] callsigns, List<Role> roles, bool isWeapons = false, bool isRecon = false, bool isSpecial = false)
        {
            this.ClassName = className;
            this.Name = name;
            this.Sides = new List<Side>();
            foreach (string side in sides)
            {
                this.Sides.Add(Constants.StringToSide(side.Trim()));
            }

            this.Callsigns = callsigns.ToList();

            this.Roles = roles;
            foreach (Role role in this.Roles)
            {
                role.Parent = this;
            }

            this.IsWeaponsElement = isWeapons;
            this.IsReconElement = isRecon;
            this.IsSpecialElement = isSpecial;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
