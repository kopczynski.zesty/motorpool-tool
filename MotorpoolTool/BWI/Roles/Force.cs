﻿using System.Collections.Generic;
using static MotorpoolTool.ARMA.Constants;

namespace MotorpoolTool.BWI.Roles
{
    /// <summary>
    /// I couldn't come up with another way to say 'Side'.
    /// </summary>
    public class Force
    {
        public Side Side { get; private set; }

        public List<FactionGroup> FactionGroups { get; set; }

        public Force(Side side)
        {
            this.Side = side;
            this.FactionGroups = new List<FactionGroup>();
        }

        public override string ToString()
        {
            return this.Side.ToString();
        }
    }
}
