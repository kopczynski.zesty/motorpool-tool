﻿using MotorpoolTool.ARMA;
using System.Collections.Generic;
using static MotorpoolTool.ARMA.Constants;

namespace MotorpoolTool.BWI
{
    public class FactionGroup
    {
        public FactionGroup(string bwiName, string name, string side, string path = null)
        {
            this.BWIName = bwiName;
            this.Name = name;
            this.Side = Constants.StringToSide(side);
            this.Directory = path;
            this.SubFactions = new List<Faction>();
            this.Years = new List<ushort>();
        }
        public List<Faction> SubFactions { get; set; }

        public string BWIName { get; set; }

        /// <summary>
        /// Name of the faction.
        /// <code>Ursus Corportation</code>
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// What side this faction is on.
        /// <code>OPFOR</code>
        /// </summary>
        public Side Side { get; set; }

        public List<ushort> Years { get; set; }

        public string Directory { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
