﻿using MotorpoolTool.ARMA;
using MotorpoolTool.ARMA.Weapons;
using MotorpoolTool.BWI.Roles;
using MotorpoolTool.BWI.Supplies;
using MotorpoolTool.Data;
using MotorpoolTool.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using static MotorpoolTool.ARMA.Constants;

namespace MotorpoolTool.BWI
{
    public class Faction
    {
        private enum Equipment { UNIFORM, WEAPON, GRENADE, AMMO }

        public FactionGroup Group { get; private set; }

        public Faction(Faction faction)
        {
            this.Group = faction.Group;
            this.BWIName = faction.BWIName;
            this.Name = faction.Name;
            this.Year = faction.Year;
            this.Type = faction.Type;
            this.MotorpoolRegulars = new List<MotorpoolVehicle>(faction.MotorpoolRegulars);
            this.MotorpoolRecon = new List<MotorpoolVehicle>(faction.MotorpoolRecon);
            this.MotorpoolSpecialForces = new List<MotorpoolVehicle>(faction.MotorpoolSpecialForces);

            this.Roles = faction.Roles;
            this.ResupplyRegular = faction.ResupplyRegular;
            this.ResupplyWeapons = faction.ResupplyWeapons;
            this.ResupplyRecon = faction.ResupplyRecon;
            this.ResupplySpecial = faction.ResupplySpecial;
            this.HiddenElements = faction.HiddenElements;
            this.HiddenRoles = faction.HiddenRoles;
        }

        // Idea: Factions should get a list of roles.

        public Faction(FactionGroup group, string bwiName, string name, ushort year, string type,
            string uniformScript, string weaponScript, string grenadeScript, string ammoScript,
            string[] resupplyRegular, string[] resupplyWeapons, string[] resupplyRecon, string[] resupplySpecial,
            string[] hiddenElements, string[] hiddenRoles)
        {
            this.Group = group;
            this.BWIName = bwiName;
            this.Name = name;
            this.Year = year;
            this.Type = type;
            this.MotorpoolRegulars = new List<MotorpoolVehicle>();
            this.MotorpoolRecon = new List<MotorpoolVehicle>();
            this.MotorpoolSpecialForces = new List<MotorpoolVehicle>();

            // Get a deep copy of roles.
            this.Roles = Role.GetCopyOfRoles();
            this.HiddenElements = hiddenElements != null ? hiddenElements.ToList() : new List<string>();
            this.HiddenRoles = hiddenRoles != null ? hiddenRoles.ToList() : new List<string>();

            this.ParseEquipment(Equipment.UNIFORM, uniformScript);
            if (uniformScript != null)
                this.AddGear();

            this.ParseEquipment(Equipment.WEAPON, weaponScript);
            this.ParseEquipment(Equipment.GRENADE, grenadeScript);
            this.ParseEquipment(Equipment.AMMO, ammoScript); // TODO: Mujahideen loadouts have if checks in the ammo file.

            this.ResupplyRegular = resupplyRegular;
            this.ResupplyWeapons = resupplyWeapons;
            this.ResupplyRecon = resupplyRecon;
            this.ResupplySpecial = resupplySpecial;
        }

        private void ParseEquipment(Equipment equipmentType, string path)
        {
            if (!File.Exists(path))
            {
                return;
            }

            // Read file.
            var data = File.ReadAllText(path);

            // Strip comments to prevent parsing headaches
            data = Helper.StripComments(data);

            // Get switch statements in the .sqf file.
            var switchStatements = Factory.CaptureProperties(data, "switch", ';');

            foreach (string switchBlock in switchStatements)
            {
                // Get cases all in the format of { "mmg_gun", "{ _unit addWeapon "rhs_weap_m240b"; }" }
                string keywordCase = "case ";
                Dictionary<string, string> pairings = GetPairsFromSwitch(switchBlock, keywordCase);


                string fallThrough = null;
                Dictionary<string, string> gearAssignments = new Dictionary<string, string>(pairings);

                // Fill in the 'null' cases. This is easier if the list is reversed.
                foreach (var kv in pairings.Reverse())
                {
                    if (string.IsNullOrEmpty(kv.Value))
                    {
                        gearAssignments[kv.Key] = fallThrough;
                    }
                    else
                    {
                        fallThrough = kv.Value;
                    }
                }

                // Add all missing roles to pairings. If they are not paired with a case, then they are paired with the default case.
                foreach (var role in this.Roles)
                {
                    if (!gearAssignments.ContainsKey(role.Key))
                    {
                        gearAssignments.Add(role.Key, gearAssignments["default"]);
                    }
                }

                // Finally, we can start assigning equipment.
                foreach (var pair in gearAssignments)
                {
                    this.SetEquipment(equipmentType, pair.Key, pair.Value);
                }

            }
        }

        private void SetEquipment(Equipment equipmentType, string roleCode, string caseToParse)
        {
            // If no default case, skip.
            if (string.IsNullOrEmpty(caseToParse)) { return; }

            // Special cases
            if (roleCode.Equals("resistance") || roleCode.Equals("east") || roleCode.Equals("west"))
            {
                return;
            }

            object equipment = null;

            if (equipmentType == Equipment.WEAPON)
            {
                var weapon = HelperWeapon(caseToParse); // TODO handle random equipment

                if (weapon != null && weapon.Length > 0)
                    equipment = Weapon.Lookup(weapon.First());
            }
            else if (equipmentType == Equipment.UNIFORM)
            {
                // CfgWeapons: helmets uniforms
                // CfgVehicles: backpacks

                var helmet = HelperHelmet(caseToParse);
                var vest = HelperVest(caseToParse);
                var uniform = HelperUniform(caseToParse);
                var backpack = HelperBackpack(caseToParse);

                if (helmet.Length > 0)
                    equipment = Weapon.Lookup(helmet.First());

                if (vest.Length > 0)
                    equipment = Weapon.Lookup(vest.First());

                if (uniform.Length > 0)
                    equipment = Weapon.Lookup(uniform.First());

                if (backpack.Length > 0)
                    equipment = Vehicle.Lookup(backpack.First());
            }
            else if (equipmentType == Equipment.GRENADE)
            {
                var grenades = HelperAmmo(caseToParse);
                equipment = grenades;
            }
            else if (equipmentType == Equipment.AMMO)
            {
                var ammo = HelperAmmo(caseToParse);
                equipment = ammo;
            }

            if (!roleCode.Equals("default"))
            {
                if (!this.Roles[roleCode].SetEquipment(equipment))
                {
                    Console.WriteLine($"debug: {roleCode} failed to set {equipment}");
                }
            }
        }

        private string[] HelperWeapon(string s)
        {
            var specificEquipment = Regex.Match(s, @"addWeapon\s+.(\w+).").Groups[1].Value;
            var randomizedEquipment = Regex.Replace(
                Regex.Match(s, @"\[_unit,\s*(\[.*?\])\s*\]\s+call\s+(bwi_armory_fnc_addRandomWeapon.*?;)",
                RegexOptions.Singleline).Groups[1].Value,
                "[ \"\t\r\n]", string.Empty).Split(new string[] { "[", ",", "]" },
                StringSplitOptions.RemoveEmptyEntries); // Handles randomization

            if (!string.IsNullOrEmpty(specificEquipment))
                return new[] { specificEquipment };
            else
                return randomizedEquipment;
        }

        private string[] HelperHelmet(string s)
        {
            var specificEquipment = Regex.Match(s, @"addHeadgear\s+.(\w+).").Groups[1].Value;
            var randomizedEquipment = Regex.Replace(
                Regex.Match(s, @"\[_unit,\s*(\[.*?\])\s*\]\s+call\s+(bwi_armory_fnc_addRandomHeadgear.*?;)",
                RegexOptions.Singleline).Groups[1].Value,
                "[ \"\t\r\n]", string.Empty).Split(new string[] { "[", ",", "]" },
                StringSplitOptions.RemoveEmptyEntries); // Handles randomization

            if (!string.IsNullOrEmpty(specificEquipment))
                return new[] { specificEquipment };
            else
                return randomizedEquipment;
        }

        private string[] HelperVest(string s)
        {
            var specificEquipment = Regex.Match(s, @"addVest\s+.(\w+).").Groups[1].Value;
            var randomizedEquipment = Regex.Replace(
                Regex.Match(s, @"\[_unit,\s*(\[.*?\])\s*\]\s+call\s+(bwi_armory_fnc_addRandomVest.*?;)",
                RegexOptions.Singleline).Groups[1].Value,
                "[ \"\t\r\n]", string.Empty).Split(new string[] { "[", ",", "]" },
                StringSplitOptions.RemoveEmptyEntries); // Handles randomization

            if (!string.IsNullOrEmpty(specificEquipment))
                return new[] { specificEquipment };
            else
                return randomizedEquipment;
        }

        private string[] HelperBackpack(string s)
        {
            var specificEquipment = Regex.Match(s, @"addBackpack\s+.(\w+).").Groups[1].Value;
            var randomizedEquipment = Regex.Replace(
                Regex.Match(s, @"\[_unit,\s*(\[.*?\])\s*\]\s+call\s+(bwi_armory_fnc_addRandomBackpack.*?;)",
                RegexOptions.Singleline).Groups[1].Value,
                "[ \"\t\r\n]", string.Empty).Split(new string[] { "[", ",", "]" },
                StringSplitOptions.RemoveEmptyEntries); // Handles randomization

            if (!string.IsNullOrEmpty(specificEquipment))
                return new[] { specificEquipment };
            else
                return randomizedEquipment;
        }

        private string[] HelperUniform(string s)
        {
            var specificEquipment = Regex.Match(s, @"forceAddUniform\s+.(\w+).").Groups[1].Value;
            var randomizedEquipment = Regex.Replace(
                Regex.Match(s, @"\[_unit,\s*(\[.*?\])\s*\]\s+call\s+(bwi_armory_fnc_addRandomUniform.*?;)",
                RegexOptions.Singleline).Groups[1].Value,
                "[ \"\t\r\n]", string.Empty).Split(new string[] { "[", ",", "]" },
                StringSplitOptions.RemoveEmptyEntries); // Handles randomization

            if (!string.IsNullOrEmpty(specificEquipment))
                return new[] { specificEquipment };
            else
                return randomizedEquipment;
        }

        private List<string[]> HelperAmmo(string s)
        {
            List<string[]> grenadeList = new List<string[]>();
            foreach (string line in s.Split('\n'))
            {
                var grenades = Regex.Match(line, @"\[_unit,\s*.(\w+).,\s*(\d+)\s*\]\s+call\s+(bwi.*?);", RegexOptions.Singleline);
                if (grenades.Groups.Count > 1)
                {
                    grenadeList.Add(new[] { grenades.Groups[1].Value, grenades.Groups[2].Value, grenades.Groups[3].Value });
                }
            }

            return grenadeList;
        }

        /// <summary>
        /// Ported code from gear.sqf, as writing a parser for this file is about as much effort as writing it out by hand.
        /// </summary>
        private void AddGear()
        {
            // TODO Cleanup
            bool hasBFT = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase);
            bool hasIRStrobes = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase) && this.Year >= 1990;
            bool hasChemlights = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR", "MILTIA" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase) && this.Year >= 1990;
            bool hasIRGreandes = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase) && this.Year >= 1990;
            bool hasKestral = this.Year >= 2000;
            bool hasEODEquipment = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase);
            bool hasIEDs = new[] { "INSURGENTS" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase) && this.Year >= 1995;
            bool hasHuntIR = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase) && this.Year >= 2006;
            bool hasFlashbangs = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase);
            bool hasTripod = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase);
            bool hasVector = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase) && this.Year >= 2000;
            bool hasVectorNite = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase) && this.Year >= 2010;
            bool hasThermalBinoculars = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase) && this.Year >= 2000;
            bool hasLaserDesignator = new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type, StringComparer.OrdinalIgnoreCase) && this.Year >= 1990;

            foreach (var role in this.Roles)
            {
                // Add standard gear for all
                role.Value.HasMap = true;
                role.Value.HasCompass = true;

                role.Value.Uniform.AddToInventory("ACE_MapTools", 1);
                role.Value.Uniform.AddToInventory("ACE_EarPlugs", 1);
                role.Value.Uniform.AddToInventory("ACE_CableTie", 2);

                // Add watch accordingly
                if (this.Type.Equals("AIRBORNE") && this.Year >= 1990)
                {
                    role.Value.Wrist = 2;
                }
                else
                {
                    role.Value.Wrist = 1;
                }

                // Add GPS/BFT Devices
                if (hasBFT)
                {
                    switch (role.Key)
                    {
                        case "plt_ldr":
                        case "log_sgt":
                        case "med_ldr":
                        case "med_cpm":
                        case "eng_eod":
                        case "eng_dem":
                        case "eng_def":
                        case "eng_mec":
                        case "tac_fac":
                        case "tac_jtc":
                        case "sqd_ldr":
                        case "sqd_ldg":
                        case "lrc_ldr":
                        case "spf_ldr":
                        case "scu_ldr":
                        case "mmg_ldr":
                        case "mat_ldr":
                        case "hat_ldr":
                        case "aat_ldr":
                        case "how_ldr":
                        case "mot_ldr":
                        case "dat_ldr":
                        case "dmg_ldr":
                        case "dgg_ldr":
                        case "mbt_cmd":
                        case "ifv_cmd":
                        case "apc_cmd":
                        case "art_cmd":
                        case "aaa_cmd":
                            if (this.Year >= 2010)
                            {
                                role.Value.Uniform.AddToInventory("ACE_microDAGR");
                            }
                            else if (this.Year >= 2000 && this.Year < 2010)
                            {
                                if (role.Key == "scu_ldr")
                                    role.Value.Backpack.AddToInventory("ACE_DAGR");
                                else
                                    role.Value.Vest.AddToInventory("ACE_DAGR");
                            }
                            break;
                        case "fwc_pil":
                        case "fwt_pil":
                        case "rwc_pil":
                        case "rwt_pil":
                            if (this.Year >= 2010)
                            {
                                role.Value.Uniform.AddToInventory("ACE_microDAGR");
                            }
                            else if(this.Year > 2010 && this.Year >= 2000)
                            {
                                role.Value.Uniform.AddToInventory("ACE_DAGR");
                            }

                            if (this.Year >= 2000)
                            {
                                
                                role.Value.HasGPS = true;
                            }
                            break;
                        case "lrc_uav":
                        case "uwc_pil":
                            if (this.Year >= 2010)
                            {
                                role.Value.Uniform.AddToInventory("ACE_microDAGR");
                            }
                            else if (this.Year >= 2000)
                            {
                                // UAV terminal is actually a side dependant item, but abstracted here
                                role.Value.HasUAVTerminal = true;
                            }
                            break;
                        default:
                            if (this.Year >= 2010)
                            {
                                role.Value.Uniform.AddToInventory("ACE_microDAGR");
                            }
                            break;
                    }
                }
                else
                {
                    if (this.Year >= 2010)
                    {
                        role.Value.HasGPS = true;
                    }
                }

                // Add IR strobes
                if (hasIRStrobes)
                {
                    role.Value.Uniform.AddToInventory("ACE_IR_Strobe_Item", 1);
                }

                // Add flashlights
                if (this.Year >= 2010)
                {
                    role.Value.Uniform.AddToInventory("ACE_Flashlight_XL50", 1);
                }
                else
                {
                    role.Value.Uniform.AddToInventory("ACE_Flashlight_MX991", 1);
                }

                // Add chemlights
                if (hasChemlights)
                {
                    switch (role.Key)
                    {
                        case "plt_ldr":
                        case "sqd_ldr":
                        case "sqd_ldg":
                        case "sqd_lat":
                        case "sqd_amg":
                        case "sqd_bre":
                            role.Value.Backpack.AddToInventory("Chemlight_red", 1);
                            role.Value.Backpack.AddToInventory("Chemlight_green", 1);
                            break;
                        case "lrc_ldr":
                        case "spf_ldr":
                        case "scu_ldr":
                            role.Value.Backpack.AddToInventory("Chemlight_red", 1);
                            role.Value.Backpack.AddToInventory("Chemlight_green", 1);
                            role.Value.Backpack.AddToInventory("ACE_Chemlight_IR", 1);
                            break;
                        case "med_ldr":
                        case "med_cpm":
                        case "eng_mec":
                            role.Value.Backpack.AddToInventory("ACE_Chemlight_HiWhite", 1);
                            break;
                        case "eng_eod":
                            role.Value.Backpack.AddToInventory("Chemlight_red", 2);
                            role.Value.Backpack.AddToInventory("Chemlight_green", 2);
                            break;
                        case "log_sgt":
                        case "tac_fac":
                        case "tac_jtc":
                            role.Value.Backpack.AddToInventory("ACE_Chemlight_UltraHiOrange", 1);
                            role.Value.Backpack.AddToInventory("ACE_Chemlight_HiYellow", 1);
                            break;
                    }
                }

                // Add IR grenades
                if (hasIRGreandes)
                {
                    string grenade = string.Empty;
                    switch (this.Group.Side)
                    {
                        case Side.BLUFOR:
                            grenade = "B_IR_Grenade";
                            break;
                        case Side.OPFOR:
                            grenade = "O_IR_Grenade";
                            break;
                        case Side.INDEP:
                            grenade = "I_IR_Grenade";
                            break;
                    }

                    switch (role.Key)
                    {
                        case "plt_ldr":
                        case "sqd_ldr":
                        case "sqd_ldg":
                        case "mmg_ldr":
                        case "mat_ldr":
                        case "hat_ldr":
                        case "aat_ldr":
                        case "how_ldr":
                        case "mot_ldr":
                        case "dat_ldr":
                        case "dmg_ldr":
                        case "dgg_ldr":
                        case "lrc_ldr":
                        case "spf_ldr":
                        case "tac_fac":
                        case "tac_jtc":
                            role.Value.Vest.AddToInventory(grenade);
                            break;
                        case "scu_ldr":
                            role.Value.Backpack.AddToInventory(grenade);
                            break;
                    }
                }

                // Add medical items
                switch (role.Key)
                {
                    case "med_ldr":
                    case "med_cpm":
                        role.Value.Uniform.AddToInventory("ACE_fieldDressing", 2);
                        role.Value.Uniform.AddToInventory("ACE_packingBandage", 3);
                        role.Value.Uniform.AddToInventory("ACE_elasticBandage", 2);
                        role.Value.Uniform.AddToInventory("ACE_quikclot", 3);
                        role.Value.Uniform.AddToInventory("ACE_tourniquet", 2);
                        role.Value.Uniform.AddToInventory("ACE_splint", 2);

                        role.Value.Backpack.AddToInventory("ACE_fieldDressing", 10);
                        role.Value.Backpack.AddToInventory("ACE_packingBandage", 10);
                        role.Value.Backpack.AddToInventory("ACE_elasticBandage", 10);
                        role.Value.Backpack.AddToInventory("ACE_quikclot", 8);
                        role.Value.Backpack.AddToInventory("ACE_tourniquet", 4);
                        role.Value.Backpack.AddToInventory("ACE_morphine", 8);
                        role.Value.Backpack.AddToInventory("ACE_epinephrine", 6);
                        role.Value.Backpack.AddToInventory("ACE_adenosine", 2);
                        role.Value.Backpack.AddToInventory("ACE_plasmaIV_500", 6);
                        role.Value.Backpack.AddToInventory("ACE_salineIV_500", 2);
                        role.Value.Backpack.AddToInventory("ACE_surgicalKit", 1);
                        break;
                    case "spf_lat":
                    case "scu_lat":
                        role.Value.Uniform.AddToInventory("ACE_fieldDressing", 2);
                        role.Value.Uniform.AddToInventory("ACE_packingBandage", 3);
                        role.Value.Uniform.AddToInventory("ACE_elasticBandage", 2);
                        role.Value.Uniform.AddToInventory("ACE_quikclot", 3);
                        role.Value.Uniform.AddToInventory("ACE_tourniquet", 2);
                        role.Value.Uniform.AddToInventory("ACE_splint", 2);

                        role.Value.Backpack.AddToInventory("ACE_fieldDressing", 10);
                        role.Value.Backpack.AddToInventory("ACE_packingBandage", 10);
                        role.Value.Backpack.AddToInventory("ACE_elasticBandage", 10);
                        role.Value.Backpack.AddToInventory("ACE_quikclot", 8);
                        role.Value.Backpack.AddToInventory("ACE_tourniquet", 4);
                        role.Value.Backpack.AddToInventory("ACE_morphine", 8);
                        role.Value.Backpack.AddToInventory("ACE_epinephrine", 6);
                        role.Value.Backpack.AddToInventory("ACE_adenosine", 2);
                        role.Value.Backpack.AddToInventory("ACE_plasmaIV", 2);
                        role.Value.Backpack.AddToInventory("ACE_plasmaIV_500", 8);
                        role.Value.Backpack.AddToInventory("ACE_surgicalKit", 1);
                        break;
                    case "fwc_pil":
                    case "fwt_pil":
                    case "rwc_pil":
                    case "rwt_pil":
                        role.Value.Uniform.AddToInventory("ACE_fieldDressing", 2);
                        role.Value.Uniform.AddToInventory("ACE_packingBandage", 3);
                        role.Value.Uniform.AddToInventory("ACE_elasticBandage", 2);
                        role.Value.Uniform.AddToInventory("ACE_quikclot", 3);
                        role.Value.Uniform.AddToInventory("ACE_tourniquet", 2);
                        role.Value.Uniform.AddToInventory("ACE_morphine", 1);
                        role.Value.Uniform.AddToInventory("ACE_splint", 2);
                        break;
                    case "civ_zeu":
                        role.Value.Uniform.AddToInventory("ACE_fieldDressing", 2);
                        role.Value.Uniform.AddToInventory("ACE_packingBandage", 2);
                        role.Value.Uniform.AddToInventory("ACE_elasticBandage", 2);
                        role.Value.Uniform.AddToInventory("ACE_quikclot", 2);
                        break;
                    default:
                        role.Value.Uniform.AddToInventory("ACE_fieldDressing", 2);
                        role.Value.Uniform.AddToInventory("ACE_packingBandage", 3);
                        role.Value.Uniform.AddToInventory("ACE_elasticBandage", 2);
                        role.Value.Uniform.AddToInventory("ACE_quikclot", 3);
                        role.Value.Uniform.AddToInventory("ACE_tourniquet", 2);
                        role.Value.Uniform.AddToInventory("ACE_splint", 2);
                        break;
                }

                // Add spray paint
                if (this.Year >= 1985)
                {
                    switch (role.Key)
                    {
                        case "plt_ldr":
                        case "sqd_ldr":
                        case "sqd_ldg":
                        case "mmg_ldr":
                        case "mat_ldr":
                        case "hat_ldr":
                        case "aat_ldr":
                        case "lrc_ldr":
                        case "spf_ldr":
                        case "scu_ldr":
                            role.Value.Uniform.AddToInventory("ACE_SpraypaintBlue", 1);
                            break;
                        case "eng_eod":
                            role.Value.Uniform.AddToInventory("ACE_SpraypaintRed", 1);
                            role.Value.Uniform.AddToInventory("ACE_SpraypaintGreen", 1);
                            break;
                    }
                }

                // Add specialty gear for specific roles
                switch (role.Key)
                {
                    case "log_sgt":
                        role.Value.Backpack.AddToInventory("ToolKit");
                        role.Value.Backpack.AddToInventory("BWI_Construction_Tool");
                        break;
                    case "eng_mec":
                        role.Value.Backpack.AddToInventory("ToolKit");
                        break;
                    case "eng_eod":
                        role.Value.SetWeapon(Weapon.Lookup("ACE_VMM3"));
                        role.Value.Backpack.AddToInventory("ACE_DefusalKit");
                        if (hasEODEquipment)
                        {
                            role.Value.Backpack.AddToInventory("ACE_Clacker");
                            role.Value.Backpack.AddToInventory("DemoCharge_Remote_Mag", 4);
                        }
                        break;
                    case "eng_def":
                        role.Value.Backpack.AddToInventory("BWI_Construction_Tool");
                        if (hasIEDs)
                        {
                            role.Value.Backpack.AddToInventory("ACE_Cellphone");
                            role.Value.Backpack.AddToInventory("IEDUrbanBig_Remote_Mag");
                            role.Value.Backpack.AddToInventory("IEDLandBig_Remote_Mag", 2);
                        }
                        else
                        {
                            role.Value.Backpack.AddToInventory("ATMine_Range_Mag", 2);
                            role.Value.Backpack.AddToInventory("SLAMDirectionalMine_Wire_Mag", 2);
                            role.Value.Backpack.AddToInventory("APERSBoundingMine_Range_Mag", 4);
                            role.Value.Backpack.AddToInventory("ACE_Clacker");
                        }
                        break;
                    case "eng_dem":
                        if (hasIEDs)
                        {
                            role.Value.Backpack.AddToInventory("ACE_Cellphone");
                            role.Value.Backpack.AddToInventory("IEDLandBig_Remote_Mag");
                            role.Value.Backpack.AddToInventory("IEDUrbanSmall_Remote_Mag");
                            role.Value.Backpack.AddToInventory("IEDLandSmall_Remote_Mag");
                            role.Value.Backpack.AddToInventory("ACE_DeadManSwitch");
                            role.Value.Backpack.AddToInventory("DemoCharge_Remote_Mag");
                        }
                        else
                        {
                            role.Value.Backpack.AddToInventory("ACE_M26_Clacker");
                            role.Value.Backpack.AddToInventory("SatchelCharge_Remote_Mag", 3);
                        }
                        break;
                    case "tac_fac":
                        if (hasKestral)
                        {
                            role.Value.Uniform.AddToInventory("ACE_Kestrel4500");
                        }
                        break;
                    case "sqd_sap":
                    case "spf_sap":
                    case "scu_sap":
                        role.Value.Backpack.AddToInventory("ACE_wirecutter");
                        if (hasIEDs)
                        {
                            role.Value.Backpack.AddToInventory("ACE_Cellphone");
                            role.Value.Backpack.AddToInventory("IEDUrbanSmall_Remote_Mag", 2);
                            role.Value.Backpack.AddToInventory("IEDLandSmall_Remote_Mag", 2);
                        }
                        else
                        {
                            role.Value.Backpack.AddToInventory("ACE_Clacker");
                            role.Value.Backpack.AddToInventory("DemoCharge_Remote_Mag", 6);
                        }
                        break;
                    case "lrc_hir":
                        if (hasHuntIR)
                        {
                            role.Value.Backpack.AddToInventory("ACE_HuntIR_monitor");
                            role.Value.Backpack.AddToInventory("ACE_HuntIR_M203", 8);
                        }
                        break;
                    case "lrc_uav":
                        role.Value.Vest.AddToInventory("ACE_UAVBattery", 2);
                        break;
                    case "sqd_bre":
                    case "spf_bre":
                    case "scu_bre":
                        if (hasFlashbangs)
                        {
                            role.Value.Backpack.AddToInventory("ACE_M84", 6);
                        }
                        break;
                    case "mmg_ldr":
                        role.Value.Backpack.AddToInventory("ACE_SpareBarrel");
                        break;
                    case "mot_ldr":
                    case "mot_gun":
                        if (hasKestral)
                        {
                            role.Value.Vest.AddToInventory("ACE_Kestrel4500");
                        }
                        role.Value.Uniform.AddToInventory("ACE_RangeTable_82mm");
                        break;
                    case "how_ldr":
                    case "how_gun":
                        if (hasKestral)
                        {
                            role.Value.Vest.AddToInventory("ACE_Kestrel4500");
                        }
                        role.Value.Uniform.AddToInventory("ACE_artilleryTable");
                        break;
                    case "lrc_dmr":
                    case "spf_dmr":
                    case "scu_dmr":
                        if (hasTripod)
                        {
                            role.Value.Backpack.AddToInventory("ACE_Tripod");
                        }
                        if (hasKestral)
                        {
                            role.Value.Uniform.AddToInventory("ACE_Kestrel4500");
                        }
                        role.Value.Uniform.AddToInventory("ACE_RangeCard");
                        break;
                }

                // Add binoculars
                switch (role.Key)
                {
                    case "plt_ldr":
                    case "log_sgt":
                    case "tac_fac":
                    case "sqd_ldr":
                    case "sqd_ldg":
                    case "sqd_amg":
                    case "mmg_ldr":
                    case "mat_ldr":
                    case "hat_ldr":
                    case "aat_ldr":
                    case "dat_ldr":
                    case "dmg_ldr":
                    case "dgg_ldr":
                        if (hasVector)
                        {
                            if (new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR", "MILITIA" }.Contains(this.Type))
                            {
                                switch (this.Group.Side)
                                {
                                    case Side.BLUFOR:
                                        role.Value.SetWeapon(Weapon.Lookup("rhsusf_bino_lrf_Vector21"));
                                        break;
                                    case Side.OPFOR:
                                    case Side.INDEP:
                                        role.Value.SetWeapon(Weapon.Lookup("rhs_pdu4"));
                                        break;
                                }
                            }
                            else
                            {
                                role.Value.SetWeapon(Weapon.Lookup("rhssaf_zrak_rd7j"));
                            }
                        }
                        else
                        {
                            role.Value.SetWeapon(Weapon.Lookup("Binocular"));
                        }
                        break;
                    case "lrc_ldr":
                    case "lrc_dmr":
                    case "spf_ldr":
                    case "scu_ldr":
                    case "scu_dmr":
                    case "how_ldr":
                    case "mot_ldr":
                        if (this.Year >= 2000)
                        {
                            if (new[] { "AIRBORNE", "MARINES", "ARMORED", "INFANTRY", "CONTRACTOR" }.Contains(this.Type))
                            {
                                if (this.Year >= 2010)
                                {
                                    role.Value.SetWeapon(Weapon.Lookup("ACE_Vector"));
                                }
                                else
                                {
                                    role.Value.SetWeapon(Weapon.Lookup("ACE_VectorDay"));
                                }
                            }
                            else
                            {
                                role.Value.SetWeapon(Weapon.Lookup("rhssaf_zrak_rd7j"));
                            }
                        }
                        else
                        {
                            role.Value.SetWeapon(Weapon.Lookup("Binocular"));
                        }
                        break;
                    case "lrc_hir":
                        if (hasThermalBinoculars)
                        {
                            role.Value.SetWeapon(Weapon.Lookup("ACE_MX2A"));
                        }
                        else
                        {
                            role.Value.SetWeapon(Weapon.Lookup("Binocular"));
                        }
                        break;
                    case "spf_dmr":
                    case "tac_jtc":
                        if (hasLaserDesignator)
                        {
                            role.Value.SetWeapon(Weapon.Lookup("Laserdesignator_01_khk_F"));
                            role.Value.Backpack.AddToInventory("Laserbatteries", 2);
                        }
                        else
                        {
                            role.Value.SetWeapon(Weapon.Lookup("Binocular"));
                        }
                        break;
                    default:
                        role.Value.SetWeapon(Weapon.Lookup("Binocular"));
                        break;

                }

            }
        }

        // TODO: This needs to be cleaner / more general.
        private void SetWeapon(string roleCode, string caseToParse)
        {
            // This statement trips if the default case is not set for a switchStatement.
            if (string.IsNullOrEmpty(caseToParse)) { return; }

            var singleWeapon = Regex.Match(caseToParse, @"addWeapon\s+.(\w+).");
            var randomWeapon = Regex.Match(caseToParse, @"\[_unit,\s*(\[.*?\])\s*\]\s+call\s+(bwi_.*?;)", RegexOptions.Singleline); // Handles weapon randomization
            var random = Regex.Replace(randomWeapon.Groups[1].Value, "[ \"\t\r\n]", string.Empty).Split(new string[] { "[", ",", "]" }, StringSplitOptions.RemoveEmptyEntries);

            var weapon = singleWeapon.Groups[1].Value;

            // Set default equipment for all roles.
            if (roleCode.Equals("default"))
            {
                foreach (var role in Role.RoleDictionary)
                {
                    if (!this.Roles.ContainsKey(role.Key))
                    {
                        this.Roles.Add(role.Key, new Role(role.Value));
                    }

                    if (random.Length != 0 && string.IsNullOrEmpty(weapon))
                    {
                        // TODO Figure out how to represent random equipment (oh jesus)
                        this.Roles[role.Key].SetWeapon(Weapon.Lookup(random[0]));
                    }
                    else
                    {
                        this.Roles[role.Key].SetWeapon(Weapon.Lookup(weapon));
                    }
                }
                return;
            }

            // Role equipment intentionally set blank.
            if (string.IsNullOrEmpty(weapon))
            {
                if (!this.Roles.ContainsKey(roleCode))
                {
                    this.Roles.Add(roleCode, new Role(Role.Lookup(roleCode)));
                }

                this.Roles[roleCode].SetWeapon(null);
                return;
            }

            if (!this.Roles.ContainsKey(roleCode))
            {
                this.Roles.Add(roleCode, new Role(Role.Lookup(roleCode)));
            }

            this.Roles[roleCode].SetWeapon(Weapon.Lookup(weapon));

        }

        private static Dictionary<string, string> GetPairsFromSwitch(string switchBlock, string keywordCase)
        {
            //List<string[]> pairings = new List<string[]>();
            Dictionary<string, string> pairings = new Dictionary<string, string>();
            int startCaptureIndex = -1;
            int bracketDepth = 0;
            string roleCode = null;
            string roleCodeContents;

            // Condition is i > -1 because eventually switchBlock.IndexOf will return -1 for no new cases found.
            for (int i = switchBlock.IndexOf(keywordCase); i > -1; i++)
            {
                // Found a case. 
                for (startCaptureIndex = i + keywordCase.Length; i < switchBlock.Length; i++)
                {
                    if (string.IsNullOrEmpty(roleCode) && (switchBlock[i] == ':' || switchBlock[i] == ';'))
                    {
                        roleCode = switchBlock.Substring(startCaptureIndex, i - startCaptureIndex);
                        roleCodeContents = null;
                    }

                    if (switchBlock[i] == '{')
                    {
                        if (bracketDepth == 0)
                            startCaptureIndex = i;

                        bracketDepth++;
                    }
                    else if (switchBlock[i] == '}')
                    {
                        bracketDepth--;
                    }
                    else if (switchBlock[i] == ';' && bracketDepth == 0)
                    {
                        roleCodeContents = switchBlock.Substring(startCaptureIndex, i - startCaptureIndex);

                        if (roleCode.Equals(roleCodeContents))
                            roleCodeContents = null;

                        //pairings.Add(new string[] { roleCode.Replace("\"", string.Empty), roleCodeContents });
                        pairings.Add(roleCode.Replace("\"", string.Empty), roleCodeContents);

                        i = switchBlock.IndexOf(keywordCase, i) - 1; // skip to next case
                        startCaptureIndex = i + keywordCase.Length;
                        roleCode = null; // ready for next code;
                        break;
                    }
                }
            }

            // Add in the default case at the end.
            //pairings.Add(GetDefaultCase(switchBlock));
            pairings.Add("default", GetDefaultCase(switchBlock));
            return pairings;
        }

        private static string GetDefaultCase(string switchBlock)
        {
            // In ARMA code, the default case can be at the start or end.
            int startCaptureIndex = -1;
            int bracketDepth = 0;

            for (int i = switchBlock.IndexOf("default"); i < switchBlock.Length; i++)
            {
                // IndexOf will set i = -1 if there is no default case.
                if (i == -1)
                {
                    break;
                }

                if (switchBlock[i] == '{')
                {
                    if (bracketDepth == 0)
                        startCaptureIndex = i;

                    bracketDepth++;
                }
                else if (switchBlock[i] == '}')
                {
                    bracketDepth--;
                }
                else if (switchBlock[i] == ';' && bracketDepth == 0)
                {
                    return switchBlock.Substring(startCaptureIndex, i - startCaptureIndex);
                }
            }

            // No default case.
            return null;
        }

        public string BWIName { get; set; }

        public string Name { get; set; }
        public ushort Year { get; set; }
        public string Type { get; set; }

        public string Uniform { get; set; }
        public Dictionary<string, Role> Roles { get; set; }
        public string Grenade { get; set; }
        public string Ammo { get; set; }
        public List<string> HiddenElements { get; set; }
        public List<string> HiddenRoles { get; set; }

        public string ACRERadioDevices { get; set; }
        public List<MotorpoolVehicle> MotorpoolRegulars { get; set; }

        public List<MotorpoolVehicle> MotorpoolRecon { get; set; }

        public List<MotorpoolVehicle> MotorpoolSpecialForces { get; set; }

        public List<MotorpoolVehicle> GetAllVehicles(bool noDuplicateVehicles = false)
        {
            List<MotorpoolVehicle> allVehicles = new List<MotorpoolVehicle>();

            if (noDuplicateVehicles)
            {
                foreach (MotorpoolVehicle vic in this.GetAllVehicles(false))
                {
                    if (!allVehicles.Contains(vic))
                        allVehicles.Add(vic);
                }
                return allVehicles;
            }
            else
            {
                allVehicles.AddRange(GetSpecialForcesVehicles());
                allVehicles.AddRange(GetReconVehicles());
                allVehicles.AddRange(GetRegularVehicles());
                return allVehicles;
            }
        }

        // Some cheap methods to set up for coloring.
        public List<MotorpoolVehicle> GetRegularVehicles()
        {
            foreach (var vic in this.MotorpoolRegulars)
            {
                vic.IsRecon = false;
                vic.IsSpecialForces = false;
            }

            return this.MotorpoolRegulars;
        }

        public List<MotorpoolVehicle> GetReconVehicles()
        {
            foreach (var vic in this.MotorpoolRecon)
            {
                vic.IsRecon = true;
                vic.IsSpecialForces = false;
            }

            return this.MotorpoolRecon;
        }

        public List<MotorpoolVehicle> GetSpecialForcesVehicles()
        {
            foreach (var vic in this.MotorpoolSpecialForces)
            {
                vic.IsRecon = false;
                vic.IsSpecialForces = true;
            }

            return this.MotorpoolSpecialForces;
        }

        public void SortMotorpool()
        {
            this.MotorpoolRegulars = this.MotorpoolRegulars.OrderBy(o => o.Category).ToList();
        }

        // TODO awful code, rewrite.
        // TODO higer priority, god damn this is bad code.
        public string VehiclesInMotorpoolLevelToString(ushort level, bool groupSimilarUtilityVehiclesTogether = true)
        {
            if (this.MotorpoolRegulars == null)
            {
                return null;
            }
            else
            {
                string formatedText = "";
                string seperator = ", ";
                string reconMarker = "*";
                string specialForcesMarker = "**";

                // Try to group stuff like GAZ-66 (Ammo) & GAZ-66 (Fuel) together.
                List<MotorpoolVehicle> vehicleList = GetVehiclesInMotorpoolLevel(level);
                List<string> groupedVehiclesThatWereFound = new List<string>();

                // Find the first instance of vehicles that should be grouped, remember the first instance, and remove the rest from the list.
                bool skip = false;
                foreach (MotorpoolVehicle vehicle in vehicleList.ToList())
                {
                    skip = false;
                    if (groupSimilarUtilityVehiclesTogether
                        && vehicle.Escalation == Constants.UTILITY
                        || (vehicle.Escalation == Constants.ANTI_CAR && vehicle.Category == Constants.TRUCK))
                    {
                        foreach (String matchingVehicle in Constants.GrouppedUtilityVehicles.Keys)
                        {
                            if (vehicle.ToString().Contains(matchingVehicle))
                            {
                                if (!groupedVehiclesThatWereFound.Contains(matchingVehicle))
                                {
                                    groupedVehiclesThatWereFound.Add(matchingVehicle);
                                    formatedText += String.Format("{0}, ", Constants.GrouppedUtilityVehicles[matchingVehicle]);
                                    //first = false;
                                    skip = true;
                                }
                                else
                                {
                                    vehicleList.Remove(vehicle);
                                    skip = true;
                                    break;
                                }
                            }
                        }
                    }

                    // Add vehicle to the formatted list
                    if (!skip)
                    {
                        formatedText += vehicle.FilteredName;
                    }

                    // Don't add a seperator if this is the last vehicle
                    if (vehicle != vehicleList.Last() && !skip)
                    {
                        formatedText += seperator;
                    }

                }

                // Recon vehicles
                var reconVehicleList = GetVehiclesInMotorpoolLevel(level, Constants.RECON);
                if (this.MotorpoolRecon.Count > 0)
                {
                    if (this.MotorpoolRegulars.Count > 0 && reconVehicleList.Count > 0 && !skip)
                    {
                        formatedText += seperator;
                    }

                    foreach (Vehicle vehicle in reconVehicleList)
                    {
                        formatedText += vehicle + reconMarker;
                        if (vehicle != reconVehicleList.Last())
                        {
                            formatedText += seperator;
                        }
                    }
                }

                // Special Forces vehicles
                var specialForcesVehicleList = GetVehiclesInMotorpoolLevel(level, Constants.SPECIAL_FORCES);
                if (this.MotorpoolSpecialForces.Count > 0)
                {
                    if (this.MotorpoolRegulars.Count > 0 && specialForcesVehicleList.Count > 0 && !skip)
                    {
                        formatedText += seperator;
                    }

                    foreach (Vehicle vehicle in specialForcesVehicleList)
                    {
                        // Don't list vehicle as SF if it's also a recon vehicle.
                        if (!reconVehicleList.Contains(vehicle))
                        {
                            formatedText += vehicle + specialForcesMarker;
                            if (vehicle != specialForcesVehicleList.Last())
                            {
                                formatedText += seperator;
                            }
                        }

                    }
                }
                return formatedText;
            }
        }

        // TODO rewrite this too
        public List<MotorpoolVehicle> GetVehiclesInMotorpoolLevel(ushort level, ushort type = Constants.REGULARS)
        {
            List<MotorpoolVehicle> motorpoolKind;
            switch (type)
            {
                case Constants.REGULARS:
                    motorpoolKind = this.MotorpoolRegulars;
                    break;
                case Constants.RECON:
                    motorpoolKind = this.MotorpoolRecon;
                    break;
                case Constants.SPECIAL_FORCES:
                    motorpoolKind = this.MotorpoolSpecialForces;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("GetVehiclesInMotorpoolLevel was an invalid value");
            }

            if (motorpoolKind == null)
            {
                return null;
            }
            else
            {
                List<MotorpoolVehicle> vehiclesInMotorpolLevel = new List<MotorpoolVehicle>();
                foreach (MotorpoolVehicle vehicle in motorpoolKind)
                {
                    if (vehicle.Escalation == level)
                    {
                        vehiclesInMotorpolLevel.Add(vehicle);
                    }
                }

                vehiclesInMotorpolLevel = vehiclesInMotorpolLevel.OrderBy(o => o.Category).ToList();
                return vehiclesInMotorpolLevel;
            }
        }

        public string[] Resupply
        {
            get
            {
                string[] array = Array.Empty<string>();
                if(this.ResupplyRegular  != null)   { array = array.Concat(this.ResupplyRegular).ToArray(); }
                if (this.ResupplyWeapons != null)   { array = array.Concat(this.ResupplyWeapons).ToArray(); }
                if (this.ResupplyRecon   != null)   { array = array.Concat(this.ResupplyRecon).ToArray();   }
                if (this.ResupplySpecial != null)   { array = array.Concat(this.ResupplySpecial).ToArray(); }

                return array;
            }
        }
        public string[] ResupplyRegular { get; set; }
        public string[] ResupplyWeapons { get; set; }
        public string[] ResupplyRecon { get; set; }
        public string[] ResupplySpecial { get; set; }

        public override string ToString()
        {
            return String.Format("{0} {1}", this.Year, this.Name);
        }
    }
}
