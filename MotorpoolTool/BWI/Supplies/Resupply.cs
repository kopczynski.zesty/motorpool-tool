﻿using MotorpoolTool.ARMA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotorpoolTool.BWI.Supplies
{
    /// <summary>
    /// resupply_main version of resupply.
    /// </summary>
    public class Resupply : Vehicle
    {
        public Resupply(Vehicle baseVehicle) : base(baseVehicle)
        {

        }

        public Resupply(string baseVehicleClassName) : this(Vehicle.Lookup(baseVehicleClassName))
        {

        }
    }
}
