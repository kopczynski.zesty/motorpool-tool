﻿using MotorpoolTool.ARMA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotorpoolTool.BWI.Supplies
{
    /// <summary>
    /// data_main side of resupply.
    /// This is a container/helper class for Resupply, not a proper ARMA class.
    /// </summary>
    public class Supply
    {
        public static Dictionary<string, Supply> SupplyDictionary = new Dictionary<string, Supply>(StringComparer.OrdinalIgnoreCase);

        public static Supply Lookup(string className)
        {
            SupplyDictionary.TryGetValue(className, out Supply supply);
            return supply;
        }

        public string ClassName { get; private set; }

        public Supply(string className, string classNameOfResupplyCrate, string[] compatibleWith = null)
        {
            this.ClassName = className;
            this.Crate = Vehicle.Lookup(classNameOfResupplyCrate);
            this.CompatibleWeapons = compatibleWith;
        }
        public Vehicle Crate { get; private set; }
        public string[] CompatibleWeapons { get; private set; }

        public static List<Supply> ArrayToList(string[] array)
        {
            var supplyList = new List<Supply>();
            foreach(string supply in array)
            {
                supplyList.Add(Supply.Lookup(supply));
            }
            return supplyList;
        }

        public override string ToString()
        {
            return this.Crate != null ? this.Crate.ToString() : this.ClassName;
        }
    }
}
