﻿using MotorpoolTool.ARMA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotorpoolTool.BWI.Supplies
{
    public class SupplyGroup : ARMAEntity
    {
        public List<Supply> Supplies;

        public SupplyGroup(string className, string displayName, List<Supply> supplies)
        {
            this.ClassName = className;
            this.DisplayName = displayName;
            this.Supplies = supplies;
        }
    }
}
