﻿namespace MotorpoolTool.BWI.Stagings
{
    public class Template
    {
        public string Author { get; private set; }
        public string OnLoadName { get; private set; }
        public string OnLoadMission { get; private set; }
        public bool EnableDebugConsole { get; private set; }


    }
}
