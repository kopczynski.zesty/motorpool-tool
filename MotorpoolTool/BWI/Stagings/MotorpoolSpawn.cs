﻿namespace MotorpoolTool.BWI.Stagings
{
    public class MotorpoolSpawn
    {
        public enum MotorpoolTypes
        {
            None,
            Ground,
            Helipad,
            Hangar,
            Apron,
            Carrier,
            Sea
        }

        public MotorpoolSpawn(string className, string label, int type)
        {
            this.ClassName = className;
            this.Label = label;
            this.Type = (MotorpoolTypes)type;
        }

        public string ClassName { get; private set; }
        public string Label { get; private set; }
        public MotorpoolTypes Type { get; private set; }

        public override string ToString()
        {
            return string.Format($"({this.Type}) {this.Label}");
        }
    }
}
