﻿using SFML.System;
using System.Collections.Generic;

namespace MotorpoolTool.BWI.Stagings
{
    public class Staging
    {
        public static Dictionary<string, List<Staging>> StagingListDictionary = new Dictionary<string, List<Staging>>();
        public List<Staging> Lookup(string className)
        {
            return StagingListDictionary[className.ToLowerInvariant()];
        }

        public Staging(string className, string displayName, Vector2f position, string teleporter,
            string[] motorpoolSpawns, string[] motorpoolLabels, int[] motorpoolTypes, string[] resupplyLocations)
        {
            this.ClassName = className;
            this.DisplayName = displayName;
            this.Position = position;
            this.Teleporter = teleporter;

            this.MotorpoolSpawns = new List<MotorpoolSpawn>();
            for (int index = 0; index < motorpoolSpawns.Length; index++)
            {
                this.MotorpoolSpawns.Add(new MotorpoolSpawn(motorpoolSpawns[index].Trim(), motorpoolLabels[index].Trim(), motorpoolTypes[index]));
            }

        }
        public string ClassName { get; private set; }
        public string DisplayName { get; private set; }
        public Vector2f Position { get; private set; }
        private string Teleporter { get; set; }
        public List<MotorpoolSpawn> MotorpoolSpawns { get; set; }
        public List<string> ResupplyLocations { get; private set; }

        public override string ToString()
        {
            return this.DisplayName;
        }
    }
}
