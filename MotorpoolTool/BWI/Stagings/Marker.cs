﻿using SFML.Graphics;
using SFML.System;

namespace MotorpoolTool.BWI.Stagings
{
    internal class Marker : Drawable
    {
        public static readonly Color COLOR_BLUFOR = new Color(0, 76, 152);
        public static readonly Color COLOR_BLUFOR_TRANS = new Color(0, 76, 152, 50);

        public static readonly Color COLOR_OPFOR = new Color(127, 0, 0);
        public static readonly Color COLOR_OPFOR_TRANS = new Color(127, 0, 0, 50);

        public static readonly Color COLOR_INDEP = new Color(0, 127, 0);
        public static readonly Color COLOR_INDEP_TRANS = new Color(0, 127, 0, 50);

        public static readonly Color COLOR_CIVIL = new Color(102, 0, 127);

        public Marker(Staging staging, Sprite sprite, Font font, string text)
        {
            this.Staging = staging;
            this.Sprite = sprite;
            this.Label = new Text(text, font);
            this.Label.CharacterSize = 24;
        }

        public Sprite Sprite { get; internal set; }
        public Text Label { get; internal set; }
        public Vector2f Scale
        {
            get { return this.Sprite.Scale; }
            internal set
            {
                this.Sprite.Scale = value;
            }
        }

        public Color Color
        {
            get { return this.Sprite.Color; }
            internal set
            {
                this.Sprite.Color = value;
                this.Label.FillColor = value;
            }
        }

        public float Rotation
        {
            get { return this.Label.Rotation; }
            internal set
            {
                this.Label.Rotation = value;
            }
        }

        public Vector2f Position
        {
            get { return this.Sprite.Position; }
            internal set
            {
                this.Sprite.Position = value;
            }
        }

        public bool Changed { get; set; }

        public Staging Staging { get; internal set; }

        public Vector2f OffsetRenderPosition { get; internal set; }


        public void Draw(RenderTarget target, RenderStates states)
        {
            this.Label.Position = new Vector2f(this.Sprite.Position.X + (this.Sprite.Texture.Size.X * this.Sprite.Scale.X), this.Sprite.Position.Y);
            target.Draw(this.Sprite, states);
            target.Draw(this.Label, states);
        }

        internal void Dispose()
        {
            this.Sprite.Dispose();
            this.Label.Dispose();
        }
    }
}
