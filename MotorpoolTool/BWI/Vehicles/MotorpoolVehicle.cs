﻿using ExtensionMethods.ARMA;
using MotorpoolTool.ARMA;
using MotorpoolTool.ARMA.Weapons;
using System;
using System.Collections.Generic;

namespace MotorpoolTool.BWI
{
    public class MotorpoolVehicle : Vehicle
    {
        /// <summary>
        /// Dictionary of all BWI Motorpool vehicles loaded. Key is the bwiClass.ToLowerInvariant()
        /// </summary>
        public static new Dictionary<string, MotorpoolVehicle> Lookup = new Dictionary<string, MotorpoolVehicle>();
        public string BWIClass { get; private set; }
        public new string Name
        {
            get
            {
                if (!string.IsNullOrEmpty(OverrideName))
                {
                    return this.OverrideName;
                }
                else
                {
                    return base.Name;
                }
            }
        }
        public string OverrideName { get; private set; }
        public int Cost { get; private set; }
        public int CrewOverride { get; private set; }
        public int PassengerOverride { get; private set; }
        public ushort Escalation { get; private set; }
        public string EscalationString
        {
            get { return string.Format("Class {0}", this.Escalation); }
        }
        public List<Magazine> Pylons { get; private set; }
        public bool IsCarrierAirWing { get; set; }
        public bool IsOutpost { get; private set; }

        // Temp bool, only for drawing in list view.
        public bool IsRecon { get; set; }

        // Temp bool, only for drawing in list view.
        public bool IsSpecialForces { get; set; }

        public string Outpost { get { return this.IsOutpost ? "Yes" : "No"; } }

        public string ReconSpecialLocked
        {
            get
            {
                if (IsRecon)
                    return "Recon";
                if (IsSpecialForces)
                    return "SF";
                else
                    return " Regulars";
            }
        }

        public MotorpoolVehicle(MotorpoolVehicle vehicle) : base(vehicle)
        {
            this.BWIClass = vehicle.BWIClass;
            this.OverrideName = vehicle.OverrideName;
            this.Cost = vehicle.Cost;
            this.CrewOverride = vehicle.CrewOverride;
            this.PassengerOverride = vehicle.PassengerOverride;
            this.Escalation = vehicle.Escalation;
            this.IsOutpost = vehicle.IsOutpost;
            this.IsCarrierAirWing = vehicle.IsCarrierAirWing;

            this.Pylons = vehicle.Pylons.Clone();
        }

        public MotorpoolVehicle(Vehicle vehicle, string bwiClass, string overrideName, int crewOverride, int passengerOverride, int cost,
            ushort escalation, string pylonLoadout, string pylonTurrets, bool isOutpost, string textureData = null, string animationData = null)
            : base(vehicle)
        {
            this.BWIClass = bwiClass;
            this.OverrideName = overrideName;
            this.Cost = cost;
            this.CrewOverride = crewOverride;
            this.PassengerOverride = passengerOverride;
            this.Escalation = escalation;
            this.IsOutpost = isOutpost;

            // Pylons
            this.Pylons = GetPylonsFromString(pylonLoadout, pylonTurrets);
        }

        private List<Magazine> GetPylonsFromString(string pylonLoadout, string pylonTurrets)
        {
            if (pylonLoadout == null) { return null; }
            List<Magazine> createdPylons = new List<Magazine>();

            var pylonMagazines = pylonLoadout.Split(',');
            var pylonControl = pylonTurrets != null ? Array.ConvertAll(pylonTurrets.Split(','), s => int.Parse(s)) : null;

            for (int index = 0; index < pylonMagazines.Length; index++)
            {
                if (pylonMagazines[index].Equals(string.Empty)) { continue; } // skip empty pylons

                if (Magazine.MagazineDictionary.TryGetValue(pylonMagazines[index].ToLowerInvariant(), out Magazine pylon))
                {
                    if (pylonControl != null) { pylon.Control = pylonControl[index]; }
                    createdPylons.Add(pylon);
                }
            }

            return createdPylons;
        }
        public int GetCrewCount()
        {
            return this.CrewOverride != -1 ? this.CrewOverride : this.Crew;
        }

        public int GetPassengerCount()
        {
            return this.PassengerOverride != -1 ? this.PassengerOverride : this.Passengers;
        }

        public int GetTotalCapacity()
        {
            return GetCrewCount() + GetPassengerCount();
        }

        public bool HasPylons
        {
            get { return this.Pylons != null && this.Pylons.Count > 0; }
        }

        public override string ToDebugString()
        {
            // god damn I love inheritance
            var debugString = base.ToDebugString();
            debugString += string.Format("IsOutpost:    {0}", this.IsOutpost);

            if (this.Pylons != null)
            {
                foreach (Magazine magazine in this.Pylons)
                {
                    debugString += string.Format("Pylons:       {0} ({1})\n", magazine.ToString(), magazine.Control == Constants.GUNNER ? "gunner" : "pilot");
                }
            }

            return debugString;
        }

        public new object Clone()
        {
            return new MotorpoolVehicle(this);
        }
    }
}
