class BWI_Resupply_CrateBaseSmall1;
class BWI_Resupply_CrateBaseSmall2;
class BWI_Resupply_CrateBaseSmall3;
class BWI_Resupply_CrateBaseSmall4;
class BWI_Resupply_CrateBaseMedium1;
class BWI_Resupply_CrateBaseMedium2;
class BWI_Resupply_CrateBaseLong1;
class Bwi_Resupply_CrateAirBase;

/**
 * Ammunition specific base crates, spawned and re-textured 
 * according to data provided through CfgPlayableSupplies.
 *
 * These definitions cover all possible munition resupply
 * types, mapping them to the corresponding position in
 * the texture set, and applying a name.
 *
 * Each file has a corresponding texture sheet and position
 * that is matched by the inherited crate name from the
 * bwi_resupply definitions as follows:
 *
 *   [Small1] [Small2]
 *   [Long1]  [Medium2]
 *   [Small3] [Small4]
 *            [Medium1]
 */
#include <configs\magazine_556.hpp> 	// Sheet 1   Small 1
#include <configs\magazine_545.hpp> 	// Sheet 1   Small 1
#include <configs\box_556.hpp> 			// Sheet 1   Small 2
#include <configs\box_545.hpp> 			// Sheet 1   Small 2
#include <configs\box_762.hpp>  		// Sheet 1   Small 3
#include <configs\magazine_762.hpp> 	// Sheet 1   Small 4
#include <configs\antitank_light.hpp>   // Sheet 1   Medium 1
#include <configs\grenade_40_frag.hpp>  // Sheet 1   Medium 2
#include <configs\antiair_heavy.hpp>	// Sheet 1   Long 1

#include <configs\magazine_12g.hpp> 	// Sheet 2   Small 1
#include <configs\magazine_9.hpp> 		// Sheet 2   Small 2
#include <configs\grenade_frag.hpp>		// Sheet 2   Small 3
#include <configs\grenade_smoke.hpp>	// Sheet 2   Small 4
//#include <configs\vehicle_misc.hpp>   // Sheet 2   Medium 1  (Unused since #457)
#include <configs\grenade_40_smoke.hpp> // Sheet 2   Medium 2
#include <configs\antitank_heavy.hpp>	// Sheet 2   Long 1

#include <configs\medical_blood.hpp>	// Sheet 3   Small 1
#include <configs\magazine_misc.hpp> 	// Sheet 3   Small 2
#include <configs\medical_general.hpp>  // Sheet 3   Small 3
#include <configs\explosive_general.hpp>// Sheet 3   Small 4
#include <configs\antitank_medium.hpp>  // Sheet 3   Medium 1
#include <configs\grenade_40_flare.hpp> // Sheet 3   Medium 2