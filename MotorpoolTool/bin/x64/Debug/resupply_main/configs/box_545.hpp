/**
 * RPK-74
 */
class BWI_Resupply_Box_45Rnd_7N10: BWI_Resupply_CrateBaseSmall2 {
	scope = 2;
	displayName = "5.45mm 45Rnd 7N10 RPK";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_1.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(60,rhs_45Rnd_545X39_7N10_AK);
		MACRO_ADDMAGAZINE(40,rhs_45Rnd_545X39_AK_Green);
	};

	class TransportItems {
		MACRO_ADDITEM(2,ACE_SpareBarrel);
	};
};

class BWI_Resupply_Box_45Rnd_7N6: BWI_Resupply_Box_45Rnd_7N10 {
	displayName = "5.45mm 45Rnd 7N6 RPK";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(60,rhs_45Rnd_545X39_7N6_AK);
		MACRO_ADDMAGAZINE(40,rhs_45Rnd_545X39_AK_Green);
	};
};

class BWI_Resupply_Box_45Rnd_7U1: BWI_Resupply_Box_45Rnd_7N10 {
	displayName = "5.45mm 45Rnd 7U1 SD RPK";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(80,rhs_45Rnd_545X39_7U1_AK);
	};
};