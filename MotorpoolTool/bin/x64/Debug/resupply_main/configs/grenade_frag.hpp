/**
 * M67 (US)
 */
class BWI_Resupply_Gren_Hand_HE_M67: BWI_Resupply_CrateBaseSmall3 {
	scope = 2;
	displayName = "M67 Fragmentation";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_2.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_m67);
	};
};


/**
 * DM51 (German)
 */
class BWI_Resupply_Gren_Hand_HE_DM51A1: BWI_Resupply_Gren_Hand_HE_M67 {
	displayName = "DM51A1 Fragmentation";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ger_signs_2.paa",
 		"\bwi_resupply_main\data\ger_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,BWA3_DM51A1);
	};
};


/**
 * M84 (Serbian)
 */
class BWI_Resupply_Gren_Hand_HE_M84: BWI_Resupply_Gren_Hand_HE_M67 {
	displayName = "M84 Fragmentation";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_2.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhssaf_mag_br_m84);
	};
};


/**
 * RGD5 (Russian)
 */
class BWI_Resupply_Gren_Hand_HE_RGD5: BWI_Resupply_Gren_Hand_HE_M67 {
	displayName = "RGD5 Fragmentation";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_2.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_rgd5);
	};
};