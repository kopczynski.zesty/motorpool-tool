/**
 * Corpsman Usage
 */
class BWI_Resupply_Medical_Blood: BWI_Resupply_CrateBaseSmall1 {
	scope = 2;
	displayName = "IV Blood Bags";

 	hiddenSelectionsTextures[] = { 
 		"\bwi_resupply_main\data\usa_signs_3.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportItems {
		MACRO_ADDITEM(12,ACE_bloodIV);
		MACRO_ADDITEM( 8,ACE_bloodIV_500);
	};
};