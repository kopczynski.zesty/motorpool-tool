/**
 * M203, M320
 */
class BWI_Resupply_Gren_40mm_WS_M714: BWI_Resupply_CrateBaseMedium2 {
	scope = 2;
	displayName = "40mm M714 White Smoke";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_2.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_m714_White);
	};	
};

class BWI_Resupply_Gren_40mm_CS_M713: BWI_Resupply_Gren_40mm_WS_M714 {
	displayName = "40mm M713 Colored Smoke";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(12,rhs_mag_m713_Red);
		MACRO_ADDMAGAZINE(10,rhs_mag_m715_Green);
		MACRO_ADDMAGAZINE( 8,rhs_mag_m716_yellow);
	};	
};


/**
 * Generic
 */
class BWI_Resupply_Gren_40mm_WS_Generic: BWI_Resupply_Gren_40mm_WS_M714 {
	displayName = "40mm White Smoke";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,1Rnd_Smoke_Grenade_shell);
	};	
};

class BWI_Resupply_Gren_40mm_CS_Generic: BWI_Resupply_Gren_40mm_WS_Generic {
	displayName = "40mm Colored Smoke";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(12,1Rnd_SmokeRed_Grenade_shell);
		MACRO_ADDMAGAZINE(10,1Rnd_SmokeGreen_Grenade_shell);
		MACRO_ADDMAGAZINE( 8,1Rnd_SmokeYellow_Grenade_shell);
	};	
};


/**
 * L123A2
 */
class BWI_Resupply_Gren_40mm_WS_L123A2: BWI_Resupply_Gren_40mm_WS_M714 {
	displayName = "40mm L123A2 White Smoke";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\gbr_signs_2.paa",
 		"\bwi_resupply_main\data\gbr_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,UK3CB_BAF_1Rnd_Smoke_Grenade_shell);
	};	
};

class BWI_Resupply_Gren_40mm_CS_L123A2: BWI_Resupply_Gren_40mm_WS_L123A2 {
	displayName = "40mm L123A2 Colored Smoke";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(12,UK3CB_BAF_1Rnd_SmokeRed_Grenade_shell);
		MACRO_ADDMAGAZINE(10,UK3CB_BAF_1Rnd_SmokeGreen_Grenade_shell);
		MACRO_ADDMAGAZINE( 8,UK3CB_BAF_1Rnd_SmokeYellow_Grenade_shell);
	};	
};


/**
 * VG-40MD
 */
class BWI_Resupply_Gren_40mm_WS_VG40MD: BWI_Resupply_Gren_40mm_WS_M714 {
	displayName = "40mm VG-40MD White Smoke";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_2.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_VG40MD_White);
	};	
};

class BWI_Resupply_Gren_40mm_CS_VG40MD: BWI_Resupply_Gren_40mm_WS_VG40MD {
	displayName = "40mm VG-40MD Colored Smoke";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,rhs_VG40MD_Red);
		MACRO_ADDMAGAZINE(14,rhs_VG40MD_Green);
	};	
};