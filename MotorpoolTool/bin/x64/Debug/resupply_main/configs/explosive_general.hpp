/**
 * Demolitions
 */
class BWI_Resupply_Explosive_M112: BWI_Resupply_CrateBaseSmall4 {
	scope = 2;
	displayName = "M112 Demolition Charge";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_3.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportItems {
		MACRO_ADDITEM(24,DemoCharge_Remote_Mag);
	};
};

class BWI_Resupply_Explosive_M183: BWI_Resupply_Explosive_M112 {
	displayName = "M183 Satchel Charge";

	class TransportItems {
		MACRO_ADDITEM( 8,SatchelCharge_Remote_Mag);
	};
};


/**
 * Improvised Devices
 */
class BWI_Resupply_Explosive_IED_Small: BWI_Resupply_Explosive_M112 {
	displayName = "Small IEDs";

	class TransportItems {
		MACRO_ADDITEM( 4,IEDUrbanSmall_Remote_Mag);
		MACRO_ADDITEM( 4,IEDLandSmall_Remote_Mag);
	};
};

class BWI_Resupply_Explosive_IED_Large: BWI_Resupply_Explosive_M112 {
	displayName = "Large IEDs";

	class TransportItems {
		MACRO_ADDITEM( 2,IEDUrbanBig_Remote_Mag);
		MACRO_ADDITEM( 2,IEDLandBig_Remote_Mag);
	};
};


/**
 * Mines
 */
class BWI_Resupply_Explosive_M6SLAM: BWI_Resupply_Explosive_M112 {
	displayName = "M4A1 SLAM Mine";

	class TransportItems {
		MACRO_ADDITEM(24,SLAMDirectionalMine_Wire_Mag);
	};
};

class BWI_Resupply_Explosive_M26: BWI_Resupply_Explosive_M112 {
	displayName = "M26 APERS Bounding Mine";

	class TransportItems {
		MACRO_ADDITEM(24,APERSBoundingMine_Range_Mag);
	};
};

class BWI_Resupply_Explosive_M15: BWI_Resupply_Explosive_M112 {
	displayName = "M15 Anti-Tank Mine";

	class TransportItems {
		MACRO_ADDITEM( 8,ATMine_Range_Mag);
	};
};

class BWI_Resupply_Explosive_M18A1: BWI_Resupply_Explosive_M112 {
	displayName = "M18A1 Claymore Mine";

	class TransportItems {
		MACRO_ADDITEM(24,ClaymoreDirectionalMine_Remote_Mag);
	};
};

class BWI_Resupply_Explosive_PMR3M: BWI_Resupply_Explosive_M112 {
	displayName = "PMR-3 Tripwire Mine";

	class TransportItems {
		MACRO_ADDITEM(24,APERSTripMine_Wire_Mag);
	};
};

class BWI_Resupply_Explosive_PMR3F: BWI_Resupply_Explosive_M112 {
	displayName = "PMR-3 Tripwire Flare";

	class TransportItems {
		MACRO_ADDITEM(24,ACE_FlareTripMine_Mag);
	};
};