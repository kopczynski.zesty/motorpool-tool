/**
 * M249, M249 PIP
 */
class BWI_Resupply_Box_200Rnd_M855: BWI_Resupply_CrateBaseSmall2 {
	scope = 2;
	displayName = "5.56mm 200Rnd M855";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_1.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,rhsusf_200rnd_556x45_M855_box);
		MACRO_ADDMAGAZINE(10,rhsusf_200rnd_556x45_M855_mixed_box);
	};

	class TransportItems {
		MACRO_ADDITEM(2,ACE_SpareBarrel);
	};
};

class BWI_Resupply_Box_200Rnd_M855A1: BWI_Resupply_Box_200Rnd_M855 {
	displayName = "5.56mm 200Rnd M855A1";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,rhsusf_200Rnd_556x45_box);
		MACRO_ADDMAGAZINE(10,rhsusf_200rnd_556x45_mixed_box);
	};	
};

class BWI_Resupply_Box_200Rnd_M855_SP_W: BWI_Resupply_Box_200Rnd_M855 {
	displayName = "5.56mm 200Rnd M855 (Softpack)";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,rhsusf_200Rnd_556x45_M855_soft_pouch);
		MACRO_ADDMAGAZINE(10,rhsusf_200Rnd_556x45_M855_mixed_soft_pouch);
	};	
};

class BWI_Resupply_Box_200Rnd_M855A1_SP_W: BWI_Resupply_Box_200Rnd_M855 {
	displayName = "5.56mm 200Rnd M855A1 (Softpack)";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,rhsusf_200Rnd_556x45_soft_pouch);
		MACRO_ADDMAGAZINE(10,rhsusf_200Rnd_556x45_mixed_soft_pouch);
	};	
};

class BWI_Resupply_Box_200Rnd_M855_SP_C: BWI_Resupply_Box_200Rnd_M855 {
	displayName = "5.56mm 200Rnd M855 (Softpack)";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,rhsusf_200Rnd_556x45_M855_soft_pouch_coyote);
		MACRO_ADDMAGAZINE(10,rhsusf_200Rnd_556x45_M855_mixed_soft_pouch_coyote);
	};	
};

class BWI_Resupply_Box_200Rnd_M855A1_SP_C: BWI_Resupply_Box_200Rnd_M855 {
	displayName = "5.56mm 200Rnd M855A1 (Softpack)";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,rhsusf_200Rnd_556x45_soft_pouch_coyote);
		MACRO_ADDMAGAZINE(10,rhsusf_200Rnd_556x45_mixed_soft_pouch_coyote);
	};	
};

class BWI_Resupply_Box_200Rnd_M855_SP_U: BWI_Resupply_Box_200Rnd_M855 {
	displayName = "5.56mm 200Rnd M855 (Softpack)";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,rhsusf_200Rnd_556x45_M855_soft_pouch_ucp);
		MACRO_ADDMAGAZINE(10,rhsusf_200Rnd_556x45_M855_mixed_soft_pouch_ucp);
	};	
};

class BWI_Resupply_Box_200Rnd_M855A1_SP_U: BWI_Resupply_Box_200Rnd_M855 {
	displayName = "5.56mm 200Rnd M855A1 (Softpack)";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,rhsusf_200Rnd_556x45_soft_pouch_ucp);
		MACRO_ADDMAGAZINE(10,rhsusf_200Rnd_556x45_mixed_soft_pouch_ucp);
	};	
};


/**
 * M27 IAR
 */
class BWI_Resupply_Box_100Rnd_M855A1_CMAG: BWI_Resupply_Box_200Rnd_M855 {
	displayName = "5.56mm 100Rnd M855A1 CMAG";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(27,rhs_mag_100Rnd_556x45_M855A1_cmag);
		MACRO_ADDMAGAZINE(15,rhs_mag_100Rnd_556x45_M855A1_cmag_mixed);
	};	
};


/**
 * G36KV, G36C
 */
class BWI_Resupply_Mag_100Rnd_M855A1_G36: BWI_Resupply_Box_200Rnd_M855 {
	displayName = "5.56mm 100Rnd M855A1 G36";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(40,rhssaf_100rnd_556x45_EPR_G36);
	};
};


/**
 * MG4
 */
class BWI_Resupply_Box_200Rnd_MG4: BWI_Resupply_Box_200Rnd_M855 {
	displayName = "5.56mm 200Rnd MG4";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ger_signs_1.paa",
 		"\bwi_resupply_main\data\ger_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,BWA3_200Rnd_556x45);
		MACRO_ADDMAGAZINE(10,BWA3_200Rnd_556x45_Tracer);
	};	
};