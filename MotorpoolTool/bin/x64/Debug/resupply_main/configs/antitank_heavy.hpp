/**
 * FGM-148 Javelin
 */
class BWI_Resupply_AT_1Rnd_FGM148: BWI_Resupply_CrateBaseLong1 {
	scope = 2;
	displayName = "FGM-148 Missile";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_2.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 1,rhs_fgm148_magazine_AT);
	};
};