/**
 * M136
 */
class BWI_Resupply_AT_Tube_M136_HEAT: BWI_Resupply_CrateBaseMedium1 {
	scope = 2; 
	displayName = "M136 HEAT Launcher";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_1.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,rhs_weap_M136);
	};
};

class BWI_Resupply_AT_Tube_M136_HEDP: BWI_Resupply_AT_Tube_M136_HEAT {
	displayName = "M136 HEDP Launcher";

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,rhs_weap_M136_hedp);
	};
};

class BWI_Resupply_AT_Tube_M136_HP: BWI_Resupply_AT_Tube_M136_HEAT {
	displayName = "M136 HP Launcher";

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,rhs_weap_M136_hp);
	};
};


/**
 * M72A7
 */
class BWI_Resupply_AT_Tube_M72: BWI_Resupply_AT_Tube_M136_HEAT {
	displayName = "M72A7 Launcher";

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,rhs_weap_m72a7);
	};
};


/**
 * RGW90
 */
class BWI_Resupply_AT_Tube_RGW90: BWI_Resupply_AT_Tube_M136_HEAT {
	displayName = "RGW90 Launcher";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ger_signs_1.paa",
 		"\bwi_resupply_main\data\ger_color_b.paa"
 	};

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,BWA3_RGW90_Loaded);
	};
};


/**
 * NLAW
 */
class BWI_Resupply_AT_Tube_NLAW: BWI_Resupply_AT_Tube_M136_HEAT {
	displayName = "NLAW Launcher";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\gbr_signs_1.paa",
 		"\bwi_resupply_main\data\gbr_color_b.paa"
 	};

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,UK3CB_BAF_NLAW_Launcher);
	};
};


/**
 * ILAW
 */
class BWI_Resupply_AT_Tube_ILAW_HEDP: BWI_Resupply_AT_Tube_NLAW {
	displayName = "ILAW HEDP Launcher";

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,UK3CB_BAF_AT4_CS_AP_Launcher);
	};
};

class BWI_Resupply_AT_Tube_ILAW_HP: BWI_Resupply_AT_Tube_NLAW {
	displayName = "ILAW HP Launcher";

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,UK3CB_BAF_AT4_CS_AT_Launcher);
	};
};


/**
 * RBR-M80
 */
class BWI_Resupply_AT_Tube_M80: BWI_Resupply_AT_Tube_M136_HEAT {
	displayName = "M80 Launcher";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_1.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,rhs_weap_m80);
	};
};


/**
 * RPG-26
 */
class BWI_Resupply_AT_Tube_RPG26: BWI_Resupply_AT_Tube_M136_HEAT {
	displayName = "RPG-26 Launcher";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_1.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,rhs_weap_rpg26);
	};
};

class BWI_Resupply_AT_Tube_RSHG2: BWI_Resupply_AT_Tube_RPG26 {
	displayName = "RShG-2 Launcher";

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,rhs_weap_rshg2);
	};
};

/**
 * RPG-75
 */
class BWI_Resupply_AT_Tube_RPG75: BWI_Resupply_AT_Tube_M136_HEAT {
	displayName = "RPG-75 Launcher";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_1.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportWeapons {
		MACRO_ADDWEAPON( 3,rhs_weap_rpg75);
	};
};