/**
 * M14-EBR
 */
class BWI_Resupply_Mag_20Rnd_M118: BWI_Resupply_CrateBaseSmall4 {
	scope = 2;
	displayName = "7.62mm 20Rnd M118";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_1.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsusf_20Rnd_762x51_m62_Mag);
		MACRO_ADDMAGAZINE(10,rhsusf_20Rnd_762x51_m118_special_Mag);
	};
};

class BWI_Resupply_Mag_20Rnd_M993: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 20Rnd M993 AP";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsusf_20Rnd_762x51_m993_Mag);
	};	
};

/**
 * M14, M21
 */
class BWI_Resupply_Mag_20Rnd_M14: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 20Rnd M14";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,UK3CB_20Rnd_762x51_B_M14);
		MACRO_ADDMAGAZINE(10,UK3CB_20Rnd_762x51_T_M14);
	};
};


/**
 * XM2010
 */
class BWI_Resupply_Mag_5Rnd_Mk248: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = ".300WM 5Rnd Mk248";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(60,rhsusf_5Rnd_300winmag_xm2010);
	};	
};


/**
 * M24 SWS
 */
class BWI_Resupply_Mag_5Rnd_M118: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 5Rnd M118";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(40,rhsusf_5Rnd_762x51_m62_Mag);
		MACRO_ADDMAGAZINE(20,rhsusf_5Rnd_762x51_m118_special_Mag);
	};
};

class BWI_Resupply_Mag_5Rnd_M993: BWI_Resupply_Mag_5Rnd_M118 {
	displayName = "7.62mm 5Rnd M993 AP";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(40,rhsusf_5Rnd_762x51_m993_Mag);
	};	
};


/**
 * M40A5
 */
class BWI_Resupply_Mag_10Rnd_M118: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 10Rnd M118";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsusf_10Rnd_762x51_m62_Mag);
		MACRO_ADDMAGAZINE(10,rhsusf_10Rnd_762x51_m118_special_Mag);
	};
};

class BWI_Resupply_Mag_10Rnd_M993: BWI_Resupply_Mag_10Rnd_M118 {
	displayName = "7.62mm 10Rnd M993 AP";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsusf_10Rnd_762x51_m993_Mag);
	};	
};


/**
 * SR25/Mk11 Mod 0
 */
class BWI_Resupply_Mag_20Rnd_M118_SR25: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 20Rnd M118 SR25";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsusf_20Rnd_762x51_SR25_m62_Mag);
		MACRO_ADDMAGAZINE(10,rhsusf_20Rnd_762x51_SR25_m118_special_Mag);
	};
};

class BWI_Resupply_Mag_20Rnd_M993_SR25: BWI_Resupply_Mag_10Rnd_M118 {
	displayName = "7.62mm 20Rnd M993 AP SR25";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsusf_20Rnd_762x51_SR25_m993_Mag);
	};	
};


/**
 * G27
 */
class BWI_Resupply_Mag_20Rnd_G27: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 20Rnd G27";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ger_signs_1.paa",
 		"\bwi_resupply_main\data\ger_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,BWA3_20Rnd_762x51_G28);
		MACRO_ADDMAGAZINE(10,BWA3_20Rnd_762x51_G28_Tracer);
		MACRO_ADDMAGAZINE( 4,BWA3_20Rnd_762x51_G28_Tracer_Dim);
	};
};

class BWI_Resupply_Mag_20Rnd_G27_AP: BWI_Resupply_Mag_20Rnd_G27 {
	displayName = "7.62mm 20Rnd G27 AP";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,BWA3_20Rnd_762x51_G28_AP);
	};	
};


/**
 * G28
 */
class BWI_Resupply_Mag_10Rnd_G28: BWI_Resupply_Mag_20Rnd_G27 {
	displayName = "7.62mm 10Rnd G28";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,BWA3_10Rnd_762x51_G28);
		MACRO_ADDMAGAZINE(10,BWA3_10Rnd_762x51_G28_Tracer);
		MACRO_ADDMAGAZINE( 4,BWA3_10Rnd_762x51_G28_Tracer_Dim);
	};
};

class BWI_Resupply_Mag_10Rnd_G28_AP: BWI_Resupply_Mag_20Rnd_G27 {
	displayName = "7.62mm 10Rnd G28 AP";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,BWA3_10Rnd_762x51_G28_AP);
	};	
};


/**
 * L129A1
 */
class BWI_Resupply_Mag_20Rnd_L42A1: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 20Rnd L42A1";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\gbr_signs_1.paa",
 		"\bwi_resupply_main\data\gbr_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,UK3CB_BAF_762_L42A1_20Rnd);
		MACRO_ADDMAGAZINE(10,UK3CB_BAF_762_L42A1_20Rnd_T);
	};
};


/**
 * L118A1
 */
class BWI_Resupply_Mag_10Rnd_L42A1: BWI_Resupply_Mag_20Rnd_L42A1 {
	displayName = "7.62mm 10Rnd L42A1";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,UK3CB_BAF_762_L42A1_10Rnd);
		MACRO_ADDMAGAZINE(10,UK3CB_BAF_762_L42A1_10Rnd_T);
	};
};


/**
 * M70B
 */
class BWI_Resupply_Mag_30Rnd_M67: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 30Rnd M67";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_1.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(12,rhssaf_30Rnd_762x39mm_M67);
		MACRO_ADDMAGAZINE( 8,rhssaf_30Rnd_762x39mm_M78_tracer);
	};
};


/**
 * SVDM, SVDS
 */
class BWI_Resupply_Mag_10Rnd_7N14: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 10Rnd 7N14";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_1.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(60,rhs_10Rnd_762x54mmR_7N14);
	};
};

class BWI_Resupply_Mag_10Rnd_7N1: BWI_Resupply_Mag_10Rnd_7N14 {
	displayName = "7.62mm 10Rnd 7N1";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(60,rhs_10Rnd_762x54mmR_7N1);
	};
};


/**
 * AK-103, AK-104, AKM
 */
class BWI_Resupply_Mag_30Rnd_57N231: BWI_Resupply_Mag_10Rnd_7N14 {
	displayName = "7.62mm 30Rnd 57-N-231";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(12,rhs_30Rnd_762x39mm);
		MACRO_ADDMAGAZINE( 8,rhs_30Rnd_762x39mm_tracer);
	};
};

class BWI_Resupply_Mag_30Rnd_57N231P: BWI_Resupply_Mag_30Rnd_57N231 {
	displayName = "7.62mm 30Rnd 57-N-231P SD";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhs_30Rnd_762x39mm_U);
	};
};


/**
 * AK-103, AK-104, AKM (Bakelite)
 */
class BWI_Resupply_Mag_30Rnd_57N231_Bakelite: BWI_Resupply_Mag_10Rnd_7N14 {
	displayName = "7.62mm 30Rnd 57-N-231 Bakelite";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(12,rhs_30Rnd_762x39mm_bakelite);
		MACRO_ADDMAGAZINE( 8,rhs_30Rnd_762x39mm_bakelite_tracer);
	};
};

class BWI_Resupply_Mag_30Rnd_57N231P_Bakelite: BWI_Resupply_Mag_30Rnd_57N231 {
	displayName = "7.62mm 30Rnd 57-N-231P SD Bakelite";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhs_30Rnd_762x39mm_bakelite_U);
	};
};


/**
 * AK-103, AK-104, AKM (Polymer)
 */
class BWI_Resupply_Mag_30Rnd_57N231_Polymer: BWI_Resupply_Mag_10Rnd_7N14 {
	displayName = "7.62mm 30Rnd 57-N-231 Polymer";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(12,rhs_30Rnd_762x39mm_polymer);
		MACRO_ADDMAGAZINE( 8,rhs_30Rnd_762x39mm_polymer_tracer);
	};
};

class BWI_Resupply_Mag_30Rnd_57N231P_Polymer: BWI_Resupply_Mag_30Rnd_57N231 {
	displayName = "7.62mm 30Rnd 57-N-231P SD Polymer";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhs_30Rnd_762x39mm_polymer_U);
	};
};


/**
 * Sa vz. 58
 */
class BWI_Resupply_Mag_30Rnd_SaVz58: BWI_Resupply_Mag_10Rnd_7N14 {
	displayName = "7.62mm 30Rnd Sa vz. 58";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(12,rhs_30Rnd_762x39mm_Savz58);
		MACRO_ADDMAGAZINE( 8,rhs_30Rnd_762x39mm_Savz58_tracer);
	};
};


/**
 * HK417
 */
class BWI_Resupply_Mag_20Rnd_M118LR: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 20Rnd HK417";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,ACE_20Rnd_762x51_M118LR_Mag);
		MACRO_ADDMAGAZINE(10,ACE_20Rnd_762x51_Mk316_Mod_0_Mag);
	};
};


/**
 * FN FAL
 */
class BWI_Resupply_Mag_20Rnd_FNFAL: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 20Rnd FN FAL";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,UK3CB_FNFAL_762_20Rnd);
		MACRO_ADDMAGAZINE(10,UK3CB_FNFAL_20rnd_762x51_YT);
	};
};


/**
 * SCAR-H CQC, SCAR-H STD, SCAR-H LB
 */
class BWI_Resupply_Mag_20Rnd_M80_SCAH_H: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 20Rnd M80 SCAR-H";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhs_mag_20Rnd_SCAR_762x51_m80_ball_bk);
		MACRO_ADDMAGAZINE(10,rhs_mag_20Rnd_SCAR_762x51_m62_tracer_bk);
	};
};
class BWI_Resupply_Mag_20Rnd_M80_SCAH_H_T: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 20Rnd M80 SCAR-H";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhs_mag_20Rnd_SCAR_762x51_m80_ball);
		MACRO_ADDMAGAZINE(10,rhs_mag_20Rnd_SCAR_762x51_m62_tracer);
	};
};

class BWI_Resupply_Mag_20Rnd_Mk316_SCAH_H: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 20Rnd Mk316 SCAR-H";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhs_mag_20Rnd_SCAR_762x51_m118_special_bk);
		MACRO_ADDMAGAZINE(10,rhs_mag_20Rnd_SCAR_762x51_mk316_special_bk);
	};
};
class BWI_Resupply_Mag_20Rnd_Mk316_SCAH_H_T: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 20Rnd Mk316 SCAR-H";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhs_mag_20Rnd_SCAR_762x51_m118_special);
		MACRO_ADDMAGAZINE(10,rhs_mag_20Rnd_SCAR_762x51_mk316_special);
	};
};


/**
 * Mosin Nagant
 */
class BWI_Resupply_Mag_5Rnd_Mosin: BWI_Resupply_Mag_20Rnd_M118 {
	displayName = "7.62mm 5Rnd Mosin";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_1.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};
	 
	class TransportMagazines {
		MACRO_ADDMAGAZINE(60,rhsgref_5Rnd_762x54_m38);
	};
};