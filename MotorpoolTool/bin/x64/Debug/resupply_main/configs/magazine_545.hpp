/**
 * AK-74, AK-105
 */
class BWI_Resupply_Mag_30Rnd_7N10: BWI_Resupply_CrateBaseSmall1 {
	scope = 2;
	displayName = "5.45mm 30Rnd 7N10";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_1.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhs_30Rnd_545x39_7N10_AK);
		MACRO_ADDMAGAZINE(10,rhs_30Rnd_545x39_7N22_AK);
	};
};

class BWI_Resupply_Mag_30Rnd_7U1: BWI_Resupply_Mag_30Rnd_7N10 {
	displayName = "5.45mm 30Rnd 7U1 SD";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_30Rnd_545x39_7U1_AK);
	};
};


/**
 * AK-74, AK-105 (Plum)
 */
class BWI_Resupply_Mag_30Rnd_7N10_Plum: BWI_Resupply_Mag_30Rnd_7N10 {
	displayName = "5.45mm 30Rnd 7N10 Plum";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(14,rhs_30Rnd_545x39_7N10_plum_AK);
		MACRO_ADDMAGAZINE(8,rhs_30Rnd_545x39_7N22_plum_AK);
		MACRO_ADDMAGAZINE(8,rhs_30Rnd_545x39_AK_plum_green);
	};
};


/**
 * AK-74, AK-105 (Camo)
 */
class BWI_Resupply_Mag_30Rnd_7N10_Camo: BWI_Resupply_Mag_30Rnd_7N10 {
	displayName = "5.45mm 30Rnd 7N10 Camo";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhs_30Rnd_545x39_7N10_camo_AK);
		MACRO_ADDMAGAZINE(10,rhs_30Rnd_545x39_7N22_camo_AK);
	};
};


/**
 * AK-74, AK-105 (Desert)
 */
class BWI_Resupply_Mag_30Rnd_7N10_Desert: BWI_Resupply_Mag_30Rnd_7N10 {
	displayName = "5.45mm 30Rnd 7N10 Desert";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhs_30Rnd_545x39_7N10_desert_AK);
		MACRO_ADDMAGAZINE(10,rhs_30Rnd_545x39_7N22_desert_AK);
	};
};


/**
 * AK-74, AK-105 (Bakelite)
 */
class BWI_Resupply_Mag_30Rnd_7N6_Bakelite: BWI_Resupply_Mag_30Rnd_7N10 {
	displayName = "5.45mm 30Rnd 7N6 Bakelite";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(14,rhs_30Rnd_545x39_7N6_AK);
		MACRO_ADDMAGAZINE(8,rhs_30Rnd_545x39_7N6M_AK);
		MACRO_ADDMAGAZINE(8,rhs_30Rnd_545x39_AK_green);
	};
};