/**
 * M203, M320
 */
class BWI_Resupply_Gren_40mm_WF_M585: BWI_Resupply_CrateBaseMedium2 {
	scope = 2;
	displayName = "40mm M585 White Flare";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_3.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_M585_white);
	};	
};

class BWI_Resupply_Gren_40mm_CF_M661: BWI_Resupply_Gren_40mm_WF_M585 {
	displayName = "40mm M661 Colored Flare";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(15,rhs_mag_m661_green);
		MACRO_ADDMAGAZINE(15,rhs_mag_m662_red);
	};	
};


/**
 * Generic
 */
class BWI_Resupply_Gren_40mm_WF_Generic: BWI_Resupply_Gren_40mm_WF_M585 {
	displayName = "40mm White Flare";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,UGL_FlareWhite_F);
	};	
};

class BWI_Resupply_Gren_40mm_CF_Generic: BWI_Resupply_Gren_40mm_WF_Generic {
	displayName = "40mm Colored Flare";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(15,UGL_FlareGreen_F);
		MACRO_ADDMAGAZINE(15,UGL_FlareRed_F);
	};	
};


/**
 * L123A2
 */
class BWI_Resupply_Gren_40mm_WF_L123A2: BWI_Resupply_Gren_40mm_WF_M585 {
	displayName = "40mm L123A2 White Flare";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\gbr_signs_3.paa",
 		"\bwi_resupply_main\data\gbr_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,UK3CB_BAF_UGL_FlareWhite_F);
	};	
};

class BWI_Resupply_Gren_40mm_CF_L123A2: BWI_Resupply_Gren_40mm_WF_L123A2 {
	displayName = "40mm L123A2 Colored Flare";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(15,UK3CB_BAF_UGL_FlareGreen_F);
		MACRO_ADDMAGAZINE(15,UK3CB_BAF_UGL_FlareRed_F);
	};	
};


/**
 * VG-40OP
 */
class BWI_Resupply_Gren_40mm_WF_VG40OP: BWI_Resupply_Gren_40mm_WF_M585 {
	displayName = "40mm VG-40OP White Flare";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_3.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_VG40OP_white);
	};	
};

class BWI_Resupply_Gren_40mm_CF_VG40OP: BWI_Resupply_Gren_40mm_WF_VG40OP {
	displayName = "40mm VG-40OP Colored Flare";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(15,rhs_VG40OP_green);
		MACRO_ADDMAGAZINE(15,rhs_VG40OP_red);
	};	
};