/**
 * ANM8/M18 (US)
 */
class BWI_Resupply_Gren_Hand_WS_ANM8: BWI_Resupply_CrateBaseSmall4 {
	scope = 2;
	displayName = "AN-M8 White Smoke";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_2.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_an_m8hc);
	};	
};

class BWI_Resupply_Gren_Hand_CS_M18: BWI_Resupply_Gren_Hand_WS_ANM8 {
	displayName = "M18 Colored Smoke";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(14,rhs_mag_m18_green);
		MACRO_ADDMAGAZINE(10,rhs_mag_m18_red);
		MACRO_ADDMAGAZINE( 6,rhs_mag_m18_purple);
	};	
};


/**
 * DM25/DM32 (German)
 */
class BWI_Resupply_Gren_Hand_WS_DM25: BWI_Resupply_Gren_Hand_WS_ANM8 {
	displayName = "DM25 White Smoke";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ger_signs_2.paa",
 		"\bwi_resupply_main\data\ger_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,BWA3_DM25);
	};	
};

class BWI_Resupply_Gren_Hand_CS_DM32: BWI_Resupply_Gren_Hand_WS_DM25 {
	displayName = "DM32A2 Colored Smoke";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(14,BWA3_DM32_Green);
		MACRO_ADDMAGAZINE(10,BWA3_DM32_Red);
		MACRO_ADDMAGAZINE( 6,BWA3_DM32_Purple);
	};	
};


/**
 * L50A1/L68 (UK)
 */
class BWI_Resupply_Gren_Hand_WS_L50A1: BWI_Resupply_Gren_Hand_WS_ANM8 {
	displayName = "L50A1 White Smoke";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\gbr_signs_2.paa",
 		"\bwi_resupply_main\data\gbr_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,UK3CB_BAF_SmokeShell);
	};	
};

class BWI_Resupply_Gren_Hand_CS_L68: BWI_Resupply_Gren_Hand_WS_L50A1 {
	displayName = "L68 Colored Smoke";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(14,UK3CB_BAF_SmokeShellGreen);
		MACRO_ADDMAGAZINE(10,UK3CB_BAF_SmokeShellRed);
		MACRO_ADDMAGAZINE( 6,UK3CB_BAF_SmokeShellPurple);
	};	
};


/**
 * M83 (Serbian)
 */
class BWI_Resupply_Gren_Hand_WS_M83: BWI_Resupply_Gren_Hand_WS_ANM8 {
	displayName = "M83 White Smoke";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_2.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhssaf_mag_brd_m83_white);
	};	
};

class BWI_Resupply_Gren_Hand_CS_M83: BWI_Resupply_Gren_Hand_WS_M83 {
	displayName = "M83 Colored Smoke";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(14,rhssaf_mag_brd_m83_green);
		MACRO_ADDMAGAZINE(10,rhssaf_mag_brd_m83_red);
		MACRO_ADDMAGAZINE( 6,rhssaf_mag_brd_m83_blue);
	};	
};


/**
 * RDG2/NSPD (Russian)
 */
class BWI_Resupply_Gren_Hand_WS_RDG2: BWI_Resupply_Gren_Hand_WS_ANM8 {
	displayName = "RDG2 White/Black Smoke";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_2.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(16,rhs_mag_rdg2_white);
		MACRO_ADDMAGAZINE(14,rhs_mag_rdg2_black);
	};	
};

class BWI_Resupply_Gren_Hand_CS_NSPD: BWI_Resupply_Gren_Hand_WS_RDG2 {
	displayName = "NSPD Colored Smoke";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_nspd);
	};	
};

class BWI_Resupply_Gren_Hand_CS_NSPN: BWI_Resupply_Gren_Hand_WS_RDG2 {
	displayName = "NSPN Colored Signals";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(10,rhs_mag_nspn_green);
		MACRO_ADDMAGAZINE(10,rhs_mag_nspn_yellow);
		MACRO_ADDMAGAZINE(10,rhs_mag_nspn_red);
	};	
};
