/**
 * M203, M320
 */
class BWI_Resupply_Gren_40mm_HE_M441: BWI_Resupply_CrateBaseMedium2 {
	scope = 2; 
	displayName = "40mm M441 High Explosive";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_1.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_M441_HE);
	};
};


/**
 * Generic
 */
class BWI_Resupply_Gren_40mm_HE_Generic: BWI_Resupply_Gren_40mm_HE_M441 {
	displayName = "40mm High Explosive";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,1Rnd_HE_Grenade_shell);
	};
};


/**
 * L123A2
 */
class BWI_Resupply_Gren_40mm_HE_L123A2: BWI_Resupply_Gren_40mm_HE_M441 {
	displayName = "40mm L123A2 High Explosive";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\gbr_signs_1.paa",
 		"\bwi_resupply_main\data\gbr_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,UK3CB_BAF_1Rnd_HE_Grenade_Shell);
	};
};


/**
 * VOG-25
 */
class BWI_Resupply_Gren_40mm_HE_VOG25: BWI_Resupply_Gren_40mm_HE_M441 {
	displayName = "40mm VOG-25 High Explosive";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_1.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_VOG25);
	};
};

class BWI_Resupply_Gren_40mm_HE_VOG25TB: BWI_Resupply_Gren_40mm_HE_VOG25 {
	displayName = "40mm VG-40TB Thermobaric";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhs_VG40TB);
	};
};