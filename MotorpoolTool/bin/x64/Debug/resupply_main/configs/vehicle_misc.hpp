/**
 * BAF Vehicle Ammunition
 */
class BWI_Resupply_Veh_200Rnd_GPMG: BWI_Resupply_CrateBaseMedium1 {
	scope = 2;
	displayName = "7.62mm 200Rnd GPMG";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\gbr_signs_2.paa",
 		"\bwi_resupply_main\data\gbr_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(12,UK3CB_BAF_762_200Rnd_T);
	};
};

class BWI_Resupply_Veh_100Rnd_HMG: BWI_Resupply_Veh_200Rnd_GPMG {
	displayName = ".50cal 100Rnd HMG";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(6,UK3CB_BAF_127_100Rnd);
	};
};

class BWI_Resupply_Veh_32Rnd_GMG: BWI_Resupply_Veh_200Rnd_GPMG {
	displayName = "40mm 32Rnd GMG";

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 4,UK3CB_BAF_32Rnd_40mm_G_Box);
	};
};

class BWI_Resupply_Veh_1Rnd_HAT: BWI_Resupply_Veh_200Rnd_GPMG {
	displayName = "Milan Missile";

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 2,UK3CB_BAF_1Rnd_Milan);
	};
};

class BWI_Resupply_Veh_6Rnd_L2A1_APDS: BWI_Resupply_Veh_200Rnd_GPMG {
	displayName = "30mm 6Rnd L2A1 APDS";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(15,UK3CB_BAF_6Rnd_30mm_L21A1_APDS);
	};
};

class BWI_Resupply_Veh_6Rnd_L2A1_HE: BWI_Resupply_Veh_200Rnd_GPMG {
	displayName = "30mm 6Rnd L2A1 HE";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(15,UK3CB_BAF_6Rnd_30mm_L21A1_HE);
	};
};