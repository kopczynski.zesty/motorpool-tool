/**
 * VSS Vintorez
 */
class BWI_Resupply_Mag_20Rnd_SP5: BWI_Resupply_CrateBaseSmall2 {
	scope = 2;
	displayName = "9mm 20Rnd SP-5";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_2.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(50,rhs_20rnd_9x39mm_SP5);
	};
};

class BWI_Resupply_Mag_20Rnd_SP6: BWI_Resupply_Mag_20Rnd_SP5 {
	displayName = "9mm 20Rnd SP-6 AP";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(50,rhs_20rnd_9x39mm_SP6);
	};
};