/**
 * MP7A2
 */
class BWI_Resupply_Mag_40Rnd_MP7SX: BWI_Resupply_CrateBaseSmall2 {
	scope = 2;
	displayName = "4.6mm 40Rnd MP7 SX";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_3.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(14,rhsusf_mag_40Rnd_46x30_FMJ);
		MACRO_ADDMAGAZINE(10,rhsusf_mag_40Rnd_46x30_AP);
		MACRO_ADDMAGAZINE( 6,rhsusf_mag_40Rnd_46x30_JHP);
	};
};


/**
 * L115A3
 */
class BWI_Resupply_Mag_5Rnd_338_LM: BWI_Resupply_Mag_40Rnd_MP7SX {
	displayName = ".338 5Rnd Lapua Magnum";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\gbr_signs_3.paa",
 		"\bwi_resupply_main\data\gbr_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(40,UK3CB_BAF_338_5Rnd);
		MACRO_ADDMAGAZINE(20,UK3CB_BAF_338_5Rnd_Tracer);
	};
};


/**
 * M76
 */
class BWI_Resupply_Mag_10Rnd_M70: BWI_Resupply_Mag_40Rnd_MP7SX {
	displayName = "7.92mm 10Rnd M70";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_3.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsgref_10Rnd_792x57_m76);
		MACRO_ADDMAGAZINE(10,rhssaf_10Rnd_792x57_m76_tracer);
	};
};


/**
 * T-5000
 */
class BWI_Resupply_Mag_5Rnd_338_T5000: BWI_Resupply_Mag_40Rnd_MP7SX {
	displayName = ".338 5Rnd T-5000";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_3.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(60,rhs_5Rnd_338lapua_t5000);
	};
};


/**
 * Lee Enfield
 */
class BWI_Resupply_Mag_10Rnd_Enfield: BWI_Resupply_Mag_40Rnd_MP7SX {
	displayName = "7.7mm 10Rnd Enfield";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(32,UK3CB_Enfield_Mag);
	};
};


/**
 * M3A1 Grease Gun
 */
class BWI_Resupply_Mag_30Rnd_M3A1: BWI_Resupply_Mag_40Rnd_MP7SX {
	displayName = ".45ACP 30Rnd M3A1";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhsgref_30rnd_1143x23_M1911B_SMG);
		MACRO_ADDMAGAZINE(20,rhsgref_30rnd_1143x23_M1T_SMG);
	};
};


/**
 * M1 Garand
 */
class BWI_Resupply_Mag_8Rnd_M1Garand: BWI_Resupply_Mag_40Rnd_MP7SX {
	displayName = ".30-06 8Rnd M1 Garand";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(110,rhsgref_8Rnd_762x63_M2B_M1rifle);
		MACRO_ADDMAGAZINE(70,rhsgref_8Rnd_762x63_Tracer_M1T_M1rifle);
	};
};


/**
 * MG42
 */
class BWI_Resupply_Mag_50Rnd_MG42: BWI_Resupply_Mag_40Rnd_MP7SX {
	displayName = "7.92mm 50Rnd Assault Drum";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsgref_50Rnd_792x57_SmE_drum);
		MACRO_ADDMAGAZINE(12,rhsgref_50Rnd_792x57_SmK_alltracers_drum);
	};
};

class BWI_Resupply_Mag_296Rnd_MG42: BWI_Resupply_Mag_40Rnd_MP7SX {
	displayName = "7.92mm 296Rnd Belt";

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 9,rhsgref_296Rnd_792x57_SmE_belt);
		MACRO_ADDMAGAZINE( 6,rhsgref_296Rnd_792x57_SmK_alltracers_belt);
	};
};