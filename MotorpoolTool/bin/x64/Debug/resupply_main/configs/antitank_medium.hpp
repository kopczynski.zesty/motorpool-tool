/**
 * M3 MAAWS
 */
class BWI_Resupply_AT_1Rnd_FFV751: BWI_Resupply_CrateBaseMedium1 {
	scope = 2;
	displayName = "FFV751 HEAT Rounds";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_3.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 6,rhs_mag_maaws_HEAT);
	};
};

class BWI_Resupply_AT_1Rnd_FFV502: BWI_Resupply_AT_1Rnd_FFV751 {
	displayName = "FFV502 HEDP Rounds";

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 6,rhs_mag_maaws_HEDP);
	};
};


/**
 * SMAW
 */
class BWI_Resupply_AT_1Rnd_Mk6HEAA: BWI_Resupply_AT_1Rnd_FFV751 {
	displayName = "Mk.6 HEAA Rounds";

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 6,rhs_mag_smaw_HEAA);
		MACRO_ADDMAGAZINE( 1,rhs_mag_smaw_SR);
	};
};

class BWI_Resupply_AT_1Rnd_Mk3HEDP: BWI_Resupply_AT_1Rnd_FFV751 {
	displayName = "Mk.3 HEDP Rounds";

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 6,rhs_mag_smaw_HEDP);
		MACRO_ADDMAGAZINE( 1,rhs_mag_smaw_SR);
	};
};


/**
 * Panzerfaust 3
 */
class BWI_Resupply_AT_1Rnd_PzF3IT: BWI_Resupply_AT_1Rnd_FFV751 {
	displayName = "Panzerfaust 3 IT Rounds";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ger_signs_3.paa",
 		"\bwi_resupply_main\data\ger_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 6,BWA3_PzF3_Tandem);
	};
};

class BWI_Resupply_AT_1Rnd_PzF3BF: BWI_Resupply_AT_1Rnd_PzF3IT {
	displayName = "Bunkerfaust DM32 Rounds";

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 6,BWA3_PzF3_DM32);
	};
};


/**
 * RPG-7
 */
class BWI_Resupply_AT_1Rnd_PG7VR: BWI_Resupply_AT_1Rnd_FFV751 {
	displayName = "PG-7VR T-HEAT Rounds"; // Modern HEAT

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_3.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 6,rhs_rpg7_PG7VR_mag);
	};
};

class BWI_Resupply_AT_1Rnd_PG7VL: BWI_Resupply_AT_1Rnd_PG7VR {
	displayName = "PG-7VL I-HEAT Rounds"; // Improved HEAT

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 6,rhs_rpg7_PG7VL_mag);
	};
};

class BWI_Resupply_AT_1Rnd_PG7V: BWI_Resupply_AT_1Rnd_PG7VR {
	displayName = "PG-7V HEAT Rounds"; // Basic HEAT

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 6,rhs_rpg7_PG7V_mag);
	};
};

class BWI_Resupply_AT_1Rnd_OG7V: BWI_Resupply_AT_1Rnd_PG7VR {
	displayName = "OG-7V Frag Rounds"; // Basic HEAT

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 6,rhs_rpg7_OG7V_mag);
	};
};

class BWI_Resupply_AT_1Rnd_TBG7V: BWI_Resupply_AT_1Rnd_PG7VR {
	displayName = "TBG-7V Thermobaric Rounds"; // Basic HEAT

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 6,rhs_rpg7_TBG7V_mag);
	};
};