/**
 * M4, M16
 */
class BWI_Resupply_Mag_30Rnd_M855: BWI_Resupply_CrateBaseSmall1 {
	scope = 2;
	displayName = "5.56mm 30Rnd M855";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_1.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(36,rhs_mag_30Rnd_556x45_M855_Stanag);
		MACRO_ADDMAGAZINE(24,rhs_mag_30Rnd_556x45_M855_Stanag_Tracer_Red);
	};
};

class BWI_Resupply_Mag_30Rnd_M855A1: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd M855A1";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(36,rhs_mag_30Rnd_556x45_M855A1_Stanag);
		MACRO_ADDMAGAZINE(24,rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red);
	};	
};

class BWI_Resupply_Mag_30Rnd_Mk262: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd Mk262";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_30Rnd_556x45_Mk262_Stanag);
	};	
};

class BWI_Resupply_Mag_30Rnd_Mk318: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd Mk318";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_30Rnd_556x45_Mk318_Stanag);
	};	
};


/**
 * M4, M16 (Special Forces)
 */
class BWI_Resupply_Mag_30Rnd_M855_PMAG: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd M855 PMAG";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(18,rhs_mag_30Rnd_556x45_M855_PMAG);
		MACRO_ADDMAGAZINE(12,rhs_mag_30Rnd_556x45_M855_PMAG_Tracer_Red);
	};	
};

class BWI_Resupply_Mag_30Rnd_M855A1_PMAG: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd M855A1 PMAG";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(18,rhs_mag_30Rnd_556x45_M855A1_PMAG);
		MACRO_ADDMAGAZINE(12,rhs_mag_30Rnd_556x45_M855A1_PMAG_Tracer_Red);
	};	
};

class BWI_Resupply_Mag_30Rnd_M855_PMAG_T: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd M855 PMAG Tan";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(18,rhs_mag_30Rnd_556x45_M855_PMAG_Tan);
		MACRO_ADDMAGAZINE(12,rhs_mag_30Rnd_556x45_M855_PMAG_Tan_Tracer_Red);
	};	
};

class BWI_Resupply_Mag_30Rnd_M855A1_PMAG_T: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd M855A1 PMAG Tan";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(18,rhs_mag_30Rnd_556x45_M855A1_PMAG_Tan);
		MACRO_ADDMAGAZINE(12,rhs_mag_30Rnd_556x45_M855A1_PMAG_Tan_Tracer_Red);
	};	
};

class BWI_Resupply_Mag_30Rnd_Mk262_PMAG: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd Mk262 PMAG";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_30Rnd_556x45_Mk262_PMAG);
	};	
};

class BWI_Resupply_Mag_30Rnd_Mk262_PMAG_T: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd Mk262 PMAG Tan";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_30Rnd_556x45_Mk262_PMAG_Tan);
	};	
};

class BWI_Resupply_Mag_30Rnd_Mk318_PMAG: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd Mk318 PMAG";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_30Rnd_556x45_Mk318_PMAG);
	};	
};

class BWI_Resupply_Mag_30Rnd_Mk318_PMAG_T: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd Mk318 PMAG Tan";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhs_mag_30Rnd_556x45_Mk318_PMAG_Tan);
	};	
};


/**
 * G36A1+, G36KA1+, G38
 */
class BWI_Resupply_Mag_30Rnd_DM11: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd DM11";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ger_signs_1.paa",
 		"\bwi_resupply_main\data\ger_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(36,BWA3_30Rnd_556x45_G36);
		MACRO_ADDMAGAZINE(24,BWA3_30Rnd_556x45_G36_Tracer);
	};
};

class BWI_Resupply_Mag_30Rnd_DM11_IR: BWI_Resupply_Mag_30Rnd_DM11 {
	displayName = "5.56mm 30Rnd DM11 IR";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,BWA3_30Rnd_556x45_G36_Tracer_Dim);
	};	
};

class BWI_Resupply_Mag_30Rnd_DM31: BWI_Resupply_Mag_30Rnd_DM11 {
	displayName = "5.56mm 30Rnd DM31 AP";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,BWA3_30Rnd_556x45_G36_AP);
	};	
};


/**
 * G36KV, G36C
 */
class BWI_Resupply_Mag_30Rnd_M855A1_G36: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd M855A1 G36";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_1.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(36,rhssaf_30rnd_556x45_EPR_G36);
		MACRO_ADDMAGAZINE(24,rhssaf_30rnd_556x45_Tracers_G36);
	};
};


/**
 * M21
 */
class BWI_Resupply_Mag_30Rnd_M855_M21: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd M855 M21";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_1.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(36,rhsgref_30rnd_556x45_m21);
		MACRO_ADDMAGAZINE(24,rhsgref_30rnd_556x45_m21_t);
	};
};


/**
 * VHS-D2, VHS-K2
 */
class BWI_Resupply_Mag_30Rnd_M855_VHS: BWI_Resupply_Mag_30Rnd_M855 {
	displayName = "5.56mm 30Rnd M855 VHS-D2";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_1.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(36,rhsgref_30rnd_556x45_vhs2);
		MACRO_ADDMAGAZINE(24,rhsgref_30rnd_556x45_vhs2_t);
	};
};