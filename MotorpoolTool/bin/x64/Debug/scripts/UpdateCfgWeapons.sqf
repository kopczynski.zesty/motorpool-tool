_newLine = toString [0x0D, 0x0A];
_configs = "getNumber (_x >> 'scope') >= 1" configClasses (configFile >> "CfgWeapons") apply
{
 _itemType = (configName _x) call BIS_fnc_itemType;

 _newLine
 + configName(_x) + ";"
 + str([_x, true] call BIS_fnc_returnParents) + ";"
 + str _itemType + ";"
 + getText (_x >> "displayName") + ";"
 + getText (_x >> "descriptionShort") + ";"
 + str(getArray (_x >> "magazines")) + ";"
 + str(getArray (_x >> "magazineWell")) + ";"
 + str(getNumber (_x >> "magazineReloadTime")) + ";"
 + str(getNumber (_x >> "reloadTime")) + ";"
 + str(getNumber (_x >> "canLock")) + ";"
 + str(getNumber (_x >> "weaponLockSystem")) + ";"
 + str(getNumber((_x >> "ItemInfo") >> "mass")) + ";"
 + getText((_x >> "ItemInfo") >> "containerClass") + ";";
};
copyToClipBoard str _configs;