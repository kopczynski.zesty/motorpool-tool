_newLine = toString [0x0D, 0x0A];
_configs = "getNumber (_x >> 'scope') >= 0 &&
(configName _x isKindOf 'Car'
|| configName _x isKindOf 'Air'
|| configName _x isKindOf 'Tank'
|| configName _x isKindOf 'Ship'
|| configName _x isKindOf 'NATO_Box_Base'
|| configName _x isKindOf 'ContainerSupply'
|| configName _x isKindOf 'Bag_Base'
|| configName _x isKindof 'Slingload_base_F'
|| configName _x isKindof 'ThingX'
) " configClasses (configFile >> "CfgVehicles") apply
{
 _crewCount = [configName (_x),false] call BIS_fnc_crewCount;
 _passengerCount = [configName (_x),true] call BIS_fnc_crewCount;
 _passengerCount = _passengerCount - _crewCount;
 _turrets = [];
 _turretConfigs = configProperties[_x >> "Turrets"];
 {
  if (getText (_x >> "gun") != "") then
  {
  _turrets pushBack configName(_x);
  _turrets pushBack getNumber(_x >> "primaryGunner");
  _turrets pushBack ((getArray (_x >> "weapons")) + (getArray (_x >> "magazines")));
  };
 } forEach _turretConfigs;

 _newLine
 + configName(_x) + ";"
 + str([_x, true] call BIS_fnc_returnParents) + ";"
 + getText (_x >> "displayName") + ";"
 + str(getNumber (_x >> "ace_cargo_hasCargo")) + ";"
 + str(getNumber (_x >> "ace_cargo_space")) + ";"
 + str(getNumber (_x >> "ace_cargo_size")) + ";"
 + str(getNumber (_x >> "ace_rearm_defaultSupply")) + ";"
 + str(getNumber (_x >> "ace_refuel_fuelCargo")) + ";"
 + str(getNumber (_x >> "ace_repair_canRepair")) + ";"
 + getText (_x >> "textSingular") + ";"
 + str _crewCount + ";"
 + str _passengerCount + ";"
 + str(getNumber (_x >> "maximumLoad")) + ";"
 + str(getNumber (_x >> "transportAmmo")) + ";"
 + str(getNumber (_x >> "transportFuel")) + ";"
 + str _turrets + ";"
 + str(getArray (_x >> "weapons") + getArray (_x >> "magazines")) + ";"
 + str(count(getArray (_x >> "slingLoadCargoMemoryPoints"))) + ";"
 + str(getNumber (_x >> "canFloat")) + ";"
 + str(getNumber (_x >> "isBackpack")) + ";";
};
copyToClipBoard str _configs;