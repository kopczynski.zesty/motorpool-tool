_newLine = toString [0x0D, 0x0A];
_configs = "true" configClasses (configFile >> "CfgWorlds") apply
{
 _grids = configProperties[_x >> "Grid"]; 
 _gridInfo = [];
 
 {
	 if("Zoom" in configName(_x)) then
	 {
		_zoom = [];
		_zoom pushBack getText(_x >> "format");
		_zoom pushBack getText(_x >> "formatX");
		_zoom pushBack getText(_x >> "formatY");
		_zoom pushBack getNumber(_x >> "stepX");
		_zoom pushBack getNumber(_x >> "stepY");
		_zoom pushBack getNumber(_x >> "zoomMax");
		_gridInfo pushBack configName(_x);
		_gridInfo pushBack _zoom;
	 };
 } forEach _grids;

 _newLine
 + configName(_x) + ";"
 + str([_x, true] call BIS_fnc_returnParents) + ";"
 + getText (_x >> "description") + ";"
 + str((configName(_x)) call BIS_fnc_mapSize) + ";"
 + str(getNumber (_x >> "worldSize")) + ";"
 + str(getNumber (_x >> "latitude")) + ";"
 + str(getNumber (_x >> "longitude")) + ";"
 + str _gridInfo;
};
copyToClipBoard str _configs;