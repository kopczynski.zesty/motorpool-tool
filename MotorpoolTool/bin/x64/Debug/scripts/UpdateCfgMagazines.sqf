_newLine = toString [0x0D, 0x0A];
_configs = "getNumber (_x >> 'scope') >= 1" configClasses (configFile >> "CfgMagazines") apply
{
 _newLine
 + configName(_x) + ";"
 + str([_x, true] call BIS_fnc_returnParents) + ";"
 + getText (_x >> "displayName") + ";"
 + getText (_x >> "displayNameShort") + ";"
 + getText (_x >> "descriptionShort") + ";"
 + getText (_x >> "pylonWeapon") + ";"
 + getText (_x >> "ammo") + ";"
 + str(getNumber (_x >> "count")) + ";"
 + str(getNumber (_x >> "mass"));
};
copyToClipBoard str _configs;