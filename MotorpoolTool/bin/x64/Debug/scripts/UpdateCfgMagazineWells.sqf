_newLine = toString [0x0D, 0x0A]; 
_configs = "true" configClasses (configFile >> "CfgMagazineWells") apply 
{ 
 _properties = configProperties [ _x ]; 
 _classes = []; 
 {  
  _classes pushBack (getArray (_x)); 
 } forEach _properties; 
 
 _newLine 
 + configName(_x) + ";"
 + str([_x, true] call BIS_fnc_returnParents) + ";"
 + str _classes + ";"; 
}; 
copyToClipBoard str _configs;