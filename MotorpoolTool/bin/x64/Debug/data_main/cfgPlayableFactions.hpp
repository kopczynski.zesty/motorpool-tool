// Shared base classes.
class FactionGroup
{
    class Faction
    {
        gearScript = "\bwi_data_main\configs\factions\gear.sqf";

        acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};
    };
};

// BLUFOR
#include <configs\factions\cdf_airborne\factions.hpp>
#include <configs\factions\cdf_armored\factions.hpp>

#include <configs\factions\gbr_airborne\factions.hpp>
#include <configs\factions\gbr_armored\factions.hpp>

#include <configs\factions\ger_armored\factions.hpp>

#include <configs\factions\ion_contractor\factions.hpp>

#include <configs\factions\muj_insurgents\factions.hpp>

#include <configs\factions\tan_marines\factions.hpp>

#include <configs\factions\usa_airborne\factions.hpp>
#include <configs\factions\usa_armored\factions.hpp>
#include <configs\factions\usa_marines\factions.hpp>

// OPFOR
#include <configs\factions\che_armored\factions.hpp>

#include <configs\factions\rus_airborne\factions.hpp>
#include <configs\factions\rus_armored\factions.hpp>

#include <configs\factions\tka_infantry\factions.hpp>

#include <configs\factions\urs_contractor\factions.hpp>

// INDEP
#include <configs\factions\bgm_contractor\factions.hpp>

#include <configs\factions\cna_militia\factions.hpp>

#include <configs\factions\ser_infantry\factions.hpp>

#include <configs\factions\tkm_insurgents\factions.hpp>

// CIVIL
#include <configs\factions\civilians\factions.hpp>