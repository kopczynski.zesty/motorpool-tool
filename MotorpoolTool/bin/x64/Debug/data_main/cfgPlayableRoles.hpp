// Shared base classes.
class Element {
    class Role {

    };
};

// BLUFOR, OPFOR & INDEP
#include <configs\roles\platoon\elements.hpp>
#include <configs\roles\squad\elements.hpp>
#include <configs\roles\weapon\elements.hpp>
#include <configs\roles\special\elements.hpp>
#include <configs\roles\armor\elements.hpp>
#include <configs\roles\air\elements.hpp>

// CIVILIAN
#include <configs\roles\civilians\elements.hpp>