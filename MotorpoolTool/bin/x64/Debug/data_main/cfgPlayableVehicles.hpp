// Shared base classes.
class Category {
    class Vehicle;
};

#include <configs\vehicles\ground_car\categories.hpp>
#include <configs\vehicles\ground_truck\categories.hpp>
#include <configs\vehicles\ground_mrap\categories.hpp>
#include <configs\vehicles\ground_rec\categories.hpp>
#include <configs\vehicles\ground_apc\categories.hpp>
#include <configs\vehicles\ground_ifv\categories.hpp>
#include <configs\vehicles\ground_mbt\categories.hpp>
#include <configs\vehicles\ground_td\categories.hpp>
#include <configs\vehicles\ground_spaa\categories.hpp>
#include <configs\vehicles\ground_sph\categories.hpp>
#include <configs\vehicles\ground_ra\categories.hpp>
#include <configs\vehicles\ground_misc\categories.hpp>
#include <configs\vehicles\air_rotary\categories.hpp>
#include <configs\vehicles\air_fixed\categories.hpp>
#include <configs\vehicles\air_unmanned\categories.hpp>
#include <configs\vehicles\sea_boat\categories.hpp>
#include <configs\vehicles\sea_sub\categories.hpp>