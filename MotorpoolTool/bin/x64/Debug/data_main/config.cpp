#include <defines.hpp>

class CfgPatches
{
	class bwi_data_main
	{
		units[] = {};
		requiredVersion = 1.0;
		authors[] = {"Fourjays", "Andrew"};
		author = "Black Watch International";
		url = "http://blackwatch-int.com";
	    version = 1.1;
	    versionStr = "1.1.0";
	    versionAr[] = {1,1,0};
		requiredAddons[] = {
			"bwi_armory",
			"bwi_data"
		};
	};
};

class CfgPlayableAccessories
{
	#include <cfgPlayableAccessories.hpp>
};

class CfgPlayableFactions
{
	#include <cfgPlayableFactions.hpp>
};

class CfgPlayableRoles
{
	#include <cfgPlayableRoles.hpp>
};

class CfgPlayableSupplies
{
	#include <cfgPlayableSupplies.hpp>
};

class CfgPlayableVehicles
{
	#include <cfgPlayableVehicles.hpp>
};