/**
 * OH/MH-6M (US 160th SOAR 1990-)
 */
class OH6M: Vehicle { classname = "RHS_MELB_H6M";  level = UTILITY; cost = 20; name = "OH-6M"; };
class MH6M: Vehicle { classname = "RHS_MELB_MH6M"; level = UTILITY; cost = 24; name = "MH-6M"; };


/**
 * OH/MH-6M (ION Services 2000-)
 */
class OH6M_ION: Vehicle { classname = "RHS_MELB_H6M";	level = UTILITY; cost = 24; name = "OH-6M"; textureData[] = {"ION",1}; };
class MH6M_ION: Vehicle { classname = "RHS_MELB_MH6M";	level = UTILITY; cost = 24; name = "MH-6M"; textureData[] = {"ION",1}; };


/**
 * UH-60M (1980-)
 */
class UH60M: Vehicle     { classname = "RHS_UH60M2";		level = UTILITY;  cost = 36; };
class UH60M_MG: Vehicle  { classname = "RHS_UH60M";			level = ANTI_CAR; cost = 40; };
class UH60M_MEV: Vehicle { classname = "RHS_UH60M_MEV2";	level = UTILITY;  cost = 32; };
class UH60M_EWS: Vehicle { classname = "RHS_UH60M_ESSS2";	level = UTILITY;  cost = 40; };


/**
 * UH-1H (1962-2018)
 */
class UH1H: Vehicle		{ classname = "UK3CB_CW_US_B_LATE_UH1H";      name = "UH-1H Huey (Unarmed)"; level = UTILITY;  cost = 26; };
class UH1H_MG: Vehicle	{ classname = "UK3CB_CW_US_B_LATE_UH1H_M240"; name = "UH-1H Huey (MG)";      level = ANTI_CAR; cost = 28; };
class UH1H_MEV: Vehicle	{ classname = "UK3CB_CW_US_B_LATE_UH1H_MED";  name = "UH-1H Huey (MEV)";     level = UTILITY;  cost = 26; };

class UH1H_TAN: Vehicle		{ classname = "UK3CB_B_G_UH1H_FIA";      name = "UH-1H Huey";       level = UTILITY;  cost = 26; textureData[] = {"OLIVE",1}; };
class UH1H_MG_TAN: Vehicle	{ classname = "UK3CB_B_G_UH1H_M240_FIA"; name = "UH-1H Huey (MG)";  level = ANTI_CAR; cost = 28; textureData[] = {"OLIVE",1}; };
class UH1H_MEV_TAN: Vehicle	{ classname = "UK3CB_B_G_UH1H_MED_FIA";  name = "UH-1H Huey (MEV)"; level = ANTI_CAR; cost = 26; textureData[] = {"OLIVE",1}; };

class UH1H_ION: Vehicle		{ classname = "UK3CB_O_G_UH1H_FIA";      name = "UH-1H Huey (Unarmed)"; level = UTILITY;  cost = 26; textureData[] = {"BLACK",1}; };
class UH1H_MG_ION: Vehicle	{ classname = "UK3CB_O_G_UH1H_M240_FIA"; name = "UH-1H Huey (MG)";      level = ANTI_CAR; cost = 28; textureData[] = {"BLACK",1}; };


/**
 * UH-1Y (2008-)
 */
class UH1Y: Vehicle    { classname = "RHS_UH1Y_UNARMED";  level = UTILITY;  cost = 28; };
class UH1Y_MG: Vehicle {
	classname = "RHS_UH1Y";
	level = ANTI_CAR;
	cost = 30;
	
	pylonLoadout[] = {
		"",
		"", 
		"rhsusf_ANALE39_CMFlare_Chaff_Magazine_x4" 
	};
};


/**
 * CH-47F (1970-)
 */
class CH47_D: Vehicle { classname = "RHS_CH_47F_light"; level = ANTI_CAR; cost = 46; };
class CH47_W: Vehicle { classname = "RHS_CH_47F"; 	    level = ANTI_CAR; cost = 46; };


/**
 * CH-53E (1981-)
 */
class CH53: Vehicle { classname = "rhsusf_CH53E_USMC"; level = UTILITY; cost = 50; };
class CH53_GAU21: Vehicle { classname = "rhsusf_CH53E_USMC_GAU21"; level = ANTI_CAR; cost = 56; };


/**
 * AW159 Wildcat AH1 (UK RAF 2014-)
 */
class AW159_AH1: Vehicle { classname = "UK3CB_BAF_Wildcat_AH1_TRN_8A"; level = UTILITY; cost = 28; };


/**
 * AW159 Wildcat HMA2 (UK RN 2014-)
 */
class AW159_HMA2: Vehicle { classname = "UK3CB_BAF_Wildcat_HMA2_TRN_8A"; level = UTILITY; cost = 28; };


/**
 * AW101 Merlin HC3 (UK RAF 1999-)
 */
class AW101_HC3: Vehicle 	  { classname = "UK3CB_BAF_Merlin_HC3_18"; 		level = UTILITY;  cost = 46; };
class AW101_HC3C: Vehicle 	  { classname = "UK3CB_BAF_Merlin_HC3_Cargo"; 	level = UTILITY;  cost = 46; };
class AW101_HC3L: Vehicle 	  { classname = "UK3CB_BAF_Merlin_HC3_24"; 	 	level = UTILITY;  cost = 48; };
class AW101_HC3_MG: Vehicle   { classname = "UK3CB_BAF_Merlin_HC3_18_GPMG"; level = ANTI_CAR; cost = 50; };
class AW101_HC3_CSAR: Vehicle { classname = "UK3CB_BAF_Merlin_HC3_CSAR"; 	level = ANTI_CAR; cost = 50; };


/**
 * AW101 Merlin HC4 (UK RN 1999-)
 */
class AW101_HC4: Vehicle 	  { classname = "UK3CB_BAF_Merlin_HC4_18"; 	    level = UTILITY;  cost = 46; };
class AW101_HC4C: Vehicle 	  { classname = "UK3CB_BAF_Merlin_HC4_Cargo";   level = UTILITY;  cost = 46; };
class AW101_HC4L: Vehicle 	  { classname = "UK3CB_BAF_Merlin_HC4_24"; 	    level = UTILITY;  cost = 48; };
class AW101_HC4_MG: Vehicle   { classname = "UK3CB_BAF_Merlin_HC4_18_GPMG"; level = ANTI_CAR; cost = 50; };
class AW101_HC4_CSAR: Vehicle { classname = "UK3CB_BAF_Merlin_HC4_CSAR"; 	level = ANTI_CAR; cost = 50; };


/**
 * Boeing Chinook (1982-)
 */
class Chinook_HC1: Vehicle { classname = "UK3CB_BAF_Chinook_HC1"; level = ANTI_CAR; cost = 46; };
class Chinook_HC2: Vehicle { classname = "UK3CB_BAF_Chinook_HC2"; level = ANTI_CAR; cost = 46; };


/**
 * Mi-8 (1967-)
 */
class Mi8_VDV: Vehicle  { classname = "RHS_Mi8mt_Cargo_vdv";  level = UTILITY; cost = 36; };
class Mi8_VVS: Vehicle  { classname = "RHS_Mi8mt_Cargo_vvs";  level = UTILITY; cost = 36; };
class Mi8_VVSC: Vehicle { classname = "RHS_Mi8mt_Cargo_vvsc"; level = UTILITY; cost = 36; };

class Mi8_MG_VDV: Vehicle  { classname = "RHS_Mi8mt_vdv";  level = ANTI_CAR; cost = 40; };
class Mi8_MG_VVS: Vehicle  { classname = "RHS_Mi8mt_vvs";  level = ANTI_CAR; cost = 40; };
class Mi8_MG_VVSC: Vehicle { classname = "RHS_Mi8mt_vvsc"; level = ANTI_CAR; cost = 40; };

class Mi8_UN: Vehicle { classname = "rhsgref_un_Mi8amt"; level = UTILITY; cost = 36; };

class Mi8_SOV: Vehicle  { classname = "UK3CB_CW_SOV_O_LATE_Mi8AMT";  level = UTILITY; cost = 36; };
class Mi8_MG_SOV: Vehicle  { classname = "UK3CB_CW_SOV_O_LATE_Mi8";  level = UTILITY; cost = 36; };

class Mi8_CHE: Vehicle	  { classname = "rhsgref_ins_Mi8amt"; name = "Mi-8AMT";		level = UTILITY; cost = 36; };
class Mi8_MG_CHE: Vehicle { classname = "RHS_Mi8mt_vvs";	  name = "Mi-8MT (MG)";	level = ANTI_CAR; cost = 40; textureData[] = {"Camo1",1}; };

class Mi8_TKA: Vehicle	  { classname = "UK3CB_TKA_I_Mi8AMT"; name = "Mi-8AMT";		level = UTILITY; cost = 36; };
class Mi8_MG_TKA: Vehicle { classname = "UK3CB_TKA_I_Mi8";	  name = "Mi-8MT (MG)";	level = ANTI_CAR; cost = 40; };


/**
 * HT-48/Mi-8 (Serbia 1998-)
 */
class HT48: Vehicle  { classname = "rhssaf_airforce_ht48";  level = UTILITY; cost = 36; };


/**
 * Mi-17Sh (2005-)
 */
class Mi17_CDF: Vehicle	   { classname = "rhsgref_cdf_b_reg_Mi8amt"; name = "Mi-17";	  level = UTILITY; cost = 36; };
class Mi17_MG_CDF: Vehicle { classname = "RHS_Mi8mt_vvs";			 name = "Mi-17 (MG)"; level = ANTI_CAR; cost = 40; textureData[] = {"Camo",1}; };


/**
 * Ka-60 (1998-)
 */
class KA60_VVS: Vehicle  { classname = "rhs_ka60_grey"; level = UTILITY; cost = 26; };
class KA60_VVSC: Vehicle { classname = "rhs_ka60_c"; 	level = UTILITY; cost = 26; };

class KA60_URS: Vehicle  { classname = "O_Heli_Light_02_unarmed_F"; name = "Ka-60 (Unarmed)"; level = UTILITY; cost = 26; textureData[] = {"Black",1}; };