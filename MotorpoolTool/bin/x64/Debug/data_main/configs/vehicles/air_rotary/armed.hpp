/**
 * AH-6M (160th SOAR 1990-)
 */
class AH6M_CAS: Vehicle {
	classname = "RHS_MELB_AH6M"; 
	level = ANTI_CAR; 
	cost = 34; 

	name = "AH-6M"; 
	pylonLoadout[] = {
		"rhs_mag_M229_7",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_M229_7"
	};
};
class AH6M_CAS_AGM: Vehicle {
	classname = "RHS_MELB_AH6M"; 
	level = ANTI_CAR; 
	cost = 38; 

	name = "AH-6M (AGM)"; 
	pylonLoadout[] = {
		"rhs_mag_AGM114K_2",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_M229_7"
	};
	pylonTurrets[] = {0,-1,-1,-1};
};
class AH6M_CAS_GAU: Vehicle {
	classname = "RHS_MELB_AH6M"; 
	level = ANTI_CAR; 
	cost = 38; 

	name = "AH-6M (GAU)"; 
	pylonLoadout[] = {
		"rhsusf_mag_gau19_melb_left",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_M229_7"
	};
};


/**
 * AH-6M (ION Services 2000-)
 */
class AH6M_CAS_ION: Vehicle {
	classname = "RHS_MELB_AH6M"; 
	level = ANTI_CAR; 
	cost = 34; 

	name = "AH-6M"; 
	pylonLoadout[] = {
		"rhs_mag_M229_7",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_M229_7"
	};
	textureData[] = {"ION",1};
};
class AH6M_CAS_AGM_ION: Vehicle {
	classname = "RHS_MELB_AH6M"; 
	level = ANTI_CAR; 
	cost = 38; 

	name = "AH-6M (AGM)"; 
	pylonLoadout[] = {
		"rhs_mag_AGM114K_2",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_M229_7"
	};
	pylonTurrets[] = {0,-1,-1,-1};
	textureData[] = {"ION",1};
};
class AH6M_CAS_GAU_ION: Vehicle {
	classname = "RHS_MELB_AH6M"; 
	level = ANTI_CAR; 
	cost = 38; 

	name = "AH-6M (GAU)"; 
	pylonLoadout[] = {
		"rhsusf_mag_gau19_melb_left",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_m134_pylon_3000",
		"rhs_mag_M229_7"
	};
	textureData[] = {"ION",1};
};


/**
 * UH-60M EWS/ESSS (160th SOAR 1990-)
 */
class UH60M_EWS_CAS: Vehicle { 
	classname = "RHS_UH60M_ESSS2";
	level = ANTI_CAR; 
	cost = 58;

	name = "UH-60M EWS (CAS)"; 
	pylonLoadout[] = {
		"rhs_mag_M229_19",
		"rhs_mag_M229_19",
		"rhsusf_M130_CMFlare_Chaff_Magazine_x2"
	};
};
class UH60M_ESSS_CAS: Vehicle {
	classname = "RHS_UH60M_ESSS"; 
	level = ANTI_APC; 
	cost = 64; 

	name = "UH-60M ESSS (CAS)"; 
	pylonLoadout[] = {
		"rhs_mag_M229_19",
		"rhs_mag_AGM114K_4",
		"rhs_mag_AGM114K_4",
		"rhs_mag_M229_19",
		"rhsusf_M130_CMFlare_Chaff_Magazine_x2"
	};
};


/**
 * UH-1H (1970-)
 */
class UH1H_CAS_TAN: Vehicle { 
	classname = "rhs_uh1h_hidf_gunship";  
	level = ANTI_CAR; 
	cost = 40; 

	name = "UH-1H (CAS)";

	pylonLoadout[] = {
		"rhs_mag_M151_7",
		"rhs_mag_M151_7"
	};
};


/**
 * UH-1Y (2008-)
 */
class UH1Y_CAS: Vehicle {
	classname = "RHS_UH1Y";
	level = ANTI_CAR;
	cost = 42;

	name = "UH-1Y (MG + Rockets)";

	pylonLoadout[] = {
		"rhs_mag_M151_7_green",
		"rhs_mag_M151_7_green",
		"rhsusf_ANALE39_CMFlare_Chaff_Magazine_x4"
	};
};


/**
 * AH-1 (US Army 1967-2020)
 */
class AH1: Vehicle { 
	classname = "UK3CB_CW_US_B_EARLY_AH1Z";
	level = ANTI_IFV; 
	cost = 102; 

	name = "AH-1 SuperCobra";

	pylonLoadout[] = {
		"rhs_mag_Sidewinder_heli_2",
		"rhs_mag_AGM114K_2",
		"rhs_mag_M151_19_green",
		"rhs_mag_M151_19_green",
		"rhs_mag_AGM114N_2",
		"rhs_mag_Sidewinder_heli_2",
		"rhsusf_ANALE39_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,0,-1,-1,0,-1,-1};
};


/**
 * AH-1W (US Marines 1985-2020)
 */
class AH1W: Vehicle { 
	classname = "UK3CB_CW_US_B_LATE_AH1Z";
	level = ANTI_IFV; 
	cost = 112; 

	name = "AH-1W";

	pylonLoadout[] = {
		"rhs_mag_Sidewinder_heli_2",
		"rhs_mag_AGM114K_2",
		"rhs_mag_M151_19_green",
		"rhs_mag_M151_19_green",
		"rhs_mag_AGM114N_2",
		"rhs_mag_Sidewinder_heli_2",
		"rhsusf_ANALE39_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,0,-1,-1,0,-1,-1};
};


/**
 * AH-1Z (US Marines 2000-)
 */
class AH1Z: Vehicle { 
	classname = "RHS_AH1Z";  
	level = ANTI_IFV; 
	cost = 124; 

	pylonLoadout[] = {
		"rhs_mag_Sidewinder_heli_2",
		"rhs_mag_M151_19_green",
		"rhs_mag_AGM114N_4",
		"rhs_mag_AGM114K_4",
		"rhs_mag_M151_19_green",
		"rhs_mag_Sidewinder_heli_2",
		"rhsusf_ANALE39_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,0,0,-1,-1,-1};
};


/**
 * AH-64 (1986-)
 */
class AH64: Vehicle { 
	classname = "RHS_AH64D_wd";
	level = ANTI_IFV; 
	cost = 124; 

	name = "AH-64";

	pylonLoadout[] = {
		"",
		"rhs_mag_M229_19",
		"rhs_mag_AGM114K_4",
		"rhs_mag_AGM114N_4",
		"rhs_mag_M229_19",
		""
	};
	pylonTurrets[] = {-1,-1,0,0,-1,-1};
	animationData[] = {"radar_hide",1}; // Hide the longbow radar.
};


/**
 * AH-64D (1997-)
 */
class AH64D: Vehicle { 
	classname = "RHS_AH64D_wd";  
	level = ANTI_IFV; 
	cost = 136; 

	pylonLoadout[] = {
		"",
		"rhs_mag_M229_19",
		"rhs_mag_AGM114K_4",
		"rhs_mag_AGM114N_4",
		"rhs_mag_M229_19",
		""
	};
	pylonTurrets[] = {-1,-1,0,0,-1,-1};
};


class AH64D_ATAS: Vehicle { 
	classname = "RHS_AH64D_wd";  
	level = ANTI_IFV; 
	cost = 150;

	name = "AH-64D (ATAS)";

	pylonLoadout[] = {
		"rhs_mag_ATAS_AH64_2",
		"rhs_mag_M229_19",
		"rhs_mag_AGM114K_4",
		"rhs_mag_AGM114N_4",
		"rhs_mag_M229_19",
		"rhs_mag_ATAS_AH64_2"
	};
	pylonTurrets[] = {-1,-1,0,0,-1,-1};
};


/**
 * AW159 Wildcat AH1 (UK RAF 2014-)
 */
class AW159_AH1_CAS: Vehicle { 
	classname = "UK3CB_BAF_Wildcat_AH1_CAS_8A";  
	level = ANTI_CAR; 
	cost = 42; 

	name = "Wildcat AH1 (CAS)";

	// Fixed loadout, no pylons.
};
class AW159_AH1_AT: Vehicle { 
	classname = "UK3CB_BAF_Wildcat_AH1_HEL_8A";  
	level = ANTI_CAR; 
	cost = 42; 

	name = "Wildcat AH1 (AT)";

	// Fixed loadout, no pylons.
};


/**
 * Apache AH1 (UK RAF)
 */
class ApacheAH1: Vehicle { 
	classname = "UK3CB_BAF_Apache_AH1_DynamicLoadoutUnlimited";  
	level = ANTI_IFV; 
	cost = 136; 

	pylonLoadout[] = {
		"UK3CB_BAF_PylonPod_19Rnd_CRV7_HEISAP",
		"UK3CB_BAF_PylonRack_4Rnd_Hellfire_K",
		"UK3CB_BAF_PylonRack_4Rnd_Hellfire_N",
		"UK3CB_BAF_PylonPod_19Rnd_CRV7_HEISAP"
	};
	pylonTurrets[] = {-1,0,0,-1};
};


/**
 * UH Tiger (2003-)
 */
class UH_Tiger: Vehicle { 
	classname = "BWA3_Tiger_RMK_Heavy";  
	level = ANTI_APC; 
	cost = 124; 

	// Fixed loadout, no pylons.
};


/**
 * Ka-52 (1995-)
 */
class Ka52_VVS: Vehicle { 
	classname = "RHS_Ka52_vvs";  
	level = ANTI_CAR; 
	cost = 68; 

	name = "Ka-52 (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_ka52_s8kom",
		"rhs_mag_b8v20a_ka52_s8kom",
		"rhs_mag_apu6_9m127m_ka52",
		"rhs_mag_apu6_9m127m_ka52",
		"rhs_UV26_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,0,0,-1};
};
class Ka52_VVSC: Vehicle { 
	classname = "RHS_Ka52_vvsc";  
	level = ANTI_CAR; 
	cost = 68; 

	name = "Ka-52 (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_ka52_s8kom",
		"rhs_mag_b8v20a_ka52_s8kom",
		"rhs_mag_apu6_9m127m_ka52",
		"rhs_mag_apu6_9m127m_ka52",
		"rhs_UV26_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,0,0,-1};
};


/**
 * Ka-60 (1998-)
 */
class Ka60_HE_URS: Vehicle { 
	classname = "O_Heli_Light_02_dynamicLoadout_F";  
	level = ANTI_CAR; 
	cost = 40; 

	name = "Ka-60 (HE)";

	pylonLoadout[] = {
		"PylonWeapon_300Rnd_20mm_shells",
		"PylonRack_12Rnd_missiles"
	};
	textureData[] = {"Black",1};
};
class Ka60_AP_URS: Vehicle { 
	classname = "O_Heli_Light_02_dynamicLoadout_F";  
	level = ANTI_CAR; 
	cost = 40; 

	name = "Ka-60 (AP)";

	pylonLoadout[] = {
		"PylonWeapon_300Rnd_20mm_shells",
		"PylonRack_12Rnd_PG_missiles"
	};
	textureData[] = {"Black",1};
};


/**
 * Mi-8MTV3 (1967-)
 */
class Mi8MTV3_CAS_VDV: Vehicle { 
	classname = "RHS_Mi8MTV3_vdv";  
	level = ANTI_APC; 
	cost = 58; 

	name = "Mi-8MTV-3 (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_upk23_mixed",
		"rhs_mag_upk23_mixed",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};
class Mi8MTV3_AT_VDV: Vehicle { 
	classname = "RHS_Mi8MTV3_vdv";  
	level = ANTI_APC; 
	cost = 58; 

	name = "Mi-8MTV-3 (AT)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8t",
		"rhs_mag_b8v20a_s8t",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};
class Mi8MTV3_CAS_VVS: Vehicle { 
	classname = "RHS_Mi8MTV3_vvs";  
	level = ANTI_APC; 
	cost = 58; 

	name = "Mi-8MTV-3 (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_upk23_mixed",
		"rhs_mag_upk23_mixed",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};
class Mi8MTV3_AT_VVS: Vehicle { 
	classname = "RHS_Mi8MTV3_vvs";  
	level = ANTI_APC; 
	cost = 58; 

	name = "Mi-8MTV-3 (AT)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8t",
		"rhs_mag_b8v20a_s8t",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};
class Mi8MTV3_CAS_VVSC: Vehicle { 
	classname = "RHS_Mi8MTV3_vvsc";  
	level = ANTI_APC; 
	cost = 58; 

	name = "Mi-8MTV-3 (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_upk23_mixed",
		"rhs_mag_upk23_mixed",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};
class Mi8MTV3_AT_VVSC: Vehicle { 
	classname = "RHS_Mi8MTV3_vvsc";  
	level = ANTI_APC; 
	cost = 58; 

	name = "Mi-8MTV-3 (AT)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8t",
		"rhs_mag_b8v20a_s8t",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};

class Mi8MTV3H_CAS_VDV: Vehicle { 
	classname = "RHS_Mi8MTV3_heavy_vdv";  
	level = ANTI_APC; 
	cost = 64; 

	name = "Mi-8MTV-3 Heavy (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_upk23_mixed",
		"rhs_mag_upk23_mixed",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};
class Mi8MTV3H_AT_VDV: Vehicle { 
	classname = "RHS_Mi8MTV3_heavy_vdv";  
	level = ANTI_APC; 
	cost = 64; 

	name = "Mi-8MTV-3 Heavy (AT)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8t",
		"rhs_mag_b8v20a_s8t",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};
class Mi8MTV3H_CAS_VVS: Vehicle { 
	classname = "RHS_Mi8MTV3_heavy_vvs";  
	level = ANTI_APC; 
	cost = 64; 

	name = "Mi-8MTV-3 Heavy (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_upk23_mixed",
		"rhs_mag_upk23_mixed",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};
class Mi8MTV3H_AT_VVS: Vehicle { 
	classname = "RHS_Mi8MTV3_heavy_vvs";  
	level = ANTI_APC; 
	cost = 64; 

	name = "Mi-8MTV-3 Heavy (AT)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8t",
		"rhs_mag_b8v20a_s8t",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};
class Mi8MTV3H_CAS_VVSC: Vehicle { 
	classname = "RHS_Mi8MTV3_heavy_vvsc";  
	level = ANTI_APC; 
	cost = 64; 

	name = "Mi-8MTV-3 Heavy (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_upk23_mixed",
		"rhs_mag_upk23_mixed",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};
class Mi8MTV3H_AT_VVSC: Vehicle { 
	classname = "RHS_Mi8MTV3_heavy_vvsc";  
	level = ANTI_APC; 
	cost = 64; 

	name = "Mi-8MTV-3 Heavy (AT)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8t",
		"rhs_mag_b8v20a_s8t",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x6"
	};
};

class Mi8AMTSh_CAS_SOV: Vehicle { 
	classname = "UK3CB_CW_SOV_O_LATE_Mi8AMTSh";  
	level = ANTI_APC; 
	cost = 64; 

	name = "Mi-8AMTSh (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
};
class Mi8AMTSh_CAS_TKA: Vehicle { 
	classname = "UK3CB_TKA_B_Mi8AMTSh";  
	level = ANTI_APC; 
	cost = 64; 

	name = "Mi-8AMTSh (CAS)";

	pylonLoadout[] = {
		"rhs_mag_ub32_s5k1",
		"rhs_mag_ub32_s5k1",
		"rhs_mag_ub32_s5ko",
		"rhs_mag_ub32_s5ko",
		"rhs_mag_ub32_s5k1",
		"rhs_mag_ub32_s5k1",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
};


/**
 * Mi-17Sh (CDF 2005-)
 */
class Mi17Sh_CAS_CDF: Vehicle { 
	classname = "rhsgref_cdf_b_reg_Mi17Sh";  
	level = ANTI_APC; 
	cost = 64; 

	name = "Mi-17Sh (CAS)";

	pylonLoadout[] = {
		"rhs_mag_ub32_s5m1",
		"rhs_mag_ub32_s5m1",
		"rhs_mag_ub32_s5m1",
		"rhs_mag_ub32_s5m1",
		"rhs_mag_ub32_s5ko",
		"rhs_mag_ub32_s5ko",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
};


/**
 * Mi-24V (Russian 1980-)
 */
class Mi24V_VDV: Vehicle { 
	classname = "RHS_Mi24V_vdv";  
	level = ANTI_CAR; 
	cost = 112; 

	name = "Mi-24V (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1};
};
class Mi24V_VVS: Vehicle { 
	classname = "RHS_Mi24V_vvs";  
	level = ANTI_CAR; 
	cost = 112; 

	name = "Mi-24V (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1};
};
class Mi24V_VVSC: Vehicle { 
	classname = "RHS_Mi24V_vvsc";  
	level = ANTI_CAR; 
	cost = 112; 

	name = "Mi-24V (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1};
};
class Mi24V_SOV: Vehicle { 
	classname = "UK3CB_CW_SOV_O_LATE_Mi_24V";  
	level = ANTI_CAR; 
	cost = 112; 

	name = "Mi-24V (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_9M114M_Mi24_2x",
		"rhs_mag_9M114M_Mi24_2x",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1};
};
class Mi24V_UN: Vehicle { 
	classname = "UK3CB_UN_I_Mi_24V";  
	level = ANTI_CAR; 
	cost = 112; 

	name = "Mi-24V (CAS)";

	pylonLoadout[] = {
		"rhs_mag_ub32_s5k1",
		"rhs_mag_ub32_s5k1",
		"rhs_mag_ub32_s5ko",
		"rhs_mag_ub32_s5ko",
		"rhs_mag_9M17p_Mi24_2x",
		"rhs_mag_9M17p_Mi24_2x",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1};
};


/**
 * Mi-24P (Russian 1990-)
 */
class Mi24P_VDV: Vehicle { 
	classname = "RHS_Mi24P_vdv";  
	level = ANTI_APC; 
	cost = 102; 

	name = "Mi-24P (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1};
};
class Mi24P_VVS: Vehicle { 
	classname = "RHS_Mi24P_vvs";  
	level = ANTI_APC; 
	cost = 102; 

	name = "Mi-24P (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1};
};
class Mi24P_VVSC: Vehicle { 
	classname = "RHS_Mi24P_vvsc";  
	level = ANTI_APC; 
	cost = 102; 

	name = "Mi-24P (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1};
};
class Mi24P_TKA: Vehicle { 
	classname = "UK3CB_TKA_I_Mi_24P";  
	level = ANTI_APC; 
	cost = 102; 

	name = "Mi-24P (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_mag_9M120M_Mi24_2x",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1};
};


/**
 * Mi-24G (CDF/Serbia 2000-)
 */
class Mi24G: Vehicle { 
	classname = "rhsgref_b_mi24g_CAS";  
	level = ANTI_APC; 
	cost = 124; 

	name = "Mi-24G (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8df",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_zt6_4",
		"rhs_mag_zt6_4",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1};
};


/**
 * Mi-24D (CDF 1973-2009)
 */
class Mi24D_CDF: Vehicle { 
	classname = "rhsgref_cdf_b_Mi24D";  
	level = ANTI_CAR; 
	cost = 102; 

	name = "Mi-24D (CAS)";

	pylonLoadout[] = {
		"rhs_mag_ub32_s5ko",
		"rhs_mag_ub32_s5ko",
		"rhs_mag_ub32_s5k1",
		"rhs_mag_ub32_s5k1",
		"rhs_mag_9M17p_Mi24_2x",
		"rhs_mag_9M17p_Mi24_2x",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1};
};


/**
 * Mi-28 (Russian 2009-)
 */
class Mi28_VVS: Vehicle { 
	classname = "rhs_mi28n_vvs";  
	level = ANTI_IFV; 
	cost = 136; 

	name = "Mi-28N (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_9M120M_Mi28_8x",
		"rhs_mag_9M120M_Mi28_8x",
		"rhs_UV26_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,0,0,-1};
};
class Mi28_VVSC: Vehicle { 
	classname = "rhs_mi28n_vvsc";  
	level = ANTI_IFV; 
	cost = 136; 

	name = "Mi-28N (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_b8v20a_s8kom",
		"rhs_mag_9M120M_Mi28_8x",
		"rhs_mag_9M120M_Mi28_8x",
		"rhs_UV26_CMFlare_Chaff_Magazine_x4"
	};
	pylonTurrets[] = {-1,-1,0,0,-1};
};