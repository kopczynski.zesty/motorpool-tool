/**
 * M109 (US Army)
 */
class M109_D: Vehicle { classname = "rhsusf_m109d_usarmy"; level = ANTI_MBT; cost = 84; crew = 2; };
class M109_W: Vehicle { classname = "rhsusf_m109_usarmy";  level = ANTI_MBT; cost = 84; crew = 2; };

class M109_CW: Vehicle { classname = "UK3CB_CW_US_B_EARLY_M109"; level = ANTI_MBT; cost = 84; crew = 2; };


/**
 * 2S3M1
 */
class 2S3M1_D: Vehicle { classname = "rhs_2s3_tv";  level = ANTI_MBT; cost = 92; crew = 2; textureData[] = {"rhs_sand",1}; };
class 2S3M1_W: Vehicle { classname = "rhs_2s3_tv";  level = ANTI_MBT; cost = 92; crew = 2; };


/**
 * 2S1
 */
class 2S1_D: Vehicle { classname = "rhs_2s1_tv";		level = ANTI_MBT; cost = 84; crew = 2; textureData[] = {"sand",1}; };
class 2S1_W: Vehicle { classname = "rhs_2s1_tv";		level = ANTI_MBT; cost = 84; crew = 2; };
class 2S1_G: Vehicle { classname = "rhssaf_army_2s1";	level = ANTI_MBT; cost = 84; crew = 2; };