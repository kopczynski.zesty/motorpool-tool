/**
 * F-14A (1974-1986)
 */
class F14A_CAP80: Vehicle {
	classname = "FIR_F14A";
	level = ANTI_IFV; 
	cost = 130; 

	name = "F-14A (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM7_P_1rnd_M",
		"FIR_AIM7_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M"
	};
	pylonTurrets[] = {0,0,0,0,0,0,0,0};
	animationData[] = {"Leftwing_Parking",1,"Rightwing_Parking",1};
};
class F14A_CAS80: Vehicle {
	classname = "FIR_F14A";
	level = ANTI_IFV; 
	cost = 144; 

	name = "F-14A (CAS)";

	pylonLoadout[] = {
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM7_P_1rnd_M",
		"FIR_LantirnPod_f14_P_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M"
	};
	pylonTurrets[] = {0,0,0,0,-1,-1,-1,-1};
	animationData[] = {"Leftwing_Parking",1,"Rightwing_Parking",1};
};


/**
 * F-14B (1987-2000)
 */
class F14B_CAP90: Vehicle { 
	classname = "FIR_F14B_VF32";  
	level = ANTI_APC; 
	cost = 130; 

	name = "F-14B (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9M_LAU138_P_1rnd_M",
		"FIR_AIM9M_LAU138_P_1rnd_M",
		"FIR_AIM7_P_1rnd_M",
		"FIR_AIM7_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M"
	};
	pylonTurrets[] = {0,0,0,0,0,0,0,0};
	animationData[] = {"Leftwing_Parking",1,"Rightwing_Parking",1};
};
class F14B_CAS90: Vehicle { 
	classname = "FIR_F14B_VF103";  
	level = ANTI_IFV; 
	cost = 144; 

	name = "F-14B (CAS)";

	pylonLoadout[] = {
		"FIR_AIM9M_LAU138_P_1rnd_M",
		"FIR_AIM9M_LAU138_P_1rnd_M",
		"FIR_AIM7_P_1rnd_M",
		"FIR_LantirnPod_f14_P_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M"
	};
	pylonTurrets[] = {0,0,0,0,-1,-1,-1,-1};
	animationData[] = {"Leftwing_Parking",1,"Rightwing_Parking",1};
};


/**
 * F-14D (US Navy 1991-2006)
 */
class F14D_CAP00: Vehicle { 
	classname = "FIR_F14D_VF101";  
	level = ANTI_IFV; 
	cost = 130; 

	name = "F-14D (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9M_LAU138_P_1rnd_M",
		"FIR_AIM9M_LAU138_P_1rnd_M",
		"FIR_AIM7F_P_1rnd_M",
		"FIR_AIM7F_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M",
		"FIR_AIM54_P_1rnd_M"
	};
	pylonTurrets[] = {0,0,0,0,0,0,0,0};
	animationData[] = {"Leftwing_Parking",1,"Rightwing_Parking",1};
};
class F14D_CAS00: Vehicle { 
	classname = "FIR_F14D_VF2";  
	level = ANTI_IFV; 
	cost = 144; 

	name = "F-14D (CAS)";

	pylonLoadout[] = {
		"FIR_AIM9M_LAU138_P_1rnd_M",
		"FIR_AIM9M_LAU138_P_1rnd_M",
		"FIR_AIM7F_P_1rnd_M",
		"FIR_LantirnPod_f14_P_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M",
		"FIR_GBU12_P_F14_1rnd_M"
	};
	pylonTurrets[] = {0,0,0,0,-1,-1,-1,-1};
	animationData[] = {"Leftwing_Parking",1,"Rightwing_Parking",1};
};


/**
 * F/A-18E (2000-)
 */
class FA18E_CAS00: Vehicle { 
	classname = "JS_JC_FA18E";  
	level = ANTI_IFV; 
	cost = 158; 

	name = "F/A-18E (CAS)";

	pylonLoadout[] = {
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_Hydra_M247_P_7rnd_M",
		"FIR_Hydra_M247_P_7rnd_M",
		"FIR_AGM65L_P_1rnd_M",
		"FIR_AGM65L_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_Litening_std_P_1rnd_M",
		"js_jc_120Rnd_CMChaff_Magazine",
		"js_jc_120Rnd_CMFlare_Magazine"
	};
};
class FA18E_CAP00: Vehicle { 
	classname = "JS_JC_FA18E";  
	level = ANTI_IFV; 
	cost = 144; 

	name = "F/A-18E (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM7_2_P_1rnd_M",
		"FIR_AIM7_2_P_1rnd_M",
		"FIR_AIM120_LAU115_P_1rnd_M",
		"FIR_AIM120_LAU115_P_1rnd_M",
		"FIR_AIM120_LAU115_P_1rnd_M",
		"FIR_AIM120_LAU115_P_1rnd_M",
		"js_m_fa18_wing_tank_x1",
		"js_jc_120Rnd_CMChaff_Magazine",
		"js_jc_120Rnd_CMFlare_Magazine"
	};
};
class FA18E_SEAD00: Vehicle { 
	classname = "JS_JC_FA18E";  
	level = ANTI_IFV; 
	cost = 158; 

	name = "F/A-18E (SEAD)";

	pylonLoadout[] = {
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_ALQ99_P_1rnd_M",
		"FIR_ALQ99_P_1rnd_M",
		"FIR_AIM120_LAU115_P_1rnd_M",
		"FIR_AIM120_LAU115_P_1rnd_M",
		"FIR_ALQ99_P_1rnd_M",
		"js_jc_120Rnd_CMChaff_Magazine",
		"js_jc_120Rnd_CMFlare_Magazine"
	};
};
class FA18E_Multi00: Vehicle { 
	classname = "JS_JC_FA18E";  
	level = ANTI_IFV; 
	cost = 174; 

	name = "F/A-18E (Multi)";

	pylonLoadout[] = {
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AGM65E_P_1rnd_M",
		"FIR_AGM65D_P_1rnd_M",
		"FIR_GBU39_P_4rnd_M",
		"FIR_GBU39_P_4rnd_M",
		"FIR_Litening_std_P_1rnd_M",
		"js_jc_120Rnd_CMChaff_Magazine",
		"js_jc_120Rnd_CMFlare_Magazine"
	};
};
class FA18E_Multi20: Vehicle { 
	classname = "JS_JC_FA18E";  
	level = ANTI_IFV; 
	cost = 174; 

	name = "F/A-18E (Multi)";

	pylonLoadout[] = {
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AGM65E_P_1rnd_M",
		"FIR_AGM65D_P_1rnd_M",
		"FIR_GBU53_P_4rnd_M",
		"FIR_GBU53_P_4rnd_M",
		"FIR_Litening_std_P_1rnd_M",
		"js_jc_120Rnd_CMChaff_Magazine",
		"js_jc_120Rnd_CMFlare_Magazine"
	};
};


/**
 * F-35B Lightning II (US 2015-)
 */
class F35B_Stealth_CAS10: Vehicle { 
	classname = "FIR_F35B_VMFA531";  
	level = ANTI_IFV; 
	cost = 174; 

	name = "F-35B Stealth (CAS)";

	pylonLoadout[] = {
		"FIR_EGBU12_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"",
		"",
		"",
		"",
		"",
		"",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};
class F35B_Beast_CAS10: Vehicle { 
	classname = "FIR_F35B_DarkGrey";  
	level = ANTI_IFV; 
	cost = 210; 

	name = "F-35B Beast (CAS)";

	pylonLoadout[] = {
		"FIR_EGBU12_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};
class F35B_Stealth_CAP10: Vehicle { 
	classname = "FIR_F35B_VMFA531";  
	level = ANTI_IFV; 
	cost = 158; 

	name = "F-35B Stealth (CAP)";

	pylonLoadout[] = {
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"",
		"",
		"",
		"",
		"",
		"",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};
class F35B_Beast_CAP10: Vehicle { 
	classname = "FIR_F35B_DarkGrey";  
	level = ANTI_IFV; 
	cost = 190; 

	name = "F-35B Beast (CAP)";

	pylonLoadout[] = {
	"FIR_AIM120_P_1rnd_M",
	"FIR_AIM120_P_1rnd_M",
	"FIR_AIM120_P_1rnd_M",
	"FIR_AIM120_P_1rnd_M",
	"FIR_AIM9X_P_1rnd_M",
	"FIR_AIM120_LAU115_P_2rnd_M",
	"FIR_AIM120_LAU115_P_2rnd_M",
	"FIR_AIM120_LAU115BA_P_2rnd_M",
	"FIR_AIM120_LAU115_P_2rnd_M",
	"FIR_AIM9X_P_1rnd_M",
	"FIR_Gunpod_Nomodel_P_1rnd_M",
	};
};


/**
 * F-35B (UK 2019-)
 */
class F35BL_Stealth_CAS10: Vehicle { 
	classname = "FIR_F35B_RAF01";  
	level = ANTI_IFV; 
	cost = 174; 

	name = "F-35B Stealth (CAS)";

	pylonLoadout[] = {
		"FIR_EGBU12_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"",
		"",
		"",
		"",
		"",
		"",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};
class F35BL_Beast_CAS10: Vehicle { 
	classname = "FIR_F35B_RAF02";  
	level = ANTI_IFV; 
	cost = 210; 

	name = "F-35B Beast (CAS)";

	pylonLoadout[] = {
		"FIR_EGBU12_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};
class F35BL_Stealth_CAP10: Vehicle { 
	classname = "FIR_F35B_RAF01";  
	level = ANTI_IFV; 
	cost = 158; 

	name = "F-35B Stealth (CAP)";

	pylonLoadout[] = {
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"",
		"",
		"",
		"",
		"",
		"",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};
class F35BL_Beast_CAP10: Vehicle { 
	classname = "FIR_F35B_RAF02";  
	level = ANTI_IFV; 
	cost = 190; 

	name = "F-35B Beast (CAP)";

	pylonLoadout[] = {
	"FIR_AIM120_P_1rnd_M",
	"FIR_AIM120_P_1rnd_M",
	"FIR_AIM120_P_1rnd_M",
	"FIR_AIM120_P_1rnd_M",
	"FIR_AIM9X_P_1rnd_M",
	"FIR_AIM120_LAU115_P_2rnd_M",
	"FIR_AIM120_LAU115_P_2rnd_M",
	"FIR_AIM120_LAU115BA_P_2rnd_M",
	"FIR_AIM120_LAU115_P_2rnd_M",
	"FIR_AIM9X_P_1rnd_M",
	"FIR_Gunpod_Nomodel_P_1rnd_M",
	};
};


/**
 * AV-8B Harrier II (US 1985-2000)
 */
class AV8B_H2_CAS90: Vehicle { 
	classname = "FIR_AV8B_VMA223_OLD";  
	level = ANTI_APC; 
	cost = 94; 

	name = "AV-8B Harrier II (CAS)";

	pylonLoadout[] = {
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_Hydra_LAU130_P_19rnd_M",
		"FIR_Mk82_GP_P_3rnd_M",
		"FIR_Mk82_GP_P_3rnd_M",
		"FIR_Hydra_LAU130_P_19rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_Litening_std_P_1rnd_M",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};
class AV8B_H2_CAP90: Vehicle { 
	classname = "FIR_AV8B_NA_VMA211";  
	level = ANTI_APC; 
	cost = 86; 

	name = "AV-8B Harrier II (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_AV8B_Fueltank_L_P_1rnd_M",
		"FIR_AV8B_Fueltank_R_P_1rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_Empty_P_1rnd_M",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};
class AV8B_H2_SEAD90: Vehicle { 
	classname = "FIR_AV8B_NA_VMA211";  
	level = ANTI_APC; 
	cost = 94; 

	name = "AV-8B Harrier II (SEAD)";

	pylonLoadout[] = {
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_AV8B_Fueltank_L_P_1rnd_M",
		"FIR_AV8B_Fueltank_R_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_Litening_std_P_1rnd_M",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};


/**
 * AV-8B Harrier II Plus (US Marines 2000-)
 */
class AV8B_H2P_CAS00: Vehicle { 
	classname = "FIR_AV8B_VMA231_02";  
	level = ANTI_APC; 
	cost = 114; 

	name = "AV-8B Harrier II Plus (CAS)";

	pylonLoadout[] = {
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_GBU38_P_1rnd_M",
		"FIR_GBU12_P_1rnd_M",
		"FIR_GBU12_P_1rnd_M",
		"FIR_GBU38_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_Litening_std_P_1rnd_M",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};
class AV8B_H2P_CAP00: Vehicle { 
	classname = "FIR_AV8B_VMA231_02";  
	level = ANTI_APC; 
	cost = 104; 

	name = "AV-8B Harrier II Plus (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AIM120_LAU115_P_1rnd_M",
		"FIR_AIM120_LAU115_P_1rnd_M",
		"FIR_AIM120_LAU115_P_1rnd_M",
		"FIR_AIM120_LAU115_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};
class AV8B_H2P_SEAD00: Vehicle { 
	classname = "FIR_AV8B_VMA223";  
	level = ANTI_APC; 
	cost = 114; 

	name = "AV-8B Harrier II Plus (SEAD)";

	pylonLoadout[] = {
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_AV8B_Fueltank_L_P_1rnd_M",
		"FIR_AV8B_Fueltank_R_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_Litening_std_P_1rnd_M",
		"FIR_Gunpod_Nomodel_P_1rnd_M"
	};
};


/**
 * AV-8B Sea Harrier (UK RN 1988-2010)
 */
class AV8B_FA2_CAS90: Vehicle { 
	classname = "FIR_AV8B_GR7A_Lucy";  
	level = ANTI_APC; 
	cost = 86; 

	name = "AV-8B Sea Harrier (CAS)";

	pylonLoadout[] = {
		"FIR_CRV7_P_19rnd_M",
		"FIR_GBU12_P_1rnd_M",
		"FIR_AV8B_Fueltank_L_P_1rnd_M",
		"FIR_AV8B_Fueltank_R_P_1rnd_M",
		"FIR_GBU12_P_1rnd_M",
		"FIR_CRV7_P_19rnd_M",
		"FIR_Empty_P_1rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_SniperXR_Nomodel_P_1rnd_M"
	};
};
class AV8B_FA2_CAP90: Vehicle { 
	classname = "FIR_AV8B_GR7A_Lucy";  
	level = ANTI_APC; 
	cost = 78; 

	name = "AV-8B Sea Harrier (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_AV8B_Fueltank_L_P_1rnd_M",
		"FIR_AV8B_Fueltank_R_P_1rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_Empty_P_1rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_AIM9L_LAU115_P_1rnd_M",
		"FIR_Undertail_Nomodel_P_1rnd_M"
	};
};
