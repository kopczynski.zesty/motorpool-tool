/**
 * A-10A (1977-2010)
 */
class A10A_CAS90: Vehicle { 
	classname = "FIR_A10A_Camo2";  
	level = ANTI_APC; 
	cost = 104; 

	name = "A-10A (CAS)";

	pylonLoadout[] = {
		"FIR_ECMPod_Camo_P_1rnd_M",
		"FIR_Hydra_P_7rnd_M",
		"FIR_AGM65D_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_AGM65D_P_1rnd_M",
		"FIR_Hydra_P_7rnd_M",
		"FIR_AIM9M_P_2rnd_M"
	};
};
class A10A_CAS00: Vehicle { 
	classname = "FIR_A10A_74fs";  
	level = ANTI_APC; 
	cost = 104; 

	name = "A-10A (CAS)";

	pylonLoadout[] = {
		"FIR_ECMPod_P_1rnd_M",
		"FIR_Hydra_P_7rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_Hydra_P_7rnd_M",
		"FIR_AIM9M_P_2rnd_M"
	};
};


/**
 * A-10C (2010-)
 */
class A10C_CAS10: Vehicle { 
	classname = "FIR_A10C";  
	level = ANTI_APC; 
	cost = 136; 

	name = "A-10C (CAS)";

	pylonLoadout[] = {
		"FIR_ALQ184_2_P_1rnd_M",
		"FIR_Hydra_LAU130_P_19rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_AGM65L_P_1rnd_M",
		"FIR_Litening_Nomodel_P_1rnd_M",
		"FIR_AIM9X_P_2rnd_M"
	};
};


/**
 * F-15C (1976-)
 */
class F15C_CAP00: Vehicle { 
	classname = "FIR_F15C_67FS";  
	level = ANTI_APC; 
	cost = 206; 

	name = "F-15C (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_F15C_Fueltank_P_1rnd_M",
		"FIR_F15C_Fueltank_P_1rnd_M",
		"FIR_F15C_Fueltank_P_1rnd_M"
	};
};
class F15C_CAP90: Vehicle { 
	classname = "FIR_F15C_LN";  
	level = ANTI_APC; 
	cost = 206; 

	name = "F-15C (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9L_P_1rnd_M",
		"FIR_AIM9L_P_1rnd_M",
		"FIR_AIM9L_P_1rnd_M",
		"FIR_AIM9L_P_1rnd_M",
		"FIR_AIM7_2_P_1rnd_M",
		"FIR_AIM7_2_P_1rnd_M",
		"FIR_AIM7_2_P_1rnd_M",
		"FIR_AIM7_2_P_1rnd_M",
		"FIR_F15C_Fueltank_P_1rnd_M",
		"FIR_F15C_Fueltank_P_1rnd_M",
		"FIR_F15C_Fueltank_P_1rnd_M"
	};
};


/**
 * F-15E (1985-)
 */
class F15E_CAS00: Vehicle { 
	classname = "FIR_F15E_SJ_336";  
	level = ANTI_IFV; 
	cost = 248; 

	name = "F-15E (CAS)";

	pylonLoadout[] = {
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_GBU54_P_1rnd_M",
		"FIR_Mk82_GP_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_GBU54_P_1rnd_M",
		"FIR_Mk82_GP_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_GBU54_P_1rnd_M",
		"FIR_Mk82_GP_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_GBU54_P_1rnd_M",
		"FIR_Mk82_GP_P_1rnd_M",
		"FIR_F15E_Fueltank_P_1rnd_M",
		"FIR_F15E_Fueltank_P_1rnd_M",
		"FIR_AGM84K_P_1rnd_M",
		"FIR_SniperXR_2_P_1rnd_M"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,0,-1,0,0,-1,0,0,-1,0,0,-1,-1,-1,0,0};
};
class F15E_CAS90: Vehicle { 
	classname = "FIR_F15E_SJ_334";  
	level = ANTI_IFV; 
	cost = 248; 

	name = "F-15E (CAS)";

	pylonLoadout[] = {
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM9L_P_1rnd_M",
		"FIR_AIM9L_P_1rnd_M",
		"FIR_AIM9M_P_1rnd_M",
		"FIR_GBU12_P_1rnd_M",
		"FIR_Mk82_GP_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_GBU12_P_1rnd_M",
		"FIR_Mk82_GP_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_GBU12_P_1rnd_M",
		"FIR_Mk82_GP_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_GBU12_P_1rnd_M",
		"FIR_Mk82_GP_P_1rnd_M",
		"FIR_Mk82_snakeye_P_1rnd_M",
		"FIR_F15E_Fueltank_P_1rnd_M",
		"FIR_F15E_Fueltank_P_1rnd_M",
		"FIR_GBU24A_BLU109_P_1rnd_M",
		"FIR_Litening_std_P_1rnd_M"
	};
	pylonTurrets[] = {-1,-1,-1,-1,0,-1,-1,0,-1,-1,0,-1,-1,0,-1,-1,-1,-1,0,0};
};


/**
 * F-16C (1984-)
 */
class F16C_CAS10: Vehicle { 
	classname = "FIR_F16C_WP_Juvat";  
	level = ANTI_IFV; 
	cost = 174; 

	name = "F-16C (CAS)";

	pylonLoadout[] = {
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_GBU39_P_4rnd_M",
		"FIR_AGM65L_P_1rnd_M",
		"FIR_F16C_center_Fueltank_P_1rnd_M",
		"FIR_SniperXR_1_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_GBU39_P_4rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M"
	};
};
class F16C_CAS00: Vehicle { 
	classname = "FIR_F16C_WP_Juvat";  
	level = ANTI_IFV; 
	cost = 158; 

	name = "F-16C (CAS)";

	pylonLoadout[] = {
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_GBU38_P_2rnd_M",
		"FIR_AGM65L_P_1rnd_M",
		"FIR_F16C_center_Fueltank_P_1rnd_M",
		"FIR_SniperXR_1_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_GBU54_P_2rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M"
	};
};
class F16C_CAP00: Vehicle { 
	classname = "FIR_F16C_WP";  
	level = ANTI_APC; 
	cost = 144; 

	name = "F-16C (CAP)";

	pylonLoadout[] = {
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_F16C_Fueltank_P_1rnd_M",
		"FIR_ALQ184_1_P_1rnd_M",
		"FIR_SniperXR_HTS_P_1rnd_M",
		"FIR_F16C_Fueltank_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M"
	};
};
class F16C_SEAD00: Vehicle { 
	classname = "FIR_F16C_Osan";  
	level = ANTI_IFV; 
	cost = 158; 

	name = "F-16C (SEAD)";

	pylonLoadout[] = {
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_F16C_Fueltank_P_1rnd_M",
		"FIR_ALQ184_1_P_1rnd_M",
		"FIR_SniperXR_HTS_P_1rnd_M",
		"FIR_F16C_Fueltank_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_AIM9X_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M"
	};
};
class F16C_CAS90: Vehicle { 
	classname = "FIR_F16C_WP_Juvat";  
	level = ANTI_IFV; 
	cost = 158; 

	name = "F-16C (CAS)";

	pylonLoadout[] = {
		"FIR_AIM9L_P_1rnd_M",
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AGM65D_P_1rnd_M",
		"FIR_GBU12_P_2rnd_M",
		"FIR_F16C_center_Fueltank_P_1rnd_M",
		"FIR_LantirnPod_P_1rnd_M",
		"FIR_GBU12_P_2rnd_M",
		"FIR_AGM65D_P_1rnd_M",
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM9L_P_1rnd_M"
	};
};
class F16C_CAP90: Vehicle { 
	classname = "FIR_F16C_WP";  
	level = ANTI_IFV; 
	cost = 144; 

	name = "F-16C (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9L_P_1rnd_M",
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM7_2_P_1rnd_M",
		"FIR_F16C_Fueltank_P_1rnd_M",
		"FIR_ALQ119_1_P_1rnd_M",
		"FIR_Empty_P_1rnd_M",
		"FIR_F16C_Fueltank_P_1rnd_M",
		"FIR_AIM7_2_P_1rnd_M",
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM9L_P_1rnd_M"
	};
};
class F16C_SEAD90: Vehicle { 
	classname = "FIR_F16C_AV";  
	level = ANTI_IFV; 
	cost = 158; 

	name = "F-16C (SEAD)";

	pylonLoadout[] = {
		"FIR_AIM9L_P_1rnd_M",
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_F16C_Fueltank_P_1rnd_M",
		"FIR_ALQ119_1_P_1rnd_M",
		"FIR_HTSPod_P_1rnd_M",
		"FIR_F16C_Fueltank_P_1rnd_M",
		"FIR_AGM88_P_1rnd_M",
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM9L_P_1rnd_M"
	};
};


/**
 * F-22 (2005-)
 */
class F22_CAP00: Vehicle { 
	classname = "rhsusf_f22";  
	level = ANTI_APC; 
	cost = 130; 

	name = "F-22 (CAP)";

	pylonLoadout[] = {
		"rhs_mag_Sidewinder_int",
		"rhs_mag_aim120d_int",
		"rhs_mag_aim120d_2_F22_l",
		"rhs_mag_aim120d_2_F22_r",
		"rhs_mag_aim120d_int",
		"rhs_mag_Sidewinder_int",
		"rhsusf_ANALE52_CMFlare_Chaff_Magazine_x4"
	};
};


/**
 * AV-8B Harrier GR7A (UK RAF 1990-2010)
 */
class AV8B_GR7_CAS00: Vehicle { 
	classname = "FIR_AV8B_GR7A_Lucy";  
	level = ANTI_APC; 
	cost = 86; 

	name = "AV-8B Harrier GR7A (CAS)";

	pylonLoadout[] = {
		"FIR_CRV7_P_19rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_AV8B_Fueltank_L_P_1rnd_M",
		"FIR_AV8B_Fueltank_R_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_CRV7_P_19rnd_M",
		"FIR_Empty_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_SniperXR_Nomodel_P_1rnd_M"
	};
};
class AV8B_GR7_CAP00: Vehicle { 
	classname = "FIR_AV8B_GR7A_Lucy";  
	level = ANTI_APC; 
	cost = 78; 

	name = "AV-8B Harrier GR7A (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AV8B_Fueltank_L_P_1rnd_M",
		"FIR_AV8B_Fueltank_R_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_Empty_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_Undertail_Nomodel_P_1rnd_M"
	};
};


/**
 * AV-8B Harrier GR9A (UK RAF 2003-2011)
 */
class AV8B_GR9_CAS00: Vehicle { 
	classname = "FIR_AV8B_GR9A";  
	level = ANTI_APC; 
	cost = 104; 

	name = "AV-8B Harrier GR9A (CAS)";

	pylonLoadout[] = {
		"FIR_EGBU12_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_AV8B_Fueltank_L_P_1rnd_M",
		"FIR_AV8B_Fueltank_R_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_EGBU12_P_1rnd_M",
		"FIR_Empty_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_SniperXR_Nomodel_P_1rnd_M"
	};
};
class AV8B_GR9_CAP00: Vehicle { 
	classname = "FIR_AV8B_GR9A";  
	level = ANTI_APC; 
	cost = 94; 

	name = "AV-8B Harrier GR9A (CAP)";

	pylonLoadout[] = {
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AV8B_Fueltank_L_P_1rnd_M",
		"FIR_AV8B_Fueltank_R_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_Empty_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_AIM9M_LAU115_P_1rnd_M",
		"FIR_Undertail_Nomodel_P_1rnd_M"
	};
};


/**
 * Su-25 (1981-2014)
 */
class Su25_CAS00: Vehicle { 
	classname = "RHS_Su25SM_vvsc";  
	level = ANTI_APC; 
	cost = 104; 

	name = "Su-25 (CAS)";

	pylonLoadout[] = {
		"rhs_mag_kh29ML",
		"rhs_mag_kh29ML",
		"rhs_mag_b8m1_s8df",
		"rhs_mag_b8m1_s8df",
		"rhs_mag_b8m1_s8kom",
		"rhs_mag_b8m1_s8kom",
		"rhs_mag_fab500",
		"rhs_mag_fab500",
		"rhs_mag_R60M",
		"rhs_mag_R60M",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
};
class Su25_CAS90: Vehicle { 
	classname = "RHS_Su25SM_vvsc";  
	level = ANTI_APC; 
	cost = 104; 

	name = "Su-25 (CAS)";

	pylonLoadout[] = {
		"rhs_mag_fab500",
		"rhs_mag_fab500",
		"rhs_mag_b13l_s13t",
		"rhs_mag_b13l_s13b",
		"rhs_mag_b8m1_s8kom",
		"rhs_mag_b8m1_s8kom",
		"rhs_mag_b8m1_s8df",
		"rhs_mag_b8m1_s8df",
		"rhs_mag_R60M",
		"rhs_mag_R60M",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
};
class Su25_CAS00_CDF: Vehicle { 
	classname = "rhsgref_cdf_b_su25";  
	level = ANTI_APC; 
	cost = 104; 

	name = "Su-25 (CAS)";

	pylonLoadout[] = {
		"rhs_mag_kh29ML",
		"rhs_mag_kh29ML",
		"rhs_mag_b8m1_s8df",
		"rhs_mag_b8m1_s8df",
		"rhs_mag_b8m1_s8kom",
		"rhs_mag_b8m1_s8kom",
		"rhs_mag_fab500",
		"rhs_mag_fab500",
		"rhs_mag_R60M",
		"rhs_mag_R60M",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
};
class Su25_CAS00_TKA: Vehicle { 
	classname = "UK3CB_TKA_O_Su25SM";  
	level = ANTI_APC; 
	cost = 104; 

	name = "Su-25SM (CAS)";

	pylonLoadout[] = {
		"rhs_mag_kh29ML",
		"rhs_mag_kh29ML",
		"rhs_mag_b8m1_s8df",
		"rhs_mag_b8m1_s8df",
		"rhs_mag_b8m1_s8kom",
		"rhs_mag_b8m1_s8kom",
		"rhs_mag_fab500",
		"rhs_mag_fab500",
		"rhs_mag_R60M",
		"rhs_mag_R60M",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
};
class Su25_CAS90_TKA: Vehicle { 
	classname = "UK3CB_TKA_O_Su25SM";  
	level = ANTI_APC; 
	cost = 104; 

	name = "Su-25SM (CAS)";

	pylonLoadout[] = {
		"rhs_mag_fab500",
		"rhs_mag_fab500",
		"rhs_mag_b13l_s13t",
		"rhs_mag_b13l_s13b",
		"rhs_mag_b8m1_s8kom",
		"rhs_mag_b8m1_s8kom",
		"rhs_mag_b8m1_s8df",
		"rhs_mag_b8m1_s8df",
		"rhs_mag_R60M",
		"rhs_mag_R60M",
		"rhs_ASO2_CMFlare_Chaff_Magazine_x4"
	};
};


/**
 * Su-25SM3 (2014-)
 */
class Su25SM3_CAS10: Vehicle { 
	classname = "FIR_Su25SM3_Camo";  
	level = ANTI_APC; 
	cost = 124; 

	name = "Su-25SM3 (CAS)";

	pylonLoadout[] = {
		"FIR_R73_P_1rnd_M",
		"FIR_S8KOM_P_20rnd_M",
		"FIR_KAB500L_P_1rnd_M",
		"FIR_Kh29D_P_1rnd_M",
		"FIR_Kh29_P_1rnd_M",
		"FIR_KAB500L_P_1rnd_M",
		"FIR_S8KOM_P_20rnd_M",
		"FIR_R73_P_1rnd_M"
	};
};
class Su25SM3_SEAD10: Vehicle { 
	classname = "FIR_Su25SM3";  
	level = ANTI_APC; 
	cost = 124; 

	name = "Su-25SM3 (SEAD)";

	pylonLoadout[] = {
		"FIR_R73_P_1rnd_M",
		"FIR_Kh25MPU_P_1rnd_M",
		"FIR_Kh25MPU_P_1rnd_M",
		"FIR_KAB500SE_P_1rnd_M",
		"FIR_KAB500SE_P_1rnd_M",
		"FIR_Kh25MPU_P_1rnd_M",
		"FIR_Kh25MPU_P_1rnd_M",
		"FIR_R73_P_1rnd_M"
	};
};


/**
 * Su-35E (2014-)
 */
class Su35_CAS10: Vehicle { 
	classname = "JS_JC_SU35";  
	level = ANTI_IFV; 
	cost = 226; 

	name = "Su-35 (CAS)";

	textureData[] = {"RussianAFGreyDigital",1};

	pylonLoadout[] = {
		"FIR_R73_P_1rnd_M",
		"FIR_R73_P_1rnd_M",
		"FIR_Kh25ML_P_1rnd_M",
		"FIR_Kh25ML_P_1rnd_M",
		"FIR_Kh25MTP_P_1rnd_M",
		"FIR_Kh25MTP_P_1rnd_M",
		"FIR_KAB500L_P_1rnd_M",
		"FIR_KAB500L_P_1rnd_M",
		"FIR_R77_P_1rnd_M",
		"FIR_R77_P_1rnd_M",
		"js_m_su35_empty",
		"PylonRack_Wing_Tank_JS_SU35_x1"
	};
};
class Su35_CAP10: Vehicle { 
	classname = "JS_JC_SU35";  
	level = ANTI_IFV; 
	cost = 206; 

	name = "Su-35 (CAP)";

	textureData[] = {"RussianAFSkyBlue",1};

	pylonLoadout[] = {
		"FIR_R73_P_1rnd_M",
		"FIR_R73_P_1rnd_M",
		"FIR_R27ER_P_1rnd_M",
		"FIR_R27ER_P_1rnd_M",
		"FIR_R27ER_P_1rnd_M",
		"FIR_R27ER_P_1rnd_M",
		"FIR_R27ET_P_1rnd_M",
		"FIR_R27ET_P_1rnd_M",
		"FIR_R77_P_1rnd_M",
		"FIR_R77_P_1rnd_M",
		"js_m_su35_empty",
		"PylonRack_Wing_Tank_JS_SU35_x1"
	};
};
class Su35_SEAD10: Vehicle { 
	classname = "JS_JC_SU35";  
	level = ANTI_IFV; 
	cost = 226; 

	name = "Su-35 (SEAD)";

	textureData[] = {"RussianAFSkyBlue",1};

	pylonLoadout[] = {
		"FIR_R73_P_1rnd_M",
		"FIR_R73_P_1rnd_M",
		"FIR_Kh25MPU_P_1rnd_M",
		"FIR_Kh25MPU_P_1rnd_M",
		"FIR_Kh25MPU_P_1rnd_M",
		"FIR_Kh25MPU_P_1rnd_M",
		"FIR_Kh25ML_P_1rnd_M",
		"FIR_Kh25ML_P_1rnd_M",
		"FIR_R77_P_1rnd_M",
		"FIR_R77_P_1rnd_M",
		"js_m_su35_empty",
		"PylonRack_Wing_Tank_JS_SU35_x1"
	};
};


/**
 * MiG-29S (1982-2014)
 */
class MiG29S_Multi9090: Vehicle { 
	classname = "rhs_mig29s_vvsc";  
	level = ANTI_APC; 
	cost = 130; 

	name = "MiG-29S (Multi)";

	textureData[] = {"Green_camo",1};

	pylonLoadout[] = {
		"rhs_mag_b8m1_bd3_umk2a_s8kom",
		"rhs_mag_b8m1_bd3_umk2a_s8kom",
		"rhs_mag_b8m1_bd3_umk2a_s8df",
		"rhs_mag_b8m1_bd3_umk2a_s8df",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};
class MiG29S_CAP90: Vehicle { 
	classname = "rhs_mig29s_vvsc";  
	level = ANTI_APC; 
	cost = 118; 

	name = "MiG-29S (CAP)";

	textureData[] = {"Green_camo",1};

	pylonLoadout[] = {
		"rhs_mag_R27ER_APU470",
		"rhs_mag_R27ET_APU470",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R60M_APU60",
		"rhs_mag_R60M_APU60",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};
class MiG29S_Multi9090_CDF: Vehicle { 
	classname = "rhsgref_cdf_b_mig29s";  
	level = ANTI_IFV; 
	cost = 130; 

	name = "MiG-29S (Multi)";

	pylonLoadout[] = {
		"rhs_mag_b8m1_bd3_umk2a_s8kom",
		"rhs_mag_b8m1_bd3_umk2a_s8kom",
		"rhs_mag_b8m1_bd3_umk2a_s8df",
		"rhs_mag_b8m1_bd3_umk2a_s8df",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};
class MiG29S_CAP90_CDF: Vehicle { 
	classname = "rhsgref_cdf_b_mig29s";  
	level = ANTI_APC; 
	cost = 118; 

	name = "MiG-29S (CAP)";

	pylonLoadout[] = {
		"rhs_mag_R27ER_APU470",
		"rhs_mag_R27ET_APU470",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R60M_APU60",
		"rhs_mag_R60M_APU60",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};
class MiG29S_Multi9090_TKA: Vehicle { 
	classname = "UK3CB_TKA_O_MIG29S";  
	level = ANTI_IFV; 
	cost = 130; 

	name = "MiG-29S (Multi)";

	pylonLoadout[] = {
		"rhs_mag_b8m1_bd3_umk2a_s8kom",
		"rhs_mag_b8m1_bd3_umk2a_s8kom",
		"rhs_mag_b8m1_bd3_umk2a_s8df",
		"rhs_mag_b8m1_bd3_umk2a_s8df",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};
class MiG29S_CAP90_TKA: Vehicle { 
	classname = "UK3CB_TKA_O_MIG29S";  
	level = ANTI_APC; 
	cost = 118; 

	name = "MiG-29S (CAP)";

	pylonLoadout[] = {
		"rhs_mag_R27ER_APU470",
		"rhs_mag_R27ET_APU470",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R60M_APU60",
		"rhs_mag_R60M_APU60",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};


/**
 * MiG-29SM (2014-)
 */
class MiG29SM_CAS10: Vehicle { 
	classname = "rhs_mig29sm_vvsc";  
	level = ANTI_IFV; 
	cost = 144; 

	name = "MiG-29SM (CAS)";

	textureData[] = {"Green_camo4",1};

	pylonLoadout[] = {
		"rhs_mag_b13l_bd3_umk2a_s13t",
		"rhs_mag_b13l_bd3_umk2a_s13of",
		"rhs_mag_kab500kr_bd3_umk2a",
		"rhs_mag_kab500kr_bd3_umk2a",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};
class MiG29SM_CAP10: Vehicle { 
	classname = "rhs_mig29sm_vvsc";  
	level = ANTI_APC; 
	cost = 130; 

	name = "MiG-29SM (CAP)";

	textureData[] = {"Green_camo4",1};

	pylonLoadout[] = {
		"rhs_mag_R27ET_APU470",
		"rhs_mag_R27ER_APU470",
		"rhs_mag_R77M_AKU170_MIG29",
		"rhs_mag_R77M_AKU170_MIG29",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};
class MiG29SM_CAS10_TKA: Vehicle { 
	classname = "UK3CB_TKA_O_MIG29SM";  
	level = ANTI_IFV; 
	cost = 144; 

	name = "MiG-29SM (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b13l_bd3_umk2a_s13t",
		"rhs_mag_b13l_bd3_umk2a_s13of",
		"rhs_mag_kab500kr_bd3_umk2a",
		"rhs_mag_kab500kr_bd3_umk2a",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};
class MiG29SM_CAP10_TKA: Vehicle { 
	classname = "UK3CB_TKA_O_MIG29SM";  
	level = ANTI_APC; 
	cost = 130; 

	name = "MiG-29SM (CAP)";

	pylonLoadout[] = {
		"rhs_mag_R27ET_APU470",
		"rhs_mag_R27ER_APU470",
		"rhs_mag_R77M_AKU170_MIG29",
		"rhs_mag_R77M_AKU170_MIG29",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};



/**
 * L-18/MiG-29B (Serbia 1982-)
 */
class L18_CAS00: Vehicle { 
	classname = "rhssaf_airforce_l_18";  
	level = ANTI_IFV; 
	cost = 130; 

	name = "L-18 (CAS)";

	pylonLoadout[] = {
		"rhs_mag_b13l_bd3_umk2a_s13b",
		"rhs_mag_b13l_bd3_umk2a_s13t",
		"rhs_mag_kab500kr_bd3_umk2a",
		"rhs_mag_kab500kr_bd3_umk2a",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};
class L18_CAP00: Vehicle { 
	classname = "rhssaf_airforce_l_18_101";  
	level = ANTI_APC; 
	cost = 118; 

	name = "L-18 (CAP)";

	pylonLoadout[] = {
		"rhs_mag_R27ER_APU470",
		"rhs_mag_R27ET_APU470",
		"rhs_mag_R77M_AKU170_MIG29",
		"rhs_mag_R77M_AKU170_MIG29",
		"rhs_mag_R73M_APU73",
		"rhs_mag_R73M_APU73",
		"rhs_mag_ptb1500",
		"rhs_BVP3026_CMFlare_Chaff_Magazine_x2"
	};
};


/**
 * Su-57 (2018-)
 */
class Su57_CAS10: Vehicle { 
	classname = "RHS_T50_vvs_generic";  
	level = ANTI_IFV; 
	cost = 174; 

	name = "Su-57 (CAS)";

	textureData[] = {"standard",1};

	pylonLoadout[] = {
		"rhs_mag_Kh38mle_int",
		"rhs_mag_Kh38mle_int",
		"rhs_mag_Kh38mte_int",
		"rhs_mag_Kh38mte_int",
		"rhs_mag_R74M2_int",
		"rhs_mag_R74M2_int"
	};
};
class Su57_CAP10: Vehicle { 
	classname = "RHS_T50_vvs_generic";  
	level = ANTI_IFV; 
	cost = 158; 

	name = "Su-57 (CAP)";

	textureData[] = {"standard",1};

	pylonLoadout[] = {
		"rhs_mag_R77M",
		"rhs_mag_R77M",
		"rhs_mag_R77M",
		"rhs_mag_R77M",
		"rhs_mag_R74M2_int",
		"rhs_mag_R74M2_int"
	};
};
class Su57_SEAD10: Vehicle { 
	classname = "RHS_T50_vvs_generic";  
	level = ANTI_IFV; 
	cost = 174; 

	name = "Su-57 (SEAD)";

	textureData[] = {"standard",1};

	pylonLoadout[] = {
		"rhs_mag_Kh38mae_int",
		"rhs_mag_Kh38mae_int",
		"rhs_mag_R77M",
		"rhs_mag_R77M",
		"rhs_mag_R74M2_int",
		"rhs_mag_R74M2_int"
	};
};


/**
 * L-159 Alca (2000-)
 */

class L159_CAS00_CDF: Vehicle { 
	classname = "rhs_l159_cdf_b_CDF";  
	level = ANTI_APC; 
	cost = 86; 

	name = "L-159 Alca (CAS)";

	pylonLoadout[] = {
		"rhs_mag_M151_7_USAF_LAU131",
		"rhs_mag_agm65d",
		"rhs_mag_agm65d",
		"rhs_mag_zpl20_hei",
		"rhs_mag_agm65e",
		"rhs_mag_agm65d",
		"rhs_mag_M151_7_USAF_LAU131",
		"rhsusf_ANALE40_CMFlare_Chaff_Magazine_x2"
	};
};
class L159_CAP00_CDF: Vehicle { 
	classname = "rhs_l159_cdf_b_CDF";  
	level = ANTI_APC; 
	cost = 78; 

	name = "L-159 Alca (CAP)";

	pylonLoadout[] = {
		"rhs_mag_aim120",
		"rhs_mag_aim9m",
		"rhs_mag_aim9m",
		"rhs_mag_zpl20_hei",
		"rhs_mag_aim9m",
		"rhs_mag_aim9m",
		"rhs_mag_aim120",
		"rhsusf_ANALE40_CMFlare_Chaff_Magazine_x2"
	};
};
class L159_Multi00_CDF: Vehicle { 
	classname = "rhs_l159_cdf_b_CDF";  
	level = ANTI_APC; 
	cost = 94; 

	name = "L-159 Alca (Multi)";

	pylonLoadout[] = {
		"rhs_mag_aim9m",
		"rhs_mag_M151_7_USAF_LAU131",
		"rhs_mag_mk82_3",
		"rhs_mag_zpl20_hei",
		"rhs_mag_mk82_3",
		"rhs_mag_M151_7_USAF_LAU131",
		"rhs_mag_aim9m",
		"rhsusf_ANALE40_CMFlare_Chaff_Magazine_x2"
	};
};


/**
 * L-39C Albatros (1972-1996)
 */
class L39_CAS80_TKA: Vehicle { 
	classname = "UK3CB_TKA_O_L39_PYLON";  
	level = ANTI_APC; 
	cost = 86; 

	name = "L-39 Albatros (CAS)";

	pylonLoadout[] = {
		"PylonRack_7Rnd_Rocket_04_HE_F",
		"PylonRack_7Rnd_Rocket_04_AP_F",
		"PylonRack_3Rnd_LG_scalpel",
		"PylonWeapon_300Rnd_20mm_shells",
		"PylonRack_3Rnd_LG_scalpel",
		"PylonRack_7Rnd_Rocket_04_AP_F",
		"PylonRack_7Rnd_Rocket_04_HE_F"
	};
};
class L39_CAP80_TKA: Vehicle { 
	classname = "UK3CB_TKA_O_L39_PYLON";  
	level = ANTI_APC; 
	cost = 78; 

	name = "L-39 Albatros (CAP)";

	pylonLoadout[] = {
		"PylonRack_1Rnd_Missile_AA_04_F",
		"PylonRack_1Rnd_GAA_missiles",
		"PylonRack_1Rnd_GAA_missiles",
		"PylonWeapon_300Rnd_20mm_shells",
		"PylonRack_1Rnd_GAA_missiles",
		"PylonRack_1Rnd_GAA_missiles",
		"PylonRack_1Rnd_Missile_AA_04_F"
	};
};
class L39_Multi80_TKA: Vehicle { 
	classname = "UK3CB_TKA_O_L39_PYLON";  
	level = ANTI_APC; 
	cost = 94; 

	name = "L-39 Albatros (Multi)";

	pylonLoadout[] = {
		"PylonRack_1Rnd_Missile_AA_04_F",
		"PylonRack_7Rnd_Rocket_04_HE_F",
		"PylonMissile_1Rnd_Mk82_F",
		"PylonWeapon_300Rnd_20mm_shells",
		"PylonMissile_1Rnd_Mk82_F",
		"PylonRack_7Rnd_Rocket_04_HE_F",
		"PylonRack_1Rnd_Missile_AA_04_F"
	};
};


/**
 *  Antonov An-2 (1950-)
 */
class AN2_CAS80_CDF: Vehicle { 
	classname = "UK3CB_B_G_Antonov_An2_Armed";  
	level = ANTI_CAR; 
	cost = 30; 

	name = "Antonov An-2 (CAS)";

	pylonLoadout[] = {
		"UK3CB_Factions_AN2_1000Rnd_127x99_Yellow",
		"UK3CB_Factions_Antonov_AN2_rocketPod"
	};
};
class AN2_CAS80_TKA: Vehicle { 
	classname = "UK3CB_TKA_O_Antonov_AN2_Armed";  
	level = ANTI_CAR; 
	cost = 30; 

	name = "Antonov An-2 (CAS)";

	pylonLoadout[] = {
		"UK3CB_Factions_AN2_1000Rnd_127x99_Red",
		"UK3CB_Factions_Antonov_AN2_rocketPod"
	};
};


/**
 * A-29 Super Tucano (Brazil 2003-)
 */
class A29_CAS00_TAN: Vehicle { 
	classname = "RHSGREF_A29B_HIDF";  
	level = ANTI_APC; 
	cost = 52; 

	name = "A-29A Super Tucano (CAS)";

	pylonLoadout[] = {
		"rhs_mag_AGM114K_2_plane",
		"rhs_mag_M151_7_USAF_LAU131",
		"rhs_mag_mk82",
		"rhs_mag_M151_7_USAF_LAU131",
		"rhs_mag_AGM114N_2_plane",
		"rhsusf_ANALE40_CMFlare_Magazine_x2"
	};
	pylonTurrets[] = {0,-1,-1,-1,0,-1};
};
class A29_CAS00_ION: Vehicle { 
	classname = "BWI_A29_ION";  
	level = ANTI_APC; 
	cost = 52; 

	name = "A-29B Super Tucano (CAS)";

	pylonLoadout[] = {
		"rhs_mag_AGM114K_2_plane",
		"rhs_mag_M151_7_USAF_LAU131",
		"rhs_mag_mk82",
		"rhs_mag_M151_7_USAF_LAU131",
		"rhs_mag_AGM114N_2_plane",
		"rhsusf_ANALE40_CMFlare_Magazine_x2"
	};
	pylonTurrets[] = {0,-1,-1,-1,0,-1};
};


/**
 * JAS 39 Gripen (Sweden 2000-)
 */
class JAS39_CAS00: Vehicle { 
	classname = "I_Plane_Fighter_04_F";  
	level = ANTI_APC; 
	cost = 130; 

	name = "JAS 39 Gripen (CAS)";

	textureData[] = {"CamoGrey",1};

	pylonLoadout[] = {
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AGM65G_P_1rnd_M",
		"FIR_AGM65L_P_1rnd_M",
		"FIR_GBU38_P_3rnd_M",
		"FIR_Mk82_GP_P_3rnd_M"
	};
};
class JAS39_CAP00: Vehicle { 
	classname = "I_Plane_Fighter_04_F";  
	level = ANTI_APC; 
	cost = 118; 

	name = "JAS 39 Gripen (CAP)";

	textureData[] = {"CamoGrey",1};

	pylonLoadout[] = {
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM9M_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M",
		"FIR_AIM120_P_1rnd_M"
	};
};