/**
 * M939 5 Ton
 */
class M939_M2_CW: Vehicle	{ classname = "UK3CB_CW_US_B_LATE_M939_Guntruck"; name = "M939 Guntruck"; level = ANTI_CAR; cost = 30; };
class M939_M2_TAN: Vehicle	{ classname = "UK3CB_B_M939_Guntruck_HIDF"; name = "M939 Guntruck"; level = ANTI_CAR; cost = 30; };


/**
 * FMTVs
 */
class M1078A1P2B_M2_D: Vehicle    { classname = "rhsusf_M1078A1P2_B_M2_D_fmtv_usarmy"; 		    level = ANTI_CAR; cost = 44; };
class M1078A1P2B_M2_W: Vehicle    { classname = "rhsusf_M1078A1P2_B_M2_WD_fmtv_usarmy"; 		level = ANTI_CAR; cost = 44; };
class M1078A1P2B_FB_M2_D: Vehicle { classname = "rhsusf_M1078A1P2_B_M2_D_flatbed_fmtv_usarmy";  level = ANTI_CAR; cost = 44; };
class M1078A1P2B_FB_M2_W: Vehicle { classname = "rhsusf_M1078A1P2_B_M2_WD_flatbed_fmtv_usarmy"; level = ANTI_CAR; cost = 44; };
class M1083A1P2B_M2_D: Vehicle    { classname = "rhsusf_M1083A1P2_B_M2_D_fmtv_usarmy"; 		    level = ANTI_CAR; cost = 54; };
class M1083A1P2B_M2_W: Vehicle    { classname = "rhsusf_M1083A1P2_B_M2_WD_fmtv_usarmy"; 		level = ANTI_CAR; cost = 54; };
class M1083A1P2B_FB_M2_D: Vehicle { classname = "rhsusf_M1083A1P2_B_M2_D_flatbed_fmtv_usarmy";  level = ANTI_CAR; cost = 54; };
class M1083A1P2B_FB_M2_W: Vehicle { classname = "rhsusf_M1083A1P2_B_M2_WD_flatbed_fmtv_usarmy"; level = ANTI_CAR; cost = 54; };
class M1084A1P2B_M2_D: Vehicle    { classname = "rhsusf_M1084A1P2_B_M2_D_fmtv_usarmy"; 		    level = ANTI_CAR; cost = 54; };
class M1084A1P2B_M2_W: Vehicle    { classname = "rhsusf_M1084A1P2_B_M2_WD_fmtv_usarmy"; 		level = ANTI_CAR; cost = 54; };

class M1078A1R_SOV_M2_D: Vehicle { classname = "rhsusf_M1078A1R_SOV_M2_D_fmtv_socom"; level = ANTI_CAR; cost = 32; };
class M1078A1R_SOV_M2_W: Vehicle { classname = "rhsusf_M1078A1R_SOV_M2_D_fmtv_socom"; level = ANTI_CAR; cost = 32; textureData[] = {"rhs_woodland",1}; };
class M1084A1R_SOV_M2_D: Vehicle { classname = "rhsusf_M1084A1R_SOV_M2_D_fmtv_socom"; level = ANTI_CAR; cost = 38; };
class M1084A1R_SOV_M2_W: Vehicle { classname = "rhsusf_M1084A1R_SOV_M2_D_fmtv_socom"; level = ANTI_CAR; cost = 38; textureData[] = {"rhs_woodland",1}; };


/**
 * HEMTTs
 */
class M977A4B_M2_D: Vehicle { classname = "rhsusf_M977A4_BKIT_M2_usarmy_d";  level = ANTI_CAR; cost = 62; };
class M977A4B_M2_W: Vehicle { classname = "rhsusf_M977A4_BKIT_M2_usarmy_wd"; level = ANTI_CAR; cost = 62; };

class M977A4B_M2_Ammo_D: Vehicle { classname = "rhsusf_M977A4_AMMO_BKIT_M2_usarmy_d";  level = ANTI_CAR; cost = 62; };
class M977A4B_M2_Ammo_W: Vehicle { classname = "rhsusf_M977A4_AMMO_BKIT_M2_usarmy_wd"; level = ANTI_CAR; cost = 62; };

class M977A4B_M2_Repair_D: Vehicle { classname = "rhsusf_M977A4_REPAIR_BKIT_M2_usarmy_d";  level = ANTI_CAR; cost = 62; };
class M977A4B_M2_Repair_W: Vehicle { classname = "rhsusf_M977A4_REPAIR_BKIT_M2_usarmy_wd"; level = ANTI_CAR; cost = 62; };