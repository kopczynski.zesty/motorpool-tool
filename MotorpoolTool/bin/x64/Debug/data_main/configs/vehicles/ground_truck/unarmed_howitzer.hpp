/**
 * FORMULA
 * Artillery Price = (Average Artillery Price / 2)
 * Howizter Truck Cost = ( Standard Truck Cost + Artillery Price)
 * Used Artillery Price: 40
 */

/**
 * M939 5 Ton
 */
class M939_M119_CW: Vehicle { classname = "UK3CB_CW_US_B_LATE_M939"; name = "M939 (M119A2)"; level = ANTI_IFV; cost = 62; loadInCargo[] = {"RHS_M119_WD"}; };

class M939_M119_TAN: Vehicle { classname = "UK3CB_B_M939_Closed_HIDF"; name = "M939 (M119A2)"; level = ANTI_IFV; cost = 62; loadInCargo[] = {"RHS_M119_WD"}; };


/**
 * MTVRs
 */
class MTVR_M119A2_D: Vehicle	{ classname = "UK3CB_B_MTVR_Closed_DES"; name = "MTVR (M119A2)"; level = ANTI_IFV; cost = 64; loadInCargo[] = {"RHS_M119_D"}; };
class MTVR_M119A2_W: Vehicle	{ classname = "UK3CB_B_MTVR_Closed_WDL"; name = "MTVR (M119A2)"; level = ANTI_IFV; cost = 64; loadInCargo[] = {"RHS_M119_WD"}; };


/**
 * FMTVs
 */
class M1078A1P2_M119A2_D: Vehicle  { classname = "rhsusf_M1078A1P2_D_fmtv_usarmy";    name = "M1078A1P2 (M119A2)";   level = ANTI_IFV; cost = 66; loadInCargo[] = {"RHS_M119_D"}; };
class M1078A1P2_M119A2_W: Vehicle  { classname = "rhsusf_M1078A1P2_WD_fmtv_usarmy";   name = "M1078A1P2 (M119A2)";   level = ANTI_IFV; cost = 66; loadInCargo[] = {"RHS_M119_WD"}; };
class M1078A1P2B_M119A2_D: Vehicle { classname = "rhsusf_M1078A1P2_B_D_fmtv_usarmy";  name = "M1078A1P2-B (M119A2)"; level = ANTI_IFV; cost = 72; loadInCargo[] = {"RHS_M119_D"}; };
class M1078A1P2B_M119A2_W: Vehicle { classname = "rhsusf_M1078A1P2_B_WD_fmtv_usarmy"; name = "M1078A1P2-B (M119A2)"; level = ANTI_IFV; cost = 72; loadInCargo[] = {"RHS_M119_WD"}; };


/**
 * MAN HX60
 * L118 is the British designation for the M119A2.
 */
class MANHX60_L118_D: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Transport_Sand";  name = "MAN HX60 4x4 (L118)"; level = ANTI_IFV; cost = 66; loadInCargo[] = {"RHS_M119_D"}; animationData[] = {"CanvasFlap_Hide",1}; };
class MANHX60_L118_W: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Transport_Green"; name = "MAN HX60 4x4 (L118)"; level = ANTI_IFV; cost = 66; loadInCargo[] = {"RHS_M119_WD"}; animationData[] = {"CanvasFlap_Hide",1}; };


/**
 * GAZ-66
 */
class GAZ66_D30A_MSV_D: Vehicle	{ classname = "rhs_gaz66_msv";		 name = "GAZ-66 (D-30A)"; level = ANTI_IFV; cost = 64; loadInCargo[] = {"rhs_D30_msv"}; textureData[] = {"camo",1}; };
class GAZ66_D30A_MSV_W: Vehicle	{ classname = "rhs_gaz66_msv";		 name = "GAZ-66 (D-30A)"; level = ANTI_IFV; cost = 64; loadInCargo[] = {"rhs_D30_msv"}; };
class GAZ66_D30A_VDV_D: Vehicle	{ classname = "rhs_gaz66_vdv";		 name = "GAZ-66 (D-30A)"; level = ANTI_IFV; cost = 64; loadInCargo[] = {"rhs_D30_vdv"}; textureData[] = {"camo",1}; };
class GAZ66_D30A_VDV_W: Vehicle	{ classname = "rhs_gaz66_vdv";		 name = "GAZ-66 (D-30A)"; level = ANTI_IFV; cost = 64; loadInCargo[] = {"rhs_D30_vdv"}; };
class GAZ66_D30A_VMF_D: Vehicle	{ classname = "rhs_gaz66_vmf";		 name = "GAZ-66 (D-30A)"; level = ANTI_IFV; cost = 64; loadInCargo[] = {"rhs_D30_vmf"}; textureData[] = {"camo",1}; };
class GAZ66_D30A_VMF_W: Vehicle	{ classname = "rhs_gaz66_vmf";		 name = "GAZ-66 (D-30A)"; level = ANTI_IFV; cost = 64; loadInCargo[] = {"rhs_D30_vmf"}; };

class GAZ66_D30A_G: Vehicle		{ classname = "rhsgref_ins_g_gaz66"; name = "GAZ-66 (D-30A)"; level = ANTI_IFV; cost = 64; loadInCargo[] = {"rhsgref_ins_g_d30"}; textureData[] = {"standard",1}; };

class GAZ66_D30A_CHE: Vehicle { classname = "rhsgref_ins_gaz66"; name = "GAZ-66 (D-30A)"; level = ANTI_IFV; cost = 64; loadInCargo[] = {"rhsgref_ins_d30"}; };


/**
 * Ural-4320
 */
class Ural4320_D30A_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_EARLY_Ural"; name = "Ural-4320 (D-30A)"; level = ANTI_IFV; cost = 66; loadInCargo[] = {"rhs_D30_msv"}; };

class Ural4320_D30A_CDF: Vehicle { classname = "rhsgref_cdf_b_ural"; name = "Ural-4320 (D-30A)"; level = ANTI_IFV; cost = 66; loadInCargo[] = {"rhsgref_cdf_b_reg_d30"}; };
class Ural4320_D30A_UN: Vehicle  { classname = "UK3CB_UN_I_Ural";    name = "Ural-4320 (D-30A)"; level = ANTI_IFV; cost = 66; loadInCargo[] = {"rhsgref_cdf_b_reg_d30"}; };

class Ural4320_D30A_URS: Vehicle { classname = "UK3CB_Ural_Covered"; name = "Ural-4320 (D-30A)"; level = ANTI_IFV; cost = 66; loadInCargo[] = {"rhs_D30_msv"}; textureData[] = {"SOV2",1}; };


/**
 * Ural-375D (Serbia)
 */
class Ural375D_D30J_G: Vehicle  { classname = "rhssaf_army_ural"; name = "Ural-375D (D30J)"; level = ANTI_IFV; cost = 66; loadInCargo[] = {"rhssaf_army_d30"}; };
class Ural375D_D30J_UN: Vehicle { classname = "rhssaf_un_ural";   name = "Ural-375D (D30J)"; level = ANTI_IFV; cost = 66; loadInCargo[] = {"rhssaf_army_d30"}; };


 /**
 * Praga V33 (Insurgents) 
 */
class PragaV3S_D30A_INS_W: Vehicle { classname = "UK3CB_CCM_O_V3S_Closed";	name = "Praga V3S (D-30A)"; level = ANTI_IFV; cost = 62; loadInCargo[] = {"rhsgref_ins_d30"}; };
class PragaV3S_D30A_INS_D: Vehicle { classname = "UK3CB_TKM_I_V3S_Closed";	name = "Praga V3S (D-30A)"; level = ANTI_IFV; cost = 62; loadInCargo[] = {"UK3CB_TKM_O_D30"}; };

class PragaV3S_D30A_TKA: Vehicle { classname = "UK3CB_TKA_I_V3S_Closed"; name = "Praga V3S (D-30A)"; level = ANTI_IFV; cost = 62; loadInCargo[] = {"UK3CB_TKA_O_D30"}; };
