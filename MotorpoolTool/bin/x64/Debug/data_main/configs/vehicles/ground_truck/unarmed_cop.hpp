/**
 * FORMULA
 * COP Truck Cost = ( Standard Truck Cost + COP Price)
 * COP Price: 40
 */

/**
 * M939 5 Ton
 */
class M939_SBCOP_CW: Vehicle  { classname = "UK3CB_CW_US_B_LATE_M939"; name = "M939 (Sandbag COP)";   level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPSandbagWoodWest"};  };

class M939_SBCOP_TAN_W: Vehicle { classname = "UK3CB_B_M939_Closed_HIDF"; name = "M939 (Sandbag COP)";	level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodWest"}; };
class M939_HBCOP_TAN_W: Vehicle { classname = "UK3CB_B_M939_Closed_HIDF"; name = "M939 (Barricade COP)";	level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPHescoWoodWest"}; };


/**
 * MTVRs
 */
class MTVR_SBCOP_D: Vehicle	{ classname = "UK3CB_B_MTVR_Closed_DES"; name = "MTVR (Sandbag COP)";	level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPSandbagDesertWest"}; };
class MTVR_SBCOP_W: Vehicle	{ classname = "UK3CB_B_MTVR_Closed_WDL"; name = "MTVR (Sandbag COP)";	level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPSandbagWoodWest"}; };


/**
 * FMTVs
 */
class M1078A1P2_SBCOP_D: Vehicle  { classname = "rhsusf_M1078A1P2_D_fmtv_usarmy";  name = "M1078A1P2 (Sandbag COP)";   level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPSandbagDesertWest"}; };
class M1078A1P2_SBCOP_W: Vehicle  { classname = "rhsusf_M1078A1P2_WD_fmtv_usarmy"; name = "M1078A1P2 (Sandbag COP)";   level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPSandbagWoodWest"};  };
class M1083A1P2_SBCOP_D: Vehicle  { classname = "rhsusf_M1083A1P2_D_fmtv_usarmy";  name = "M1083A1P2 (Sandbag COP)";   level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPSandbagTallDesertWest"}; };
class M1083A1P2_SBCOP_W: Vehicle  { classname = "rhsusf_M1083A1P2_WD_fmtv_usarmy"; name = "M1083A1P2 (Sandbag COP)";   level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodWest"}; };
class M1084A1P2_HBCOP_D: Vehicle { classname = "rhsusf_M1084A1P2_D_fmtv_usarmy";   name = "M1084A1P2 (H-Barrier COP)"; level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPHescoDesertWest"}; };
class M1084A1P2_HBCOP_W: Vehicle { classname = "rhsusf_M1084A1P2_WD_fmtv_usarmy";  name = "M1084A1P2 (H-Barrier COP)"; level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPHescoWoodWest"}; };

class M1078A1P2B_SBCOP_D: Vehicle { classname = "rhsusf_M1078A1P2_B_D_fmtv_usarmy";  name = "M1078A1P2-B (Sandbag COP)";   level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPSandbagDesertWest"}; };
class M1078A1P2B_SBCOP_W: Vehicle { classname = "rhsusf_M1078A1P2_B_WD_fmtv_usarmy"; name = "M1078A1P2-B (Sandbag COP)";   level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPSandbagWoodWest"}; };
class M1083A1P2B_SBCOP_D: Vehicle { classname = "rhsusf_M1083A1P2_B_D_fmtv_usarmy";  name = "M1083A1P2-B (Sandbag COP)";   level = UTILITY; cost = 78; loadInCargo[] = {"BWI_Staging_COPSandbagTallDesertWest"}; };
class M1083A1P2B_SBCOP_W: Vehicle { classname = "rhsusf_M1083A1P2_B_WD_fmtv_usarmy"; name = "M1083A1P2-B (Sandbag COP)";   level = UTILITY; cost = 78; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodWest"}; };
class M1084A1P2B_HBCOP_D: Vehicle { classname = "rhsusf_M1084A1P2_B_D_fmtv_usarmy";  name = "M1084A1P2-B (H-Barrier COP)"; level = UTILITY; cost = 78; loadInCargo[] = {"BWI_Staging_COPHescoDesertWest"}; };
class M1084A1P2B_HBCOP_W: Vehicle { classname = "rhsusf_M1084A1P2_B_WD_fmtv_usarmy"; name = "M1084A1P2-B (H-Barrier COP)"; level = UTILITY; cost = 78; loadInCargo[] = {"BWI_Staging_COPHescoWoodWest"}; };


/**
 * HEMTTs
 */
class M977A4_HBCOP_D: Vehicle  { classname = "rhsusf_M977A4_usarmy_d"; 	     name = "M977A4 (H-Barrier COP)";   level = UTILITY; cost = 76; loadInCargo[] = {"BWI_Staging_COPHescoDesertWest"}; };
class M977A4_HBCOP_W: Vehicle  { classname = "rhsusf_M977A4_usarmy_wd"; 	 name = "M977A4 (H-Barrier COP)";   level = UTILITY; cost = 76; loadInCargo[] = {"BWI_Staging_COPHescoWoodWest"}; };
class M977A4B_HBCOP_D: Vehicle { classname = "rhsusf_M977A4_BKIT_usarmy_d";  name = "M977A4-B (H-Barrier COP)"; level = UTILITY; cost = 84; loadInCargo[] = {"BWI_Staging_COPHescoHeavyDesertWest"}; };
class M977A4B_HBCOP_W: Vehicle { classname = "rhsusf_M977A4_BKIT_usarmy_wd"; name = "M977A4-B (H-Barrier COP)"; level = UTILITY; cost = 84; loadInCargo[] = {"BWI_Staging_COPHescoHeavyWoodWest"}; };


/**
 * MAN HX58/60
 */
class MANHX60_SBCOP_D: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Transport_Sand";  name = "MAN HX60 4x4 (Sandbag COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPSandbagDesertWest"}; animationData[] = {"CanvasFlap_Hide",1}; };
class MANHX60_SBCOP_W: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Transport_Green"; name = "MAN HX60 4x4 (Sandbag COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPSandbagWoodWest"};   animationData[] = {"CanvasFlap_Hide",1}; };
class MANHX58_SBCOP_D: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Transport_Sand";  name = "MAN HX58 6x6 (Sandbag COP)"; level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPSandbagTallDesertWest"}; animationData[] = {"CanvasFlap_Hide",1}; };
class MANHX58_SBCOP_W: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Transport_Green"; name = "MAN HX58 6x6 (Sandbag COP)"; level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodWest"};   animationData[] = {"CanvasFlap_Hide",1}; };

class MANHX60_HBCOP_D: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Transport_Sand";  name = "MAN HX60 4x4 (H-Barrier COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPHescoDesertWest"}; animationData[] = {"CanvasFlap_Hide",1}; };
class MANHX60_HBCOP_W: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Transport_Green"; name = "MAN HX60 4x4 (H-Barrier COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPHescoWoodWest"}; animationData[] = {"CanvasFlap_Hide",1}; };
class MANHX58_HBCOP_D: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Transport_Sand";  name = "MAN HX58 6x6 (H-Barrier COP)"; level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPHescoHeavyDesertWest"}; animationData[] = {"CanvasFlap_Hide",1}; };
class MANHX58_HBCOP_W: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Transport_Green"; name = "MAN HX58 6x6 (H-Barrier COP)"; level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPHescoHeavyWoodWest"}; animationData[] = {"CanvasFlap_Hide",1}; };


/**
 * LKW KAT I
 */
class LKW5T_SBCOP_D: Vehicle	{ classname = "rnt_lkw_5t_mil_gl_kat_i_transport_trope";	name = "LKW 5t KAT I (Sandbag COP)";	level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPSandbagDesertWest"}; };
class LKW5T_SBCOP_W: Vehicle	{ classname = "rnt_lkw_5t_mil_gl_kat_i_transport_fleck";	name = "LKW 5t KAT I (Sandbag COP)";	level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPSandbagWoodWest"}; };
class LKW7T_SBCOP_D: Vehicle	{ classname = "rnt_lkw_7t_mil_gl_kat_i_transport_trope";	name = "LKW 7t KAT I (Sandbag COP)";	level = UTILITY; cost = 78; loadInCargo[] = {"BWI_Staging_COPSandbagTallDesertWest"}; };
class LKW7T_SBCOP_W: Vehicle	{ classname = "rnt_lkw_7t_mil_gl_kat_i_transport_fleck";	name = "LKW 7t KAT I (Sandbag COP)";	level = UTILITY; cost = 78; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodWest"}; };

class LKW5T_HBCOP_D: Vehicle	{ classname = "rnt_lkw_5t_mil_gl_kat_i_transport_trope";	name = "LKW 5t KAT I (H-Barrier COP)";	level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPHescoDesertWest"}; };
class LKW5T_HBCOP_W: Vehicle	{ classname = "rnt_lkw_5t_mil_gl_kat_i_transport_fleck";	name = "LKW 5t KAT I (H-Barrier COP)";	level = UTILITY; cost = 72; loadInCargo[] = {"BWI_Staging_COPHescoWoodWest"}; };
class LKW7T_HBCOP_D: Vehicle	{ classname = "rnt_lkw_7t_mil_gl_kat_i_transport_trope";	name = "LKW 7t KAT I (H-Barrier COP)";	level = UTILITY; cost = 78; loadInCargo[] = {"BWI_Staging_COPHescoHeavyDesertWest"}; };
class LKW7T_HBCOP_W: Vehicle	{ classname = "rnt_lkw_7t_mil_gl_kat_i_transport_fleck";	name = "LKW 7t KAT I (H-Barrier COP)";	level = UTILITY; cost = 78; loadInCargo[] = {"BWI_Staging_COPHescoHeavyWoodWest"}; };


/**
 * GAZ-66
 */
class GAZ66_SBCOP_MSV_D: Vehicle { classname = "rhs_gaz66_msv";  	  name = "GAZ-66 (Sandbag COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPSandbagDesertEast"}; textureData[] = {"camo",1}; };
class GAZ66_SBCOP_MSV_W: Vehicle { classname = "rhs_gaz66_msv";  	  name = "GAZ-66 (Sandbag COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPSandbagWoodEast"}; };
class GAZ66_SBCOP_VDV_D: Vehicle { classname = "rhs_gaz66_vdv";  	  name = "GAZ-66 (Sandbag COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPSandbagDesertEast"}; textureData[] = {"camo",1}; };
class GAZ66_SBCOP_VDV_W: Vehicle { classname = "rhs_gaz66_vdv";  	  name = "GAZ-66 (Sandbag COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPSandbagWoodEast"}; };
class GAZ66_SBCOP_VMF_D: Vehicle { classname = "rhs_gaz66_vmf";  	  name = "GAZ-66 (Sandbag COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPSandbagDesertEast"}; textureData[] = {"camo",1}; };
class GAZ66_SBCOP_VMF_W: Vehicle { classname = "rhs_gaz66_vmf";  	  name = "GAZ-66 (Sandbag COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPSandbagWoodEast"}; };
class GAZ66_SBCOP_G: Vehicle 	 { classname = "rhsgref_ins_g_gaz66"; name = "GAZ-66 (Sandbag COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPSandbagWoodEast"};   textureData[] = {"standard",1}; };

class GAZ66_SBCOP_CHE_W: Vehicle { classname = "rhsgref_ins_gaz66"; name = "GAZ-66 (Sandbag COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodEast"}; };
class GAZ66_BBCOP_CHE_W: Vehicle { classname = "rhsgref_ins_gaz66"; name = "GAZ-66 (Barricade COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; };


/**
 * ZiL-131
 */
class ZiL131_BBCOP_MSV_D: Vehicle { classname = "rhs_zil131_msv"; name = "ZiL-131 (Barricade COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPBarricadeDesertEast"}; };
class ZiL131_BBCOP_MSV_W: Vehicle { classname = "rhs_zil131_msv"; name = "ZiL-131 (Barricade COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPBarricadeWoodEast"}; };
class ZiL131_BBCOP_VDV_D: Vehicle { classname = "rhs_zil131_vdv"; name = "ZiL-131 (Barricade COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPBarricadeDesertEast"}; };
class ZiL131_BBCOP_VDV_W: Vehicle { classname = "rhs_zil131_vdv"; name = "ZiL-131 (Barricade COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPBarricadeWoodEast"}; };
class ZiL131_BBCOP_VMF_D: Vehicle { classname = "rhs_zil131_vmf"; name = "ZiL-131 (Barricade COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPBarricadeDesertEast"}; };
class ZiL131_BBCOP_VMF_W: Vehicle { classname = "rhs_zil131_vmf"; name = "ZiL-131 (Barricade COP)"; level = UTILITY; cost = 64; loadInCargo[] = {"BWI_Staging_COPBarricadeWoodEast"}; };


/**
 * Ural-4320
 */
class Ural4320_BBCOP_MSV_D: Vehicle { classname = "RHS_Ural_MSV_01"; name = "Ural-4320 (Barricade COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPBarricadeTallDesertEast"}; };
class Ural4320_BBCOP_MSV_W: Vehicle { classname = "RHS_Ural_MSV_01"; name = "Ural-4320 (Barricade COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; };
class Ural4320_BBCOP_VDV_D: Vehicle { classname = "RHS_Ural_VDV_01"; name = "Ural-4320 (Barricade COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPBarricadeTallDesertEast"}; };
class Ural4320_BBCOP_VDV_W: Vehicle { classname = "RHS_Ural_VDV_01"; name = "Ural-4320 (Barricade COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; };
class Ural4320_BBCOP_VMF_D: Vehicle { classname = "RHS_Ural_VMF_01"; name = "Ural-4320 (Barricade COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPBarricadeTallDesertEast"}; };
class Ural4320_BBCOP_VMF_W: Vehicle { classname = "RHS_Ural_VMF_01"; name = "Ural-4320 (Barricade COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; };

class Ural4320_SBCOP_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_EARLY_Ural"; name = "Ural-4320 (Sandbag COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPBarricadeTallDesertEast"}; };
class Ural4320_BBCOP_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_EARLY_Ural"; name = "Ural-4320 (Barricade COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPBarricadeTallDesertEast"}; };

class Ural4320_SBCOP_CDF_W: Vehicle { classname = "rhsgref_cdf_b_ural"; name = "Ural-4320 (Sandbag COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodEast"}; };
class Ural4320_BBCOP_CDF_W: Vehicle { classname = "rhsgref_cdf_b_ural"; name = "Ural-4320 (Barricade COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; };
class Ural4320_HBCOP_CDF_W: Vehicle { classname = "rhsgref_cdf_b_ural"; name = "Ural-4320 (H-Barrier COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPHescoWoodEast"}; };

class Ural4320_SBCOP_UN_W: Vehicle { classname = "UK3CB_UN_I_Ural"; name = "Ural-4320 (Sandbag COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodEast"}; };
class Ural4320_BBCOP_UN_W: Vehicle { classname = "UK3CB_UN_I_Ural"; name = "Ural-4320 (Barricade COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; };

class Ural4320_SBCOP_URS_W: Vehicle { classname = "UK3CB_Ural_Covered"; name = "Ural-4320 (Sandbag COP)";   level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodEast"};   textureData[] = {"SOV2",1}; };
class Ural4320_BBCOP_URS_W: Vehicle { classname = "UK3CB_Ural_Covered"; name = "Ural-4320 (Barricade COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; textureData[] = {"SOV2",1}; };


/**
 * KamAZ-5350
 */
class KamAZ5350_BBCOP_MSV_D: Vehicle { classname = "rhs_kamaz5350_msv"; name = "KamAZ-5350 (Barricade COP)"; level = UTILITY; cost = 68; loadInCargo[] = {"BWI_Staging_COPBarricadeTallDesertEast"}; };
class KamAZ5350_BBCOP_MSV_W: Vehicle { classname = "rhs_kamaz5350_msv"; name = "KamAZ-5350 (Barricade COP)"; level = UTILITY; cost = 68; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; };
class KamAZ5350_BBCOP_VDV_D: Vehicle { classname = "rhs_kamaz5350_vdv"; name = "KamAZ-5350 (Barricade COP)"; level = UTILITY; cost = 68; loadInCargo[] = {"BWI_Staging_COPBarricadeTallDesertEast"}; };
class KamAZ5350_BBCOP_VDV_W: Vehicle { classname = "rhs_kamaz5350_vdv"; name = "KamAZ-5350 (Barricade COP)"; level = UTILITY; cost = 68; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; };
class KamAZ5350_BBCOP_VMF_D: Vehicle { classname = "rhs_kamaz5350_vmf"; name = "KamAZ-5350 (Barricade COP)"; level = UTILITY; cost = 68; loadInCargo[] = {"BWI_Staging_COPBarricadeTallDesertEast"}; };
class KamAZ5350_BBCOP_VMF_W: Vehicle { classname = "rhs_kamaz5350_vmf"; name = "KamAZ-5350 (Barricade COP)"; level = UTILITY; cost = 68; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; };


/**
 * Ural-375D (Serbia)
 */
class Ural375D_SBCOP_G: Vehicle  { classname = "rhssaf_army_ural"; name = "Ural-375D (Sandbag COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodEast"}; };
class Ural375D_SBCOP_UN: Vehicle { classname = "rhssaf_un_ural";   name = "Ural-375D (Sandbag COP)"; level = UTILITY; cost = 66; loadInCargo[] = {"BWI_Staging_COPSandbagTallDesertEast"}; };


/**
 * Praga V3S
 */
class PragaV3S_TCOP_INS_W: Vehicle { classname = "UK3CB_CCM_O_V3S_Closed"; name = "Praga V3S (Tent COP)"; level = UTILITY; cost = 52; loadInCargo[] = {"BWI_Staging_COPTents"}; };
class PragaV3S_TCOP_INS_D: Vehicle { classname = "UK3CB_TKM_I_V3S_Closed"; name = "Praga V3S (Tent COP)"; level = UTILITY; cost = 52; loadInCargo[] = {"BWI_Staging_COPTents"}; };

class PragaV3S_SBCOP_INS_W: Vehicle { classname = "UK3CB_CCM_O_V3S_Closed"; name = "Praga V3S (Sandbag COP)"; level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodEast"}; };
class PragaV3S_SBCOP_INS_D: Vehicle { classname = "UK3CB_TKM_I_V3S_Closed"; name = "Praga V3S (Sandbag COP)"; level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPSandbagTallDesertEast"}; };

class PragaV3S_BBCOP_INS_W: Vehicle { classname = "UK3CB_CCM_O_V3S_Closed"; name = "Praga V3S (Barricade COP)"; level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; };
class PragaV3S_BBCOP_INS_D: Vehicle { classname = "UK3CB_TKM_I_V3S_Closed"; name = "Praga V3S (Barricade COP)"; level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPBarricadeTallDesertEast"}; };

class PragaV3S_SBCOP_TKA_D: Vehicle { classname = "UK3CB_TKA_I_V3S_Closed"; name = "Praga V3S (Sandbag COP)"; level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPSandbagTallDesertEast"}; };
class PragaV3S_BBCOP_TKA_D: Vehicle { classname = "UK3CB_TKA_I_V3S_Closed"; name = "Praga V3S (Barricade COP)"; level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPBarricadeTallDesertEast"}; };
class PragaV3S_HBCOP_TKA_D: Vehicle { classname = "UK3CB_TKA_I_V3S_Closed"; name = "Praga V3S (H-Barrier COP)"; level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPHescoWoodEast"}; };


/**
 * Iveco Daily Truck
 */
class Iveco_SBCOP_ION_D: Vehicle { classname = "C_Van_01_transport_F"; name = "Iveco Truck (Sandbag COP)";   level = UTILITY; cost = 56; loadInCargo[] = {"BWI_Staging_COPSandbagTallDesertWest"}; textureData[] = {"Black",1}; };
class Iveco_SBCOP_ION_W: Vehicle { classname = "C_Van_01_transport_F"; name = "Iveco Truck (Sandbag COP)";   level = UTILITY; cost = 56; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodWest"}; textureData[] = {"Black",1}; };
class Iveco_HBCOP_ION_D: Vehicle { classname = "C_Van_01_transport_F"; name = "Iveco Truck (H-Barrier COP)"; level = UTILITY; cost = 56; loadInCargo[] = {"BWI_Staging_COPHescoDesertWest"}; textureData[] = {"Black",1}; };
class Iveco_HBCOP_ION_W: Vehicle { classname = "C_Van_01_transport_F"; name = "Iveco Truck (H-Barrier COP)"; level = UTILITY; cost = 56; loadInCargo[] = {"BWI_Staging_COPHescoWoodWest"}; textureData[] = {"Black",1}; };

class KrAZ255B1_SBCOP_BGM_D: Vehicle { classname = "rhsgref_ins_g_kraz255b1_flatbed"; name = "KrAZ-255B1 (Sandbag COP)";   level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPSandbagTallDesertEast"}; textureData[] = {"Standard",1}; };
class KrAZ255B1_SBCOP_BGM_W: Vehicle { classname = "rhsgref_ins_g_kraz255b1_flatbed"; name = "KrAZ-255B1 (Sandbag COP)";   level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPSandbagTallWoodEast"}; textureData[] = {"Standard",1}; };
class KrAZ255B1_BBCOP_BGM_D: Vehicle { classname = "rhsgref_ins_g_kraz255b1_flatbed"; name = "KrAZ-255B1 (Barricade COP)"; level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPBarricadeTallDesertEast"}; textureData[] = {"Standard",1}; };
class KrAZ255B1_BBCOP_BGM_W: Vehicle { classname = "rhsgref_ins_g_kraz255b1_flatbed"; name = "KrAZ-255B1 (Barricade COP)"; level = UTILITY; cost = 62; loadInCargo[] = {"BWI_Staging_COPBarricadeTallWoodEast"}; textureData[] = {"Standard",1}; };