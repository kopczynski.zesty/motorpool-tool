/**
 * M939 5 Ton
 */
class M939_CW: Vehicle			{ classname = "UK3CB_CW_US_B_LATE_M939";          name = "M939 Transport (Closed)"; level = UTILITY; cost = 22; };
class M939_O_CW: Vehicle		{ classname = "UK3CB_CW_US_B_LATE_M939_Open";     name = "M939 Transport (Open)";   level = UTILITY; cost = 22; };
class M939_Recover_CW: Vehicle	{ classname = "UK3CB_CW_US_B_LATE_M939_Recovery"; name = "M939 Flatbed (Recovery)"; level = UTILITY; cost = 22; };
class M939_Fuel_CW: Vehicle		{ classname = "UK3CB_CW_US_B_LATE_M939_Fuel";     name = "M939 (Fuel)";             level = UTILITY; cost = 22; };
class M939_Ammo_CW: Vehicle		{ classname = "UK3CB_CW_US_B_LATE_M939_Reammo";   name = "M939 (Ammo)";             level = UTILITY; cost = 22; };
class M939_Repair_CW: Vehicle	{ classname = "UK3CB_CW_US_B_LATE_M939_Repair";   name = "M939 (Repair)";           level = UTILITY; cost = 22; };

class M939_TAN: Vehicle			{ classname = "UK3CB_B_M939_Closed_HIDF";   name = "M939 Transport (Closed)"; level = UTILITY; cost = 22; };
class M939_O_TAN: Vehicle		{ classname = "UK3CB_B_M939_Open_HIDF";     name = "M939 Transport (Open)";   level = UTILITY; cost = 22; };
class M939_Recover_TAN: Vehicle	{ classname = "UK3CB_B_M939_Recovery_HIDF"; name = "M939 Flatbed (Recovery)"; level = UTILITY; cost = 22; };
class M939_Fuel_TAN: Vehicle	{ classname = "UK3CB_B_M939_Refuel_HIDF";   name = "M939 (Fuel)";             level = UTILITY; cost = 22; };
class M939_Ammo_TAN: Vehicle	{ classname = "UK3CB_B_M939_Reammo_HIDF";   name = "M939 (Ammo)";             level = UTILITY; cost = 22; };
class M939_Repair_TAN: Vehicle	{ classname = "UK3CB_B_M939_Repair_HIDF";   name = "M939 (Repair)";           level = UTILITY; cost = 22; };


/**
 * MTVRs
 */
class MTVR_D: Vehicle	{ classname = "UK3CB_B_MTVR_Closed_DES";		 level = UTILITY; cost = 24; };
class MTVR_W: Vehicle	{ classname = "UK3CB_B_MTVR_Closed_WDL";		 level = UTILITY; cost = 24; };
class MTVR_O_D: Vehicle	{ classname = "UK3CB_B_MTVR_Open_DES";			 level = UTILITY; cost = 24; };
class MTVR_O_W: Vehicle	{ classname = "UK3CB_B_MTVR_Open_WDL";			 level = UTILITY; cost = 24; };
class MTVR_Recover_D: Vehicle { classname = "UK3CB_B_MTVR_Recovery_DES"; level = UTILITY; cost = 24; };
class MTVR_Recover_W: Vehicle { classname = "UK3CB_B_MTVR_Recovery_WDL"; level = UTILITY; cost = 24; };
class MTVR_Ammo_D: Vehicle	  { classname = "UK3CB_B_MTVR_Reammo_DES";	 level = UTILITY; cost = 24; };
class MTVR_Ammo_W: Vehicle	  { classname = "UK3CB_B_MTVR_Reammo_WDL";	 level = UTILITY; cost = 24; };
class MTVR_Fuel_D: Vehicle	  { classname = "UK3CB_B_MTVR_Refuel_DES";	 level = UTILITY; cost = 24; };
class MTVR_Fuel_W: Vehicle	  { classname = "UK3CB_B_MTVR_Refuel_WDL";	 level = UTILITY; cost = 24; };
class MTVR_Repair_D: Vehicle  { classname = "UK3CB_B_MTVR_Repair_DES";	 level = UTILITY; cost = 24; };
class MTVR_Repair_W: Vehicle  { classname = "UK3CB_B_MTVR_Repair_WDL";	 level = UTILITY; cost = 24; };


/**
 * FMTVs
 */
class M1078A1P2_D: Vehicle			{ classname = "rhsusf_M1078A1P2_D_fmtv_usarmy";				level = UTILITY; cost = 26; };
class M1078A1P2_W: Vehicle			{ classname = "rhsusf_M1078A1P2_WD_fmtv_usarmy";			level = UTILITY; cost = 26; };
class M1078A1P2_FB_D: Vehicle		{ classname = "rhsusf_M1078A1P2_D_flatbed_fmtv_usarmy";		level = UTILITY; cost = 26; };
class M1078A1P2_FB_W: Vehicle		{ classname = "rhsusf_M1078A1P2_WD_flatbed_fmtv_usarmy";	level = UTILITY; cost = 26; };
class M1083A1P2_D: Vehicle			{ classname = "rhsusf_M1083A1P2_D_fmtv_usarmy";				level = UTILITY; cost = 32; };
class M1083A1P2_W: Vehicle			{ classname = "rhsusf_M1083A1P2_WD_fmtv_usarmy";			level = UTILITY; cost = 32; };
class M1083A1P2_FB_D: Vehicle		{ classname = "rhsusf_M1083A1P2_D_flatbed_fmtv_usarmy";		level = UTILITY; cost = 32; };
class M1083A1P2_FB_W: Vehicle		{ classname = "rhsusf_M1083A1P2_WD_flatbed_fmtv_usarmy";	level = UTILITY; cost = 32; };
class M1084A1P2_D: Vehicle			{ classname = "rhsusf_M1084A1P2_D_fmtv_usarmy";				level = UTILITY; cost = 32; };
class M1084A1P2_W: Vehicle			{ classname = "rhsusf_M1084A1P2_WD_fmtv_usarmy";			level = UTILITY; cost = 32; };

class M1078A1P2B_D: Vehicle			{ classname = "rhsusf_M1078A1P2_B_D_fmtv_usarmy";			level = UTILITY; cost = 32; };
class M1078A1P2B_W: Vehicle			{ classname = "rhsusf_M1078A1P2_B_WD_fmtv_usarmy";			level = UTILITY; cost = 32; };
class M1078A1P2B_FB_D: Vehicle		{ classname = "rhsusf_M1078A1P2_B_D_flatbed_fmtv_usarmy";	level = UTILITY; cost = 32; };
class M1078A1P2B_FB_W: Vehicle		{ classname = "rhsusf_M1078A1P2_B_WD_flatbed_fmtv_usarmy";	level = UTILITY; cost = 32; };
class M1083A1P2B_D: Vehicle			{ classname = "rhsusf_M1083A1P2_B_D_fmtv_usarmy";			level = UTILITY; cost = 38; };
class M1083A1P2B_W: Vehicle			{ classname = "rhsusf_M1083A1P2_B_WD_fmtv_usarmy";			level = UTILITY; cost = 38; };
class M1083A1P2B_FB_D: Vehicle		{ classname = "rhsusf_M1083A1P2_B_D_flatbed_fmtv_usarmy";	level = UTILITY; cost = 38; };
class M1083A1P2B_FB_W: Vehicle		{ classname = "rhsusf_M1083A1P2_B_WD_flatbed_fmtv_usarmy";	level = UTILITY; cost = 38; };
class M1084A1P2B_D: Vehicle			{ classname = "rhsusf_M1084A1P2_B_D_fmtv_usarmy";			level = UTILITY; cost = 38; };
class M1084A1P2B_W: Vehicle			{ classname = "rhsusf_M1084A1P2_B_WD_fmtv_usarmy";			level = UTILITY; cost = 38; };

class M1078A1P2B_CP_D: Vehicle		{ classname = "rhsusf_M1078A1P2_B_D_CP_fmtv_usarmy";		level = UTILITY; cost = 26; };
class M1078A1P2B_CP_W: Vehicle		{ classname = "rhsusf_M1078A1P2_B_WD_CP_fmtv_usarmy";		level = UTILITY; cost = 26; };
class M1078A1P2B_Medical_D: Vehicle	{ classname = "rhsusf_M1085A1P2_B_D_Medical_fmtv_usarmy";	level = UTILITY; cost = 28; };
class M1078A1P2B_Medical_W: Vehicle	{ classname = "rhsusf_M1085A1P2_B_WD_Medical_fmtv_usarmy";	level = UTILITY; cost = 28; };


/**
 * HEMTTs
 */
class M977A4_D: Vehicle			{ classname = "rhsusf_M977A4_usarmy_d";					level = UTILITY; cost = 36; };
class M977A4_W: Vehicle			{ classname = "rhsusf_M977A4_usarmy_wd";				level = UTILITY; cost = 36; };
class M977A4B_D: Vehicle		{ classname = "rhsusf_M977A4_BKIT_usarmy_d";			level = UTILITY; cost = 44; };
class M977A4B_W: Vehicle		{ classname = "rhsusf_M977A4_BKIT_usarmy_wd";			level = UTILITY; cost = 44; };

class M978A4_Fuel_D: Vehicle	{ classname = "rhsusf_M978A4_usarmy_d";					level = UTILITY; cost = 36; };
class M978A4_Fuel_W: Vehicle	{ classname = "rhsusf_M978A4_usarmy_wd";				level = UTILITY; cost = 36; };
class M978A4B_Fuel_D: Vehicle	{ classname = "rhsusf_M978A4_BKIT_usarmy_d";			level = UTILITY; cost = 44; };
class M978A4B_Fuel_W: Vehicle	{ classname = "rhsusf_M978A4_BKIT_usarmy_wd";			level = UTILITY; cost = 44; };

class M977A4_Ammo_D: Vehicle	{ classname = "rhsusf_M977A4_AMMO_usarmy_d";			level = UTILITY; cost = 36; };
class M977A4_Ammo_W: Vehicle	{ classname = "rhsusf_M977A4_AMMO_usarmy_wd";			level = UTILITY; cost = 36; };
class M977A4B_Ammo_D: Vehicle	{ classname = "rhsusf_M977A4_AMMO_BKIT_usarmy_d";		level = UTILITY; cost = 44; };
class M977A4B_Ammo_W: Vehicle	{ classname = "rhsusf_M977A4_AMMO_BKIT_usarmy_wd";		level = UTILITY; cost = 44; };

class M977A4_Repair_D: Vehicle	{ classname = "rhsusf_M977A4_REPAIR_usarmy_d";			level = UTILITY; cost = 36; };
class M977A4_Repair_W: Vehicle	{ classname = "rhsusf_M977A4_REPAIR_usarmy_wd";			level = UTILITY; cost = 36; };
class M977A4B_Repair_D: Vehicle	{ classname = "rhsusf_M977A4_REPAIR_BKIT_usarmy_d";		level = UTILITY; cost = 44; };
class M977A4B_Repair_W: Vehicle	{ classname = "rhsusf_M977A4_REPAIR_BKIT_usarmy_wd";	level = UTILITY; cost = 44; };


/**
 * MAN HX58/60
 */
class MANHX60_D: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Transport_Sand";  name = "MAN HX60 4x4 Transport"; level = UTILITY; cost = 26; animationData[] = {"CanvasFlap_Hide",1}; };
class MANHX60_W: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Transport_Green"; name = "MAN HX60 4x4 Transport"; level = UTILITY; cost = 26; animationData[] = {"CanvasFlap_Hide",1}; };
class MANHX58_D: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Transport_Sand";  name = "MAN HX58 6x6 Transport"; level = UTILITY; cost = 32; animationData[] = {"CanvasFlap_Hide",1}; };
class MANHX58_W: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Transport_Green"; name = "MAN HX58 6x6 Transport"; level = UTILITY; cost = 32; animationData[] = {"CanvasFlap_Hide",1}; };

class MANHX60_FB_D: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Cargo_Sand_A";  name = "MAN HX60 4x4 Flatbed"; level = UTILITY; cost = 26; };
class MANHX60_FB_W: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Cargo_Green_A"; name = "MAN HX60 4x4 Flatbed"; level = UTILITY; cost = 26; };
class MANHX58_FB_D: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Cargo_Sand_A";  name = "MAN HX58 6x6 Flatbed"; level = UTILITY; cost = 32; };
class MANHX58_FB_W: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Cargo_Green_A"; name = "MAN HX58 6x6 Flatbed"; level = UTILITY; cost = 32; };

class MANHX60_Fuel_D: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Fuel_Sand";  name = "MAN HX60 4x4 Fuel"; level = UTILITY; cost = 26; };
class MANHX60_Fuel_W: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Fuel_Green"; name = "MAN HX60 4x4 Fuel"; level = UTILITY; cost = 26; };
class MANHX58_Fuel_D: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Fuel_Sand";  name = "MAN HX58 6x6 Fuel"; level = UTILITY; cost = 32; };
class MANHX58_Fuel_W: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Fuel_Green"; name = "MAN HX58 6x6 Fuel"; level = UTILITY; cost = 32; };

class MANHX60_Repair_D: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Repair_Sand";  name = "MAN HX60 4x4 Rearm/Repair";  level = UTILITY; cost = 26; };
class MANHX60_Repair_W: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Repair_Green"; name = "MAN HX60 4x4 Rearm/Repair"; level = UTILITY; cost = 26; };
class MANHX58_Repair_D: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Repair_Sand";  name = "MAN HX58 6x6 Rearm/Repair";  level = UTILITY; cost = 32; };
class MANHX58_Repair_W: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Repair_Green"; name = "MAN HX58 6x6 Rearm/Repair"; level = UTILITY; cost = 32; };


/**
 * LKW KAT I
 */
class LKW5T_D: Vehicle			{ classname = "rnt_lkw_5t_mil_gl_kat_i_transport_trope";	name = "LKW 5t KAT I";			level = UTILITY; cost = 32; };
class LKW5T_W: Vehicle			{ classname = "rnt_lkw_5t_mil_gl_kat_i_transport_fleck";	name = "LKW 5t KAT I";			level = UTILITY; cost = 32; };

class LKW7T_D: Vehicle			{ classname = "rnt_lkw_7t_mil_gl_kat_i_transport_trope";	name = "LKW 7t KAT I";			level = UTILITY; cost = 38; };
class LKW7T_W: Vehicle			{ classname = "rnt_lkw_7t_mil_gl_kat_i_transport_fleck";	name = "LKW 7t KAT I";			level = UTILITY; cost = 38; };

class LKW5T_Fuel_D: Vehicle		{ classname = "rnt_lkw_5t_mil_gl_kat_i_fuel_trope";			name = "LKW 5t KAT I Fuel";		level = UTILITY; cost = 32; };
class LKW5T_Fuel_W: Vehicle		{ classname = "rnt_lkw_5t_mil_gl_kat_i_fuel_fleck";			name = "LKW 5t KAT I Fuel";		level = UTILITY; cost = 32; };

class LKW7T_Ammo_D: Vehicle		{ classname = "rnt_lkw_7t_mil_gl_kat_i_mun_trope";			name = "LKW 7t KAT I Ammo";		level = UTILITY; cost = 38; };
class LKW7T_Ammo_W: Vehicle		{ classname = "rnt_lkw_7t_mil_gl_kat_i_mun_fleck";			name = "LKW 7t KAT I Ammo";		level = UTILITY; cost = 38; };

class LKW10T_Repair_D: Vehicle	{ classname = "rnt_lkw_10t_mil_gl_kat_i_repair_trope";		name = "LKW 10t KAT I Repair";	level = UTILITY; cost = 38; };
class LKW10T_Repair_W: Vehicle	{ classname = "rnt_lkw_10t_mil_gl_kat_i_repair_fleck";		name = "LKW 10t KAT I Repair";	level = UTILITY; cost = 38; };


/**
 * GAZ-66
 */
class GAZ66_MSV_D: Vehicle		{ classname = "rhs_gaz66_msv";			level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_MSV_W: Vehicle		{ classname = "rhs_gaz66_msv";			level = UTILITY; cost = 24; };
class GAZ66_FB_MSV_D: Vehicle	{ classname = "rhs_gaz66_flat_msv";		level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_FB_MSV_W: Vehicle	{ classname = "rhs_gaz66_flat_msv";		level = UTILITY; cost = 24; };
class GAZ66_O_MSV_D: Vehicle	{ classname = "rhs_gaz66o_msv";			level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_O_MSV_W: Vehicle	{ classname = "rhs_gaz66o_msv";			level = UTILITY; cost = 24; };
class GAZ66_O_FB_MSV_D: Vehicle	{ classname = "rhs_gaz66o_flat_msv";	level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_O_FB_MSV_W: Vehicle	{ classname = "rhs_gaz66o_flat_msv";	level = UTILITY; cost = 24; };
class GAZ66_Ammo_MSV_D: Vehicle	{ classname = "rhs_gaz66_ammo_msv";		level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_Ammo_MSV_W: Vehicle	{ classname = "rhs_gaz66_ammo_msv";		level = UTILITY; cost = 24; };

class GAZ66_VDV_D: Vehicle		{ classname = "rhs_gaz66_vdv";			level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_VDV_W: Vehicle		{ classname = "rhs_gaz66_vdv";			level = UTILITY; cost = 24; };
class GAZ66_FB_VDV_D: Vehicle	{ classname = "rhs_gaz66_flat_vdv";		level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_FB_VDV_W: Vehicle	{ classname = "rhs_gaz66_flat_vdv";		level = UTILITY; cost = 24; };
class GAZ66_O_VDV_D: Vehicle	{ classname = "rhs_gaz66o_vdv";			level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_O_VDV_W: Vehicle	{ classname = "rhs_gaz66o_vdv";			level = UTILITY; cost = 24; };
class GAZ66_O_FB_VDV_D: Vehicle	{ classname = "rhs_gaz66o_flat_vdv";	level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_O_FB_VDV_W: Vehicle	{ classname = "rhs_gaz66o_flat_vdv";	level = UTILITY; cost = 24; };
class GAZ66_Ammo_VDV_D: Vehicle	{ classname = "rhs_gaz66_ammo_vdv";		level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_Ammo_VDV_W: Vehicle	{ classname = "rhs_gaz66_ammo_vdv";		level = UTILITY; cost = 24; };

class GAZ66_VMF_D: Vehicle		{ classname = "rhs_gaz66_vmf";			level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_VMF_W: Vehicle		{ classname = "rhs_gaz66_vmf";			level = UTILITY; cost = 24; };
class GAZ66_FB_VMF_D: Vehicle	{ classname = "rhs_gaz66_flat_vmf";		level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_FB_VMF_W: Vehicle	{ classname = "rhs_gaz66_flat_vmf";		level = UTILITY; cost = 24; };
class GAZ66_O_VMF_D: Vehicle	{ classname = "rhs_gaz66o_vmf";			level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_O_VMF_W: Vehicle	{ classname = "rhs_gaz66o_vmf";			level = UTILITY; cost = 24; };
class GAZ66_O_FB_VMF_D: Vehicle	{ classname = "rhs_gaz66o_flat_vmf";	level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_O_FB_VMF_W: Vehicle	{ classname = "rhs_gaz66o_flat_vmf";	level = UTILITY; cost = 24; };
class GAZ66_Ammo_VMF_D: Vehicle	{ classname = "rhs_gaz66_ammo_vmf";		level = UTILITY; cost = 24; textureData[] = {"camo",1}; };
class GAZ66_Ammo_VMF_W: Vehicle	{ classname = "rhs_gaz66_ammo_vmf";		level = UTILITY; cost = 24; };

class GAZ66_G: Vehicle		{ classname = "rhsgref_ins_g_gaz66";		level = UTILITY; cost = 24; textureData[] = {"standard",1}; };
class GAZ66_FB_G: Vehicle	{ classname = "rhsgref_ins_g_gaz66_flat";	level = UTILITY; cost = 24; textureData[] = {"standard",1}; };
class GAZ66_O_G: Vehicle	{ classname = "rhsgref_ins_g_gaz66o";		level = UTILITY; cost = 24; textureData[] = {"standard",1}; };
class GAZ66_O_FB_G: Vehicle	{ classname = "rhsgref_ins_g_gaz66o_flat";	level = UTILITY; cost = 24; textureData[] = {"standard",1}; };
class GAZ66_Ammo_G: Vehicle	{ classname = "rhsgref_ins_g_gaz66_ammo";	level = UTILITY; cost = 24; textureData[] = {"standard",1}; };

class GAZ66_CDF: Vehicle		{ classname = "rhsgref_cdf_gaz66";		  level = UTILITY; cost = 24; };
class GAZ66_O_CDF: Vehicle		{ classname = "rhsgref_cdf_gaz66o";		  level = UTILITY; cost = 24; };
class GAZ66_FB_CDF: Vehicle		{ classname = "rhsgref_cdf_gaz66_flat";	  level = UTILITY; cost = 24; };
class GAZ66_O_FB_CDF: Vehicle	{ classname = "rhsgref_cdf_gaz66o_flat";  level = UTILITY; cost = 24; };
class GAZ66_Ammo_CDF: Vehicle	{ classname = "rhsgref_cdf_gaz66_ammo";	  level = UTILITY; cost = 24; };
class GAZ66_Repair_CDF: Vehicle { classname = "rhsgref_cdf_gaz66_repair"; name = "GAZ-66 (Repair)";	level = UTILITY; cost = 24; };

class GAZ66_CHE: Vehicle		{ classname = "rhsgref_ins_gaz66";		   level = UTILITY; cost = 24; };
class GAZ66_O_CHE: Vehicle		{ classname = "rhsgref_ins_gaz66o";		   level = UTILITY; cost = 24; };
class GAZ66_FB_CHE: Vehicle		{ classname = "rhsgref_ins_gaz66o_flat";   level = UTILITY; cost = 24; };
class GAZ66_O_FB_CHE: Vehicle 	{ classname = "rhsgref_ins_g_gaz66o_flat"; level = UTILITY; cost = 24; };
class GAZ66_Ammo_CHE: Vehicle	{ classname = "rhsgref_ins_gaz66_ammo";    level = UTILITY; cost = 24; };
class GAZ66_Repair_CHE: Vehicle { classname = "rhsgref_ins_gaz66_repair"; name = "GAZ-66 (Repair)";	level = UTILITY; cost = 24; };


/**
 * ZiL-131
 */
class ZiL131_MSV: Vehicle		{ classname = "rhs_zil131_msv";					level = UTILITY; cost = 24; };
class ZiL131_FB_MSV: Vehicle	{ classname = "rhs_zil131_flatbed_cover_msv";	level = UTILITY; cost = 24; };
class ZiL131_O_MSV: Vehicle		{ classname = "rhs_zil131_open_msv";			level = UTILITY; cost = 24; };
class ZiL131_O_FB_MSV: Vehicle	{ classname = "rhs_zil131_flatbed_msv";			level = UTILITY; cost = 24; };

class ZiL131_VDV: Vehicle		{ classname = "rhs_zil131_vdv";					level = UTILITY; cost = 24; };
class ZiL131_FB_VDV: Vehicle	{ classname = "rhs_zil131_flatbed_cover_vdv";	level = UTILITY; cost = 24; };
class ZiL131_O_VDV: Vehicle		{ classname = "rhs_zil131_open_vdv";			level = UTILITY; cost = 24; };
class ZiL131_O_FB_VDV: Vehicle	{ classname = "rhs_zil131_flatbed_vdv";			level = UTILITY; cost = 24; };

class ZiL131_VMF: Vehicle		{ classname = "rhs_zil131_vmf";					level = UTILITY; cost = 24; };
class ZiL131_FB_VMF: Vehicle	{ classname = "rhs_zil131_flatbed_cover_vmf";	level = UTILITY; cost = 24; };
class ZiL131_O_VMF: Vehicle		{ classname = "rhs_zil131_open_vmf";			level = UTILITY; cost = 24; };
class ZiL131_O_FB_VMF: Vehicle	{ classname = "rhs_zil131_flatbed_vmf";			level = UTILITY; cost = 24; };


/**
 * Ural-4320
 */
class Ural4320_MSV: Vehicle			{ classname = "RHS_Ural_MSV_01";			level = UTILITY; cost = 26; };
class Ural4320_FB_MSV: Vehicle		{ classname = "RHS_Ural_Flat_MSV_01";		level = UTILITY; cost = 26; };
class Ural4320_O_MSV: Vehicle		{ classname = "RHS_Ural_Open_MSV_01";		level = UTILITY; cost = 26; };
class Ural4320_O_FB_MSV: Vehicle	{ classname = "RHS_Ural_Open_Flat_MSV_01";	level = UTILITY; cost = 26; };
class Ural4320_Fuel_MSV: Vehicle	{ classname = "RHS_Ural_Fuel_MSV_01";		level = UTILITY; cost = 26; };
class Ural4320_Repair_MSV: Vehicle	{ classname = "RHS_Ural_Repair_MSV_01";		level = UTILITY; cost = 26; };

class Ural4320_VDV: Vehicle			{ classname = "RHS_Ural_VDV_01";			level = UTILITY; cost = 26; };
class Ural4320_FB_VDV: Vehicle		{ classname = "RHS_Ural_Flat_VDV_01";		level = UTILITY; cost = 26; };
class Ural4320_O_VDV: Vehicle		{ classname = "RHS_Ural_Open_VDV_01";		level = UTILITY; cost = 26; };
class Ural4320_O_FB_VDV: Vehicle	{ classname = "RHS_Ural_Open_Flat_VDV_01";	level = UTILITY; cost = 26; };
class Ural4320_Fuel_VDV: Vehicle	{ classname = "RHS_Ural_Fuel_VDV_01";		level = UTILITY; cost = 26; };
class Ural4320_Repair_VDV: Vehicle	{ classname = "RHS_Ural_Repair_VDV_01";		level = UTILITY; cost = 26; };

class Ural4320_VMF: Vehicle			{ classname = "RHS_Ural_VMF_01";			level = UTILITY; cost = 26; };
class Ural4320_FB_VMF: Vehicle		{ classname = "RHS_Ural_Flat_VMF_01";		level = UTILITY; cost = 26; };
class Ural4320_O_VMF: Vehicle		{ classname = "RHS_Ural_Open_VMF_01";		level = UTILITY; cost = 26; };
class Ural4320_O_FB_VMF: Vehicle	{ classname = "RHS_Ural_Open_Flat_VMF_01";	level = UTILITY; cost = 26; };
class Ural4320_Fuel_VMF: Vehicle	{ classname = "RHS_Ural_Fuel_VMF_01";		level = UTILITY; cost = 26; };
class Ural4320_Repair_VMF: Vehicle	{ classname = "RHS_Ural_Repair_VMF_01";		level = UTILITY; cost = 26; };

class Ural4320_SOV: Vehicle			{ classname = "UK3CB_CW_SOV_O_EARLY_Ural";			level = UTILITY; cost = 26; };
class Ural4320_O_SOV: Vehicle		{ classname = "UK3CB_CW_SOV_O_EARLY_Ural_Open";		level = UTILITY; cost = 26; };
class Ural4320_Recover_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_EARLY_Ural_Recovery"; level = UTILITY; cost = 26; };
class Ural4320_Fuel_SOV: Vehicle	{ classname = "UK3CB_CW_SOV_O_EARLY_Ural_Fuel";		level = UTILITY; cost = 26; };
class Ural4320_Ammo_SOV: Vehicle	{ classname = "UK3CB_CW_SOV_O_EARLY_Ural_Ammo"; 	level = UTILITY; cost = 26; };
class Ural4320_Repair_SOV: Vehicle	{ classname = "UK3CB_CW_SOV_O_EARLY_Ural_Repair";	level = UTILITY; cost = 26; };

class Ural4320_CDF: Vehicle			{ classname = "rhsgref_cdf_b_ural";			level = UTILITY; cost = 26; };
class Ural4320_O_CDF: Vehicle		{ classname = "rhsgref_cdf_b_ural_open";	level = UTILITY; cost = 26; };
class Ural4320_Recover_CDF: Vehicle { classname = "UK3CB_I_Ural_Recovery_CDF";	level = UTILITY; cost = 26; };
class Ural4320_Fuel_CDF: Vehicle	{ classname = "rhsgref_cdf_b_ural_fuel";	level = UTILITY; cost = 26; };
class Ural4320_Repair_CDF: Vehicle	{ classname = "UK3CB_B_Ural_Repair_CDF";	level = UTILITY; cost = 26; };
class Ural4320_Ammo_CDF: Vehicle	{ classname = "UK3CB_B_Ural_Ammo_CDF";		level = UTILITY; cost = 26; };

class Ural4320_UN: Vehicle			{ classname = "UK3CB_UN_I_Ural";			level = UTILITY; cost = 26; };
class Ural4320_O_UN: Vehicle		{ classname = "UK3CB_UN_I_Ural_Open";		level = UTILITY; cost = 26; };
class Ural4320_Recover_UN: Vehicle  { classname = "UK3CB_UN_I_Ural_Recovery";	level = UTILITY; cost = 26; };
class Ural4320_Fuel_UN: Vehicle		{ classname = "UK3CB_UN_I_Ural_Fuel";		level = UTILITY; cost = 26; };
class Ural4320_Repair_UN: Vehicle	{ classname = "UK3CB_UN_I_Ural_Repair";		level = UTILITY; cost = 26; };
class Ural4320_Ammo_UN: Vehicle		{ classname = "UK3CB_UN_I_Ural_Ammo";		level = UTILITY; cost = 26; };

class Ural4320_Fuel_CHE: Vehicle	{ classname = "RHS_Ural_Fuel_VV_01";			level = UTILITY; cost = 26;  textureData[] = {"Camo2",1}; };
class Ural4320_Recover_CHE: Vehicle { classname = "UK3CB_I_G_Ural_Recovery_CHK";	level = UTILITY; cost = 26; };

class Ural4320_URS: Vehicle			{ classname = "UK3CB_Ural_Covered";  level = UTILITY; cost = 26; textureData[] = {"SOV2",1}; };
class Ural4320_O_URS: Vehicle		{ classname = "UK3CB_Ural_Open";     level = UTILITY; cost = 26; textureData[] = {"SOV2",1}; };
class Ural4320_Recover_URS: Vehicle { classname = "UK3CB_Ural_Recovery"; level = UTILITY; cost = 26; textureData[] = {"SOV2",1}; };
class Ural4320_Fuel_URS: Vehicle	{ classname = "UK3CB_Ural_Fuel";     level = UTILITY; cost = 26; textureData[] = {"SOV2",1}; };
class Ural4320_Repair_URS: Vehicle	{ classname = "UK3CB_Ural_Repair";   level = UTILITY; cost = 26; textureData[] = {"SOV2",1}; };
class Ural4320_Ammo_URS: Vehicle	{ classname = "UK3CB_Ural_Ammo";     level = UTILITY; cost = 26; textureData[] = {"SOV2",1}; };


/**
 * KamAZ-5350
 */
class KamAZ5350_MSV: Vehicle		{ classname = "rhs_kamaz5350_msv";					level = UTILITY; cost = 28; };
class KamAZ5350_FB_MSV: Vehicle		{ classname = "rhs_kamaz5350_flatbed_cover_msv";	level = UTILITY; cost = 28; };
class KamAZ5350_O_MSV: Vehicle		{ classname = "rhs_kamaz5350_open_msv";				level = UTILITY; cost = 28; };
class KamAZ5350_O_FB_MSV: Vehicle	{ classname = "rhs_kamaz5350_flatbed_msv";			level = UTILITY; cost = 28; };

class KamAZ5350_VDV: Vehicle		{ classname = "rhs_kamaz5350_vdv";					level = UTILITY; cost = 28; };
class KamAZ5350_FB_VDV: Vehicle		{ classname = "rhs_kamaz5350_flatbed_cover_vdv";	level = UTILITY; cost = 28; };
class KamAZ5350_O_VDV: Vehicle		{ classname = "rhs_kamaz5350_open_vdv";				level = UTILITY; cost = 28; };
class KamAZ5350_O_FB_VDV: Vehicle	{ classname = "rhs_kamaz5350_flatbed_vdv";			level = UTILITY; cost = 28; };

class KamAZ5350_VMF: Vehicle		{ classname = "rhs_kamaz5350_vmf";					level = UTILITY; cost = 28; };
class KamAZ5350_FB_VMF: Vehicle		{ classname = "rhs_kamaz5350_flatbed_cover_vmf";	level = UTILITY; cost = 28; };
class KamAZ5350_O_VMF: Vehicle		{ classname = "rhs_kamaz5350_open_vmf";				level = UTILITY; cost = 28; };
class KamAZ5350_O_FB_VMF: Vehicle	{ classname = "rhs_kamaz5350_flatbed_vmf";			level = UTILITY; cost = 28; };

class KamAZ5350_CDF: Vehicle		{ classname = "UK3CB_B_Kamaz_Covered_CDF";	level = UTILITY; cost = 28; };
class KamAZ5350_O_CDF: Vehicle		{ classname = "UK3CB_B_Kamaz_Open_CDF";		level = UTILITY; cost = 28; };


/**
 * PTS-M (Russia VMF/Serbia)
 */
class PTSM_VMF: Vehicle	{ classname = "rhs_pts_vmf";		level = UTILITY; cost = 18; };
class PTSM_G: Vehicle	{ classname = "rhssaf_army_pts";	level = UTILITY; cost = 18; };


/**
 * Ural-375D (Serbia)
 */
class Ural375D_G: Vehicle		  { classname = "rhssaf_army_ural";						level = UTILITY; cost = 28; };
class Ural375D_O_G: Vehicle		  { classname = "rhssaf_army_ural_open";				level = UTILITY; cost = 28; };
class Ural375D_Recover_G: Vehicle { classname = "UK3CB_CW_SOV_O_EARLY_Ural_Recovery";	level = UTILITY; cost = 28; };
class Ural375D_Fuel_G: Vehicle	  { classname = "rhssaf_army_ural_fuel";				level = UTILITY; cost = 28; };

class Ural375D_UN: Vehicle		   { classname = "rhssaf_un_ural";				level = UTILITY; cost = 28; };
class Ural375D_Ammo_UN: Vehicle	   { classname = "UK3CB_UN_I_Ural_Ammo";		level = UTILITY; cost = 28; };
class Ural375D_Recover_UN: Vehicle { classname = "UK3CB_UN_I_Ural_Recovery";	level = UTILITY; cost = 28; };
class Ural375D_Fuel_UN: Vehicle	   { classname = "UK3CB_UN_I_Ural_Fuel";		level = UTILITY; cost = 28; };


/**
 * Praga V3S
 */
class PragaV3S_INS_W: Vehicle			{ classname = "UK3CB_CCM_O_V3S_Closed";		level = UTILITY; cost = 22; };
class PragaV3S_O_INS_W: Vehicle			{ classname = "UK3CB_CCM_O_V3S_Open";		level = UTILITY; cost = 22; };
class PragaV3S_Recover_INS_W: Vehicle	{ classname = "UK3CB_CHC_O_V3S_Recovery";	level = UTILITY; cost = 22; };
class PragaV3S_Fuel_INS_W: Vehicle		{ classname = "UK3CB_CCM_O_V3S_Refuel";		level = UTILITY; cost = 22; };
class PragaV3S_Repair_INS_W: Vehicle	{ classname = "UK3CB_CCM_O_V3S_Repair";		level = UTILITY; cost = 22; };
class PragaV3S_Ammo_INS_W: Vehicle		{ classname = "UK3CB_CCM_O_V3S_Reammo";		level = UTILITY; cost = 22; };

class PragaV3S_INS_D: Vehicle			{ classname = "UK3CB_TKM_I_V3S_Closed";		level = UTILITY; cost = 22; };
class PragaV3S_O_INS_D: Vehicle			{ classname = "UK3CB_TKM_I_V3S_Open";		level = UTILITY; cost = 22; };
class PragaV3S_Recover_INS_D: Vehicle	{ classname = "UK3CB_TKM_I_V3S_Recovery";	level = UTILITY; cost = 22; };
class PragaV3S_Fuel_INS_D: Vehicle		{ classname = "UK3CB_TKM_I_V3S_Refuel";		level = UTILITY; cost = 22; };
class PragaV3S_Repair_INS_D: Vehicle	{ classname = "UK3CB_TKM_I_V3S_Repair";		level = UTILITY; cost = 22; };
class PragaV3S_Ammo_INS_D: Vehicle		{ classname = "UK3CB_TKM_I_V3S_Reammo";		level = UTILITY; cost = 22; };

class PragaV3S_TKA: Vehicle			{ classname = "UK3CB_TKA_I_V3S_Closed";   level = UTILITY; cost = 22; };
class PragaV3S_O_TKA: Vehicle		{ classname = "UK3CB_TKA_I_V3S_Open";     level = UTILITY; cost = 22; };
class PragaV3S_Recover_TKA: Vehicle	{ classname = "UK3CB_TKA_I_V3S_Recovery"; level = UTILITY; cost = 22; };
class PragaV3S_Fuel_TKA: Vehicle	{ classname = "UK3CB_TKA_I_V3S_Refuel";   level = UTILITY; cost = 22; };
class PragaV3S_Repair_TKA: Vehicle	{ classname = "UK3CB_TKA_I_V3S_Repair";   level = UTILITY; cost = 22; };
class PragaV3S_Ammo_TKA: Vehicle	{ classname = "UK3CB_TKA_I_V3S_Reammo";   level = UTILITY; cost = 22; };


/**
 * Ikarus
 */
class Ikarus_INS_W: Vehicle { classname = "UK3CB_CCM_I_Ikarus";		level = UTILITY; cost = 24; };
class Ikarus_INS_D: Vehicle { classname = "UK3CB_TKC_I_Ikarus";		level = UTILITY; cost = 24; };


/**
 * Iveco Daily Truck
 */
class Iveco_ION: Vehicle	  { classname = "C_Van_01_transport_F"; name = "Iveco Truck (Transport)"; level = UTILITY; cost = 16; textureData[] = {"Black",1}; };
class Iveco_Fuel_ION: Vehicle { classname = "C_Van_01_fuel_F";	    name = "Iveco Truck (Fuel)";	  level = UTILITY; cost = 16; textureData[] = {"Black",1}; };
class Iveco_Ammo_ION: Vehicle { classname = "C_Van_01_box_ammo_F";  name = "Iveco Truck (Ammo)";	  level = UTILITY; cost = 16; textureData[] = {"Black",1}; };
class Iveco_Fuel_BGM: Vehicle { classname = "rhsgref_nat_van_fuel"; name = "Iveco Truck (Fuel)";	  level = UTILITY; cost = 16; };
class Iveco_Ammo_BGM: Vehicle { classname = "C_Van_01_box_ammo_F";  name = "Iveco Truck (Ammo)";	  level = UTILITY; cost = 16; textureData[] = {"White",1}; };


/**
 * KrAZ-255B1 Truck
 */
class KrAZ255B1_BGM: Vehicle 	{ classname = "rhsgref_ins_g_kraz255b1_cargo_open";	level = UTILITY; cost = 22; textureData[] = {"Standard",1}; };
class KrAZ255B1_FB_BGM: Vehicle { classname = "rhsgref_ins_g_kraz255b1_flatbed";	level = UTILITY; cost = 22; textureData[] = {"Standard",1}; };
