/**
 * M6 Linebacker
 */
class M6_D: Vehicle { classname = "RHS_M6";    level = ANTI_APC; cost = 80; animationData[] = {"IFF_Panels_Hide",1}; };
class M6_W: Vehicle { classname = "RHS_M6_wd"; level = ANTI_APC; cost = 80; animationData[] = {"IFF_Panels_Hide",1}; textureData[] = {"standard",1}; };


/**
 * Gepard 1A2
 */
class Gepard1A2_D: Vehicle { classname = "Redd_Tank_Gepard_1A2_Tropentarn"; level = ANTI_APC; cost = 96; crew = 3; };
class Gepard1A2_W: Vehicle { classname = "Redd_Tank_Gepard_1A2_Flecktarn";  level = ANTI_APC; cost = 96; crew = 3; };
class Gepard1A2_A: Vehicle { classname = "Redd_Tank_Gepard_1A2_Wintertarn"; level = ANTI_APC; cost = 96; crew = 3; };


/**
 * Praga V3S ZU-23
 */
class PragaV3S_ZU23_INS_W: Vehicle	{ classname = "UK3CB_TKM_O_V3S_Zu23";	level = ANTI_APC; cost = 56; };
class PragaV3S_ZU23_INS_D: Vehicle	{ classname = "UK3CB_TKM_I_V3S_Zu23";	level = ANTI_APC; cost = 56; };

class PragaV3S_ZU23_TKA: Vehicle	{ classname = "UK3CB_TKA_I_V3S_Zu23";	level = ANTI_APC; cost = 56; };


/**
 * GAZ-66 ZU-23
 */
class GAZ66_ZU23_MSV_D: Vehicle { classname = "rhs_gaz66_zu23_msv";  level = ANTI_APC; cost = 60; textureData[] = {"camo",1}; };
class GAZ66_ZU23_MSV_W: Vehicle { classname = "rhs_gaz66_zu23_msv";  level = ANTI_APC; cost = 60; };
class GAZ66_ZU23_VDV_D: Vehicle { classname = "rhs_gaz66_zu23_vdv";  level = ANTI_APC; cost = 60; textureData[] = {"camo",1}; };
class GAZ66_ZU23_VDV_W: Vehicle { classname = "rhs_gaz66_zu23_vdv";  level = ANTI_APC; cost = 60; };
class GAZ66_ZU23_VMF_D: Vehicle { classname = "rhs_gaz66_zu23_vmf";  level = ANTI_APC; cost = 60; textureData[] = {"camo",1}; };
class GAZ66_ZU23_VMF_W: Vehicle { classname = "rhs_gaz66_zu23_vmf";  level = ANTI_APC; cost = 60; };

class GAZ66_ZU23_CDF: Vehicle	{ classname = "rhsgref_cdf_gaz66_zu23";	level = ANTI_APC; cost = 60; };
class GAZ66_ZU23_CHE: Vehicle	{ classname = "rhsgref_ins_gaz66_zu23";	level = ANTI_APC; cost = 60; };


/**
 * Ural-4320 ZU-23
 */
class Ural4320_ZU23_MSV: Vehicle { classname = "RHS_Ural_Zu23_MSV_01";  level = ANTI_APC; cost = 66; };
class Ural4320_ZU23_VDV: Vehicle { classname = "RHS_Ural_Zu23_VDV_01";  level = ANTI_APC; cost = 66; };
class Ural4320_ZU23_VMF: Vehicle { classname = "RHS_Ural_Zu23_VMF_01";  level = ANTI_APC; cost = 66; };

class Ural4320_ZU23_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_EARLY_Ural_Zu23";  level = ANTI_APC; cost = 66; };

class Ural4320_ZU23_CDF: Vehicle	{ classname = "rhsgref_cdf_b_ural_Zu23";	level = ANTI_APC; cost = 66; };

class Ural4320_ZU23_URS: Vehicle	{ classname = "UK3CB_TKA_O_Ural_Zu23";	level = ANTI_APC; cost = 66; textureData[] = {"SOV2",1}; };


/**
 * ZSU-234
 */
class ZSU234_D: Vehicle { classname = "rhs_zsu234_aa"; level = ANTI_APC; cost = 80; crew = 2; textureData[] = {"rhs_sand",1}; };
class ZSU234_W: Vehicle { classname = "rhs_zsu234_aa"; level = ANTI_APC; cost = 80; crew = 2; };

class ZSU234_G: Vehicle { classname = "UK3CB_I_G_ZsuTank"; level = ANTI_APC; cost = 80; crew = 2; };

class ZSU234_CDF: Vehicle { classname = "rhsgref_cdf_b_zsu234"; level = ANTI_APC; cost = 80; crew = 2; };
class ZSU234_CHE: Vehicle { classname = "rhsgref_ins_zsu234";	level = ANTI_APC; cost = 80; crew = 2; };

class ZSU234_UN: Vehicle { classname = "UK3CB_UN_I_ZsuTank"; level = ANTI_APC; cost = 80; crew = 2; };


/**
 * MT-LB ZU-23
 */
class MTLB_ZU23_INS_D: Vehicle	{ classname = "UK3CB_TKM_I_MTLB_PKT";	level = ANTI_CAR; cost = 68; crew = 3; };
class MTLB_ZU23_INS_W: Vehicle	{ classname = "UK3CB_I_G_MTLB_Zu23";	level = ANTI_CAR; cost = 68; crew = 3; };
class MTLB_ZU23_TKA: Vehicle		{ classname = "UK3CB_TKA_I_MTLB_ZU23";	level = ANTI_CAR; cost = 68; crew = 3; };