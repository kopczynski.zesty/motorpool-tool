class UnmannedCAS: Category {
    name = "Fixed-Wing UAV";
    type = HANGAR;

	#include <armed_cas.hpp>
    #include <armed_cvw.hpp> // Include for ground too.
};

class UnmannedCVW: Category {
    name = "Carrier-Based UAV";
    type = CARRIER;

	#include <armed_cvw.hpp>
};