/**
 * MQ-9 (US)
 */
class MQ9_GBU: Vehicle { 
	classname = "B_UAV_02_dynamicLoadout_F";  
	level = ANTI_APC; 
	cost = 68; 

	name = "MQ-9 (GBU)";

	pylonLoadout[] = {
		"PylonMissile_1Rnd_Bomb_04_F",
		"PylonMissile_1Rnd_Bomb_04_F"
	};
	pylonTurrets[] = {0,0};
};
class MQ9_AGM: Vehicle { 
	classname = "B_UAV_02_dynamicLoadout_F";  
	level = ANTI_APC; 
	cost = 74; 

	name = "MQ-9 (AGM)";

	pylonLoadout[] = {
		"PylonRack_3Rnd_ACE_Hellfire_AGM114K",
		"PylonRack_3Rnd_ACE_Hellfire_AGM114K"
	};
	pylonTurrets[] = {0,0};
};


/**
 * MQ-9 Reaper (UK)
 */
class MQ9R_CAS: Vehicle { 
	classname = "UK3CB_BAF_MQ9_Reaper";  
	level = ANTI_APC; 
	cost = 74; 

	name = "MQ-9 Reaper (Mixed)";

	pylonLoadout[] = {
		"UK3CB_BAF_PylonRack_2Rnd_Hellfire_K",
		"UK3CB_BAF_PylonRack_1Rnd_GBU",
		"UK3CB_BAF_PylonRack_1Rnd_GBU",
		"UK3CB_BAF_PylonRack_2Rnd_Hellfire_N"
	};
	pylonTurrets[] = {0,0,0,0};
};
class MQ9R_GBU: Vehicle { 
	classname = "UK3CB_BAF_MQ9_Reaper";  
	level = ANTI_APC; 
	cost = 82; 

	name = "MQ-9 Reaper (GBU)";

	pylonLoadout[] = {
		"UK3CB_BAF_PylonRack_1Rnd_GBU",
		"UK3CB_BAF_PylonRack_1Rnd_GBU",
		"UK3CB_BAF_PylonRack_1Rnd_GBU",
		"UK3CB_BAF_PylonRack_1Rnd_GBU"
	};
	pylonTurrets[] = {0,0,0,0};
};
class MQ9R_AGM: Vehicle { 
	classname = "UK3CB_BAF_MQ9_Reaper";  
	level = ANTI_APC; 
	cost = 82; 

	name = "MQ-9 Reaper (AGM)";

	pylonLoadout[] = {
		"UK3CB_BAF_PylonRack_2Rnd_Hellfire_K",
		"UK3CB_BAF_PylonRack_2Rnd_Hellfire_K",
		"UK3CB_BAF_PylonRack_2Rnd_Hellfire_N",
		"UK3CB_BAF_PylonRack_2Rnd_Hellfire_N"
	};
	pylonTurrets[] = {0,0,0,0};
};