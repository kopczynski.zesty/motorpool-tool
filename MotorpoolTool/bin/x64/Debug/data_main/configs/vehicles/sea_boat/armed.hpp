/**
 * Gunboats
 */
class Gunboat: Vehicle { classname = "B_Boat_Armed_01_minigun_F"; level = ANTI_APC; cost = 44; };


/**
 * RHIBs
 */
class RHIB_MG: Vehicle { classname = "UK3CB_TKA_B_RHIB"; 		 level = ANTI_CAR; cost = 28; name = "RHIB (HMG)"; };
class RHIB_GL: Vehicle { classname = "UK3CB_TKA_B_RHIB_Gunboat"; level = ANTI_APC; cost = 40; name = "RHIB (GMG)"; };


/**
 * Off Shore Raiding Craft
 */
class RHIB_ORC_GPMG: Vehicle { classname = "UK3CB_BAF_RHIB_GPMG_DPMT_RM"; level = ANTI_CAR; cost = 24; };
class RHIB_ORC_HMG: Vehicle  { classname = "UK3CB_BAF_RHIB_HMG_DPMT_RM";  level = ANTI_CAR; cost = 28; };


/**
 * SOC Boats
 */
class MkV_SOC: Vehicle { classname = "rhsusf_mkvsoc"; level = ANTI_CAR; cost = 40; };