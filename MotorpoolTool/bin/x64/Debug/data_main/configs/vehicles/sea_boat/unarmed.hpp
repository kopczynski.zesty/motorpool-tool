/**
 * Canoes
 */
class Canoe: Vehicle { classname = "rhsgref_hidf_canoe"; level = UTILITY; cost = 4; };


/**
 * Inflatable Boats
 */
class AssaultBoat: Vehicle { classname = "B_Boat_Transport_01_F"; level = UTILITY; cost = 16; };


/**
 * RHIBs
 */
class RHIB_Small: Vehicle { classname = "I_C_Boat_Transport_02_F"; level = UTILITY; cost = 20; name = "RHIB (Small)"; };
