/**
 * Humvees (US Army Variants)
 */
class M1025A2_Mk19_D: Vehicle { classname = "rhsusf_m1025_d_Mk19"; level = ANTI_APC; cost = 48; };
class M1025A2_Mk19_W: Vehicle { classname = "rhsusf_m1025_w_mk19"; level = ANTI_APC; cost = 48; };

class M1025A1_Mk19_D: Vehicle { classname = "rhsusf_m1025_d_Mk19"; level = ANTI_APC; cost = 48; animationData[] = {"hide_A2_Parts",1}; };
class M1025A1_Mk19_W: Vehicle { classname = "rhsusf_m1025_w_mk19"; level = ANTI_APC; cost = 48; animationData[] = {"hide_A2_Parts",1}; };

class M1151A1_Mk19_GPK_D: Vehicle  	{ classname = "rhsusf_m1151_mk19_v1_usarmy_d"; 		level = ANTI_APC; cost = 50; };
class M1151A1_Mk19_GPK_W: Vehicle  	{ classname = "rhsusf_m1151_mk19_v1_usarmy_wd";		level = ANTI_APC; cost = 50; };
class M1151A1_Mk19_OGPK_D: Vehicle 	{ classname = "rhsusf_m1151_mk19_v2_usarmy_d";		level = ANTI_APC; cost = 52; };
class M1151A1_Mk19_OGPK_W: Vehicle 	{ classname = "rhsusf_m1151_mk19_v2_usarmy_wd";		level = ANTI_APC; cost = 52; };
class M1151A1_Mk19_CROWS_D: Vehicle	{ classname = "rhsusf_m1151_mk19crows_usarmy_d"; 	level = ANTI_APC; cost = 70; };
class M1151A1_Mk19_CROWS_W: Vehicle	{ classname = "rhsusf_m1151_mk19crows_usarmy_wd";	level = ANTI_APC; cost = 70; };

class M1025A2_Mk19_CW: Vehicle 		{ classname = "UK3CB_CW_US_B_LATE_M1025_MK19"; 		level = ANTI_APC; cost = 48; };


/**
 * Humvees (US Marine Variants)
 */
class M1025A2_S_Mk19_D: Vehicle { classname = "rhsusf_m1025_d_s_Mk19"; level = ANTI_APC; cost = 48; };
class M1025A2_S_Mk19_W: Vehicle { classname = "rhsusf_m1025_w_s_Mk19"; level = ANTI_APC; cost = 48; };
class M1043A2_S_Mk19_D: Vehicle { classname = "rhsusf_m1043_d_s_mk19"; level = ANTI_APC; cost = 48; };
class M1043A2_S_Mk19_W: Vehicle { classname = "rhsusf_m1043_w_s_mk19"; level = ANTI_APC; cost = 48; };

class M1025A1_S_Mk19_D: Vehicle { classname = "rhsusf_m1025_d_s_Mk19"; level = ANTI_APC; cost = 48; animationData[] = {"hide_A2_Parts",1}; };
class M1025A1_S_Mk19_W: Vehicle { classname = "rhsusf_m1025_w_s_Mk19"; level = ANTI_APC; cost = 48; animationData[] = {"hide_A2_Parts",1}; };
class M1043A1_S_Mk19_D: Vehicle { classname = "rhsusf_m1043_d_s_mk19"; level = ANTI_APC; cost = 48; animationData[] = {"hide_A2_Parts",1}; };
class M1043A1_S_Mk19_W: Vehicle { classname = "rhsusf_m1043_w_s_mk19"; level = ANTI_APC; cost = 48; animationData[] = {"hide_A2_Parts",1}; };

class M1151A1_S_Mk19_MCTAGS_D: Vehicle  { classname = "rhsusf_m1151_mk19_v3_usmc_d"; 	level = ANTI_APC; cost = 50; };
class M1151A1_S_Mk19_MCTAGS_W: Vehicle  { classname = "rhsusf_m1151_mk19_v3_usmc_wd"; 	level = ANTI_APC; cost = 50; };
class M1151A1_S_Mk19_CROWS_D: Vehicle  	{ classname = "rhsusf_m1151_mk19crows_usmc_d"; 	level = ANTI_APC; cost = 70; };
class M1151A1_S_Mk19_CROWS_W: Vehicle  	{ classname = "rhsusf_m1151_mk19crows_usmc_wd";	level = ANTI_APC; cost = 70; };

/**
 * Humvees (Other Variants)
 */
class M1151A1_Mk19_GPK_O: Vehicle  	{ classname = "rhsusf_m1151_mk19_v1_usarmy_wd"; 	level = ANTI_APC; cost = 50; textureData[] = {"rhs_olive",1}; };
class M1151A1_Mk19_OGPK_O: Vehicle 	{ classname = "rhsusf_m1151_mk19_v2_usarmy_wd";		level = ANTI_APC; cost = 52; textureData[] = {"rhs_olive",1}; };
class M1025A2_GMG_TKA: Vehicle 		{ classname = "UK3CB_TKA_B_M1025_MK19";				level = ANTI_APC; cost = 48; };
class M1025A1_Mk19_TAN: Vehicle 	{ classname = "rhsgref_hidf_m1025_mk19"; 			level = ANTI_APC; cost = 48; };


/**
 * Land Rovers
 */
class LandRoverWMIK_GMG_D: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GMG_Sand_A";    level = ANTI_APC; cost = 44; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_GMG_W: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GMG_Green_A";   level = ANTI_APC; cost = 44; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_GMG_T: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GMG_Green_B";   level = ANTI_APC; cost = 44; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };

class LandRoverWMIK_S_GMG_D: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GMG_Sand_A";  level = ANTI_APC; cost = 44; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_S_GMG_W: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GMG_Green_A"; level = ANTI_APC; cost = 44; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_S_GMG_T: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GMG_Green_B"; level = ANTI_APC; cost = 44; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };

class LandRover110_AGS30_INS: Vehicle  { classname = "UK3CB_TKM_I_LR_AGS30";    level = ANTI_APC; cost = 40; };
class LandRoverWMIK_AGS30_INS: Vehicle { classname = "UK3CB_TKM_I_LR_SF_AGS30"; level = ANTI_APC; cost = 44; };

class LandRover110_AGS30_TKA: Vehicle  { classname = "UK3CB_TKA_I_LR_AGS30";    level = ANTI_APC; cost = 40; };
class LandRoverWMIK_AGS30_TKA: Vehicle { classname = "UK3CB_TKA_B_LR_SF_AGS30"; level = ANTI_APC; cost = 44; };


/**
 * UAZ
 */
class UAZ_AGS30_SOV: Vehicle	{ classname = "UK3CB_CW_SOV_O_EARLY_UAZ_AGS30";	level = ANTI_APC; cost = 40; };

class UAZ_AGS30_CDF: Vehicle  { classname = "rhsgref_cdf_b_reg_uaz_ags";  level = ANTI_APC; cost = 40; };
class UAZ_AGS30_UN: Vehicle   { classname = "UK3CB_UN_I_UAZ_AGS30";       level = ANTI_APC; cost = 40; };

class UAZ_AGS30_CHE: Vehicle  { classname = "rhsgref_ins_uaz_ags"; level = ANTI_APC; cost = 40; };


/**
 * GAZ Vodnik
 */
class GAZVodnik_GMG_TKA: Vehicle { classname = "UK3CB_TKA_O_GAZ_Vodnik_GMG"; level = ANTI_APC; cost = 52; };


/**
 * Hilux
 */
class Hilux_AGS30_INS: Vehicle { classname = "UK3CB_I_G_Hilux_GMG"; level = ANTI_APC; cost = 28; };
class Hilux_AGS30_BGM: Vehicle { classname = "UK3CB_O_G_Hilux_GMG"; level = ANTI_APC; cost = 28; };