/**
 * Quadbikes
 */
class Quadbike: Vehicle { classname = "B_Quadbike_01_F"; level = UTILITY; cost = 2; };


/**
 * Motorcycles
 */
class M1030_D: Vehicle		{ classname = "UK3CB_ADA_O_M1030"; 		level = UTILITY; cost = 2; };
class M1030_W: Vehicle		{ classname = "UK3CB_B_M1030_NATO"; 	level = UTILITY; cost = 2; };
class M1030_USMC_D: Vehicle	{ classname = "UK3CB_B_M1030_USMC_DES"; level = UTILITY; cost = 2; };
class M1030_USMC_W: Vehicle	{ classname = "UK3CB_B_M1030_USMC_WDL"; level = UTILITY; cost = 2; };
class M1030_TAN: Vehicle	{ classname = "UK3CB_B_M1030_HIDF"; 	level = UTILITY; cost = 2; };
class TT650: Vehicle		{ classname = "UK3CB_O_G_TT650"; 		level = UTILITY; cost = 2; };
class Yava250N: Vehicle		{ classname = "UK3CB_O_G_YAVA";			level = UTILITY; cost = 2; };
class Yava250N_CNA: Vehicle { classname = "UK3CB_CCM_I_YAVA"; 		level = UTILITY; cost = 2; };
class Yava250N_MUJ: Vehicle { classname = "UK3CB_TKC_C_YAVA"; 		level = UTILITY; cost = 2; };
class Yava250N_SOV: Vehicle { classname = "UK3CB_O_G_YAVA"; 		level = UTILITY; cost = 2; };
class Yava250N_TKA: Vehicle { classname = "UK3CB_TKA_O_YAVA"; 		level = UTILITY; cost = 2; };


/**
 * MRZRs
 */
class MRZR_D: Vehicle { classname = "rhsusf_mrzr4_d"; level = UTILITY; cost = 12; };
class MRZR_W: Vehicle { classname = "rhsusf_mrzr4_d"; level = UTILITY; cost = 12; textureData[] = {"paint1",1}; };


/**
 * Willys Jeep
 */
class Willys_CW: Vehicle { classname = "UK3CB_CW_US_B_EARLY_Willys_Jeep_Open";	level = UTILITY; cost = 16; };


/**
 * M151 Jeep
 */
class M151_CW: Vehicle   { classname = "UK3CB_CW_US_B_LATE_M151_Jeep_Closed";	level = UTILITY; cost = 16; };
class M151_O_CW: Vehicle { classname = "UK3CB_CW_US_B_LATE_M151_Jeep_Open";		level = UTILITY; cost = 16; };

class M151_TAN: Vehicle		{ classname = "UK3CB_B_M151_Jeep_Closed_HIDF"; level = UTILITY; cost = 16; };
class M151_O_TAN: Vehicle	{ classname = "UK3CB_B_M151_Jeep_Open_HIDF";   level = UTILITY; cost = 16; };


/**
 * Humvees (US Army Variants)
 */
class M1025A2_D: Vehicle		{ classname = "rhsusf_m1025_d";					level = UTILITY; cost = 24; };
class M1025A2_W: Vehicle		{ classname = "rhsusf_m1025_w";					level = UTILITY; cost = 24; };
class M1097A2_2D_D: Vehicle		{ classname = "rhsusf_m998_d_2dr";				level = UTILITY; cost = 28; };
class M1097A2_2D_W: Vehicle		{ classname = "rhsusf_m998_w_2dr";				level = UTILITY; cost = 28; };
class M1097A2_2DHT_D: Vehicle	{ classname = "rhsusf_m998_d_2dr_halftop";		level = UTILITY; cost = 28; };
class M1097A2_2DHT_W: Vehicle	{ classname = "rhsusf_m998_w_2dr_halftop";		level = UTILITY; cost = 28; };
class M1097A2_2DFT_D: Vehicle	{ classname = "rhsusf_m998_d_2dr_fulltop";		level = UTILITY; cost = 28; };
class M1097A2_2DFT_W: Vehicle	{ classname = "rhsusf_m998_w_2dr_fulltop";		level = UTILITY; cost = 28; };
class M1097A2_4D_D: Vehicle		{ classname = "rhsusf_m998_d_4dr";				level = UTILITY; cost = 26; };
class M1097A2_4D_W: Vehicle		{ classname = "rhsusf_m998_w_4dr";				level = UTILITY; cost = 26; };
class M1097A2_4DHT_D: Vehicle	{ classname = "rhsusf_m998_d_4dr_halftop";		level = UTILITY; cost = 24;  };
class M1097A2_4DHT_W: Vehicle	{ classname = "rhsusf_m998_w_4dr_halftop";		level = UTILITY; cost = 24;  };
class M1097A2_4DFT_D: Vehicle	{ classname = "rhsusf_m998_d_4dr_fulltop";		level = UTILITY; cost = 24;  };
class M1097A2_4DFT_W: Vehicle	{ classname = "rhsusf_m998_w_4dr_fulltop";		level = UTILITY; cost = 24;  };

class M1025A1_D: Vehicle		{ classname = "rhsusf_m1025_d";					level = UTILITY; cost = 24;	 animationData[] = {"hide_A2_Parts",1}; };
class M1025A1_W: Vehicle		{ classname = "rhsusf_m1025_w";					level = UTILITY; cost = 24;	 animationData[] = {"hide_A2_Parts",1}; };
class M1097A1_2D_D: Vehicle		{ classname = "rhsusf_m998_d_2dr";				level = UTILITY; cost = 28; animationData[] = {"hide_A2_Parts",1}; }; 
class M1097A1_2D_W: Vehicle		{ classname = "rhsusf_m998_w_2dr";				level = UTILITY; cost = 28; animationData[] = {"hide_A2_Parts",1}; };
class M1097A1_2DHT_D: Vehicle	{ classname = "rhsusf_m998_d_2dr_halftop";		level = UTILITY; cost = 28; animationData[] = {"hide_A2_Parts",1}; };
class M1097A1_2DHT_W: Vehicle	{ classname = "rhsusf_m998_w_2dr_halftop";		level = UTILITY; cost = 28; animationData[] = {"hide_A2_Parts",1}; };
class M1097A1_2DFT_D: Vehicle	{ classname = "rhsusf_m998_d_2dr_fulltop";		level = UTILITY; cost = 28; animationData[] = {"hide_A2_Parts",1}; };
class M1097A1_2DFT_W: Vehicle	{ classname = "rhsusf_m998_w_2dr_fulltop";		level = UTILITY; cost = 28; animationData[] = {"hide_A2_Parts",1}; };
class M1097A1_4D_D: Vehicle		{ classname = "rhsusf_m998_d_4dr";				level = UTILITY; cost = 26;	 animationData[] = {"hide_A2_Parts",1}; }; 
class M1097A1_4D_W: Vehicle		{ classname = "rhsusf_m998_w_4dr";				level = UTILITY; cost = 26;	 animationData[] = {"hide_A2_Parts",1}; };
class M1097A1_4DHT_D: Vehicle	{ classname = "rhsusf_m998_d_4dr_halftop";		level = UTILITY; cost = 24;  animationData[] = {"hide_A2_Parts",1}; };
class M1097A1_4DHT_W: Vehicle	{ classname = "rhsusf_m998_w_4dr_halftop";		level = UTILITY; cost = 24;  animationData[] = {"hide_A2_Parts",1}; };
class M1097A1_4DFT_D: Vehicle	{ classname = "rhsusf_m998_d_4dr_fulltop";		level = UTILITY; cost = 24;  animationData[] = {"hide_A2_Parts",1}; };
class M1097A1_4DFT_W: Vehicle	{ classname = "rhsusf_m998_w_4dr_fulltop";		level = UTILITY; cost = 24;  animationData[] = {"hide_A2_Parts",1}; };

class M1151A1_4D_D: Vehicle			{ classname = "rhsusf_m1151_usarmy_d";			level = UTILITY; cost = 30; };
class M1151A1_4D_W: Vehicle			{ classname = "rhsusf_m1151_usarmy_wd";			level = UTILITY; cost = 30; };
class M1165A1_4D_D: Vehicle			{ classname = "rhsusf_m1165_usarmy_d";			level = UTILITY; cost = 28; };
class M1165A1_4D_W: Vehicle			{ classname = "rhsusf_m1165_usarmy_wd";			level = UTILITY; cost = 28; };
class M1152A1_2D_D: Vehicle			{ classname = "rhsusf_m1152_usarmy_d";			level = UTILITY; cost = 28; };
class M1152A1_2D_W: Vehicle			{ classname = "rhsusf_m1152_usarmy_wd";			level = UTILITY; cost = 28; };
class M1152A1_2D_RSV_D: Vehicle		{ classname = "rhsusf_m1152_rsv_usarmy_d";		level = UTILITY; cost = 28; };
class M1152A1_2D_RSV_W: Vehicle		{ classname = "rhsusf_m1152_rsv_usarmy_wd";		level = UTILITY; cost = 28; };
class M1152A1_2D_SICPS_D: Vehicle	{ classname = "rhsusf_m1152_sicps_usarmy_d";	level = UTILITY; cost = 28; };
class M1152A1_2D_SICPS_W: Vehicle	{ classname = "rhsusf_m1152_sicps_usarmy_wd";	level = UTILITY; cost = 28; };

class M1025A2_CW: Vehicle		{ classname = "UK3CB_CW_US_B_LATE_M1025_Unarmed";	level = UTILITY; cost = 24; };

class M1097A2_2D_CW: Vehicle	{ classname = "UK3CB_CW_US_B_LATE_M998_2DR";		level = UTILITY; cost = 28; };
class M1097A2_4D_CW: Vehicle	{ classname = "UK3CB_CW_US_B_LATE_M998_4DR";		level = UTILITY; cost = 26; };


/**
 * Humvees (US Marine Variants)
 */
class M1025A2_S_D: Vehicle		{ classname = "rhsusf_m1025_d_s";			 level = UTILITY; cost = 24; };
class M1025A2_S_W: Vehicle		{ classname = "rhsusf_m1025_w_s";			 level = UTILITY; cost = 24; };
class M1043A2_S_D: Vehicle		{ classname = "rhsusf_m1043_d_s";			 level = UTILITY; cost = 24; };
class M1043A2_S_W: Vehicle		{ classname = "rhsusf_m1043_w_s";			 level = UTILITY; cost = 24; };
class M1123A2_S_2D_D: Vehicle	{ classname = "rhsusf_m998_d_s_2dr";		 level = UTILITY; cost = 28; };
class M1123A2_S_2D_W: Vehicle	{ classname = "rhsusf_m998_w_s_2dr";		 level = UTILITY; cost = 28; }; 
class M1123A2_S_2DHT_D: Vehicle	{ classname = "rhsusf_m998_d_s_2dr_halftop"; level = UTILITY; cost = 28; };
class M1123A2_S_2DHT_W: Vehicle	{ classname = "rhsusf_m998_w_s_2dr_halftop"; level = UTILITY; cost = 28; };
class M1123A2_S_2DFT_D: Vehicle	{ classname = "rhsusf_m998_d_s_2dr_fulltop"; level = UTILITY; cost = 28; };
class M1123A2_S_2DFT_W: Vehicle	{ classname = "rhsusf_m998_w_s_2dr_fulltop"; level = UTILITY; cost = 28; };
class M1123A2_S_4D_D: Vehicle	{ classname = "rhsusf_m998_d_s_4dr";		 level = UTILITY; cost = 26; };
class M1123A2_S_4D_W: Vehicle	{ classname = "rhsusf_m998_w_s_4dr";		 level = UTILITY; cost = 26; };
class M1123A2_S_4DHT_D: Vehicle	{ classname = "rhsusf_m998_d_s_4dr_halftop"; level = UTILITY; cost = 24; };
class M1123A2_S_4DHT_W: Vehicle	{ classname = "rhsusf_m998_w_s_4dr_halftop"; level = UTILITY; cost = 24; };
class M1123A2_S_4DFT_D: Vehicle	{ classname = "rhsusf_m998_d_s_4dr_fulltop"; level = UTILITY; cost = 24; };
class M1123A2_S_4DFT_W: Vehicle	{ classname = "rhsusf_m998_w_s_4dr_fulltop"; level = UTILITY; cost = 24; };

class M1025A1_S_D: Vehicle		{ classname = "rhsusf_m1025_d_s";			 level = UTILITY; cost = 24;	 animationData[] = {"hide_A2_Parts",1}; };
class M1025A1_S_W: Vehicle		{ classname = "rhsusf_m1025_w_s";			 level = UTILITY; cost = 24;	 animationData[] = {"hide_A2_Parts",1}; };
class M1043A1_S_D: Vehicle		{ classname = "rhsusf_m1043_d_s";			 level = UTILITY; cost = 24;	 animationData[] = {"hide_A2_Parts",1}; };
class M1043A1_S_W: Vehicle		{ classname = "rhsusf_m1043_w_s";			 level = UTILITY; cost = 24;	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_2D_D: Vehicle	{ classname = "rhsusf_m998_d_s_2dr";		 level = UTILITY; cost = 28; 	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_2D_W: Vehicle	{ classname = "rhsusf_m998_w_s_2dr";		 level = UTILITY; cost = 28; 	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_2DHT_D: Vehicle	{ classname = "rhsusf_m998_d_s_2dr_halftop"; level = UTILITY; cost = 28; 	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_2DHT_W: Vehicle	{ classname = "rhsusf_m998_w_s_2dr_halftop"; level = UTILITY; cost = 28; 	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_2DFT_D: Vehicle	{ classname = "rhsusf_m998_d_s_2dr_fulltop"; level = UTILITY; cost = 28; 	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_2DFT_W: Vehicle	{ classname = "rhsusf_m998_w_s_2dr_fulltop"; level = UTILITY; cost = 28; 	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_4D_D: Vehicle	{ classname = "rhsusf_m998_d_s_4dr";		 level = UTILITY; cost = 26;	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_4D_W: Vehicle	{ classname = "rhsusf_m998_w_s_4dr";		 level = UTILITY; cost = 26;	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_4DHT_D: Vehicle	{ classname = "rhsusf_m998_d_s_4dr_halftop"; level = UTILITY; cost = 24;	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_4DHT_W: Vehicle	{ classname = "rhsusf_m998_w_s_4dr_halftop"; level = UTILITY; cost = 24;	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_4DFT_D: Vehicle	{ classname = "rhsusf_m998_d_s_4dr_fulltop"; level = UTILITY; cost = 24;	 animationData[] = {"hide_A2_Parts",1}; };
class M1123A1_S_4DFT_W: Vehicle	{ classname = "rhsusf_m998_w_s_4dr_fulltop"; level = UTILITY; cost = 24;	 animationData[] = {"hide_A2_Parts",1}; };

class M1151A1_S_4D_D: Vehicle			{ classname = "rhsusf_m1151_usmc_d";			level = UTILITY; cost = 30; };
class M1151A1_S_4D_W: Vehicle			{ classname = "rhsusf_m1151_usmc_wd";			level = UTILITY; cost = 30; };
class M1165A1_S_4D_D: Vehicle			{ classname = "rhsusf_m1165_usmc_d";			level = UTILITY; cost = 28; };
class M1165A1_S_4D_W: Vehicle			{ classname = "rhsusf_m1165_usmc_wd";			level = UTILITY; cost = 28; };
class M1152A1_S_2D_D: Vehicle			{ classname = "rhsusf_m1152_usmc_d";			level = UTILITY; cost = 28; };
class M1152A1_S_2D_W: Vehicle			{ classname = "rhsusf_m1152_usmc_wd";			level = UTILITY; cost = 28; };
class M1152A1_S_2D_RSV_D: Vehicle		{ classname = "rhsusf_m1152_rsv_usmc_d";		level = UTILITY; cost = 28; };
class M1152A1_S_2D_RSV_W: Vehicle		{ classname = "rhsusf_m1152_rsv_usmc_wd";		level = UTILITY; cost = 28; };


/**
 * Humvees (Other Variants)
 */
class M1025A1_O: Vehicle		{ classname = "rhssaf_m1025_olive";				level = UTILITY; cost = 24;	};
class M1097A1_2DHT_O: Vehicle	{ classname = "rhssaf_m998_olive_2dr_halftop";	level = UTILITY; cost = 28; };
class M1097A1_2DFT_O: Vehicle	{ classname = "rhssaf_m998_olive_2dr_fulltop";	level = UTILITY; cost = 28; };
class M1151A1_4D_O: Vehicle		{ classname = "rhssaf_m1151_olive";				level = UTILITY; cost = 30; };
class M1152A1_2D_O: Vehicle		{ classname = "rhssaf_m1152_TCV_olive";			level = UTILITY; cost = 32; };
class M1152A1_2D_FB_O: Vehicle	{ classname = "rhssaf_m1152_olive";				level = UTILITY; cost = 28; };
class M1152A1_2D_RSV_O: Vehicle	{ classname = "rhssaf_m1152_rsv_olive";			level = UTILITY; cost = 28; };

class M998_2D_TKA: Vehicle 		{ classname = "UK3CB_TKA_B_M998_2DR";			level = UTILITY; cost = 28; };
class M998_4D_TKA: Vehicle 		{ classname = "UK3CB_TKA_B_M998_4DR";			level = UTILITY; cost = 26; };
class M1025A2_TKA: Vehicle 		{ classname = "UK3CB_TKA_B_M1025";				level = UTILITY; cost = 24; };

class M1025A1_TAN: Vehicle		{ classname = "rhsgref_hidf_m1025";				level = UTILITY; cost = 24; };
class M998A1_2D_TAN: Vehicle	{ classname = "rhsgref_hidf_M998_2dr";			level = UTILITY; cost = 28; };
class M998A1_2DHT_TAN: Vehicle	{ classname = "rhsgref_hidf_M998_2dr_halftop";	level = UTILITY; cost = 28; };
class M998A1_2DFT_TAN: Vehicle	{ classname = "rhsgref_hidf_M998_2dr_fulltop";	level = UTILITY; cost = 28; };
class M998A1_4D_TAN: Vehicle	{ classname = "rhsgref_hidf_m998_4dr";			level = UTILITY; cost = 28; };
class M998A1_4DHT_TAN: Vehicle	{ classname = "rhsgref_hidf_M998_4dr_halftop";	level = UTILITY; cost = 28; };
class M998A1_4DFT_TAN: Vehicle	{ classname = "rhsgref_hidf_M998_4dr_fulltop";	level = UTILITY; cost = 28; };


/**
 * Land Rovers
 */
class LandRoverSoft_D: Vehicle		{ classname = "UK3CB_BAF_LandRover_Soft_Sand_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",1,"Canvas_Hide",0,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverSoft_W: Vehicle		{ classname = "UK3CB_BAF_LandRover_Soft_Green_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",1,"Canvas_Hide",0,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverSoft_T: Vehicle		{ classname = "UK3CB_BAF_LandRover_Soft_Green_B";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",1,"Canvas_Hide",0,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverSoft_A: Vehicle		{ classname = "UK3CB_BAF_LandRover_Soft_Arctic_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",1,"Canvas_Hide",0,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverSoft_UN: Vehicle		{ classname = "UK3CB_BAF_LandRover_Soft_UN_A";			level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",1,"Canvas_Hide",0,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverHard_D: Vehicle		{ classname = "UK3CB_BAF_LandRover_Hard_Sand_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",1,"Beacons_Hide",1,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"Siren_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverHard_W: Vehicle		{ classname = "UK3CB_BAF_LandRover_Hard_Green_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",1,"Beacons_Hide",1,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"Siren_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverHard_T: Vehicle		{ classname = "UK3CB_BAF_LandRover_Hard_Green_B";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",1,"Beacons_Hide",1,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"Siren_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverHard_A: Vehicle		{ classname = "UK3CB_BAF_LandRover_Hard_Arctic_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",1,"Beacons_Hide",1,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"Siren_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverHard_UN: Vehicle		{ classname = "UK3CB_BAF_LandRover_Hard_UN_A";			level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",1,"Beacons_Hide",1,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"Siren_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };

class LandRoverSoft_S_D: Vehicle	{ classname = "UK3CB_BAF_LandRover_Soft_Sand_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",0,"Canvas_Hide",0,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverSoft_S_W: Vehicle	{ classname = "UK3CB_BAF_LandRover_Soft_Green_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",0,"Canvas_Hide",0,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverSoft_S_T: Vehicle	{ classname = "UK3CB_BAF_LandRover_Soft_Green_B";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",0,"Canvas_Hide",0,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverSoft_S_A: Vehicle	{ classname = "UK3CB_BAF_LandRover_Soft_Arctic_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",0,"Canvas_Hide",0,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverSoft_S_UN: Vehicle	{ classname = "UK3CB_BAF_LandRover_Soft_UN_A";			level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",0,"Canvas_Hide",0,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverHard_S_D: Vehicle	{ classname = "UK3CB_BAF_LandRover_Hard_Sand_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",0,"Beacons_Hide",1,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"Siren_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverHard_S_W: Vehicle	{ classname = "UK3CB_BAF_LandRover_Hard_Green_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",0,"Beacons_Hide",1,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"Siren_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverHard_S_T: Vehicle	{ classname = "UK3CB_BAF_LandRover_Hard_Green_B";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",0,"Beacons_Hide",1,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"Siren_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverHard_S_A: Vehicle	{ classname = "UK3CB_BAF_LandRover_Hard_Arctic_A";		level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",0,"Beacons_Hide",1,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"Siren_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };
class LandRoverHard_S_UN: Vehicle	{ classname = "UK3CB_BAF_LandRover_Hard_UN_A";			level = UTILITY; cost = 22; animationData[] = {"AerialMountL_Hide",0,"AerialMountR_Hide",1,"AerialSL_Hide",0,"AerialSR_Hide",1,"AirIntakeSnorkel_Hide",0,"Beacons_Hide",1,"ClanLogo_Hide",1,"HeadlightGrills_Hide",0,"Sign_Hide",1,"Siren_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",0,"SideLockers_Hide",0}; };

class LandRoverSnatch_D: Vehicle	{ classname = "UK3CB_BAF_LandRover_Snatch_Sand_A";		level = UTILITY; cost = 26; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNetMain_Hide",0,"CamoNetDoorFrontLeft_Hide",0,"CamoNetDoorFrontRight_Hide",0,"CamoNetDoorRearLeft_Hide",0,"CamoNetDoorRearRight_Hide",0,"Photos_Hide",1,"Spotlight_Hide",0,"WireCutter_Hide",0,"AerialAtuL_Hide",0,"AerialAtuR_Hide",1,"AerialFL_Hide",0,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",1,"SideLockers_Hide",0}; };
class LandRoverSnatch_W: Vehicle	{ classname = "UK3CB_BAF_LandRover_Snatch_Green_A";		level = UTILITY; cost = 26; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNetMain_Hide",0,"CamoNetDoorFrontLeft_Hide",0,"CamoNetDoorFrontRight_Hide",0,"CamoNetDoorRearLeft_Hide",0,"CamoNetDoorRearRight_Hide",0,"Photos_Hide",1,"Spotlight_Hide",0,"WireCutter_Hide",0,"AerialAtuL_Hide",0,"AerialAtuR_Hide",1,"AerialFL_Hide",0,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",1,"SideLockers_Hide",0}; };
class LandRoverSnatch_S_D: Vehicle	{ classname = "UK3CB_BAF_LandRover_Snatch_Sand_A";		level = UTILITY; cost = 26; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNetMain_Hide",0,"CamoNetDoorFrontLeft_Hide",0,"CamoNetDoorFrontRight_Hide",0,"CamoNetDoorRearLeft_Hide",0,"CamoNetDoorRearRight_Hide",0,"Photos_Hide",1,"Spotlight_Hide",0,"WireCutter_Hide",0,"AerialAtuL_Hide",0,"AerialAtuR_Hide",1,"AerialFL_Hide",0,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",1,"SideLockers_Hide",0}; };
class LandRoverSnatch_S_W: Vehicle	{ classname = "UK3CB_BAF_LandRover_Snatch_Green_A";		level = UTILITY; cost = 26; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNetMain_Hide",0,"CamoNetDoorFrontLeft_Hide",0,"CamoNetDoorFrontRight_Hide",0,"CamoNetDoorRearLeft_Hide",0,"CamoNetDoorRearRight_Hide",0,"Photos_Hide",1,"Spotlight_Hide",0,"WireCutter_Hide",0,"AerialAtuL_Hide",0,"AerialAtuR_Hide",1,"AerialFL_Hide",0,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",1,"SideLockers_Hide",0}; };

class LandRoverMedical_D: Vehicle	{ classname = "UK3CB_BAF_LandRover_Amb_Sand_A";			level = UTILITY; cost = 22; animationData[] = {"ClanLogo_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"AirIntakeSnorkel_Hide",0,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",1,"SideLockers_Hide",1}; };
class LandRoverMedical_W: Vehicle	{ classname = "UK3CB_BAF_LandRover_Amb_Green_A";		level = UTILITY; cost = 22; animationData[] = {"ClanLogo_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"AirIntakeSnorkel_Hide",0,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",1,"SideLockers_Hide",1}; };
class LandRoverMedical_UN: Vehicle	{ classname = "UK3CB_BAF_LandRover_Amb_UN_A";			level = UTILITY; cost = 22; animationData[] = {"ClanLogo_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"AirIntakeSnorkel_Hide",0,"Flag_Hide",1,"Mudguards_Hide",0,"Radio_Hide",1,"SideLockers_Hide",1}; };

class LandRover110_INS: Vehicle   { classname = "UK3CB_TKM_I_LR_Closed"; level = UTILITY; cost = 22; };
class LandRover110_O_INS: Vehicle { classname = "UK3CB_TKM_I_LR_Open";	 level = UTILITY; cost = 22; };

class LandRover110_O_TKA: Vehicle   { classname = "UK3CB_TKA_I_LR_Open"; level = UTILITY; cost = 22; };


/**
 * GL Wolf
 */
class GLWolf_D: Vehicle { classname = "Redd_Tank_LKW_leicht_gl_Wolf_Tropentarn_FueFu"; name = "Light Truck gl Wolf Command"; level = UTILITY; cost = 16; };
class GLWolf_W: Vehicle { classname = "Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_FueFu";  name = "Light Truck gl Wolf Command"; level = UTILITY; cost = 16; };
class GLWolf_A: Vehicle { classname = "Redd_Tank_LKW_leicht_gl_Wolf_Wintertarn_FueFu"; name = "Light Truck gl Wolf Command"; level = UTILITY; cost = 16; };

class GLWolf_Medical_D: Vehicle { classname = "Redd_Tank_LKW_leicht_gl_Wolf_Tropentarn_San"; name = "Light Truck gl Wolf Medical"; level = UTILITY; cost = 16; };
class GLWolf_Medical_W: Vehicle { classname = "Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_San";  name = "Light Truck gl Wolf Medical"; level = UTILITY; cost = 16; };
class GLWolf_Medical_A: Vehicle { classname = "Redd_Tank_LKW_leicht_gl_Wolf_Wintertarn_San"; name = "Light Truck gl Wolf Medical"; level = UTILITY; cost = 16; };


/**
 * UAZ
 */
class UAZ_MSV_D: Vehicle	{ classname = "RHS_UAZ_MSV_01";			level = UTILITY; cost = 20; textureData[] = {"Camo",1}; };
class UAZ_MSV_W: Vehicle	{ classname = "RHS_UAZ_MSV_01";			level = UTILITY; cost = 20; };
class UAZ_O_MSV_D: Vehicle	{ classname = "rhs_uaz_open_MSV_01";	level = UTILITY; cost = 20; textureData[] = {"Camo",1}; };
class UAZ_O_MSV_W: Vehicle	{ classname = "rhs_uaz_open_MSV_01";	level = UTILITY; cost = 20; };

class UAZ_VDV_D: Vehicle	{ classname = "rhs_uaz_vdv";			level = UTILITY; cost = 20; textureData[] = {"Camo",1}; };
class UAZ_VDV_W: Vehicle	{ classname = "rhs_uaz_vdv";			level = UTILITY; cost = 20; };
class UAZ_O_VDV_D: Vehicle	{ classname = "rhs_uaz_open_vdv";		level = UTILITY; cost = 20; textureData[] = {"Camo",1}; };
class UAZ_O_VDV_W: Vehicle	{ classname = "rhs_uaz_open_vdv";		level = UTILITY; cost = 20; };

class UAZ_VMF_D: Vehicle	{ classname = "rhs_uaz_vmf";			level = UTILITY; cost = 20; textureData[] = {"Camo",1}; };
class UAZ_VMF_W: Vehicle	{ classname = "rhs_uaz_vmf";			level = UTILITY; cost = 20; };
class UAZ_O_VMF_D: Vehicle	{ classname = "rhs_uaz_open_vmf";		level = UTILITY; cost = 20; textureData[] = {"Camo",1}; };
class UAZ_O_VMF_W: Vehicle	{ classname = "rhs_uaz_open_vmf";		level = UTILITY; cost = 20; };

class UAZ_UN: Vehicle		{ classname = "rhssaf_un_uaz";			level = UTILITY; cost = 20; };
class UAZ_O_UN: Vehicle		{ classname = "rhssaf_un_uaz_open";		level = UTILITY; cost = 20; };

class UAZ_SOV: Vehicle   { classname = "UK3CB_CW_SOV_O_EARLY_UAZ_Closed";	level = UTILITY; cost = 20; };
class UAZ_O_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_EARLY_UAZ_Open";		level = UTILITY; cost = 20; };

class UAZ_CDF: Vehicle   { classname = "rhsgref_cdf_b_reg_uaz";			level = UTILITY; cost = 20; };
class UAZ_O_CDF: Vehicle { classname = "rhsgref_cdf_b_reg_uaz_open";	level = UTILITY; cost = 20; };

class UAZ_CHE: Vehicle   { classname = "rhsgref_ins_uaz";		level = UTILITY; cost = 20; };
class UAZ_O_CHE: Vehicle { classname = "rhsgref_ins_uaz_open";	level = UTILITY; cost = 20; };


/**
 * GAZ Vodnik
 */
class GAZVodnik_TKA: Vehicle     { classname = "UK3CB_TKA_O_GAZ_Vodnik";		 level = UTILITY; cost = 24; };
class GAZVodnik_MEV_TKA: Vehicle { classname = "UK3CB_TKA_O_GAZ_Vodnik_MedEvac"; level = UTILITY; cost = 22; };

class GAZVodnik_URS: Vehicle     { classname = "UK3CB_TKA_O_GAZ_Vodnik";         level = UTILITY; cost = 24; textureData[] = {"Russian",1}; };
class GAZVodnik_MEV_URS: Vehicle { classname = "UK3CB_TKA_O_GAZ_Vodnik_MedEvac"; level = UTILITY; cost = 22; textureData[] = {"Russian",1}; };


/**
 * Skoda S1203
 */
class SkodaS1203_INS: Vehicle { classname = "UK3CB_CCM_O_S1203";	level = UTILITY; cost = 10; };


/**
 * Datsun 620
 */
class Datsun_INS: Vehicle { classname = "UK3CB_CCM_O_Datsun_Civ_Closed";	level = UTILITY; cost = 12; };
class Datsun_O_INS: Vehicle { classname = "UK3CB_CCM_O_Datsun_Civ_Open";	level = UTILITY; cost = 12; };


/**
 * Hilux
 */
class Hilux_INS: Vehicle { classname = "UK3CB_CCM_O_Hilux_Civ_Closed";	level = UTILITY; cost = 14; };
class Hilux_O_INS: Vehicle { classname = "UK3CB_CCM_O_Hilux_Civ_Open";	level = UTILITY; cost = 14; };

class Hilux_BGM: Vehicle { classname = "UK3CB_O_G_Hilux_Closed";	level = UTILITY; cost = 14; };
class Hilux_O_BGM: Vehicle { classname = "UK3CB_O_G_Hilux_Open";		level = UTILITY; cost = 14; };


/**
 * Offroad
 */
class Offroad_ION: Vehicle { classname = "tacs_Offroad_B_Black";   level = UTILITY; cost = 16; };
class Polaris_ION: Vehicle { classname = "tacs_Polaris_B_Black";   level = UTILITY; cost = 16; };

class Offroad_INS: Vehicle        { classname = "I_G_Offroad_01_F";			level = UTILITY; cost = 16; };
class Offroad_Repair_INS: Vehicle { classname = "I_G_Offroad_01_repair_F";	level = UTILITY; cost = 16; };


/**
 * Jeep Wrangler
 */
class Jeep_D: Vehicle { classname = "I_C_Offroad_02_unarmed_F"; level = UTILITY; cost = 14; textureData[] = {"Brown",1}; };
class Jeep_W: Vehicle { classname = "C_Offroad_02_unarmed_F"; level = UTILITY; cost = 14; textureData[] = {"Olive",1}; };


/**
 * SUV 4x4
 */
class SUV: Vehicle { classname = "UK3CB_C_SUV"; name = "SUV 4x4"; level = UTILITY; cost = 16; };
class SUV_Armored_ION: Vehicle { classname = "UK3CB_C_SUV_Armoured"; name = "SUV 4x4 Armored"; level = UTILITY; cost = 24; textureData[] = {"Black",1}; };