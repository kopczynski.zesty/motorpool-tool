/**
 * M151 Jeep
 */
class M151_M2_CW: Vehicle { classname = "UK3CB_CW_US_B_LATE_M151_Jeep_HMG";	level = ANTI_CAR; cost = 22; };

class M151_M2_TAN: Vehicle { classname = "UK3CB_B_M151_Jeep_HMG_HIDF";	level = ANTI_CAR; cost = 22; };


/**
 * Humvees (US Army Variants)
 */
class M1025A2_M2_D: Vehicle  { classname = "rhsusf_m1025_d_m2";		level = ANTI_CAR; cost = 34; };
class M1025A2_M2_W: Vehicle  { classname = "rhsusf_m1025_w_m2";		level = ANTI_CAR; cost = 34; };

class M1025A1_M2_D: Vehicle  { classname = "rhsusf_m1025_d_m2";		level = ANTI_CAR; cost = 34; animationData[] = {"hide_A2_Parts",1}; };
class M1025A1_M2_W: Vehicle  { classname = "rhsusf_m1025_w_m2";		level = ANTI_CAR; cost = 34; animationData[] = {"hide_A2_Parts",1}; };

class M1151A2_M240_GPK_D: Vehicle   { classname = "rhsusf_m1151_m240_v1_usarmy_d";	level = ANTI_CAR; cost = 32; };
class M1151A2_M240_GPK_W: Vehicle   { classname = "rhsusf_m1151_m240_v1_usarmy_wd";	level = ANTI_CAR; cost = 32; };
class M1151A2_M240_OGPK_W: Vehicle  { classname = "rhsusf_m1151_m240_v2_usarmy_d";	level = ANTI_CAR; cost = 34; };
class M1151A2_M240_OGPK_D: Vehicle  { classname = "rhsusf_m1151_m240_v2_usarmy_wd";	level = ANTI_CAR; cost = 34; };
class M1151A2_M2_GPK_D: Vehicle  	{ classname = "rhsusf_m1151_m2_v1_usarmy_d";	level = ANTI_CAR; cost = 38; };
class M1151A2_M2_GPK_W: Vehicle  	{ classname = "rhsusf_m1151_m2_v1_usarmy_wd";	level = ANTI_CAR; cost = 38; };
class M1151A2_M2_OGPK_D: Vehicle  	{ classname = "rhsusf_m1151_m2_v2_usarmy_d";	level = ANTI_CAR; cost = 40; };
class M1151A2_M2_OGPK_W: Vehicle  	{ classname = "rhsusf_m1151_m2_v2_usarmy_wd";	level = ANTI_CAR; cost = 40; };
class M1151A2_M2_CROWS_D: Vehicle  	{ classname = "rhsusf_m1151_m2crows_usarmy_d";	level = ANTI_APC; cost = 56; };
class M1151A2_M2_CROWS_W: Vehicle  	{ classname = "rhsusf_m1151_m2crows_usarmy_wd";	level = ANTI_APC; cost = 56; };

class M1025A2_M2_CW: Vehicle { classname = "UK3CB_CW_US_B_LATE_M1025_M2";	level = ANTI_CAR; cost = 34; }; 


/**
 * Humvees (US Marine Variants)
 */
class M1025A2_S_M2_D: Vehicle 			{ classname = "rhsusf_m1025_d_s_m2";			level = ANTI_CAR; cost = 34; };
class M1025A2_S_M2_W: Vehicle 			{ classname = "rhsusf_m1025_w_s_m2";			level = ANTI_CAR; cost = 34; };
class M1043A2_S_M2_D: Vehicle 			{ classname = "rhsusf_m1043_d_s_m2";			level = ANTI_CAR; cost = 34; };
class M1043A2_S_M2_W: Vehicle 			{ classname = "rhsusf_m1043_w_s_m2";			level = ANTI_CAR; cost = 34; };

class M1025A1_S_M2_D: Vehicle 			{ classname = "rhsusf_m1025_d_s_m2";			level = ANTI_CAR; cost = 34; animationData[] = {"hide_A2_Parts",1}; };
class M1025A1_S_M2_W: Vehicle 			{ classname = "rhsusf_m1025_w_s_m2";			level = ANTI_CAR; cost = 34; animationData[] = {"hide_A2_Parts",1}; };
class M1043A1_S_M2_D: Vehicle 			{ classname = "rhsusf_m1043_d_s_m2";			level = ANTI_CAR; cost = 34; animationData[] = {"hide_A2_Parts",1}; };
class M1043A1_S_M2_W: Vehicle 			{ classname = "rhsusf_m1043_w_s_m2";			level = ANTI_CAR; cost = 34; animationData[] = {"hide_A2_Parts",1}; };
class M1151A1_S_M240_MCTAGS_D: Vehicle	{ classname = "rhsusf_m1151_m240_v3_usmc_d";	level = ANTI_CAR; cost = 32; };
class M1151A1_S_M240_MCTAGS_W: Vehicle	{ classname = "rhsusf_m1151_m240_v3_usmc_wd";	level = ANTI_CAR; cost = 32; };
class M1151A1_S_M2_MCTAGS_D: Vehicle	{ classname = "rhsusf_m1151_m2_v3_usmc_d";		level = ANTI_CAR; cost = 38; };
class M1151A1_S_M2_MCTAGS_W: Vehicle	{ classname = "rhsusf_m1151_m2_v3_usmc_wd";		level = ANTI_CAR; cost = 38; };
class M1151A2_S_M2_CROWS_D: Vehicle		{ classname = "rhsusf_m1151_m2crows_usmc_d";	level = ANTI_APC; cost = 56; };
class M1151A2_S_M2_CROWS_W: Vehicle		{ classname = "rhsusf_m1151_m2crows_usmc_wd";	level = ANTI_APC; cost = 56; };


/**
 * Humvees (Other Variants)
 */
class M1025A1_M2_O: Vehicle  		{ classname = "rhssaf_m1025_olive_m2";			level = ANTI_CAR; cost = 34; };
class M1025A1_M2_GPOK_O: Vehicle	{ classname = "rhsusf_m1151_m2_v1_usarmy_wd";	level = ANTI_CAR; cost = 38; textureData[] = {"rhs_olive",1}; };
class M1151A1_PKM_GPK_O: Vehicle	{ classname = "rhssaf_m1151_olive_pkm";			level = ANTI_CAR; cost = 32; };
class M1025A2_HMG_TKA: Vehicle 		{ classname = "UK3CB_TKA_B_M1025_M2";			level = ANTI_CAR; cost = 34; };
class M1025A1_M2_TAN: Vehicle  		{ classname = "rhsgref_hidf_m1025_m2";			level = ANTI_CAR; cost = 34; };


/**
 * Land Rovers
 */
class LandRoverWMIK_GPMG_D: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GPMG_Sand_A";	level = ANTI_CAR; cost = 26; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_GPMG_W: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GPMG_Green_A";	level = ANTI_CAR; cost = 26; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_GPMG_T: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GPMG_Green_B";	level = ANTI_CAR; cost = 26; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_HMG_D:  Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_HMG_Sand_A";	level = ANTI_CAR; cost = 30; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_HMG_W:  Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_HMG_Green_A";	level = ANTI_CAR; cost = 30; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_HMG_T:  Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_HMG_Green_B";	level = ANTI_CAR; cost = 30; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };

class LandRoverWMIK_S_GPMG_D: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GPMG_Sand_A";	 level = ANTI_CAR; cost = 26; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_S_GPMG_W: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GPMG_Green_A"; level = ANTI_CAR; cost = 26; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_S_GPMG_T: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_GPMG_Green_B"; level = ANTI_CAR; cost = 26; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_S_HMG_D: Vehicle  { classname = "UK3CB_BAF_LandRover_WMIK_HMG_Sand_A";	 level = ANTI_CAR; cost = 30; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_S_HMG_W: Vehicle  { classname = "UK3CB_BAF_LandRover_WMIK_HMG_Green_A";	 level = ANTI_CAR; cost = 30; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_S_HMG_T: Vehicle  { classname = "UK3CB_BAF_LandRover_WMIK_HMG_Green_B";	 level = ANTI_CAR; cost = 30; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };

class LandRover110_HMG_INS: Vehicle  { classname = "UK3CB_TKM_I_LR_M2";		level = ANTI_CAR; cost = 26; };
class LandRoverWMIK_HMG_INS: Vehicle { classname = "UK3CB_TKM_I_LR_SF_M2";	level = ANTI_CAR; cost = 30; };

class LandRover110_HMG_TKA: Vehicle  { classname = "UK3CB_TKA_I_LR_M2";		level = ANTI_CAR; cost = 26; };
class LandRoverWMIK_HMG_TKA: Vehicle { classname = "UK3CB_TKA_B_LR_SF_M2";	level = ANTI_CAR; cost = 30; };


/**
 * UAZ
 */
class UAZ_DSHKM_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_EARLY_UAZ_MG"; level = ANTI_CAR; cost = 28; };

class UAZ_DSHKM_CDF: Vehicle { classname = "rhsgref_cdf_b_reg_uaz_dshkm"; level = ANTI_CAR; cost = 28; };
class UAZ_DSHKM_UN: Vehicle  { classname = "UK3CB_UN_I_UAZ_MG";			  level = ANTI_CAR; cost = 28; };

class UAZ_DSHKM_CHE: Vehicle { classname = "rhsgref_ins_uaz_dshkm"; level = ANTI_CAR; cost = 28; };


/**
 * GAZ Vodnik
 */
class GAZVodnik_PKT_TKA: Vehicle   { classname = "UK3CB_TKA_I_GAZ_Vodnik_PKT";		level = ANTI_CAR; cost = 28; };
class GAZVodnik_HMG_TKA: Vehicle   { classname = "UK3CB_TKA_O_GAZ_Vodnik_KVPT";		level = ANTI_CAR; cost = 34; };
class GAZVodnik_CROWS_TKA: Vehicle { classname = "UK3CB_TKA_O_GAZ_Vodnik_HMG";		level = ANTI_CAR; cost = 44; };
class GAZVodnik_30MM_TKA: Vehicle  { classname = "UK3CB_TKA_O_GAZ_Vodnik_Cannon";	level = ANTI_APC; cost = 60; };

class GAZVodnik_PKT_URS: Vehicle { classname = "UK3CB_TKA_O_GAZ_Vodnik_PKT"; level = ANTI_CAR; cost = 28; textureData[] = {"Russian",1}; };


/**
 * Datsun 620
 */
class Datsun_PKM_INS: Vehicle { classname = "UK3CB_CCM_O_Datsun_Pkm"; level = ANTI_CAR; cost = 14; };


/**
 * Hilux
 */
class Hilux_PKM_INS: Vehicle   { classname = "UK3CB_CCM_O_Hilux_Pkm";	level = ANTI_CAR; cost = 16; };
class Hilux_DSHKM_INS: Vehicle { classname = "UK3CB_CCM_O_Hilux_Dshkm";	level = ANTI_CAR; cost = 20; };
class Hilux_PKM_BGM: Vehicle   { classname = "UK3CB_O_G_Hilux_Pkm";		level = ANTI_CAR; cost = 16; };
class Hilux_DSHKM_BGM: Vehicle { classname = "UK3CB_O_G_Hilux_Dshkm";	level = ANTI_CAR; cost = 20; };


/**
 * Offroad
 */
class Offroad_M2_INS: Vehicle { classname = "I_G_Offroad_01_armed_F";		level = ANTI_CAR; cost = 22; };

class Offroad_M2_ION: Vehicle { classname = "tacs_Offroad_B_Armed_Black";	level = ANTI_CAR; cost = 22; };


/**
 * Jeep Wrangler (PMC)
 */
class Jeep_LMG_D: Vehicle	{ classname = "I_C_Offroad_02_LMG_F";	level = ANTI_CAR; cost = 20; textureData[] = {"Brown",1}; };
class Jeep_LMG_W: Vehicle	{ classname = "I_C_Offroad_02_LMG_F";	level = ANTI_CAR; cost = 20; textureData[] = {"Olive",1}; };


/**
 * SUV 4x4
 */
class SUV_Armored_M134_ION: Vehicle { classname = "UK3CB_B_SUV_Armed"; name = "SUV 4x4 Armored (M134)"; level = ANTI_CAR; cost = 34; textureData[] = {"Black",1}; };