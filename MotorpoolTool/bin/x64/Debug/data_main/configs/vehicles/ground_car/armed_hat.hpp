/**
 * M151 Jeep
 */
class M151_TOW_CW: Vehicle { classname = "UK3CB_CW_US_B_LATE_M151_Jeep_TOW"; level = ANTI_APC; cost = 26; };

class M151_TOW_TAN: Vehicle	{ classname = "UK3CB_B_M151_Jeep_TOW_HIDF";	level = ANTI_APC; cost = 26; };


/**
 * Humvees (US Army Variants)
 */
class M1045A2_TOW_D: Vehicle { classname = "rhsusf_m966_d"; level = ANTI_APC; cost = 36; };
class M1045A2_TOW_W: Vehicle { classname = "rhsusf_m966_w"; level = ANTI_APC; cost = 36; };

class M1045A1_TOW_D: Vehicle { classname = "rhsusf_m966_d"; level = ANTI_APC; cost = 36; animationData[] = {"hide_A2_Parts",1}; };
class M1045A1_TOW_W: Vehicle { classname = "rhsusf_m966_w"; level = ANTI_APC; cost = 36; animationData[] = {"hide_A2_Parts",1}; };

class M1045A2_TOW_CW: Vehicle { classname = "UK3CB_CW_US_B_LATE_M1025_TOW"; level = ANTI_APC; cost = 36; };


/**
 * Humvees (US Marine Variants)
 */
class M1045A2_S_TOW_D: Vehicle { classname = "rhsusf_m1045_d_s"; level = ANTI_APC; cost = 36; };
class M1045A2_S_TOW_W: Vehicle { classname = "rhsusf_m1045_w_s"; level = ANTI_APC; cost = 36; };

class M1045A1_S_TOW_D: Vehicle { classname = "rhsusf_m1045_d_s"; level = ANTI_APC; cost = 36; animationData[] = {"hide_A2_Parts",1}; };
class M1045A1_S_TOW_W: Vehicle { classname = "rhsusf_m1045_w_s"; level = ANTI_APC; cost = 36; animationData[] = {"hide_A2_Parts",1}; };


/**
 * Land Rovers
 */
class LandRoverWMIK_HAT_D: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_Milan_Sand_A";  level = ANTI_APC; cost = 34; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_HAT_W: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_Milan_Green_A"; level = ANTI_APC; cost = 34; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_HAT_T: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_Milan_Green_B"; level = ANTI_APC; cost = 34; animationData[] = {"AirIntakeSnorkel_Hide",1,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };

class LandRoverWMIK_S_HAT_D: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_Milan_Sand_A";  level = ANTI_APC; cost = 34; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_S_HAT_W: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_Milan_Green_A"; level = ANTI_APC; cost = 34; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };
class LandRoverWMIK_S_HAT_T: Vehicle { classname = "UK3CB_BAF_LandRover_WMIK_Milan_Green_B"; level = ANTI_APC; cost = 34; animationData[] = {"AirIntakeSnorkel_Hide",0,"CamoNet_Hide",1,"Gear_Hide",1,"AerialAtuL_Hide",1,"AerialAtuR_Hide",1,"AerialFL_Hide",1,"AerialFR_Hide",1,"Flag_Hide",1,"Mudguards_Hide",0,"SideLockers_Hide",0}; };

class LandRover110_SPG9_INS: Vehicle { classname = "UK3CB_TKM_I_LR_SPG9"; level = ANTI_APC; cost = 34; };

class LandRover110_SPG9_TKA: Vehicle { classname = "UK3CB_TKA_I_LR_SPG9"; level = ANTI_APC; cost = 34; };


/**
 * UAZ
 */
class UAZ_SPG9_SOV: Vehicle	{ classname = "UK3CB_CW_SOV_O_EARLY_UAZ_SPG9";	level = ANTI_APC; cost = 30; };

class UAZ_SPG9_CDF: Vehicle { classname = "rhsgref_cdf_b_reg_uaz_spg9";	level = ANTI_APC; cost = 30; };
class UAZ_SPG9_UN: Vehicle  { classname = "UK3CB_UN_I_UAZ_SPG9";	level = ANTI_APC; cost = 30; };

class UAZ_SPG9_CHE: Vehicle { classname = "rhsgref_ins_uaz_spg9";	level = ANTI_APC; cost = 30; };


/**
 * Hilux
 */
class Hilux_SPG9_INS: Vehicle   { classname = "UK3CB_TKM_I_Hilux_Spg9";	level = ANTI_APC; cost = 22; };
class Hilux_Rocket_INS: Vehicle { classname = "UK3CB_I_G_Hilux_Rocket";	level = ANTI_APC; cost = 22; };
class Hilux_Rocket_BGM: Vehicle { classname = "UK3CB_O_G_Hilux_Rocket";	level = ANTI_APC; cost = 22; };


/**
 * Offroad
 */
class Offroad_SPG9_INS: Vehicle { classname = "I_G_Offroad_01_AT_F"; level = ANTI_APC; cost = 24; };


/**
 * Jeep Wrangler (PMC)
 */
class Jeep_SPG9_D: Vehicle	{ classname = "I_C_Offroad_02_AT_F"; level = ANTI_APC; cost = 22; textureData[] = {"Brown",1}; };
class Jeep_SPG9_W: Vehicle	{ classname = "I_C_Offroad_02_AT_F"; level = ANTI_APC; cost = 22; textureData[] = {"Olive",1}; };