/**
 * M1A1 (US Army Variants)
 */
class M1A1_D: Vehicle    { classname = "rhsusf_m1a1aimd_usarmy";  level = ANTI_MBT; cost = 138; crew = 3; animationData[] = {"IFF_Panels_Hide",1,"Miles_Hide",1}; };
class M1A1_W: Vehicle    { classname = "rhsusf_m1a1aimwd_usarmy"; level = ANTI_MBT; cost = 138; crew = 3; animationData[] = {"IFF_Panels_Hide",1,"Miles_Hide",1}; };
class M1A1_T1_D: Vehicle { classname = "rhsusf_m1a1aim_tuski_d";  level = ANTI_MBT; cost = 168; crew = 3; };
class M1A1_T1_W: Vehicle { classname = "rhsusf_m1a1aim_tuski_wd"; level = ANTI_MBT; cost = 168; crew = 3; };

class M1A1_CW: Vehicle    { classname = "UK3CB_CW_US_B_EARLY_M1A1"; level = ANTI_MBT; cost = 130; crew = 3; };


/**
 * M1A2 (US Army Variants)
 */
class M1A2_D: Vehicle    { classname = "rhsusf_m1a2sep1d_usarmy";  		 level = ANTI_MBT; cost = 184; crew = 3; animationData[] = {"IFF_Panels_Hide",1,"Miles_Hide",1}; };
class M1A2_W: Vehicle    { classname = "rhsusf_m1a2sep1wd_usarmy"; 		 level = ANTI_MBT; cost = 184; crew = 3; animationData[] = {"IFF_Panels_Hide",1,"Miles_Hide",1}; };
class M1A2_T1_D: Vehicle { classname = "rhsusf_m1a2sep1tuskid_usarmy";   level = ANTI_MBT; cost = 202; crew = 3; };
class M1A2_T1_W: Vehicle { classname = "rhsusf_m1a2sep1tuskiwd_usarmy";  level = ANTI_MBT; cost = 202; crew = 3; };
class M1A2_T2_D: Vehicle { classname = "rhsusf_m1a2sep1tuskiid_usarmy";  level = ANTI_MBT; cost = 222; crew = 3; };
class M1A2_T2_W: Vehicle { classname = "rhsusf_m1a2sep1tuskiiwd_usarmy"; level = ANTI_MBT; cost = 222; crew = 3; };


/**
 * M1A1 (US Marine Variants)
 */
class M1A1FEP_D: Vehicle { classname = "rhsusf_m1a1fep_d";  level = ANTI_MBT; cost = 152; crew = 3; animationData[] = {"IFF_Panels_Hide",1,"Miles_Hide",1}; };
class M1A1FEP_W: Vehicle { classname = "rhsusf_m1a1fep_wd"; level = ANTI_MBT; cost = 152; crew = 3; animationData[] = {"IFF_Panels_Hide",1,"Miles_Hide",1}; };


/**
 * Leopard 2
 */
class Leopard2_D: Vehicle { classname = "BWA3_Leopard2_Tropen"; level = ANTI_MBT; cost = 222; crew = 3; };
class Leopard2_W: Vehicle { classname = "BWA3_Leopard2_Fleck";  level = ANTI_MBT; cost = 222; crew = 3; };


/**
 * T-72
 */
class T72B84_D: Vehicle { classname = "rhs_t72ba_tv"; level = ANTI_MBT; cost = 114; crew = 3; textureData[] = {"rhs_Sand",1}; };
class T72B84_W: Vehicle { classname = "rhs_t72ba_tv"; level = ANTI_MBT; cost = 114; crew = 3; };
class T72B85_D: Vehicle { classname = "rhs_t72bb_tv"; level = ANTI_MBT; cost = 126; crew = 3; textureData[] = {"rhs_Sand",1}; };
class T72B85_W: Vehicle { classname = "rhs_t72bb_tv"; level = ANTI_MBT; cost = 126; crew = 3; };
class T72B89_D: Vehicle { classname = "rhs_t72bc_tv"; level = ANTI_MBT; cost = 138; crew = 3; textureData[] = {"rhs_Sand",1}; };
class T72B89_W: Vehicle { classname = "rhs_t72bc_tv"; level = ANTI_MBT; cost = 138; crew = 3; };
class T72B12_D: Vehicle { classname = "rhs_t72bd_tv"; level = ANTI_MBT; cost = 152; crew = 3; textureData[] = {"rhs_Sand",1}; };
class T72B12_W: Vehicle { classname = "rhs_t72bd_tv"; level = ANTI_MBT; cost = 152; crew = 3; };
class T72B16_D: Vehicle { classname = "rhs_t72be_tv"; level = ANTI_MBT; cost = 168; crew = 3; textureData[] = {"rhs_Sand",1}; };
class T72B16_W: Vehicle { classname = "rhs_t72be_tv"; level = ANTI_MBT; cost = 168; crew = 3; };

class T72B_SOV: Vehicle  { classname = "UK3CB_CW_SOV_O_LATE_T72BM"; level = ANTI_MBT; cost = 114; crew = 3; };
class T72BA_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_LATE_T72BA"; level = ANTI_MBT; cost = 126; crew = 3; };
class T72BB_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_LATE_T72BB"; level = ANTI_MBT; cost = 138; crew = 3; };

class T72B84_CDF: Vehicle	{ classname = "rhsgref_cdf_b_t72ba_tv";	level = ANTI_MBT; cost = 114; crew = 3; };
class T72B85_CDF: Vehicle	{ classname = "rhsgref_cdf_b_t72bb_tv";	level = ANTI_MBT; cost = 126; crew = 3; };

class T72A_UN: Vehicle	{ classname = "UK3CB_UN_I_T72A";	level = ANTI_MBT; cost = 102; crew = 3; };
class T72BM_UN: Vehicle	{ classname = "UK3CB_UN_I_T72BM";	level = ANTI_MBT; cost = 114; crew = 3; };
class T72B_UN: Vehicle	{ classname = "UK3CB_UN_I_T72B";	level = ANTI_MBT; cost = 126; crew = 3; };

class T72B84_CHE: Vehicle	{ classname = "rhsgref_ins_t72ba";	level = ANTI_MBT; cost = 114; crew = 3; };
class T72B85_CHE: Vehicle	{ classname = "rhsgref_ins_t72bb";	level = ANTI_MBT; cost = 126; crew = 3; };

class T72A_TKA: Vehicle		{ classname = "UK3CB_TKA_B_T72A";	level = ANTI_MBT; cost = 102; crew = 3; };
class T72BM_TKA: Vehicle	{ classname = "UK3CB_TKA_B_T72BM";	level = ANTI_MBT; cost = 114; crew = 3; };
class T72B_TKA: Vehicle		{ classname = "UK3CB_TKA_B_T72B";	level = ANTI_MBT; cost = 126; crew = 3; };

class T72BM_URS: Vehicle	{ classname = "UK3CB_CW_SOV_O_LATE_T72BM"; level = ANTI_MBT; cost = 114; crew = 3; textureData[] = {"SOV2",1}; };
class T72B_URS: Vehicle		{ classname = "UK3CB_CW_SOV_O_LATE_T72B";  level = ANTI_MBT; cost = 126; crew = 3; textureData[] = {"SOV2",1}; };


/**
 * T-80
 */
class T80: Vehicle 	   { classname = "rhs_t80";    level = ANTI_MBT; cost = 138; crew = 3; };
class T80A: Vehicle    { classname = "rhs_t80a";   level = ANTI_MBT; cost = 152; crew = 3; };
class T80B: Vehicle    { classname = "rhs_t80b";   level = ANTI_MBT; cost = 138; crew = 3; };
class T80BV: Vehicle   { classname = "rhs_t80bv";  level = ANTI_MBT; cost = 152; crew = 3; };
class T80BVK: Vehicle  { classname = "rhs_t80bvk"; level = ANTI_MBT; cost = 168; crew = 3; };
class T80U_D: Vehicle  { classname = "rhs_t80u";   level = ANTI_MBT; cost = 184; crew = 3; textureData[] = {"tricolor",1}; };
class T80U_W: Vehicle  { classname = "rhs_t80u";   level = ANTI_MBT; cost = 184; crew = 3; };
class T80UM_D: Vehicle { classname = "rhs_t80um";  level = ANTI_MBT; cost = 202; crew = 3; textureData[] = {"tricolor",1}; };
class T80UM_W: Vehicle { classname = "rhs_t80um";  level = ANTI_MBT; cost = 202; crew = 3; };

class T80B_CDF: Vehicle		{ classname = "rhsgref_cdf_b_t80b_tv";	level = ANTI_MBT; cost = 138; crew = 3; };
class T80BV_CDF: Vehicle	{ classname = "rhsgref_cdf_b_t80bv_tv";	level = ANTI_MBT; cost = 152; crew = 3; };


/**
 * T-90
 */
class T90: Vehicle  { classname = "rhs_t90_tv";  level = ANTI_MBT; cost = 168; };
class T90A: Vehicle { classname = "rhs_t90a_tv"; level = ANTI_MBT; cost = 184; };
class T90SA04: Vehicle  { classname = "rhs_t90saa_tv"; level = ANTI_MBT; cost = 202; };
class T90SA16: Vehicle  { classname = "rhs_t90sab_tv"; level = ANTI_MBT; cost = 208; };
class T90AM: Vehicle  { classname = "rhs_t90am_tv"; level = ANTI_MBT; cost = 216; };
class T90SM: Vehicle  { classname = "rhs_t90sm_tv"; level = ANTI_MBT; cost = 222; };


/**
 * T-14
 */
class T14: Vehicle  { classname = "rhs_t14_tv";  level = ANTI_MBT; cost = 244; };


/**
 * T-72S (Serbia)
 */
class T72S: Vehicle { classname = "rhssaf_army_t72s"; level = ANTI_MBT; cost = 138; crew = 3; };


/**
 * T-34-85M (Insurgents)
 */
class T3485M_INS_W: Vehicle { classname = "UK3CB_I_G_T34";   level = ANTI_IFV; cost = 58; crew = 3; };
class T3485M_INS_D: Vehicle { classname = "UK3CB_TKM_I_T34"; level = ANTI_IFV; cost = 58; crew = 3; };

class T3485M_TKA: Vehicle { classname = "UK3CB_TKA_O_T34";   level = ANTI_IFV; cost = 58; crew = 3; };


/**
 * T-55 Series (Insurgents)
 */
class T55_INS_W: Vehicle { classname = "UK3CB_I_G_T55";   level = ANTI_MBT; cost = 82; };
class T55_INS_D: Vehicle { classname = "UK3CB_TKM_I_T55"; level = ANTI_MBT; cost = 82; };

class T55A_TKA: Vehicle	{ classname = "UK3CB_TKA_B_T55";  level = ANTI_MBT; cost = 82; };


/**
 * M60 (Horizon Islands)
 */
class M60A1_TAN: Vehicle { classname = "UK3CB_B_M60A1_HIDF";   level = ANTI_MBT; cost = 90;  crew = 3; };
class M60A3_TAN: Vehicle { classname = "UK3CB_B_M60A3_HIDF";   level = ANTI_MBT; cost = 110; crew = 3; };