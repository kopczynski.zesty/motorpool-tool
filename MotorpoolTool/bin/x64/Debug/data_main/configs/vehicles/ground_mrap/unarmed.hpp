/**
 * Caiman MRAPs
 */
class M1220_D: Vehicle   { classname = "rhsusf_M1220_usarmy_d";    level = ANTI_CAR; cost = 44; };
class M1220_W: Vehicle   { classname = "rhsusf_M1220_usarmy_wd";   level = ANTI_CAR; cost = 44; };
class M1230A1_D: Vehicle { classname = "rhsusf_M1230a1_usarmy_d";  level = ANTI_CAR; cost = 44; };
class M1230A1_W: Vehicle { classname = "rhsusf_M1230a1_usarmy_wd"; level = ANTI_CAR; cost = 44; };


/**
 * RG-33 MRAPs (US Army Variants)
 */
class M1232_D: Vehicle   { classname = "rhsusf_M1232_usarmy_d";   level = ANTI_CAR; cost = 52; };
class M1232_W: Vehicle   { classname = "rhsusf_M1232_usarmy_wd";  level = ANTI_CAR; cost = 52; };

class M1238A1_D: Vehicle { classname = "rhsusf_M1238A1_socom_d";  level = ANTI_CAR; cost = 48; };
class M1238A1_W: Vehicle { classname = "rhsusf_M1238A1_socom_wd"; level = ANTI_CAR; cost = 48; }; 

class M1239_D: Vehicle   { classname = "rhsusf_M1239_socom_d";    level = ANTI_CAR; cost = 36; };
class M1239_W: Vehicle   { classname = "rhsusf_M1239_socom_wd";   level = ANTI_CAR; cost = 36; };


/**
 * M-ATV MRAPs (US Army Variants)
 */
class M1240_D: Vehicle   { classname = "rhsusf_m1240a1_usarmy_d";  level = ANTI_CAR; cost = 36; };
class M1240_W: Vehicle   { classname = "rhsusf_m1240a1_usarmy_wd"; level = ANTI_CAR; cost = 36; };


/**
 * CAT MRAPs (US Marine Variants)
 */
class CGRCAT1A2_D: Vehicle   { classname = "rhsusf_CGRCAT1A2_usmc_d";  level = ANTI_CAR; cost = 44; };
class CGRCAT1A2_W: Vehicle   { classname = "rhsusf_CGRCAT1A2_usmc_wd"; level = ANTI_CAR; cost = 44; };


/**
 * Eagle IV
 */
class EagleIV_D: Vehicle { classname = "BWA3_Eagle_Tropen"; level = ANTI_CAR; cost = 36; animationData[] = {"hide_rope",0,"backpack_back",1,"backpack_top",1,"tarnrolle_1",0}; };
class EagleIV_W: Vehicle { classname = "BWA3_Eagle_Fleck";  level = ANTI_CAR; cost = 36; animationData[] = {"hide_rope",0,"backpack_back",1,"backpack_top",1,"tarnrolle_1",0}; };

class EagleIV_ION: Vehicle { classname = "tacs_BWA3_B_Eagle_Black"; level = ANTI_CAR; cost = 36; textureData[] = {"tacs_Black",1}; animationData[] = {"hide_rope",0,"backpack_back",1,"backpack_top",1,"tarnrolle_1",1}; };


/**
 * GAZ Tigr
 */
class GAZTigr_MSV_D: Vehicle   { classname = "rhs_tigr_3camo_msv";   level = ANTI_CAR; cost = 36; };
class GAZTigr_MSV_W: Vehicle   { classname = "rhs_tigr_msv"; 	     level = ANTI_CAR; cost = 36; };
class GAZTigr_O_MSV_D: Vehicle { classname = "rhs_tigr_m_3camo_msv"; level = ANTI_CAR; cost = 36; };
class GAZTigr_O_MSV_W: Vehicle { classname = "rhs_tigr_m_msv"; 	     level = ANTI_CAR; cost = 36; };

class GAZTigr_VDV_D: Vehicle   { classname = "rhs_tigr_3camo_vdv";   level = ANTI_CAR; cost = 36; };
class GAZTigr_VDV_W: Vehicle   { classname = "rhs_tigr_vdv"; 	     level = ANTI_CAR; cost = 36; };
class GAZTigr_O_VDV_D: Vehicle { classname = "rhs_tigr_m_3camo_vdv"; level = ANTI_CAR; cost = 36; };
class GAZTigr_O_VDV_W: Vehicle { classname = "rhs_tigr_m_vdv"; 	     level = ANTI_CAR; cost = 36; };

class GAZTigr_VMF_D: Vehicle   { classname = "rhs_tigr_3camo_vmf";   level = ANTI_CAR; cost = 36; };
class GAZTigr_VMF_W: Vehicle   { classname = "rhs_tigr_vmf"; 	     level = ANTI_CAR; cost = 36; };
class GAZTigr_O_VMF_D: Vehicle { classname = "rhs_tigr_m_3camo_vmf"; level = ANTI_CAR; cost = 36; };
class GAZTigr_O_VMF_W: Vehicle { classname = "rhs_tigr_m_vmf"; 	     level = ANTI_CAR; cost = 36; };