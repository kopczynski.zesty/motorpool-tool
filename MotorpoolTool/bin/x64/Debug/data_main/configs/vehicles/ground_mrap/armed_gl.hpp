/**
 * Caiman MRAPs
 */
class M1220_Mk19_D: Vehicle { classname = "rhsusf_M1220_MK19_usarmy_d";  level = ANTI_APC; cost = 88; };
class M1220_Mk19_W: Vehicle { classname = "rhsusf_M1220_MK19_usarmy_wd"; level = ANTI_APC; cost = 88; };
class M1220_CROWS_Mk19_D: Vehicle { classname = "rhsusf_M1220_M153_MK19_usarmy_d";  level = ANTI_APC; cost = 96; };
class M1220_CROWS_Mk19_W: Vehicle { classname = "rhsusf_M1220_M153_MK19_usarmy_wd"; level = ANTI_APC; cost = 96; };
class M1230_Mk19_D: Vehicle { classname = "rhsusf_M1230_MK19_usarmy_d";  level = ANTI_APC; cost = 88; };
class M1230_Mk19_W: Vehicle { classname = "rhsusf_M1230_MK19_usarmy_wd"; level = ANTI_APC; cost = 88; };


/**
 * RG-33 MRAPs (US Army Variants)
 */
class M1232_Mk19_D: Vehicle { classname = "rhsusf_M1232_MK19_usarmy_d";  level = ANTI_APC; cost = 104; };
class M1232_Mk19_W: Vehicle { classname = "rhsusf_M1232_MK19_usarmy_wd"; level = ANTI_APC; cost = 104; };
class M1237_Mk19_D: Vehicle { classname = "rhsusf_M1237_MK19_usarmy_d";  level = ANTI_APC; cost = 116; };
class M1237_Mk19_W: Vehicle { classname = "rhsusf_M1237_MK19_usarmy_wd"; level = ANTI_APC; cost = 116; };


/**
 * RG-33 MRAPs (US Marine Variants)
 */
class M1232_MC_Mk19_D: Vehicle { classname = "rhsusf_M1232_MC_MK19_usmc_d";					level = ANTI_APC; cost = 104; };
class M1232_MC_Mk19_W: Vehicle { classname = "rhsusf_M1232_MC_MK19_usmc_wd";				level = ANTI_APC; cost = 104; };

class M1238A1_CROWS_Mk19_D: Vehicle { classname = "rhsusf_M1238A1_Mk19_socom_d";			level = ANTI_APC; cost = 106; };
class M1238A1_CROWS_Mk19_W: Vehicle { classname = "rhsusf_M1238A1_Mk19_socom_wd";			level = ANTI_APC; cost = 106; };

class M1239_CROWS_Mk19_D: Vehicle { classname = "rhsusf_M1239_MK19_socom_d";				level = ANTI_APC; cost = 80; };
class M1239_CROWS_Mk19_W: Vehicle { classname = "rhsusf_M1239_MK19_socom_wd";				level = ANTI_APC; cost = 80; };
class M1239_Deploy_CROWS_Mk19_D: Vehicle { classname = "rhsusf_M1239_MK19_Deploy_socom_d";	level = ANTI_APC; cost = 80; };
class M1239_Deploy_CROWS_Mk19_W: Vehicle { classname = "rhsusf_M1239_MK19_Deploy_socom_wd";	level = ANTI_APC; cost = 80; };


/**
 * M-ATV MRAPs (US Army Variants)
 */
class M1240_Mk19_D: Vehicle    { classname = "rhsusf_m1240a1_mk19_usarmy_d";  	 level = ANTI_APC; cost = 72; };
class M1240_Mk19_W: Vehicle    { classname = "rhsusf_m1240a1_mk19_usarmy_wd"; 	 level = ANTI_APC; cost = 72; };
class M1240_CROWS_Mk19_D: Vehicle { classname = "rhsusf_m1240a1_mk19crows_usarmy_d";  level = ANTI_APC; cost = 80; };
class M1240_CROWS_Mk19_W: Vehicle { classname = "rhsusf_m1240a1_mk19crows_usarmy_wd"; level = ANTI_APC; cost = 80; };


/**
 * CAT MRAPs (US Marine Variants)
 */
class CGRCAT1A2_Mk19_D: Vehicle   { classname = "rhsusf_CGRCAT1A2_Mk19_usmc_d";  level = ANTI_APC; cost = 88; };
class CGRCAT1A2_Mk19_W: Vehicle   { classname = "rhsusf_CGRCAT1A2_Mk19_usmc_wd"; level = ANTI_APC; cost = 88; };


/**
 * Maxxpro MRAPs (US Army Variants)
 */
class MaxxPro_Mk19_D: Vehicle { classname = "UK3CB_B_MaxxPro_MK19_US"; level = ANTI_APC; cost = 88; };


/**
 * Jackal/Coyote
 */
class Jackal_GMG_D: Vehicle { classname = "UK3CB_BAF_Jackal2_L134A1_D";  level = ANTI_APC; cost = 56; };
class Jackal_GMG_W: Vehicle { classname = "UK3CB_BAF_Jackal2_L134A1_W";  level = ANTI_APC; cost = 56; };
class Coyote_GMG_D: Vehicle { classname = "UK3CB_BAF_Coyote_Passenger_L134A1_D";  level = ANTI_APC; cost = 68; };
class Coyote_GMG_W: Vehicle { classname = "UK3CB_BAF_Coyote_Passenger_L134A1_W";  level = ANTI_APC; cost = 68; };


/**
 * Husky TSV
 */
class HuskyP_GMG_D:  Vehicle { classname = "UK3CB_BAF_Husky_Passenger_GMG_Sand";   level = ANTI_APC; cost = 68; };
class HuskyP_GMG_W:  Vehicle { classname = "UK3CB_BAF_Husky_Passenger_GMG_Green";  level = ANTI_APC; cost = 68; };


/**
 * GAZ Tigr
 */
class GAZTigr_GMG_MSV_D: Vehicle { classname = "rhs_tigr_sts_3camo_msv"; level = ANTI_APC; cost = 72; };
class GAZTigr_GMG_MSV_W: Vehicle { classname = "rhs_tigr_sts_msv"; 	     level = ANTI_APC; cost = 72; };
class GAZTigr_GMG_VDV_D: Vehicle { classname = "rhs_tigr_sts_3camo_vdv"; level = ANTI_APC; cost = 72; };
class GAZTigr_GMG_VDV_W: Vehicle { classname = "rhs_tigr_sts_vdv"; 	     level = ANTI_APC; cost = 72; };
class GAZTigr_GMG_VMF_D: Vehicle { classname = "rhs_tigr_sts_3camo_vmf"; level = ANTI_APC; cost = 72; };
class GAZTigr_GMG_VMF_W: Vehicle { classname = "rhs_tigr_sts_vmf"; 	     level = ANTI_APC; cost = 72; };
