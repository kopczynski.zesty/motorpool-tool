/**
 * MAN HX58/60 Cargo Containers
 */
class MANHX60_CO_D: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Container_Sand";  name = "4x4 Cargo Container"; level = UTILITY; cost = 10; };
class MANHX60_CO_W: Vehicle { classname = "UK3CB_BAF_MAN_HX60_Container_Green"; name = "4x4 Cargo Container"; level = UTILITY; cost = 10; };
class MANHX58_CO_D: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Container_Sand";  name = "6x6 Cargo Container"; level = UTILITY; cost = 12; };
class MANHX58_CO_W: Vehicle { classname = "UK3CB_BAF_MAN_HX58_Container_Green"; name = "6x6 Cargo Container"; level = UTILITY; cost = 12; };