/**
 * M2A2
 */
class M2A2_D: Vehicle 	 { classname = "RHS_M2A2"; 			level = ANTI_IFV; cost = 86; animationData[] = {"IFF_Panels_Hide",1}; };
class M2A2_W: Vehicle 	 { classname = "RHS_M2A2_wd"; 		level = ANTI_IFV; cost = 86; animationData[] = {"IFF_Panels_Hide",1}; };
class M2A2_B1_D: Vehicle { classname = "RHS_M2A2_BUSKI"; 	level = ANTI_IFV; cost = 104; };
class M2A2_B1_W: Vehicle { classname = "RHS_M2A2_BUSKI_WD"; level = ANTI_IFV; cost = 104; };


/**
 * M2A3
 */
class M2A3_D: Vehicle 	 { classname = "RHS_M2A3"; 			  level = ANTI_IFV; cost = 104; };
class M2A3_W: Vehicle 	 { classname = "RHS_M2A3_wd"; 		  level = ANTI_IFV; cost = 104; };
class M2A3_B1_D: Vehicle { classname = "RHS_M2A3_BUSKI"; 	  level = ANTI_IFV; cost = 124; };
class M2A3_B1_W: Vehicle { classname = "RHS_M2A3_BUSKI_wd";   level = ANTI_IFV; cost = 124; };
class M2A3_B3_D: Vehicle { classname = "RHS_M2A3_BUSKIII"; 	  level = ANTI_IFV; cost = 148; };
class M2A3_B3_W: Vehicle { classname = "RHS_M2A3_BUSKIII_wd"; level = ANTI_IFV; cost = 148; };


/**
 * Marder 1A5
 */
class Marder1A5_D: Vehicle { classname = "Redd_Marder_1A5_Tropentarn"; level = ANTI_IFV; cost = 86; crew = 3; };
class Marder1A5_W: Vehicle { classname = "Redd_Marder_1A5_Flecktarn";  level = ANTI_IFV; cost = 86; crew = 3; };
class Marder1A5_A: Vehicle { classname = "Redd_Marder_1A5_Wintertarn"; level = ANTI_IFV; cost = 86; crew = 3; };


/**
 * Puma
 */
class Puma_D: Vehicle { classname = "BWA3_Puma_Tropen"; level = ANTI_IFV; cost = 120; animationData[] = {"hide_netz",1,"hide_netz_turret",1,"hide_netz_gun",1,"hide_netz_mells",1}; };
class Puma_W: Vehicle { classname = "BWA3_Puma_Fleck";  level = ANTI_IFV; cost = 120; animationData[] = {"hide_netz",1,"hide_netz_turret",1,"hide_netz_gun",1,"hide_netz_mells",1}; };


/**
 * FV510 Warrior
 */
class FV510_D: Vehicle  { classname = "UK3CB_BAF_Warrior_A3_D_MTP"; name = "FV510 Warrior"; level = ANTI_IFV; cost = 86; animationData[] = {"showBags",0,"showBags2",1,"showCamonetHull",0,"showCamonetTurret",0,"showTools",1,"showSLATHull",0,"showSLATTurret",0}; };
class FV510_W: Vehicle  { classname = "UK3CB_BAF_Warrior_A3_W_MTP";  name = "FV510 Warrior"; level = ANTI_IFV; cost = 86; animationData[] = {"showBags",0,"showBags2",1,"showCamonetHull",0,"showCamonetTurret",0,"showTools",1,"showSLATHull",0,"showSLATTurret",0}; };

class FV510N_D: Vehicle  { classname = "UK3CB_BAF_Warrior_A3_D_MTP"; name = "FV510 Warrior (Net)"; level = ANTI_IFV; cost = 86; animationData[] = {"showBags",0,"showBags2",1,"showCamonetHull",1,"showCamonetTurret",1,"showTools",1,"showSLATHull",0,"showSLATTurret",0}; };
class FV510N_W: Vehicle  { classname = "UK3CB_BAF_Warrior_A3_W_MTP";  name = "FV510 Warrior (Net)"; level = ANTI_IFV; cost = 86; animationData[] = {"showBags",0,"showBags2",1,"showCamonetHull",1,"showCamonetTurret",1,"showTools",1,"showSLATHull",0,"showSLATTurret",0}; };

class FV510C_D: Vehicle { classname = "UK3CB_BAF_Warrior_A3_D_Cage_MTP"; name = "FV510 Warrior (Cage)"; level = ANTI_IFV; cost = 104; animationData[] = {"showBags",0,"showBags2",1,"showCamonetHull",0,"showCamonetTurret",0,"showTools",1,"showSLATHull",1,"showSLATTurret",1}; };
class FV510C_W: Vehicle { classname = "UK3CB_BAF_Warrior_A3_W_Cage_MTP";  name = "FV510 Warrior (Cage)"; level = ANTI_IFV; cost = 104; animationData[] = {"showBags",0,"showBags2",1,"showCamonetHull",0,"showCamonetTurret",0,"showTools",1,"showSLATHull",1,"showSLATTurret",1}; };

class FV510CN_D: Vehicle { classname = "UK3CB_BAF_Warrior_A3_D_Cage_MTP"; name = "FV510 Warrior (Cage & Net)"; level = ANTI_IFV; cost = 104; animationData[] = {"showBags",0,"showBags2",1,"showCamonetHull",1,"showCamonetTurret",1,"showTools",1,"showSLATHull",1,"showSLATTurret",1}; };
class FV510CN_W: Vehicle { classname = "UK3CB_BAF_Warrior_A3_W_Cage_MTP";  name = "FV510 Warrior (Cage & Net)"; level = ANTI_IFV; cost = 104; animationData[] = {"showBags",0,"showBags2",1,"showCamonetHull",1,"showCamonetTurret",1,"showTools",1,"showSLATHull",1,"showSLATTurret",1}; };


/**
 * BMP-1
 */
class BMP1_MSV_D: Vehicle  { classname = "rhs_bmp1_msv";  level = ANTI_IFV; cost = 72; textureData[] = {"rhs_sand",1}; };
class BMP1_MSV_W: Vehicle  { classname = "rhs_bmp1_msv";  level = ANTI_IFV; cost = 72; };
class BMP1P_MSV_D: Vehicle { classname = "rhs_bmp1p_msv"; level = ANTI_IFV; cost = 86; crew = 3; textureData[] = {"rhs_sand",1}; };
class BMP1P_MSV_W: Vehicle { classname = "rhs_bmp1p_msv"; level = ANTI_IFV; cost = 86; crew = 3; };

class BMP1D_VDV_D: Vehicle { classname = "rhs_bmp1d_vdv";  level = ANTI_IFV; cost = 72; textureData[] = {"rhs_sand",1}; };
class BMP1D_VDV_W: Vehicle { classname = "rhs_bmp1d_vdv";  level = ANTI_IFV; cost = 72; };

class BMP1_VMF_D: Vehicle  { classname = "rhs_bmp1_vmf";  level = ANTI_IFV; cost = 72; textureData[] = {"rhs_sand",1}; };
class BMP1_VMF_W: Vehicle  { classname = "rhs_bmp1_vmf";  level = ANTI_IFV; cost = 72; };
class BMP1P_VMF_D: Vehicle { classname = "rhs_bmp1p_vmf"; level = ANTI_IFV; cost = 86; crew = 3; textureData[] = {"rhs_sand",1}; };
class BMP1P_VMF_W: Vehicle { classname = "rhs_bmp1p_vmf"; level = ANTI_IFV; cost = 86; crew = 3; };

class BMP1D_G: Vehicle { classname = "UK3CB_I_G_BMP1"; level = ANTI_IFV; cost = 72; };

class BMP1_UN: Vehicle	{ classname = "UK3CB_UN_I_BMP1";	level = ANTI_IFV; cost = 72; };

class BMP1D_CDF: Vehicle	{ classname = "rhsgref_cdf_b_bmp1d";	level = ANTI_IFV; cost = 72; };
class BMP1_CHE: Vehicle	{ classname = "rhsgref_ins_bmp1";	level = ANTI_IFV; cost = 72; };

class BMP1_INS_D: Vehicle	{ classname = "UK3CB_TKM_I_BMP1";	level = ANTI_IFV; cost = 72; };
class BMP1_TKA: Vehicle	{ classname = "UK3CB_TKA_O_BMP1";	level = ANTI_IFV; cost = 72; };


/**
 * BMP-2
 */
class BMP2E_MSV_D: Vehicle { classname = "rhs_bmp2e_msv"; level = ANTI_IFV; cost = 82; textureData[] = {"rhs_sand",1}; };
class BMP2E_MSV_W: Vehicle { classname = "rhs_bmp2e_msv"; level = ANTI_IFV; cost = 82; };
class BMP2_MSV_D: Vehicle  { classname = "rhs_bmp2_msv";  level = ANTI_IFV; cost = 86; textureData[] = {"rhs_sand",1}; };
class BMP2_MSV_W: Vehicle  { classname = "rhs_bmp2_msv";  level = ANTI_IFV; cost = 86; };

class BMP2D_VDV_D: Vehicle  { classname = "rhs_bmp2d_vdv"; level = ANTI_IFV; cost = 104; textureData[] = {"rhs_sand",1}; };
class BMP2D_VDV_W: Vehicle  { classname = "rhs_bmp2d_vdv"; level = ANTI_IFV; cost = 104; };

class BMP2E_VMF_D: Vehicle { classname = "rhs_bmp2e_vmf"; level = ANTI_IFV; cost = 82; textureData[] = {"rhs_sand",1}; };
class BMP2E_VMF_W: Vehicle { classname = "rhs_bmp2e_vmf"; level = ANTI_IFV; cost = 82; };
class BMP2_VMF_D: Vehicle  { classname = "rhs_bmp2_vmf";  level = ANTI_IFV; cost = 86; textureData[] = {"rhs_sand",1}; };
class BMP2_VMF_W: Vehicle  { classname = "rhs_bmp2_vmf";  level = ANTI_IFV; cost = 86; };

class BMP2E_CDF: Vehicle { classname = "rhsgref_cdf_b_bmp2e"; level = ANTI_IFV; cost = 82; };
class BMP2_CDF: Vehicle  { classname = "rhsgref_cdf_b_bmp2";  level = ANTI_IFV; cost = 86; };
class BMP2D_CDF: Vehicle { classname = "rhsgref_cdf_b_bmp2d"; level = ANTI_IFV; cost = 104; };
class BMP2E_CHE: Vehicle { classname = "rhsgref_ins_bmp2e";	  level = ANTI_IFV; cost = 82; };

class BMP2_UN: Vehicle	{ classname = "UK3CB_UN_I_BMP2";  level = ANTI_IFV; cost = 86; };

class BMP2_TKA: Vehicle	{ classname = "UK3CB_TKA_O_BMP2"; level = ANTI_IFV; cost = 86; };

class BMP2_URS: Vehicle	{ classname = "UK3CB_O_G_BMP2";	level = ANTI_IFV; cost = 86; textureData[] = {"Olive",1}; };


/**
 * BMP-3
 */
class BMP3_MSV_D: Vehicle    { classname = "rhs_bmp3_late_msv"; level = ANTI_IFV; cost = 104; textureData[] = {"rhs_sand",1}; };
class BMP3_MSV_W: Vehicle    { classname = "rhs_bmp3_late_msv"; level = ANTI_IFV; cost = 104; };
class BMP3VK_MSV_D: Vehicle  { classname = "rhs_bmp3m_msv"; 	level = ANTI_IFV; cost = 124; textureData[] = {"rhs_sand",1}; };
class BMP3VK_MSV_W: Vehicle  { classname = "rhs_bmp3m_msv"; 	level = ANTI_IFV; cost = 124; };
class BMP3VKA_MSV_D: Vehicle { classname = "rhs_bmp3mera_msv";  level = ANTI_IFV; cost = 148; textureData[] = {"rhs_sand",1}; };
class BMP3VKA_MSV_W: Vehicle { classname = "rhs_bmp3mera_msv";  level = ANTI_IFV; cost = 148; };


/**
 * BMD-1
 */
class BMD1_VDV_D: Vehicle  { classname = "rhs_bmd1";  level = ANTI_IFV; cost = 72; crew = 3; textureData[] = {"Camo1",1}; };
class BMD1_VDV_W: Vehicle  { classname = "rhs_bmd1";  level = ANTI_IFV; cost = 72; crew = 3; };
class BMD1P_VDV_D: Vehicle { classname = "rhs_bmd1p"; level = ANTI_IFV; cost = 86; crew = 3; textureData[] = {"Camo1",1}; };
class BMD1P_VDV_W: Vehicle { classname = "rhs_bmd1p"; level = ANTI_IFV; cost = 86; crew = 3; };
class BMD1R_VDV_D: Vehicle { classname = "rhs_bmd1r"; level = ANTI_IFV; cost = 104; crew = 3; textureData[] = {"Camo1",1}; };
class BMD1R_VDV_W: Vehicle { classname = "rhs_bmd1r"; level = ANTI_IFV; cost = 104; crew = 3; };


/**
 * BMD-2
 */
class BMD2_VDV_D: Vehicle  { classname = "rhs_bmd2";  level = ANTI_IFV; cost = 86; crew = 3; textureData[] = {"Camo1",1}; };
class BMD2_VDV_W: Vehicle  { classname = "rhs_bmd2";  level = ANTI_IFV; cost = 86; crew = 3; };
class BMD2M_VDV_D: Vehicle { classname = "rhs_bmd2m"; level = ANTI_IFV; cost = 104; crew = 3; textureData[] = {"Camo1",1}; };
class BMD2M_VDV_W: Vehicle { classname = "rhs_bmd2m"; level = ANTI_IFV; cost = 104; crew = 3; };

class BMD2_CDF: Vehicle		{ classname = "rhsgref_cdf_b_bmd2"; level = ANTI_IFV; cost = 86; crew = 3; };
class BMD2M_CDF: Vehicle	{ classname = "rhs_bmd2m";			level = ANTI_IFV; cost = 104; crew = 3; textureData[] = {"CDF",1}; };


/**
 * BMD-4
 */
class BMD4_VDV: Vehicle		{ classname = "rhs_bmd4_vdv";	level = ANTI_IFV; cost = 104; crew = 3; };
class BMD4M_VDV: Vehicle	{ classname = "rhs_bmd4m_vdv";	level = ANTI_IFV; cost = 114; crew = 3; };
class BMD4MA_VDV: Vehicle	{ classname = "rhs_bmd4ma_vdv";	level = ANTI_IFV; cost = 136; crew = 3; };