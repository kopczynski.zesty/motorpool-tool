/**
 * M142
 */
class M142_D: Vehicle	{ classname = "rhsusf_M142_usarmy_D";	level = ANTI_MBT; cost = 128; };
class M142_W: Vehicle	{ classname = "rhsusf_M142_usarmy_WD";	level = ANTI_MBT; cost = 128; };


/**
 * BM-21
 */
class BM21_MSV: Vehicle	{ classname = "RHS_BM21_MSV_01";	level = ANTI_MBT; cost = 102; };
class BM21_VDV: Vehicle	{ classname = "RHS_BM21_VDV_01";	level = ANTI_MBT; cost = 102; };
class BM21_VMF: Vehicle	{ classname = "RHS_BM21_VMF_01";	level = ANTI_MBT; cost = 102; };

class BM21_CDF: Vehicle	{ classname = "rhsgref_cdf_b_reg_BM21";	level = ANTI_MBT; cost = 102; };

class BM21_CHE: Vehicle	{ classname = "rhsgref_ins_BM21";	level = ANTI_MBT; cost = 102; };

class BM21_TKA: Vehicle	{ classname = "UK3CB_TKA_I_BM21";	level = ANTI_MBT; cost = 102; };

class BM21_URS: Vehicle	{ classname = "UK3CB_TKA_I_BM21";	level = ANTI_MBT; cost = 102; textureData[] = {"SOV2",1}; };


/**
 * Hilux Artillery
 */
class Hilux_Artillery_INS: Vehicle	{ classname = "UK3CB_I_G_Hilux_Rocket_Arty";	level = ANTI_MBT; cost = 56; };
class Hilux_Artillery_BGM: Vehicle	{ classname = "UK3CB_O_G_Hilux_Rocket_Arty";	level = ANTI_MBT; cost = 56; };