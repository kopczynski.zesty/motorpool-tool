/**
 * M113
 */
class M113A3_D: Vehicle    		{ classname = "rhsusf_m113d_usarmy_unarmed"; level = UTILITY; cost = 28; crew = 1; };
class M113A3_W: Vehicle    		{ classname = "rhsusf_m113_usarmy_unarmed";  level = UTILITY; cost = 28; crew = 1; };
class M113A3_Medical_D: Vehicle { classname = "rhsusf_m113d_usarmy_medical"; level = UTILITY; cost = 22; crew = 1; };
class M113A3_Medical_W: Vehicle { classname = "rhsusf_m113_usarmy_medical";  level = UTILITY; cost = 22; crew = 1; };

class M113A3E_D: Vehicle    	 { classname = "rhsusf_m113d_usarmy_unarmed"; level = UTILITY; cost = 28; crew = 1; animationData[] = {"IFF_Panels_Hide",1}; };
class M113A3E_W: Vehicle    	 { classname = "rhsusf_m113_usarmy_unarmed";  level = UTILITY; cost = 28; crew = 1; animationData[] = {"IFF_Panels_Hide",1}; };
class M113A3E_Medical_D: Vehicle { classname = "rhsusf_m113d_usarmy_medical"; level = UTILITY; cost = 22; crew = 1; animationData[] = {"IFF_Panels_Hide",1}; };
class M113A3E_Medical_W: Vehicle { classname = "rhsusf_m113_usarmy_medical";  level = UTILITY; cost = 22; crew = 1; animationData[] = {"IFF_Panels_Hide",1}; };

class M113A3_CW: Vehicle		 { classname = "UK3CB_CW_US_B_EARLY_M113_unarmed";	level = UTILITY; cost = 28; crew = 1; };
class M113A3_Medical_CW: Vehicle { classname = "UK3CB_CW_US_B_EARLY_M113_AMB";		level = UTILITY; cost = 22; crew = 1; };

class M113A3_TAN: Vehicle { classname = "rhsgref_hidf_m113a3_unarmed";	level = UTILITY; cost = 28; crew = 1; };


/**
 * Fuchs 1A4
 */
class Fuchs1A4_Medical_D: Vehicle { classname = "Redd_Tank_Fuchs_1A4_San_Tropentarn"; level = UTILITY; cost = 52; crew = 1; };
class Fuchs1A4_Medical_W: Vehicle { classname = "Redd_Tank_Fuchs_1A4_San_Flecktarn";  level = UTILITY; cost = 52; crew = 1; };
class Fuchs1A4_Medical_A: Vehicle { classname = "Redd_Tank_Fuchs_1A4_San_Wintertarn"; level = UTILITY; cost = 52; crew = 1; };


/**
 * BTR-40
 */
class BTR40_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_EARLY_BTR40"; level = UTILITY; cost = 24; };

class BTR40_CDF: Vehicle { classname = "UK3CB_I_BTR40_CDF"; level = UTILITY; cost = 24; };
class BTR40_UN: Vehicle  { classname = "UK3CB_UN_I_BTR40";  level = UTILITY; cost = 24; };

class BTR40_INS_D: Vehicle { classname = "UK3CB_TKM_I_BTR40"; level = UTILITY; cost = 24; };

class BTR40_TKA: Vehicle { classname = "UK3CB_TKA_B_BTR40";	  level = UTILITY; cost = 24; };
