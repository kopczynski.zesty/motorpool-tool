/**
 * M113s
 */
class M113A3_Mk19_D: Vehicle { classname = "rhsusf_m113d_usarmy_MK19"; level = ANTI_APC; cost = 56; };
class M113A3_Mk19_W: Vehicle { classname = "rhsusf_m113_usarmy_MK19";  level = ANTI_APC; cost = 56; };

class M113A3E_Mk19_D: Vehicle { classname = "rhsusf_m113_usarmy_MK19_90"; level = ANTI_APC; cost = 56; animationData[] = {"IFF_Panels_Hide",1}; textureData[] = {"Desert",1}; };
class M113A3E_Mk19_W: Vehicle { classname = "rhsusf_m113_usarmy_MK19_90"; level = ANTI_APC; cost = 56; animationData[] = {"IFF_Panels_Hide",1}; };

class M113A3E_Mk19_CW: Vehicle { classname = "UK3CB_CW_US_B_EARLY_M113_MK19"; level = ANTI_APC; cost = 56; };

class M113A3E_Mk19_TAN: Vehicle { classname = "rhsgref_hidf_m113a3_mk19"; level = ANTI_APC; cost = 56; };


/**
 * M1126 Stryker
 */
class M1126_CROWS_MK19_D: Vehicle { classname = "rhsusf_stryker_m1126_mk19_d";  name = "M1126 (CROWS Mk19)"; level = ANTI_APC; cost = 96; crew = 2; };
class M1126_CROWS_MK19_W: Vehicle { classname = "rhsusf_stryker_m1126_mk19_wd"; name = "M1126 (CROWS Mk19)"; level = ANTI_APC; cost = 96; crew = 2; };


/**
 * AAVP-7A1 APC
 */
class AAVP71A_D: Vehicle	{ classname = "UK3CB_B_AAV_US_DES"; level = ANTI_APC; cost = 70; crew = 2; };
class AAVP71A_W: Vehicle	{ classname = "UK3CB_B_AAV_US_WDL"; level = ANTI_APC; cost = 70; crew = 2; };
class AAVP71A_TAN: Vehicle	{ classname = "UK3CB_B_AAV_HIDF"; 	level = ANTI_APC; cost = 70; crew = 2; };