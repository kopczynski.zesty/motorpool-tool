/**
 * Fuchs 1A4 (Bundeswehr)
 */
class Fuchs1A4_HAT_D: Vehicle { classname = "Redd_Tank_Fuchs_1A4_Jg_Milan_Tropentarn"; level = ANTI_APC; cost = 78; crew = 2; };
class Fuchs1A4_HAT_W: Vehicle { classname = "Redd_Tank_Fuchs_1A4_Jg_Milan_Flecktarn";  level = ANTI_APC; cost = 78; crew = 2; };
class Fuchs1A4_HAT_A: Vehicle { classname = "Redd_Tank_Fuchs_1A4_Jg_Milan_Wintertarn"; level = ANTI_APC; cost = 78; crew = 2; };


/**
 * M1134 Stryker
 */
class M1134_ATGM_D: Vehicle { classname = "rhsusf_stryker_m1134_d";  name = "M1134 (ATGM)"; level = ANTI_APC; cost = 70; crew = 2; };
class M1134_ATGM_W: Vehicle { classname = "rhsusf_stryker_m1134_wd"; name = "M1134 (ATGM)"; level = ANTI_APC; cost = 70; crew = 2; };
