class APC: Category {
    name = "Armored Personnel Carriers";
    type = GROUND;

	#include <unarmed.hpp>
	#include <armed_hmg.hpp>
	#include <armed_hat.hpp>
	#include <armed_gl.hpp>
};