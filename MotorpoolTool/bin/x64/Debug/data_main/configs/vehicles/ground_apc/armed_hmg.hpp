/**
 * M113s
 */
class M113A3_M240_D: Vehicle { classname = "rhsusf_m113d_usarmy_M240"; level = ANTI_CAR; cost = 34; };
class M113A3_M240_W: Vehicle { classname = "rhsusf_m113_usarmy_M240";  level = ANTI_CAR; cost = 34; };
class M113A3_M2_D: Vehicle   { classname = "rhsusf_m113d_usarmy";      level = ANTI_APC; cost = 40; };
class M113A3_M2_W: Vehicle   { classname = "rhsusf_m113_usarmy";       level = ANTI_APC; cost = 40; };
class M113A3_Ammo_D: Vehicle { classname = "rhsusf_m113d_usarmy_supply";  level = ANTI_APC; cost = 44; crew = 1; };
class M113A3_Ammo_W: Vehicle { classname = "rhsusf_m113_usarmy_supply";   level = ANTI_APC; cost = 44; crew = 1; textureData[] = {"standard",1}; };

class M113A3E_M240_D: Vehicle { classname = "rhsusf_m113d_usarmy_M240"; level = ANTI_CAR; cost = 34; animationData[] = {"IFF_Panels_Hide",1}; };
class M113A3E_M240_W: Vehicle { classname = "rhsusf_m113_usarmy_M240";  level = ANTI_CAR; cost = 34; animationData[] = {"IFF_Panels_Hide",1}; };
class M113A3E_M2_D: Vehicle   { classname = "rhsusf_m113_usarmy_M2_90"; level = ANTI_APC; cost = 40; animationData[] = {"IFF_Panels_Hide",1}; textureData[] = {"Desert",1}; };
class M113A3E_M2_W: Vehicle   { classname = "rhsusf_m113_usarmy_M2_90"; level = ANTI_APC; cost = 40; animationData[] = {"IFF_Panels_Hide",1}; };
class M113A3E_Ammo_D: Vehicle { classname = "rhsusf_m113d_usarmy_supply";  level = ANTI_APC; cost = 44; crew = 1; animationData[] = {"IFF_Panels_Hide",1}; };
class M113A3E_Ammo_W: Vehicle { classname = "rhsusf_m113_usarmy_supply";   level = ANTI_APC; cost = 44; crew = 1; animationData[] = {"IFF_Panels_Hide",1}; textureData[] = {"standard",1}; };

class M113A3_M240_CW: Vehicle	{ classname = "UK3CB_CW_US_B_EARLY_M113_M240";	level = ANTI_CAR; cost = 34; };
class M113A3_M2_CW: Vehicle		{ classname = "UK3CB_CW_US_B_EARLY_M113_M2";	level = ANTI_APC; cost = 40; };
class M113A3_Ammo_CW: Vehicle	 { classname = "UK3CB_CW_US_B_EARLY_M113_supply";	level = ANTI_APC; cost = 44; crew = 1; };

class M113A3_M2_TAN: Vehicle	{ classname = "rhsgref_hidf_m113a3_m2";	level = ANTI_APC; cost = 40; };


/**
 * M1126 Stryker
 */
class M1126_CROWS_M2_D: Vehicle { classname = "rhsusf_stryker_m1126_m2_d";  name = "M1126 (CROWS M2)"; level = ANTI_APC; cost = 80; crew = 2; };
class M1126_CROWS_M2_W: Vehicle { classname = "rhsusf_stryker_m1126_m2_wd"; name = "M1126 (CROWS M2)"; level = ANTI_APC; cost = 80; crew = 2; };

class M1132_CROWS_M2_D: Vehicle { classname = "rhsusf_stryker_m1132_m2_d";  name = "M1132 (CROWS M2/Plow)"; level = ANTI_APC; cost = 80; crew = 2; };
class M1132_CROWS_M2_W: Vehicle { classname = "rhsusf_stryker_m1132_m2_wd"; name = "M1132 (CROWS M2/Plow)"; level = ANTI_APC; cost = 80; crew = 2; };


/**
 * Fuchs 1A4
 */
class Fuchs1A4_D: Vehicle { classname = "Redd_Tank_Fuchs_1A4_Jg_Tropentarn"; level = ANTI_CAR; cost = 62; crew = 2; };
class Fuchs1A4_W: Vehicle { classname = "Redd_Tank_Fuchs_1A4_Jg_Flecktarn";  level = ANTI_CAR; cost = 62; crew = 2; };
class Fuchs1A4_A: Vehicle { classname = "Redd_Tank_Fuchs_1A4_Jg_Wintertarn"; level = ANTI_CAR; cost = 62; crew = 2; };


/**
 * FV432
 */
class FV432_GPMG_D: Vehicle { classname = "UK3CB_BAF_FV432_Mk3_GPMG_Sand";   level = ANTI_APC; cost = 34; };
class FV432_GPMG_W: Vehicle { classname = "UK3CB_BAF_FV432_Mk3_GPMG_Green";  level = ANTI_APC; cost = 34; };

class FV432_RWS_D: Vehicle { classname = "UK3CB_BAF_FV432_Mk3_RWS_Sand";   level = ANTI_APC; cost = 50; };
class FV432_RWS_W: Vehicle { classname = "UK3CB_BAF_FV432_Mk3_RWS_Green";  level = ANTI_APC; cost = 50; };


/**
 * BTR-40
 */
class BTR40_DSHKM_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_EARLY_BTR40_MG"; level = ANTI_CAR; cost = 28; };

class BTR40_DSHKM_CDF: Vehicle { classname = "UK3CB_I_BTR40_MG_CDF"; level = ANTI_CAR; cost = 28; };
class BTR40_DSHKM_UN: Vehicle  { classname = "UK3CB_UN_I_BTR40_MG";  level = ANTI_CAR; cost = 28; };

class BTR40_DSHKM_INS_D: Vehicle { classname = "UK3CB_TKM_I_BTR40_MG"; level = ANTI_CAR; cost = 28; };

class BTR40_DSHKM_TKA: Vehicle { classname = "UK3CB_TKA_B_BTR40_MG";   level = ANTI_CAR; cost = 28; };


/**
 * BTR-60
 */
class BTR60_MSV: Vehicle { classname = "rhs_btr60_msv"; level = ANTI_APC; cost = 48; crew = 2; };
class BTR60_VDV: Vehicle { classname = "rhs_btr60_vdv"; level = ANTI_APC; cost = 48; crew = 2; };
class BTR60_VMF: Vehicle { classname = "rhs_btr60_vmf"; level = ANTI_APC; cost = 48; crew = 2; };

class BTR60_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_LATE_BTR60";	level = ANTI_APC; cost = 48; crew = 2; };

class BTR60_G: Vehicle     { classname = "UK3CB_I_G_BTR60";     level = ANTI_APC; cost = 48; crew = 2; };
class BTR60_CDF: Vehicle   { classname = "rhsgref_cdf_b_btr60";	level = ANTI_APC; cost = 48; crew = 2; };
class BTR60_CHE: Vehicle   { classname = "rhsgref_ins_btr60";	level = ANTI_APC; cost = 48; crew = 2; };

class BTR60_INS_D: Vehicle { classname = "UK3CB_TKM_I_BTR60";	level = ANTI_APC; cost = 48; crew = 2; };
class BTR60_TKA: Vehicle   { classname = "UK3CB_TKA_O_BTR60";	level = ANTI_APC; cost = 48; crew = 2; };


/**
 * BTR-70 (Russia)
 */
class BTR70_MSV_D: Vehicle  { classname = "rhs_btr70_msv"; level = ANTI_APC; cost = 52; crew = 2; textureData[] = {"rhs_sand",1}; };
class BTR70_MSV_W: Vehicle  { classname = "rhs_btr70_msv"; level = ANTI_APC; cost = 52; crew = 2; };

class BTR70_VDV_D: Vehicle  { classname = "rhs_btr70_vdv"; level = ANTI_APC; cost = 52; crew = 2; textureData[] = {"Camo1",1}; };
class BTR70_VDV_W: Vehicle  { classname = "rhs_btr70_vdv"; level = ANTI_APC; cost = 52; crew = 2; };

class BTR70_VMF_D: Vehicle  { classname = "rhs_btr70_vmf"; level = ANTI_APC; cost = 52; crew = 2; textureData[] = {"Camo1",1}; };
class BTR70_VMF_W: Vehicle  { classname = "rhs_btr70_vmf"; level = ANTI_APC; cost = 52; crew = 2; };

class BTR70_SOV: Vehicle  { classname = "UK3CB_CW_SOV_O_LATE_BTR70"; level = ANTI_APC; cost = 52; crew = 2; };

class BTR70_UN: Vehicle  { classname = "rhsgref_un_btr70";  level = ANTI_APC; cost = 52; crew = 2; };

class BTR70_CDF: Vehicle	{ classname = "rhsgref_cdf_b_btr70"; level = ANTI_APC; cost = 52; crew = 2; };
class BTR70_CHE: Vehicle	{ classname = "rhsgref_ins_btr70";	 level = ANTI_APC; cost = 52; crew = 2; };
class BTR70_CNA: Vehicle	{ classname = "rhsgref_nat_btr70";	 level = ANTI_APC; cost = 52; crew = 2; };

class BTR70_TKA: Vehicle  { classname = "UK3CB_TKA_O_BTR70";	 level = ANTI_APC; cost = 52; crew = 2; }; 


/**
 * BTR-80 (Russia)
 */
class BTR80_MSV_D: Vehicle  { classname = "rhs_btr80_msv";  level = ANTI_APC; cost = 62; crew = 2; textureData[] = {"rhs_sand",1}; };
class BTR80_MSV_W: Vehicle  { classname = "rhs_btr80_msv";  level = ANTI_APC; cost = 62; crew = 2; };
class BTR80A_MSV_D: Vehicle { classname = "rhs_btr80a_msv"; level = ANTI_APC; cost = 80; crew = 2; textureData[] = {"rhs_sand",1}; };
class BTR80A_MSV_W: Vehicle { classname = "rhs_btr80a_msv"; level = ANTI_APC; cost = 80; crew = 2; };

class BTR80_VDV_D: Vehicle  { classname = "rhs_btr80_vdv";  level = ANTI_APC; cost = 62; crew = 2; textureData[] = {"tricolourhard",1}; };
class BTR80_VDV_W: Vehicle  { classname = "rhs_btr80_vdv";  level = ANTI_APC; cost = 62; crew = 2; };
class BTR80A_VDV_D: Vehicle { classname = "rhs_btr80a_vdv"; level = ANTI_APC; cost = 80; crew = 2; textureData[] = {"tricolourhard",1}; };
class BTR80A_VDV_W: Vehicle { classname = "rhs_btr80a_vdv"; level = ANTI_APC; cost = 80; crew = 2; };

class BTR80_VMF_D: Vehicle  { classname = "rhs_btr80_vmf";  level = ANTI_APC; cost = 62; crew = 2; textureData[] = {"tricolourhard",1}; };
class BTR80_VMF_W: Vehicle  { classname = "rhs_btr80_vmf";  level = ANTI_APC; cost = 62; crew = 2; };
class BTR80A_VMF_D: Vehicle { classname = "rhs_btr80a_vmf"; level = ANTI_APC; cost = 80; crew = 2; textureData[] = {"tricolourhard",1}; };
class BTR80A_VMF_W: Vehicle { classname = "rhs_btr80a_vmf"; level = ANTI_APC; cost = 80; crew = 2; };

class BTR80_SOV: Vehicle  { classname = "UK3CB_CW_SOV_O_LATE_BTR80";  level = ANTI_APC; cost = 62; crew = 2; };
class BTR80A_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_LATE_BTR80a"; level = ANTI_APC; cost = 80; crew = 2; };

class BTR80_TKA: Vehicle  { classname = "UK3CB_TKA_O_BTR80";  level = ANTI_APC; cost = 62; crew = 2; };
class BTR80A_TKA: Vehicle { classname = "UK3CB_TKA_O_BTR80a"; level = ANTI_APC; cost = 80; crew = 2; };


/**
 * MT-LB 
 */
class MTLB_INS_D: Vehicle	{ classname = "UK3CB_TKM_I_MTLB_PKT";	level = ANTI_CAR; cost = 34; crew = 2; };
class MTLB_INS_W: Vehicle	{ classname = "UK3CB_I_G_MTLB_PKT";		level = ANTI_CAR; cost = 34; crew = 2; };
class MTLB_TKA: Vehicle		{ classname = "UK3CB_TKA_B_MTLB_PKT";	level = ANTI_CAR; cost = 34; crew = 2; };
