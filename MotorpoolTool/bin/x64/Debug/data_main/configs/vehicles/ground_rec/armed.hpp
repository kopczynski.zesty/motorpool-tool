/**
 * M1117 ASV
 */
class M1117_D: Vehicle    { classname = "rhsusf_M1117_D"; level = ANTI_APC; cost = 50; crew = 2; };
class M1117_W: Vehicle    { classname = "rhsusf_M1117_W"; level = ANTI_APC; cost = 50; crew = 2; };


/**
 * M1151 LRAS3 Humvee
 */
class M1151_M2_LRAS3_D: Vehicle { classname = "rhsusf_m1151_m2_lras3_v1_usarmy_d";  level = ANTI_CAR; cost = 40; };
class M1151_M2_LRAS3_W: Vehicle { classname = "rhsusf_m1151_m2_lras3_v1_usarmy_wd"; level = ANTI_CAR; cost = 40; };


/**
 * M1127 LRAS3 Stryker
 */
class M1127_M2_D: Vehicle { classname = "rhsusf_stryker_m1127_m2_d";  name = "M1127 (M2/LRAS3)"; level = ANTI_APC; cost = 64; crew = 2; };
class M1127_M2_W: Vehicle { classname = "rhsusf_stryker_m1127_m2_wd"; name = "M1127 (M2/LRAS3)"; level = ANTI_APC; cost = 64; crew = 2; };


/**
 * Wiesel
 */
class Wiesel1A2_TOW_D: Vehicle { classname = "Redd_Tank_Wiesel_1A2_TOW_Tropentarn"; level = ANTI_APC; cost = 36; crew = 2; passengers = 1; };
class Wiesel1A2_TOW_W: Vehicle { classname = "Redd_Tank_Wiesel_1A2_TOW_Flecktarn";  level = ANTI_APC; cost = 36; crew = 2; passengers = 1; };
class Wiesel1A2_TOW_A: Vehicle { classname = "Redd_Tank_Wiesel_1A2_TOW_Wintertarn"; level = ANTI_APC; cost = 36; crew = 2; passengers = 1; };

class Wiesel1A2_Mk20_D: Vehicle { classname = "Redd_Tank_Wiesel_1A4_MK20_Tropentarn"; level = ANTI_APC; cost = 44; crew = 2; };
class Wiesel1A2_Mk20_W: Vehicle { classname = "Redd_Tank_Wiesel_1A4_MK20_Flecktarn";  level = ANTI_APC; cost = 44; crew = 2; };
class Wiesel1A2_Mk20_A: Vehicle { classname = "Redd_Tank_Wiesel_1A4_MK20_Wintertarn"; level = ANTI_APC; cost = 44; crew = 2; };


/**
 * SpPz 2A2 Luchs
 */
class Luchs2A2_D: Vehicle { classname = "rnt_sppz_2a2_luchs_tropentarn"; level = ANTI_APC; cost = 60; crew = 3; };
class Luchs2A2_W: Vehicle { classname = "rnt_sppz_2a2_luchs_flecktarn";  level = ANTI_APC; cost = 60; crew = 3; };
class Luchs2A2_A: Vehicle { classname = "rnt_sppz_2a2_luchs_wintertarn"; level = ANTI_APC; cost = 60; crew = 3; };


/**
 * BRDM-2
 */
class BRDM2_GPMG_MSV_D: Vehicle { classname = "rhsgref_BRDM2_HQ_msv";   level = ANTI_CAR; cost = 44; crew = 2; textureData[] = {"3tone",1}; };
class BRDM2_GPMG_MSV_W: Vehicle { classname = "rhsgref_BRDM2_HQ_msv";   level = ANTI_CAR; cost = 44; };
class BRDM2_HMG_MSV_D: Vehicle  { classname = "rhsgref_BRDM2_msv"; 	    level = ANTI_CAR; cost = 50; crew = 2; textureData[] = {"3tone",1}; };
class BRDM2_HMG_MSV_W: Vehicle  { classname = "rhsgref_BRDM2_msv"; 	    level = ANTI_CAR; cost = 50; crew = 2; };
class BRDM2_ATGM_MSV_D: Vehicle { classname = "rhsgref_BRDM2_ATGM_msv"; level = ANTI_APC; cost = 54; textureData[] = {"khaki",1}; };
class BRDM2_ATGM_MSV_W: Vehicle { classname = "rhsgref_BRDM2_ATGM_msv"; level = ANTI_APC; cost = 54; };

class BRDM2_GPMG_VDV_D: Vehicle { classname = "rhsgref_BRDM2_HQ_vdv";   level = ANTI_CAR; cost = 44; textureData[] = {"3tone",1}; };
class BRDM2_GPMG_VDV_W: Vehicle { classname = "rhsgref_BRDM2_HQ_vdv";   level = ANTI_CAR; cost = 44; };
class BRDM2_HMG_VDV_D: Vehicle  { classname = "rhsgref_BRDM2_vdv"; 	    level = ANTI_CAR; cost = 50; crew = 2; textureData[] = {"3tone",1}; };
class BRDM2_HMG_VDV_W: Vehicle  { classname = "rhsgref_BRDM2_vdv"; 	    level = ANTI_CAR; cost = 50; crew = 2; };
class BRDM2_ATGM_VDV_D: Vehicle { classname = "rhsgref_BRDM2_ATGM_vdv"; level = ANTI_APC; cost = 54; textureData[] = {"khaki",1}; };
class BRDM2_ATGM_VDV_W: Vehicle { classname = "rhsgref_BRDM2_ATGM_vdv"; level = ANTI_APC; cost = 54; };

class BRDM2_GPMG_VMF_D: Vehicle { classname = "rhsgref_BRDM2_HQ_vmf";   level = ANTI_CAR; cost = 44; textureData[] = {"3tone",1}; };
class BRDM2_GPMG_VMF_W: Vehicle { classname = "rhsgref_BRDM2_HQ_vmf";   level = ANTI_CAR; cost = 44; };
class BRDM2_HMG_VMF_D: Vehicle  { classname = "rhsgref_BRDM2_vmf"; 	    level = ANTI_CAR; cost = 50; crew = 2; textureData[] = {"3tone",1}; };
class BRDM2_HMG_VMF_W: Vehicle  { classname = "rhsgref_BRDM2_vmf"; 	    level = ANTI_CAR; cost = 50; crew = 2; };
class BRDM2_ATGM_VMF_D: Vehicle { classname = "rhsgref_BRDM2_ATGM_vmf"; level = ANTI_APC; cost = 54; textureData[] = {"khaki",1}; };
class BRDM2_ATGM_VMF_W: Vehicle { classname = "rhsgref_BRDM2_ATGM_vmf"; level = ANTI_APC; cost = 54; };

class BRDM2_GPMG_G: Vehicle { classname = "rhsgref_BRDM2_HQ";   level = ANTI_CAR; cost = 44; textureData[] = {"olive",1}; };
class BRDM2_GPMG_K: Vehicle { classname = "rhsgref_BRDM2_HQ";   level = ANTI_CAR; cost = 44; textureData[] = {"khaki",1}; };
class BRDM2_HMG_G: Vehicle  { classname = "rhsgref_BRDM2"; 	    level = ANTI_CAR; cost = 50; crew = 2; textureData[] = {"olive",1}; };
class BRDM2_HMG_K: Vehicle  { classname = "rhsgref_BRDM2"; 	    level = ANTI_CAR; cost = 50; crew = 2; textureData[] = {"khaki",1}; };
class BRDM2_ATGM_G: Vehicle { classname = "rhsgref_BRDM2_ATGM"; level = ANTI_APC; cost = 54; textureData[] = {"olive",1}; };
class BRDM2_ATGM_K: Vehicle { classname = "rhsgref_BRDM2_ATGM"; level = ANTI_APC; cost = 54; textureData[] = {"khaki",1}; };

class BRDM2_GPMG_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_LATE_BRDM2_HQ"; 	level = ANTI_CAR; cost = 44; };
class BRDM2_HMG_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_LATE_BRDM2";			level = ANTI_CAR; cost = 50; crew = 2; };
class BRDM2_ATGM_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_LATE_BRDM2_ATGM";	level = ANTI_APC; cost = 54; };

class BRDM2_GPMG_CDF: Vehicle	{ classname = "rhsgref_BRDM2_HQ_b";		level = ANTI_CAR; cost = 44; };
class BRDM2_HMG_CDF: Vehicle	{ classname = "rhsgref_BRDM2_b";		level = ANTI_CAR; cost = 50; crew = 2; };
class BRDM2_ATGM_CDF: Vehicle	{ classname = "rhsgref_BRDM2_ATGM_b";	level = ANTI_APC; cost = 54; };

class BRDM2_GPMG_UN: Vehicle	{ classname = "UK3CB_UN_I_BRDM2_HQ";	level = ANTI_CAR; cost = 44; };
class BRDM2_HMG_UN: Vehicle	{ classname = "UK3CB_UN_I_BRDM2";			level = ANTI_CAR; cost = 50; crew = 2; };

class BRDM2_GPMG_CHE: Vehicle	{ classname = "rhsgref_BRDM2_HQ_ins";	level = ANTI_CAR; cost = 44; };
class BRDM2_HMG_CHE: Vehicle	{ classname = "rhsgref_BRDM2_ins";		level = ANTI_CAR; cost = 50; crew = 2; };
class BRDM2_ATGM_CHE: Vehicle	{ classname = "rhsgref_BRDM2_ATGM_ins";	level = ANTI_APC; cost = 54; };

class BRDM2_GPMG_INS_D: Vehicle	{ classname = "UK3CB_TKM_I_BRDM2_HQ";	level = ANTI_CAR; cost = 44; };
class BRDM2_HMG_INS_D: Vehicle	{ classname = "UK3CB_TKM_I_BRDM2";		level = ANTI_CAR; cost = 50; crew = 2; };
class BRDM2_ATGM_INS_D: Vehicle	{ classname = "UK3CB_TKM_I_BRDM2_ATGM";	level = ANTI_APC; cost = 54; };

class BRDM2_GPMG_TKA: Vehicle	{ classname = "UK3CB_TKA_B_BRDM2_HQ";	level = ANTI_CAR; cost = 44; };
class BRDM2_HMG_TKA: Vehicle	{ classname = "UK3CB_TKA_B_BRDM2";		level = ANTI_CAR; cost = 50; crew = 2; };
class BRDM2_ATGM_TKA: Vehicle	{ classname = "UK3CB_TKA_B_BRDM2_ATGM";	level = ANTI_APC; cost = 54; };

class BRDM2_GPMG_URS: Vehicle	{ classname = "UK3CB_CW_SOV_O_LATE_BRDM2_HQ";   level = ANTI_CAR; cost = 44; textureData[] = {"KHK",1}; };
class BRDM2_HMG_URS: Vehicle	{ classname = "UK3CB_CW_SOV_O_LATE_BRDM2";      level = ANTI_CAR; cost = 50; crew = 2; textureData[] = {"KHK",1}; };
class BRDM2_ATGM_URS: Vehicle	{ classname = "UK3CB_CW_SOV_O_LATE_BRDM2_ATGM"; level = ANTI_APC; cost = 54; textureData[] = {"KHK",1}; };


/**
 * BRM-1K
 */
class BRM1K_MSV_D: Vehicle { classname = "rhs_brm1k_msv"; level = ANTI_IFV; cost = 64; crew = 3; textureData[] = {"rhs_sand",1}; };
class BRM1K_MSV_W: Vehicle { classname = "rhs_brm1k_msv"; level = ANTI_IFV; cost = 64; crew = 3; };
class BRM1K_VDV_D: Vehicle { classname = "rhs_brm1k_vdv"; level = ANTI_IFV; cost = 64; crew = 3; textureData[] = {"rhs_sand",1}; };
class BRM1K_VDV_W: Vehicle { classname = "rhs_brm1k_vdv"; level = ANTI_IFV; cost = 64; crew = 3; };
class BRM1K_VMF_D: Vehicle { classname = "rhs_brm1k_vmf"; level = ANTI_IFV; cost = 64; crew = 3; textureData[] = {"rhs_sand",1}; };
class BRM1K_VMF_W: Vehicle { classname = "rhs_brm1k_vmf"; level = ANTI_IFV; cost = 64; crew = 3; };

class BRM1K_CDF: Vehicle { classname = "rhs_brm1k_msv"; level = ANTI_IFV; cost = 64; crew = 3; textureData[] = {"CDF",1}; };

class BRM1K_G: Vehicle { classname = "UK3CB_CW_SOV_O_LATE_BRM1K"; level = ANTI_IFV; cost = 64; crew = 3; };
class BRM1K_TKA: Vehicle { classname = "UK3CB_CW_SOV_O_LATE_BRM1K"; level = ANTI_IFV; cost = 64; crew = 3; textureData[] = {"TKA",1}; };
class BRM1K_UN: Vehicle  { classname = "UK3CB_CW_SOV_O_LATE_BRM1K"; level = ANTI_IFV; cost = 64; crew = 3; textureData[] = {"UN",1}; };
