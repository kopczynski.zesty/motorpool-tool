allowedVehicles[] = {
	"UAZ_SOV",
	"UAZ_O_SOV",
	"UAZ_DSHKM_SOV",
	"UAZ_AGS30_SOV",
	"UAZ_SPG9_SOV",
	"Ural4320_SOV",
	"Ural4320_O_SOV",
	"Ural4320_Recover_SOV",
	"Ural4320_Fuel_SOV",
	"Ural4320_Ammo_SOV",
	"Ural4320_Repair_SOV",
	"Ural4320_ZU23_SOV",
	"Ural4320_D30A_SOV",
	"BRDM2_SOV",
	"BRDM2_GPMG_SOV",
	"BRDM2_HMG_SOV",
	"BRDM2_ATGM_SOV",
	"BRM1K_SOV",
	"BMD1_VDV_W",
	"BMD1P_VDV_W",
	"MANHX60_CO_W",
	"Mi8_SOV",
	"Mi8_MG_SOV",
	"Mi8AMTSh_CAS_SOV",
	"Mi24V_SOV",
	"AN2_SOV",
	"Su25_CAS90",
	"MiG29S_Multi9090",
	"MiG29S_CAP90"
};

allowedVehiclesRecon[] = {
	"Yava250N_SOV"
};

allowedVehiclesSpecial[] = {
	"AssaultBoat"
};