allowedVehicles[] = {
	"UAZ_VDV_W",
	"UAZ_O_VDV_W",
	"GAZTigr_VDV_W",
	"GAZTigr_O_VDV_W",
	"GAZTigr_GMG_VDV_W",
	"GAZ66_VDV_W",
	"GAZ66_FB_VDV_W",
	"GAZ66_O_VDV_W",
	"GAZ66_O_FB_VDV_W",
	"GAZ66_Ammo_VDV_W",
	"GAZ66_ZU23_VDV_W",
    "GAZ66_D30A_VDV_W",
	"KamAZ5350_VDV",
	"KamAZ5350_FB_VDV",
	"KamAZ5350_O_VDV",
	"KamAZ5350_O_FB_VDV",
	"BRDM2_VDV_W",
	"BRDM2_GPMG_VDV_W",
	"BRDM2_HMG_VDV_W",
	"BRDM2_ATGM_VDV_W",
	"BMD2_VDV_W",
	"BMD2M_VDV_W",
	"BMD4_VDV",
	"BMD4M_VDV",
	"BMD4MA_VDV",
	"2S25_VDV",
	"MANHX60_CO_W",
	"KA60_VVSC",
	"Ka52_VVSC",
	"Mi8_VDV",
	"Mi8_MG_VDV",
	"Mi8MTV3_CAS_VDV",
	"Mi8MTV3_AT_VDV",
	"Mi8MTV3H_CAS_VDV",
	"Mi8MTV3H_AT_VDV",
	"Mi28_VVSC",
	"AN2_SOV",
	"Su25SM3_CAS10",
	"Su25SM3_SEAD10",
	"Su35_CAS10",
	"Su35_CAP10",
	"Su35_SEAD10",
	"Su57_CAS10",
	"Su57_CAP10",
	"Su57_SEAD10"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat",
	"RHIB_Small",
	"SDV"
};