allowedVehicles[] = {
	"UAZ_VDV_W",
	"UAZ_O_VDV_W",
	"GAZ66_VDV_W",
	"GAZ66_FB_VDV_W",
	"GAZ66_O_VDV_W",
	"GAZ66_O_FB_VDV_W",
	"GAZ66_Ammo_VDV_W",
	"GAZ66_ZU23_VDV_W",
    "GAZ66_D30A_VDV_W",
	"Ural4320_VDV",
	"Ural4320_FB_VDV",
	"Ural4320_O_VDV",
	"Ural4320_O_FB_VDV",
	"Ural4320_Fuel_VDV",
	"Ural4320_Repair_VDV",
	"Ural4320_ZU23_VDV",
	"BRDM2_VDV_W",
	"BRDM2_GPMG_VDV_W",
	"BRDM2_HMG_VDV_W",
	"BRDM2_ATGM_VDV_W",
	"BRM1K_VDV_W",
	"BMD1_VDV_W",
	"BMD1P_VDV_W",
	"BMD1R_VDV_W",
	"BMD2_VDV_W",
	"BMD2M_VDV_W",
	"2S25_VDV",
	"MANHX60_CO_W",
	"KA60_VVSC",
	"Ka52_VVSC",
	"Mi8_VDV",
	"Mi8_MG_VDV",
	"Mi8MTV3_CAS_VDV",
	"Mi8MTV3_AT_VDV",
	"Mi8MTV3H_CAS_VDV",
	"Mi8MTV3H_AT_VDV",
	"Mi24P_VDV",
	"Mi24V_VDV",
	"AN2_SOV",
	"Su25_CAS00",
	"MiG29S_Multi9090",
	"MiG29S_CAP90"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat",
	"RHIB_Small",
	"SDV"
};