// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Uniform.
switch ( _role ) do {
	default { [_unit, [ "UK3CB_CW_SOV_O_Early_U_VDV_CombatUniform_01_KLMK",
						"UK3CB_CW_SOV_O_Early_U_VDV_Crew_Uniform_01_KLMK"]] call bwi_armory_fnc_addRandomUniform; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit forceAddUniform "UK3CB_CW_SOV_O_Late_U_VDV_Spetsnaz_Uniform_Gorka_02_KLMK"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit forceAddUniform "UK3CB_CW_SOV_O_LATE_U_Crew_Uniform_01_TTSKO"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit forceAddUniform "U_B_Wetsuit"; };

	case "rwc_pil";
	case "rwt_pil": { _unit forceAddUniform "UK3CB_CW_SOV_O_LATE_U_H_Pilot_Uniform_01_TTSKO"; };
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "UK3CB_CW_SOV_O_LATE_U_J_Pilot_Uniform_01_OLI"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "UK3CB_CW_SOV_O_EARLY_H_SSh68"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addHeadgear "rhs_fieldcap_vsr"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		_unit addGoggles "G_Bandanna_oli";
		_unit addHeadgear "UK3CB_CW_SOV_O_EARLY_H_SSh68";
	};

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addGoggles "G_B_Diving"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "rhs_tsh4"; };

	case "rwc_pil": { _unit addHeadgear "rhs_zsh7a_mike_alt"; };
	case "rwt_pil": { _unit addHeadgear "rhs_zsh7a_mike"; };
	case "fwc_pil": { _unit addHeadgear "rhs_zsh7a"; };
	case "fwt_pil": { _unit addHeadgear "rhs_zsh7a_mike"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_V_CW_Chestrig"; };

	case "spf_ldr": { _unit addVest "rhs_6b5_officer"; };
	case "spf_lmg";
	case "spf_sap";
	case "spf_bre";
	case "spf_rif": { _unit addVest "rhs_6b5_rifleman"; };
	case "spf_lat": { _unit addVest "rhs_6b5_medic"; };
	case "spf_dmr": { _unit addVest "rhs_6b5_sniper"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addVest "V_RebreatherB"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_crew_Oli"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "rhsgref_6b23_khaki"; };
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "V_TacVest_brn"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "rhs_sidor"; };

	case "plt_ldr";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_amg";
	case "sqd_sap";
	case "mmg_gun": { _unit addBackpack "B_Kitbag_sgg";  };

	case "tac_fac": { _unit addBackpack "B_FieldPack_oli";  };

	case "sqd_lmg";
	case "lrc_lmg";
	case "spc_dem";
	case "eng_def";
	case "mmg_ldr";
	case "mat_ldr";
	case "aat_ldr": { _unit addBackpack "B_CarryAll_oli"; };

	case "how_ldr";
	case "how_gun": { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_sap";
	case "spf_bre";
	case "spf_rif";
	case "spf_lat": { _unit addBackpack "B_FieldPack_oli"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack";  };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};