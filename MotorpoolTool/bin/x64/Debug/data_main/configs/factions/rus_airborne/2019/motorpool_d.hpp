allowedVehicles[] = {
	"UAZ_VDV_D",
	"UAZ_O_VDV_D",
	"GAZTigr_VDV_D",
	"GAZTigr_O_VDV_D",
	"GAZTigr_GMG_VDV_D",
	"GAZ66_VDV_D",
	"GAZ66_FB_VDV_D",
	"GAZ66_O_VDV_D",
	"GAZ66_O_FB_VDV_D",
	"GAZ66_Ammo_VDV_D",
	"GAZ66_ZU23_VDV_D",
    "GAZ66_D30A_VDV_D",
	"KamAZ5350_VDV",
	"KamAZ5350_FB_VDV",
	"KamAZ5350_O_VDV",
	"KamAZ5350_O_FB_VDV",
	"BRDM2_VDV_D",
	"BRDM2_GPMG_VDV_D",
	"BRDM2_HMG_VDV_D",
	"BRDM2_ATGM_VDV_D",
	"BMD2_VDV_D",
	"BMD2M_VDV_D",
	"BMD4_VDV",
	"BMD4M_VDV",
	"BMD4MA_VDV",
	"2S25_VDV",
	"MANHX60_CO_D",
	"KA60_VVSC",
	"Ka52_VVSC",
	"Mi8_VDV",
	"Mi8_MG_VDV",
	"Mi8MTV3_CAS_VDV",
	"Mi8MTV3_AT_VDV",
	"Mi8MTV3H_CAS_VDV",
	"Mi8MTV3H_AT_VDV",
	"Mi28_VVSC",
	"AN2_SOV",
	"Su25SM3_CAS10",
	"Su25SM3_SEAD10",
	"Su35_CAS10",
	"Su35_CAP10",
	"Su35_SEAD10",
	"Su57_CAS10",
	"Su57_CAP10",
	"Su57_SEAD10"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat",
	"RHIB_Small",
	"SDV"
};