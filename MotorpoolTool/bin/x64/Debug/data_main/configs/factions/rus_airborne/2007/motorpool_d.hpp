allowedVehicles[] = {
	"UAZ_VDV_D",
	"UAZ_O_VDV_D",
	"GAZ66_VDV_D",
	"GAZ66_FB_VDV_D",
	"GAZ66_O_VDV_D",
	"GAZ66_O_FB_VDV_D",
	"GAZ66_Ammo_VDV_D",
	"GAZ66_ZU23_VDV_D",
    "GAZ66_D30A_VDV_D",
	"Ural4320_VDV",
	"Ural4320_FB_VDV",
	"Ural4320_O_VDV",
	"Ural4320_O_FB_VDV",
	"Ural4320_Fuel_VDV",
	"Ural4320_Repair_VDV",
	"Ural4320_ZU23_VDV",
	"BRDM2_VDV_D",
	"BRDM2_GPMG_VDV_D",
	"BRDM2_HMG_VDV_D",
	"BRDM2_ATGM_VDV_D",
	"BRM1K_VDV_D",
	"BMD1_VDV_D",
	"BMD1P_VDV_D",
	"BMD1R_VDV_D",
	"BMD2_VDV_D",
	"BMD2M_VDV_D",
	"2S25_VDV",
	"MANHX60_CO_D",
	"KA60_VVSC",
	"Ka52_VVSC",
	"Mi8_VDV",
	"Mi8_MG_VDV",
	"Mi8MTV3_CAS_VDV",
	"Mi8MTV3_AT_VDV",
	"Mi8MTV3H_CAS_VDV",
	"Mi8MTV3H_AT_VDV",
	"Mi24P_VDV",
	"Mi24V_VDV",
	"AN2_SOV",
	"Su25_CAS00",
	"MiG29S_Multi9090",
	"MiG29S_CAP90"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat",
	"RHIB_Small",
	"SDV"
};