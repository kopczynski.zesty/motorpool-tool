// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on special forces and scuba roles.
if ( (_role select [0,3]) in ["spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhs_uniform_cu_ocp_101st"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit forceAddUniform "rhs_uniform_cu_ocp_101st"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit forceAddUniform "rhs_uniform_cu_ocp"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit forceAddUniform "U_B_Wetsuit"; };

	case "rwc_pil";
	case "rwt_pil";
	case "uwc_pil": { _unit forceAddUniform "rhs_uniform_cu_ocp"; };
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "rhs_uniform_g3_rgr"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "rhsusf_ach_helmet_ESS_ocp"; };

	case "plt_ldr";
	case "log_sgt";
	case "sqd_ldr";
	case "sqd_ldg";
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit addHeadgear "rhsusf_ach_helmet_headset_ess_ocp"; };	
	
	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addHeadgear "rhsusf_ach_helmet_camo_ocp"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		_unit addGoggles "rhsusf_shemagh2_gogg_od";
		_unit addHeadgear "rhsusf_opscore_mc_cover_pelt_cam";
	};

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addGoggles "G_B_Diving"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "rhsusf_cvc_ess"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addHeadgear "rhsusf_hgu56p_visor"; };
	case "fwc_pil": { _unit addHeadgear "RHS_jetpilot_usaf"; };
	case "fwt_pil": { _unit addHeadgear "rhsusf_hgu56p"; };
	case "uwc_pil": { _unit addHeadgear "rhsusf_ach_helmet_ESS_ocp"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "rhsusf_iotv_ocp_Rifleman"; };

	case "plt_ldr";
	case "log_sgt": { _unit addVest "rhsusf_iotv_ocp_Squadleader"; };

	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit addVest "rhsusf_iotv_ocp_Teamleader"; };

	case "med_ldr";
	case "med_cpm": { _unit addVest "rhsusf_iotv_ocp_Medic"; };	

	case "spc_eod";
	case "spc_dem";
	case "eng_def";
	case "eng_mec": { _unit addVest "rhsusf_iotv_ocp_Repair"; };	

	case "sqd_ldr";
	case "sqd_ldg": { _unit addVest "rhsusf_iotv_ocp_Squadleader"; };
	case "sqd_lmg": { _unit addVest "rhsusf_iotv_ocp_SAW"; };
	case "sqd_gre": { _unit addVest "rhsusf_iotv_ocp_Grenadier"; };

	case "mmg_gun": { _unit addVest "rhsusf_iotv_ocp_SAW"; };

	case "spf_ldr": { _unit addVest "rhsusf_iotv_ocp_Squadleader"; };
	case "spf_lmg": { _unit addVest "rhsusf_iotv_ocp_SAW"; };
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit addVest "rhsusf_iotv_ocp_Rifleman"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addVest "V_RebreatherB"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "rhsusf_iotv_ocp_Rifleman"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "rhsusf_iotv_ocp_Rifleman"; };
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "V_TacVest_oli"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "rhsusf_assault_eagleaiii_ocp"; };

	case "plt_ldr": { _unit addBackpack "B_Kitbag_cbr"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "B_Kitbag_cbr"; };

	case "sqd_amg": { _unit addBackpack "B_Kitbag_cbr"; };

	case "spc_dem";
	case "eng_def": { _unit addBackpack "B_Carryall_cbr"; };

	case "mmg_ldr": { _unit addBackpack "B_Kitbag_cbr";  };
	case "mat_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "B_Carryall_cbr"; };

	case "how_ldr";
	case "how_gun": { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_uav": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_rif": { _unit addBackpack "rhsusf_assault_eagleaiii_ocp"; };
	case "spf_lat";
	case "spf_sap";
	case "spf_bre": { _unit addBackpack "B_Kitbag_cbr"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack";  };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};