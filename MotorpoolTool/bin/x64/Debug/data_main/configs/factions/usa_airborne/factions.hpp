class USA_Airborne: FactionGroup
{
    name = "US Army Airborne";
    side = BLUFOR;
	
	flag  = "\bwi_data_main\data\flag_usa.paa";
    icon  = "\bwi_data_main\data\icon_s_usa.paa";
    image = "\bwi_data_main\data\icon_l_usa.paa";

    #include <1986\faction.hpp>
    #include <2007\faction.hpp>
    #include <2019\faction.hpp>
};