allowedVehicles[] = {
	"M1025A2_D",
	"M1097A2_2D_D",
	"M1097A2_2DHT_D",
	"M1097A2_2DFT_D",
	"M1097A2_4D_D",
	"M1097A2_4DHT_D",
	"M1097A2_4DFT_D",
	"M1025A2_M2_D",
	"M1045A2_TOW_D",
	"M1025A2_Mk19_D",
	"M1078A1P2_D",
	"M1078A1P2_FB_D",
    "M1078A1P2_M119A2_D",
	"M113A3_D",
	"M113A3_Medical_D",
	"M113A3_Ammo_D",
	"M113A3_M240_D",
	"M113A3_M2_D",
	"M113A3_Mk19_D",
	"MANHX60_CO_D",
	"OH6M",
	"MH6M",
	"UH60M",
	"UH60M_MG",
	"UH60M_MEV",
	"UH60M_EWS",
	"CH47_D",
	"AH6M_CAS",
	"AH6M_CAS_AGM",
	"AH6M_CAS_GAU",
	"AH64D",
	"C130J",
	"C130J_Cargo",
	"A10C_CAS10",
	"F15C_CAP00",
	"F15E_CAS00",
	"F16C_CAS00",
	"F16C_CAP00",
	"F16C_SEAD00",
	"MQ9_GBU",
	"MQ9_AGM",
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"M1078A1R_SOV_M2_D",
	"M1084A1R_SOV_M2_D",
	"UH60M_EWS_CAS",
	"UH60M_ESSS_CAS",
	"Canoe",
	"AssaultBoat",
	"SDV"
};