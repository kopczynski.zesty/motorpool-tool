class Desert_2019: Faction
{
    name = "OCP / Desert";
    year = 2019;
    type = AIRBORNE;

    uniformScript = "\bwi_data_main\configs\factions\usa_airborne\2019\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\usa_airborne\2019\weapons_d.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\usa_airborne\2019\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\usa_airborne\2019\ammo_d.sqf";

    hiddenElements[] = {"IFV", "MBT", "ART", "AAA"};
	hiddenRoles[] = {"LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\usa_color_t.paa",
        "\bwi_resupply_main\data\usd_signs_1.paa",
        "\bwi_resupply_main\data\usd_signs_2.paa",
        "\bwi_resupply_main\data\usd_signs_3.paa"
    };

    #include <motorpool_d.hpp>
    #include <resupply_d.hpp>
};


class Woodland_2019: Faction
{
    name = "OCP / Woodland";
    year = 2019;
    type = AIRBORNE;

    uniformScript = "\bwi_data_main\configs\factions\usa_airborne\2019\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\usa_airborne\2019\weapons_w.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\usa_airborne\2019\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\usa_airborne\2019\ammo_w.sqf";

    hiddenElements[] = {"IFV", "MBT", "ART", "AAA"};
	hiddenRoles[] = {"LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\usa_color_g.paa",
        "\bwi_resupply_main\data\usa_signs_1.paa",
        "\bwi_resupply_main\data\usa_signs_2.paa",
        "\bwi_resupply_main\data\usa_signs_3.paa"
    };

    #include <motorpool_w.hpp>
    #include <resupply_w.hpp>
};