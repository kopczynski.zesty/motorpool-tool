// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Uniform.
switch ( _role ) do {
	default { [_unit, [ "UK3CB_CW_US_B_LATE_U_CombatUniform_01_WDL",
						"UK3CB_CW_US_B_LATE_U_CombatUniform_02_WDL",
						"UK3CB_CW_US_B_LATE_U_CombatUniform_03_WDL",
						"UK3CB_CW_US_B_LATE_U_CombatUniform_04_WDL"]] call bwi_armory_fnc_addRandomUniform; };

	case "plt_ldr";
	case "log_sgt": { _unit forceAddUniform "UK3CB_CW_US_B_LATE_U_OFFICER_Uniform_01_WDL"; };
	case "sqd_ldr";
	case "sqd_ldg";
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit forceAddUniform "UK3CB_CW_US_B_LATE_U_JNR_OFFICER_Uniform_01_WDL"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit forceAddUniform "UK3CB_CW_US_B_Early_U_SF_CombatUniform_03_ERDL_OLI"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit forceAddUniform "U_B_Wetsuit"; };

	case "rwc_pil";
	case "rwt_pil";
	case "uwc_pil": { _unit forceAddUniform "UK3CB_CW_US_B_LATE_U_H_Pilot_Uniform_01_NATO"; };
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "UK3CB_CW_US_B_LATE_U_J_Pilot_Uniform_01_NATO"; };
};


// Helmet.
switch ( _role ) do {
	default { [_unit, [ "UK3CB_CW_US_B_LATE_H_PASGT_01_WDL",
						"UK3CB_CW_US_B_LATE_H_PASGT_02_WDL"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addHeadgear "UK3CB_CW_US_B_EARLY_H_BoonieHat_ERDL_01"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		_unit addGoggles "UK3CB_G_Neck_Shemag_Oli";
		_unit addHeadgear "H_Watchcap_khk";
	};

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addGoggles "G_B_Diving"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "rhsusf_cvc_green_helmet"; };

	case "rwc_pil": { _unit addHeadgear "UK3CB_H_Crew_Helmet"; };
	case "rwt_pil": { _unit addHeadgear "UK3CB_H_Pilot_Helmet"; };
	case "fwc_pil": { _unit addHeadgear "RHS_jetpilot_usaf"; };
	case "fwt_pil": { _unit addHeadgear "rhsusf_hgu56p"; };
	case "uwc_pil": { _unit addHeadgear "rhssaf_beret_green"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_CW_US_B_LATE_V_PASGT_Rif_Vest"; };

	case "med_ldr";
	case "med_cpm": { _unit addVest "UK3CB_CW_US_B_LATE_V_PASGT_Medic_Vest"; };	


	case "sqd_lmg": { _unit addVest "UK3CB_CW_US_B_LATE_V_PASGT_MG_Vest"; };

	case "mmg_gun": { _unit addVest "UK3CB_CW_US_B_LATE_V_PASGT_MG_Vest"; };

	case "spf_ldr";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit addVest "UK3CB_CW_US_B_LATE_V_PASGT_Rif_Vest"; };
	case "spf_lmg": { _unit addVest "UK3CB_CW_US_B_LATE_V_PASGT_MG_Vest"; };
	case "spf_lat": { _unit addVest "UK3CB_CW_US_B_LATE_V_PASGT_Medic_Vest"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addVest "V_RebreatherB"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "UK3CB_CW_US_B_LATE_V_PASGT_Crew_Vest"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "V_TacVest_oli"; };
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "UK3CB_V_Pilot_Vest"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_AssaultPack_rgr"; };

	case "plt_ldr";
	case "tac_fac": { _unit addBackpack "UK3CB_CW_US_B_LATE_B_RIF"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "UK3CB_CW_US_B_LATE_B_RIF"; };// medic pack instead?

	case "sqd_amg": { _unit addBackpack "UK3CB_CW_US_B_LATE_B_RIF"; };

	case "spc_dem";
	case "eng_def": { _unit addBackpack "B_Carryall_oli"; };

	case "mmg_gun": { _unit addBackpack "UK3CB_CW_US_B_LATE_B_RIF"; };
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "B_Carryall_oli"; };

	case "how_ldr";
	case "how_gun": { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_uav": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_rif";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre": { _unit addBackpack "UK3CB_CW_US_B_LATE_B_RIF"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack";  };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};