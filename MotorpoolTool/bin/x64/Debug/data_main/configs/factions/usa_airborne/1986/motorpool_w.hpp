allowedVehicles[] = {
	"Willys_CW",
	"M151_CW",
	"M151_O_CW",
	"M151_M2_CW",
	"M151_TOW_CW",
	"M939_CW",
	"M939_O_CW",
	"M939_Recover_CW",
	"M939_Fuel_CW",
    "M939_Ammo_CW",
	"M939_Repair_CW",
	"M939_M2_CW",
	"M939_SBCOP_CW",
	"M113A3_CW",
	"M113A3_M240_CW",
	"M113A3_M2_CW",
	"M113A3E_Mk19_CW",
	"M113A3_Medical_CW",
	"M113A3_Ammo_CW",
	"MANHX60_CO_W",
	"OH6M",
	"MH6M",
	"UH60M",
	"UH60M_MG",
	"CH47_W",
	"AH6M_CAS",
	"AH6M_CAS_AGM",
	"AH6M_CAS_GAU",
	"AH1",
	"AH64",
	"C130",
	"C130_Cargo",
	"A10A_CAS90",
	"F15C_CAP90"
};

allowedVehiclesRecon[] = {
	"M1030_W"
};

allowedVehiclesSpecial[] = {
	"AssaultBoat",
	"RHIB_Small"
};