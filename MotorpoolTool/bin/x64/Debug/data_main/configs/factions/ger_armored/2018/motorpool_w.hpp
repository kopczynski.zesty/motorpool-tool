allowedVehicles[] = {
	"GLWolf_W",
	"GLWolf_Medical_W",
	"LKW5T_W",
	"LKW7T_W",
	"LKW5T_Fuel_W",
	"LKW7T_Ammo_W",
	"LKW10T_Repair_W",
	"LKW5T_SBCOP_W",
	"LKW7T_SBCOP_W",
	"LKW5T_HBCOP_W",
	"LKW7T_HBCOP_W",
	"EagleIV_W",
	"EagleIV_FLW_W",
	"Wiesel1A2_TOW_W",
	"Wiesel1A2_Mk20_W",
	"Fuchs1A4_W",
	"Fuchs1A4_Medical_W",
	"Fuchs1A4_HAT_W",
	"Marder1A5_W",
	"Puma_W",
	"Leopard2_W",
	"Gepard1A2_W",
	"UH_Tiger"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat",
	"SDV"
};