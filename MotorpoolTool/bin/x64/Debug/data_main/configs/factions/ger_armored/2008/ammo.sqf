// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary ammo.
switch ( _role ) do {
	default {
		[_unit, "BWA3_30Rnd_556x45_G36", 5] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_30Rnd_556x45_G36_Tracer", 4] call bwi_armory_fnc_addToVest;		
	};
	case "sqd_bre": {
		[_unit, "BWA3_30Rnd_556x45_G36", 4] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_30Rnd_556x45_G36_Tracer", 3] call bwi_armory_fnc_addToVest;		
	};

	case "sqd_lmg": {
		[_unit, "BWA3_200Rnd_556x45", 2] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_200Rnd_556x45_Tracer", 2] call bwi_armory_fnc_addToBackpack;		
	};
	case "mmg_gun": {
		[_unit, "BWA3_120Rnd_762x51_soft", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "BWA3_120Rnd_762x51_Tracer_soft", 2] call bwi_armory_fnc_addToVest;	
	};

	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": {
		[_unit, "BWA3_30Rnd_556x45_G36", 4] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_30Rnd_556x45_G36_Tracer", 3] call bwi_armory_fnc_addToVest;		
	};

	case "lrc_ldr";
	case "lrc_hir";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_rif": {
		[_unit, "BWA3_30Rnd_556x45_G36", 3] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_30Rnd_556x45_G36_Tracer", 2] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_30Rnd_556x45_G36_Tracer_Dim", 2] call bwi_armory_fnc_addToVest;	
	};
	case "lrc_lmg": {
		[_unit, "BWA3_200Rnd_556x45", 2] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_200Rnd_556x45_Tracer", 2] call bwi_armory_fnc_addToBackpack;	
	};
	case "lrc_dmr": {
		[_unit, "UK3CB_BAF_338_5Rnd", 15] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_BAF_338_5Rnd_Tracer", 15] call bwi_armory_fnc_addToBackpack;
	};

	case "spf_ldr";
	case "spf_ldg";
	case "spf_bre";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": {
		[_unit, "BWA3_30Rnd_556x45_G36", 4] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_30Rnd_556x45_G36_AP", 2] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_30Rnd_556x45_G36_Tracer", 2] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_30Rnd_556x45_G36_Tracer_Dim", 2] call bwi_armory_fnc_addToBackpack;
	}; 
	case "spf_lmg": {
		[_unit, "BWA3_200Rnd_556x45", 2] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_200Rnd_556x45_Tracer", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_dmr": {
		[_unit, "UK3CB_BAF_338_5Rnd", 20] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_BAF_338_5Rnd_Tracer", 20] call bwi_armory_fnc_addToBackpack;
	}; 

	case "scu_ldr";
	case "scu_bre";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif": {
		[_unit, "BWA3_30Rnd_556x45_G36_AP", 2] call bwi_armory_fnc_addToUniform;
		[_unit, "BWA3_30Rnd_556x45_G36", 4] call bwi_armory_fnc_addToBackpack;
		[_unit, "BWA3_30Rnd_556x45_G36_Tracer", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "BWA3_30Rnd_556x45_G36_Tracer_Dim", 2] call bwi_armory_fnc_addToBackpack;		
	}; 
	case "scu_lmg": {
		[_unit, "BWA3_200Rnd_556x45", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "BWA3_200Rnd_556x45_Tracer", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "BWA3_200Rnd_556x45_Tracer", 2] call bwi_armory_fnc_addToBackpack;	
	};
	case "scu_dmr": {
		[_unit, "UK3CB_BAF_338_5Rnd", 20] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_338_5Rnd_Tracer", 20] call bwi_armory_fnc_addToBackpack;
	};

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, "BWA3_30Rnd_556x45_G36", 3] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_30Rnd_556x45_G36_Tracer", 2] call bwi_armory_fnc_addToVest;	
	};

	case "rwc_pil": {
		[_unit, "BWA3_30Rnd_556x45_G36", 3] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_30Rnd_556x45_G36_Tracer", 2] call bwi_armory_fnc_addToVest;	
	};
};


// Secondary ammo.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg": {
		[_unit, "BWA3_15Rnd_9x19_P8", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "BWA3_15Rnd_9x19_P8", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "fwc_pil";
	case "fwt_pil": {
		[_unit, "BWA3_15Rnd_9x19_P8", 2] call bwi_armory_fnc_addToVest;
		[_unit, "BWA3_15Rnd_9x19_P8", 1] call bwi_armory_fnc_addToVest;
	};

	case "spf_ldr";
	case "spf_ldg";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": {
		[_unit, "rhsusf_mag_17Rnd_9x19_FMJ", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "scu_ldr";
	case "scu_lmg";
	case "scu_dmr";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif": {
		[_unit, "rhsusf_mag_17Rnd_9x19_FMJ", 2] call bwi_armory_fnc_addToUniform;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sqd_amg": {
		[_unit, "BWA3_200Rnd_556x45", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "BWA3_200Rnd_556x45_Tracer", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "mmg_ldr": {
		[_unit, "BWA3_120Rnd_762x51_soft", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "BWA3_120Rnd_762x51_Tracer_soft", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "mat_gun": {
		[_unit, "BWA3_PzF3_Tandem", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "mat_ldr": {
		[_unit, "BWA3_PzF3_Tandem", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "BWA3_PzF3_DM32", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "aat_gun": {
		[_unit, "BWA3_Fliegerfaust_Mag", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "aat_ldr": {
		[_unit, "BWA3_Fliegerfaust_Mag", 2] call bwi_armory_fnc_addToBackpack;
	};
};


// Other ammo.
switch ( _role ) do {
	case "sqd_bre": {
		[_unit, "rhsusf_8Rnd_00Buck", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhsusf_8Rnd_Slug", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_bre": {
		[_unit, "rhsusf_8Rnd_00Buck", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_8Rnd_Slug", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "scu_bre": {
		[_unit, "rhsusf_5Rnd_00Buck", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_5Rnd_Slug", 2] call bwi_armory_fnc_addToBackpack;
	};
};