class GER_Armored: FactionGroup
{
    name = "German Panzer Division";
    side = BLUFOR;
	
	flag  = "\bwi_data_main\data\flag_ger.paa";
    icon  = "\bwi_data_main\data\icon_s_ger.paa";
    image = "\bwi_data_main\data\icon_l_ger.paa";

    #include <2008\faction.hpp>
    #include <2018\faction.hpp>
};