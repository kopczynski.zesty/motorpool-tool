allowedSupplies[] = {
	"Mag_30Rnd_DM11",
	"Box_200Rnd_MG4",
	"Mag_8Rnd_Buck",
	"Mag_8Rnd_Slug",
	"Gren_Hand_HE_DM51A1",
	"Gren_Hand_WS_DM25",
	"Gren_Hand_CS_DM32",
	"Gren_40mm_HE_Generic",
	"Gren_40mm_WS_Generic",
	"Gren_40mm_CS_Generic",
	"Gren_40mm_WF_Generic",
	"Gren_40mm_CF_Generic",
	"AT_Tube_RGW90",
	"Medical_Bandages",
	"Medical_Medication",
	"Medical_Blood",
	"Medical_Bodybags",
	"Explosive_M112",
	"Explosive_M183",
	"Explosive_M6SLAM",
	"Explosive_M26",
	"Explosive_M15",
	"Explosive_M18A1",
	"Explosive_PMR3M",
	"Explosive_PMR3F",
	
	"DMG_MG3",
	"DMG_M2_Low",
	"DMG_M2_High",
	
	"Spare_Track",
	"Spare_Wheel",
	"Jerry_Can",

    "BaseWallWood",
    "BaseRampartWood",
    "BaseRampartCornerWood",
    "BaseRampartInvCornerWood",
    "BaseRampartBunkerWood",
    "BaseWallCargoWood",
    "BaseWallCornerBunkerWood",
    "BaseWallCornerCargoWood",
    "BaseSideEntranceWood",
    "BaseMainEntranceBunkerWood",
    "BaseMainEntranceCargoWood",
    "BaseFuelBladderWood",
    "BaseHelipadWood",
    "BaseCargoHouseWood",
    "BaseCargoHQWood",
    "BaseCargoTowerWood",
    "BaseAmmoDumpWoodWest",
    "BaseRampartAmmoDumpWoodWest",
    "BaseVehicleResupplyWoodWest",
    "MortarSandbagWood",
    "BunkerSandbagWood",
    "BunkerSandbagLargeWood",
    "SquadSandbagWoodWest",
    "SquadSandbagTallWoodWest",
    "CheckpointSandbagWoodWest",
    "SquadHescoWoodWest",
    "VehicleHescoWood",
    "MortarHescoWood",
    "MortarHescoWireWood",
    "TowerHescoWood",
    "BunkerHescoWood",
    "VehicleHescoCamoWoodWest",
    "CheckpointHescoWoodWest",
    "CheckpointHescoHeavyWoodWest",
    "VehicleTrenchWoodland",
    "SquadTrench90WoodWest",
    "SquadTrench180WoodWest",
    "SquadTrench360WoodWest",
    "VehicleTrenchCamoWoodlandWest",
    "VehicleTrenchLargeWoodlandWest",
    "PortableLightKit",
    "TallLightKit",
    "Shelter2",
    "Shelter4",
    "TankTrap4",
    "TankTrap8"
};

allowedSuppliesWeapons[] = {
	"Box_120Rnd_MG3",
	"AT_1Rnd_PzF3IT",
	"AT_1Rnd_PzF3BF",
	"AA_1Rnd_Fliegerfaust"
};

allowedSuppliesRecon[] = {
	"Mag_30Rnd_DM11_IR",
	"Mag_5Rnd_338_LM"
};

allowedSuppliesSpecial[] = {
	"Mag_30Rnd_DM11_IR",
	"Mag_30Rnd_DM31",
	"Mag_5Rnd_338_LM"
};