// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary weapon.
switch ( _role ) do {
	default { _unit addWeapon "BWA3_G36A3"; };

	case "plt_ldg";
	case "sqd_ldg": { _unit addWeapon "BWA3_G36A3_AG40"; };

	case "sqd_gre": { _unit addWeapon "BWA3_G36A3_AG40"; };
	case "sqd_lmg": { _unit addWeapon "BWA3_MG4"; };

	case "mmg_gun": { _unit addWeapon "BWA3_MG5"; };

	case "lrc_ldr";
	case "lrc_hir": { _unit addWeapon "BWA3_G36A3_AG40"; };
	case "lrc_lmg": { _unit addWeapon "BWA3_MG4"; };
	case "lrc_dmr": { _unit addWeapon "BWA3_G27"; };
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_rif": { _unit addWeapon "BWA3_G36KA3"; };

	case "spf_lmg": { _unit addWeapon "BWA3_MG4"; };
	case "spf_dmr": { _unit addWeapon "BWA3_G28"; }; 
	case "spf_ldg": { _unit addWeapon "BWA3_G38_AG40"; };
	case "spf_ldr";
	case "spf_bre";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": { _unit addWeapon "BWA3_G38"; }; 

	case "scu_lmg": { _unit addWeapon "BWA3_MG4"; };
	case "scu_dmr": { _unit addWeapon "BWA3_G28"; };
	case "scu_ldr": { _unit addWeapon "BWA3_G38K_AG40"; }; 
	case "scu_bre";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif": { _unit addWeapon "BWA3_G38K"; }; 

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addWeapon "BWA3_G36KA3"; };

	case "rwc_pil": { _unit addWeapon "BWA3_G36KA3"; };
};


// Secondary weapon.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg";
	case "fwc_pil";
	case "fwt_pil": { _unit addWeapon "BWA3_P8"; };

	case "spf_ldr";
	case "spf_ldg";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif";

	case "scu_ldr";
	case "scu_lmg";
	case "scu_dmr";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif": {
		_unit addWeapon "rhsusf_weap_glock17g4";
		_unit addHandgunItem "rhsusf_acc_omega9k";
	};
};


// Launcher.
switch ( _role ) do {
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat";
	case "spf_ldr";
	case "spf_lat";
	case "scu_lat": { _unit addWeapon "BWA3_RGW90_Loaded"; }; 

	case "mat_gun": { _unit addWeapon "BWA3_Pzf3"; };
	case "aat_gun": { _unit addWeapon "BWA3_Fliegerfaust"; };
};


// Backpack weapon.
switch ( _role ) do {
	case "mot_ldr": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "mot_gun": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "dat_ldr": { _unit addBackpack "Redd_Milan_Static_Tripod"; };
	case "dat_gun": { _unit addBackpack "Redd_Milan_Static_Barrel"; };
	case "dgg_ldr": { _unit addBackpack "rnt_gmw_static_tripod_ai"; };
	case "dgg_gun": { _unit addBackpack "rnt_gmw_static_barell_ai"; };
	case "lrc_uav": { _unit addBackpack "B_UAV_01_backpack_F"; };

	case "sqd_bre";
	case "spf_bre": { [_unit, "rhs_weap_M590_8RD", 1] call bwi_armory_fnc_addToBackpack; };
	case "scu_bre": { [_unit, "rhs_weap_M590_5RD", 1] call bwi_armory_fnc_addToBackpack; };
};