class Desert_2008: Faction
{
    name = "Tropentarn / Desert";
    year = 2008;
    type = ARMORED;

    uniformScript = "\bwi_data_main\configs\factions\ger_armored\2008\uniform_d.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\ger_armored\2008\weapons_d.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\ger_armored\2008\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\ger_armored\2008\ammo.sqf";

    hiddenElements[] = {"HAT", "RWT", "FWT", "FWC", "UWC", "HOW"};
    hiddenRoles[]    = {"UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\ger_color_b.paa",
        "\bwi_resupply_main\data\ger_signs_1.paa",
        "\bwi_resupply_main\data\ger_signs_2.paa",
        "\bwi_resupply_main\data\ger_signs_3.paa"
    };

    #include <motorpool_d.hpp>
    #include <resupply_d.hpp>
};


class Woodland_2008: Faction
{
    name = "Flecktarn / Woodland";
    year = 2008;
    type = ARMORED;

    uniformScript = "\bwi_data_main\configs\factions\ger_armored\2008\uniform_w.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\ger_armored\2008\weapons_w.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\ger_armored\2008\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\ger_armored\2008\ammo.sqf";

    hiddenElements[] = {"HAT", "RWT", "FWT", "FWC", "UWC", "HOW"};
    hiddenRoles[]    = {"UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\ger_color_b.paa",
        "\bwi_resupply_main\data\ger_signs_1.paa",
        "\bwi_resupply_main\data\ger_signs_2.paa",
        "\bwi_resupply_main\data\ger_signs_3.paa"
    };

    #include <motorpool_w.hpp>
    #include <resupply_w.hpp>
};