// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on special forces and scuba roles.
if ( (_role select [0,3]) in ["spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "BWA3_Uniform2_Fleck"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit forceAddUniform "U_B_Wetsuit"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit forceAddUniform "BWA3_Uniform_Crew_Fleck"; };

	case "rwc_pil";
	case "rwt_pil";
	case "uwc_pil": { _unit forceAddUniform "BWA3_Uniform_Helipilot"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "BWA3_M92_Fleck"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		_unit addGoggles "rhsusf_shemagh2_gogg_od";
		_unit addHeadgear "BWA3_OpsCore_Fleck_Camera";
	};

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addGoggles "G_B_Diving"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "BWA3_CrewmanKSK_Fleck_Headset"; };

	case "rwc_pil": { _unit addHeadgear "BWA3_Knighthelm"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "BWA3_Vest_Rifleman_Fleck"; };

	case "plt_ldr";
	case "log_sgt";
	case "sqd_ldr";
	case "sqd_ldg";
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit addVest "BWA3_Vest_Leader_Fleck"; };

	case "med_ldr";
	case "med_cpm": { _unit addVest "BWA3_Vest_Medic_Fleck"; };	
	
	case "sqd_gre": { _unit addVest "BWA3_Vest_Grenadier_Fleck"; };

	case "sqd_lmg";
	case "mmg_gun": { _unit addVest "BWA3_Vest_MachineGunner_Fleck"; };

	case "spf_ldr": { _unit addVest "BWA3_Vest_Leader_Fleck"; };
	case "spf_lmg": { _unit addVest "BWA3_Vest_MachineGunner_Fleck"; };
	case "spf_dmr": { _unit addVest "BWA3_Vest_Marksman_Fleck"; };
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_rif": { _unit addVest "BWA3_Vest_Rifleman_Fleck"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addVest "V_RebreatherB"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "BWA3_Vest_Fleck"; };

	case "rwc_pil": { _unit addVest "BWA3_Vest_Fleck"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "BWA3_TacticalPack_Fleck"; };

	case "plt_ldr": { _unit addBackpack "BWA3_Kitbag_Fleck"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "BWA3_Kitbag_Fleck_Medic"; };

	case "spc_dem";
	case "eng_def": { _unit addBackpack "BWA3_Carryall_Fleck"; };

	case "mmg_ldr": { _unit addBackpack "BWA3_Kitbag_Fleck";  };
	case "mat_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "BWA3_Carryall_Fleck"; };

	case "how_ldr";
	case "how_gun": { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_uav": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_rif": { _unit addBackpack "BWA3_TacticalPack_Fleck"; };
	case "spf_lat";
	case "spf_sap";
	case "spf_bre": { _unit addBackpack "BWA3_Kitbag_Fleck"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack";  };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil": { /* No backpack */ };
};