allowedVehicles[] = {
	"UAZ_CDF",
	"UAZ_O_CDF",
	"UAZ_DSHKM_CDF",
	"UAZ_AGS30_CDF",
	"UAZ_SPG9_CDF",
	"GAZ66_CDF",
	"GAZ66_O_CDF",
	"GAZ66_FB_CDF",
	"GAZ66_O_FB_CDF",
	"GAZ66_Ammo_CDF",
	"GAZ66_Repair_CDF",
	"GAZ66_ZU23_CDF",
	"Ural4320_CDF",
	"Ural4320_O_CDF",
	"Ural4320_Recover_CDF",
	"Ural4320_Fuel_CDF",
	"Ural4320_Repair_CDF",
	"Ural4320_Ammo_CDF",
	"Ural4320_ZU23_CDF",
	"Ural4320_D30A_CDF",
	"BRDM2_CDF",
	"BRDM2_GPMG_CDF",
	"BRDM2_HMG_CDF",
	"BRM1K_CDF",
	"BMD2_CDF",
	"MANHX60_CO_W",
	"Mi17_CDF",
	"Mi17_MG_CDF",
	"Mi17Sh_CAS_CDF",
	"Mi24D_CDF",
	"Su25_CAS00_CDF",
	"AN2_CDF",
	"L159_CAS00_CDF",
	"L159_CAP00_CDF",
	"L159_Multi00_CDF",
	"JAS39_CAS00",
	"JAS39_CAP00",
	"MiG29S_Multi9090_CDF",
	"MiG29S_CAP90_CDF"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat"
};