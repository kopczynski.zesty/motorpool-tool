class Woodland_2018: Faction
{
	name = "TTsKO / Woodland";
	year = 2018;
	type = AIRBORNE;

	uniformScript = "\bwi_data_main\configs\factions\cdf_airborne\2018\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\cdf_airborne\2018\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\cdf_airborne\2018\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\cdf_airborne\2018\ammo.sqf";

	hiddenElements[] = {"UWC", "SCU", "MBT", "ART"};
	hiddenRoles[]	 = {"UAV", "LHG", "AHG"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\che_color_g.paa",
		"\bwi_resupply_main\data\che_signs_1.paa",
		"\bwi_resupply_main\data\che_signs_2.paa",
		"\bwi_resupply_main\data\che_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_w.hpp>
};