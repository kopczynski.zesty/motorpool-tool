class Desert_2019: Faction
{
    name = "MTP / Desert";
    year = 2019;
    type = AIRBORNE;

    uniformScript = "\bwi_data_main\configs\factions\gbr_airborne\2019\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\gbr_airborne\2019\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\gbr_airborne\2019\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\gbr_airborne\2019\ammo.sqf";

    hiddenElements[] = {"MAT", "DAT", "APC", "IFV", "MBT", "ART", "AAA"};
	hiddenRoles[] = {"LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\gbr_color_b.paa",
        "\bwi_resupply_main\data\gbr_signs_1.paa",
        "\bwi_resupply_main\data\gbr_signs_2.paa",
        "\bwi_resupply_main\data\gbr_signs_3.paa"
    };

    #include <motorpool_d.hpp>
    #include <resupply_d.hpp>
};


class Woodland_2019: Faction
{
    name = "MTP / Woodland";
    year = 2019;
    type = AIRBORNE;

    uniformScript = "\bwi_data_main\configs\factions\gbr_airborne\2019\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\gbr_airborne\2019\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\gbr_airborne\2019\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\gbr_airborne\2019\ammo.sqf";

    hiddenElements[] = {"MAT", "DAT", "APC", "IFV", "MBT", "ART", "AAA"};
	hiddenRoles[] = {"LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\gbr_color_b.paa",
        "\bwi_resupply_main\data\gbr_signs_1.paa",
        "\bwi_resupply_main\data\gbr_signs_2.paa",
        "\bwi_resupply_main\data\gbr_signs_3.paa"
    };

    #include <motorpool_w.hpp>
    #include <resupply_w.hpp>
};