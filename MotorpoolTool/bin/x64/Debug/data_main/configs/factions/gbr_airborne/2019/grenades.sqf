// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Hand grenades.
switch ( _role ) do {
	default {
		[_unit, "UK3CB_BAF_SmokeShell", 2] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_BAF_SmokeShellGreen", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_m67", 2] call bwi_armory_fnc_addToVest;
	};

	case "log_sgt";
	case "tac_fac";
	case "tac_jtc": {
		[_unit, "UK3CB_BAF_SmokeShell", 2] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_BAF_SmokeShellGreen", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_m67", 2] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_BAF_SmokeShellPurple", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_SmokeShellRed", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": {
		[_unit, "UK3CB_BAF_SmokeShell", 1] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_BAF_SmokeShellGreen", 1] call bwi_armory_fnc_addToVest;	
		[_unit, "UK3CB_BAF_SmokeShellRed", 1] call bwi_armory_fnc_addToVest;	
	};

	case "scu_ldr";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": {
		[_unit, "UK3CB_BAF_SmokeShell", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_SmokeShellGreen", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_m67", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "scu_lmg": {
		[_unit, "UK3CB_BAF_SmokeShell", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_SmokeShellGreen", 1] call bwi_armory_fnc_addToUniform;
		[_unit, "rhs_mag_m67", 2] call bwi_armory_fnc_addToUniform;
	};

	case "eng_def";
	case "eng_mec";
	case "med_ldr";
	case "med_cpm";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr";
	case "aat_gun";
	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun";
	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, "UK3CB_BAF_SmokeShell", 2] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_BAF_SmokeShellGreen", 1] call bwi_armory_fnc_addToVest;	
	};

	case "fwc_pil";
	case "fwt_pil";
	case "rwc_pil";
	case "rwt_pil": {
		[_unit, "UK3CB_BAF_SmokeShellGreen", 1] call bwi_armory_fnc_addToVest;
		[_unit, "ACE_HandFlare_Green", 1] call bwi_armory_fnc_addToVest;	
	};
};


// 40mm rounds.
switch ( _role ) do {
	case "plt_ldg": {
		[_unit, "UK3CB_BAF_UGL_FlareWhite_F", 2] call bwi_armory_fnc_addToBackpack;

		[_unit, "UK3CB_BAF_1Rnd_SmokeRed_Grenade_shell", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_1Rnd_Smoke_Grenade_shell", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "UK3CB_BAF_1Rnd_HE_Grenade_Shell", 3] call bwi_armory_fnc_addToBackpack;
	};

	
	case "sqd_ldg": {
		[_unit, "UK3CB_BAF_UGL_FlareWhite_F", 2] call bwi_armory_fnc_addToBackpack;

		[_unit, "UK3CB_BAF_1Rnd_SmokeRed_Grenade_shell", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_1Rnd_Smoke_Grenade_shell", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "UK3CB_BAF_1Rnd_HE_Grenade_Shell", 5] call bwi_armory_fnc_addToBackpack;
	};

	case "sqd_gre": {
		[_unit, "UK3CB_BAF_UGL_FlareWhite_F", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_UGL_FlareRed_F", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "UK3CB_BAF_1Rnd_SmokeRed_Grenade_shell", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_1Rnd_Smoke_Grenade_shell", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_1Rnd_SmokeGreen_Grenade_shell", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "UK3CB_BAF_1Rnd_HE_Grenade_Shell", 10] call bwi_armory_fnc_addToBackpack;
	};

	case "lrc_ldr": {
		[_unit, "UK3CB_BAF_UGL_FlareWhite_F", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_UGL_FlareGreen_F", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_UGL_FlareRed_F", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "UK3CB_BAF_1Rnd_SmokeRed_Grenade_shell", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_1Rnd_Smoke_Grenade_shell", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "UK3CB_BAF_1Rnd_HE_Grenade_Shell", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "spf_ldg";
	case "scu_ldr": {
		[_unit, "UK3CB_BAF_UGL_FlareWhite_F", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_UGL_FlareRed_F", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "UK3CB_BAF_1Rnd_SmokeRed_Grenade_shell", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_1Rnd_Smoke_Grenade_shell", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_BAF_1Rnd_SmokeGreen_Grenade_shell", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "UK3CB_BAF_1Rnd_HE_Grenade_Shell", 10] call bwi_armory_fnc_addToBackpack;
	};
};