allowedVehicles[] = {
	"LandRoverSoft_W",
	"LandRoverHard_W",
	"LandRoverSnatch_W",
	"LandRoverMedical_W",
	"LandRoverWMIK_GPMG_W",
	"LandRoverWMIK_HMG_W",
	"LandRoverWMIK_HAT_W",
	"LandRoverWMIK_GMG_W",
	"MANHX60_W",
	"MANHX60_FB_W",
	"MANHX60_CO_W",
	"MANHX60_Fuel_W",
	"MANHX60_Repair_W",
    "MANHX60_L118_W",
	"Jackal_HMG_W",
	"Jackal_GMG_W",
	"AW159_AH1",
	"AW159_AH1_CAS",
	"AW159_AH1_AT",
	"AW101_HC3",
	"AW101_HC3C",
	"AW101_HC3L",
	"AW101_HC3_MG",
	"AW101_HC3_CSAR",
	"Chinook_HC2",
	"ApacheAH1",
	"C130C4",
	"C130C4_Cargo",
	"F35BL_Stealth_CAS10",
	"F35BL_Beast_CAS10",
	"F35BL_Stealth_CAP10",
	"F35BL_Beast_CAP10",
	"MQ9R_CAS",
	"MQ9R_GBU",
	"MQ9R_AGM"
};

allowedVehiclesRecon[] = {
	"Quadbike",
	"MRZR_W"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"MRZR_W",
	"Canoe",
	"AssaultBoat",
	"RHIB_Small",
	"RHIB_ORC_GPMG",
	"RHIB_ORC_HMG",
	"SDV"
};