allowedVehicles[] = {
	"LandRoverSoft_D",
	"LandRoverHard_D",
	"LandRoverSnatch_D",
	"LandRoverMedical_D",
	"LandRoverWMIK_GPMG_D",
	"LandRoverWMIK_HMG_D",
	"LandRoverWMIK_HAT_D",
	"LandRoverWMIK_GMG_D",
	"MANHX60_D",
	"MANHX60_FB_D",
	"MANHX60_CO_D",
	"MANHX60_Fuel_D",
	"MANHX60_Repair_D",
    "MANHX60_L118_D",
	"Jackal_HMG_D",
	"Jackal_GMG_D",
	"AW159_AH1",
	"AW159_AH1_CAS",
	"AW159_AH1_AT",
	"AW101_HC3",
	"AW101_HC3C",
	"AW101_HC3L",
	"AW101_HC3_MG",
	"AW101_HC3_CSAR",
	"Chinook_HC2",
	"ApacheAH1",
	"C130C4",
	"C130C4_Cargo",
	"F35BL_Stealth_CAS10",
	"F35BL_Beast_CAS10",
	"F35BL_Stealth_CAP10",
	"F35BL_Beast_CAP10",
	"MQ9R_CAS",
	"MQ9R_GBU",
	"MQ9R_AGM"
};

allowedVehiclesRecon[] = {
	"Quadbike",
	"MRZR_D"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"MRZR_D",
	"Canoe",
	"AssaultBoat",
	"RHIB_Small",
	"RHIB_ORC_GPMG",
	"RHIB_ORC_HMG",
	"SDV"
};