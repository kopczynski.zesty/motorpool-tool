allowedVehicles[] = {
	"UAZ_MSV_D",
	"UAZ_O_MSV_D",
	"GAZ66_MSV_D",
	"GAZ66_FB_MSV_D",
	"GAZ66_O_MSV_D",
	"GAZ66_O_FB_MSV_D",
	"GAZ66_Ammo_MSV_D",
	"GAZ66_SBCOP_MSV_D",
	"GAZ66_ZU23_MSV_D",
	"GAZ66_D30A_MSV_D",
	"ZiL131_MSV",
	"ZiL131_FB_MSV",
	"ZiL131_O_MSV",
	"ZiL131_O_FB_MSV",
	"ZiL131_BBCOP_MSV_D",
	"PTSM_VMF",
	"BRDM2_MSV_D",
	"BRDM2_GPMG_MSV_D",
	"BRDM2_HMG_MSV_D",
	"BRDM2_ATGM_MSV_D",
	"BRM1K_MSV_D",
	"BTR70_MSV_D",
	"BTR80_MSV_D",
	"BMP1_MSV_D",
	"BMP1P_MSV_D",
	"BMP2E_MSV_D",
	"BMP2_MSV_D",
	"BMP3_MSV_D",
	"T72B84_D",
	"T72B85_D",
	"T72B89_D",
	"T80BVK",
	"T80U_D",
	"T90",
	"ZSU234_D",
	"2S1_D",
	"2S3M1_D",
	"BM21_MSV",
	"Mi24V_VVSC",
	"Mi24P_VVSC",
	"Su25_CAS00",
	"MiG29S_Multi9090",
	"MiG29S_CAP90"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat",
	"RHIB_Small",
	"SDV"
};