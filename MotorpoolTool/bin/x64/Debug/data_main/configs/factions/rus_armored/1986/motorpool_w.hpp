allowedVehicles[] = {
	"UAZ_SOV",
	"UAZ_O_SOV",
	"UAZ_DSHKM_SOV",
	"UAZ_AGS30_SOV",
	"UAZ_SPG9_SOV",
	"Ural4320_SOV",
	"Ural4320_O_SOV",
	"Ural4320_Recover_SOV",
	"Ural4320_Fuel_SOV",
	"Ural4320_Ammo_SOV",
	"Ural4320_Repair_SOV",
	"Ural4320_ZU23_SOV",
	"Ural4320_D30A_SOV",
	"Ural4320_SBCOP_SOV",
	"Ural4320_BBCOP_SOV",
	"BRDM2_SOV",
	"BRDM2_GPMG_SOV",
	"BRDM2_HMG_SOV",
	"BRDM2_ATGM_SOV",
	"BRM1K_SOV",
	"BTR60_SOV",
	"BTR70_SOV",
	"ZSU234_W",
	"BMP1_MSV_W",
	"BMP2E_MSV_W",
	"T72B_SOV",
	"T72BA_SOV",
	"T72BB_SOV",
	"T80",
	"T80A",
	"T80B",
	"Mi24V_SOV",
	"Su25_CAS90"
};

allowedVehiclesRecon[] = {
	"Yava250N_SOV"
};

allowedVehiclesSpecial[] = {
	"AssaultBoat"
};