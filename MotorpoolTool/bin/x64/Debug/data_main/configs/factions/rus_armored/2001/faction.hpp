class Desert_2001: Faction
{
    name = "Flora / Desert";
    year = 2001;
    type = ARMORED;

    uniformScript = "\bwi_data_main\configs\factions\rus_armored\2001\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\rus_armored\2001\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\rus_armored\2001\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\rus_armored\2001\ammo.sqf";

    hiddenElements[] = {"HAT", "UWC"};
    hiddenRoles[]    = {"HIR", "UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\rus_color_k.paa",
        "\bwi_resupply_main\data\rus_signs_1.paa",
        "\bwi_resupply_main\data\rus_signs_2.paa",
        "\bwi_resupply_main\data\rus_signs_3.paa"
    };

    #include <motorpool_d.hpp>
    #include <resupply_d.hpp>
};


class Woodland_2001: Faction
{
    name = "Flora / Woodland";
    year = 2001;
    type = ARMORED;

    uniformScript = "\bwi_data_main\configs\factions\rus_armored\2001\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\rus_armored\2001\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\rus_armored\2001\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\rus_armored\2001\ammo.sqf";

    hiddenElements[] = {"HAT", "UWC"};
    hiddenRoles[]    = {"HIR", "UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\rus_color_k.paa",
        "\bwi_resupply_main\data\rus_signs_1.paa",
        "\bwi_resupply_main\data\rus_signs_2.paa",
        "\bwi_resupply_main\data\rus_signs_3.paa"
    };

    #include <motorpool_w.hpp>
    #include <resupply_w.hpp>
};