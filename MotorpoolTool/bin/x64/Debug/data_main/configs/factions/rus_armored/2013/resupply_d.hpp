allowedSupplies[] = {
	"Mag_30Rnd_7N10",
    "Box_60Rnd_RPK",
	"Gren_Hand_HE_RGD5",
	"Gren_Hand_WS_RDG2",
	"Gren_Hand_CS_NSPD",
    "Gren_Hand_CS_NSPN",
	"Gren_40mm_HE_VOG25",
	"Gren_40mm_WS_VG40MD",
	"Gren_40mm_CS_VG40MD",
	"Gren_40mm_WF_VG40OP",
	"Gren_40mm_CF_VG40OP",
	"AT_Tube_RPG26",
	"AT_Tube_RSHG2",
	"Medical_Bandages",
	"Medical_Medication",
	"Medical_Blood",
	"Medical_Bodybags",
	"Explosive_M112",
	"Explosive_M183",
	"Explosive_M6SLAM",
	"Explosive_M26",
	"Explosive_M15",
	"Explosive_M18A1",
	"Explosive_PMR3M",
	"Explosive_PMR3F",
	
	"DMG_Kord_Low",
	"DMG_Kord_High",
	
	"Spare_Track",
	"Spare_Wheel",
	"Jerry_Can",

	"BaseVehicleResupplyDesertEast",
    "MortarSandbagDesert",
    "BunkerSandbagDesert",
    "BunkerSandbagLargeDesert",
    "SquadSandbagDesertEast",
    "SquadSandbagTallDesertEast",
    "CheckpointSandbagDesertEast",
	"BunkerBarricadeSmall",
	"BunkerBarricadeMedium",
	"BunkerBarricadeLarge",
	"CheckpointBarricadeEast",
    "SquadBarricadeDesertEast",
    "SquadBarricadeTallDesertEast",
    "VehicleTrenchDesert",
    "SquadTrench90DesertEast",
    "SquadTrench180DesertEast",
    "SquadTrench360DesertEast",
    "VehicleTrenchCamoDesertEast",
    "VehicleTrenchLargeDesertEast",
    "PortableLightKit",
    "TankTrap4",
    "TankTrap8"
};

allowedSuppliesWeapons[] = {
    "Box_100Rnd_57N323S",
    "Box_100Rnd_7N13",
	"AT_1Rnd_PG7VR",
	"AT_1Rnd_PG7VL",
    "AT_1Rnd_OG7V",
	"AT_1Rnd_TBG7V",
	"AA_1Rnd_9K38"
};

allowedSuppliesRecon[] = {
    "Mag_30Rnd_7U1",
    "Mag_10Rnd_7N14"
};

allowedSuppliesSpecial[] = {
    "Mag_30Rnd_57N231_Polymer",
    "Mag_30Rnd_57N231P_Polymer",
    "Mag_10Rnd_7N14",
    "Mag_20Rnd_SP5",
    "Mag_20Rnd_SP6",
	"Gren_40mm_HE_M441",
	"Gren_40mm_WS_M714",
	"Gren_40mm_CS_M713",
	"Gren_40mm_WF_M585",
	"Gren_40mm_CF_M661"
};