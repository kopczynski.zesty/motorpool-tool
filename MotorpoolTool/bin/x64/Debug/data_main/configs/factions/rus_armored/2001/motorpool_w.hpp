allowedVehicles[] = {
	"UAZ_MSV_W",
	"UAZ_O_MSV_W",
	"GAZ66_MSV_W",
	"GAZ66_FB_MSV_W",
	"GAZ66_O_MSV_W",
	"GAZ66_O_FB_MSV_W",
	"GAZ66_Ammo_MSV_W",
	"GAZ66_SBCOP_MSV_W",
	"GAZ66_ZU23_MSV_W",
	"GAZ66_D30A_MSV_W",
	"ZiL131_MSV",
	"ZiL131_FB_MSV",
	"ZiL131_O_MSV",
	"ZiL131_O_FB_MSV",
	"ZiL131_BBCOP_MSV_W",
	"PTSM_VMF",
	"BRDM2_MSV_W",
	"BRDM2_GPMG_MSV_W",
	"BRDM2_HMG_MSV_W",
	"BRDM2_ATGM_MSV_W",
	"BRM1K_MSV_W",
	"BTR70_MSV_W",
	"BTR80_MSV_W",
	"BMP1_MSV_W",
	"BMP1P_MSV_W",
	"BMP2E_MSV_W",
	"BMP2_MSV_W",
	"BMP3_MSV_W",
	"T72B84_W",
	"T72B85_W",
	"T72B89_W",
	"T80BVK",
	"T80U_W",
	"T90",
	"ZSU234_W",
	"2S1_W",
	"2S3M1_W",
	"BM21_MSV",
	"Mi24V_VVSC",
	"Mi24P_VVSC",
	"Su25_CAS00",
	"MiG29S_Multi9090",
	"MiG29S_CAP90"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat",
	"RHIB_Small",
	"SDV"
};