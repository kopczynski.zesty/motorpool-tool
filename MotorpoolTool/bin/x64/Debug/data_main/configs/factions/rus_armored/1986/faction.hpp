class Woodland_1986: Faction
{
	name = "Khaki / Woodland";
	year = 1986;
	type = ARMORED;

	uniformScript = "\bwi_data_main\configs\factions\rus_armored\1986\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\rus_armored\1986\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\rus_armored\1986\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\rus_armored\1986\ammo.sqf";

	hiddenElements[] = {"SCU", "HAT", "DGG", "UWC", "RWT", "FWT"};
	hiddenRoles[]	 = {"HIR", "UAV", "LHG", "AHG"};

	acreRadioDevices[] = {"NONE", "NONE", "ACRE_SEM52SL", "ACRE_PRC77"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\rus_color_k.paa",
		"\bwi_resupply_main\data\rus_signs_1.paa",
		"\bwi_resupply_main\data\rus_signs_2.paa",
		"\bwi_resupply_main\data\rus_signs_3.paa"
	};

	#include <motorpool_w.hpp>
	#include <resupply_w.hpp>
};