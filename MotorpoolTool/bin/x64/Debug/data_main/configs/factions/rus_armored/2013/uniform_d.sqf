// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on special forces and scuba roles.
if ( (_role select [0,3]) in ["spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhs_uniform_emr_des_patchless"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit forceAddUniform "rhs_uniform_gorka_r_y"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit forceAddUniform "U_B_Wetsuit"; };

	case "rwc_pil";
	case "rwt_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "rhs_uniform_df15"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "rhs_6b7_1m"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		// No goggles.
		_unit addHeadgear "rhs_6b7_1m_bala2";
	};

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addGoggles "G_B_Diving"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "rhs_tsh4"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addHeadgear "rhs_zsh7a_mike"; };
	case "fwc_pil": { _unit addHeadgear "rhs_zsh7a_alt"; };
	case "fwt_pil": { _unit addHeadgear "rhs_zsh7a_mike"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "rhs_6b23_digi_6sh92"; };

	case "plt_ldr";
	case "log_sgt": { _unit addVest "rhs_6b23_digi_6sh92_radio"; };

	case "mmg_ldr";
	case "mat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit addVest "rhs_6b23_digi_6sh92_radio"; };

	case "med_ldr";
	case "med_cpm": { _unit addVest "rhs_6b23_digi_medic"; };	

	case "spc_eod";
	case "spc_dem";
	case "eng_def";
	case "eng_mec": { _unit addVest "rhs_6b23_digi_6sh92_vog_headset"; };	

	case "sqd_ldr": { _unit addVest "rhs_6b23_digi_6sh92_headset"; };
	case "sqd_ldg": { _unit addVest "rhs_6b23_digi_6sh92_vog_headset"; };
	case "sqd_gre": { _unit addVest "rhs_6b23_6sh116_vog"; };

	case "lrc_uav": { _unit addVest "rhs_6b23_digi_6sh92_vog"; };

	case "lrc_dmr";
	case "spf_dmr": { _unit addVest "rhs_6b23_digi_sniper"; };

	case "spf_ldr": { _unit addVest "rhs_6b23_6sh116_vog"; };
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_rif": { _unit addVest "rhs_6b23_6sh116"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addVest "V_RebreatherB"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "rhs_6b23_digi_crew"; };

	case "rwc_pil";
	case "rwt_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "V_TacVest_blk"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_oli"; };

	case "plt_ldr";
	case "med_ldr";
	case "med_cpm";
	case "sqd_amg";
	case "sqd_lmg";
	case "mmg_gun": { _unit addBackpack "B_Kitbag_rgr";  };

	case "spc_dem";
	case "eng_def";
	case "mmg_ldr";
	case "mat_ldr";
	case "aat_ldr": { _unit addBackpack "B_CarryAll_oli"; };

	case "how_ldr";
	case "how_gun": { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_uav": { /* Backpack weapon */ };

	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre": { _unit addBackpack "B_Kitbag_rgr"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack";  };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};