// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Uniform.
switch ( _role ) do {
	default { [_unit, [ "UK3CB_TKA_I_U_CombatUniform_01_OLI",
						"UK3CB_TKA_I_U_CombatUniform_02_OLI",
						"UK3CB_TKA_I_U_CombatUniform_03_OLI"]] call bwi_armory_fnc_addRandomUniform; };

	case "plt_ldr";
	case "log_sgt";
	case "sqd_ldr";
	case "sqd_ldg";
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit forceAddUniform "UK3CB_TKA_I_U_Officer_OLI"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit forceAddUniform "UK3CB_TKP_I_U_CombatUniform_BLK"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { [_unit, [ "UK3CB_TKA_I_U_CombatUniform_01_TKA_Brush",
								"UK3CB_TKA_I_U_CombatUniform_02_TKA_Brush",
								"UK3CB_TKA_I_U_CombatUniform_03_TKA_Brush"]] call bwi_armory_fnc_addRandomUniform; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { [_unit, [ "UK3CB_TKA_I_U_CrewUniform_01_KHK",
								"UK3CB_TKA_I_U_CrewUniform_02_KHK",
								"UK3CB_TKA_I_U_CrewUniform_03_KHK"]] call bwi_armory_fnc_addRandomUniform; };

	case "rwc_pil";
	case "rwt_pil": { [_unit, [ "UK3CB_TKA_I_U_H_Pilot_01_DES",
								"UK3CB_TKA_I_U_H_Pilot_02_DES",
								"UK3CB_TKA_I_U_H_Pilot_03_DES"]] call bwi_armory_fnc_addRandomUniform; };
	
	case "uwc_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "UK3CB_TKA_I_U_J_Pilot_Des"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "UK3CB_TKA_I_H_SSh68_Oli"; };

	case "plt_ldr": { _unit addHeadgear "UK3CB_TKA_I_H_Patrolcap_OFF_OLI"; };
	case "log_sgt": { _unit addHeadgear "UK3CB_TKA_I_H_Patrolcap_OLI"; };

	case "lrc_ldr": { _unit addHeadgear "UK3CB_TKP_B_H_Patrolcap_Off_TAN"; };
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addHeadgear "UK3CB_TKP_B_H_Patrolcap_TAN"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		_unit addGoggles "rhsusf_shemagh2_tan";
		_unit addHeadgear "UK3CB_TKA_I_H_SSh68_Khk";
	};

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "UK3CB_H_Crew_Cap"; };

	case "rwc_pil": { _unit addHeadgear "UK3CB_H_Crew_Helmet"; };
	case "rwt_pil": { _unit addHeadgear "UK3CB_H_Pilot_Helmet"; };
	
	case "fwc_pil": { _unit addHeadgear "UK3CB_TKA_O_H_zsh7a_Des"; };
	case "fwt_pil": { _unit addHeadgear "UK3CB_TKA_O_H_zsh7a_mike_Des"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_TKA_I_V_6Sh92_TKA_Brush"; };

	case "plt_ldr";
	case "log_sgt";
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr";
	case "sqd_ldr";
	case "sqd_ldg": { _unit addVest "UK3CB_TKA_I_V_6Sh92_Radio_TKA_Brush"; };

	case "lrc_ldr": { _unit addVest "UK3CB_TKP_I_V_6Sh92_Radio_Des"; };
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_rif";
	case "lrc_dmr": { _unit addVest "UK3CB_TKP_I_V_6Sh92_Des"; };
	

	case "spf_ldr": { _unit addVest "UK3CB_TKA_I_V_6Sh92_Radio_Des"; };
	case "spf_lat";
	case "spf_lmg";
	case "spf_sap";
	case "spf_bre";
	case "spf_rif";
	case "spf_dmr": { _unit addVest "UK3CB_TKA_I_V_6Sh92_Des"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "UK3CB_TKA_I_V_6Sh92_Des"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "UK3CB_TKA_I_V_vydra_3m_Tan"; };
	
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "V_TacVest_khk"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_khk"; };

	case "plt_ldr";
	case "log_sgt";
	case "tac_fac": { _unit addBackpack "B_Kitbag_cbr"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "UK3CB_B_Alice_Medic_Bedroll_K"; };

	case "spc_eod";
	case "spc_dem";
	case "eng_def": { _unit addBackpack "UK3CB_B_Alice_Bedroll_K"; };

	case "mmg_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "UK3CB_B_Alice_Bedroll_K"; };

	case "mat_ldr": { _unit addBackpack "B_Kitbag_cbr"; };
	case "mat_gun": { _unit addBackpack "rhs_rpg_empty"; };

	case "sqd_lmg";
	case "sqd_amg": { _unit addBackpack "UK3CB_B_Alice_Bedroll_K"; };
	case "mmg_gun";
	case "sqd_sap": { _unit addBackpack "B_Kitbag_cbr"; };

	case "how_ldr";
	case "how_gun"; { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_uav": { /* Backpack weapon */ };
	case "lrc_lmg": { _unit addBackpack "UK3CB_B_Alice_Bedroll_K"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_rif";
	case "spf_lat": { _unit addBackpack "B_FieldPack_cbr"; };
	case "spf_sap": { _unit addBackpack "B_Kitbag_cbr"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};
