allowedSupplies[] = {
	"Mag_30Rnd_M855A1_HK33",
	"Box_50Rnd_G3SG1",
	
	"Gren_Hand_HE_RGD5",
	"Gren_Hand_WS_RDG2",
	"Gren_Hand_CS_NSPD",
	"Gren_Hand_CS_NSPN",
	"Gren_40mm_HE_M441",
	"Gren_40mm_WS_M714",
	"Gren_40mm_CS_M713",
	"Gren_40mm_WF_M585",
	"Gren_40mm_CF_M661",
	
	"Gren_40mm_HE_MGL",
	"Gren_40mm_HEDP_MGL",
	"Gren_40mm_HET_MGL",
	"Gren_40mm_WS_MGL",
	"Gren_40mm_CS_MGL",
	"Gren_40mm_WF_MGL",
	"Gren_40mm_CF_MGL",
	
	"AT_Tube_RPG26",
	"Medical_Bandages",
	"Medical_Medication",
	"Medical_Blood",
	"Medical_Bodybags",
	"Explosive_M112",
	"Explosive_M183",
	"Explosive_M6SLAM",
	"Explosive_M26",
	"Explosive_M15",
	"Explosive_M18A1",
	"Explosive_PMR3M",
	"Explosive_PMR3F",
	
	"DMG_Kord_Low",
	"DMG_Kord_High",

	"Spare_Track",
	"Spare_Wheel",
	"Jerry_Can",

	"BaseVehicleResupplyDesertEast",
	"MortarSandbagDesert",
	"BunkerSandbagDesert",
	"BunkerSandbagLargeDesert",
	"SquadSandbagDesertEast",
	"SquadSandbagTallDesertEast",
	"CheckpointSandbagDesertEast",
	"BunkerBarricadeSmall",
	"BunkerBarricadeMedium",
	"BunkerBarricadeLarge",
	"CheckpointBarricadeEast",
	"SquadBarricadeDesertEast",
	"SquadBarricadeTallDesertEast",
	"VehicleTrenchDesert",
	"SquadTrench90DesertEast",
	"SquadTrench180DesertEast",
	"SquadTrench360DesertEast",
	"VehicleTrenchCamoDesertEast",
	"VehicleTrenchLargeDesertEast",
	"PortableLightKit",
	"TankTrap4",
	"TankTrap8"
};

allowedSuppliesWeapons[] = {
	"Box_100Rnd_M80",
	"Box_100Rnd_M61AP",
	"Box_50Rnd_M80_SP",
	"Box_50Rnd_M61AP_SP",
	"AT_1Rnd_PG7VR",
	"AT_1Rnd_PG7VL",
	"AT_1Rnd_OG7V",
	"AT_1Rnd_TBG7V",
	"AA_1Rnd_9K38"
};

allowedSuppliesRecon[] = {
	"Mag_20Rnd_PSG1"
};

allowedSuppliesSpecial[] = {
	"Mag_20Rnd_G3",
	"Mag_20Rnd_PSG1"
};