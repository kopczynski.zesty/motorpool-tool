// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on special forces and scuba roles.
if ( (_role select [0,3]) in ["spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { [_unit, [ "UK3CB_TKA_O_U_CombatUniform_01_ADPM",
						"UK3CB_TKA_O_U_CombatUniform_02_ADPM",
						"UK3CB_TKA_O_U_CombatUniform_03_ADPM"]] call bwi_armory_fnc_addRandomUniform; };

	case "plt_ldr";
	case "log_sgt";
	case "sqd_ldr";
	case "sqd_ldg";
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit forceAddUniform "UK3CB_TKA_O_U_Officer_ADPM"; };

	case "lrc_ldr": { _unit forceAddUniform "UK3CB_TKP_O_U_Officer_Blk"; };
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit forceAddUniform "UK3CB_TKP_O_U_CombatUniform_Blk"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit forceAddUniform "UK3CB_TKA_O_U_CombatUniform_02_Des_TKA_Brush"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { [_unit, [ "UK3CB_TKA_O_U_CrewUniform_01_ADPM",
								"UK3CB_TKA_O_U_CrewUniform_02_ADPM",
								"UK3CB_TKA_O_U_CrewUniform_03_ADPM"]] call bwi_armory_fnc_addRandomUniform; };

	case "rwc_pil";
	case "rwt_pil": { [_unit, [ "UK3CB_TKA_O_U_H_Pilot_01_ADD",
								"UK3CB_TKA_O_U_H_Pilot_02_ADD",
								"UK3CB_TKA_O_U_H_Pilot_03_ADD"]] call bwi_armory_fnc_addRandomUniform; };
	
	case "uwc_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "UK3CB_TKA_O_U_J_Pilot_Digi"; };
};


// Helmet.
switch ( _role ) do {
	default { [_unit, [ "UK3CB_TKA_O_H_6b27m_ADPM",
						"UK3CB_TKA_O_H_6b27m_ESS_ADPM",
						"UK3CB_TKA_O_H_6b27m_Tan",
						"UK3CB_TKA_O_H_6b27m_ESS_Tan"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "lrc_ldr": { _unit addHeadgear "UK3CB_TKP_O_H_Patrolcap_Off_Blk"; };
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addHeadgear "UK3CB_TKP_O_H_Patrolcap_Blk"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		/* No goggles. */
		_unit addHeadgear "UK3CB_TKA_O_H_6b7_1m_bala2_Surpat";
	};

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "rhs_tsh4"; };

	case "rwc_pil": { _unit addHeadgear "UK3CB_H_Crew_Helmet"; };
	case "rwt_pil": { _unit addHeadgear "UK3CB_H_Pilot_Helmet"; };
	
	case "fwc_pil": { _unit addHeadgear "UK3CB_TKA_O_H_zsh7a_Des"; };
	case "fwt_pil": { _unit addHeadgear "UK3CB_TKA_O_H_zsh7a_mike_Des"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_TKA_O_V_6b23_ml_Oli_ADPM"; };

	case "plt_ldr";
	case "log_sgt";
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr";
	case "sqd_ldr";
	case "sqd_ldg": { _unit addVest "UK3CB_TKA_O_V_6b23_ML_6sh92_radio_Oli_ADPM"; };

	case "med_ldr";
	case "med_cpm": { _unit addVest "UK3CB_TKA_O_V_6b23_medic_Oli"; };

	case "sqd_gre";
	case "sqd_lhg": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_6sh92_vog_Oli_ADPM"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_rif";
	case "lrc_dmr": { _unit addVest "UK3CB_TKP_O_V_6b23_ML_6sh92_radio_BLK"; };

	case "spf_ldr": { _unit addVest "UK3CB_TKA_O_V_6b23_ML_6sh92_radio_Surpat"; };
	case "spf_lat": { _unit addVest "UK3CB_TKA_O_V_6b23_medic_Surpat"; };
	case "spf_lmg";
	case "spf_sap";
	case "spf_bre";
	case "spf_rif": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_Surpat_02"; };
	case "spf_dmr": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_sniper_Surpat"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_crew_Oli"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "UK3CB_TKA_O_V_6b23_vydra_3m_ADD_02"; };
	
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "V_TacVest_khk"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "UK3CB_TKA_O_B_RIF_Khk"; };

	case "plt_ldr";
	case "log_sgt";
	case "tac_fac": { _unit addBackpack "B_Kitbag_cbr"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "UK3CB_B_Alice_Medic_Bedroll_K"; };

	case "spc_dem";
	case "eng_def": { _unit addBackpack "UK3CB_B_Alice_Bedroll_K"; };

	case "mmg_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "UK3CB_B_Alice_Bedroll_K"; };
	case "aat_gun": { _unit addBackpack "B_Kitbag_cbr"; };

	case "mat_ldr": { _unit addBackpack "B_Kitbag_cbr"; };
	case "mat_gun": { _unit addBackpack "rhs_rpg_empty"; };

	case "sqd_lmg";
	case "sqd_lhg";
	case "sqd_amg";
	case "sqd_ahg": { _unit addBackpack "UK3CB_B_Alice_Bedroll_K"; };
	case "mmg_gun";
	case "sqd_sap": { _unit addBackpack "B_Kitbag_cbr"; };

	case "how_ldr";
	case "how_gun"; { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addBackpack "UK3CB_TKP_O_B_ASS_BLK"; };
	case "lrc_uav": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_rif";
	case "spf_lat";
	case "spf_sap": { _unit addBackpack "B_Kitbag_cbr"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};