allowedSupplies[] = {
	"Mag_20Rnd_FNFAL",
	"Box_75Rnd_RPK",
	"Gren_Hand_HE_RGD5",
	"Gren_Hand_WS_RDG2",
	"Gren_Hand_CS_NSPD",
	"Gren_Hand_CS_NSPN",
	"AT_Tube_RPG26",
	"Medical_Bandages",
	"Medical_Medication",
	"Medical_Blood",
	"Medical_Bodybags",
	"Explosive_M112",
	"Explosive_M183",
	"Explosive_M6SLAM",
	"Explosive_M26",
	"Explosive_M15",
	"Explosive_M18A1",
	"Explosive_PMR3M",
	"Explosive_PMR3F",
	
	"DMG_DShKM_Low",
	"DMG_DShKM_High",

	"Spare_Track",
	"Spare_Wheel",
	"Jerry_Can",

	"BaseVehicleResupplyDesertEast",
	"MortarSandbagDesert",
	"BunkerSandbagDesert",
	"BunkerSandbagLargeDesert",
	"SquadSandbagDesertEast",
	"SquadSandbagTallDesertEast",
	"CheckpointSandbagDesertEast",
	"BunkerBarricadeSmall",
	"BunkerBarricadeMedium",
	"BunkerBarricadeLarge",
	"CheckpointBarricadeEast",
	"SquadBarricadeDesertEast",
	"SquadBarricadeTallDesertEast",
	"VehicleTrenchDesert",
	"SquadTrench90DesertEast",
	"SquadTrench180DesertEast",
	"SquadTrench360DesertEast",
	"VehicleTrenchCamoDesertEast",
	"VehicleTrenchLargeDesertEast",
	"PortableLightKit",
	"TankTrap4",
	"TankTrap8"
};

allowedSuppliesWeapons[] = {
	"Box_100Rnd_57N323S",
	"Box_100Rnd_7N13",
	"AT_1Rnd_PG7V",
	"AT_1Rnd_PG7VL",
	"AA_1Rnd_9K38"
};

allowedSuppliesRecon[] = {
	"Mag_10Rnd_7N14",
	"Mag_10Rnd_7N1"
};

allowedSuppliesSpecial[] = {
	"Mag_30Rnd_7N6_Bakelite",
	"Box_60Rnd_RPK",
	"Gren_40mm_HE_VOG25",
	"Gren_40mm_WS_VG40MD",
	"Gren_40mm_CS_VG40MD",
	"Gren_40mm_WF_VG40OP",
	"Gren_40mm_CF_VG40OP",
	"Mag_10Rnd_7N14",
	"Mag_10Rnd_7N1"
};