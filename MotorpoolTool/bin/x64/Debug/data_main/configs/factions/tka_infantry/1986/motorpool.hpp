allowedVehicles[] = {
	"LandRover110_O_TKA",
	"LandRover110_HMG_TKA",
	"LandRover110_SPG9_TKA",
	"BTR40_TKA",
	"BTR40_DSHKM_TKA",
	"PragaV3S_TKA",
	"PragaV3S_O_TKA",
	"PragaV3S_Recover_TKA",
	"PragaV3S_Fuel_TKA",
	"PragaV3S_Repair_TKA",
	"PragaV3S_Ammo_TKA",
	"PragaV3S_ZU23_TKA",
	"PragaV3S_D30A_TKA",
	"PragaV3S_SBCOP_TKA_D",
	"PragaV3S_BBCOP_TKA_D",
	"BRDM2_GPMG_TKA",
	"BRDM2_HMG_TKA",
	"BRDM2_ATGM_TKA",
	"BRM1K_TKA",
	"MTLB_TKA",
	"BTR60_TKA",
	"T3485M_TKA",
	"T55A_TKA",
	"BM21_TKA",
	"Mi8_TKA",
	"Mi8_MG_TKA",
	"Mi8AMTSh_CAS_TKA",
	"Mi24P_TKA",
	"L39_CAS80_TKA",
	"L39_CAP80_TKA",
	"L39_Multi80_TKA",
	"Su25_CAS90_TKA",
	"AN2_TKA",
	"MiG29S_Multi9090_TKA",
	"MiG29S_CAP90_TKA"
};

allowedVehiclesRecon[] = {
	"Yava250N_TKA"
};

allowedVehiclesSpecial[] = {
	"AssaultBoat"
};