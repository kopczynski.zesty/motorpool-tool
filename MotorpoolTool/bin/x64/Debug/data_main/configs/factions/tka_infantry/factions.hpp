// Takistan Socialist Army (INDEP)
class TSA_Infantry: FactionGroup
{
	name = "Takistan Socialist Army";
	side = INDEP;
	
	flag  = "\bwi_data_main\data\flag_tka.paa";
    icon  = "\bwi_data_main\data\icon_s_tka.paa";
    image = "\bwi_data_main\data\icon_l_tka.paa";

	#include <1986\faction.hpp>
};

// Takistan National Army (OPFOR)
class TKA_Infantry: FactionGroup
{
	name = "Takistan National Army";
	side = OPFOR;

	flag  = "\bwi_data_main\data\flag_tka.paa";
    icon  = "\bwi_data_main\data\icon_s_tka.paa";
    image = "\bwi_data_main\data\icon_l_tka.paa";

	#include <2009\faction.hpp>
};

// New Takistan Army (BLUFOR)
class NTA_Infantry: FactionGroup
{
	name = "New Takistan Army";
	side = BLUFOR;
	
	flag  = "\bwi_data_main\data\flag_tka.paa";
    icon  = "\bwi_data_main\data\icon_s_tka.paa";
    image = "\bwi_data_main\data\icon_l_tka.paa";

	#include <2019\faction.hpp>
};