// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on special forces and scuba roles.
if ( (_role select [0,3]) in ["spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { [_unit, [ "UK3CB_TKA_B_U_CombatUniform_01_WDL",
						"UK3CB_TKA_B_U_CombatUniform_Shortsleeve_01_WDL",
						"UK3CB_TKA_B_U_CombatUniform_02_WDL",
						"UK3CB_TKA_B_U_CombatUniform_Shortsleeve_02_WDL"]] call bwi_armory_fnc_addRandomUniform; };

	case "plt_ldr";
	case "log_sgt";
	case "sqd_ldr";
	case "sqd_ldg";
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit forceAddUniform "UK3CB_TKA_B_U_Officer_WDL"; };

	case "lrc_ldr": { _unit forceAddUniform "UK3CB_TKP_B_U_Officer_TAN"; };
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit forceAddUniform "UK3CB_TKP_B_U_CombatUniform_TAN"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { [_unit, [ "UK3CB_TKA_B_U_CombatUniform_01_DES_MARPAT",
								"UK3CB_TKA_B_U_CombatUniform_Shortsleeve_01_DES_MARPAT",
								"UK3CB_TKA_B_U_CombatUniform_02_DES_MARPAT",
								"UK3CB_TKA_B_U_CombatUniform_Shortsleeve_02_DES_MARPAT"]] call bwi_armory_fnc_addRandomUniform; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit forceAddUniform "UK3CB_TKA_B_U_CrewUniform_WDL"; };

	case "rwc_pil";
	case "rwt_pil": { _unit forceAddUniform "UK3CB_TKA_B_U_H_Pilot_DES"; };
	
	case "uwc_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "UK3CB_TKA_B_U_J_Pilot_NATO"; };
};


// Helmet.
switch ( _role ) do {
	default { [_unit, [ "UK3CB_CW_US_B_LATE_H_PASGT_01_WDL",
						"UK3CB_CW_US_B_LATE_H_PASGT_02_WDL"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "lrc_ldr": { _unit addHeadgear "UK3CB_TKP_B_H_Patrolcap_Off_TAN"; };
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addHeadgear "UK3CB_TKP_B_H_Patrolcap_TAN"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		_unit addGoggles "UK3CB_G_Neck_Shemag_Tan";
		_unit addHeadgear "UK3CB_TKA_B_H_DES_MARPAT";
	};

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "rhs_tsh4"; };

	case "rwc_pil": { _unit addHeadgear "UK3CB_H_Crew_Helmet"; };
	case "rwt_pil": { _unit addHeadgear "UK3CB_H_Pilot_Helmet"; };
	
	case "fwc_pil": { _unit addHeadgear "rhs_zsh7a_alt"; };
	case "fwt_pil": { _unit addHeadgear "rhs_zsh7a"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_TKA_B_V_GA_LITE_WDL"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_rif";
	case "lrc_dmr": { _unit addVest "UK3CB_TKP_B_V_TacVest_Tan"; };

	case "spf_ldr";
	case "spf_lat";
	case "spf_lmg";
	case "spf_sap";
	case "spf_bre";
	case "spf_rif";
	case "spf_dmr": { _unit addVest "UK3CB_TKA_B_V_GA_HEAVY_DES_MARPAT"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "V_TacVest_khk"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "V_TacVest_oli"; };
	
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "UK3CB_V_Pilot_Vest"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "UK3CB_TKA_B_B_ASS"; };

	case "plt_ldr";
	case "log_sgt";
	case "tac_fac": { _unit addBackpack "B_Kitbag_cbr"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "UK3CB_B_Alice_Medic_Bedroll_K"; };

	case "spc_dem";
	case "eng_def": { _unit addBackpack "UK3CB_B_Alice_Bedroll_K"; };

	case "mmg_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "UK3CB_B_Alice_Bedroll_K"; };
	case "aat_gun";
	
	case "mat_ldr": { _unit addBackpack "B_Kitbag_cbr"; };
	case "mat_gun": { _unit addBackpack "rhs_rpg_empty"; };

	case "sqd_lmg";
	case "sqd_lhg";
	case "sqd_amg";
	case "sqd_ahg": { _unit addBackpack "UK3CB_B_Alice_Bedroll_K"; };
	case "mmg_gun";
	case "sqd_sap": { _unit addBackpack "B_Kitbag_cbr"; };

	case "how_ldr";
	case "how_gun"; { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addBackpack "UK3CB_UN_B_B_ASS"; };
	case "lrc_uav": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_rif";
	case "spf_lat";
	case "spf_sap": { _unit addBackpack "B_Kitbag_cbr"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};