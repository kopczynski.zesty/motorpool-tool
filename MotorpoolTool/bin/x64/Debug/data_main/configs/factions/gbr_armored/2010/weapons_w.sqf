// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary weapon.
switch ( _role ) do {
	default { _unit addWeapon "UK3CB_BAF_L85A2_RIS_G_AFG"; };

	case "plt_ldg";
	case "sqd_ldg": { _unit addWeapon "UK3CB_BAF_L85A2_UGL_HWS"; };

	case "sqd_gre": { _unit addWeapon "UK3CB_BAF_L85A2_UGL_HWS"; };
	case "sqd_lmg": { _unit addWeapon "rhs_weap_m249_pip_S_para"; };

	case "mmg_gun": { _unit addWeapon "UK3CB_BAF_L7A2"; };

	case "lrc_ldr";
	case "lrc_hir": { _unit addWeapon "UK3CB_BAF_L85A2_UGL_HWS"; };
	case "lrc_lmg": { _unit addWeapon "rhs_weap_m249_pip_S_para"; };
	case "lrc_dmr": { _unit addWeapon "UK3CB_BAF_L129A1"; };
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_rif": { _unit addWeapon "UK3CB_BAF_L85A2_RIS_G_AFG"; };

	case "spf_lmg": { _unit addWeapon "rhs_weap_m249_pip_S_para"; };
	case "spf_dmr": { _unit addWeapon "UK3CB_BAF_L115A3_BL"; }; 
	case "spf_ldg": { _unit addWeapon "UK3CB_BAF_L119A1_UKUGL"; };
	case "spf_ldr";
	case "spf_bre";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": { _unit addWeapon "UK3CB_BAF_L119A1_FG"; }; 

	case "scu_lmg": { _unit addWeapon "rhs_weap_m249_pip_S_para"; };
	case "scu_dmr": { _unit addWeapon "UK3CB_BAF_L115A3_BL"; };
	case "scu_ldr": { _unit addWeapon "UK3CB_BAF_L119A1_UKUGL"; }; 
	case "scu_bre";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif": { _unit addWeapon "UK3CB_BAF_L119A1_FG"; }; 

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addWeapon "UK3CB_BAF_L22A2"; };

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": { _unit addWeapon "UK3CB_BAF_L22A2"; };
};


// Secondary weapon.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg";
	case "fwc_pil";
	case "fwt_pil": { _unit addWeapon "UK3CB_BAF_L131A1"; };

	case "spf_ldr";
	case "spf_ldg";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif";

	case "scu_lmg";
	case "scu_dmr";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif": {
		_unit addWeapon "UK3CB_BAF_L131A1";
		_unit addHandgunItem "muzzle_snds_l";
	};
};


// Launcher.
switch ( _role ) do {
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat": { _unit addWeapon "UK3CB_BAF_NLAW_Launcher"; };
	case "spf_ldr";
	case "spf_lat";
	case "scu_lat": { _unit addWeapon "rhs_weap_m72a7"; };

	case "hat_gun": { _unit addWeapon "rhs_weap_fgm148"; };
	case "aat_gun": { _unit addWeapon "rhs_weap_fim92"; };
};


// Backpack weapon.
switch ( _role ) do {
	case "mot_ldr": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "mot_gun": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "dgg_ldr": { _unit addBackpack "RHS_Mk19_Tripod_Bag"; };
	case "dgg_gun": { _unit addBackpack "RHS_Mk19_Gun_Bag"; };
	case "lrc_uav": { _unit addBackpack "B_UAV_01_backpack_F"; };

	case "sqd_bre";
	case "spf_bre";
	case "scu_bre": { [_unit, "UK3CB_BAF_L128A1", 1] call bwi_armory_fnc_addToBackpack; };
};