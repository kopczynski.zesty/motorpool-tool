// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on special forces and scuba roles.
if ( (_role select [0,3]) in ["spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "UK3CB_BAF_U_CombatUniform_MTP"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit forceAddUniform "UK3CB_BAF_U_Smock_DPMT"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit forceAddUniform "U_B_Wetsuit"; };

	case "rwc_pil";
	case "rwt_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "UK3CB_BAF_U_HeliPilotCoveralls_RAF"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "UK3CB_BAF_H_Mk7_Camo_D"; };
	
	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addHeadgear "UK3CB_BAF_H_Mk7_Scrim_B"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		_unit addGoggles "UK3CB_G_Balaclava";
		_unit addHeadgear "rhsusf_mich_bare_norotos_arc_alt_headset";
	};

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addGoggles "G_B_Diving"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "UK3CB_BAF_H_CrewHelmet_A"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addHeadgear "UK3CB_BAF_H_PilotHelmetHeli_A"; };
	case "fwc_pil": { _unit addHeadgear "RHS_jetpilot_usaf"; };
	case "fwt_pil": { _unit addHeadgear "UK3CB_H_Pilot_Helmet"; };
	case "uwc_pil": { _unit addHeadgear "UK3CB_BAF_H_Mk7_Camo_D"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_BAF_V_Osprey_Rifleman_E"; };

	case "plt_ldr";
	case "log_sgt": { _unit addVest "UK3CB_BAF_V_Osprey_SL_D"; };

	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit addVest "UK3CB_BAF_V_Osprey_SL_D"; };

	case "med_ldr";
	case "med_cpm": { _unit addVest "UK3CB_BAF_V_Osprey_Medic_D"; };	

	case "spc_eod";
	case "spc_dem";
	case "eng_def";
	case "eng_mec": { _unit addVest "UK3CB_BAF_V_Osprey_Rifleman_B"; };	

	case "sqd_ldr": { _unit addVest "UK3CB_BAF_V_Osprey_SL_D"; };
	case "sqd_ldg";
	case "lrc_ldr": { _unit addVest "UK3CB_BAF_V_Osprey_SL_C"; };
	case "sqd_gre": { _unit addVest "UK3CB_BAF_V_Osprey_Grenadier_B"; };

	case "sqd_lmg";
	case "lrc_lmg";
	case "mmg_gun": { _unit addVest "UK3CB_BAF_V_Osprey_MG_A"; };

	case "lrc_dmr": { _unit addVest "UK3CB_BAF_V_Osprey_Marksman_A"; };

	case "spf_ldr": { _unit addVest "UK3CB_BAF_V_Osprey_DPMT3"; };
	case "spf_lmg": { _unit addVest "UK3CB_BAF_V_Osprey_DPMT7"; };
	case "spf_dmr": { _unit addVest "UK3CB_BAF_V_Osprey_DPMT4"; };
	case "spf_lat": { _unit addVest "UK3CB_BAF_V_Osprey_DPMT5"; };
	case "spf_sap";
	case "spf_bre";
	case "spf_rif": { _unit addVest "UK3CB_BAF_V_Osprey_DPMT8"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addVest "V_RebreatherB"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "UK3CB_BAF_V_Osprey_Lite"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "UK3CB_V_Pilot_Vest"; };
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "V_TacVest_oli"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Rifleman_L_A"; };

	case "plt_ldr": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Rifleman_L_B"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Medic_L_B"; };

	case "spc_eod";
	case "spc_dem";
	case "eng_mec": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Engineer_L_A"; };
	case "eng_def": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Engineer_H_A"; };

	case "sqd_bre": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_PointMan_L_A"; };

	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Rifleman_L_C"; };

	case "how_ldr";
	case "how_gun": { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_uav": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_rif": { _unit addBackpack "UK3CB_BAF_B_Bergen_DPMT_Rifleman_A"; };
	case "spf_lat";
	case "spf_sap";
	case "spf_bre": { _unit addBackpack "UK3CB_BAF_B_Bergen_DPMT_Rifleman_B"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack";  };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};