class Desert_2010: Faction
{
    name = "MTP / Desert";
    year = 2010;
    type = ARMORED;

    uniformScript = "\bwi_data_main\configs\factions\gbr_armored\2010\uniform_d.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\gbr_armored\2010\weapons_d.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\gbr_armored\2010\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\gbr_armored\2010\ammo.sqf";

    hiddenElements[] = {"MAT", "DAT", "MBT", "FWT", "RWT", "AAA"};
    hiddenRoles[]    = {"UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\gbr_color_b.paa",
        "\bwi_resupply_main\data\gbr_signs_1.paa",
        "\bwi_resupply_main\data\gbr_signs_2.paa",
        "\bwi_resupply_main\data\gbr_signs_3.paa"
    };

    #include <motorpool_d.hpp>
    #include <resupply_d.hpp>
};


class Woodland_2010: Faction
{
    name = "MTP / Woodland";
    year = 2010;
    type = ARMORED;

    uniformScript = "\bwi_data_main\configs\factions\gbr_armored\2010\uniform_w.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\gbr_armored\2010\weapons_w.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\gbr_armored\2010\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\gbr_armored\2010\ammo.sqf";

    hiddenElements[] = {"MAT", "DAT", "MBT", "FWT", "RWT", "AAA"};
    hiddenRoles[]    = {"UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\gbr_color_b.paa",
        "\bwi_resupply_main\data\gbr_signs_1.paa",
        "\bwi_resupply_main\data\gbr_signs_2.paa",
        "\bwi_resupply_main\data\gbr_signs_3.paa"
    };

    #include <motorpool_w.hpp>
    #include <resupply_w.hpp>
};