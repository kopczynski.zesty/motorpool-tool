class GBR_Armored: FactionGroup
{
    name = "British Army Mechanized";
    side = BLUFOR;
	
	flag  = "\bwi_data_main\data\flag_gbr.paa";
    icon  = "\bwi_data_main\data\icon_s_gbr.paa";
    image = "\bwi_data_main\data\icon_l_gbr.paa";

    #include <2010\faction.hpp>
};