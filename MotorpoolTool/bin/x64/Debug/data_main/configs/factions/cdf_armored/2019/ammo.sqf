// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary ammo.
switch ( _role ) do {
	default {
		[_unit, "rhsgref_30rnd_556x45_vhs2", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_vhs2_t", 4] call bwi_armory_fnc_addToBackpack;
	};

	case "med_ldr";
	case "med_cpm": {
		[_unit, "rhsgref_30rnd_556x45_vhs2", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_vhs2_t", 4] call bwi_armory_fnc_addToVest;
	};

	case "spc_eod";
	case "spc_dem";
	case "eng_def";
	case "eng_mec": {
		[_unit, "rhsgref_30rnd_556x45_vhs2", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_vhs2_t", 2] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_vhs2_t", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "sqd_bre";
	case "sqd_lhg";
	case "sqd_ahg":	{
		[_unit, "rhsgref_30rnd_556x45_vhs2", 4] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_vhs2_t", 3] call bwi_armory_fnc_addToVest;
	};

	case "sqd_lmg": {
		[_unit, "rhsusf_200Rnd_556x45_soft_pouch", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhsusf_200Rnd_556x45_soft_pouch", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_200Rnd_556x45_mixed_soft_pouch", 2] call bwi_armory_fnc_addToBackpack;
	};
	
	case "mat_gun": {
		[_unit, "rhsgref_30rnd_556x45_vhs2", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_vhs2_t", 4] call bwi_armory_fnc_addToVest;
	};
	
	case "mmg_gun": {
		[_unit, "rhsusf_100Rnd_762x51_m80a1epr", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_100Rnd_762x51_m62_tracer", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "hat_ldr";
	case "hat_gun";
	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": {
		[_unit, "rhsgref_30rnd_556x45_vhs2", 4] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_vhs2_t", 2] call bwi_armory_fnc_addToVest;
	};

	case "lrc_ldr";
	case "lrc_hir";
	case "lrc_lat";
	case "lrc_rif": {
		[_unit, "rhsgref_30rnd_556x45_vhs2", 4] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_vhs2_t", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_lmg": {
		[_unit, "rhsusf_200Rnd_556x45_soft_pouch", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhsusf_200Rnd_556x45_soft_pouch", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_200Rnd_556x45_mixed_soft_pouch", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_dmr": {
		[_unit, "ACE_20Rnd_762x51_M118LR_Mag", 4] call bwi_armory_fnc_addToVest;
		[_unit, "ACE_20Rnd_762x51_Mk316_Mod_0_Mag", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_uav": { };

	case "spf_ldr";
	case "spf_ldg";
	case "spf_bre";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": {
		[_unit, "rhs_mag_20Rnd_SCAR_762x51_mk316_special_bk", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_20Rnd_SCAR_762x51_mk316_special_bk", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_20Rnd_SCAR_762x51_m62_tracer_bk", 4] call bwi_armory_fnc_addToBackpack;
	}; 
	case "spf_lmg": {
		[_unit, "rhsusf_200Rnd_556x45_soft_pouch", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhsusf_200Rnd_556x45_soft_pouch", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_200Rnd_556x45_mixed_soft_pouch", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_dmr": {
		[_unit, "rhs_mag_20Rnd_SCAR_762x51_m118_special_bk", 4] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_20Rnd_SCAR_762x51_mk316_special_bk", 2] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_20Rnd_SCAR_762x51_m80_ball_bk", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_20Rnd_SCAR_762x51_m62_tracer_bk", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "scu_ldr";
	case "scu_bre";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif"; 
	case "scu_lmg";
	case "scu_dmr": { };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, "rhsgref_30rnd_556x45_vhs2", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_vhs2_t", 2] call bwi_armory_fnc_addToVest;
	};

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": {
		[_unit, "rhsgref_30rnd_556x45_vhs2", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_vhs2_t", 2] call bwi_armory_fnc_addToVest;
	};
	case "uwc_pil": { /* No primary */ };
};


// Secondary ammo.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg": {
		[_unit, "rhsusf_mag_17Rnd_9x19_FMJ", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "fwc_pil";
	case "fwt_pil";
	case "uwc_pil": {
		[_unit, "rhsusf_mag_17Rnd_9x19_FMJ", 3] call bwi_armory_fnc_addToVest;
	};

	case "spf_ldr";
	//case "spf_ldg"; // M320 GLM
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": {
		[_unit, "rhs_mag_9x18_8_57N181S", 2] call bwi_armory_fnc_addToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sqd_amg": {
		[_unit, "rhsusf_200Rnd_556x45_soft_pouch", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_200Rnd_556x45_mixed_soft_pouch", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "mmg_ldr": {
		[_unit, "rhsusf_100Rnd_762x51_m80a1epr", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_100Rnd_762x51_m62_tracer", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "hat_gun": {
		[_unit, "rhs_fgm148_magazine_AT", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "hat_ldr": {
		[_unit, "rhs_fgm148_magazine_AT", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "mat_gun": {
		[_unit, "rhs_rpg7_PG7VR_mag", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "mat_ldr": {
		[_unit, "rhs_rpg7_PG7VR_mag", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_rpg7_PG7VL_mag", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_rpg7_OG7V_mag", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "aat_gun": {
		[_unit, "rhs_mag_9k38_rocket", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "aat_ldr": {
		[_unit, "rhs_mag_9k38_rocket", 2] call bwi_armory_fnc_addToBackpack;
	};
};


// Other ammo.
// None.