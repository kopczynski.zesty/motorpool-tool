class Woodland_2009: Faction
{
	name = "TTsKO / Woodland";
	year = 2009;
	type = ARMORED;

	uniformScript = "\bwi_data_main\configs\factions\cdf_armored\2009\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\cdf_armored\2009\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\cdf_armored\2009\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\cdf_armored\2009\ammo.sqf";

	hiddenElements[] = {"FWT", "RWT", "UWC", "SCU", "HAT"};
	hiddenRoles[]	 = {"HIR", "UAV"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\che_color_g.paa",
		"\bwi_resupply_main\data\che_signs_1.paa",
		"\bwi_resupply_main\data\che_signs_2.paa",
		"\bwi_resupply_main\data\che_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_w.hpp>
};
class Woodland_UN_2009: Faction
{
	name = "TTsKO / Woodland (UN)";
	year = 2009;
	type = ARMORED;

	uniformScript = "\bwi_data_main\configs\factions\cdf_armored\2009\uniform_w_un.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\cdf_armored\2009\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\cdf_armored\2009\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\cdf_armored\2009\ammo.sqf";

	hiddenElements[] = {"FWC", "RWC", "UWC", "SCU", "HAT"};
	hiddenRoles[]	 = {"HIR", "UAV"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\che_color_g.paa",
		"\bwi_resupply_main\data\che_signs_1.paa",
		"\bwi_resupply_main\data\che_signs_2.paa",
		"\bwi_resupply_main\data\che_signs_3.paa"
	};

	#include <motorpool_un.hpp>
	#include <resupply_w.hpp>
};