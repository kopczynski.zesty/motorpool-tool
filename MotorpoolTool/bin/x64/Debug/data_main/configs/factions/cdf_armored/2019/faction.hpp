class Woodland_2019: Faction
{
	name = "TTsKO / Woodland";
	year = 2019;
	type = ARMORED;

	uniformScript = "\bwi_data_main\configs\factions\cdf_armored\2019\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\cdf_armored\2019\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\cdf_armored\2019\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\cdf_armored\2019\ammo.sqf";

	hiddenElements[] = {"FWT", "RWT", "UWC", "SCU"};
	hiddenRoles[]	 = {"UAV"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\che_color_g.paa",
		"\bwi_resupply_main\data\che_signs_1.paa",
		"\bwi_resupply_main\data\che_signs_2.paa",
		"\bwi_resupply_main\data\che_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_w.hpp>
};
class Woodland_UN_2019: Faction
{
	name = "TTsKO / Woodland (UN)";
	year = 2019;
	type = ARMORED;

	uniformScript = "\bwi_data_main\configs\factions\cdf_armored\2019\uniform_w_un.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\cdf_armored\2019\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\cdf_armored\2019\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\cdf_armored\2019\ammo.sqf";

	hiddenElements[] = {"FWC", "RWC", "UWC", "SCU"};
	hiddenRoles[]	 = {"UAV"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\che_color_g.paa",
		"\bwi_resupply_main\data\che_signs_1.paa",
		"\bwi_resupply_main\data\che_signs_2.paa",
		"\bwi_resupply_main\data\che_signs_3.paa"
	};

	#include <motorpool_un.hpp>
	#include <resupply_w.hpp>
};