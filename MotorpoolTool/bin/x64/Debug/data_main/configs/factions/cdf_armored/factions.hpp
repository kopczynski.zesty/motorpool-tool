class CDF_Armored: FactionGroup
{
	name = "Chernarus Defence Force";
	side = BLUFOR;
	
	flag  = "\bwi_data_main\data\flag_cdf.paa";
    icon  = "\bwi_data_main\data\icon_s_cdf.paa";
    image = "\bwi_data_main\data\icon_l_cdf.paa";

	#include <2009\faction.hpp>
	#include <2019\faction.hpp>
};