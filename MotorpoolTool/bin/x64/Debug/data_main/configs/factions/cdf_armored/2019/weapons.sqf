// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary weapon.
switch ( _role ) do {
	default { _unit addWeapon "rhs_weap_vhsd2"; };

	case "plt_ldg";
	case "sqd_ldg";
	case "sqd_gre": { _unit addWeapon "rhs_weap_vhsd2_bg"; };
	case "sqd_lmg": { _unit addWeapon "rhs_weap_m249_pip_L"; };

	case "mmg_gun": { _unit addWeapon "rhs_weap_fnmag"; };

	case "lrc_ldr";
	case "lrc_hir": { _unit addWeapon "rhs_weap_vhsd2_bg"; };
	case "lrc_lmg": { _unit addWeapon "rhs_weap_m249_pip_S"; };
	case "lrc_dmr": { _unit addWeapon "arifle_SPAR_03_blk_F"; };
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_rif": { _unit addWeapon "rhs_weap_vhsk2"; };

	case "spf_lmg": { _unit addWeapon "rhs_weap_m249_pip_L_vfg"; };
	case "spf_dmr": { _unit addWeapon "rhs_weap_SCARH_LB"; };
	case "spf_ldg"; // M320 GLM
	case "spf_ldr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": { _unit addWeapon "rhs_weap_SCARH_STD"; };
	case "spf_bre": { _unit addWeapon "rhs_weap_SCARH_CQC"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addWeapon "rhs_weap_vhsk2"; };

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": { _unit addWeapon "rhs_weap_vhsk2"; };
};


// Secondary weapon.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg";
	case "fwc_pil";
	case "fwt_pil": { _unit addWeapon "rhsusf_weap_glock17g4"; };

	case "spf_ldg": { _unit addWeapon "rhs_weap_M320"; };
	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": {
		_unit addWeapon "rhs_weap_pb_6p9";
		_unit addHandgunItem "rhs_acc_6p9_suppressor";
	};
};


// Launcher.
switch ( _role ) do {
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat";
	case "spf_ldr";
	case "spf_lat": { _unit addWeapon "rhs_weap_rpg75"; };

	case "mat_gun": {
		_unit addWeapon "rhs_weap_rpg7";
		_unit addSecondaryWeaponItem "rhs_acc_pgo7v3";
	};
	case "hat_gun": { _unit addWeapon "rhs_weap_fgm148"; };
	case "aat_gun": { _unit addWeapon "rhs_weap_igla"; };
};


// Backpack weapon.
switch ( _role ) do {
	case "mot_ldr": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "mot_gun": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "dat_ldr": { _unit addBackpack "RHS_Kornet_Tripod_Bag"; };
	case "dat_gun": { _unit addBackpack "RHS_Kornet_Gun_Bag"; };
	case "dgg_ldr": { _unit addBackpack "RHS_AGS30_Tripod_Bag"; };
	case "dgg_gun": { _unit addBackpack "RHS_AGS30_Gun_Bag"; };
	case "lrc_uav": { _unit addBackpack "B_UAV_01_backpack_F"; };
	
	case "sqd_lhg": { [_unit, "rhs_weap_m32", 1] call bwi_armory_fnc_addToBackpack; };
};