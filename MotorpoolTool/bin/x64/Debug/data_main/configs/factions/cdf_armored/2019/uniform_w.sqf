// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on special forces and scuba roles.
if ( (_role select [0,3]) in ["spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhsgref_uniform_ttsko_forest"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit forceAddUniform "rhsgref_uniform_vsr"; };

	case "rwc_pil";
	case "rwt_pil": { _unit forceAddUniform "rhsgref_uniform_ttsko_forest"; };
	
	case "uwc_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "rhsgref_uniform_para_ttsko_oxblood"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "rhsgref_6b27m_ttsko_forest"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		/* No goggles */ 
		_unit addHeadgear "rhs_6b26_bala";
	};

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "rhs_tsh4"; };

	case "rwc_pil": { _unit addHeadgear "rhs_zsh7a_mike_alt"; };
	case "rwt_pil": { _unit addHeadgear "rhs_zsh7a_mike"; };
	
	case "fwc_pil": { _unit addHeadgear "rhs_zsh7a"; };
	case "fwt_pil": { _unit addHeadgear "rhs_zsh7a_mike"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "rhs_6b5_rifleman_khaki"; };

	case "plt_ldr";
	case "log_sgt": { _unit addVest "rhs_6b5_officer_khaki"; };

	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit addVest "rhs_6b5_officer_khaki"; };

	case "med_ldr";
	case "med_cpm": { _unit addVest "rhs_6b5_medic_khaki"; };

	case "sqd_ldr";
	case "sqd_ldg": { _unit addVest "rhs_6b5_officer_khaki"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_rif": { _unit addVest "rhs_6b5_rifleman_ttsko"; };
	case "lrc_dmr": { _unit addVest "rhs_6b5_sniper_ttsko"; };

	case "spf_ldr": { _unit addVest "rhs_6b5_officer_vsr"; };
	case "spf_lat";
	case "spf_lmg";
	case "spf_sap";
	case "spf_bre";
	case "spf_rif": { _unit addVest "rhs_6b5_rifleman_vsr"; };
	case "spf_dmr": { _unit addVest "rhs_6b5_sniper_vsr"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_crew_Oli"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_Oli"; };
	
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "V_TacVest_oli"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "rhs_assault_umbts"; };

	case "plt_ldr": { _unit addBackpack "B_Kitbag_sgg"; };

	case "log_sgt";
	case "tac_fac": { _unit addBackpack "B_FieldPack_oli"; };

	case "spc_dem";
	case "eng_def": { _unit addBackpack "B_CarryAll_oli"; };

	case "mmg_gun": { _unit addBackpack "B_Kitbag_sgg"; };
	case "mmg_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "B_CarryAll_oli"; };

	case "mat_ldr": { _unit addBackpack "B_Kitbag_sgg"; };
	case "mat_gun": { _unit addBackpack "rhs_rpg_empty"; };

	case "sqd_lmg": { _unit addBackpack "B_FieldPack_oli"; };
	case "sqd_amg";
	case "sqd_ahg";
	case "sqd_sap": { _unit addBackpack "B_Kitbag_sgg"; };

	case "how_ldr";
	case "how_gun"; { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_uav": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_rif";
	case "spf_lat";
	case "spf_sap";
	case "spf_dmr": { _unit addBackpack "B_Kitbag_sgg"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};
