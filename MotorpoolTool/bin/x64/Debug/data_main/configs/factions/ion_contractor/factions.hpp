// ION Services
class ION_Contractor: FactionGroup
{
	name = "ION Services";
	side = BLUFOR;
	
	flag  = "\bwi_data_main\data\flag_ion.paa";
    icon  = "\bwi_data_main\data\icon_s_ion.paa";
    image = "\bwi_data_main\data\icon_l_ion.paa";

	#include <2012\faction.hpp>
};