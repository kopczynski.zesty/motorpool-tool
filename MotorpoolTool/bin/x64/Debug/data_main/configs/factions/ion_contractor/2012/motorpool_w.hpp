allowedVehicles[] = {
	"Jeep_W",
	"Jeep_LMG_W",
	"Jeep_SPG9_W",
	"Polaris_ION",
	"SUV_Armored_ION",
	"SUV_Armored_M134_ION",
	"Offroad_ION",
	"Offroad_M2_ION",
	"Iveco_ION",
	"Iveco_Fuel_ION",
	"Iveco_Ammo_ION",
	"Iveco_SBCOP_ION_W",
	"Iveco_HBCOP_ION_W",
	"OH6M_ION",
	"MH6M_ION",
	"AH6M_CAS_ION",
	"AH6M_CAS_AGM_ION",
	"AH6M_CAS_GAU_ION",
	"A29_CAS00_ION"
};

allowedVehiclesRecon[] = {
	"Quadbike",
	"M1030_W",
	"EagleIV_ION",
	"AssaultBoat",
	"RHIB_Small",
	"RHIB_MG"
};

allowedVehiclesSpecial[] = {
	/* None */
};