// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on special forces and scuba roles.
if ( (_role select [0,3]) in ["spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { [_unit, [ "tacs_Uniform_Garment_LS_GS_BP_BB",
						"tacs_Uniform_Garment_LS_GS_TP_TB",
						"tacs_Uniform_Garment_RS_BS_GP_BB",
						"tacs_Uniform_Garment_RS_GS_BP_BB",
						"tacs_Uniform_Garment_RS_TS_GP_BB",
						"tacs_Uniform_Polo_TP_BS_LP_BB_NoLogo",
						"tacs_Uniform_Polo_TP_LS_GP_BB_NoLogo",
						"tacs_Uniform_Polo_TP_GS_TP_TB_NoLogo",
						"tacs_Uniform_Polo_CP_LS_TP_OB",
						"tacs_Uniform_Polo_CP_RS_LP_BB",
						"tacs_Uniform_Polo_TP_TS_GP_BB_NoLogo",
						"tacs_Uniform_Polo_TP_WS_GP_BB_NoLogo"]] call bwi_armory_fnc_addRandomUniform; };

	case "plt_ldr": { _unit forceAddUniform "U_Competitor"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { [_unit, [ "tacs_Uniform_Garment_LS_BS_GP_BB",
								"tacs_Uniform_Garment_LS_GS_BP_BB",
								"tacs_Uniform_Garment_LS_GS_EP_TB",
								"tacs_Uniform_Garment_RS_BS_GP_BB",
								"tacs_Uniform_Garment_RS_GS_BP_BB",
								"tacs_Uniform_Garment_RS_GS_TP_TB"]] call bwi_armory_fnc_addRandomUniform; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit forceAddUniform "UK3CB_CHC_C_U_Overall_01"; };
};


// Helmet.
switch ( _role ) do {
	default { [_unit, [ "H_Cap_oli_hs",
						"H_Cap_oli",
						"H_Cap_blk_ION"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": {
		_unit addGoggles "rhs_googles_black";
		_unit addHeadgear "rhsusf_bowman_cap";
	};

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "H_Cap_headphones"; };

	case "rwc_pil";
	case "rwt_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit addHeadgear "H_PilotHelmetHeli_B"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "tacs_Vest_PlateCarrierFull_Black"; };

	case "plt_ldr";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg": { _unit addVest "V_PlateCarrier1_blk"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_hir";
	case "lrc_rif";
	case "lrc_dmr": { _unit addVest "V_TacVest_blk"; };
	case "lrc_uav": { _unit addVest "V_TacChestrig_grn_F"; };

	case "spf_ldr";
	case "spf_lat";
	case "spf_lmg";
	case "spf_sap";
	case "spf_bre";
	case "spf_rif";
	case "spf_dmr": { _unit addVest "V_PlateCarrier2_blk"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "V_TacChestrig_grn_F"; };

	case "rwc_pil";
	case "rwt_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "V_TacVest_blk"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_AssaultPack_rgr"; };

	case "plt_ldr": { _unit addBackpack "B_Kitbag_rgr"; };
	
	case "log_sgt";
	case "tac_fac": { _unit addBackpack "B_FieldPack_oli"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "tacs_Backpack_Kitbag_Medic_Green"; };

	case "spc_dem";
	case "eng_def": { _unit addBackpack "B_Carryall_oli"; };

	case "mmg_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "B_Carryall_oli"; };
	case "aat_gun";
	case "mat_ldr": { _unit addBackpack "B_Kitbag_rgr"; };
	case "mat_gun": { _unit addBackpack "B_FieldPack_oli"; };

	case "sqd_lmg";
	case "sqd_bre": { _unit addBackpack "B_FieldPack_oli"; };
	case "sqd_amg";
	case "sqd_sap";
	case "lrc_lmg": { _unit addBackpack "B_Kitbag_rgr"; };

	case "how_ldr";
	case "how_gun"; { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_rif";
	case "spf_lat": { _unit addBackpack "B_FieldPack_oli"; };
	case "spf_bre";
	case "spf_sap": { _unit addBackpack "B_Kitbag_rgr"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};