// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary ammo.
switch ( _role ) do {
	default {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 5] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 4] call bwi_armory_fnc_addToVest;
	};

	case "spc_eod": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 2] call bwi_armory_fnc_addToVest;
	};
	case "eng_def": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "sqd_bre": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 4] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "sqd_lmg": {
		[_unit, "rhs_mag_100Rnd_556x45_M855A1_cmag", 2] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_100Rnd_556x45_M855A1_cmag_mixed", 4] call bwi_armory_fnc_addToBackpack;
	};

	case "mmg_ldr": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 4] call bwi_armory_fnc_addToBackpack;
	};
	case "mmg_gun": {
		[_unit, "rhsusf_100Rnd_762x51_m80a1epr", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_100Rnd_762x51_m62_tracer", 2] call bwi_armory_fnc_addToVest;
	};
	case "aat_ldr";
	case "mat_ldr": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "aat_gun";
	case "mat_gun": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 4] call bwi_armory_fnc_addToBackpack;
	};

	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 2] call bwi_armory_fnc_addToVest;
	};

	case "lrc_ldr";
	case "lrc_hir";
	case "lrc_lat";
	case "lrc_rif": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 4] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_lmg": {
		[_unit, "rhs_mag_100Rnd_556x45_M855A1_cmag", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_100Rnd_556x45_M855A1_cmag", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_100Rnd_556x45_M855A1_cmag_mixed", 4] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_dmr": {
		[_unit, "ACE_20Rnd_762x51_M118LR_Mag", 4] call bwi_armory_fnc_addToVest;
		[_unit, "ACE_20Rnd_762x51_Mk316_Mod_0_Mag", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_uav": { };

	case "spf_ldr";
	case "spf_ldg";
	case "spf_bre";
	case "spf_sap";
	case "spf_rif";
	case "spf_lat";
	case "spf_lmg";
	case "spf_dmr";
	case "scu_ldr";
	case "scu_bre";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif"; 
	case "scu_lmg";
	case "scu_dmr": { };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 2] call bwi_armory_fnc_addToVest;
	};

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 2] call bwi_armory_fnc_addToVest;
	};
	case "uwc_pil": { /* No primary */ };
};


// Secondary ammo.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg": {
		[_unit, "rhsusf_mag_17Rnd_9x19_FMJ", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "fwc_pil";
	case "fwt_pil";
	case "uwc_pil": {
		[_unit, "rhsusf_mag_17Rnd_9x19_FMJ", 3] call bwi_armory_fnc_addToVest;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sqd_amg": {
		[_unit, "rhs_mag_100Rnd_556x45_M855A1_cmag", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_100Rnd_556x45_M855A1_cmag_mixed", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "mmg_ldr": {
		[_unit, "rhsusf_100Rnd_762x51_m80a1epr", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_100Rnd_762x51_m62_tracer", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "mat_gun": {
		[_unit, "rhs_mag_maaws_HEAT", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "mat_ldr": {
		[_unit, "rhs_mag_maaws_HEAT", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_maaws_HEDP", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "aat_gun": {
		[_unit, "rhs_fim92_mag", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "aat_ldr": {
		[_unit, "rhs_fim92_mag", 2] call bwi_armory_fnc_addToBackpack;
	};
};


// Other ammo.
switch ( _role ) do {
	case "sqd_bre": {
		[_unit, "rhsusf_8Rnd_00Buck", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhsusf_8Rnd_Slug", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_bre": {
		[_unit, "rhsusf_8Rnd_00Buck", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_8Rnd_Slug", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "scu_bre": {
		[_unit, "rhsusf_5Rnd_00Buck", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_5Rnd_Slug", 2] call bwi_armory_fnc_addToBackpack;
	};
};