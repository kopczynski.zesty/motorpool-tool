class Desert_2012: Faction
{
	name = "Combat / Desert";
	year = 2012;
	type = CONTRACTOR;

	uniformScript = "\bwi_data_main\configs\factions\ion_contractor\2012\uniform_d.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\ion_contractor\2012\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\ion_contractor\2012\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\ion_contractor\2012\ammo.sqf";

	hiddenElements[] = {"UWC", "SCU", "SPF", "HAT", "APC", "IFV", "MBT", "ART", "AAA", "HOW", "FWT" };
	hiddenRoles[]	 = {"UAV", "LHG", "AHG"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\usa_color_t.paa",
		"\bwi_resupply_main\data\usa_signs_1.paa",
		"\bwi_resupply_main\data\usa_signs_2.paa",
		"\bwi_resupply_main\data\usa_signs_3.paa"
	};

	#include <motorpool_d.hpp>
	#include <resupply_d.hpp>
};


class Woodland_2012: Faction
{
	name = "Combat / Woodland";
	year = 2012;
	type = CONTRACTOR;

	uniformScript = "\bwi_data_main\configs\factions\ion_contractor\2012\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\ion_contractor\2012\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\ion_contractor\2012\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\ion_contractor\2012\ammo.sqf";

	hiddenElements[] = {"UWC", "SCU", "SPF", "HAT", "APC", "IFV", "MBT", "ART", "AAA", "HOW", "FWT"};
	hiddenRoles[]	 = {"UAV", "LHG", "AHG"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\usa_color_g.paa",
		"\bwi_resupply_main\data\usa_signs_1.paa",
		"\bwi_resupply_main\data\usa_signs_2.paa",
		"\bwi_resupply_main\data\usa_signs_3.paa"
	};

	#include <motorpool_w.hpp>
	#include <resupply_w.hpp>
};