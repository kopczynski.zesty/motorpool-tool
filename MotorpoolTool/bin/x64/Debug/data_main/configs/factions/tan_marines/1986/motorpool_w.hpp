allowedVehicles[] = {
	"M151_TAN",
	"M151_O_TAN",
	"M151_M2_TAN",
	"M151_TOW_TAN",
	"M939_TAN",
	"M939_O_TAN",
	"M939_Recover_TAN",
	"M939_Fuel_TAN",
	"M939_Ammo_TAN",
	"M939_Repair_TAN",
	"M939_M2_TAN",
	"M939_SBCOP_TAN_W",
	"M113A3_TAN",
	"M113A3_M2_TAN",
	"M113A3E_Mk19_TAN",
	"AAVP71A_TAN",
	"M60A1_TAN",
	"UH1H_TAN",
	"UH1H_MG_TAN",
	"UH1H_MEV_TAN",
	"UH1H_CAS_TAN",
	"O3A_TAN",
	"AssaultBoat",
	"RHIB_Small",
	"Gunboat",
	"RHIB_MG",
	"RHIB_GL"
};

allowedVehiclesRecon[] = {
	"M1030_TAN"
};

allowedVehiclesSpecial[] = {
	/* None. */
};