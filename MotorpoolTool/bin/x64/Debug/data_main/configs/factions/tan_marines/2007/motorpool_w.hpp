allowedVehicles[] = {
	"M151_TAN",
	"M151_O_TAN",
	"M151_M2_TAN",
	"M151_TOW_TAN",
	"M998A1_2D_TAN",
	"M998A1_2DHT_TAN",
	"M998A1_2DFT_TAN",
	"M998A1_4D_TAN",
	"M998A1_4DHT_TAN",
	"M998A1_4DFT_TAN",
	"M1025A1_TAN",
	"M1025A1_M2_TAN",
	"M1025A1_Mk19_TAN",
	"M939_TAN",
	"M939_O_TAN",
	"M939_Recover_TAN",
	"M939_Fuel_TAN",
	"M939_Ammo_TAN",
	"M939_Repair_TAN",
	"M939_M2_TAN",
	"M939_M119_TAN",
	"M939_SBCOP_TAN_W",
	"M939_HBCOP_TAN_W",
	"M113A3_TAN",
	"M113A3_M2_TAN",
	"M113A3E_Mk19_TAN",
	"AAVP71A_TAN",
	"M60A3_TAN",
	"UH1H_TAN",
	"UH1H_MG_TAN",
	"UH1H_MEV_TAN",
	"UH1H_CAS_TAN",
	"O3A_TAN",
	"A29_CAS00_TAN",
	"AssaultBoat",
	"RHIB_Small",
	"Gunboat",
	"RHIB_MG",
	"RHIB_GL"
};

allowedVehiclesRecon[] = {
	"M1030_TAN",
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike"
};