// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhs_uniform_bdu_erdl"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit forceAddUniform "rhsgref_uniform_ERDL"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit forceAddUniform "rhsgref_uniform_og107_erdl"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit forceAddUniform "U_B_Wetsuit"; };

	case "rwc_pil";
	case "rwt_pil";
	case "uwc_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "rhsgref_uniform_og107_erdl"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "rhsgref_helmet_pasgt_erdl_rhino"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		_unit addGoggles "UK3CB_G_Neck_Shemag_Tan";
		_unit addHeadgear "UK3CB_CW_US_B_EARLY_H_BoonieHat_ERDL_02";
	};

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addGoggles "G_B_Diving"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "rhsusf_cvc_green_alt_helmet"; };

	case "rwc_pil": { _unit addHeadgear "rhsusf_hgu56p_visor_green"; };
	case "rwt_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit addHeadgear "rhsusf_hgu56p_green"; };
	case "uwc_pil": { _unit addHeadgear "H_Beret_gen_F"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "rhsgref_TacVest_ERDL"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addVest "rhsgref_alice_webbing"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addVest "V_RebreatherB"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "V_TacVest_oli"; };
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "UK3CB_V_Pilot_Vest"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_green_F"; };

	case "plt_ldr";
	case "tac_fac": { _unit addBackpack "B_Kitbag_sgg"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "tacs_Backpack_Kitbag_Medic_Sage"; };

	case "sqd_amg";
	case "sqd_lhg";
	case "sqd_ahg": { _unit addBackpack "B_Kitbag_sgg"; };

	case "spc_dem";
	case "eng_def": { _unit addBackpack "rhsgref_hidf_alicepack"; };

	case "mmg_gun": { _unit addBackpack "B_Kitbag_sgg"; };
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "rhsgref_hidf_alicepack"; };

	case "how_ldr";
	case "how_gun": { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_uav": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_rif": { _unit addBackpack "B_FieldPack_green_F"; };
	case "spf_bre";
	case "spf_sap";
	case "spf_lat": { _unit addBackpack "B_Kitbag_sgg"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack";  };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};