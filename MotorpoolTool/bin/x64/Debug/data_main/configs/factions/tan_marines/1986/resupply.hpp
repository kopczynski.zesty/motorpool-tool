allowedSupplies[] = {
	"Mag_8Rnd_M1Garand",
	"Box_30Rnd_BrenLMG",
	"Mag_30Rnd_M3A1",
	
	"Gren_Hand_HE_M67",
	"Gren_Hand_WS_ANM8",
	"Gren_Hand_CS_M18",
	"Gren_40mm_HE_M441",
	"Gren_40mm_WS_M714",
	"Gren_40mm_CS_M713",
	"Gren_40mm_WF_M585",
	"Gren_40mm_CF_M661",
	
	"AT_Tube_M72",
	"Medical_Bandages",
	"Medical_Medication",
	"Medical_Blood",
	"Medical_Bodybags",
	"Explosive_M112",
	"Explosive_M183",
	"Explosive_M6SLAM",
	"Explosive_M26",
	"Explosive_M15",
	"Explosive_M18A1",
	"Explosive_PMR3M",
	"Explosive_PMR3F",
	
	"DMG_M2_Low",
	"DMG_M2_High",

	"Spare_Track",
	"Spare_Wheel",
	"Jerry_Can",

	"BaseVehicleResupplyWoodWest",
	"MortarSandbagWood",
	"BunkerSandbagWood",
	"BunkerSandbagLargeWood",
	"SquadSandbagWoodWest",
	"SquadSandbagTallWoodWest",
	"CheckpointSandbagWoodWest",
	"VehicleTrenchWoodland",
	"SquadTrench90WoodWest",
	"SquadTrench180WoodWest",
	"SquadTrench360WoodWest",
	"VehicleTrenchCamoWoodlandWest",
	"VehicleTrenchLargeWoodlandWest",
	"PortableLightKit",
	"TallLightKit",
	"Shelter2",
	"Shelter4",
	"TankTrap4",
	"TankTrap8"
};

allowedSuppliesWeapons[] = {
	"Box_100Rnd_M13",
	"AA_1Rnd_FIM92"
};

allowedSuppliesRecon[] = {
	"Mag_20Rnd_M14",
};

allowedSuppliesSpecial[] = {
	"Mag_30Rnd_Mk262"
};
