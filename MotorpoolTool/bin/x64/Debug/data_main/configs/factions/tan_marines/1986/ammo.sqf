// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary ammo.
switch ( _role ) do {
	default {
		[_unit, "rhsgref_8Rnd_762x63_M2B_M1rifle", 12] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_8Rnd_762x63_M2B_M1rifle", 4] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsgref_8Rnd_762x63_Tracer_M1T_M1rifle", 12] call bwi_armory_fnc_addToBackpack;
	};

	case "spc_eod";
	case "spc_dem": {
		[_unit, "rhsgref_8Rnd_762x63_M2B_M1rifle", 12] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_8Rnd_762x63_Tracer_M1T_M1rifle", 10] call bwi_armory_fnc_addToBackpack;
	};
	
	case "eng_mec";
	case "aat_ldr";
	case "aat_gun": {
		[_unit, "rhsgref_8Rnd_762x63_M2B_M1rifle", 16] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_8Rnd_762x63_Tracer_M1T_M1rifle", 12] call bwi_armory_fnc_addToBackpack;
	};
	case "eng_def": {
		[_unit, "rhsgref_8Rnd_762x63_M2B_M1rifle", 8] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_8Rnd_762x63_Tracer_M1T_M1rifle", 8] call bwi_armory_fnc_addToVest;
	};

	case "sqd_lmg": {
		[_unit, "UK3CB_Bren_30Rnd_762x51_Magazine_R", 5] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_Bren_30Rnd_762x51_Magazine_R", 5] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_Bren_30Rnd_762x51_Magazine_RT", 10] call bwi_armory_fnc_addToBackpack;
	};
	
	case "mmg_ldr": {
		[_unit, "rhsgref_8Rnd_762x63_M2B_M1rifle", 8] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_8Rnd_762x63_M2B_M1rifle", 4] call bwi_armory_fnc_addToVest;
	};
	case "mmg_gun": {
		[_unit, "UK3CB_100Rnd_762x51_B_M60", 1] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_100Rnd_762x51_B_M60", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_100Rnd_762x51_T_M60", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "mat_ldr": {
		[_unit, "rhsgref_8Rnd_762x63_M2B_M1rifle", 16] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_8Rnd_762x63_Tracer_M1T_M1rifle", 10] call bwi_armory_fnc_addToBackpack;
	};

	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": {
		[_unit, "rhsgref_8Rnd_762x63_M2B_M1rifle", 8] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_8Rnd_762x63_Tracer_M1T_M1rifle", 8] call bwi_armory_fnc_addToVest;
	};

	case "lrc_ldr";
	case "lrc_hir";
	case "lrc_lat";
	case "lrc_rif": {
		[_unit, "UK3CB_20Rnd_762x51_B_M14", 5] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_20Rnd_762x51_T_M14", 4] call bwi_armory_fnc_addToVest;
	};
	case "lrc_lmg": {
		[_unit, "UK3CB_Bren_30Rnd_762x51_Magazine_R", 5] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_Bren_30Rnd_762x51_Magazine_R", 5] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_Bren_30Rnd_762x51_Magazine_RT", 10] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_dmr": {
		[_unit, "UK3CB_20Rnd_762x51_B_M14", 5] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_20Rnd_762x51_T_M14", 4] call bwi_armory_fnc_addToVest;
	};
	case "lrc_uav": { };

	case "spf_ldr";
	case "spf_ldg";
	case "spf_bre";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": {
		[_unit, "rhsgref_30rnd_1143x23_M1911B_SMG", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_1143x23_M1911B_SMG", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsgref_30rnd_1143x23_M1T_SMG", 4] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_lmg": {
		[_unit, "UK3CB_Bren_30Rnd_762x51_Magazine_R", 11] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_Bren_30Rnd_762x51_Magazine_RT", 11] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_dmr": {
		[_unit, "UK3CB_20Rnd_762x51_B_M14", 6] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_20Rnd_762x51_T_M14", 2] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_20Rnd_762x51_T_M14", 4] call bwi_armory_fnc_addToBackpack;
	};

	case "scu_ldr";
	case "scu_bre";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif"; 
	case "scu_lmg";
	case "scu_dmr": { };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, "rhsgref_30rnd_1143x23_M1911B_SMG", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_1143x23_M1T_SMG", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_1143x23_M1T_SMG", 1] call bwi_armory_fnc_addToUniform;
	};

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": {
		[_unit, "rhsgref_30rnd_1143x23_M1911B_SMG", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_1143x23_M1T_SMG", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_1143x23_M1T_SMG", 1] call bwi_armory_fnc_addToUniform;
	};
	case "uwc_pil": { /* No primary */ };
};


// Secondary ammo.
switch ( _role ) do {
	case "plt_ldr": {
		[_unit, "rhsusf_mag_7x45acp_MHP", 2] call bwi_armory_fnc_addToUniform;
		[_unit, "rhsusf_mag_7x45acp_MHP", 1] call bwi_armory_fnc_addToVest;
	};
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg": {
		[_unit, "rhsusf_mag_7x45acp_MHP", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "fwc_pil";
	case "fwt_pil";
	case "uwc_pil": {
		[_unit, "rhsusf_mag_7x45acp_MHP", 3] call bwi_armory_fnc_addToVest;
	};

	case "spf_ldr";
	case "spf_ldg";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": {
		[_unit, "rhsusf_mag_7x45acp_MHP", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "scu_lmg";
	case "scu_dmr";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif": {
		[_unit, "rhsusf_mag_7x45acp_MHP", 2] call bwi_armory_fnc_addToUniform;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sqd_amg": {
		[_unit, "UK3CB_Bren_30Rnd_762x51_Magazine_R", 8] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_Bren_30Rnd_762x51_Magazine_RT", 8] call bwi_armory_fnc_addToBackpack;
	};

	case "mmg_ldr": {
		[_unit, "UK3CB_100Rnd_762x51_B_M60", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_100Rnd_762x51_T_M60", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "mat_gun": {
		[_unit, "rhs_mag_maaws_HEAT", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "mat_ldr": {
		[_unit, "rhs_mag_maaws_HEAT", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_maaws_HEDP", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "aat_gun": {
		[_unit, "rhs_fim92_mag", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "aat_ldr": {
		[_unit, "rhs_fim92_mag", 2] call bwi_armory_fnc_addToBackpack;
	};
};


// Other ammo.
/* None */