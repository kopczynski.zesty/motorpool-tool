allowedSupplies[] = {
	"Mag_20Rnd_G3",
	"Box_50Rnd_G3SG1",
	"Gren_Hand_HE_RGD5",
	"Gren_Hand_WS_RDG2",
	"Gren_Hand_CS_NSPD",
	"Gren_Hand_CS_NSPN",
	"Gren_40mm_HE_M441",
	"Gren_40mm_WS_M714",
	"Gren_40mm_CS_M713",
	"Gren_40mm_WF_M585",
	"Gren_40mm_CF_M661",
	"AT_Tube_RPG26",
	"Medical_Bandages",
	"Medical_Medication",
	"Medical_Blood",
	"Medical_Bodybags",
	"Explosive_TNT400",
	"Explosive_IED_Small",
	"Explosive_IED_Large",
	
	"DMG_Kord_Low",
	"DMG_Kord_High",

	"Spare_Track",
	"Spare_Wheel",
	"Jerry_Can",

	"BaseVehicleResupplyDesertEast",
	"BunkerBarricadeSmall",
	"BunkerBarricadeMedium",
	"BunkerBarricadeLarge",
	"CheckpointBarricadeEast",
	"SquadBarricadeDesertEast",
	"SquadBarricadeTallDesertEast",
	"VehicleTrenchDesert",
	"SquadTrench90DesertEast",
	"SquadTrench180DesertEast",
	"SquadTrench360DesertEast",
	"VehicleTrenchCamoDesertEast",
	"VehicleTrenchLargeDesertEast",
	"PortableLightKit"
};

allowedSuppliesWeapons[] = {
	"Box_250Rnd_MG3",
	"AT_1Rnd_PG7V",
	"AT_1Rnd_PG7VL",
	"AT_1Rnd_OG7V",
	"AT_1Rnd_TBG7V",
	"AA_1Rnd_9K38"
};

allowedSuppliesRecon[] = {
	"Mag_10Rnd_7N1",
	"Mag_10Rnd_7N14"
};

allowedSuppliesSpecial[] = {
	/* None */
};