allowedSupplies[] = {
	"Mag_20Rnd_G3",
	"Box_50Rnd_G3SG1",
	"Gren_Hand_HE_RGD5",
	"Gren_Hand_WS_RDG2",
	"Gren_Hand_CS_NSPD",
	"Gren_Hand_CS_NSPN",
	"AT_Tube_RPG26",
	"Medical_Bandages",
	"Medical_Medication",
	"Medical_Blood",
	"Medical_Bodybags",
	"Explosive_TNT400",
	"Explosive_IED_Small",
	"Explosive_IED_Large",
	
	"DMG_DShKM_Low",
	"DMG_DShKM_High",

	"Spare_Track",
	"Spare_Wheel",
	"Jerry_Can",

	"BaseVehicleResupplyDesertEast",
	"BunkerBarricadeSmall",
	"BunkerBarricadeMedium",
	"BunkerBarricadeLarge",
	"CheckpointBarricadeEast",
	"SquadBarricadeDesertEast",
	"SquadBarricadeTallDesertEast",
	"VehicleTrenchDesert",
	"SquadTrench90DesertEast",
	"SquadTrench180DesertEast",
	"SquadTrench360DesertEast",
	"VehicleTrenchCamoDesertEast",
	"VehicleTrenchLargeDesertEast",
	"PortableLightKit"
};

allowedSuppliesWeapons[] = {
	"Box_250Rnd_MG3",
	"AT_1Rnd_PG7V",
	"AT_1Rnd_PG7VL",
	"AT_1Rnd_OG7V",
	"AA_1Rnd_9K38"
};

allowedSuppliesRecon[] = {
	"Mag_10Rnd_7N14",
	"Mag_10Rnd_7N1"
};

allowedSuppliesSpecial[] = {
	/* None */
};