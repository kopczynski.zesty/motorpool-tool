class TKM_Insurgents: FactionGroup
{
	name = "Takistan Militia";
	side = INDEP;
	
	flag  = "\bwi_data_main\data\flag_tkm.paa";
    icon  = "\bwi_data_main\data\icon_s_tkm.paa";
    image = "\bwi_data_main\data\icon_l_tkm.paa";

	#include <2008\faction.hpp>
};

class TKR_Insurgents: FactionGroup
{
	name = "Takistan Rebels";
	side = INDEP;
	
	flag  = "\bwi_data_main\data\flag_tkm.paa";
    icon  = "\bwi_data_main\data\icon_s_tkm.paa";
    image = "\bwi_data_main\data\icon_l_tkm.paa";

	#include <2018\faction.hpp>
};