// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit; // Remove goggles to prevent clipping with shemags.

// Remove goggles on light recon roles.
//if ( (_role select [0,3]) in ["lrc"] ) then {
//	removeGoggles _unit;
//};

//
//
//

// Uniform.
switch ( _role ) do {
	default { [_unit, [ "rhsgref_uniform_woodland_olive",
						"rhsgref_uniform_altis_lizard_olive",
						"rhsgref_uniform_dpm_olive",
						"UK3CB_TKM_O_U_06_C",
						"UK3CB_TKM_B_U_06_B",
						"UK3CB_TKM_B_U_06_C",
						"UK3CB_TKM_I_U_06",
						"U_I_C_Soldier_Bandit_2_F",
						"U_I_C_Soldier_Bandit_3_F",
						"U_BG_Guerilla2_1",
						"U_BG_Guerilla2_2",
						"U_BG_Guerilla2_3"
						]] call bwi_armory_fnc_addRandomUniform; };

	case "log_sgt";
	case "med_ldr";
	case "tac_jtc";
	case "tac_fac";
	case "mmg_ldr";
	case "mat_ldr";
	case "aat_ldr";
	case "mot_ldr";
	case "how_ldr";
	case "dat_ldr";
	case "dgg_ldr";
	case "sqd_ldr";
	case "sqd_ldg": { [_unit, [ "UK3CB_TKM_O_U_06_C",
								"UK3CB_TKM_B_U_06_B",
								"UK3CB_TKM_B_U_06_C",
								"UK3CB_TKM_I_U_06",
								"rhsgref_uniform_woodland_olive",
								"rhsgref_uniform_altis_lizard_olive",
								"rhsgref_uniform_dpm_olive"
								]] call bwi_armory_fnc_addRandomUniform; };

	case "lrc_ldr";
	case "lrc_hir";
	case "lrc_lat";
	case "lrc_rif";
	case "lrc_lmg": { [_unit, [ "UK3CB_TKM_O_U_06",
								"UK3CB_TKM_B_U_06"]] call bwi_armory_fnc_addRandomUniform; };
};


// Helmet.
switch ( _role ) do {
	default { [_unit, [ "H_Shemag_olive_hs",
						"UK3CB_H_Shemag_tan",
						"H_ShemagOpen_tan",
						"H_ShemagOpen_khk",
						"UK3CB_H_Shemag_blk",
						"UK3CB_H_Shemag_grey",
						"UK3CB_H_Shemag_red"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, ["rhs_tsh4", "rhs_tsh4_ess"]] call bwi_armory_fnc_addRandomHeadgear;
	};
};


// Vest.
switch ( _role ) do {
	default {
		if ( "UK3CB_TKM" in (uniform _unit)) then {
			// Avoids combinations of vests that would clip with uniform.
			_unit addVest "UK3CB_V_Pouch";
		} else {
			[_unit, [ "V_TacVest_oli",
					  "V_TacVest_brn",
					  "V_TacVest_khk",
					  "V_TacVest_blk",
					  "V_I_G_resistanceLeader_F",
					  "V_TacVest_camo"]] call bwi_armory_fnc_addRandomVest;
		};
	};
	
	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun";
	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, [ "V_TacChestrig_cbr_F",
				  "V_TacChestrig_grn_F",
				  "V_TacChestrig_oli_F"]] call bwi_armory_fnc_addRandomVest;
	};
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_cbr"; };

	case "plt_ldr";
	case "log_sgt";
	case "eng_mec": { _unit addBackpack "B_Kitbag_cbr"; };

	case "spc_dem";
	case "eng_def";
	case "mmg_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "UK3CB_B_Alice_K"; };
	case "mat_gun": { _unit addBackpack "rhs_rpg_empty"; };
	case "mat_ldr";
	case "mmg_gun": { _unit addBackpack "B_Kitbag_cbr"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "tacs_Backpack_Kitbag_Medic_Coyote"; };

	case "sqd_amg": { _unit addBackpack "UK3CB_B_Alice_K"; };
	case "sqd_lmg";
	case "sqd_ldg";
	case "sqd_gre";
	case "sqd_sap": { _unit addBackpack "B_Kitbag_cbr"; };

	case "how_ldr";
	case "how_gun"; { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };
};
