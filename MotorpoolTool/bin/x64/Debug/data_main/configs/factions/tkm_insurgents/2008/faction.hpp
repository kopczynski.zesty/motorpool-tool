class Desert_2008: Faction
{
	name = "Tribal Fighter / Desert";
	year = 2008;
	type = INSURGENTS;

	uniformScript = "\bwi_data_main\configs\factions\tkm_insurgents\2008\uniform_d.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\tkm_insurgents\2008\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\tkm_insurgents\2008\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\tkm_insurgents\2008\ammo.sqf";

	hiddenElements[] = {"FWT", "FWC", "RWC", "RWT", "TAC", "UWC", "SCU", "SPF", "HAT", "ART"};
	hiddenRoles[]	 = {"HIR", "UAV", "EOD", "LDG", "GRE", "BRE", "LHG", "AHG"};

	acreRadioDevices[] = {"NONE", "ACRE_SEM52SL", "ACRE_SEM52SL", "ACRE_PRC77"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\rus_color_k.paa",
		"\bwi_resupply_main\data\tkru_signs_1.paa",
		"\bwi_resupply_main\data\tkru_signs_2.paa",
		"\bwi_resupply_main\data\tkru_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_d.hpp>
};