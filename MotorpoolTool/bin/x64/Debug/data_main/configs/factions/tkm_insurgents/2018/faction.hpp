class Desert_2018: Faction
{
	name = "Rebel / Desert";
	year = 2018;
	type = INSURGENTS;

	uniformScript = "\bwi_data_main\configs\factions\tkm_insurgents\2018\uniform_d.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\tkm_insurgents\2018\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\tkm_insurgents\2018\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\tkm_insurgents\2018\ammo.sqf";

	hiddenElements[] = {"FWT", "FWC", "RWC", "RWT", "TAC", "UWC", "SCU", "SPF", "HAT", "ART"};
	hiddenRoles[]	 = {"HIR", "UAV", "EOD", "BRE", "LHG", "AHG"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\usa_color_t.paa",
		"\bwi_resupply_main\data\tkus_signs_1.paa",
		"\bwi_resupply_main\data\tkus_signs_2.paa",
		"\bwi_resupply_main\data\tkus_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_d.hpp>
};