// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit; // Override facewear.


// Uniform.
switch ( _role ) do {
	default { [_unit, [ "UK3CB_TKM_O_U_01",
						"UK3CB_TKM_O_U_03",
						"UK3CB_TKM_O_U_04",
						"UK3CB_TKM_O_U_05",
						"UK3CB_TKM_B_U_01",
						"UK3CB_TKM_B_U_03",
						"UK3CB_TKM_B_U_04",
						"UK3CB_TKM_B_U_05",
						"UK3CB_TKM_I_U_01",
						"UK3CB_TKM_I_U_03",
						"UK3CB_TKM_I_U_03_B",
						"UK3CB_TKM_I_U_04",
						"UK3CB_TKM_I_U_05",
						"UK3CB_TKM_I_U_03",
						"UK3CB_TKM_I_U_03",
						"UK3CB_TKM_I_U_03"]] call bwi_armory_fnc_addRandomUniform; };
	case "plt_ldr";
	case "log_sgt";
	case "sqd_ldr";
	case "sqd_ldg": { [_unit, [ "UK3CB_TKM_O_U_06",
								"UK3CB_TKM_B_U_06",
								"UK3CB_TKM_I_U_06"]] call bwi_armory_fnc_addRandomUniform; };
};


// Helmet.
switch ( _role ) do {
	default { [_unit, [ "UK3CB_TKC_H_Turban_04_1",
						"UK3CB_TKC_H_Turban_05_1",
						"UK3CB_TKM_O_H_Turban_01_1",
						"UK3CB_TKM_O_H_Turban_02_1",
						"UK3CB_TKM_B_H_Turban_01_1",
						"UK3CB_TKM_B_H_Turban_02_1",
						"UK3CB_TKM_I_H_Turban_01_1",
						"UK3CB_TKM_I_H_Turban_02_2",
						"UK3CB_TKM_I_H_Turban_02_1"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { [_unit, [	"UK3CB_TKC_H_Turban_05_1",
								"UK3CB_TKM_O_H_Turban_02_1",
								"UK3CB_TKM_I_H_Turban_02_1"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, ["rhs_tsh4", "rhs_tsh4_ess"]] call bwi_armory_fnc_addRandomHeadgear;
	};
};


// Facewear.
switch ( _role ) do {
	default { [_unit, [ "G_Bandanna_blk",
						"G_Bandanna_khk",
						"G_Bandanna_oli",
						"UK3CB_G_Face_Wrap_01",
						"UK3CB_G_Neck_Shemag_Tan",
						"rhs_scarf",
						"rhsusf_shemagh2_tan",
						"rhsusf_shemagh2_od",
						"rhsusf_shemagh_tan"]] call bwi_armory_fnc_addRandomGoggles; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { [_unit, [ "UK3CB_G_Neck_Shemag_Tan",
								"G_Bandanna_khk",
								"G_Bandanna_oli",
								"G_Bandanna_blk"]] call bwi_armory_fnc_addRandomGoggles; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_V_Pouch"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_cbr"; };

	case "spc_dem";
	case "eng_def";
	case "mmg_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "UK3CB_B_Alice_K"; };
	case "mat_gun": { _unit addBackpack "rhs_rpg_empty"; };
	case "mat_ldr";
	case "sqd_lmg";
	case "sqd_amg";
	case "mmg_gun": { _unit addBackpack "B_Kitbag_cbr"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "UK3CB_TKC_C_B_Sidor_MED"; };

	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };
};