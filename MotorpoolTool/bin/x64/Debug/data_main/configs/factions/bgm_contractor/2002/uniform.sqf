// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on light recon, special forces, and scuba roles.
if ( (_role select [0,3]) in ["lrc", "spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhsgref_uniform_tigerstripe"; };
};

// Helmet.
switch ( _role ) do {
	default { [_unit, [ "H_Cap_oli_hs",
						"H_Bandanna_khk_hs",
						"H_Watchcap_camo",
						"UK3CB_BAF_H_Wool_Hat"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": {
		_unit addGoggles "G_Bandanna_oli";
		_unit addHeadgear "H_Cap_oli_hs";
	};

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "H_Cap_headphones"; };

	case "rwc_pil";
	case "rwt_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit addHeadgear "H_PilotHelmetHeli_B"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "V_TacVest_oli"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "V_TacChestrig_oli_F"; };

	case "rwc_pil";
	case "rwt_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "V_TacVest_oli"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_AssaultPack_rgr"; };

	case "plt_ldr": { _unit addBackpack "B_Kitbag_rgr"; };
	
	case "log_sgt";
	case "tac_fac": { _unit addBackpack "B_FieldPack_oli"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "tacs_Backpack_Kitbag_Medic_Green"; };

	case "spc_dem";
	case "eng_def": { _unit addBackpack "B_Carryall_oli"; };

	case "mmg_ldr";
	case "mmg_gun";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "B_Carryall_oli"; };
	case "aat_gun";
	case "mat_ldr": { _unit addBackpack "B_Kitbag_rgr"; };
	case "mat_gun": { _unit addBackpack "B_FieldPack_oli"; };

	case "sqd_lmg";
	case "sqd_bre": { _unit addBackpack "B_FieldPack_oli"; };
	case "sqd_amg";
	case "sqd_sap";
	case "lrc_lmg": { _unit addBackpack "B_Kitbag_rgr"; };

	case "how_ldr";
	case "how_gun"; { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_rif";
	case "spf_lat": { _unit addBackpack "B_FieldPack_oli"; };
	case "spf_bre";
	case "spf_sap": { _unit addBackpack "B_Kitbag_rgr"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};