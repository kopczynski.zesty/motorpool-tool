// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary weapon.
switch ( _role ) do {
	default { _unit addWeapon "arifle_TRG21_F"; };

	case "mmg_gun": { _unit addWeapon "rhs_weap_mg42"; };

	case "plt_ldg";
	case "sqd_ldg";
	case "sqd_gre": { _unit addWeapon "arifle_TRG21_GL_F"; };
	
	case "sqd_lmg": { _unit addWeapon "LMG_Zafir_F"; };

	case "lrc_ldr";
	case "lrc_hir": { _unit addWeapon "arifle_TRG21_GL_F"; };
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_rif": { _unit addWeapon "arifle_TRG20_F"; };
	case "lrc_lmg": { _unit addWeapon "LMG_Zafir_F"; };
	case "lrc_dmr": { _unit addWeapon "rhs_weap_svds"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addWeapon "arifle_TRG20_F"; };

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": { _unit addWeapon "arifle_TRG20_F"; };
};


// Secondary weapon.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg";
	case "fwc_pil";
	case "fwt_pil": { _unit addWeapon "rhsusf_weap_glock17g4"; };
};


// Launcher.
switch ( _role ) do {
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat": { _unit addWeapon "rhs_weap_rpg26"; };

	case "mat_gun": {
		_unit addWeapon "rhs_weap_rpg7";
		_unit addSecondaryWeaponItem "rhs_acc_pgo7v3";
	};
	case "aat_gun": { _unit addWeapon "rhs_weap_igla"; };
};


// Backpack weapon.
switch ( _role ) do {
	case "mot_ldr": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "mot_gun": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "dat_ldr": { _unit addBackpack "RHS_SPG9_Tripod_Bag"; };
	case "dat_gun": { _unit addBackpack "RHS_SPG9_Gun_Bag"; };
	case "dgg_ldr": { _unit addBackpack "RHS_AGS30_Tripod_Bag"; };
	case "dgg_gun": { _unit addBackpack "RHS_AGS30_Gun_Bag"; };
	case "lrc_uav": { _unit addBackpack "I_UAV_01_backpack_F"; };
};