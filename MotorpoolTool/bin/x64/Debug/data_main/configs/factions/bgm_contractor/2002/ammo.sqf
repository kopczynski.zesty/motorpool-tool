// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary ammo.
switch ( _role ) do {
	default {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 5] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 4] call bwi_armory_fnc_addToVest;
	};

	case "spc_eod": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 2] call bwi_armory_fnc_addToVest;
	};
	case "eng_def": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "sqd_bre": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 4] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "sqd_lmg": {
		[_unit, "rhssaf_100rnd_556x45_EPR_G36", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_100rnd_556x45_EPR_G36", 5] call bwi_armory_fnc_addToBackpack;
	};

	case "mmg_ldr": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 4] call bwi_armory_fnc_addToBackpack;
	};
	case "mmg_gun": {
		[_unit, "rhsgref_50Rnd_792x57_SmE_drum", 2] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_296Rnd_792x57_SmK_alltracers_belt", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "aat_ldr";
	case "mat_ldr": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 2] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "aat_gun";
	case "mat_gun": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 4] call bwi_armory_fnc_addToBackpack;
	};

	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 2] call bwi_armory_fnc_addToVest;
	};

	case "lrc_ldr";
	case "lrc_hir";
	case "lrc_lat";
	case "lrc_rif": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 4] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_lmg": {
		[_unit, "rhssaf_100rnd_556x45_EPR_G36", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_100rnd_556x45_EPR_G36", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhssaf_100rnd_556x45_EPR_G36", 4] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_dmr": {
		[_unit, "rhs_10Rnd_762x54mmR_7N1", 8] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_10Rnd_762x54mmR_7N14", 7] call bwi_armory_fnc_addToVest;
	};
	case "lrc_uav": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 4] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 3] call bwi_armory_fnc_addToVest;
	};

	case "spf_ldr";
	case "spf_ldg";
	case "spf_bre";
	case "spf_sap";
	case "spf_rif";
	case "spf_lat";
	case "spf_lmg";
	case "spf_dmr";
	case "scu_ldr";
	case "scu_bre";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif"; 
	case "scu_lmg";
	case "scu_dmr": { };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 2] call bwi_armory_fnc_addToVest;
	};

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 2] call bwi_armory_fnc_addToVest;
	};
};


// Secondary ammo.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg": {
		[_unit, "rhsusf_mag_17Rnd_9x19_FMJ", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "fwc_pil";
	case "fwt_pil": {
		[_unit, "rhsusf_mag_17Rnd_9x19_FMJ", 3] call bwi_armory_fnc_addToVest;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sqd_amg": {
		[_unit, "rhssaf_100rnd_556x45_EPR_G36", 4] call bwi_armory_fnc_addToBackpack;
	};

	case "mmg_ldr": {
		[_unit, "rhsgref_296Rnd_792x57_SmK_alltracers_belt", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "mat_gun": {
		[_unit, "rhs_rpg7_PG7VL_mag", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "mat_ldr": {
		[_unit, "rhs_rpg7_PG7VL_mag", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_rpg7_PG7V_mag", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_rpg7_OG7V_mag", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "aat_gun": {
		[_unit, "rhs_mag_9k38_rocket", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "aat_ldr": {
		[_unit, "rhs_mag_9k38_rocket", 2] call bwi_armory_fnc_addToBackpack;
	};
};


// Other ammo.
/* None */