allowedSupplies[] = {
	"Mag_30Rnd_M855A1",
	"Box_150Rnd_Negev",
	"Gren_Hand_HE_M67",
	"Gren_Hand_WS_ANM8",
	"Gren_Hand_CS_M18",
	"Gren_40mm_HE_M441",
	"Gren_40mm_WS_M714",
	"Gren_40mm_CS_M713",
	"Gren_40mm_WF_M585",
	"Gren_40mm_CF_M661",
	"AT_Tube_RPG26",
	"AT_Tube_RSHG2",
	"Medical_Bandages",
	"Medical_Medication",
	"Medical_Blood",
	"Medical_Bodybags",
	"Explosive_M112",
	"Explosive_M183",
	"Explosive_M6SLAM",
	"Explosive_M26",
	"Explosive_M15",
	"Explosive_M18A1",
	"Explosive_PMR3M",
	"Explosive_PMR3F",
	
	"DMG_Kord_Low",
	"DMG_Kord_High",

	"Spare_Track",
	"Spare_Wheel",
	"Jerry_Can",

	"BaseVehicleResupplyWoodEast",
	"MortarSandbagWood",
	"BunkerSandbagWood",
	"BunkerSandbagLargeWood",
	"SquadSandbagWoodEast",
	"SquadSandbagTallWoodEast",
	"CheckpointSandbagWoodEast",
	"BunkerBarricadeSmall",
	"BunkerBarricadeMedium",
	"BunkerBarricadeLarge",
	"CheckpointBarricadeEast",
	"SquadBarricadeWoodEast",
	"SquadBarricadeTallWoodEast",
	"VehicleTrenchWoodland",
	"SquadTrench90WoodEast",
	"SquadTrench180WoodEast",
	"SquadTrench360WoodEast",
	"VehicleTrenchCamoWoodlandEast",
	"VehicleTrenchLargeWoodlandEast",
	"PortableLightKit",
	"TankTrap4",
	"TankTrap8"
};

allowedSuppliesWeapons[] = {
	"Mag_50Rnd_MG42",
	"Mag_296Rnd_MG42",
	"AT_1Rnd_PG7VR",
	"AT_1Rnd_PG7VL",
	"AT_1Rnd_OG7V",
	"AT_1Rnd_TBG7V",
	"AA_1Rnd_9K38"
};

allowedSuppliesRecon[] = {
	"Mag_10Rnd_7N14",
	"Mag_10Rnd_7N1"
};

allowedSuppliesSpecial[] = {
	/* None */
};