allowedVehicles[] = {
	"Jeep_W",
	"Jeep_LMG_W",
	"Jeep_SPG9_W",
	"Hilux_BGM",
	"Hilux_O_BGM",
	"Hilux_PKM_BGM",
	"Hilux_DSHKM_BGM",
	"Hilux_AGS30_BGM",
	"Hilux_Rocket_BGM",
	"Hilux_Artillery_BGM",
	"Iveco_Fuel_BGM",
	"Iveco_Ammo_BGM",
	"KrAZ255B1_BGM",
	"KrAZ255B1_FB_BGM",
	"KrAZ255B1_SBCOP_BGM_W",
	"KrAZ255B1_BBCOP_BGM_W",
	"MTLB_INS_W",
	"MTLB_ZU23_INS_W",
	"AssaultBoat",
	"RHIB_Small",
	"RHIB_MG"
};

allowedVehiclesRecon[] = {
	"M1030_W",
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	/* None */
};