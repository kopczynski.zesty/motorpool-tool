class Desert_2000: Faction
{
	name = "Tiger Stripe / Desert";
	year = 2002;
	type = CONTRACTOR;

	uniformScript = "\bwi_data_main\configs\factions\bgm_contractor\2002\uniform.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\bgm_contractor\2002\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\bgm_contractor\2002\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\bgm_contractor\2002\ammo.sqf";

	hiddenElements[] = {"UWC", "SCU", "SPF", "HAT", "IFV", "MBT", "ART", "AAA", "HOW", "RWC", "RWT", "FWT", "FWC", "TAC"};
	hiddenRoles[]	 = {"UAV", "HIR", "LHG", "AHG"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\nor_color_b.paa",
		"\bwi_resupply_main\data\usa_signs_1.paa",
		"\bwi_resupply_main\data\usa_signs_2.paa",
		"\bwi_resupply_main\data\usa_signs_3.paa"
	};

	#include <motorpool_d.hpp>
	#include <resupply_d.hpp>
};


class Woodland_2000: Faction
{
	name = "Tiger Stripe / Woodland";
	year = 2002;
	type = CONTRACTOR;

	uniformScript = "\bwi_data_main\configs\factions\bgm_contractor\2002\uniform.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\bgm_contractor\2002\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\bgm_contractor\2002\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\bgm_contractor\2002\ammo.sqf";

	hiddenElements[] = {"UWC", "SCU", "SPF", "HAT", "IFV", "MBT", "ART", "AAA",  "HOW", "RWC", "RWT", "FWT", "FWC", "TAC"};
	hiddenRoles[]	 = {"UAV", "HIR", "LHG", "AHG"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\nor_color_b.paa",
		"\bwi_resupply_main\data\usa_signs_1.paa",
		"\bwi_resupply_main\data\usa_signs_2.paa",
		"\bwi_resupply_main\data\usa_signs_3.paa"
	};

	#include <motorpool_w.hpp>
	#include <resupply_w.hpp>
};