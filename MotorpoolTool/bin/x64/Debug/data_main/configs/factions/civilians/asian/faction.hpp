class Asian: Faction
{
    name = "Asian";
    year = 2020;
    type = REGULARS;

    uniformScript = "\bwi_data_main\configs\factions\civilians\asian\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\civilians\asian\weapons.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\civilians\asian\ammo.sqf";
};