// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Uniform.
[_unit, ["tacs_Uniform_Polo_CP_BS_TP_BB_NoLogo", "tacs_Uniform_Polo_TP_LS_GP_BB_NoLogo", "tacs_Uniform_Polo_TP_GS_TP_TB_NoLogo"]] call bwi_armory_fnc_addRandomUniform;

// Helmet.
[_unit, ["UK3CB_CHC_C_H_Can_Cap", "H_Cap_oli", "H_Cap_red", "H_Cap_usblack"]] call bwi_armory_fnc_addRandomHeadgear;

// Vest.
// No vest.