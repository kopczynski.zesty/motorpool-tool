class European: Faction
{
    name = "European";
    year = 2020;
    type = REGULARS;

    uniformScript = "\bwi_data_main\configs\factions\civilians\european\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\civilians\european\weapons.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\civilians\european\ammo.sqf";
};