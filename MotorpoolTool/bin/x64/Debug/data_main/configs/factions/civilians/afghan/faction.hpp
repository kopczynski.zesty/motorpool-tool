class Afghan: Faction
{
    name = "Afghan";
    year = 2020;
    type = REGULARS;

    uniformScript = "\bwi_data_main\configs\factions\civilians\afghan\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\civilians\afghan\weapons.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\civilians\afghan\ammo.sqf";
};