class Civilians: FactionGroup
{
    name = "Civilians";
    side = CIVIL;
	
	flag  = "\bwi_data_main\data\flag_un.paa";
    icon  = "\bwi_data_main\data\icon_s_un.paa";
    image = "\bwi_data_main\data\icon_l_un.paa";

    #include <afghan\faction.hpp>
    #include <american\faction.hpp>
    #include <asian\faction.hpp>
    #include <european\faction.hpp>
    #include <russian\faction.hpp>
};