// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Uniform.
[_unit, ["UK3CB_TKC_C_U_01", "UK3CB_TKC_C_U_02", "UK3CB_TKC_C_U_03", "UK3CB_TKC_C_U_06"]] call bwi_armory_fnc_addRandomUniform;

// Helmet.
[_unit, ["UK3CB_TKM_O_H_Turban_03_1", "UK3CB_TKM_O_H_Turban_04_1", "UK3CB_TKM_O_H_Turban_01_1", "UK3CB_TKM_B_H_Turban_02_1"]] call bwi_armory_fnc_addRandomHeadgear;

// Vest.
// No vest.