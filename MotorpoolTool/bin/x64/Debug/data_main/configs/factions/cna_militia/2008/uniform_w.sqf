// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on all units for National Army headgear (prevents clipping).
removeGoggles _unit;

// Remove goggles on special forces and scuba roles.
//if ( (_role select [0,3]) in ["spf", "scu"] ) then {
//	removeGoggles _unit;
//};


// Uniform.
switch ( _role ) do {
	default { [_unit, [ "rhsgref_uniform_flecktarn",
						"rhsgref_uniform_woodland_olive",
						"rhsgref_uniform_dpm_olive",
						"rhsgref_uniform_altis_lizard_olive",
						"UK3CB_CCM_I_U_WOOD_02",
						"UK3CB_CCM_I_U_WOOD_03",
						"UK3CB_CCM_I_U_WOOD_04",
						"UK3CB_CCM_I_U_COACH_02",
						"UK3CB_CCM_I_U_COACH_03",
						"UK3CB_CCM_I_U_COACH_04"]] call bwi_armory_fnc_addRandomUniform; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { [_unit, [ "rhsgref_uniform_flecktarn",
						"rhsgref_uniform_woodland_olive",
						"rhsgref_uniform_dpm_olive",
						"rhsgref_uniform_altis_lizard_olive"]] call bwi_armory_fnc_addRandomUniform; };
};


// Helmet.
switch ( _role ) do {
	default { [_unit, [ "rhs_6b28_green",
						"rhs_6b28_green_bala",
						"rhs_6b7_1m",
						"rhs_6b7_1m_bala2",
						"H_Cap_oli",
						"H_Cap_grn",
						"H_Cap_blk",
						"H_Watchcap_blk",
						"H_Watchcap_cbr",
						"H_Watchcap_camo",
						"H_Watchcap_khk",
						"H_Beret_blk",
						"rhs_beanie_green"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addGoggles "rhsusf_shemagh2_od"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { [_unit, ["rhs_tsh4", "rhs_tsh4_bala"]] call bwi_armory_fnc_addRandomHeadgear; };
};


// Vest.
switch ( _role ) do {
	default { [_unit, [ "V_Chestrig_oli", "V_Chestrig_rgr", "V_Chestrig_blk"]] call bwi_armory_fnc_addRandomVest; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { [_unit, [ "V_TacVest_oli", "V_TacVest_camo", "V_TacVest_blk", "V_I_G_resistanceLeader_F"]] call bwi_armory_fnc_addRandomVest; };
};


// Backpack.
switch ( _role ) do {
	default { [_unit, [ "B_FieldPack_khk", "B_FieldPack_oli", "B_FieldPack_cbr"]] call bwi_armory_fnc_addRandomBackpack; };

	case "plt_ldr";
	case "log_sgt": { [_unit, [ "B_Kitbag_rgr", "B_Kitbag_sgg", "B_Kitbag_cbr"]] call bwi_armory_fnc_addRandomBackpack; };

	case "spc_dem";
	case "eng_def";
	case "mmg_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { [_unit, [ "UK3CB_B_Hiker", "UK3CB_B_Hiker_Camo"]] call bwi_armory_fnc_addRandomBackpack; };
	case "mat_gun": { _unit addBackpack "rhs_rpg_empty"; };
	case "mat_ldr";
	case "mmg_gun": { [_unit, [ "B_Kitbag_rgr", "B_Kitbag_sgg", "B_Kitbag_cbr"]] call bwi_armory_fnc_addRandomBackpack; };

	case "med_ldr";
	case "med_cpm": { [_unit, [ "tacs_Backpack_Kitbag_Medic_Green", "tacs_Backpack_Kitbag_Medic_Sage", "tacs_Backpack_Kitbag_Medic_Coyote"]] call bwi_armory_fnc_addRandomBackpack; };

	case "sqd_amg";
	case "sqd_sap";
	case "sqd_lmg": { [_unit, [ "B_Kitbag_rgr", "B_Kitbag_sgg", "B_Kitbag_cbr"]] call bwi_armory_fnc_addRandomBackpack; };

	case "how_ldr";
	case "how_gun"; { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_uav": { /* Backpack weapon */ };
	case "lrc_lmg": { [_unit, [ "B_Kitbag_rgr", "B_Kitbag_sgg", "B_Kitbag_cbr"]] call bwi_armory_fnc_addRandomBackpack; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };
};