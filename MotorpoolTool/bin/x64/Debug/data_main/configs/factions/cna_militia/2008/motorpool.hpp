allowedVehicles[] = {
	"Offroad_INS",
	"SkodaS1203_INS",
	"Offroad_Repair_INS",
	"Datsun_INS",
	"Datsun_O_INS",
	"Datsun_PKM_INS",
	"Hilux_INS",
	"Hilux_O_INS",
	"Hilux_PKM_INS",
	"Hilux_DSHKM_INS",
	"Hilux_AGS30_INS",
	"Hilux_SPG9_INS",
	"Hilux_Rocket_INS",
	"Hilux_Artillery_INS",
	"PragaV3S_INS_W",
	"PragaV3S_O_INS_W",
	"PragaV3S_Recover_INS_W",
	"PragaV3S_Fuel_INS_W",
	"PragaV3S_Repair_INS_W",
	"PragaV3S_Ammo_INS_W",
	"PragaV3S_ZU23_INS_W",
	"PragaV3S_D30A_INS_W",
	"PragaV3S_TCOP_INS_W",
	"PragaV3S_SBCOP_INS_W",
	"PragaV3S_BBCOP_INS_W",
	"Ikarus_INS_W",
	"BTR70_CNA",
	"T3485M_INS_W"
};

allowedVehiclesRecon[] = {
	"Yava250N_CNA"
};

allowedVehiclesSpecial[] = {
	/* None. */
};