allowedSupplies[] = {
	"Mag_30Rnd_SaVz58",
	"Box_75Rnd_RPK",
	"Gren_Hand_HE_RGD5",
	"Gren_Hand_WS_RDG2",
	"Gren_Hand_CS_NSPD",
	"Gren_Hand_CS_NSPN",
	"AT_Tube_RPG26",
	"Medical_Bandages",
	"Medical_Medication",
	"Medical_Blood",
	"Medical_Bodybags",
	"Explosive_TNT400",
	"Explosive_IED_Small",
	"Explosive_IED_Large",
	
	"DMG_DShKM_Low",
	"DMG_DShKM_High",

	"Spare_Track",
	"Spare_Wheel",
	"Jerry_Can",

	"BaseVehicleResupplyWoodEast",
	"MortarSandbagWood",
	"BunkerSandbagWood",
	"BunkerSandbagLargeWood",
	"SquadSandbagWoodEast",
	"SquadSandbagTallWoodEast",
	"CheckpointSandbagWoodEast",
	"BunkerBarricadeSmall",
	"BunkerBarricadeMedium",
	"BunkerBarricadeLarge",
	"CheckpointBarricadeEast",
	"SquadBarricadeWoodEast",
	"SquadBarricadeTallWoodEast",
	"VehicleTrenchWoodland",
	"SquadTrench90WoodEast",
	"SquadTrench180WoodEast",
	"SquadTrench360WoodEast",
	"VehicleTrenchCamoWoodlandEast",
	"VehicleTrenchLargeWoodlandEast",
	"PortableLightKit",
	"TankTrap4",
	"TankTrap8"
};

allowedSuppliesWeapons[] = {
	"Box_100Rnd_57N323S",
	"Box_100Rnd_7N13",
	"AT_1Rnd_PG7V",
	"AT_1Rnd_PG7VL",
	"AT_1Rnd_OG7V",
	"AA_1Rnd_9K38"
};

allowedSuppliesRecon[] = {
	"Mag_10Rnd_M70"
};

allowedSuppliesSpecial[] = {
	/* None */
};