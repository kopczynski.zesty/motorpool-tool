// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on light recon, special forces, and scuba roles.
if ( (_role select [0,3]) in ["lrc", "spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default  { _unit forceAddUniform "rhs_uniform_gorka_r_y"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit forceAddUniform "rhs_uniform_gorka_1_a"; };
};


// Helmet.
switch ( _role ) do {
	default { [_unit, [ "rhs_altyn"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "sqd_bre": {
		_unit addHeadgear "rhs_altyn_visordown";
		removeGoggles _unit; // Prevent clipping
	};

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": {
		_unit addGoggles "rhs_balaclava";
		_unit addHeadgear "rhs_altyn_novisor_ess";
	};

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "rhs_tsh4"; };

	case "rwc_pil";
	case "rwt_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit addHeadgear "H_PilotHelmetHeli_O"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_TKA_O_V_6b23_ml_Oli_02"; };

	case "plt_ldr";
	case "log_sgt";
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit addVest "UK3CB_TKA_O_V_6b23_ML_6sh92_radio_Oli"; };

	case "med_ldr";
	case "med_cpm": { _unit addVest "UK3CB_TKA_O_V_6b23_medic_Oli"; };

	case "sqd_ldr";
	case "sqd_ldg": { _unit addVest "UK3CB_TKA_O_V_6b23_ML_6sh92_radio_Oli"; };
	case "sqd_gre": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_6sh92_vog_Oli"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_hir";
	case "lrc_rif";
	case "lrc_dmr": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_Oli_02"; };
	case "lrc_uav": { _unit addVest "UK3CB_V_Chestrig_TKA_OLI"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_crew_Oli"; };

	case "rwc_pil";
	case "rwt_pil";
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_crew_Oli"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_ViperLightHarness_khk_F"; };

	case "plt_ldr": { _unit addBackpack "B_Kitbag_rgr"; };



	case "spc_dem";
	case "eng_def": { _unit addBackpack "B_Carryall_oli"; };

	case "mmg_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "B_Carryall_oli"; };
	case "aat_gun";
	case "mat_ldr": { _unit addBackpack "B_Kitbag_rgr"; };
	case "mat_gun": { _unit addBackpack "B_FieldPack_oli"; };

	case "sqd_amg";
	case "lrc_lmg": { _unit addBackpack "B_Kitbag_rgr"; };

	case "how_ldr";
	case "how_gun"; { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};