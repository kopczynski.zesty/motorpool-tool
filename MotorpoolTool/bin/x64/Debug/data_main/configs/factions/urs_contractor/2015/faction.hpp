class Woodland_2015: Faction
{
	name = "Gorka / Woodland";
	year = 2015;
	type = CONTRACTOR;

	uniformScript = "\bwi_data_main\configs\factions\urs_contractor\2015\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\urs_contractor\2015\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\urs_contractor\2015\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\urs_contractor\2015\ammo.sqf";

	hiddenElements[] = {"UWC", "SCU", "SPF", "HAT", "ART", "FWT", "FWC"};
	hiddenRoles[]	 = {"HIR", "LHG", "AHG"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\rus_color_k.paa",
		"\bwi_resupply_main\data\rus_signs_1.paa",
		"\bwi_resupply_main\data\rus_signs_2.paa",
		"\bwi_resupply_main\data\rus_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_w.hpp>
};