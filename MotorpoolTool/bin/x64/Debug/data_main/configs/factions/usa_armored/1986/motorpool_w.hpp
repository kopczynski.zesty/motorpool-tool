allowedVehicles[] = {
	"Willys_CW",
	"M1025A2_CW",
	"M1025A2_M2_CW",
	"M1025A2_Mk19_CW",
	"M1045A2_TOW_CW",
	"M1097A2_2D_CW",
	"M1097A2_4D_CW",
	"M939_CW",
	"M939_O_CW",
	"M939_Recover_CW",
	"M939_Fuel_CW",
    "M939_Ammo_CW",
	"M939_Repair_CW",
	"M939_M2_CW",
	"M939_SBCOP_CW",
	"M977A4_W",
	"M978A4_Fuel_W",
	"M977A4_Ammo_W",
	"M977A4_Repair_W",
	"M113A3_CW",
	"M113A3_M240_CW",
	"M113A3_M2_CW",
	"M113A3E_Mk19_CW",
	"M113A3_Medical_CW",
	"M113A3_Ammo_CW",
	"M2A2_W",
	"M1A1_CW",
	"M109_CW",
	"AH64",
	"A10A_CAS90",
	"F16C_CAS90",
	"F16C_CAP90"
};

allowedVehiclesRecon[] = {
	"M1030_W",
	"OH6M"
};

allowedVehiclesSpecial[] = {
	"AssaultBoat",
	"RHIB_Small",
	"OH6M",
	"MH6M",
	"AH6M_CAS",
	"AH6M_CAS_AGM",
	"AH6M_CAS_GAU"
};