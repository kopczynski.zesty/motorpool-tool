// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary weapon.
switch ( _role ) do {
	default { _unit addWeapon "rhs_weap_m4a1_carryhandle"; };

	case "plt_ldg";
	case "sqd_ldg": { _unit addWeapon "rhs_weap_m4a1_carryhandle_m203"; };

	case "sqd_gre": { _unit addWeapon "rhs_weap_m4a1_carryhandle_m203"; };
	case "sqd_lmg": { _unit addWeapon "rhs_weap_m249_pip_L"; };

	case "mmg_gun": { _unit addWeapon "rhs_weap_m240B"; };

	case "lrc_ldr";
	case "lrc_hir": { _unit addWeapon "rhs_weap_m4a1_m203s_wd"; };
	case "lrc_lmg": { _unit addWeapon "rhs_weap_m249_pip_S"; };
	case "lrc_dmr": { _unit addWeapon "rhs_weap_m14ebrri"; };
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_rif": { _unit addWeapon "rhs_weap_m4a1_wd"; };

	case "spf_lmg": { _unit addWeapon "rhs_weap_m249_pip_L"; };
	case "spf_dmr": { _unit addWeapon "rhs_weap_XM2010_sa"; }; 
	case "spf_ldg"; // M320 GLM
	case "spf_ldr";
	case "spf_bre";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": { _unit addWeapon "rhs_weap_hk416d145_wd"; }; 

	case "scu_lmg": { _unit addWeapon "rhs_weap_m249_pip_S"; };
	case "scu_dmr": { _unit addWeapon "rhs_weap_XM2010"; };
	case "scu_ldr"; // M320 GLM
	case "scu_bre";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif": { _unit addWeapon "rhsusf_weap_MP7A2"; }; 

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addWeapon "rhs_weap_m4a1_carryhandle"; };

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": { _unit addWeapon "rhs_weap_m4a1_carryhandle"; };
	case "uwc_pil": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg";
	case "fwc_pil";
	case "fwt_pil";
	case "uwc_pil": { _unit addWeapon "rhsusf_weap_m9"; };

	case "spf_ldg": { _unit addWeapon "rhs_weap_M320"; };
	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif";
	
	case "scu_ldr": { _unit addWeapon "rhs_weap_M320"; };
	case "scu_lmg";
	case "scu_dmr";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif": {
		_unit addWeapon "hgun_Pistol_heavy_01_F";
		_unit addHandgunItem "muzzle_snds_acp";
	};
};


// Launcher.
switch ( _role ) do {
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat";
	case "spf_ldr";
	case "spf_lat";
	case "scu_lat": { _unit addWeapon "rhs_weap_M136"; };

	case "mat_gun": {
		_unit addWeapon "rhs_weap_maaws";
		_unit addSecondaryWeaponItem "rhs_optic_maaws";
	};
	case "hat_gun": { _unit addWeapon "rhs_weap_fgm148"; };
	case "aat_gun": { _unit addWeapon "rhs_weap_fim92"; };
};


// Backpack weapon.
switch ( _role ) do {
	case "mot_ldr": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "mot_gun": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "dat_ldr": { _unit addBackpack "rhs_TOW_Tripod_Bag"; };
	case "dat_gun": { _unit addBackpack "rhs_Tow_Gun_Bag"; };
	case "dgg_ldr": { _unit addBackpack "RHS_Mk19_Tripod_Bag"; };
	case "dgg_gun": { _unit addBackpack "RHS_Mk19_Gun_Bag"; };
	case "lrc_uav": { _unit addBackpack "B_UAV_01_backpack_F"; };

	case "sqd_bre";
	case "spf_bre": { [_unit, "rhs_weap_M590_8RD", 1] call bwi_armory_fnc_addToBackpack; };
	case "scu_bre": { [_unit, "rhs_weap_M590_5RD", 1] call bwi_armory_fnc_addToBackpack; };
};