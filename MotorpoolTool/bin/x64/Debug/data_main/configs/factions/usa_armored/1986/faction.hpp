class Woodland_1986: Faction
{
	name = "ERDL / Woodland";
	year = 1986;
	type = ARMORED;

	uniformScript = "\bwi_data_main\configs\factions\usa_armored\1986\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\usa_armored\1986\weapons_w.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\usa_armored\1986\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\usa_armored\1986\ammo_w.sqf";

	hiddenElements[] = {"SCU", "MAT", "HAT", "UWC", "AAA"};
	hiddenRoles[]	 = {"HIR", "UAV", "LHG", "AHG"};

	acreRadioDevices[] = {"NONE", "NONE", "ACRE_SEM52SL", "ACRE_PRC77"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\usa_color_g.paa",
		"\bwi_resupply_main\data\usa_signs_1.paa",
		"\bwi_resupply_main\data\usa_signs_2.paa",
		"\bwi_resupply_main\data\usa_signs_3.paa"
	};

	#include <motorpool_w.hpp>
	#include <resupply_w.hpp>
};