// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on special forces and scuba roles.
if ( (_role select [0,3]) in ["spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhs_uniform_FROG01_d"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit forceAddUniform "rhs_uniform_FROG01_d"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit forceAddUniform "rhs_uniform_FROG01_d"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit forceAddUniform "U_B_Wetsuit"; };

	case "rwc_pil";
	case "rwt_pil";
	case "uwc_pil": { _unit forceAddUniform "rhs_uniform_FROG01_d"; };
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "rhs_uniform_g3_rgr"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "rhsusf_lwh_helmet_marpatd"; };
	
	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addHeadgear "rhs_booniehat2_marpatd"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		_unit addGoggles "rhsusf_shemagh2_tan";
		_unit addHeadgear "rhsusf_mich_helmet_marpatd_norotos_arc_headset";
	};

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addGoggles "G_B_Diving"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "rhsusf_cvc_ess"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addHeadgear "rhsusf_hgu56p_visor"; };
	case "fwc_pil": { _unit addHeadgear "RHS_jetpilot_usaf"; };
	case "fwt_pil": { _unit addHeadgear "rhsusf_hgu56p"; };
	case "uwc_pil": { _unit addHeadgear "rhsusf_lwh_helmet_marpatd"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "rhsusf_spc_rifleman"; };

	case "plt_ldr";
	case "log_sgt": { _unit addVest "rhsusf_spc_squadleader"; };

	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit addVest "rhsusf_spc_teamleader"; };

	case "med_ldr";
	case "med_cpm": { _unit addVest "rhsusf_spc_corpsman"; };	

	case "sqd_ldr";
	case "sqd_ldg": { _unit addVest "rhsusf_spc_squadleader"; };
	case "sqd_lmg": { _unit addVest "rhsusf_spc_mg"; };
	case "sqd_gre";
	case "sqd_lhg": { _unit addVest "rhsusf_spc_teamleader"; };

	case "mmg_gun": { _unit addVest "rhsusf_spc_mg"; };

	case "lrc_dmr": { _unit addVest "rhsusf_spc_marksman"; };

	case "spf_ldr": { _unit addVest "rhsusf_mbav_grenadier"; };
	case "spf_lmg": { _unit addVest "rhsusf_mbav_mg"; };
	case "spf_lat": { _unit addVest "rhsusf_mbav_medic"; };
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit addVest "rhsusf_mbav_rifleman"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addVest "V_RebreatherB"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "rhsusf_spc_crewman"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "rhsusf_spc_light"; };
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "V_TacVest_oli"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "rhsusf_assault_eagleaiii_coy"; };

	case "plt_ldr": { _unit addBackpack "B_Kitbag_cbr"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "B_Kitbag_cbr"; };

	case "spc_dem";
	case "eng_def": { _unit addBackpack "B_Carryall_cbr"; };

	case "sqd_amg";
	case "sqd_ahg": { _unit addBackpack "B_Kitbag_cbr"; };

	case "mmg_ldr": { _unit addBackpack "B_Kitbag_cbr";  };
	case "mat_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "B_Carryall_cbr"; };

	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_uav": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_rif": { _unit addBackpack "rhsusf_assault_eagleaiii_coy"; };
	case "spf_lat";
	case "spf_sap";
	case "spf_bre": { _unit addBackpack "B_Kitbag_cbr"; };

	case "scu_ldr";
	case "scu_lmg";
	case "scu_lat";
	case "scu_sap";
	case "scu_bre";
	case "scu_dmr";
	case "scu_rif": { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};