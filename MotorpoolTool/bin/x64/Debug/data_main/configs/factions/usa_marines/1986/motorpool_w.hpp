allowedVehicles[] = {
	"Willys_CW",
	"M151_CW",
	"M151_O_CW",
	"M151_M2_CW",
	"M151_TOW_CW",
	"M1025A2_CW",
	"M1025A2_M2_CW",
	"M1025A2_Mk19_CW",
	"M1045A2_TOW_CW",
	"M1097A2_2D_CW",
	"M1097A2_4D_CW",
	"M939_CW",
	"M939_O_CW",
	"M939_Recover_CW",
	"M939_Fuel_CW",
	"M939_Ammo_CW",
	"M939_Repair_CW",
	"M939_M2_CW",
	"M939_SBCOP_CW",
	"AAVP71A_W",
	"M1A1_CW",
	"UH1H",
	"UH1H_MG",
	"UH1H_MEV",
	"AH1W",
	"AV8B_H2_CAS90",
	"AV8B_H2_CAP90",
	"AV8B_H2_SEAD90",
	"F14A_CAP80",
	"F14A_CAS80",
	"AssaultBoat",
	"RHIB_Small"
};

allowedVehiclesRecon[] = {
	"M1030_USMC_W"
};

allowedVehiclesSpecial[] = {
	/* None. */
};