class Desert_2012: Faction
{
    name = "MARPAT-D / Desert";
    year = 2012;
    type = MARINES;

    uniformScript = "\bwi_data_main\configs\factions\usa_marines\2012\uniform_d.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\usa_marines\2012\weapons_d.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\usa_marines\2012\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\usa_marines\2012\ammo_d.sqf";

    hiddenElements[] = {"HAT", "IFV", "AAA"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\usa_color_t.paa",
        "\bwi_resupply_main\data\usd_signs_1.paa",
        "\bwi_resupply_main\data\usd_signs_2.paa",
        "\bwi_resupply_main\data\usd_signs_3.paa"
    };

    #include <motorpool_d.hpp>
    #include <resupply_d.hpp>
};


class Woodland_2012: Faction
{
    name = "MARPAT-W / Woodland";
    year = 2012;
    type = MARINES;

    uniformScript = "\bwi_data_main\configs\factions\usa_marines\2012\uniform_w.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\usa_marines\2012\weapons_w.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\usa_marines\2012\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\usa_marines\2012\ammo_w.sqf";

    hiddenElements[] = {"HAT", "IFV", "AAA"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\usa_color_g.paa",
        "\bwi_resupply_main\data\usa_signs_1.paa",
        "\bwi_resupply_main\data\usa_signs_2.paa",
        "\bwi_resupply_main\data\usa_signs_3.paa"
    };

    #include <motorpool_w.hpp>
    #include <resupply_w.hpp>
};