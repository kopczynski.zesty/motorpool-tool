// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary weapon.
switch ( _role ) do {
	default { _unit addWeapon "rhs_weap_m27iar"; };

	case "plt_ldg";
	case "sqd_ldg": { _unit addWeapon "rhs_weap_m4a1_m203"; };

	case "sqd_gre": { _unit addWeapon "rhs_weap_m4a1_m203"; };
	case "sqd_lmg": { _unit addWeapon "rhs_weap_m27iar"; };

	case "mmg_gun": { _unit addWeapon "rhs_weap_m240B"; };

	case "lrc_ldr";
	case "lrc_hir": { _unit addWeapon "rhs_weap_mk18_m320"; };
	case "lrc_lmg": { _unit addWeapon "rhs_weap_m27iar"; };
	case "lrc_dmr": { _unit addWeapon "rhs_weap_sr25_ec_wd"; };
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_rif": { _unit addWeapon "rhs_weap_mk18_wd"; };

	case "spf_lmg": { _unit addWeapon "rhs_weap_m249_pip_L_vfg"; };
	case "spf_dmr": { _unit addWeapon "rhs_weap_SCARH_LB"; }; 
	case "spf_ldr";
	case "spf_ldg";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": { _unit addWeapon "rhs_weap_SCARH_STD"; };
	case "spf_bre": { _unit addWeapon "rhs_weap_SCARH_CQC"; }; 

	case "scu_lmg": { _unit addWeapon "rhs_weap_m249_pip_S_vfg"; };
	case "scu_dmr": { _unit addWeapon "rhs_weap_sr25_ec"; };
	case "scu_ldr": { _unit addWeapon "rhs_weap_mk18_m320"; };
	case "scu_bre";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif": { _unit addWeapon "rhs_weap_mk18"; }; 

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addWeapon "rhs_weap_m4a1_carryhandle"; };

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": { _unit addWeapon "rhs_weap_m4a1_carryhandle"; };
	case "uwc_pil": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg";
	case "fwc_pil";
	case "fwt_pil";
	case "uwc_pil": { _unit addWeapon "rhsusf_weap_m9"; };

	case "spf_ldg": { _unit addWeapon "rhs_weap_M320"; };
	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif";

	case "scu_ldr";
	case "scu_lmg";
	case "scu_dmr";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif": {
		_unit addWeapon "rhsusf_weap_glock17g4";
		_unit addHandgunItem "rhsusf_acc_omega9k";
	};
};


// Launcher.
switch ( _role ) do {
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat";
	case "spf_ldr";
	case "spf_lat";
	case "scu_lat": { _unit addWeapon "rhs_weap_M136"; };

	case "mat_gun": {
		_unit addWeapon "rhs_weap_smaw_green";
		_unit addSecondaryWeaponItem "rhs_weap_optic_smaw";
	};
	case "aat_gun": { _unit addWeapon "rhs_weap_fim92"; };
};


// Backpack weapon.
switch ( _role ) do {
	case "mot_ldr": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "mot_gun": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "dat_ldr": { _unit addBackpack "rhs_TOW_Tripod_Bag"; };
	case "dat_gun": { _unit addBackpack "rhs_Tow_Gun_Bag"; };
	case "dgg_ldr": { _unit addBackpack "RHS_Mk19_Tripod_Bag"; };
	case "dgg_gun": { _unit addBackpack "RHS_Mk19_Gun_Bag"; };	
	case "lrc_uav": { _unit addBackpack "B_UAV_01_backpack_F"; };

	case "sqd_lhg": { [_unit, "rhs_weap_m32", 1] call bwi_armory_fnc_addToBackpack; };
	case "sqd_bre";
	case "spf_bre": { [_unit, "rhs_weap_M590_8RD", 1] call bwi_armory_fnc_addToBackpack; };
	case "scu_bre": { [_unit, "rhs_weap_M590_5RD", 1] call bwi_armory_fnc_addToBackpack; };
};