class USA_Marines: FactionGroup
{
    name = "US Marine Corps";
    side = BLUFOR;
	
	flag  = "\bwi_data_main\data\flag_usa.paa";
    icon  = "\bwi_data_main\data\icon_s_usa.paa";
    image = "\bwi_data_main\data\icon_l_usa.paa";

    #include <1986\faction.hpp>
    #include <2004\faction.hpp>
    #include <2012\faction.hpp>
    #include <2020\faction.hpp>
};