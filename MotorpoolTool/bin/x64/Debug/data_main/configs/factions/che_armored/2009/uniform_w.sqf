// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on all units for Chedaki headgear (prevents clipping).
removeGoggles _unit;

// Remove goggles on special forces and scuba roles.
//if ( (_role select [0,3]) in ["spf", "scu"] ) then {
//	removeGoggles _unit;
//};


// Uniform.
switch ( _role ) do {
	default { [_unit, ["UK3CB_CCM_I_U_COM_01", "rhsgref_uniform_reed", "rhsgref_uniform_specter"]] call bwi_armory_fnc_addRandomUniform; };

	case "eng_def";
	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { _unit forceAddUniform "rhsgref_uniform_reed"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit forceAddUniform "rhsgref_uniform_specter"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { _unit forceAddUniform "rhs_uniform_gorka_r_y"; };

	case "rwc_pil";
	case "rwt_pil": { _unit forceAddUniform "rhsgref_uniform_specter"; };
	
	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit forceAddUniform "rhsgref_uniform_specter"; };
};


// Helmet.
switch ( _role ) do {
	default { [_unit, [ "", // Balaclava added later
						"rhsgref_patrolcap_specter",
						"rhs_beanie_green",
						"H_Watchcap_khk"]] call bwi_armory_fnc_addRandomHeadgear; 
				if ((headgear _unit) == "") then { _unit addGoggles "rhs_balaclava"; };
			};

	case "plt_ldr": { _unit addHeadgear "UK3CB_H_Civ_Beret"; };
	case "log_sgt": { _unit addHeadgear "H_Watchcap_camo"; };

	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit addHeadgear "rhsgref_patrolcap_specter"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addHeadgear "rhs_6b27m_green_bala"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_dmr";
	case "spf_rif": {
		_unit addGoggles "rhs_balaclava";
		_unit addHeadgear "rhs_altyn";
	};
	case "spf_bre": {
		_unit addGoggles "rhs_balaclava";
		_unit addHeadgear "rhs_altyn_visordown";
	};

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { [_unit, ["rhs_tsh4", "rhs_tsh4_bala"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "rwc_pil";
	case "rwt_pil": { 
		_unit addHeadgear "rhs_zsh7a_mike_green"; 
		_unit addGoggles "UK3CB_G_Balaclava_Neck_Shemag"
	};
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "rhs_6sh92"; };

	case "plt_ldr": { _unit addVest "rhs_6sh92_vog_headset"; };
	case "log_sgt": { _unit addVest "rhs_6sh92_radio"; };

	case "mmg_ldr": { _unit addVest "V_Chestrig_oli"; };
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr": { _unit addVest "rhs_6sh92_radio"; };

	case "eng_def";
	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { _unit addVest "V_Chestrig_oli"; };

	case "med_ldr";
	case "med_cpm": { _unit addVest "rhs_6sh92_radio"; };

	case "sqd_ldr": { _unit addVest "rhs_6sh92_headset"; };
	case "sqd_ldg": { _unit addVest "rhs_6sh92_vog_headset"; };
	case "sqd_gre": { _unit addVest "rhs_6sh92_vog"; };

	case "lrc_ldr": { _unit addVest "rhs_6sh92_radio"; };
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addVest "rhs_6sh92"; };

	case "spf_ldr": { _unit addVest "UK3CB_TKA_O_V_6b23_ML_6sh92_radio_Oli"; };
	case "spf_lat": { _unit addVest "UK3CB_TKA_O_V_6b23_medic_Oli"; };
	case "spf_dmr";
	case "spf_lmg";
	case "spf_sap";
	case "spf_bre";
	case "spf_rif": { _unit addVest "UK3CB_TKA_O_V_6b23_ml_Oli_02"; };

	case "mbt_cmd";
	case "ifv_cmd";
	case "apc_cmd";
	case "art_cmd";
	case "mbt_gun";
	case "ifv_gun";
	case "apc_gun";
	case "art_gun";
	case "mbt_drv";
	case "ifv_drv";
	case "apc_drv";
	case "art_drv": { _unit addVest "rhs_6sh92"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "rhs_6sh92"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_Kitbag_rgr"; };

	case "spc_dem";
	case "eng_def": { [_unit, [ "UK3CB_B_Hiker", "UK3CB_B_Hiker_Camo"]] call bwi_armory_fnc_addRandomBackpack; };

	case "mmg_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { [_unit, [ "UK3CB_B_Hiker", "UK3CB_B_Hiker_Camo"]] call bwi_armory_fnc_addRandomBackpack; };

	case "mat_ldr": { _unit addBackpack "B_Kitbag_rgr"; };
	case "mat_gun": { _unit addBackpack "rhs_rpg_empty"; };

	case "med_ldr";
	case "med_cpm": { _unit addBackpack "tacs_Backpack_Kitbag_Medic_Green"; };

	case "sqd_ldr";
	case "sqd_ldg";
	case "sqd_gre";
	case "sqd_bre";
	case "sqd_int";
	case "sqd_lat": { _unit addBackpack "B_FieldPack_oli"; };

	case "how_ldr";
	case "how_gun"; { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_ldr";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addBackpack "rhs_sidor"; };
	case "lrc_lmg": {_unit addBackpack "B_Kitbag_rgr"; };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_rif";
	case "spf_lat";
	case "spf_bre";
	case "spf_sap": {_unit addBackpack "B_ViperLightHarness_khk_F"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};