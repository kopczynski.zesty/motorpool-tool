// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary weapon.
switch ( _role ) do {
	default { _unit addWeapon "rhs_weap_akmn"; };

	case "plt_ldg";
	case "sqd_ldg": { _unit addWeapon "rhs_weap_akmn_gp25"; };

	case "sqd_gre": { _unit addWeapon "rhs_weap_akmn_gp25"; };
	case "sqd_lmg": { _unit addWeapon "UK3CB_RPK"; };

	case "mmg_gun": { _unit addWeapon "rhs_weap_pkm"; };

	case "lrc_ldr";
	case "lrc_hir": { _unit addWeapon "rhs_weap_akmn_gp25"; };
	case "lrc_lmg": { _unit addWeapon "UK3CB_RPK"; };
	case "lrc_dmr": { _unit addWeapon "rhs_weap_svdp_wd"; };
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_rif": { _unit addWeapon "rhs_weap_akmn"; };

	case "spf_ldg": { _unit addWeapon "rhs_weap_ak103_gp25"; };
	case "spf_ldr";
	case "spf_lmg";
	case "spf_bre";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": { _unit addWeapon "rhs_weap_ak103"; };
	case "spf_dmr": { _unit addWeapon "rhs_weap_svdp"; }; 

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addWeapon "rhs_weap_akms"; };

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": { _unit addWeapon "rhs_weap_akms"; };
};


// Secondary weapon.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg";
	case "fwc_pil";
	case "fwt_pil": { _unit addWeapon "rhs_weap_makarov_pm"; };

	case "spf_ldr";
	case "spf_ldg";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_rif": {
		_unit addWeapon "hgun_Rook40_F";
		_unit addHandgunItem "muzzle_snds_l";
	};
};


// Launcher.
switch ( _role ) do {
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat";
	case "spf_ldr";
	case "spf_lat": { _unit addWeapon "rhs_weap_rpg26"; };

	case "mat_gun": {
		_unit addWeapon "rhs_weap_rpg7";
		_unit addSecondaryWeaponItem "rhs_acc_pgo7v3";
	};
	case "aat_gun": { _unit addWeapon "rhs_weap_igla"; };
};


// Backpack weapon.
switch ( _role ) do {
	case "mot_ldr": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "mot_gun": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "dat_ldr": { _unit addBackpack "RHS_SPG9_Tripod_Bag"; };
	case "dat_gun": { _unit addBackpack "RHS_SPG9_Gun_Bag"; };
	case "dgg_ldr": { _unit addBackpack "RHS_AGS30_Tripod_Bag"; };
	case "dgg_gun": { _unit addBackpack "RHS_AGS30_Gun_Bag"; };
};