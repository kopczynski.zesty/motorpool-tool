allowedVehicles[] = {
	"UAZ_CHE",
	"UAZ_O_CHE",
	"UAZ_DSHKM_CHE",
	"UAZ_AGS30_CHE",
	"UAZ_SPG9_CHE",
	"Ural4320_Fuel_CHE",
	"Ural4320_Recover_CHE",
	"GAZ66_CHE",
	"GAZ66_O_CHE",
	"GAZ66_FB_CHE",
	"GAZ66_O_FB_CHE",
	"GAZ66_Repair_CHE",
	"GAZ66_Ammo_CHE",
	"GAZ66_ZU23_CHE",
	"GAZ66_D30A_CHE",
	"GAZ66_SBCOP_CHE_W",
	"GAZ66_BBCOP_CHE_W",
	"BM21_CHE",
	"BRDM2_CHE",
	"BRDM2_GPMG_CHE",
	"BRDM2_HMG_CHE",
	"BRDM2_ATGM_CHE",
	"BTR60_CHE",
	"BTR70_CHE",
	"ZSU234_CHE",
	"BMP1_CHE",
	"BMP2E_CHE",
	"T72B84_CHE",
	"T72B85_CHE",
	"Mi8_CHE",
	"Mi8_MG_CHE"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat"
};