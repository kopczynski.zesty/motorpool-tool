class Woodland_2009: Faction
{
	name = "Rastr / Woodland";
	year = 2009;
	type = ARMORED;

	uniformScript = "\bwi_data_main\configs\factions\che_armored\2009\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\che_armored\2009\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\che_armored\2009\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\che_armored\2009\ammo.sqf";

	hiddenElements[] = {"FWT", "FWC", "RWC", "UWC", "SCU", "HAT"};
	hiddenRoles[]	 = {"HIR", "UAV", "JTC", "LHG", "AHG"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\rus_color_k.paa",
		"\bwi_resupply_main\data\rus_signs_1.paa",
		"\bwi_resupply_main\data\rus_signs_2.paa",
		"\bwi_resupply_main\data\rus_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_w.hpp>
};