// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary weapon.
switch ( _role ) do {
	default { _unit addWeapon "rhs_weap_m21a_pr"; };

	case "plt_ldg";
	case "sqd_ldg";
	case "sqd_gre": { _unit addWeapon "rhs_weap_m21a_pr_pbg40"; };
	case "sqd_lmg": { _unit addWeapon "rhs_weap_m70b3n"; };

	case "mmg_gun": { _unit addWeapon "rhs_weap_m84"; };

	case "lrc_ldr";
	case "lrc_hir": { _unit addWeapon "rhs_weap_m21a_pr_pbg40"; };
	case "lrc_lmg": { _unit addWeapon "rhs_weap_m70b3n"; };
	case "lrc_dmr": { _unit addWeapon "rhs_weap_m76"; };
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_rif": { _unit addWeapon "rhs_weap_m21s_pr"; };

	case "spf_lmg": { _unit addWeapon "rhs_weap_m249_pip_L_vfg"; };
	case "spf_dmr": { _unit addWeapon "rhs_weap_m76"; }; 
	case "spf_ldg"; // M320 GLM
	case "spf_ldr";
	case "spf_bre";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": { _unit addWeapon "rhs_weap_g36kv"; }; 

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addWeapon "rhs_weap_scorpion"; };

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": { _unit addWeapon "rhs_weap_m21s"; };
};


// Secondary weapon.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg": { _unit addWeapon "rhs_weap_cz99_etched"; };
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg";
	case "fwc_pil";
	case "fwt_pil": { _unit addWeapon "rhs_weap_cz99"; };
	
	case "spf_ldg": { _unit addWeapon "rhs_weap_M320"; };
	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": { _unit addWeapon "rhs_weap_cz99"; };
};


// Launcher.
switch ( _role ) do {
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat";
	case "spf_lat";
	case "scu_lat": { _unit addWeapon "rhs_weap_m80"; };

	case "mat_gun": {
		_unit addWeapon "rhs_weap_smaw_green";
		_unit addSecondaryWeaponItem "rhs_weap_optic_smaw";
	};
	case "aat_gun": { _unit addWeapon "rhs_weap_igla"; };
};


// Backpack weapon.
switch ( _role ) do {
	case "mot_ldr": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "mot_gun": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "dat_ldr": { _unit addBackpack "RHS_Kornet_Tripod_Bag"; };
	case "dat_gun": { _unit addBackpack "RHS_Kornet_Gun_Bag"; };

	case "sqd_bre";
	case "spf_bre": { [_unit, "rhs_weap_M590_8RD", 1] call bwi_armory_fnc_addToBackpack; };
};
