class Desert_2014: Faction
{
    name = "MDU-10-D / Desert";
    year = 2014;
    type = INFANTRY;

    uniformScript = "\bwi_data_main\configs\factions\ser_infantry\2014\uniform_d.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\ser_infantry\2014\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\ser_infantry\2014\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\ser_infantry\2014\ammo.sqf";

    hiddenElements[] = {"HAT", "DGG", "SCU", "FWT", "UWC"};
    hiddenRoles[]    = {"HIR", "UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\ser_color_g.paa",
        "\bwi_resupply_main\data\ser_signs_1.paa",
        "\bwi_resupply_main\data\ser_signs_2.paa",
        "\bwi_resupply_main\data\ser_signs_3.paa"
    };

    #include <motorpool.hpp>
    #include <resupply_d.hpp>
};


class Woodland_2014: Faction
{
    name = "MDU-10-W / Woodland";
    year = 2014;
    type = INFANTRY;

    uniformScript = "\bwi_data_main\configs\factions\ser_infantry\2014\uniform_w.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\ser_infantry\2014\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\ser_infantry\2014\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\ser_infantry\2014\ammo.sqf";

    hiddenElements[] = {"HAT", "DGG", "SCU", "FWT", "UWC"};
    hiddenRoles[]    = {"HIR", "UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\ser_color_g.paa",
        "\bwi_resupply_main\data\ser_signs_1.paa",
        "\bwi_resupply_main\data\ser_signs_2.paa",
        "\bwi_resupply_main\data\ser_signs_3.paa"
    };

    #include <motorpool.hpp>
    #include <resupply_w.hpp>
};


class Desert_UN_2014: Faction
{
    name = "MDU-10-D / Desert (UN)";
    year = 2014;
    type = INFANTRY;

    uniformScript = "\bwi_data_main\configs\factions\ser_infantry\2014\uniform_d_un.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\ser_infantry\2014\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\ser_infantry\2014\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\ser_infantry\2014\ammo.sqf";

    hiddenElements[] = {"HAT", "DGG", "SCU", "FWT", "UWC", "AAA"};
    hiddenRoles[]    = {"HIR", "UAV"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\ser_color_g.paa",
        "\bwi_resupply_main\data\ser_signs_1.paa",
        "\bwi_resupply_main\data\ser_signs_2.paa",
        "\bwi_resupply_main\data\ser_signs_3.paa"
    };

    #include <motorpool_un.hpp>
    #include <resupply_d.hpp>
};


class Woodland_UN_2014: Faction
{
    name = "MDU-10-W / Woodland (UN)";
    year = 2014;
    type = INFANTRY;

    uniformScript = "\bwi_data_main\configs\factions\ser_infantry\2014\uniform_w_un.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\ser_infantry\2014\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\ser_infantry\2014\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\ser_infantry\2014\ammo.sqf";

    hiddenElements[] = {"HAT", "DGG", "SCU", "FWT", "UWC", "AAA"};
    hiddenRoles[]    = {"HIR", "UAV"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\ser_color_g.paa",
        "\bwi_resupply_main\data\ser_signs_1.paa",
        "\bwi_resupply_main\data\ser_signs_2.paa",
        "\bwi_resupply_main\data\ser_signs_3.paa"
    };

    #include <motorpool_un.hpp>
    #include <resupply_w.hpp>
};