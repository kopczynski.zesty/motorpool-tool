// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary ammo.
switch ( _role ) do {
	default {
		[_unit, "rhsgref_30rnd_556x45_m21", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_m21_t", 4] call bwi_armory_fnc_addToBackpack;		
	};
	case "eng_def";
	case "mat_ldr": {
		[_unit, "rhsgref_30rnd_556x45_m21", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_m21_t", 2] call bwi_armory_fnc_addToVest;	
		[_unit, "rhsgref_30rnd_556x45_m21_t", 2] call bwi_armory_fnc_addToBackpack;	
	};
	case "sqd_bre": {
		[_unit, "rhsgref_30rnd_556x45_m21", 4] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_m21_t", 3] call bwi_armory_fnc_addToBackpack;		
	};

	case "sqd_lmg": {
		[_unit, "rhs_75Rnd_762x39mm", 2] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_75Rnd_762x39mm", 4] call bwi_armory_fnc_addToBackpack;	
		[_unit, "rhs_75Rnd_762x39mm_tracer", 4] call bwi_armory_fnc_addToBackpack;		
	};
	case "mmg_gun": {
		[_unit, "rhssaf_250Rnd_762x54R", 2] call bwi_armory_fnc_addToBackpack;	
	};

	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": {
		[_unit, "rhsgref_30rnd_556x45_m21", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_m21_t", 2] call bwi_armory_fnc_addToVest;		
	};

	case "lrc_ldr";
	case "lrc_hir";
	case "lrc_lat";
	case "lrc_rif": {
		[_unit, "rhsgref_30rnd_556x45_m21", 4] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_m21_t", 3] call bwi_armory_fnc_addToBackpack;	
	};
	case "lrc_lmg": {
		[_unit, "rhs_75Rnd_762x39mm", 2] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_75Rnd_762x39mm", 2] call bwi_armory_fnc_addToBackpack;	
		[_unit, "rhs_75Rnd_762x39mm_tracer", 3] call bwi_armory_fnc_addToBackpack;	
	};
	case "lrc_dmr": {
		[_unit, "rhsgref_10Rnd_792x57_m76", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_10Rnd_792x57_m76", 5] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhssaf_10Rnd_792x57_m76_tracer", 10] call bwi_armory_fnc_addToBackpack;
	};

	case "spf_ldr";
	case "spf_ldg";
	case "spf_bre";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": {
		[_unit, "rhssaf_30rnd_556x45_EPR_G36", 6] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 2] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_30rnd_556x45_Tracers_G36", 2] call bwi_armory_fnc_addToBackpack;
	}; 
	case "spf_lmg": {
		[_unit, "rhsusf_200Rnd_556x45_soft_pouch", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhsusf_200Rnd_556x45_soft_pouch", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_200Rnd_556x45_mixed_soft_pouch", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_dmr": {
		[_unit, "rhsgref_10Rnd_792x57_m76", 5] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_10Rnd_792x57_m76", 5] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhssaf_10Rnd_792x57_m76_tracer", 10] call bwi_armory_fnc_addToBackpack;
	}; 

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, "rhsgref_20rnd_765x17_vz61", 7] call bwi_armory_fnc_addToVest;
	};

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": {
		[_unit, "rhsgref_30rnd_556x45_m21", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhsgref_30rnd_556x45_m21_t", 2] call bwi_armory_fnc_addToVest;	
	};
};


// Secondary ammo.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg": {
		[_unit, "rhssaf_mag_15Rnd_9x19_FMJ", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhssaf_mag_15Rnd_9x19_JHP", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "fwc_pil";
	case "fwt_pil": {
		[_unit, "rhssaf_mag_15Rnd_9x19_JHP", 3] call bwi_armory_fnc_addToVest;
	};

	case "spf_ldr";
	//case "spf_ldg"; //M320 GLM
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": {
		[_unit, "rhssaf_mag_15Rnd_9x19_FMJ", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhssaf_mag_15Rnd_9x19_JHP", 1] call bwi_armory_fnc_addToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sqd_amg": {
		[_unit, "rhs_75Rnd_762x39mm", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_75Rnd_762x39mm_tracer", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "mmg_ldr": {
		[_unit, "rhssaf_250Rnd_762x54R", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "mat_gun": {
		[_unit, "rhs_mag_smaw_HEAA", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_smaw_SR", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "mat_ldr": {
		[_unit, "rhs_mag_smaw_HEAA", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_smaw_HEDP", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_smaw_SR", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "aat_gun": {
		[_unit, "rhs_mag_9k38_rocket", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "aat_ldr": {
		[_unit, "rhs_mag_9k38_rocket", 2] call bwi_armory_fnc_addToBackpack;
	};
};


// Other ammo.
switch ( _role ) do {
	case "sqd_bre": {
		[_unit, "rhsusf_8Rnd_00Buck", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhsusf_8Rnd_Slug", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_bre": {
		[_unit, "rhsusf_8Rnd_00Buck", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_8Rnd_Slug", 2] call bwi_armory_fnc_addToBackpack;
	};
};
