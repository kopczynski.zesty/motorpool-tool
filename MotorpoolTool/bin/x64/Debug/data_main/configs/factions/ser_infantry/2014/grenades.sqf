// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Hand grenades.
switch ( _role ) do {
	default {
		[_unit, "rhssaf_mag_brd_m83_white", 2] call bwi_armory_fnc_addToUniform;
		[_unit, "rhssaf_mag_brd_m83_green", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_mag_br_m84", 2] call bwi_armory_fnc_addToVest;
	};

	case "log_sgt";
	case "tac_fac";
	case "tac_jtc": {
		[_unit, "rhssaf_mag_brd_m83_white", 2] call bwi_armory_fnc_addToUniform;
		[_unit, "rhssaf_mag_brd_m83_green", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_mag_br_m84", 2] call bwi_armory_fnc_addToVest;
		[_unit, "rhssaf_mag_brd_m83_blue", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhssaf_mag_brd_m83_red", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": {
		[_unit, "rhssaf_mag_brd_m83_white", 1] call bwi_armory_fnc_addToUniform;
		[_unit, "rhssaf_mag_brd_m83_green", 1] call bwi_armory_fnc_addToVest;	
		[_unit, "rhssaf_mag_brd_m83_red", 1] call bwi_armory_fnc_addToVest;	
	};

	case "eng_def";
	case "eng_mec";
	case "med_ldr";
	case "med_cpm";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr";
	case "aat_gun";
	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun";
	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, "rhssaf_mag_brd_m83_white", 2] call bwi_armory_fnc_addToUniform;
		[_unit, "rhssaf_mag_brd_m83_green", 1] call bwi_armory_fnc_addToVest;	
	};

	case "fwc_pil";
	case "fwt_pil";
	case "rwc_pil";
	case "rwt_pil": {
		[_unit, "rhssaf_mag_brd_m83_green", 1] call bwi_armory_fnc_addToVest;
		[_unit, "ACE_HandFlare_Green", 1] call bwi_armory_fnc_addToVest;	
	};
};


// 40mm rounds.
switch ( _role ) do {
	case "plt_ldg": {
		[_unit, "rhs_VG40OP_white", 2] call bwi_armory_fnc_addToBackpack;

		[_unit, "rhs_VG40MD_Red", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_VG40MD_White", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "rhs_VOG25", 3] call bwi_armory_fnc_addToBackpack;
	};

	
	case "sqd_ldg": {
		[_unit, "rhs_VG40OP_white", 2] call bwi_armory_fnc_addToBackpack;

		[_unit, "rhs_VG40MD_Red", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_VG40MD_White", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "rhs_VOG25", 5] call bwi_armory_fnc_addToBackpack;
	};

	case "sqd_gre": {
		[_unit, "rhs_VG40OP_white", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_VG40OP_red", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "rhs_VG40MD_Red", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_VG40MD_White", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_VG40MD_Green", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "rhs_VOG25", 10] call bwi_armory_fnc_addToBackpack;
	};

	case "lrc_ldr": {
		[_unit, "rhs_VG40OP_white", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_VG40OP_green", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_VG40OP_red", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "rhs_VG40MD_Red", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_VG40MD_White", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "rhs_VOG25", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "spf_ldg": {
		[_unit, "rhs_mag_M585_white", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_m662_red", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "rhs_mag_m713_Red", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_m714_White", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_m715_Green", 1] call bwi_armory_fnc_addToBackpack;

		[_unit, "rhs_mag_M441_HE", 10] call bwi_armory_fnc_addToBackpack;
	};
};