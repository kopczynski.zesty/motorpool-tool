allowedSupplies[] = {
    "Mag_30Rnd_M855_M21",
    "Box_75Rnd_RPK",
	"Mag_8Rnd_Buck",
	"Mag_8Rnd_Slug",
	"Gren_Hand_HE_M84",
	"Gren_Hand_WS_M83",
	"Gren_Hand_CS_M83",
	"Gren_40mm_HE_VOG25",
	"Gren_40mm_WS_VG40MD",
	"Gren_40mm_CS_VG40MD",
	"Gren_40mm_WF_VG40OP",
	"Gren_40mm_CF_VG40OP",
	"AT_Tube_M80",
	"Medical_Bandages",
	"Medical_Medication",
	"Medical_Blood",
	"Medical_Bodybags",
	"Explosive_M112",
	"Explosive_M183",
	"Explosive_M6SLAM",
	"Explosive_M26",
	"Explosive_M15",
	"Explosive_M18A1",
	"Explosive_PMR3M",
	"Explosive_PMR3F",
	
	"DMG_M2_Low",
	"DMG_M2_High",
	
	"Spare_Track",
	"Spare_Wheel",
	"Jerry_Can",

	"BaseVehicleResupplyWoodEast",
	"MortarSandbagWood",
	"BunkerSandbagWood",
	"BunkerSandbagLargeWood",
	"SquadSandbagWoodEast",
	"SquadSandbagTallWoodEast",
	"CheckpointSandbagWoodEast",
    "BunkerBarricadeSmall",
    "BunkerBarricadeMedium",
    "BunkerBarricadeLarge",
    "CheckpointBarricadeEast",
    "SquadBarricadeWoodEast",
    "SquadBarricadeTallWoodEast",
    "VehicleTrenchWoodland",
    "SquadTrench90WoodEast",
    "SquadTrench180WoodEast",
    "SquadTrench360WoodEast",
    "VehicleTrenchCamoWoodlandEast",
    "VehicleTrenchLargeWoodlandEast",
    "PortableLightKit",
    "TankTrap4",
    "TankTrap8"
};

allowedSuppliesWeapons[] = {
    "Box_250Rnd_M30",
	"AT_1Rnd_Mk6HEAA",
	"AT_1Rnd_Mk3HEDP",
	"AA_1Rnd_9K38"
};

allowedSuppliesRecon[] = {
	"Mag_10Rnd_M70"
};

allowedSuppliesSpecial[] = {
    "Mag_30Rnd_M855A1_G36",
	"Box_200Rnd_M855A1",
	"Box_200Rnd_M855_SP_W",
    "Mag_10Rnd_M70",
	"Gren_40mm_HE_M441",
	"Gren_40mm_WS_M714",
	"Gren_40mm_CS_M713",
	"Gren_40mm_WF_M585",
	"Gren_40mm_CF_M661"
};