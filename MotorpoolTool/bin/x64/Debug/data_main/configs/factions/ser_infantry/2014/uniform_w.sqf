// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on special forces and scuba roles.
if ( (_role select [0,3]) in ["spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhssaf_uniform_m10_digital"; };

	case "rwc_pil";
	case "rwt_pil": { _unit forceAddUniform "rhssaf_uniform_heli_pilot"; };
	case "fwc_pil";
	case "fwt_pil": { _unit forceAddUniform "rhssaf_uniform_mig29_pilot"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "rhssaf_helmet_m97_digital"; };

	case "plt_ldr";
	case "log_sgt";
	case "sqd_ldr";
	case "sqd_ldg";
	case "lrc_ldr";
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "how_ldr";
	case "mot_ldr";
	case "dat_ldr";
	case "dgg_ldr": { _unit addHeadgear "rhssaf_helmet_m97_digital_black_ess_bare"; };	

	case "spf_ldr";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": {
		_unit addGoggles "rhs_balaclava";
		_unit addHeadgear "rhssaf_helmet_m97_black_nocamo_black_ess_bare";
	};

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "rhs_tsh4"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addHeadgear "rhsusf_hgu56p_visor_saf"; };
	case "fwc_pil": { _unit addHeadgear "rhs_zsh7a_alt"; };
	case "fwt_pil": { _unit addHeadgear "rhs_zsh7a_mike"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "rhssaf_vest_md12_digital"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "rhssaf_vest_md99_digital"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "V_TacVest_oli"; };
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "V_TacVest_oli"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_oli"; };

	case "plt_ldr";
	case "med_ldr";
	case "med_cpm";
	case "sqd_lmg";
	case "sqd_amg";
	case "mmg_ldr";
	case "mmg_gun": { _unit addBackpack "rhssaf_kitbag_digital"; };

	case "spc_dem";
	case "eng_def";
	case "mat_ldr";
	case "hat_ldr";
	case "hat_gun";
	case "aat_ldr": { _unit addBackpack "B_Carryall_oli"; };

	case "how_ldr";
	case "how_gun": { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_rif": { _unit addBackpack "B_FieldPack_oli"; };
	case "spf_lat";
	case "spf_sap";
	case "spf_bre": { _unit addBackpack "rhssaf_kitbag_digital"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};