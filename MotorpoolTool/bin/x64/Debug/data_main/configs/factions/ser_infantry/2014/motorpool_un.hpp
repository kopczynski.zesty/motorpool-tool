allowedVehicles[] = {
	"UAZ_UN",
	"UAZ_O_UN",
	"UAZ_DSHKM_UN",
	"UAZ_AGS30_UN",
	"UAZ_SPG9_UN",
	"BTR40_UN",
	"BTR40_DSHKM_UN",
	"Ural375D_UN",
	"Ural375D_Recover_UN",
	"Ural375D_Ammo_UN",
	"Ural375D_Fuel_UN",
	"Ural375D_SBCOP_UN",
    "Ural375D_D30J_UN",
	"BRDM2_GPMG_UN",
	"BRDM2_HMG_UN",
	"BRM1K_UN",
	"BTR70_UN",
	"BMP1_UN",
	"BRM1K_UN",
	"T72A_UN",
	"T72B_UN",
	"ZSU234_UN",
	"Mi24V_UN",
	"Mi8_UN"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat"
};