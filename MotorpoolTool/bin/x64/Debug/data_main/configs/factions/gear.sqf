// Types: "AIR" Airborne, "MAR" = Marines, "ARM" = Armored, "INF" = Infantry, "CON" = Contractor, "MIL" = Militia, "INS" = Insurgents
params ["_unit", "_role", "_year", "_type"];


// Add standard gear for all.
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";

[_unit, "ACE_MapTools", 1] call bwi_armory_fnc_addToUniform;
[_unit, "ACE_EarPlugs", 1] call bwi_armory_fnc_addToUniform;
[_unit, "ACE_CableTie", 2] call bwi_armory_fnc_addToUniform;


// Add watch accordingly.
if ( _type in ["AIR"] && _year >= 1990 ) then {
	_unit linkItem "ACE_Altimeter";
} else {
	_unit linkItem "ItemWatch";
};


// Add GPS/BFT devices.
if ( _type in ["AIR", "MAR", "ARM", "INF", "CON"] ) then {
	switch ( _role ) do {
		case "plt_ldr";
		case "log_sgt";
		case "med_ldr";
		case "med_cpm";
		case "eng_def";
		case "eng_mec";
		case "tac_fac";
		case "tac_jtc";
		case "sqd_ldr";
		case "sqd_ldg";
		case "lrc_ldr";
		case "spf_ldr";
		case "scu_ldr";
		case "mmg_ldr";
		case "mat_ldr";
		case "hat_ldr";
		case "aat_ldr";
		case "how_ldr";
		case "mot_ldr";
		case "dat_ldr";
		case "dgg_ldr";
		case "mbt_cmd";
		case "ifv_cmd";
		case "apc_cmd";
		case "art_cmd";
		case "aaa_cmd": {
			if ( _year >= 2010 ) then {
				[_unit, "ACE_microDAGR", 1] call bwi_armory_fnc_addToUniform;
				_unit linkItem "ItemGPS";
			};

			if ( _year >= 2000 && _year < 2010 ) then {
				if ( _role in ["scu_ldr"] ) then {
					[_unit, "ACE_DAGR", 1] call bwi_armory_fnc_addToBackpack;
				} else {
					[_unit, "ACE_DAGR", 1] call bwi_armory_fnc_addToVest;
				};
			};
		};

		case "fwc_pil";
		case "fwt_pil";
		case "rwc_pil";
		case "rwt_pil": {
			if ( _year >= 2010 ) then {
				[_unit, "ACE_microDAGR", 1] call bwi_armory_fnc_addToUniform;
				_unit linkItem "ItemGPS";
			};

			if ( _year >= 2000 && _year < 2010 ) then {
				[_unit, "ACE_DAGR", 1] call bwi_armory_fnc_addToVest;
			};
		};

		case "lrc_uav";
		case "uwc_pil": {
			if ( _year >= 2010 ) then {
				[_unit, "ACE_microDAGR", 1] call bwi_armory_fnc_addToUniform;
			};

			if ( _year >= 2000 ) then {
				switch ( side _unit ) do { // UAV terminal is side dependent.
					case west: 		 { _unit linkItem "B_UavTerminal"; };
					case east: 		 { _unit linkItem "O_UavTerminal"; };
					case resistance: { _unit linkItem "I_UavTerminal"; };
				};
			};
		};

		default {
			if ( _year >= 2010 ) then {
				[_unit, "ACE_microDAGR", 1] call bwi_armory_fnc_addToUniform;
				_unit linkItem "ItemGPS";
			};
		};
	};
} else {
	if ( _year >= 2010 ) then {
		// Militias don't get the MicroDAGR.
		_unit linkItem "ItemGPS";
	};
};


// Add IR strobes.
if ( _type in ["AIR", "MAR", "ARM", "INF", "CON"] && _year >= 1990 ) then {
	[_unit, "ACE_IR_Strobe_Item", 1] call bwi_armory_fnc_addToUniform;
};


// Add flashlights.
if ( _year >= 2010 ) then {
	[_unit, "ACE_Flashlight_XL50", 1] call bwi_armory_fnc_addToUniform;
} else {
	[_unit, "ACE_Flashlight_MX991", 1] call bwi_armory_fnc_addToUniform;
};


// Add chemlights.
if ( _type in ["AIR", "MAR", "ARM", "INF", "CON", "MIL"] && _year >= 1990 ) then {
	switch ( _role ) do {
		case "plt_ldr";
		case "sqd_ldr";
		case "sqd_ldg";
		case "sqd_lat";
		case "sqd_amg";
		case "sqd_bre": {
			[_unit, "Chemlight_red", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "Chemlight_green", 1] call bwi_armory_fnc_addToBackpack;
		};

		case "lrc_ldr";
		case "spf_ldr";
		case "scu_ldr": {
			[_unit, "Chemlight_red", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "Chemlight_green", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "ACE_Chemlight_IR", 1] call bwi_armory_fnc_addToBackpack;
		};

		case "med_ldr";
		case "med_cpm";
		case "eng_mec": {
			[_unit, "ACE_Chemlight_HiWhite", 2] call bwi_armory_fnc_addToBackpack;
		};

		case "spc_eod": {
			[_unit, "Chemlight_red", 2] call bwi_armory_fnc_addToBackpack;
			[_unit, "Chemlight_green", 2] call bwi_armory_fnc_addToBackpack;
		};

		case "log_sgt";
		case "tac_fac";
		case "tac_jtc": {
			[_unit, "ACE_Chemlight_UltraHiOrange", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "ACE_Chemlight_HiYellow", 1] call bwi_armory_fnc_addToBackpack;
		};
	};
};


// Add IR grenades.
if ( _type in ["AIR", "MAR", "ARM", "INF", "CON"] && _year >= 1990 ) then {
	// Item class varies by side.
	private _grenadeClass = "";
	switch ( side _unit ) do {
		case west: { 	   _grenadeClass = "B_IR_Grenade"; };
		case east: { 	   _grenadeClass = "O_IR_Grenade"; };
		case resistance: { _grenadeClass = "I_IR_Grenade"; };
	};

	switch ( _role ) do {
		case "plt_ldr";
		case "sqd_ldr";
		case "sqd_ldg";
		case "mmg_ldr";
		case "mat_ldr";
		case "hat_ldr";
		case "aat_ldr";
		case "how_ldr";
		case "mot_ldr";
		case "dat_ldr";
		case "dgg_ldr";
		case "lrc_ldr";
		case "spf_ldr";
		case "tac_fac";
		case "tac_jtc": {
			[_unit, _grenadeClass,  1] call bwi_armory_fnc_addToVest;
		};

		case "scu_ldr": {
			[_unit, _grenadeClass,  1] call bwi_armory_fnc_addToBackpack;
		};
	};
};


// Add medical items.
switch ( _role ) do {
	case "med_ldr";
	case "med_cpm": {
		[_unit, "ACE_fieldDressing",  2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_packingBandage", 3] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_elasticBandage", 2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_quikclot", 	  3] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_tourniquet", 	  2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_splint",		  2] call bwi_armory_fnc_addToUniform;

		[_unit, "ACE_fieldDressing",  10] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_packingBandage", 10] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_elasticBandage", 10] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_quikclot", 	   8] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_tourniquet", 	   4] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_splint",		   4] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_morphine", 	   8] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_epinephrine", 	   6] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_adenosine", 	   2] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_plasmaIV_500",    6] call bwi_armory_fnc_addToBackpack;	
		[_unit, "ACE_salineIV_500",    2] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_surgicalKit",     1] call bwi_armory_fnc_addToBackpack;
	};

	case "spf_lat";
	case "scu_lat": {
		[_unit, "ACE_fieldDressing",  2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_packingBandage", 3] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_elasticBandage", 2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_quikclot", 	  3] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_tourniquet", 	  2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_splint",		  2] call bwi_armory_fnc_addToUniform;

		[_unit, "ACE_fieldDressing",  10] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_packingBandage", 10] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_elasticBandage", 10] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_quikclot", 	   8] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_tourniquet", 	   4] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_splint",		   6] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_morphine", 	   8] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_epinephrine", 	   6] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_adenosine", 	   2] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_plasmaIV", 	   2] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_plasmaIV_500",    8] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_surgicalKit",     1] call bwi_armory_fnc_addToBackpack;
	};
	
	case "fwc_pil";
	case "fwt_pil";
	case "rwc_pil";
	case "rwt_pil": {
		[_unit, "ACE_fieldDressing",  2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_packingBandage", 3] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_elasticBandage", 2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_quikclot", 	  3] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_tourniquet", 	  2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_morphine", 	  1] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_splint",		  2] call bwi_armory_fnc_addToUniform;
	};

	case "civ_zeu": {
		[_unit, "ACE_fieldDressing",  2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_packingBandage", 2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_elasticBandage", 2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_quikclot", 	  2] call bwi_armory_fnc_addToUniform;
	};
	
	default {
		[_unit, "ACE_fieldDressing",  2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_packingBandage", 3] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_elasticBandage", 2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_quikclot", 	  3] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_tourniquet", 	  2] call bwi_armory_fnc_addToUniform;
		[_unit, "ACE_splint",		  2] call bwi_armory_fnc_addToUniform;
	};
};


// Add spray paint.
if ( _year >= 1985 ) then {
	switch ( _role ) do {
		case "plt_ldr";
		case "sqd_ldr";
		case "sqd_ldg";
		case "mmg_ldr";
		case "mat_ldr";
		case "hat_ldr";
		case "aat_ldr";
		case "lrc_ldr";
		case "spf_ldr";
		case "scu_ldr": {
			[_unit, "ACE_SpraypaintBlue",  1] call bwi_armory_fnc_addToBackpack;
		};

		case "spc_eod": {
			[_unit, "ACE_SpraypaintRed",  1] call bwi_armory_fnc_addToBackpack;
			[_unit, "ACE_SpraypaintGreen",  1] call bwi_armory_fnc_addToBackpack;
		};
	};
};


// Add specialty gear for specific roles.
switch ( _role ) do {
	case "log_sgt": {
		[_unit, "ToolKit", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "BWI_Construction_Tool", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "eng_mec": {
		[_unit, "ToolKit", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "spc_eod": {
		_unit addWeapon "ACE_VMM3";

		[_unit, "ACE_DefusalKit", 1] call bwi_armory_fnc_addToBackpack;

		if ( _type in ["AIR", "MAR", "ARM", "INF", "CON", "MIL"] ) then {
			[_unit, "ACE_Clacker", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "DemoCharge_Remote_Mag", 4] call bwi_armory_fnc_addToBackpack;
		};
	};

	case "eng_def": {
		[_unit, "BWI_Construction_Tool", 1] call bwi_armory_fnc_addToBackpack;

		if ( _type in ["INS"] && _year >= 1995 ) then {
			[_unit, "ACE_Cellphone", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "IEDUrbanBig_Remote_Mag", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "IEDLandBig_Remote_Mag", 2] call bwi_armory_fnc_addToBackpack;
		} else {
			[_unit, "ATMine_Range_Mag", 2] call bwi_armory_fnc_addToBackpack;
			[_unit, "SLAMDirectionalMine_Wire_Mag", 2] call bwi_armory_fnc_addToBackpack;
			[_unit, "APERSBoundingMine_Range_Mag", 4] call bwi_armory_fnc_addToBackpack;
			[_unit, "ACE_Clacker", 1] call bwi_armory_fnc_addToBackpack;
		};
	};

	case "spc_dem": {
		if ( _type in ["INS"] && _year >= 1995 ) then {
			[_unit, "ACE_Cellphone", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "IEDLandBig_Remote_Mag", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "IEDUrbanSmall_Remote_Mag", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "IEDLandSmall_Remote_Mag", 1] call bwi_armory_fnc_addToBackpack;

			[_unit, "ACE_DeadManSwitch", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "DemoCharge_Remote_Mag", 1] call bwi_armory_fnc_addToBackpack;

		} else {
			[_unit, "ACE_M26_Clacker", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "SatchelCharge_Remote_Mag", 3] call bwi_armory_fnc_addToBackpack;
		};
	};

	case "tac_fac": {
		if ( _year >= 2000 ) then {
			[_unit, "ACE_Kestrel4500", 1] call bwi_armory_fnc_addToUniform;
		};
	};

	case "sqd_sap";
	case "spf_sap";
	case "scu_sap": {
		[_unit, "ACE_wirecutter", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "ACE_EntrenchingTool", 1] call bwi_armory_fnc_addToBackpack;
		
		if ( _type in ["INS", "MIL"] ) then {
			[_unit, "rhs_ec400_sand_mag", 6] call bwi_armory_fnc_addToBackpack;
		} else {
			[_unit, "DemoCharge_Remote_Mag", 6] call bwi_armory_fnc_addToBackpack;
		};
	};

	case "lrc_hir": {
		if ( _type in ["AIR", "MAR", "ARM", "INF", "CON"] && _year >= 2006 ) then {
			[_unit, "ACE_HuntIR_monitor", 1] call bwi_armory_fnc_addToBackpack;
			[_unit, "ACE_HuntIR_M203", 8] call bwi_armory_fnc_addToBackpack;
		};
	};

	case "lrc_uav": {
		[_unit, "ACE_UAVBattery", 2] call bwi_armory_fnc_addToVest;
	};

	case "sqd_bre";
	case "spf_bre";
	case "scu_bre": {
		if ( _type in ["AIR", "MAR", "ARM", "INF", "CON"] ) then {
			[_unit, "ACE_M84", 6] call bwi_armory_fnc_addToBackpack;
		};
	};

	case "mmg_ldr": {
		[_unit, "ACE_SpareBarrel", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "mot_ldr";
	case "mot_gun": {
		if ( _year >= 2000 ) then {
			[_unit, "ACE_Kestrel4500", 1] call bwi_armory_fnc_addToVest;
		};

		[_unit, "ACE_RangeTable_82mm", 1] call bwi_armory_fnc_addToUniform;
	};

	case "how_ldr";
	case "how_gun": {
		if ( _year >= 2000 ) then {
			[_unit, "ACE_Kestrel4500", 1] call bwi_armory_fnc_addToVest;
		};

		[_unit, "ACE_artilleryTable", 1] call bwi_armory_fnc_addToUniform;
	};

	case "lrc_dmr";
	case "spf_dmr";
	case "scu_dmr": {
		if ( _type in ["AIR", "MAR", "ARM", "INF", "CON"] ) then {
			[_unit, "ACE_Tripod"] call bwi_armory_fnc_addToBackpack;
		};

		if ( _year >= 2000 ) then {
			[_unit, "ACE_Kestrel4500", 1] call bwi_armory_fnc_addToUniform;
		};

		[_unit, "ACE_RangeCard", 1] call bwi_armory_fnc_addToUniform;
	};
};


// Add binoculars
switch ( _role ) do {
	case "plt_ldr";
	case "log_sgt";
	case "tac_fac";
	case "sqd_ldr";
	case "sqd_ldg";
	case "sqd_amg";
	case "sqd_ahg";
	case "mmg_ldr";
	case "mat_ldr";
	case "hat_ldr";
	case "aat_ldr";
	case "dat_ldr";
	case "dgg_ldr": {
		if ( _year >= 2000 ) then {
			if ( _type in ["AIR", "MAR", "ARM", "INF", "CON", "MIL"] ) then {
				switch ( side _unit ) do {
					case west:		 { _unit addWeapon "rhsusf_bino_lrf_Vector21"; };
					case east;
					case resistance: { _unit addWeapon "rhs_pdu4"; };
				};
			} else {
				_unit addWeapon "rhssaf_zrak_rd7j";
			};
		} else {
			_unit addWeapon "Binocular";
		};
	};

	case "lrc_ldr";
	case "lrc_dmr";
	case "spf_ldr";
	case "scu_ldr";
	case "scu_dmr";
	case "how_ldr";
	case "mot_ldr": {
		if ( _year >= 2000 ) then {
			if ( _type in ["AIR", "MAR", "ARM", "INF", "CON"] ) then {
				if ( _year >= 2010 ) then {
					_unit addWeapon "ACE_Vector";
				} else {
					_unit addWeapon "ACE_VectorDay";
				};
			} else {
				_unit addWeapon "rhssaf_zrak_rd7j";
			};
		} else {
			_unit addWeapon "Binocular";
		};
	};

	case "lrc_hir": {
		if ( _type in ["AIR", "MAR", "ARM", "INF", "CON"] && _year >= 2000 ) then {
			_unit addWeapon "ACE_MX2A";
		} else {
			_unit addWeapon "Binocular";
		};
	};

	case "spf_dmr";
	case "tac_jtc": {
		if ( _type in ["AIR", "MAR", "ARM", "INF", "CON"] && _year >= 1990 ) then {
			_unit addWeapon "Laserdesignator_01_khk_F";
			[_unit, "Laserbatteries", 2] call bwi_armory_fnc_addToBackpack;
		} else {
			_unit addWeapon "Binocular";
		};
	};

	default {
		_unit addWeapon "Binocular";
	};
};