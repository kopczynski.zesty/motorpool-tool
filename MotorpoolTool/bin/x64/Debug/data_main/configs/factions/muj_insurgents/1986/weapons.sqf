// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary weapon.
switch ( _role ) do {
	default { [_unit, ["UK3CB_Enfield", "rhs_weap_m38", "rhs_weap_akm", "rhs_weap_akms"]] call bwi_armory_fnc_addRandomWeapon; };

	case "sqd_lmg": { _unit addWeapon "UK3CB_RPK"; };

	case "mmg_gun": { _unit addWeapon "rhs_weap_pkm"; };

	case "lrc_ldr";
	case "lrc_hir": { _unit addWeapon "rhs_weap_akms"; };
	case "lrc_lmg": { _unit addWeapon "UK3CB_RPK"; };
	case "lrc_dmr": { _unit addWeapon "UK3CB_SVD_OLD"; };
	case "lrc_lat";
	case "lrc_uav";
	case "lrc_rif": { _unit addWeapon "rhs_weap_akms"; };
};


// Secondary weapon.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg";
	case "fwc_pil";
	case "fwt_pil": { _unit addWeapon "rhs_weap_tt33"; };
};


// Launcher.
switch ( _role ) do {
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat": { _unit addWeapon "rhs_weap_rpg26"; };

	case "sqd_gre": { _unit addWeapon "rhs_weap_rshg2"; }; // Special: RShG-2 instead of GP-25

	case "mat_gun": {
		_unit addWeapon "rhs_weap_rpg7";
		_unit addSecondaryWeaponItem "rhs_acc_pgo7v2";
	};
	case "aat_gun": { _unit addWeapon "rhs_weap_fim92"; };
};


// Backpack weapon.
switch ( _role ) do {
	case "mot_ldr": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "mot_gun": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "dat_ldr": { _unit addBackpack "RHS_SPG9_Tripod_Bag"; };
	case "dat_gun": { _unit addBackpack "RHS_SPG9_Gun_Bag"; };
	case "dgg_ldr": { _unit addBackpack "RHS_AGS30_Tripod_Bag"; };
	case "dgg_gun": { _unit addBackpack "RHS_AGS30_Gun_Bag"; };
};
