// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary ammo.
switch ( _role ) do {
	default {
		if (_unit hasWeapon "UK3CB_Enfield") then {
			[_unit, "UK3CB_Enfield_Mag", 8] call bwi_armory_fnc_addToUniform;
			[_unit, "UK3CB_Enfield_Mag", 8] call bwi_armory_fnc_addToVest;
		};
		if (_unit hasWeapon "rhs_weap_m38") then {
			[_unit, "rhsgref_5Rnd_762x54_m38", 15] call bwi_armory_fnc_addToUniform;
			[_unit, "rhsgref_5Rnd_762x54_m38", 15] call bwi_armory_fnc_addToVest;
		};
		if (_unit hasWeapon "rhs_weap_akm" || _unit hasWeapon "rhs_weap_akms") then {
			[_unit, "rhs_30Rnd_762x39mm_bakelite", 5] call bwi_armory_fnc_addToUniform;
			[_unit, "rhs_30Rnd_762x39mm_bakelite_tracer", 4] call bwi_armory_fnc_addToVest;
		};
	};

	case "sqd_lmg": {
		[_unit, "UK3CB_RPK_75Rnd_Drum_T", 1] call bwi_armory_fnc_addToUniform;
		[_unit, "UK3CB_RPK_75Rnd_Drum_T", 1] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_RPK_75Rnd_Drum_T", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_RPK_75Rnd_Drum", 3] call bwi_armory_fnc_addToBackpack;
	};
	
	case "mmg_gun": {
		[_unit, "rhs_100Rnd_762x54mmR", 1] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_100Rnd_762x54mmR", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR_green", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "lrc_ldr";
	case "lrc_hir";
	case "lrc_lat";
	case "lrc_rif": {
		[_unit, "rhs_30Rnd_762x39mm", 4] call bwi_armory_fnc_addToUniform;
		[_unit, "rhs_30Rnd_762x39mm_tracer", 3] call bwi_armory_fnc_addToVest;
	};
	case "lrc_lmg": {
		[_unit, "UK3CB_RPK_75Rnd_Drum_T", 1] call bwi_armory_fnc_addToUniform;
		[_unit, "UK3CB_RPK_75Rnd_Drum_T", 1] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_RPK_75Rnd_Drum_T", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_RPK_75Rnd_Drum", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_dmr": {
		[_unit, "rhs_10Rnd_762x54mmR_7N1", 8] call bwi_armory_fnc_addToUniform;
		[_unit, "rhs_10Rnd_762x54mmR_7N14", 7] call bwi_armory_fnc_addToVest;
	};
	case "lrc_uav": { };

	case "spf_ldr";
	case "spf_ldg";
	case "spf_bre";
	case "spf_sap";
	case "spf_rif";
	case "spf_lat";
	case "spf_lmg";
	case "spf_dmr";
	case "scu_ldr";
	case "scu_bre";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif"; 
	case "scu_lmg";
	case "scu_dmr": { };

	case "fwt_pil": { /* No primary */ };
	case "uwc_pil": { /* No primary */ };
};

// Secondary ammo.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_ldr";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg": {
		[_unit, "rhs_mag_762x25_8", 3] call bwi_armory_fnc_addToVest;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sqd_amg": {
		[_unit, "UK3CB_RPK_75Rnd_Drum", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "UK3CB_RPK_75Rnd_Drum_T", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "mmg_ldr": {
		[_unit, "rhs_100Rnd_762x54mmR", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call bwi_armory_fnc_addToBackpack;
	};

	case "mat_gun": {
		[_unit, "rhs_rpg7_PG7VL_mag", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "mat_ldr": {
		[_unit, "rhs_rpg7_PG7VL_mag", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_rpg7_PG7V_mag", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "aat_gun": {
		[_unit, "rhs_fim92_mag", 1] call bwi_armory_fnc_addToBackpack;
	};
	case "aat_ldr": {
		[_unit, "rhs_fim92_mag", 2] call bwi_armory_fnc_addToBackpack;
	};
};


// Other ammo.
// None.