allowedVehicles[] = {
	"SkodaS1203_INS",
	"Datsun_INS",
	"Datsun_O_INS",
	"Datsun_PKM_INS",
	"Hilux_INS",
	"Hilux_O_INS",
	"Hilux_PKM_INS",
	"Hilux_DSHKM_INS",
	"Hilux_AGS30_INS",
	"Hilux_SPG9_INS",
	"Hilux_Rocket_INS",
	"Hilux_Artillery_INS",
	"PragaV3S_INS_D",
	"PragaV3S_O_INS_D",
	"PragaV3S_Recover_INS_D",
	"PragaV3S_Fuel_INS_D",
	"PragaV3S_Repair_INS_D",
	"PragaV3S_Ammo_INS_D",
	"PragaV3S_ZU23_INS_D",
	"PragaV3S_D30A_INS_D",
	"PragaV3S_TCOP_INS_D",
	"PragaV3S_BBCOP_INS_D",
	"Ikarus_INS_D",
	"MTLB_INS_D",
	"BTR60_INS_D",
	"BMP1_INS_D",
	"T55_INS_D"
};

allowedVehiclesRecon[] = {
	"Yava250N_MUJ"
};

allowedVehiclesSpecial[] = {
	/* None. */
};