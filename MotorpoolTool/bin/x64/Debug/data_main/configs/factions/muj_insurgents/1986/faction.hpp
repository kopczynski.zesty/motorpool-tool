class Desert_1986: Faction
{
	name = "Tribal Fighter / Desert";
	year = 1986;
	type = INSURGENTS;

	uniformScript = "\bwi_data_main\configs\factions\muj_insurgents\1986\uniform_d.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\muj_insurgents\1986\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\muj_insurgents\1986\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\muj_insurgents\1986\ammo.sqf";

	hiddenElements[] = {"FWT", "FWC", "RWC", "RWT", "TAC", "UWC", "SCU", "SPF", "HAT", "DGG", "ART"};
	hiddenRoles[]	 = {"HIR", "UAV", "EOD", "LDG", "BRE", "LHG", "AHG"};

	acreRadioDevices[] = {"NONE", "NONE", "ACRE_SEM52SL", "ACRE_PRC77"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\usa_color_t.paa",
		"\bwi_resupply_main\data\tkus_signs_1.paa",
		"\bwi_resupply_main\data\tkus_signs_2.paa",
		"\bwi_resupply_main\data\tkus_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_d.hpp>
};