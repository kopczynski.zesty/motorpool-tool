class SquadTrench90WoodEast: Supply {
	classname = "BWI_Construction_SquadTrench90WoodEast";
	compatible[] = {">1000m"};
};

class SquadTrench90WoodWest: Supply {
	classname = "BWI_Construction_SquadTrench90WoodWest";
	compatible[] = {">1000m"};
};

class SquadTrench180WoodEast: Supply {
	classname = "BWI_Construction_SquadTrench180WoodEast";
	compatible[] = {">1000m"};
};

class SquadTrench180WoodWest: Supply {
	classname = "BWI_Construction_SquadTrench180WoodWest";
	compatible[] = {">1000m"};
};

class SquadTrench360WoodEast: Supply {
	classname = "BWI_Construction_SquadTrench360WoodEast";
	compatible[] = {">1000m"};
};

class SquadTrench360WoodWest: Supply {
	classname = "BWI_Construction_SquadTrench360WoodWest";
	compatible[] = {">1000m"};
};

class VehicleTrenchWoodland: Supply {
	classname = "BWI_Construction_VehicleTrenchWood";
	compatible[] = {">1000m"};
};

class VehicleTrenchCamoWoodlandEast: Supply {
	classname = "BWI_Construction_VehicleTrenchCamoWoodEast";
	compatible[] = {">1000m"};
};

class VehicleTrenchCamoWoodlandWest: Supply {
	classname = "BWI_Construction_VehicleTrenchCamoWoodWest";
	compatible[] = {">1000m"};
};

class VehicleTrenchLargeWoodlandEast: Supply {
	classname = "BWI_Construction_VehicleTrenchLargeWoodEast";
	compatible[] = {">1000m"};
};

class VehicleTrenchLargeWoodlandWest: Supply {
	classname = "BWI_Construction_VehicleTrenchLargeWoodWest";
	compatible[] = {">1000m"};
};