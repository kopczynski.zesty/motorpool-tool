class ConstructionMBK: Category {
    name = "Modular Base Kits";
    
    #include <modular_base_d.hpp>
    #include <modular_base_w.hpp>
};

class ConstructionFK_SB: Category {
    name = "Field Kits (Sandbag)";
    
    #include <field_kit_sb_d.hpp>
    #include <field_kit_sb_w.hpp>
};

class ConstructionFK_BB: Category {
    name = "Field Kits (Barricade)";

    #include <field_kit_bb_n.hpp>
    #include <field_kit_bb_d.hpp>
    #include <field_kit_bb_w.hpp>
};

class ConstructionFK_HB: Category {
    name = "Field Kits (H-Barrier)";

    #include <field_kit_hb_d.hpp>
    #include <field_kit_hb_w.hpp>
};

class ConstructionTK: Category {
    name = "Trench Kits";
    
    #include <trench_kit_d.hpp>
    #include <trench_kit_w.hpp>
};

class ConstructionCOP: Category {
    name = "Combat Outpost Kits";

    #include <staging_cop_n.hpp>
    #include <staging_cop_d.hpp>
    #include <staging_cop_w.hpp>
};

class ConstructionEK: Category {
    name = "Other Construction Kits";

    #include <extra_kit.hpp>
};