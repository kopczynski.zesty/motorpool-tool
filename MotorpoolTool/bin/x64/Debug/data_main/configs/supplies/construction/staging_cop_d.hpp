class COPSandbagDesertWest: Supply {
	classname = "BWI_Staging_COPSandbagDesertWest";
	compatible[] = {">3000m"};
};

class COPSandbagDesertEast: Supply {
	classname = "BWI_Staging_COPSandbagDesertEast";
	compatible[] = {">3000m"};
};

class COPSandbagTallDesertWest: Supply {
	classname = "BWI_Staging_COPSandbagTallDesertWest";
	compatible[] = {">3000m"};
};

class COPSandbagTallDesertEast: Supply {
	classname = "BWI_Staging_COPSandbagTallDesertEast";
	compatible[] = {">3000m"};
};

class COPBarricadeDesertEast: Supply {
	classname = "BWI_Staging_COPBarricadeDesertEast";
	compatible[] = {">3000m"};
};

class COPBarricadeTallDesertEast: Supply {
	classname = "BWI_Staging_COPBarricadeTallDesertEast";
	compatible[] = {">3000m"};
};

class COPHescoDesertWest: Supply {
	classname = "BWI_Staging_COPHescoDesertWest";
	compatible[] = {">3000m"};
};

class COPHescoDesertEast: Supply {
	classname = "BWI_Staging_COPHescoDesertEast";
	compatible[] = {">3000m"};
};

class COPHescoHeavyDesertWest: Supply {
	classname = "BWI_Staging_COPHescoHeavyDesertWest";
	compatible[] = {">3000m"};
};

class COPHescoHeavyDesertEast: Supply {
	classname = "BWI_Staging_COPHescoHeavyDesertEast";
	compatible[] = {">3000m"};
};