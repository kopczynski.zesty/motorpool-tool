class SquadBarricadeDesertEast: Supply {
	classname = "BWI_Construction_SquadBarricadeDesertEast";
	compatible[] = {">1000m"};
};

class SquadBarricadeDesertWest: Supply {
	classname = "BWI_Construction_SquadBarricadeDesertWest";
	compatible[] = {">1000m"};
};

class SquadBarricadeTallDesertEast: Supply {
	classname = "BWI_Construction_SquadBarricadeTallDesertEast";
	compatible[] = {">1000m"};
};

class SquadBarricadeTallDesertWest: Supply {
	classname = "BWI_Construction_SquadBarricadeTallDesertWest";
	compatible[] = {">1000m"};
};