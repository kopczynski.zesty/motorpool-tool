class PortableLightKit: Supply {
	classname = "BWI_Construction_PortableLightKit";
	compatible[] = {">1000m"};
};

class TallLightKit: Supply {
	classname = "BWI_Construction_TallLightKit";
	compatible[] = {">1000m"};
};

class Shelter2: Supply {
	classname = "BWI_Construction_Shelter2";
	compatible[] = {">1000m"};
};

class Shelter4: Supply {
	classname = "BWI_Construction_Shelter4";
	compatible[] = {">1000m"};
};

class TankTrap4: Supply {
	classname = "BWI_Construction_TankTrap4";
	compatible[] = {">1000m"};
};

class TankTrap8: Supply {
	classname = "BWI_Construction_TankTrap8";
	compatible[] = {">1000m"};
};