class BaseWallDesert: Supply {
	classname = "BWI_Construction_BaseWallDesert";
	compatible[] = {">1000m"};
};

class BaseRampartDesert: Supply {
	classname = "BWI_Construction_BaseRampartDesert";
	compatible[] = {">1000m"};
};

class BaseRampartCornerDesert: Supply {
	classname = "BWI_Construction_BaseRampartCornerDesert";
	compatible[] = {">1000m"};
};

class BaseRampartInvCornerDesert: Supply {
	classname = "BWI_Construction_BaseRampartInvCornerDesert";
	compatible[] = {">1000m"};
};

class BaseRampartBunkerDesert: Supply {
	classname = "BWI_Construction_BaseRampartBunkerDesert";
	compatible[] = {">1000m"};
};

class BaseWallCargoDesert: Supply {
	classname = "BWI_Construction_BaseWallCargoDesert";
	compatible[] = {">1000m"};
};

class BaseWallCornerBunkerDesert: Supply {
	classname = "BWI_Construction_BaseWallCornerBunkerDesert";
	compatible[] = {">1000m"};
};

class BaseWallCornerCargoDesert: Supply {
	classname = "BWI_Construction_BaseWallCornerCargoDesert";
	compatible[] = {">1000m"};
};

class BaseSideEntranceDesert: Supply {
	classname = "BWI_Construction_BaseSideEntranceDesert";
	compatible[] = {">1000m"};
};

class BaseMainEntranceBunkerDesert: Supply {
	classname = "BWI_Construction_BaseMainEntranceBunkerDesert";
	compatible[] = {">1000m"};
};

class BaseMainEntranceCargoDesert: Supply {
	classname = "BWI_Construction_BaseMainEntranceCargoDesert";
	compatible[] = {">1000m"};
};

class BaseAmmoDumpDesertWest: Supply {
	classname = "BWI_Construction_BaseAmmoDumpDesertWest";
	compatible[] = {">1000m"};
};

class BaseAmmoDumpDesertEast: Supply {
	classname = "BWI_Construction_BaseAmmoDumpDesertEast";
	compatible[] = {">1000m"};
};

class BaseRampartAmmoDumpDesertWest: Supply {
	classname = "BWI_Construction_BaseRampartAmmoDumpDesertWest";
	compatible[] = {">1000m"};
};

class BaseRampartAmmoDumpDesertEast: Supply {
	classname = "BWI_Construction_BaseRampartAmmoDumpDesertEast";
	compatible[] = {">1000m"};
};

class BaseVehicleResupplyDesertWest: Supply {
	classname = "BWI_Construction_BaseVehicleResupplyDesertWest";
	compatible[] = {">1000m"};
};

class BaseVehicleResupplyDesertEast: Supply {
	classname = "BWI_Construction_BaseVehicleResupplyDesertEast";
	compatible[] = {">1000m"};
};

class BaseFuelBladderDesert: Supply {
	classname = "BWI_Construction_BaseFuelBladderDesert";
	compatible[] = {">1000m"};
};

class BaseHelipadDesert: Supply {
	classname = "BWI_Construction_BaseHelipadDesert";
	compatible[] = {">1000m"};
};

class BaseCargoHouseDesert: Supply {
	classname = "BWI_Construction_BaseCargoHouseDesert";
	compatible[] = {">1000m"};
};

class BaseCargoHQDesert: Supply {
	classname = "BWI_Construction_BaseCargoHQDesert";
	compatible[] = {">1000m"};
};

class BaseCargoTowerDesert: Supply {
	classname = "BWI_Construction_BaseCargoTowerDesert";
	compatible[] = {">1000m"};
};