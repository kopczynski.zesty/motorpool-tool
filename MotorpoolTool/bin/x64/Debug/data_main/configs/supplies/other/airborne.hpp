class Airborne_M855_M136: Supply {
	classname = "BWI_Resupply_Airborne_M855_M136";
};

class Airborne_M855A1_M136: Supply {
	classname = "BWI_Resupply_Airborne_M855A1_M136";
};

class Airborne_M855_M72: Supply {
	classname = "BWI_Resupply_Airborne_M855_M72";
};

class Airborne_M855A1_M72: Supply {
	classname = "BWI_Resupply_Airborne_M855A1_M72";
};

class Airborne_M855_ILAW: Supply {
	classname = "BWI_Resupply_Airborne_M855_ILAW";
};

class Airborne_M855A1_NLAW: Supply {
	classname = "BWI_Resupply_Airborne_M855A1_NLAW";
};




