class Spare_Wheel: Supply {
	classname = "ACE_Wheel";
	compatible[] = {"Wheeled Vehicles"};
};

class Spare_Track: Supply {
	classname = "ACE_Track";
	compatible[] = {"Tracked Vehicles"};
};

class Jerry_Can: Supply {
	classname = "Land_CanisterFuel_F";
	compatible[] = {"All Vehicles"};
};