class AntiTank: Category {
    name = "Anti-Tank";

    #include <at_light.hpp>
	#include <at_medium.hpp>
	#include <at_heavy.hpp>
};

class AntiAir: Category {
    name = "Anti-Air";

    #include <aa_heavy.hpp>
};