class AA_1Rnd_FIM92: Supply {   
	classname = "BWI_Resupply_AA_1Rnd_FIM92";
	compatible[] = {"FIM-92F Stinger"};
	textureIndexes[] = {1,0};
};

class AA_1Rnd_Fliegerfaust: Supply {   
	classname = "BWI_Resupply_AA_1Rnd_Fliegerfaust";
	compatible[] = {"Fliegerfaust 2"};
	textureIndexes[] = {1,0};
};

class AA_1Rnd_9K38: Supply {   
	classname = "BWI_Resupply_AA_1Rnd_9K38";
	compatible[] = {"9K38 Igla"};
	textureIndexes[] = {1,0};
}; 