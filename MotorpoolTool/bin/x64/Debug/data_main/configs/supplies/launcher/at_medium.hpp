class AT_1Rnd_FFV751: Supply {
	classname = "BWI_Resupply_AT_1Rnd_FFV751";
	compatible[] = {"M3 MAAWS"};
	textureIndexes[] = {3,0};
};

class AT_1Rnd_FFV502: Supply {
	classname = "BWI_Resupply_AT_1Rnd_FFV502";
	compatible[] = {"M3 MAAWS"};
	textureIndexes[] = {3,0};
};

class AT_1Rnd_Mk6HEAA: Supply {
	classname = "BWI_Resupply_AT_1Rnd_Mk6HEAA";
	compatible[] = {"SMAW"};
	textureIndexes[] = {3,0};
};

class AT_1Rnd_Mk3HEDP: Supply {
	classname = "BWI_Resupply_AT_1Rnd_Mk3HEDP";
	compatible[] = {"SMAW"};
	textureIndexes[] = {3,0};
};

class AT_1Rnd_PzF3IT: Supply {
	classname = "BWI_Resupply_AT_1Rnd_PzF3IT";
	compatible[] = {"Panzerfaust 3"};
	textureIndexes[] = {3,0};
};

class AT_1Rnd_PzF3BF: Supply {
	classname = "BWI_Resupply_AT_1Rnd_PzF3BF";
	compatible[] = {"Panzerfaust 3"};
	textureIndexes[] = {3,0};
};

class AT_1Rnd_PG7VR: Supply {
	classname = "BWI_Resupply_AT_1Rnd_PG7VR";
	compatible[] = {"RPG-7V2"};
	textureIndexes[] = {3,0};
};

class AT_1Rnd_PG7VL: Supply {
	classname = "BWI_Resupply_AT_1Rnd_PG7VL";
	compatible[] = {"RPG-7V2"};
	textureIndexes[] = {3,0};
};

class AT_1Rnd_PG7V: Supply {
	classname = "BWI_Resupply_AT_1Rnd_PG7V";
	compatible[] = {"RPG-7V2"};
	textureIndexes[] = {3,0};
};

class AT_1Rnd_OG7V: Supply {
	classname = "BWI_Resupply_AT_1Rnd_OG7V";
	compatible[] = {"RPG-7V2"};
	textureIndexes[] = {3,0};
};

class AT_1Rnd_TBG7V: Supply {
	classname = "BWI_Resupply_AT_1Rnd_TBG7V";
	compatible[] = {"RPG-7V2"};
	textureIndexes[] = {3,0};
};
