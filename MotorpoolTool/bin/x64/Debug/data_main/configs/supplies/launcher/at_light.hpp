class AT_Tube_M136_HEAT: Supply {
	classname = "BWI_Resupply_AT_Tube_M136_HEAT";
	textureIndexes[] = {1,0};
};

class AT_Tube_M136_HEDP: Supply {
	classname = "BWI_Resupply_AT_Tube_M136_HEDP";
	textureIndexes[] = {1,0};
};

class AT_Tube_M136_HP: Supply {
	classname = "BWI_Resupply_AT_Tube_M136_HP";
	textureIndexes[] = {1,0};
};

class AT_Tube_M72: Supply {
	classname = "BWI_Resupply_AT_Tube_M72";
	textureIndexes[] = {1,0};
};

class AT_Tube_RGW90: Supply {
	classname = "BWI_Resupply_AT_Tube_RGW90";
	textureIndexes[] = {1,0};
};

class AT_Tube_NLAW: Supply {
	classname = "BWI_Resupply_AT_Tube_NLAW";
	textureIndexes[] = {1,0};
};

class AT_Tube_ILAW_HEDP: Supply {
	classname = "BWI_Resupply_AT_Tube_ILAW_HEDP";
	textureIndexes[] = {1,0};
};

class AT_Tube_ILAW_HP: Supply {
	classname = "BWI_Resupply_AT_Tube_ILAW_HP";
	textureIndexes[] = {1,0};
};

class AT_Tube_M80: Supply {
	classname = "BWI_Resupply_AT_Tube_M80";
	textureIndexes[] = {1,0};
};

class AT_Tube_RPG26: Supply {
	classname = "BWI_Resupply_AT_Tube_RPG26";
	textureIndexes[] = {1,0};
};

class AT_Tube_RSHG2: Supply {
	classname = "BWI_Resupply_AT_Tube_RSHG2";
	textureIndexes[] = {1,0};
};

class AT_Tube_RPG75: Supply {
	classname = "BWI_Resupply_AT_Tube_RPG75";
	textureIndexes[] = {1,0};
};