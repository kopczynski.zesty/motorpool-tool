class Gren_Hand_HE_M67: Supply {
	classname = "BWI_Resupply_Gren_Hand_HE_M67";
	textureIndexes[] = {2,0};
};

class Gren_Hand_WS_ANM8: Supply {
	classname = "BWI_Resupply_Gren_Hand_WS_ANM8";
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_M18: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_M18";
	textureIndexes[] = {2,0};
};

class Gren_Hand_HE_DM51A1: Supply {
	classname = "BWI_Resupply_Gren_Hand_HE_DM51A1";
	textureIndexes[] = {2,0};
};

class Gren_Hand_WS_DM25: Supply {
	classname = "BWI_Resupply_Gren_Hand_WS_DM25";
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_DM32: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_DM32";
	textureIndexes[] = {2,0};
};

class Gren_Hand_WS_L50A1: Supply {
	classname = "BWI_Resupply_Gren_Hand_WS_L50A1";
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_L68: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_L68";
	textureIndexes[] = {2,0};
};

class Gren_Hand_HE_M84: Supply {
	classname = "BWI_Resupply_Gren_Hand_HE_M84";
	textureIndexes[] = {2,0};
};

class Gren_Hand_WS_M83: Supply {
	classname = "BWI_Resupply_Gren_Hand_WS_M83";
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_M83: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_M83";
	textureIndexes[] = {2,0};
};

class Gren_Hand_HE_RGD5: Supply {
	classname = "BWI_Resupply_Gren_Hand_HE_RGD5";
	textureIndexes[] = {2,0};
};

class Gren_Hand_WS_RDG2: Supply {
	classname = "BWI_Resupply_Gren_Hand_WS_RDG2";
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_NSPD: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_NSPD";
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_NSPN: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_NSPN";
	textureIndexes[] = {2,0};
};