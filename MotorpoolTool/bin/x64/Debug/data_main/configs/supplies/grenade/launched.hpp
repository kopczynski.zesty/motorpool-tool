class Gren_40mm_HE_M441: Supply {
	classname = "BWI_Resupply_Gren_40mm_HE_M441";
	compatible[] = {"M203", "M320"};
	textureIndexes[] = {1,0};
};

class Gren_40mm_WS_M714: Supply {
	classname = "BWI_Resupply_Gren_40mm_WS_M714";
	compatible[] = {"M203", "M320"};
	textureIndexes[] = {2,0};
};

class Gren_40mm_CS_M713: Supply {
	classname = "BWI_Resupply_Gren_40mm_CS_M713";
	compatible[] = {"M203", "M320"};
	textureIndexes[] = {2,0};
};

class Gren_40mm_WF_M585: Supply {
	classname = "BWI_Resupply_Gren_40mm_WF_M585";
	compatible[] = {"M203", "M320"};
	textureIndexes[] = {3,0};
};

class Gren_40mm_CF_M661: Supply {
	classname = "BWI_Resupply_Gren_40mm_CF_M661";
	compatible[] = {"M203", "M320"};
	textureIndexes[] = {3,0};
};

class Gren_40mm_HE_Generic: Supply {
	classname = "BWI_Resupply_Gren_40mm_HE_Generic";
	compatible[] = {"AG40"};
	textureIndexes[] = {1,0};
};

class Gren_40mm_WS_Generic: Supply {
	classname = "BWI_Resupply_Gren_40mm_WS_Generic";
	compatible[] = {"AG40"};
	textureIndexes[] = {2,0};
};

class Gren_40mm_CS_Generic: Supply {
	classname = "BWI_Resupply_Gren_40mm_CS_Generic";
	compatible[] = {"AG40"};
	textureIndexes[] = {2,0};
};

class Gren_40mm_WF_Generic: Supply {
	classname = "BWI_Resupply_Gren_40mm_WF_Generic";
	compatible[] = {"AG40"};
	textureIndexes[] = {3,0};
};

class Gren_40mm_CF_Generic: Supply {
	classname = "BWI_Resupply_Gren_40mm_CF_Generic";
	compatible[] = {"AG40"};
	textureIndexes[] = {3,0};
};

class Gren_40mm_HE_L123A2: Supply {
	classname = "BWI_Resupply_Gren_40mm_HE_L123A2";
	compatible[] = {"L123A2"};
	textureIndexes[] = {1,0};
};

class Gren_40mm_WS_L123A2: Supply {
	classname = "BWI_Resupply_Gren_40mm_WS_L123A2";
	compatible[] = {"L123A2"};
	textureIndexes[] = {2,0};
};

class Gren_40mm_CS_L123A2: Supply {
	classname = "BWI_Resupply_Gren_40mm_CS_L123A2";
	compatible[] = {"L123A2"};
	textureIndexes[] = {2,0};
};

class Gren_40mm_WF_L123A2: Supply {
	classname = "BWI_Resupply_Gren_40mm_WF_L123A2";
	compatible[] = {"L123A2"};
	textureIndexes[] = {3,0};
};

class Gren_40mm_CF_L123A2: Supply {
	classname = "BWI_Resupply_Gren_40mm_CF_L123A2";
	compatible[] = {"L123A2"};
	textureIndexes[] = {3,0};
};

class Gren_40mm_HE_VOG25: Supply {
	classname = "BWI_Resupply_Gren_40mm_HE_VOG25";
	compatible[] = {"GP-25", "PBG"};
	textureIndexes[] = {1,0};
};

class Gren_40mm_HE_VOG25TB: Supply {
	classname = "BWI_Resupply_Gren_40mm_HE_VOG25TB";
	compatible[] = {"GP-25", "PBG"};
	textureIndexes[] = {1,0};
};

class Gren_40mm_WS_VG40MD: Supply {
	classname = "BWI_Resupply_Gren_40mm_WS_VG40MD";
	compatible[] = {"GP-25", "PBG"};
	textureIndexes[] = {2,0};
};

class Gren_40mm_CS_VG40MD: Supply {
	classname = "BWI_Resupply_Gren_40mm_CS_VG40MD";
	compatible[] = {"GP-25", "PBG"};
	textureIndexes[] = {2,0};
};

class Gren_40mm_WF_VG40OP: Supply {
	classname = "BWI_Resupply_Gren_40mm_WF_VG40OP";
	compatible[] = {"GP-25", "PBG"};
	textureIndexes[] = {3,0};
};

class Gren_40mm_CF_VG40OP: Supply {
	classname = "BWI_Resupply_Gren_40mm_CF_VG40OP";
	compatible[] = {"GP-25", "PBG"};
	textureIndexes[] = {3,0};
};

class Gren_40mm_HE_MGL: Supply {
	classname = "BWI_Resupply_Gren_40mm_HE_MGL";
	compatible[] = {"M32 MGL"};
	textureIndexes[] = {1,0};
};

class Gren_40mm_HEDP_MGL: Supply {
	classname = "BWI_Resupply_Gren_40mm_HEDP_MGL";
	compatible[] = {"M32 MGL"};
	textureIndexes[] = {1,0};
};

class Gren_40mm_HET_MGL: Supply {
	classname = "BWI_Resupply_Gren_40mm_HET_MGL";
	compatible[] = {"M32 MGL"};
	textureIndexes[] = {1,0};
};

class Gren_40mm_WS_MGL: Supply {
	classname = "BWI_Resupply_Gren_40mm_WS_MGL";
	compatible[] = {"M32 MGL"};
	textureIndexes[] = {2,0};
};

class Gren_40mm_CS_MGL: Supply {
	classname = "BWI_Resupply_Gren_40mm_CS_MGL";
	compatible[] = {"M32 MGL"};
	textureIndexes[] = {2,0};
};

class Gren_40mm_WF_MGL: Supply {
	classname = "BWI_Resupply_Gren_40mm_WF_MGL";
	compatible[] = {"M32 MGL"};
	textureIndexes[] = {2,0};
};

class Gren_40mm_CF_MGL: Supply {
	classname = "BWI_Resupply_Gren_40mm_CF_MGL";
	compatible[] = {"M32 MGL"};
	textureIndexes[] = {2,0};
};