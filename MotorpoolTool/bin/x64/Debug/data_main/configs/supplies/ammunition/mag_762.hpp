class Mag_20Rnd_M118: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_M118";
	compatible[] = {"M14", "M14-EBR"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_M993: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_M993";
	compatible[] = {"M14", "M14-EBR"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_M14: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_M14";
	compatible[] = {"M14", "M21"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_M118LR: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_M118LR";
	compatible[] = {"HK417"};
	textureIndexes[] = {1,0};
};

class Mag_5Rnd_Mk248: Supply {
	classname = "BWI_Resupply_Mag_5Rnd_Mk248";
	compatible[] = {"M2010 ESR"};
	textureIndexes[] = {1,0};
};

class Mag_5Rnd_M118: Supply {
	classname = "BWI_Resupply_Mag_5Rnd_M118";
	compatible[] = {"M24 SWS", "M40A5"};
	textureIndexes[] = {1,0};
};

class Mag_5Rnd_M993: Supply {
	classname = "BWI_Resupply_Mag_5Rnd_M993";
	compatible[] = {"M24 SWS", "M40A5"};
	textureIndexes[] = {1,0};
};

class Mag_10Rnd_M118: Supply {
	classname = "BWI_Resupply_Mag_10Rnd_M118";
	compatible[] = {"M40A5"};
	textureIndexes[] = {1,0};
};

class Mag_10Rnd_M993: Supply {
	classname = "BWI_Resupply_Mag_10Rnd_M993";
	compatible[] = {"M40A5"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_M118_SR25: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_M118_SR25";
	compatible[] = {"Mk 11 Mod 0"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_M993_SR25: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_M993_SR25";
	compatible[] = {"Mk 11 Mod 0"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_G27: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_G27";
	compatible[] = {"G27"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_G27_AP: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_G27_AP";
	compatible[] = {"G27"};
	textureIndexes[] = {1,0};
};

class Mag_10Rnd_G28: Supply {
	classname = "BWI_Resupply_Mag_10Rnd_G28";
	compatible[] = {"G28"};
	textureIndexes[] = {1,0};
};

class Mag_10Rnd_G28_AP: Supply {
	classname = "BWI_Resupply_Mag_10Rnd_G28_AP";
	compatible[] = {"G28"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_L42A1: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_L42A1";
	compatible[] = {"L129A1"};
	textureIndexes[] = {1,0};
};

class Mag_10Rnd_L42A1: Supply {
	classname = "BWI_Resupply_Mag_10Rnd_L42A1";
	compatible[] = {"L118A1"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_M67: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M67";
	compatible[] = {"M70B"};
	textureIndexes[] = {1,0};
};

class Mag_10Rnd_7N14: Supply {
	classname = "BWI_Resupply_Mag_10Rnd_7N14";
	compatible[] = {"SVDM", "SVDS"};
	textureIndexes[] = {1,0};
};

class Mag_10Rnd_7N1: Supply {
	classname = "BWI_Resupply_Mag_10Rnd_7N1";
	compatible[] = {"SVDM", "SVDS"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_57N231: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_57N231";
	compatible[] = {"AK-103", "AK-104", "AKM", "AKMN", "AKMS"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_57N231P: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_57N231P";
	compatible[] = {"AK-103", "AK-104", "AKM", "AKMN", "AKMS"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_57N231_Bakelite: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_57N231_Bakelite";
	compatible[] = {"AK-103", "AK-104", "AKM", "AKMN", "AKMS"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_57N231P_Bakelite: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_57N231P_Bakelite";
	compatible[] = {"AK-103", "AK-104", "AKM", "AKMN", "AKMS"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_57N231_Polymer: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_57N231_Polymer";
	compatible[] = {"AK-103", "AK-104", "AKM", "AKMN", "AKMS"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_57N231P_Polymer: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_57N231P_Polymer";
	compatible[] = {"AK-103", "AK-104", "AKM", "AKMN", "AKMS"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_SaVz58: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_SaVz58";
	compatible[] = {"Sa vz. 58P", "Sa vz. 58V"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_FNFAL: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_FNFAL";
	compatible[] = {"FN FAL"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_M80_SCAR_H: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_M80_SCAH_H";
	compatible[] = {"SCAR-H"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_M80_SCAR_H_T: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_M80_SCAH_H_T";
	compatible[] = {"SCAR-H"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_Mk316_SCAR_H: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_Mk316_SCAH_H";
	compatible[] = {"SCAR-H"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_Mk316_SCAR_H_T: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_Mk316_SCAH_H_T";
	compatible[] = {"SCAR-H"};
	textureIndexes[] = {1,0};
};

class Mag_5Rnd_Mosin: Supply {
	classname = "BWI_Resupply_Mag_5Rnd_Mosin";
	compatible[] = {"Mosin Nagant"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_G3: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_G3";
	compatible[] = {"G3A3", "G3KA4"};
	textureIndexes[] = {1,0};
};

class Mag_20Rnd_PSG1: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_PSG1";
	compatible[] = {"PGS1"};
	textureIndexes[] = {1,0};
};