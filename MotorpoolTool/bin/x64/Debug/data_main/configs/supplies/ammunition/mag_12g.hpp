class Mag_8Rnd_Buck: Supply {
	classname = "BWI_Resupply_Mag_8Rnd_Buck";
	compatible[] = {"M590A1 (Long)"};
	textureIndexes[] = {2,0};
};

class Mag_8Rnd_Slug: Supply {
	classname = "BWI_Resupply_Mag_8Rnd_Slug";
	compatible[] = {"M590A1 (Long)"};
	textureIndexes[] = {2,0};
};

class Mag_5Rnd_Buck: Supply {
	classname = "BWI_Resupply_Mag_5Rnd_Buck";
	compatible[] = {"M590A1 (Short)"};
	textureIndexes[] = {2,0};
};

class Mag_5Rnd_Slug: Supply {
	classname = "BWI_Resupply_Mag_5Rnd_Slug";
	compatible[] = {"M590A1 (Short)"};
	textureIndexes[] = {2,0};
};

class Mag_8Rnd_L128_Buck: Supply {
	classname = "BWI_Resupply_Mag_8Rnd_L128_Buck";
	compatible[] = {"L128A1"};
	textureIndexes[] = {2,0};
};

class Mag_8Rnd_L128_Slug: Supply {
	classname = "BWI_Resupply_Mag_8Rnd_L128_Slug";
	compatible[] = {"L128A1"};
	textureIndexes[] = {2,0};
};
