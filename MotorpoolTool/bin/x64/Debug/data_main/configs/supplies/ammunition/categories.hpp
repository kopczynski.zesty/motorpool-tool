class Ammunition: Category {
    name = "Ammunition";

    #include <mag_556.hpp>
    #include <mag_545.hpp>
    #include <mag_762.hpp>

	#include <box_556.hpp>
	#include <box_545.hpp>
	#include <box_762.hpp>

	#include <mag_12g.hpp>
    #include <mag_9.hpp>

    #include <mag_misc.hpp>
};