class Mag_40Rnd_MP7SX: Supply {
	classname = "BWI_Resupply_Mag_40Rnd_MP7SX";
	compatible[] = {"MP7A2"};
	textureIndexes[] = {3,0};
};

class Mag_5Rnd_338_LM: Supply {
	classname = "BWI_Resupply_Mag_5Rnd_338_LM";
	compatible[] = {"L115A3"};
	textureIndexes[] = {3,0};
};

class Mag_10Rnd_M70: Supply {
	classname = "BWI_Resupply_Mag_10Rnd_M70";
	compatible[] = {"M76"};
	textureIndexes[] = {3,0};
};

class Mag_5Rnd_338_T5000: Supply {
	classname = "BWI_Resupply_Mag_5Rnd_338_T5000";
	compatible[] = {"T-5000"};
	textureIndexes[] = {3,0};
};

class Mag_30Rnd_M3A1: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M3A1";
	compatible[] = {"M3A1 Grease Gun"};
	textureIndexes[] = {2,0};
};

class Mag_10Rnd_Enfield: Supply {
	classname = "BWI_Resupply_Mag_10Rnd_Enfield";
	compatible[] = {"Lee Enfield"};
	textureIndexes[] = {3,0};
};

class Mag_8Rnd_M1Garand: Supply {
	classname = "BWI_Resupply_Mag_8Rnd_M1Garand";
	compatible[] = {"M1 Garand"};
	textureIndexes[] = {3,0};
};

class Mag_50Rnd_MG42: Supply {
	classname = "BWI_Resupply_Mag_50Rnd_MG42";
	compatible[] = {"MG42"};
	textureIndexes[] = {3,0};
};

class Mag_296Rnd_MG42: Supply {
	classname = "BWI_Resupply_Mag_296Rnd_MG42";
	compatible[] = {"MG42"};
	textureIndexes[] = {3,0};
};