class Mag_30Rnd_7N10: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_7N10";
	compatible[] = {"AK-74", "AKS-74"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_7U1: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_7U1";
	compatible[] = {"AK-74", "AKS-74"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_7N10_Plum: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_7N10_Plum";
	compatible[] = {"AK-74", "AKS-74"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_7N10_Camo: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_7N10_Camo";
	compatible[] = {"AK-74", "AKS-74"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_7N10_Desert: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_7N10_Desert";
	compatible[] = {"AK-74", "AKS-74"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_7N6_Bakelite: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_7N6_Bakelite";
	compatible[] = {"AK-74", "AKS-74"};
	textureIndexes[] = {1,0};
};