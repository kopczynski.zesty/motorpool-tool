class Box_200Rnd_M855: Supply {
	classname = "BWI_Resupply_Box_200Rnd_M855";
	compatible[] = {"M249", "L110"};
	textureIndexes[] = {1,0};
};

class Box_200Rnd_M855A1: Supply {
	classname = "BWI_Resupply_Box_200Rnd_M855A1";
	compatible[] = {"M249", "L110"};
	textureIndexes[] = {1,0};
};

class Box_200Rnd_M855_SP_W: Supply {
	classname = "BWI_Resupply_Box_200Rnd_M855_SP_W";
	compatible[] = {"M249", "L110"};
	textureIndexes[] = {1,0};
};

class Box_200Rnd_M855A1_SP_W: Supply {
	classname = "BWI_Resupply_Box_200Rnd_M855A1_SP_W";
	compatible[] = {"M249", "L110"};
	textureIndexes[] = {1,0};
};

class Box_200Rnd_M855_SP_C: Supply {
	classname = "BWI_Resupply_Box_200Rnd_M855_SP_C";
	compatible[] = {"M249", "L110"};
	textureIndexes[] = {1,0};
};

class Box_200Rnd_M855A1_SP_C: Supply {
	classname = "BWI_Resupply_Box_200Rnd_M855A1_SP_C";
	compatible[] = {"M249", "L110"};
	textureIndexes[] = {1,0};
};

class Box_200Rnd_M855_SP_U: Supply {
	classname = "BWI_Resupply_Box_200Rnd_M855_SP_U";
	compatible[] = {"M249", "L110"};
	textureIndexes[] = {1,0};
};

class Box_200Rnd_M855A1_SP_U: Supply {
	classname = "BWI_Resupply_Box_200Rnd_M855A1_SP_U";
	compatible[] = {"M249", "L110"};
	textureIndexes[] = {1,0};
};

class Box_100Rnd_M855A1_CMAG: Supply {
	classname = "BWI_Resupply_Box_100Rnd_M855A1_CMAG";
	compatible[] = {"M27 IAR"};
	textureIndexes[] = {1,0};
};

class Box_100Rnd_M855A1_G36: Supply {
	classname = "BWI_Resupply_Mag_100Rnd_M855A1_G36";
	compatible[] = {"G36C", "G36KV"};
	textureIndexes[] = {1,0};
};

class Box_200Rnd_MG4: Supply {
	classname = "BWI_Resupply_Box_200Rnd_MG4";
	compatible[] = {"MG4"};
	textureIndexes[] = {1,0};
};