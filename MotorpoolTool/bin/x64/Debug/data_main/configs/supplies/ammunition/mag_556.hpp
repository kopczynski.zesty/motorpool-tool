class Mag_30Rnd_M855: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M855";
	compatible[] = {"M4", "M16", "L119A1", "L85A2"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_M855A1: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M855A1";
	compatible[] = {"M4", "M16", "L119A1", "L85A2"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_Mk262: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_Mk262";
	compatible[] = {"M4", "M16", "L119A1", "L119A1", "L85A2"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_Mk318: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_Mk318";
	compatible[] = {"M4", "M16", "L119A1", "L119A1", "L85A2"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_M855_PMAG: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M855_PMAG";
	compatible[] = {"M4", "M16"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_M855A1_PMAG: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M855A1_PMAG";
	compatible[] = {"M4", "M16"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_M855_PMAG_T: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M855_PMAG_T";
	compatible[] = {"M4", "M16"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_M855A1_PMAG_T: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M855A1_PMAG_T";
	compatible[] = {"M4", "M16", "L119A1"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_Mk262_PMAG: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_Mk262_PMAG";
	compatible[] = {"M4", "M16"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_Mk262_PMAG_T: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_Mk262_PMAG_T";
	compatible[] = {"M4", "M16"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_Mk318_PMAG: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_Mk318_PMAG";
	compatible[] = {"M4", "M16"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_Mk318_PMAG_T: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_Mk318_PMAG_T";
	compatible[] = {"M4", "M16"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_DM11: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_DM11";
	compatible[] = {"G36", "G38"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_DM11_IR: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_DM11_IR";
	compatible[] = {"G36A", "G36KA", "G38"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_DM31: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_DM31";
	compatible[] = {"G36", "G38"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_M855_M21: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M855_M21";
	compatible[] = {"M21A", "M21S"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_M855A1_G36: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M855A1_G36";
	compatible[] = {"G36C", "G36KV"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_M855A1_VHS: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M855_VHS";
	compatible[] = {"VHS-D2", "VHS-K2"};
	textureIndexes[] = {1,0};
};

class Mag_30Rnd_M855A1_HK33: Supply {
	classname = "BWI_Resupply_Mag_30Rnd_M855_HK33";
	compatible[] = {"HK33"};
	textureIndexes[] = {1,0};
};