class Explosive_M112: Supply {
	classname = "BWI_Resupply_Explosive_M112";
	textureIndexes[] = {3,0};
};

class Explosive_M183: Supply {
	classname = "BWI_Resupply_Explosive_M183";
	textureIndexes[] = {3,0};
};

class Explosive_TNT400: Supply {
	classname = "BWI_Resupply_Explosive_TNT400";
	textureIndexes[] = {3,0};
};

class Explosive_IED_Small: Supply {
	classname = "BWI_Resupply_Explosive_IED_Small";
	textureIndexes[] = {3,0};
};

class Explosive_IED_Large: Supply {
	classname = "BWI_Resupply_Explosive_IED_Large";
	textureIndexes[] = {3,0};
};


