class Explosive_M6SLAM: Supply {
	classname = "BWI_Resupply_Explosive_M6SLAM";
	textureIndexes[] = {3,0};
};

class Explosive_M26: Supply {
	classname = "BWI_Resupply_Explosive_M26";
	textureIndexes[] = {3,0};
};

class Explosive_M15: Supply {
	classname = "BWI_Resupply_Explosive_M15";
	textureIndexes[] = {3,0};
};

class Explosive_M18A1: Supply {
	classname = "BWI_Resupply_Explosive_M18A1";
	textureIndexes[] = {3,0};
};

class Explosive_PMR3M: Supply {
	classname = "BWI_Resupply_Explosive_PMR3M";
	textureIndexes[] = {3,0};
};

class Explosive_PMR3F: Supply {
	classname = "BWI_Resupply_Explosive_PMR3F";
	textureIndexes[] = {3,0};
};