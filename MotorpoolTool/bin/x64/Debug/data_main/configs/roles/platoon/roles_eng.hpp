class DEF: Role
{
    name = "Defence Engineer";
    callsign = "Engineer";
    showOnCommcard = 1;

    acreRadioChannels[] = {0,0,1,0};

    allowedAccessories[] = {2,3,1,1};
};

class MEC: Role
{
    name = "Vehicle Mechanic";
    callsign = "Mechanic";
    showOnCommcard = 1;

    aceIsEngineer = 2;

    acreRadioChannels[] = {0,0,1,0};

    allowedAccessories[] = {2,3,1,1};
};