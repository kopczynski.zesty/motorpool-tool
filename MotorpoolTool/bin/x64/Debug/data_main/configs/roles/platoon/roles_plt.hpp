class LDR: Role
{
    name = "Leader (AT)";
    showOnCommcard = 1;

    acreRadioChannels[] = {0,2,0,1};

    allowedAccessories[] = {3,3,1,1};
};

class LDG: Role
{
    name = "Leader (GL)";
    showOnCommcard = 1;

    acreRadioChannels[] = {0,2,0,1};

    allowedAccessories[] = {3,3,1,1};
};