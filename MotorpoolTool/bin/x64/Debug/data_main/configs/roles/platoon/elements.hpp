class PLT: Element
{
    name = "Platoon";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Platoon"};

    #include <roles_plt.hpp>
};

class LOG: Element
{
    name = "Logistics";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Logistics"};

    #include <roles_log.hpp>
};

class MED: Element
{
    name = "Medical";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Medical"};

    #include <roles_med.hpp>
};

class ENG: Element
{
    name = "Engineering";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Engineering"};

    #include <roles_eng.hpp>
};

class TAC: Element
{
    name = "TACP";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"TACP"};

    #include <roles_tac.hpp>
};