class LDR: Role
{
    name = "Leader";
    showOnCommcard = 1;

    aceIsMedic = 2;

    acreRadioChannels[] = {4,0,1,0};

    allowedAccessories[] = {2,3,1,1};
};

class CPM: Role
{
    name = "Corpsman";

    aceIsMedic = 2;

    acreRadioChannels[] = {4,0,0,0};

    allowedAccessories[] = {2,3,1,1};
};