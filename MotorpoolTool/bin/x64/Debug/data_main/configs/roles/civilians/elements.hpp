class CIV: Element
{
    name = "Civilians";
    sides[] = {CIVIL};
    callsigns[] = {"Civilian"};

    #include <roles.hpp>
};