class LDR: Role
{
    name = "Leader (AT)";
    showOnCommcard = 1;

    aceStaminaFactor = 2.0;

    acreRadioChannels[] = {3,0,1,0};

    allowedAccessories[] = {4,3,1,2};
};

class LDG: Role
{
    name = "Leader (GL)";
    showOnCommcard = 1;

    aceStaminaFactor = 2.0;

    acreRadioChannels[] = {3,0,1,0};

    allowedAccessories[] = {4,3,1,2};
};

class LMG: Role
{
    name = "Automatic Rifleman";

    aceStaminaFactor = 2.0;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {4,3,2,2};
};

class LAT: Role
{
    name = "Anti-Tank Rifleman / Corpsman";

    aceIsMedic = 2;

    aceStaminaFactor = 2.0;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {4,3,1,2};
};

class SAP: Role
{
    name = "Sapper";

    aceStaminaFactor = 2.0;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {4,3,1,2};
};

class BRE: Role
{
    name = "Breacher";

    aceStaminaFactor = 2.0;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {4,3,1,2};
};

class DMR: Role
{
    name = "Designated Marksman / JTAC";

    aceStaminaFactor = 2.0;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {8,3,2,2};
};

class RIF: Role
{
    name = "Rifleman";
    isHidden = 1;

    aceStaminaFactor = 2.0;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {4,3,1,2};
};