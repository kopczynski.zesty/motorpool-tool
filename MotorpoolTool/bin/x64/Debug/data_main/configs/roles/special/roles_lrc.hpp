class LDR: Role
{
    name = "Leader";
    showOnCommcard = 1;

    aceStaminaFactor = 1.5;

    acreRadioChannels[] = {3,0,1,0};

    allowedAccessories[] = {4,3,1,2};
};

class LMG: Role
{
    name = "Automatic Rifleman";

    aceStaminaFactor = 1.5;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {4,3,2,2};
};

class LAT: Role
{
    name = "Anti-Tank Rifleman";

    aceStaminaFactor = 1.5;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {4,3,1,2};
};

class UAV: Role
{
    name = "UAV Recon";

    aceStaminaFactor = 1.5;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {4,3,1,2};
};

class HIR: Role
{
    name = "HuntIR Recon";

    aceStaminaFactor = 1.5;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {4,3,1,2};
};

class DMR: Role
{
    name = "Designated Marksman";

    aceStaminaFactor = 1.5;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {8,3,2,2};
};

class RIF: Role
{
    name = "Rifleman";
    isHidden = 1;

    aceStaminaFactor = 1.5;

    acreRadioChannels[] = {3,0,0,0};

    allowedAccessories[] = {4,3,1,2};
};