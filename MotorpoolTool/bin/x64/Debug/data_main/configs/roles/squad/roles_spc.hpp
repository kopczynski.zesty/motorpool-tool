class DEM: Role
{
    name = "Demolitions Engineer";

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};

class EOD: Role
{
    name = "Explosive Ordnance Disposal";

    aceIsEOD = 1;

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};