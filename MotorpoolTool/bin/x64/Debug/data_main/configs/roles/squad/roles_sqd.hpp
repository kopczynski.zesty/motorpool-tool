class LDR: Role
{
    name = "Leader (AT)";
    showOnCommcard = 1;
    
    acreRadioChannels[] = {1,0,1,0};

    allowedAccessories[] = {3,3,1,1};
};

class LDG: Role
{
    name = "Leader (GL)";
    showOnCommcard = 1;
    
    acreRadioChannels[] = {1,0,1,0};

    allowedAccessories[] = {3,3,1,1};
};

class LMG: Role
{
    name = "Automatic Rifleman";

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,2,1};
};

class AMG: Role
{
    name = "Assistant Automatic Rifleman";

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};

class LHG: Role
{
    name = "Heavy Grenadier";

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};

class AHG: Role
{
    name = "Assistant Heavy Grenadier";

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};

class LAT: Role
{
    name = "Anti-Tank Rifleman";

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};

class GRE: Role
{
    name = "Grenadier";

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};

class SAP: Role
{
    name = "Sapper";

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};

class BRE: Role
{
    name = "Breacher";

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};

class INT: Role
{
    name = "Interpreter";

    acreBabelInterpreter = 1;
    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};

class RIF: Role
{
    name = "Rifleman";
    isHidden = 1;

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};