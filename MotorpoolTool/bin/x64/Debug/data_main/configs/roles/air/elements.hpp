class FWC: Element
{
    name = "Air Crew (Fixed CAS)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Eagle 1", "Eagle 2", "Eagle 3", "Eagle 4", "Eagle 5", "Eagle 6"};

    #include <roles.hpp> // Shared roles
};

class FWT: Element
{
    name = "Air Crew (Fixed Transport)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Condor 1", "Condor 2", "Condor 3", "Condor 4", "Condor 5", "Condor 6"};

    #include <roles.hpp> // Shared roles
};

class RWC: Element
{
    name = "Air Crew (Rotary CAS)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Falcon 1", "Falcon 2", "Falcon 3", "Falcon 4", "Falcon 5", "Falcon 6"};

    #include <roles.hpp> // Shared roles
};

class RWT: Element
{
    name = "Air Crew (Rotary Transport)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Sparrow 1", "Sparrow 2", "Sparrow 3", "Sparrow 4", "Sparrow 5", "Sparrow 6"};

    #include <roles.hpp> // Shared roles
};

class UWC: Element
{
    name = "Air Crew (Unmanned CAS)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Merlin 1", "Merlin 2", "Merlin 3", "Merlin 4", "Merlin 5", "Merlin 6"};

    #include <roles_uwc.hpp> // Unique role, has radio.
};