class CMD: Role
{
    name = "Commander";

    allowedAccessories[] = {1,1,1,1};
};

class GUN: Role
{
    name = "Gunner";

    allowedAccessories[] = {1,1,1,1};
};

class DRV: Role
{
    name = "Driver";

    allowedAccessories[] = {1,1,1,1};
};