/**
 * Bipods are only offered for weapons 
 * that can be used as marksman rifles.
 */
class rhs_acc_harris_swivel: Accessory {
    year = 2000;
    
    compatibleClasses[] = {
        "rhs_weap_m14ebrri",
        "rhs_weap_XM2010",
        "rhs_weap_XM2010_d",
        "rhs_weap_XM2010_sa",
        "rhs_weap_XM2010_wd",
        "rhs_weap_t5000",
		"arifle_SPAR_03_blk_F",
		"rhs_weap_SCARH_LB",
		"rhs_weap_SCARH_FDE_LB",
		"UK3CB_PSG1A1",
		"UK3CB_PSG1A1_RIS"
    };
};

class rhsusf_acc_harris_swivel: Accessory {
    year = 2000;
    
    compatibleClasses[] = {
		"rhs_weap_m24sws",
		"rhs_weap_m24sws_d",
		"rhs_weap_m24sws_wd",
		"rhs_weap_m40a5",
		"rhs_weap_m40a5_d",
		"rhs_weap_m40a5_wd"
    };
};

class rhsusf_acc_harris_bipod: Accessory {
    year = 1990;
    
    compatibleClasses[] = {
        "rhs_weap_m27iar",
        "rhs_weap_m14ebrri",
        "rhs_weap_XM2010",
        "rhs_weap_XM2010_d",
        "rhs_weap_XM2010_sa",
        "rhs_weap_XM2010_wd",
        "rhs_weap_sr25",
        "rhs_weap_sr25_ec",
        "rhs_weap_sr25_d",
        "rhs_weap_sr25_ec_d",
        "rhs_weap_sr25_wd",
        "rhs_weap_sr25_ec_wd",
        "rhs_weap_t5000"
    };
};

class bipod_01_f_blk: Accessory { 
    year = 2010; 
    
    compatibleClasses[] = {
        "rhs_weap_XM2010",
        "rhs_weap_sr25",
        "rhs_weap_sr25_ec",
		"rhs_weap_SCARH_LB"
    };    
};

class bipod_01_f_snd: Accessory { 
    year = 2010; 
    
    compatibleClasses[] = {
        "rhs_weap_XM2010_d",
        "rhs_weap_sr25_d",
        "rhs_weap_sr25_ec_d",
		"rhs_weap_SCARH_FDE_LB"
    };  
};

class bipod_03_f_oli: Accessory { 
    year = 2010; 
    
    compatibleClasses[] = {
        "rhs_weap_XM2010_sa",
        "rhs_weap_sr25_wd",
        "rhs_weap_sr25_ec_wd"
    }; 
};

class bipod_02_f_tan: Accessory { 
    year = 2010; 
    
    compatibleClasses[] = {
        "rhs_weap_XM2010_wd"
    }; 
};

class bwa3_bipod_mg3: Accessory { 
    year = 1960;

    compatibleClasses[] = {
        "BWA3_MG3"
    };
};

class uk3cb_underbarrel_acc_bipod: Accessory       { year = 2010; compatibleClasses[] = {"UK3CB_BAF_L129A1"}; };
class uk3cb_underbarrel_acc_fgrip_bipod: Accessory { year = 2010; compatibleClasses[] = {"UK3CB_BAF_L129A1"}; };

class rhsusf_acc_m14_bipod: Accessory { 
    year = 1990;

    compatibleClasses[] = {
        "rhs_weap_m14",
        "rhs_weap_m14_d",
        "rhs_weap_m14_wd",
        "rhs_weap_m14_fiberglass",
        "rhs_weap_m14_rail",
        "rhs_weap_m14_rail_d",
        "rhs_weap_m14_rail_wd",
        "rhs_weap_m14_rail_fiberglass",
        "rhs_weap_m14_ris",
        "rhs_weap_m14_ris_d",
        "rhs_weap_m14_ris_wd",
        "rhs_weap_m14_ris_fiberglass",
        "rhs_weap_m14_socom",
        "rhs_weap_m14_socom_rail"
    };
};