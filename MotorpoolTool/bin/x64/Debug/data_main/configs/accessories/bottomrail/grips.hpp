/**
 * Grips are purely aesthetic. No more
 * than 2-3 should be offered per-weapon.
 */
class rhsusf_acc_grip1: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m27iar",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhs_weap_ak74mr",
        "rhs_weap_ak105_zenitco01_b33",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_ct15x",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsd2_bg_ct15x",
		"rhs_weap_vhsk2",
		"rhs_weap_SCARH_CQC",
		"rhs_weap_SCARH_STD",
		"rhs_weap_SCARH_FDE_CQC",
		"rhs_weap_SCARH_FDE_STD"
    };
};

class rhsusf_acc_grip2: Accessory {
    year = 2008;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_mk18",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_ct15x",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsd2_bg_ct15x",
		"rhs_weap_vhsk2",
		"rhs_weap_SCARH_CQC",
		"rhs_weap_SCARH_STD"
    };
};

class rhsusf_acc_grip2_tan: Accessory {
    year = 2008;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_mk18_d",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
		"rhs_weap_SCARH_FDE_CQC",
		"rhs_weap_SCARH_FDE_STD",
		"rhs_weap_SCARH_FDE_LB"
    };
};

class rhsusf_acc_grip2_wd: Accessory {
    year = 2008;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_mk18_wd",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2"
    };
};

class rhsusf_acc_grip3: Accessory {
    year = 2012;
    
    compatibleClasses[] = {
        "rhs_weap_mk18",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
        "rhs_weap_m27iar",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhs_weap_ak74mr",
        "rhs_weap_ak105_zenitco01_b33"
    };
};

class rhsusf_acc_grip4: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg"
    };
};

class rhs_acc_grip_ffg2: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhsusf_weap_MP7A2",
        "rhsusf_weap_MP7A2_aor1",
        "rhsusf_weap_MP7A2_desert",
        "rhsusf_weap_MP7A2_winter"
    };
};

class rhsusf_acc_rvg_blk: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhsusf_weap_MP7A2",
        "rhsusf_weap_MP7A2_winter"
    };
};

class rhsusf_acc_rvg_de: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhsusf_weap_MP7A2_aor1",
        "rhsusf_weap_MP7A2_desert"
    };
};

class rhsusf_acc_tdstubby_blk: Accessory {
    year = 2012;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_blockII",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_mk18",
        "rhs_weap_mk18_wd",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhsusf_weap_MP7A2",
        "rhsusf_weap_MP7A2_winter",
        "rhs_weap_ak74mr",
        "rhs_weap_ak105_zenitco01_b33"
    };
};

class rhsusf_acc_tdstubby_tan: Accessory {
    year = 2012;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_d",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhsusf_weap_MP7A2_aor1",
        "rhsusf_weap_MP7A2_desert"
    };
};

class uk3cb_underbarrel_acc_afg: Accessory   { year = 2010; compatibleClasses[] = {"UK3CB_BAF_L129A1"}; };
class uk3cb_underbarrel_acc_fgrip: Accessory { year = 2010; compatibleClasses[] = {"UK3CB_BAF_L129A1"}; };