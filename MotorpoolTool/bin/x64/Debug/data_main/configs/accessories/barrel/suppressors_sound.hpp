/**
 * Suppressor aren't aesthetic except to each
 * other. No more than 2-3 should be offered 
 * per-weapon.
 */
class rhsusf_acc_nt4_black: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_wd",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "rhs_weap_g36kv",
        "rhs_weap_g36c",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_ct15x",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsd2_bg_ct15x",
		"rhs_weap_vhsk2"
    };
};

class rhsusf_acc_nt4_tan: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2"
    };
};

class rhsusf_acc_rotex5_grey: Accessory {
    year = 1990;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
		"UK3CB_M16_Carbine",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "rhs_weap_g36kv",
        "rhs_weap_g36c"
    };
};

class rhsusf_acc_m2010s: Accessory    { year = 2010; compatibleClasses[] = {"rhs_weap_XM2010"};    };
class rhsusf_acc_m2010s_d: Accessory  { year = 2010; compatibleClasses[] = {"rhs_weap_XM2010_d"};  };
class rhsusf_acc_m2010s_sa: Accessory { year = 2010; compatibleClasses[] = {"rhs_weap_XM2010_sa"}; };
class rhsusf_acc_m2010s_wd: Accessory { year = 2010; compatibleClasses[] = {"rhs_weap_XM2010_wd"}; };

class rhsusf_acc_rotex_mp7: Accessory        { year = 2001; compatibleClasses[] = {"rhsusf_weap_MP7A2"};        };
class rhsusf_acc_rotex_mp7_aor1: Accessory   { year = 2001; compatibleClasses[] = {"rhsusf_weap_MP7A2_aor1"};   };
class rhsusf_acc_rotex_mp7_desert: Accessory { year = 2001; compatibleClasses[] = {"rhsusf_weap_MP7A2_desert"}; };
class rhsusf_acc_rotex_mp7_winter: Accessory { year = 2001; compatibleClasses[] = {"rhsusf_weap_MP7A2_winter"}; };

class rhsusf_acc_m24_silencer_black: Accessory { year = 1990; compatibleClasses[] = {"rhs_weap_m24sws"};    };
class rhsusf_acc_m24_silencer_d: Accessory     { year = 1990; compatibleClasses[] = {"rhs_weap_m24sws_d"};  };
class rhsusf_acc_m24_silencer_wd: Accessory    { year = 1990; compatibleClasses[] = {"rhs_weap_m24sws_wd"}; };

class rhsusf_acc_sr25s: Accessory    { year = 1990; compatibleClasses[] = {"rhs_weap_sr25", "rhs_weap_sr25_ec"};       };
class rhsusf_acc_sr25s_d: Accessory  { year = 1990; compatibleClasses[] = {"rhs_weap_sr25_d", "rhs_weap_sr25_ec_d"};   };
class rhsusf_acc_sr25s_wd: Accessory { year = 1990; compatibleClasses[] = {"rhs_weap_sr25_wd", "rhs_weap_sr25_ec_wd"}; };

class muzzle_snds_b: Accessory    { year = 2001; compatibleClasses[] = {"arifle_SPAR_03_blk_F"}; };
class muzzle_snds_m: Accessory {
	year = 2001;
    
    compatibleClasses[] = {
        "arifle_TRG20_F",
        "arifle_TRG21_GL_F",
        "arifle_TRG21_F"
    };
};

class rhs_acc_pbs1: Accessory {
    year = 1990;
    
    compatibleClasses[] = {
        "rhs_weap_m70b1n",
        "rhs_weap_m70ab2",
        "rhs_weap_m70b3n",
        "rhs_weap_m70b3n_pbg40",
        "rhs_weap_m76",
		"rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak104_zenitco01_b33"
    };
};

class bwa3_muzzle_snds_qdss: Accessory { 
    year = 2003; 

    compatibleClasses[] = {
        "BWA3_G36A1",
        "BWA3_G36A1_AG40",
        "BWA3_G36A2",
        "BWA3_G36A2_AG40",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA1",
        "BWA3_G36KA2",
        "BWA3_G36KA3",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_MG4"
    };
};

class bwa3_muzzle_snds_qdss_tan: Accessory { 
    year = 2003; 
    
    compatibleClasses[] = {
        "BWA3_G36A1_tan",
        "BWA3_G36A1_AG40_tan",
        "BWA3_G36A2_tan",
        "BWA3_G36A2_AG40_tan",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA1_tan",
        "BWA3_G36KA2_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan"
    };
};

class bwa3_muzzle_snds_rotex_iiic: Accessory { 
    year = 1990; 

    compatibleClasses[] = {
        "BWA3_G36A1",
        "BWA3_G36A1_AG40",
        "BWA3_G36A2",
        "BWA3_G36A2_AG40",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA1",
        "BWA3_G36KA2",
        "BWA3_G36KA3",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_MG4"
    };
};

class bwa3_muzzle_snds_rotex_iiic_tan: Accessory { 
    year = 1990; 

    compatibleClasses[] = {
        "BWA3_G36A1_tan",
        "BWA3_G36A1_AG40_tan",
        "BWA3_G36A2_tan",
        "BWA3_G36A2_AG40_tan",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA1_tan",
        "BWA3_G36KA2_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan"
    };
};

class bwa3_muzzle_snds_rotex_iia: Accessory { 
    year = 1990; 

    compatibleClasses[] = {
        "BWA3_G27",
        "BWA3_G27_tan",
        "BWA3_G28",
        "BWA3_G28_Patrol",
        "BWA3_MG5"
    };
};

class uk3cb_baf_silencer_l115a3: Accessory { 
    year = 1996; 

    compatibleClasses[] = {
        "UK3CB_BAF_L129A1",
        "UK3CB_BAF_L115A3",
        "UK3CB_BAF_L115A3_BL",
        "UK3CB_BAF_L115A3_DE",
        "UK3CB_BAF_L115A3_Ghillie",
        "UK3CB_BAF_L115A3_BL_Ghillie",
        "UK3CB_BAF_L115A3_DE_Ghillie"
    };
};

class uk3cb_baf_silencer_l85: Accessory {
    year = 1990;
    
    compatibleClasses[] = {
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_UGL",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL",
        "UK3CB_BAF_L85A2_EMAG",
        "UK3CB_BAF_L85A2",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL"
    };
};

class rhs_acc_dtk4short: Accessory {
    year = 1990;

    compatibleClasses[] = {
        "rhs_weap_ak105",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz",
        "rhs_weap_aks74un"
    };
};

class rhs_acc_tgpa: Accessory {
    year = 1990;

    compatibleClasses[] = {
        "rhs_weap_ak105",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz",
        "rhs_weap_aks74un"
    };
};

class rhs_acc_dtk4screws: Accessory    {
    year = 1990;
    compatibleClasses[] = {
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak104_zenitco01_b33"
    };
};

class rhs_acc_tgpv2: Accessory    {
    year = 1990;
    compatibleClasses[] = {
        "rhs_weap_svdp",
        "rhs_weap_svdp_wd",
        "rhs_weap_svdp_wd_npz",
        "rhs_weap_svdp_npz",
        "rhs_weap_svds",
        "rhs_weap_svds_npz"
    };
};

class rhsusf_acc_aac_scarh_silencer: Accessory    {
	year = 2011;
	compatibleClasses[] = {
		"rhs_weap_SCARH_CQC",
		"rhs_weap_SCARH_STD",
		"rhs_weap_SCARH_LB",
		"rhs_weap_SCARH_FDE_CQC",
		"rhs_weap_SCARH_FDE_STD",
		"rhs_weap_SCARH_FDE_LB"
	};
};

class rhsusf_acc_aac_m14dcqd_silencer: Accessory {
	year = 2004;
	compatibleClasses[] = {
		"rhs_weap_m14",
		"rhs_weap_m14_fiberglass",
		"rhs_weap_m14_rail",
		"rhs_weap_m14_rail_fiberglass",
		"rhs_weap_m14_ris",
		"rhs_weap_m14_ris_fiberglass",
		"rhs_weap_m14_socom",
		"rhs_weap_m14_socom_rail"
	};
};

class rhsusf_acc_aac_m14dcqd_silencer_d: Accessory {
	year = 2004;
	compatibleClasses[] = {
		"rhs_weap_m14_d",
		"rhs_weap_m14_rail_d",
		"rhs_weap_m14_ris_d"
	};
};

class rhsusf_acc_aac_m14dcqd_silencer_wd: Accessory {
	year = 2004;
	compatibleClasses[] = {
		"rhs_weap_m14_wd",
		"rhs_weap_m14_rail_wd",
		"rhs_weap_m14_ris_wd"
	};
};

class uk3cb_muzzle_snds_g3: Accessory {
	year = 2004;
	compatibleClasses[] = {
		"UK3CB_G3SG1",
		"UK3CB_G3SG1_RIS",
		"UK3CB_G3A3",
		"UK3CB_G3A3_RIS",
		"UK3CB_G3A3V",
		"UK3CB_G3A3V_RIS",
		"UK3CB_G3KA4",
		"UK3CB_G3KA4_GL",
		"UK3CB_PSG1A1",
		"UK3CB_PSG1A1_RIS"
	};
};

class uk3cb_muzzle_snds_hk33: Accessory {
	year = 2004;
	compatibleClasses[] = {
		"UK3CB_HK33KA2_RIS",
		"UK3CB_HK33KA2_RIS_GL",
		"UK3CB_HK33KA1",
		"UK3CB_HK33KA2",
		"UK3CB_HK33KA3"
	};
};