/**
 * Flash suppressors are purely aesthetic.
 * Only 2-3 should be offered per-weapon.
 */
class rhsusf_acc_sf3p556: Accessory {
    year = 2000;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "rhs_weap_g36kv",
        "rhs_weap_g36c"
    };
};

class rhsusf_acc_sfmb556: Accessory {
    year = 1990;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "rhs_weap_g36kv",
        "rhs_weap_g36c",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_ct15x",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsd2_bg_ct15x",
		"rhs_weap_vhsk2"
    };
};

class rhsusf_acc_ardec_m240: Accessory {
    year = 1990;
    
    compatibleClasses[] = {
        "rhs_weap_m240B"
    };
};

class rhsusf_acc_m24_muzzlehider_black: Accessory { year = 1990; compatibleClasses[] = {"rhs_weap_m24sws"};    };
class rhsusf_acc_m24_muzzlehider_d: Accessory     { year = 1990; compatibleClasses[] = {"rhs_weap_m24sws_d"};  };
class rhsusf_acc_m24_muzzlehider_wd: Accessory    { year = 1990; compatibleClasses[] = {"rhs_weap_m24sws_wd"}; };

class rhs_acc_dtk1l: Accessory {
    year = 1990;
    
    compatibleClasses[] = {
        "rhs_weap_m70b1n",
        "rhs_weap_m70ab2",
        "rhs_weap_m70b3n",
        "rhs_weap_m70b3n_pbg40",
        "rhs_weap_m76"
    };
};

class uk3cb_baf_sffh: Accessory {
    year = 1990;
    
    compatibleClasses[] = {
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_UGL",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL",
        "UK3CB_BAF_L85A2_EMAG",
        "UK3CB_BAF_L85A2",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL"
    };
};

class rhs_acc_dtk: Accessory {
    year = 1983;

    compatibleClasses[] = {
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz"
    };
};

class rhs_acc_dtk1: Accessory {
    year = 1983;

    compatibleClasses[] = {
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz"
    };
};

class rhs_acc_dtk2: Accessory {
    year = 1983;

    compatibleClasses[] = {
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz"
    };
};

class rhs_acc_dtk3: Accessory {
    year = 1983;

    compatibleClasses[] = {
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz"
    };
};

class rhs_acc_dtk1983: Accessory {
    year = 1983;

    compatibleClasses[] = {
        "rhs_weap_ak105",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz"
    };
};

class rhs_acc_uuk: Accessory {
    year = 1990;

    compatibleClasses[] = {
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25"
    };
};

class rhs_acc_pgs64: Accessory {
    year = 1990;

    compatibleClasses[] = {
        "rhs_weap_ak105",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak105_zenitco01_b33"
    };
};

class rhs_acc_pgs64_74u: Accessory {
    year = 1990;

    compatibleClasses[] = {
        "rhs_weap_aks74u"
    };
};

class rhs_acc_pgs64_74un: Accessory {
    year = 1990;

    compatibleClasses[] = {
        "rhs_weap_aks74un"
    };
};