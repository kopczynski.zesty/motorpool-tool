class rhsusf_acc_acog_mdo: Accessory {
	year = 2011;
	
	compatibleClasses[] = {
		"rhs_weap_m240B",
		"rhs_weap_fnmag"
	};
};

class uk3cb_baf_ta648: Accessory {
	year = 2008;

	compatibleClasses[] = {
		"UK3CB_BAF_L129A1",
		"UK3CB_BAF_L118A1_Covert",
		"UK3CB_BAF_L118A1_Covert_BL",
		"UK3CB_BAF_L118A1_Covert_DE"
	};
};

class uk3cb_baf_ta648_308: Accessory {
	year = 2008;

	compatibleClasses[] = {
		"UK3CB_BAF_L129A1",
		"UK3CB_BAF_L118A1_Covert",
		"UK3CB_BAF_L118A1_Covert_BL",
		"UK3CB_BAF_L118A1_Covert_DE"
	};
};

class uk3cb_baf_maxikite: Accessory {
	year = 2011;

	compatibleClasses[] = {
		"UK3CB_BAF_L129A1",
		"UK3CB_BAF_L118A1_Covert",
		"UK3CB_BAF_L118A1_Covert_BL",
		"UK3CB_BAF_L118A1_Covert_DE"
	};
};

class uk3cb_optic_zfsg1: Accessory {
	year = 1985;

	compatibleClasses[] = {
		"UK3CB_PSG1A1"
	};
};