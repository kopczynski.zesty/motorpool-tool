class optic_aco: Accessory {
	year = 1997;

	compatibleClasses[] = {
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
        "rhs_weap_g36c",
        "rhs_weap_g36kv",
        "rhs_weap_g36kv_ag36"
	};
};

class optic_aco_grn: Accessory {
	year = 1997;

	compatibleClasses[] = {
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
        "rhs_weap_g36c",
        "rhs_weap_g36kv",
        "rhs_weap_g36kv_ag36"
	};
};

class rhsusf_acc_compm4: Accessory {
    year = 2007;
    
    compatibleClasses[] = {
		"arifle_TRG20_F",
		"arifle_TRG21_F",
		"arifle_TRG21_GL_F",
		"LMG_Zafir_F",
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s",
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
		"UK3CB_M16A2",
		"UK3CB_M16A2_UGL",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL",
		"rhs_weap_SCARH_CQC",
		"rhs_weap_SCARH_STD",
		"rhs_weap_SCARH_FDE_CQC",
		"rhs_weap_SCARH_FDE_STD"
    };
};

class rhsusf_acc_t1_high: Accessory {
    year = 2007;
    
    compatibleClasses[] = {
		"arifle_TRG20_F",
		"arifle_TRG21_F",
		"arifle_TRG21_GL_F",
		"LMG_Zafir_F",
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s",
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
		"UK3CB_M16A2",
		"UK3CB_M16A2_UGL",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
        "rhs_weap_g36kv_ag36",
        "rhs_weap_g36kv",
        "rhs_weap_g36c",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25_npz",
		"rhs_weap_SCARH_CQC",
		"rhs_weap_SCARH_STD",
		"rhs_weap_SCARH_FDE_CQC",
		"rhs_weap_SCARH_FDE_STD"
    };
};

class rhsusf_acc_t1_low: Accessory {
    year = 2007;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
        "rhs_weap_g36kv_ag36",
        "rhs_weap_g36kv",
        "rhs_weap_g36c"
    };
};

class rhsusf_acc_eotech_552: Accessory {
    year = 2004;
    
    compatibleClasses[] = {
		"arifle_TRG20_F",
		"arifle_TRG21_F",
		"arifle_TRG21_GL_F",
		"LMG_Zafir_F",
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
		"UK3CB_M16A2",
		"UK3CB_M16A2_UGL",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhsusf_weap_MP7A2",
        "rhsusf_weap_MP7A2_winter",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz",
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL",
		"UK3CB_HK33KA2_RIS",
		"UK3CB_HK33KA2_RIS_GL",
		"UK3CB_G3SG1_RIS",
		"UK3CB_G3A3_RIS",
		"UK3CB_G3A3V_RIS",
		"UK3CB_G3KA4",
		"UK3CB_G3KA4_GL",
		"UK3CB_MG3_Railed",
		"UK3CB_MG3_KWS_B",
		"rhs_weap_SCARH_CQC",
		"rhs_weap_SCARH_STD"
    };
};

class rhsusf_acc_eotech_552_d: Accessory {
    year = 2004;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_mk18_d",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
        "rhsusf_weap_MP7A2_aor1",
        "rhsusf_weap_MP7A2_desert",
		"rhs_weap_SCARH_FDE_CQC",
		"rhs_weap_SCARH_FDE_STD",
		"rhs_weap_SCARH_FDE_LB",
		"UK3CB_MG3_KWS_T",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL"
    };
};

class rhsusf_acc_eotech_552_wd: Accessory {
    year = 2004;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18_wd",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
		"UK3CB_MG3_KWS_G"
    };
};

class rhsusf_acc_eotech_xps3: Accessory {
    year = 2007;
    
    compatibleClasses[] = {
		"arifle_TRG20_F",
		"arifle_TRG21_F",
		"arifle_TRG21_GL_F",
		"LMG_Zafir_F",
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s",
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
		"UK3CB_M16A2",
		"UK3CB_M16A2_UGL",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL"
    };
};

class rhsusf_acc_mrds: Accessory {
    year = 2012;

    compatibleClasses[] = {
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
        "rhs_weap_m27iar",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz",
        "rhs_weap_asval_grip_npz",
        "rhs_weap_asval_npz",
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL",
		"UK3CB_HK33KA2_RIS",
		"UK3CB_HK33KA2_RIS_GL",
		"UK3CB_G3SG1_RIS",
		"UK3CB_G3A3_RIS",
		"UK3CB_G3A3V_RIS",
		"UK3CB_G3KA4",
		"UK3CB_G3KA4_GL",
    };
};

class rhsusf_acc_mrds_c: Accessory {
    year = 2012;

    compatibleClasses[] = {
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL"
    };
};

class rhsusf_acc_rm05: Accessory {
    year = 2012;

    compatibleClasses[] = {
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
        "rhs_weap_m27iar",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz",
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL"
    };
};

class rhs_acc_rakurspm: Accessory {
    year = 2006;

    compatibleClasses[] = {
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz",
        "rhs_weap_asval_grip_npz",
        "rhs_weap_asval_npz"
    };
};

class rhs_acc_1p87: Accessory {
    year = 2016;

    compatibleClasses[] = {
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz",
        "rhs_weap_asval_grip_npz",
        "rhs_weap_asval_npz"
    };
};

class rhs_acc_ekp8_18: Accessory {
    year = 2009;

    compatibleClasses[] = {
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz",
        "rhs_weap_asval_grip_npz",
        "rhs_weap_asval_npz"
    };
};

class rhs_acc_1p63: Accessory {
    year = 2003;

    compatibleClasses[] = {
        "rhs_weap_m70b1n",
        "rhs_weap_m70ab2",
        "rhs_weap_m70b3n",
        "rhs_weap_m70b3n_pbg40",
        "rhs_weap_pkp",
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak104",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak105",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74un",
        "rhs_weap_asval",
        "rhs_weap_asval_grip"
    };
};

class rhs_acc_ekp1: Accessory {
    year = 2008;

    compatibleClasses[] = {
        "rhs_weap_m70b1n",
        "rhs_weap_m70ab2",
        "rhs_weap_m70b3n",
        "rhs_weap_m70b3n_pbg40",
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak104",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak105",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74un",
        "rhs_weap_asval",
        "rhs_weap_asval_grip"
    };
};

class rhs_acc_ekp8_02: Accessory {
    year = 2008;

    compatibleClasses[] = {
        "rhs_weap_m70b1n",
        "rhs_weap_m70ab2",
        "rhs_weap_m70b3n",
        "rhs_weap_m70b3n_pbg40",
		"rhs_weap_akmn",
		"rhs_weap_akmn_gp25",
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak104",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak105",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74un",
        "rhs_weap_asval",
        "rhs_weap_asval_grip"
    };
};

class rhs_acc_okp7_dovetail: Accessory {
    year = 2008;

    compatibleClasses[] = {
        "rhs_weap_m70b1n",
        "rhs_weap_m70ab2",
        "rhs_weap_m70b3n",
        "rhs_weap_m70b3n_pbg40",
		"rhs_weap_akmn",
		"rhs_weap_akmn_gp25",
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak104",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak105",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74un",
        "rhs_weap_asval",
        "rhs_weap_asval_grip"
    };
};

class rhs_acc_pkas: Accessory {
    year = 2006;

    compatibleClasses[] = {
        "rhs_weap_m70b1n",
        "rhs_weap_m70ab2",
        "rhs_weap_m70b3n",
        "rhs_weap_m70b3n_pbg40",
		"rhs_weap_akmn",
		"rhs_weap_akmn_gp25",
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak104",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak105",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74un",
        "rhs_weap_asval",
        "rhs_weap_asval_grip"
    };
};

class bwa3_optic_compm2: Accessory {
    year = 2000;

    compatibleClasses[] = {
        "BWA3_G36A2",
        "BWA3_G36A2_AG40",
        "BWA3_G36KA2",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA3",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36"
    };
};

class bwa3_optic_compm2_tan: Accessory {
    year = 2000;

    compatibleClasses[] = {
        "BWA3_G36A2_tan",
        "BWA3_G36A2_AG40_tan",
        "BWA3_G36KA2_tan",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan"
    };
};

class bwa3_optic_eotech552: Accessory {
    year = 2004;

    compatibleClasses[] = {
        "BWA3_G36A2",
        "BWA3_G36A2_AG40",
        "BWA3_G36KA2",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA3",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40"
    };
};

class bwa3_optic_eotech552_tan: Accessory {
    year = 2004;

    compatibleClasses[] = {
        "BWA3_G36A2_tan",
        "BWA3_G36A2_AG40_tan",
        "BWA3_G36KA2_tan",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan"
    };
};

class bwa3_optic_eotech: Accessory {
    year = 2007;

    compatibleClasses[] = {
        "BWA3_G36A2",
        "BWA3_G36A2_AG40",
        "BWA3_G36KA2",
        "BWA3_G36A2_tan",
        "BWA3_G36A2_AG40_tan",
        "BWA3_G36KA2_tan",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA3",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan"
    };
};

class bwa3_optic_rsas: Accessory {
    year = 2005;

    compatibleClasses[] = {
        "rhs_weap_g36kv_ag36",
        "rhs_weap_g36kv",
        "rhs_weap_g36c",
        "BWA3_G36A2",
        "BWA3_G36A2_AG40",
        "BWA3_G36KA2",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA3",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40"
    };
};

class bwa3_optic_rsas_tan: Accessory {
    year = 2005;

    compatibleClasses[] = {
        "BWA3_G36A2_tan",
        "BWA3_G36A2_AG40_tan",
        "BWA3_G36KA2_tan",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan"
    };
};

class rksl_optic_rmr_rm33: Accessory {
    year = 2017;

    compatibleClasses[] = {
        "rhs_weap_g36kv_ag36",
        "rhs_weap_g36kv",
        "rhs_weap_g36c",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA3",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan",
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL"
    };
};

class rksl_optic_rmr_rm33_fde: Accessory {
    year = 2017;

    compatibleClasses[] = {
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL"
    };
};

class rhsgref_mg42_acc_aasight: Accessory {
	year = 1942;

	compatibleClasses[] = {
		"rhs_weap_mg42"
	};
};