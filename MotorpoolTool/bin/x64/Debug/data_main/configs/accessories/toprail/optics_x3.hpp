class rhsusf_acc_g33_t1: Accessory {
    year = 2007;
    
    compatibleClasses[] = {
		"arifle_TRG20_F",
		"arifle_TRG21_F",
		"arifle_TRG21_GL_F",
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s",
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
		"UK3CB_M16A2",
		"UK3CB_M16A2_UGL",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
        "rhsusf_weap_MP7A2",
        "rhsusf_weap_MP7A2_aor1",
        "rhsusf_weap_MP7A2_desert",
        "rhsusf_weap_MP7A2_winter",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA3",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan",
		"rhs_weap_SCARH_CQC",
		"rhs_weap_SCARH_STD",
		"rhs_weap_SCARH_FDE_CQC",
		"rhs_weap_SCARH_FDE_STD",
		"UK3CB_HK33KA2_RIS",
		"UK3CB_HK33KA2_RIS_GL",
		"UK3CB_G3SG1_RIS",
		"UK3CB_G3A3_RIS",
		"UK3CB_G3A3V_RIS",
		"UK3CB_G3KA4",
		"UK3CB_G3KA4_GL"
    };
};

class rhsusf_acc_g33_xps3: Accessory {
    year = 2007;
    
    compatibleClasses[] = {
		"arifle_TRG20_F",
		"arifle_TRG21_F",
		"arifle_TRG21_GL_F",
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_wd",
		"UK3CB_M16A2",
		"UK3CB_M16A2_UGL",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhsusf_weap_MP7A2",
        "rhsusf_weap_MP7A2_winter",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA3",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan",
		"rhs_weap_SCARH_CQC",
		"rhs_weap_SCARH_STD",
		"rhs_weap_SCARH_FDE_CQC",
		"rhs_weap_SCARH_FDE_STD",
		"UK3CB_HK33KA2_RIS",
		"UK3CB_HK33KA2_RIS_GL",
		"UK3CB_G3SG1_RIS",
		"UK3CB_G3A3_RIS",
		"UK3CB_G3A3V_RIS",
		"UK3CB_G3KA4",
		"UK3CB_G3KA4_GL"
    };
};

class rhsusf_acc_g33_xps3_tan: Accessory {
    year = 2007;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_mk18_d",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
        "rhsusf_weap_MP7A2_aor1",
        "rhsusf_weap_MP7A2_desert"
    };
};

class rhsusf_acc_acog: Accessory {
    year = 2001;
		
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_m14ebrri",
		"arifle_SPAR_03_blk_F",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
		"UK3CB_HK33KA2_RIS",
		"UK3CB_HK33KA2_RIS_GL",
		"UK3CB_G3SG1_RIS",
		"UK3CB_G3A3_RIS",
		"UK3CB_G3A3V_RIS",
		"UK3CB_G3KA4",
		"UK3CB_G3KA4_GL"
    };
};

class rhsusf_acc_acog2: Accessory {
    year = 2001;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_m14ebrri",
		"arifle_SPAR_03_blk_F",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
		"UK3CB_HK33KA2_RIS",
		"UK3CB_HK33KA2_RIS_GL",
		"UK3CB_G3SG1_RIS",
		"UK3CB_G3A3_RIS",
		"UK3CB_G3A3V_RIS",
		"UK3CB_G3KA4",
		"UK3CB_G3KA4_GL"
    };
};

class rhsusf_acc_acog3: Accessory {
    year = 2001;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_m14ebrri",
		"arifle_SPAR_03_blk_F",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
		"UK3CB_HK33KA2_RIS",
		"UK3CB_HK33KA2_RIS_GL",
		"UK3CB_G3SG1_RIS",
		"UK3CB_G3A3_RIS",
		"UK3CB_G3A3V_RIS",
		"UK3CB_G3KA4",
		"UK3CB_G3KA4_GL"
    };
};

class rhsusf_acc_acog_usmc: Accessory {
    year = 2001;
    
    compatibleClasses[] = {
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
		"UK3CB_M16A2",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
		"rhs_weap_SCARH_CQC",
		"rhs_weap_SCARH_STD",
		"rhs_weap_SCARH_LB"
    };
};

class rhsusf_acc_acog2_usmc: Accessory {
    year = 2001;
    
    compatibleClasses[] = {
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
		"UK3CB_M16A2",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar"
    };
};

class rhsusf_acc_acog3_usmc: Accessory {
    year = 2001;
    
    compatibleClasses[] = {
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
		"UK3CB_M16A2",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar"
    };
};

class rhsusf_acc_acog_rmr: Accessory {
    year = 2007;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
		"UK3CB_M16A2",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_m14ebrri",
		"arifle_SPAR_03_blk_F",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA3",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL"
    };
};

class rhsusf_acc_acog_d: Accessory {
    year = 2001;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_mk18_d",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
		"rhs_weap_SCARH_FDE_CQC",
		"rhs_weap_SCARH_FDE_STD",
		"rhs_weap_SCARH_FDE_LB",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL"
    };
};

class rhsusf_acc_acog_wd: Accessory {
    year = 2001;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_mk18_wd",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2"
    };
};

class rhsusf_acc_elcan: Accessory {
    year = 1989;
    
    compatibleClasses[] = {
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhs_weap_m240B",
		"rhs_weap_fnmag",
		"UK3CB_MG3_Railed",
		"UK3CB_MG3_KWS_B",
		"UK3CB_MG3_KWS_G",
		"UK3CB_MG3_KWS_T",
        "BWA3_MG4",
        "BWA3_MG5"
    };
};

class rhsusf_acc_elcan_ard: Accessory {
    year = 1989;
    
    compatibleClasses[] = {
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhs_weap_m240B",
		"rhs_weap_fnmag",
		"UK3CB_MG3_Railed",
		"UK3CB_MG3_KWS_B",
		"UK3CB_MG3_KWS_G",
		"UK3CB_MG3_KWS_T",
        "BWA3_MG4",
        "BWA3_MG5"
    };
};

class rhs_acc_1p29: Accessory {
    year = 1994;
    
    compatibleClasses[] = {
		"rhs_weap_akmn",
		"rhs_weap_akmn_gp25",
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak104",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak105",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m_zenitco01",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_gp25",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_gp25",
        "rhs_weap_aks74un"
    };
};

class rhs_acc_1pn93_1: Accessory {
	year = 1994;
	
	compatibleClasses[] = {
		"rhs_weap_akmn",
		"rhs_weap_akmn_gp25",
        "rhs_weap_ak103",
        "rhs_weap_ak103_gp25",
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak104",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak105",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_gp25",
        "rhs_weap_ak74m_zenitco01",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_gp25",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_gp25",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_gp25"
	};
};

class rhs_acc_pso1m2: Accessory {
	year = 1964;

	compatibleClasses[] = {
		"rhs_weap_m76",
		"UK3CB_SVD_OLD",
        "rhs_weap_svdp",
        "rhs_weap_svdp_wd",
        "rhs_weap_svds"
	};
};

class rhs_acc_pso1m21: Accessory {
	year = 1987;

	compatibleClasses[] = {
        "rhs_weap_asval",
        "rhs_weap_asval_grip",
        "rhs_weap_vss",
        "rhs_weap_vss_grip"
	};
};