class bwa3_optic_pmii_dmr: Accessory {
	year = 2012;

	compatibleClasses[] = {
		"BWA3_G28",
		"BWA3_G28_Patrol"
	};
};

class bwa3_optic_pmii_dmr_microt1_front: Accessory {
	year = 2012;

	compatibleClasses[] = {
		"BWA3_G28",
		"BWA3_G28_Patrol"
	};
};

class bwa3_optic_pmii_dmr_microt1_rear: Accessory {
	year = 2012;

	compatibleClasses[] = {
		"BWA3_G28",
		"BWA3_G28_Patrol"
	};
};