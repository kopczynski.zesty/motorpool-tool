class bwa3_optic_hensoldt: Accessory { 
	year = 2006;

	compatibleClasses[] = {
		"UK3CB_BAF_L115A3",
		"UK3CB_BAF_L115A3_BL",
		"UK3CB_BAF_L115A3_DE",
		"UK3CB_BAF_L115A3_Ghillie",
		"UK3CB_BAF_L115A3_BL_Ghillie",
		"UK3CB_BAF_L115A3_DE_Ghillie"
	};
};

class bwa3_optic_m5xi_msr: Accessory {
	year = 2015;

	compatibleClasses[] = {
		"BWA3_G28",
		"BWA3_G28_Patrol"
	};
};

class bwa3_optic_m5xi_msr_microt2: Accessory {
	year = 2015;

	compatibleClasses[] = {
		"BWA3_G28",
		"BWA3_G28_Patrol"
	};
};

class bwa3_optic_m5xi_tremor3: Accessory {
	year = 2015;

	compatibleClasses[] = {
		"BWA3_G28",
		"BWA3_G28_Patrol"
	};
};

class bwa3_optic_m5xi_tremor3_microt2: Accessory {
	year = 2015;

	compatibleClasses[] = {
		"BWA3_G28",
		"BWA3_G28_Patrol"
	};
};

class rksl_optic_pmii_525: Accessory {
	year = 2014;

	compatibleClasses[] = {
		"UK3CB_BAF_L115A3_BL",
		"UK3CB_BAF_L115A3_BL_Ghillie",
		"UK3CB_BAF_L118A1_Covert",
		"UK3CB_BAF_L118A1_Covert_BL",
		"UK3CB_BAF_L118A1_Covert_DE"
	};
};

class rksl_optic_pmii_525_des: Accessory {
	year = 2014;

	compatibleClasses[] = {
		"UK3CB_BAF_L115A3_DE",
		"UK3CB_BAF_L115A3_DE_Ghillie"
	};
};

class rksl_optic_pmii_525_wdl: Accessory {
	year = 2014;

	compatibleClasses[] = {
		"UK3CB_BAF_L115A3",
		"UK3CB_BAF_L115A3_Ghillie"
	};
};