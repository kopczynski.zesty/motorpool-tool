class rhsusf_acc_su230: Accessory {
    year = 2012;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_wd",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL",
		"UK3CB_HK33KA2_RIS",
		"UK3CB_HK33KA2_RIS_GL",
		"UK3CB_G3SG1_RIS",
		"UK3CB_G3A3_RIS",
		"UK3CB_G3A3V_RIS",
		"UK3CB_G3KA4",
		"UK3CB_G3KA4_GL"
    };
};

class rhsusf_acc_su230_mrds: Accessory {
    year = 2017;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_wd",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
        "rhs_weap_m21a_pr",
        "rhs_weap_m21a_pr_pbg40",
        "rhs_weap_m21s_pr",
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL"
    };
};

class rhsusf_acc_su230_c: Accessory {
    year = 2012;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_mk18_d",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL"
    };
};

class rhsusf_acc_su230_mrds_c: Accessory {
    year = 2017;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_mk18_d",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL"
    };
};

class rhsusf_acc_su230a: Accessory {
    year = 2017;

    compatibleClasses[] = {
        "BWA3_G27"
    };
};

class rhsusf_acc_su230a_mrds: Accessory {
    year = 2017;

    compatibleClasses[] = {
        "BWA3_G27"
    };
};

class rhsusf_acc_su230a_c: Accessory {
    year = 2017;

    compatibleClasses[] = {
        "BWA3_G27_tan"
    };
};

class rhsusf_acc_su230a_mrds_c: Accessory {
    year = 2017;

    compatibleClasses[] = {
        "BWA3_G27_tan"
    };
};

class rhsgref_acc_l1a1_anpvs2 : Accessory {
	year = 1972;

	compatibleClasses[] = {
		"rhs_weap_l1a1",
		"rhs_weap_l1a1_wood",
		"UK3CB_FNFAL_FULL",
		"UK3CB_FNFAL_PARA"
	};
};

class rksl_optic_lds: Accessory {
    year = 2010;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
		"UK3CB_M16A2",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_m27iar",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsk2",
        "rhs_weap_ak103_gp25_npz",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105_npz",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_ak74m_fullplum_gp25_npz",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m_gp25_npz",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_gp25_npz",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_ak74n_2_gp25_npz",
        "rhs_weap_aks74n_gp25_npz",
        "rhs_weap_aks74n_npz",
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_UGL",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A2",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL",
		"UK3CB_HK33KA2_RIS",
		"UK3CB_HK33KA2_RIS_GL"
    };
};

class rksl_optic_lds_c: Accessory {
    year = 2010;
    
    compatibleClasses[] = {
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL"
    };
};

class bwa3_optic_zo4x30i: Accessory {
    year = 2012;

    compatibleClasses[] = {
        "rhs_weap_g36kv_ag36",
        "rhs_weap_g36kv",
        "rhs_weap_g36c",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA3",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_G27",
        "BWA3_MG4",
        "BWA3_MG5"
    };
};

class bwa3_optic_zo4x30i_sand: Accessory {
    year = 2012;

    compatibleClasses[] = {
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan",
        "BWA3_G27_tan"
    };
};

class bwa3_optic_zo4x30i_microt2: Accessory {
    year = 2014;

    compatibleClasses[] = {
        "rhs_weap_g36kv_ag36",
        "rhs_weap_g36kv",
        "rhs_weap_g36c",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA3",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_G27",
        "BWA3_MG4",
        "BWA3_MG5"
    };
};

class bwa3_optic_zo4x30i_microt2_sand: Accessory {
    year = 2014;

    compatibleClasses[] = {
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan",
        "BWA3_G27_tan"
    };
};

class bwa3_optic_zo4x30i_rsas: Accessory {
    year = 2012;

    compatibleClasses[] = {
        "rhs_weap_g36kv_ag36",
        "rhs_weap_g36kv",
        "rhs_weap_g36c",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA3",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_G27",
        "BWA3_MG4",
        "BWA3_MG5"
    };
};

class bwa3_optic_zo4x30i_rsas_sand: Accessory {
    year = 2012;

    compatibleClasses[] = {
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan",
        "BWA3_G27_tan"
    };
};

class uk3cb_baf_kite: Accessory {
    year = 2011;

    compatibleClasses[] = {
        "UK3CB_BAF_L129A1"
    };
};

class uk3cb_baf_susat: Accessory {
    year = 1985;

    compatibleClasses[] = {
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_UGL",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL",
        "UK3CB_BAF_L85A2_EMAG",
        "UK3CB_BAF_L85A2"
    };
};

class uk3cb_baf_ta31f: Accessory {
    year = 2001;

    compatibleClasses[] = {
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL"
    };
};

class uk3cb_optic_pvs4m14: Accessory {
	year = 1978;

	compatibleClasses[] = {
		"UK3CB_M21"
	};
};

class rhsgref_acc_l1a1_l2a2 : Accessory {
	year = 1972;

	compatibleClasses[] = {
		"rhs_weap_l1a1",
		"rhs_weap_l1a1_wood",
		"UK3CB_FNFAL_FULL",
		"UK3CB_FNFAL_PARA"
	};
};

class uk3cb_optic_stanagzf_g3: Accessory {
	year = 1972;

	compatibleClasses[] = {
		"UK3CB_HK33KA1",
		"UK3CB_HK33KA2",
		"UK3CB_HK33KA3",
		"UK3CB_G3SG1",
		"UK3CB_G3A3",
		"UK3CB_G3A3V",
		"UK3CB_PSG1A1"
	};
};