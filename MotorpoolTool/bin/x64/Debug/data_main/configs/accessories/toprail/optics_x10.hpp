class rhsusf_acc_m8541: Accessory {
	year = 2005;
	
	compatibleClasses[] = {
		"rhs_weap_XM2010",
		"rhs_weap_XM2010_d",
		"rhs_weap_XM2010_sa",
		"rhs_weap_XM2010_wd",
		"rhs_weap_m40a5",
		"rhs_weap_m40a5_d",
		"rhs_weap_m40a5_wd",
		"arifle_SPAR_03_blk_F"
	};
};

class rhsusf_acc_m8541_low: Accessory {
	year = 2004;

	compatibleClasses[] = {
		"rhs_weap_m14_rail",
		"rhs_weap_m14_rail_fiberglass",
		"rhs_weap_m14_rail_wd",
		"rhs_weap_m14_rail",
		"rhs_weap_m14ebrri",
		"rhs_weap_m24sws",
		"rhs_weap_m40a5",
        "rhs_weap_svdp_wd_npz",
        "rhs_weap_svdp_npz",
        "rhs_weap_svds_npz",
        "rhs_weap_vss_npz",
        "rhs_weap_vss_grip_npz"
	};
};

class rhsusf_acc_m8541_low_d: Accessory {
	year = 2005;

	compatibleClasses[] = {
		"rhs_weap_m24sws_d",
		"rhs_weap_m40a5_d"
	};
};

class rhsusf_acc_m8541_low_wd: Accessory {
	year = 2005;

	compatibleClasses[] = {
		"rhs_weap_m24sws_wd",
		"rhs_weap_m40a5_wd"
	};
};

class rksl_optic_pmii_312: Accessory {
	year = 2006;

	compatibleClasses[] = {
		"UK3CB_BAF_L115A3_BL",
		"UK3CB_BAF_L115A3_BL_Ghillie",
		"UK3CB_BAF_L118A1_Covert",
		"UK3CB_BAF_L118A1_Covert_BL",
		"UK3CB_BAF_L118A1_Covert_DE"
	};
};

class rksl_optic_pmii_312_des: Accessory {
	year = 2006;

	compatibleClasses[] = {
		"UK3CB_BAF_L115A3_DE",
		"UK3CB_BAF_L115A3_DE_Ghillie"
	};
};

class rksl_optic_pmii_312_wdl: Accessory {
	year = 2006;

	compatibleClasses[] = {
		"UK3CB_BAF_L115A3",
		"UK3CB_BAF_L115A3_Ghillie"
	};
};

class rksl_optic_pmii_312_sunshade: Accessory {
	year = 2006;

	compatibleClasses[] = {
		"UK3CB_BAF_L115A3_BL",
		"UK3CB_BAF_L115A3_BL_Ghillie",
		"UK3CB_BAF_L118A1_Covert",
		"UK3CB_BAF_L118A1_Covert_BL",
		"UK3CB_BAF_L118A1_Covert_DE"
	};
};

class rksl_optic_pmii_312_sunshade_des: Accessory {
	year = 2006;

	compatibleClasses[] = {
		"UK3CB_BAF_L115A3_DE",
		"UK3CB_BAF_L115A3_DE_Ghillie"
	};
};

class rksl_optic_pmii_312_sunshade_wdl: Accessory {
	year = 2006;

	compatibleClasses[] = {
		"UK3CB_BAF_L115A3",
		"UK3CB_BAF_L115A3_Ghillie"
	};
};
