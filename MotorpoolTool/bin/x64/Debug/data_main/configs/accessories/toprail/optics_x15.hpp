class rhsusf_acc_premier: Accessory {
	year = 2005;

	compatibleClasses[] = {
		"rhs_weap_XM2010",
		"rhs_weap_XM2010_d",
		"rhs_weap_XM2010_sa",
		"rhs_weap_XM2010_wd",
		"rhs_weap_m40a5",
		"rhs_weap_m40a5_d",
		"rhs_weap_m40a5_wd",
		"arifle_SPAR_03_blk_F",
		"rhs_weap_SCARH_LB",
		"rhs_weap_SCARH_FDE_LB"
	};
};

class rhsusf_acc_premier_low: Accessory {
	year = 2005;
	
	compatibleClasses[] = {
		"rhs_weap_m24sws",
		"rhs_weap_m24sws_d",
		"rhs_weap_m24sws_wd",
		"rhs_weap_m40a5",
		"rhs_weap_m40a5_d",
		"rhs_weap_m40a5_wd",
		"rhs_weap_sr25",
		"rhs_weap_sr25_ec",
		"rhs_weap_sr25_d",
		"rhs_weap_sr25_ec_d",
		"rhs_weap_sr25_wd",
		"rhs_weap_sr25_ec_wd"
	};
};

class rhsusf_acc_premier_anpvs27: Accessory {
	year = 2010;
	
	compatibleClasses[] = {
		"rhs_weap_XM2010",
		"rhs_weap_XM2010_d",
		"rhs_weap_XM2010_sa",
		"rhs_weap_XM2010_wd",
		"rhs_weap_m40a5",
		"rhs_weap_m40a5_d",
		"rhs_weap_m40a5_wd",
		"rhs_weap_sr25",
		"rhs_weap_sr25_ec",
		"rhs_weap_sr25_d",
		"rhs_weap_sr25_ec_d",
		"rhs_weap_sr25_wd",
		"rhs_weap_sr25_ec_wd"
	};
};

class rhsusf_acc_leupoldmk4_2: Accessory {
	year = 1990;
	
	compatibleClasses[] = {
		"rhs_weap_XM2010",
		"rhs_weap_XM2010_wd",
		"rhs_weap_sr25",
		"rhs_weap_sr25_ec",
		"rhs_weap_sr25_wd",
		"rhs_weap_sr25_ec_wd",
        "rhs_weap_t5000"
	};
};

class rhsusf_acc_leupoldmk4_2_d: Accessory {
	year = 1990;

	compatibleClasses[] = {
		"rhs_weap_XM2010_d",
		"rhs_weap_XM2010_sa",
		"rhs_weap_sr25_d",
		"rhs_weap_sr25_ec_d"
	};
};

class rhs_acc_dh520x56: Accessory {
	year = 2017;

	compatibleClasses[] = {
        "rhs_weap_svdp_wd_npz",
        "rhs_weap_svdp_npz",
        "rhs_weap_svds_npz",
        "rhs_weap_vss_npz",
        "rhs_weap_vss_grip_npz",
        "rhs_weap_t5000"
	};
};