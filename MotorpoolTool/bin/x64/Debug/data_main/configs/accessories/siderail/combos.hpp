/**
 * Combos are equipped on any weapons
 * possible, matched to camo.
 */
class rhsusf_acc_anpeq15_bk: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_wd",
        "rhs_weap_m16a4",
        "rhs_weap_m16a4_carryhandle",
        "rhs_weap_hk416d10_m320",
        "rhs_weap_hk416d145_m320",
        "rhs_weap_hk416d10",
        "rhs_weap_hk416d10_LMT",
        "rhs_weap_hk416d145",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2"
    };
};

class rhsusf_acc_anpeq15: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_mk18_d",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2"
    };
};

class rhsusf_acc_anpeq15_wmx: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_mk18_d",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2"
    };
};

class rhsusf_acc_anpeq16a: Accessory {
    year = 2013;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd",
        "rhs_weap_hk416d10_LMT_wd",
        "rhs_weap_hk416d145_wd",
        "rhs_weap_hk416d145_wd_2",
        "rhs_weap_hk416d10_LMT_d",
        "rhs_weap_hk416d145_d",
        "rhs_weap_hk416d145_d_2"
    };
};

class rhsusf_acc_anpeq16a_top: Accessory {
    year = 2013;
    
    compatibleClasses[] = {
        "rhs_weap_m4",
        "rhs_weap_m4_mstock",
        "rhs_weap_m4_carryhandle",
        "rhs_weap_m4_carryhandle_mstock",
        "rhs_weap_m4a1",
        "rhs_weap_m4a1_d",
        "rhs_weap_m4a1_wd",
        "rhs_weap_m4a1_carryhandle",
        "rhs_weap_m4a1_carryhandle_mstock",
        "rhs_weap_m4a1_blockII_bk",
        "rhs_weap_m4a1_blockII_d",
        "rhs_weap_m4a1_blockII_wd",
        "rhs_weap_mk18",
        "rhs_weap_mk18_m320",
        "rhs_weap_mk18_d",
        "rhs_weap_mk18_wd"
    };
};

class bwa3_acc_varioray_irlaser_black: Accessory {
    year = 2014;

    compatibleClasses[] = {
        "rhs_weap_g36kv_ag36",
        "rhs_weap_g36kv",
        "rhs_weap_g36c",
        "BWA3_G36A1",
        "BWA3_G36A1_AG40",
        "BWA3_G36A2",
        "BWA3_G36A2_AG40",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA1",
        "BWA3_G36KA2",
        "BWA3_G36KA3",
        "BWA3_G36A1_tan",
        "BWA3_G36A1_AG40_tan",
        "BWA3_G36A2_tan",
        "BWA3_G36A2_AG40_tan",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA1_tan",
        "BWA3_G36KA2_tan",
        "BWA3_G36KA3_tan",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan",
        "BWA3_G27",
        "BWA3_MG4",
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_UGL",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL",
        "UK3CB_BAF_L129A1"
    };
};

class bwa3_acc_varioray_irlaser: Accessory {
    year = 2014;

    compatibleClasses[] = {
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan",
        "BWA3_G27_tan",
        "BWA3_G28",
        "BWA3_G28_Patrol",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL"
    };
};

class bwa3_acc_llm01_irlaser: Accessory {
    year = 2005;

    compatibleClasses[] = {
        "BWA3_G36A1",
        "BWA3_G36A1_AG40",
        "BWA3_G36A2",
        "BWA3_G36A2_AG40",
        "BWA3_G36A3",
        "BWA3_G36A3_AG40",
        "BWA3_G36KA1",
        "BWA3_G36KA2",
        "BWA3_G36KA3",
        "BWA3_G38",
        "BWA3_G38_AG40",
        "BWA3_G38K",
        "BWA3_G38K_AG40",
        "BWA3_G38_tan",
        "BWA3_G38_AG40_tan",
        "BWA3_G38K_tan",
        "BWA3_G38K_AG40_tan",
        "BWA3_G27",
        "BWA3_G27_tan",
        "BWA3_G28",
        "BWA3_G28_Patrol"
    };
};

class bwa3_acc_llm01_irlaser_tan: Accessory {
    year = 2005;

    compatibleClasses[] = {
        "BWA3_G36A1_tan",
        "BWA3_G36A1_AG40_tan",
        "BWA3_G36A2_tan",
        "BWA3_G36A2_AG40_tan",
        "BWA3_G36A3_tan",
        "BWA3_G36A3_AG40_tan",
        "BWA3_G36KA1_tan",
        "BWA3_G36KA2_tan",
        "BWA3_G36KA3_tan"
    };
};

class uk3cb_baf_llm_flashlight_black: Accessory {
    year = 2005;
    
    compatibleClasses[] = {
        "rhs_weap_g36kv_ag36",
        "rhs_weap_g36kv",
        "rhs_weap_g36c",
        "UK3CB_BAF_L85A2_UGL_HWS",
        "UK3CB_BAF_L85A2_UGL",
        "UK3CB_BAF_L85A2_RIS",
        "UK3CB_BAF_L85A2_RIS_G",
        "UK3CB_BAF_L85A2_RIS_D",
        "UK3CB_BAF_L85A2_RIS_W",
        "UK3CB_BAF_L85A2_RIS_AFG",
        "UK3CB_BAF_L85A2_RIS_G_AFG",
        "UK3CB_BAF_L85A2_RIS_D_AFG",
        "UK3CB_BAF_L85A2_RIS_W_AFG",
		"UK3CB_BAF_L85A3",
		"UK3CB_BAF_L85A3_AFG",
		"UK3CB_BAF_L85A3_UGL",
        "UK3CB_BAF_L129A1",
        "UK3CB_BAF_L119A1_FG",
        "UK3CB_BAF_L119A1_UKUGL",
		"UK3CB_HK33KA2_RIS",
		"UK3CB_HK33KA2_RIS_GL",
		"UK3CB_G3KA4",
		"UK3CB_G3KA4_GL"
    };
};


class rhs_acc_perst3_2dp_h: Accessory {
    year = 2010;
    
    compatibleClasses[] = {
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr"
    };
};