/**
 * Standalone lights are only equipped 
 * when a combo is unavailable.
 */
class acc_flashlight: Accessory {
    year = 1970;
    
    compatibleClasses[] = {
		"LMG_Zafir_F",
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhs_weap_m240B",
		"rhs_weap_fnmag",
        "rhs_weap_m14ebrri",
        "rhsusf_weap_MP7A2",
        "rhsusf_weap_MP7A2_aor1",
        "rhsusf_weap_MP7A2_desert",
        "rhsusf_weap_MP7A2_winter",
        "BWA3_MG4",
        "BWA3_MG5",
		"rhs_weap_g36c",
		"rhs_weap_g36kv",
		"rhs_weap_g36kv_ag36",
		"rhs_weap_vhsd2",
		"rhs_weap_vhsd2_ct15x",
		"rhs_weap_vhsd2_bg",
		"rhs_weap_vhsd2_bg_ct15x",
		"rhs_weap_vhsk2",
		"arifle_SPAR_03_blk_F",
		"arifle_TRG20_F",
		"arifle_TRG21_F",
		"arifle_TRG21_GL_F"
    };
};

class rhsusf_acc_m952v: Accessory {
    year = 1991;
    
    compatibleClasses[] = {
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
		"rhs_weap_m249_light_L",
		"rhs_weap_m249_light_S",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhs_weap_m240B",
		"rhs_weap_fnmag",
        "rhs_weap_m14ebrri"
    };
};

class rhsusf_acc_wmx_bk: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhs_weap_m4_m203",
        "rhs_weap_m4_m203S",
        "rhs_weap_m4a1_m203",
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_m203s_wd",
        "rhs_weap_m4a1_carryhandle_m203",
        "rhs_weap_m4a1_carryhandle_m203S",
        "rhs_weap_m4a1_blockII_M203_bk",
        "rhs_weap_m4a1_blockII_M203_d",
        "rhs_weap_m4a1_blockII_M203_wd",
        "rhs_weap_m16a4_carryhandle_M203",
        "rhs_weap_m27iar",
        "rhs_weap_m249_pip_L",
        "rhs_weap_m249_pip_S",
        "rhs_weap_m249_pip_S_para",
        "rhs_weap_m249_pip_L_para",
        "rhs_weap_m249_pip_L_vfg",
        "rhs_weap_m249_pip_S_vfg",
        "rhs_weap_m240B",
		"rhs_weap_fnmag",
        "rhs_weap_m14ebrri",
        "rhsusf_weap_MP7A2",
        "rhsusf_weap_MP7A2_aor1",
        "rhsusf_weap_MP7A2_desert",
        "rhsusf_weap_MP7A2_winter",
        "BWA3_MG4",
        "BWA3_MG5",
        "BWA3_G36KA1",
        "BWA3_G36KA2",
        "BWA3_G36KA3",
        "BWA3_G36KA1_tan",
        "BWA3_G36KA2_tan",
        "BWA3_G36KA3_tan",
		"rhs_weap_SCARH_CQC",
		"rhs_weap_SCARH_STD",
		"rhs_weap_SCARH_LB",
		"rhs_weap_SCARH_FDE_CQC",
		"rhs_weap_SCARH_FDE_STD",
		"rhs_weap_SCARH_FDE_LB"
    };
};

class rhsusf_acc_wmx: Accessory {
    year = 2003;
    
    compatibleClasses[] = {
        "rhs_weap_m4a1_m203s_d",
        "rhs_weap_m4a1_blockII_M203_d"
    };
};

class rhs_acc_2dpzenit: Accessory {
    year = 2000;
    
    compatibleClasses[] = {
        "rhs_weap_m21a_pr",
        "rhs_weap_m21s_pr",
        "rhs_weap_m70b1n",
        "rhs_weap_m70ab2",
        "rhs_weap_m70b3n",
        "rhs_weap_ak103",
        "rhs_weap_ak103_npz",
        "rhs_weap_ak104",
        "rhs_weap_ak104_npz",
        "rhs_weap_ak105",
        "rhs_weap_ak105_npz",
		"rhs_weap_akm",
		"rhs_weap_akms",
        "rhs_weap_ak74m_camo",
		"rhs_weap_ak74m_desert",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74m_fullplum",
        "rhs_weap_ak74m_fullplum_npz",
        "rhs_weap_ak74m",
        "rhs_weap_ak74m_npz",
        "rhs_weap_ak74n",
        "rhs_weap_ak74n_npz",
        "rhs_weap_ak74n_2",
        "rhs_weap_ak74n_2_npz",
        "rhs_weap_aks74n",
        "rhs_weap_aks74n_npz"
    };
};

class rhs_acc_2dpzenit_ris: Accessory {
    year = 2000;
    
    compatibleClasses[] = {
        "rhs_weap_ak103_zenitco01",
        "rhs_weap_ak103_zenitco01_b33",
        "rhs_weap_ak104_zenitco01",
        "rhs_weap_ak104_zenitco01_b33",
        "rhs_weap_ak105_zenitco01",
        "rhs_weap_ak105_zenitco01_b33",
        "rhs_weap_ak74mr",
        "rhs_weap_ak74mr_gp25",
        "rhs_weap_asval_grip",
        "rhs_weap_asval_grip_npz"
    };
};
