/**
 * Universal Side IDs
 */
#define BLUFOR 0
#define OPFOR  1
#define INDEP  2
#define CIVIL  3


/**
 * Faction Types
 */
#define AIRBORNE 	"AIR"
#define MARINES		"MAR"
#define ARMORED		"ARM"
#define INFANTRY	"INF"
#define CONTRACTOR	"CON"
#define MILITIA		"MIL"
#define INSURGENTS	"INS"


/**
 * Accessory Slots
 */
#define SLOT_TOP    0
#define SLOT_SIDE   1
#define SLOT_BOTTOM 2
#define SLOT_BARREL 3
#define CHESTPACK   4
#define HEADMOUNT   5


/**
 * Types of Accessories
 * 
 * These are entirely arbitary, but tally with
 * levels defined by allowedAccessories in
 * CfgPlayableRoles to filter available options.
 */
#define OPTIC_NONE 0
#define OPTIC_1X   1
#define OPTIC_2X   2
#define OPTIC_3X   3
#define OPTIC_4X   4  
#define OPTIC_6X   6
#define OPTIC_8X   8

#define RAIL_NONE  0
#define RAIL_LIGHT 1
#define RAIL_LASER 2
#define RAIL_COMBO 3

#define REST_NONE  0
#define REST_GRIP  1
#define REST_BIPOD 2

#define BARREL_NONE  0
#define BARREL_FLASH 1
#define BARREL_SOUND 2

#define PARACHUTE   0

#define NIGHTVISION 0


/**
 * Types of Motorpool Surfaces
 *
 * Used to map types of vehicle to types of
 * location in bwi_motorpool. Ensures that only
 * ground vehicles can be spawned on land, sea
 * vehicles in water and so on.
 */
#define GROUND 		1
#define HELIPAD     2
#define HANGAR		3
#define APRON		4
#define CARRIER		5
#define SEA			6


/**
 * Levels of Vehicle
 *
 * Designates a vehicle a numerical level based 
 * on its capabilities. Used by bwi_motorpool for
 * restricting access to vehicles that will be
 * overpowered for a specific mission.
 */
#define UTILITY		1
#define ANTI_CAR	2
#define ANTI_APC	3
#define ANTI_IFV	4
#define ANTI_MBT	5