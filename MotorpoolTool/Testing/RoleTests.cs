﻿using MotorpoolTool.ARMA;
using MotorpoolTool.ARMA.Weapons;
using MotorpoolTool.BWI;
using MotorpoolTool.BWI.Roles;
using System;
using System.Linq;

namespace MotorpoolTool.Testing
{
    public static class RoleTests
    {
        // Some weapons come pre-loaded with ammo or don't use ammo (like the IED sweeper).
        private static readonly string[] whitelist =
        {
            "ACE_VMM3",
            "BWA3_RGW90_Loaded",
            "rhs_weap_M136",
            "rhs_weap_m72a7",
            "rhs_weap_m80",
            "rhs_weap_rpg26",
            "rhs_weap_rpg75",
            "rhs_weap_rshg2",
            "UK3CB_BAF_NLAW_Launcher",
            "UK3CB_BAF_AT4_CS_AT_Launcher"
        };

        public static bool WeaponHasCompatibleAmmo(Weapon weapon, Role role)
        {
            if (weapon == null)
            {
                return true; // If this unit doesn't have a weapon, then it doesn't need ammo.
            }
            else if (whitelist.Contains(weapon.ClassName, StringComparer.InvariantCultureIgnoreCase))
            {
                return true;
            }
            else if (WeaponCompatibleAmmoHelper(weapon, role.Uniform))
            {
                return true;
            }
            else if (WeaponCompatibleAmmoHelper(weapon, role.Vest))
            {
                return true;
            }
            else if (WeaponCompatibleAmmoHelper(weapon, role.Backpack))
            {
                return true;
            }

            return false;
        }

        private static bool WeaponCompatibleAmmoHelper(Weapon weapon, ARMAEntity container)
        {
            if (container == null)
            {
                return false;
            }

            foreach (var item in container.Inventory)
            {
                if (weapon.CompatibleMagazines.Contains(item.Key.ClassName, StringComparer.InvariantCultureIgnoreCase))
                {
                    return true;
                }
                else if (weapon.CompatibleMagazineWells.Contains(item.Key.ClassName, StringComparer.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
