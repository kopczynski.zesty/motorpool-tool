﻿using MotorpoolTool.ARMA;
using MotorpoolTool.Data;
using MotorpoolTool.GUI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MotorpoolTool.Testing
{
    public static class InhertianceTests
    {
        private static Dictionary<string, Vehicle> baseVehicles;

        static InhertianceTests()
        {
            baseVehicles = VehicleFactory.InitializeLookupFromDataset(FormMotorpoolTool.CFG_BASE_VEHICLES_PATH);
        }

        public static void PrintIfInheritanceViolated(Vehicle vehicle)
        {
            if (vehicle == null) { return; } // null vehicles don't have inheritance
            else if(baseVehicles.Count == 0) { return; } // don't do inheritance tests if the dataset was not valid

            if (!baseVehicles.ContainsKey(vehicle.ClassName.ToLowerInvariant()))
            {
                return; // this is the situation where the vehicle didn't exist before, thus its inheritance is always valid
            }
            else
            {
                Vehicle baseVehicle = baseVehicles[vehicle.ClassName.ToLowerInvariant()];
                if (!baseVehicle.Inhertiance.SequenceEqual(vehicle.Inhertiance))
                {
                    Console.WriteLine($"Error: Inheritance is inconsistent between '{vehicle}' and '{baseVehicle}'\n{{0}}\n{{1}}\n",
                        string.Join(", ", vehicle.Inhertiance), string.Join(", ", baseVehicle.Inhertiance));
                }
            }
        }
    }
}
