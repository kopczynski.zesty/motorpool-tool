﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MotorpoolTool.Utility
{
    public static class Helper
    {
        public static string StripComments(string s)
            => Regex.Replace(s, @"(/\*.*?\*/)*(//.*?\n)*", "", RegexOptions.Singleline);
    }
}
