﻿namespace MotorpoolTool.GUI
{
    partial class FormUpdateDataset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUpdateDataset));
            this.PictureBoxTutorial = new System.Windows.Forms.PictureBox();
            this.LabelUpdateDataset = new System.Windows.Forms.Label();
            this.ButtonUpdateCfgVehicles = new System.Windows.Forms.Button();
            this.ButtonUpdateCfgWeapons = new System.Windows.Forms.Button();
            this.ButtonUpdateCfgMagazines = new System.Windows.Forms.Button();
            this.LabelUpdateCfgWeapons = new System.Windows.Forms.Label();
            this.LabelUpdateCfgMagazines = new System.Windows.Forms.Label();
            this.LabelUpdateCfgVehicles = new System.Windows.Forms.Label();
            this.ScriptTimer = new System.Windows.Forms.Timer(this.components);
            this.ButtonUpdateCfgMagazineWells = new System.Windows.Forms.Button();
            this.LabelUpdateCfgMagazineWells = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxTutorial)).BeginInit();
            this.SuspendLayout();
            // 
            // PictureBoxTutorial
            // 
            this.PictureBoxTutorial.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PictureBoxTutorial.ImageLocation = "tutorial/updateDataset.jpg";
            this.PictureBoxTutorial.Location = new System.Drawing.Point(375, 13);
            this.PictureBoxTutorial.Name = "PictureBoxTutorial";
            this.PictureBoxTutorial.Size = new System.Drawing.Size(547, 506);
            this.PictureBoxTutorial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxTutorial.TabIndex = 0;
            this.PictureBoxTutorial.TabStop = false;
            // 
            // LabelUpdateDataset
            // 
            this.LabelUpdateDataset.AutoSize = true;
            this.LabelUpdateDataset.Location = new System.Drawing.Point(12, 13);
            this.LabelUpdateDataset.Name = "LabelUpdateDataset";
            this.LabelUpdateDataset.Size = new System.Drawing.Size(357, 377);
            this.LabelUpdateDataset.TabIndex = 1;
            this.LabelUpdateDataset.Text = resources.GetString("LabelUpdateDataset.Text");
            // 
            // ButtonUpdateCfgVehicles
            // 
            this.ButtonUpdateCfgVehicles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ButtonUpdateCfgVehicles.Location = new System.Drawing.Point(12, 409);
            this.ButtonUpdateCfgVehicles.Name = "ButtonUpdateCfgVehicles";
            this.ButtonUpdateCfgVehicles.Size = new System.Drawing.Size(159, 23);
            this.ButtonUpdateCfgVehicles.TabIndex = 2;
            this.ButtonUpdateCfgVehicles.Text = "Update CfgVehicles";
            this.ButtonUpdateCfgVehicles.UseVisualStyleBackColor = true;
            this.ButtonUpdateCfgVehicles.Click += new System.EventHandler(this.ButtonUpdateCfgVehicles_Click);
            // 
            // ButtonUpdateCfgWeapons
            // 
            this.ButtonUpdateCfgWeapons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ButtonUpdateCfgWeapons.Location = new System.Drawing.Point(12, 438);
            this.ButtonUpdateCfgWeapons.Name = "ButtonUpdateCfgWeapons";
            this.ButtonUpdateCfgWeapons.Size = new System.Drawing.Size(159, 23);
            this.ButtonUpdateCfgWeapons.TabIndex = 3;
            this.ButtonUpdateCfgWeapons.Text = "Update CfgWeapons";
            this.ButtonUpdateCfgWeapons.UseVisualStyleBackColor = true;
            this.ButtonUpdateCfgWeapons.Click += new System.EventHandler(this.ButtonUpdateCfgWeapons_Click);
            // 
            // ButtonUpdateCfgMagazines
            // 
            this.ButtonUpdateCfgMagazines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ButtonUpdateCfgMagazines.Location = new System.Drawing.Point(12, 467);
            this.ButtonUpdateCfgMagazines.Name = "ButtonUpdateCfgMagazines";
            this.ButtonUpdateCfgMagazines.Size = new System.Drawing.Size(159, 23);
            this.ButtonUpdateCfgMagazines.TabIndex = 4;
            this.ButtonUpdateCfgMagazines.Text = "Update CfgMagazines";
            this.ButtonUpdateCfgMagazines.UseVisualStyleBackColor = true;
            this.ButtonUpdateCfgMagazines.Click += new System.EventHandler(this.ButtonUpdateCfgMagazines_Click);
            // 
            // LabelUpdateCfgWeapons
            // 
            this.LabelUpdateCfgWeapons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelUpdateCfgWeapons.AutoSize = true;
            this.LabelUpdateCfgWeapons.Location = new System.Drawing.Point(177, 443);
            this.LabelUpdateCfgWeapons.Name = "LabelUpdateCfgWeapons";
            this.LabelUpdateCfgWeapons.Size = new System.Drawing.Size(66, 13);
            this.LabelUpdateCfgWeapons.TabIndex = 5;
            this.LabelUpdateCfgWeapons.Text = "Dataset idle.";
            // 
            // LabelUpdateCfgMagazines
            // 
            this.LabelUpdateCfgMagazines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelUpdateCfgMagazines.AutoSize = true;
            this.LabelUpdateCfgMagazines.Location = new System.Drawing.Point(177, 472);
            this.LabelUpdateCfgMagazines.Name = "LabelUpdateCfgMagazines";
            this.LabelUpdateCfgMagazines.Size = new System.Drawing.Size(66, 13);
            this.LabelUpdateCfgMagazines.TabIndex = 6;
            this.LabelUpdateCfgMagazines.Text = "Dataset idle.";
            // 
            // LabelUpdateCfgVehicles
            // 
            this.LabelUpdateCfgVehicles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelUpdateCfgVehicles.AutoSize = true;
            this.LabelUpdateCfgVehicles.Location = new System.Drawing.Point(177, 414);
            this.LabelUpdateCfgVehicles.Name = "LabelUpdateCfgVehicles";
            this.LabelUpdateCfgVehicles.Size = new System.Drawing.Size(66, 13);
            this.LabelUpdateCfgVehicles.TabIndex = 7;
            this.LabelUpdateCfgVehicles.Text = "Dataset idle.";
            // 
            // ScriptTimer
            // 
            this.ScriptTimer.Interval = 1000;
            this.ScriptTimer.Tick += new System.EventHandler(this.ScriptTimer_Tick);
            // 
            // ButtonUpdateCfgMagazineWells
            // 
            this.ButtonUpdateCfgMagazineWells.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ButtonUpdateCfgMagazineWells.Location = new System.Drawing.Point(12, 496);
            this.ButtonUpdateCfgMagazineWells.Name = "ButtonUpdateCfgMagazineWells";
            this.ButtonUpdateCfgMagazineWells.Size = new System.Drawing.Size(159, 23);
            this.ButtonUpdateCfgMagazineWells.TabIndex = 8;
            this.ButtonUpdateCfgMagazineWells.Text = "Update CfgMagazineWells";
            this.ButtonUpdateCfgMagazineWells.UseVisualStyleBackColor = true;
            this.ButtonUpdateCfgMagazineWells.Click += new System.EventHandler(this.ButtonUpdateCfgMagazineWells_Click);
            // 
            // LabelUpdateCfgMagazineWells
            // 
            this.LabelUpdateCfgMagazineWells.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelUpdateCfgMagazineWells.AutoSize = true;
            this.LabelUpdateCfgMagazineWells.Location = new System.Drawing.Point(177, 501);
            this.LabelUpdateCfgMagazineWells.Name = "LabelUpdateCfgMagazineWells";
            this.LabelUpdateCfgMagazineWells.Size = new System.Drawing.Size(66, 13);
            this.LabelUpdateCfgMagazineWells.TabIndex = 9;
            this.LabelUpdateCfgMagazineWells.Text = "Dataset idle.";
            // 
            // FormUpdateDataset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 531);
            this.Controls.Add(this.LabelUpdateCfgMagazineWells);
            this.Controls.Add(this.ButtonUpdateCfgMagazineWells);
            this.Controls.Add(this.LabelUpdateCfgVehicles);
            this.Controls.Add(this.LabelUpdateCfgMagazines);
            this.Controls.Add(this.LabelUpdateCfgWeapons);
            this.Controls.Add(this.ButtonUpdateCfgMagazines);
            this.Controls.Add(this.ButtonUpdateCfgWeapons);
            this.Controls.Add(this.ButtonUpdateCfgVehicles);
            this.Controls.Add(this.LabelUpdateDataset);
            this.Controls.Add(this.PictureBoxTutorial);
            this.MinimumSize = new System.Drawing.Size(950, 570);
            this.Name = "FormUpdateDataset";
            this.Text = "Update Dataset";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormUpdateDataset_FormClosing);
            this.Load += new System.EventHandler(this.FormUpdateDataset_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxTutorial)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBoxTutorial;
        private System.Windows.Forms.Label LabelUpdateDataset;
        private System.Windows.Forms.Button ButtonUpdateCfgVehicles;
        private System.Windows.Forms.Button ButtonUpdateCfgWeapons;
        private System.Windows.Forms.Button ButtonUpdateCfgMagazines;
        private System.Windows.Forms.Label LabelUpdateCfgWeapons;
        private System.Windows.Forms.Label LabelUpdateCfgMagazines;
        private System.Windows.Forms.Label LabelUpdateCfgVehicles;
        private System.Windows.Forms.Timer ScriptTimer;
        private System.Windows.Forms.Button ButtonUpdateCfgMagazineWells;
        private System.Windows.Forms.Label LabelUpdateCfgMagazineWells;
    }
}