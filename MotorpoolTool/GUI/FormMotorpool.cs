﻿using BrightIdeasSoftware;
using MotorpoolTool.ARMA;
using MotorpoolTool.BWI;
using MotorpoolTool.Data;
using MotorpoolTool.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using static MotorpoolTool.ARMA.Constants;
using MotorpoolTool.BWI.Accessories;
using MotorpoolTool.ARMA.Weapons;
using MotorpoolTool.BWI.Roles;
using MotorpoolTool.BWI.Stagings;
using MotorpoolTool.BWI.Supplies;
using System.Text;

namespace MotorpoolTool.GUI
{

    public partial class FormMotorpoolTool : Form
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        private const string MPT_VEHICLE_THUMBNAILS = @"thumbnails\{0}.jpg";
        private static Image MPT_VEHICLE_MISSING = Bitmap.FromFile(@"thumbnails\missing.jpg");

        public const string CFG_BASE_VEHICLES_PATH = @"cfg\base\CfgVehicles.txt";
        public const string CFG_VEHICLES_PATH = @"cfg\CfgVehicles.txt";

        public const string CFG_WEAPONS_PATH = @"cfg\CfgWeapons.txt";
        public const string CFG_MAGAZINES_PATH = @"cfg\CfgMagazines.txt";
        public const string CFG_MAGAZINE_WELLS_PATH = @"cfg\CfgMagazineWells.txt";
        public const string CFG_WORLDS_PATH = @"cfg\CfgWorlds.txt";

        private const string BWI_DATA_VEHICLES = @"data_main\configs\vehicles";
        private const string BWI_DATA_FACTIONS = @"data_main\configs\factions";
        private const string BWI_DATA_ROLES = @"data_main\configs\roles";
        private const string BWI_DATA_ACCESSORIES = @"data_main\configs\accessories";
        private const string BWI_DATA_SUPPLIES = @"data_main\configs\supplies";
        private const string BWI_RESUPPLY = @"resupply_main\configs";
        private const string BWI_TEMPLATES = @"templates";
        private const string SKIP_FILES = @"categories.hpp";

        private static bool enableSmallPanelMode = true;

        public static List<Element> allElements = new List<Element>();
        public static List<AccessoryGroup> accesoryGroups;
        public static List<SupplyGroup> supplyGroups;

        private static Form StagingViewer;

        public FormMotorpoolTool()
        {
            InitializeComponent();
        }

        private void FormPlatoonPlanner_Load(object sender, EventArgs e)
        {
            // Handle settings.
            this.Hide(); // TODO move most of this logic to program.cs
            LoadSettings(this);
            LoadDatasets();
            this.Show();

            // Check user pref for dev console.
            if (!EnableDEVModeToolStripMenuItem.Checked)
            {
                ShowWindow(GetConsoleWindow(), SW_HIDE);
            }
        }

        public void LoadDatasets()
        {
            Console.Clear();
            ListBoxWeapons.Items.Clear();
            ObjectListViewMotorpool.Items.Clear();
            TreeListViewSides.Items.Clear();

            LoadCfgData();

            // Variables used for text processing the config files, and later for GUI display.
            List<FactionGroup> allFactionGroups = new List<FactionGroup>();

            // Read all motorpool vehicle configs
            string[] allFiles = Directory.GetFiles(BWI_DATA_VEHICLES, "*.hpp", SearchOption.AllDirectories);
            int skippedFiles = 0;

            // Start reading through motorpool configs
            foreach (string file in allFiles)
            {
                if (file.Contains(SKIP_FILES))
                {
                    skippedFiles++;
                    continue; // skip categories.
                }
                VehicleFactory.InitializeMotorpool(file);
            }
            Console.WriteLine("Skipped {0} files named '{1}'.", skippedFiles, SKIP_FILES);

            // Start reading through BWI Templates
            allFiles = Directory.GetFiles(BWI_TEMPLATES, "*.ext", SearchOption.AllDirectories);
            Staging.StagingListDictionary.Clear();
            foreach (string file in allFiles)
            {
                StagingFactory.InitalizeStagings(file);
            }

            // Setup Roles
            allFiles = Directory.GetFiles(BWI_DATA_ROLES, "elements.hpp", SearchOption.AllDirectories);
            foreach (string file in allFiles)
            {
                string roles = File.ReadAllText(file);
                var dir = Directory.GetParent(file);
                allElements.AddRange(ElementFactory.StringToList(roles, dir.ToString()));
            }

            TreeListViewRoles.CanExpandGetter = delegate (Object x)
            {
                return (x is Element);
            };
            TreeListViewRoles.ChildrenGetter = delegate (Object x)
            {
                if (x is Element)
                    return ((Element)x).Roles;
                if (x is Role)
                    return ((Role)x).Name;

                throw new ArgumentException("Tell Andrew that TreeListViewRoles crashed the program.");
            };
            //TreeListViewRoles.Roots = allElements;


            // Setup Faction Groups TODO Rewrite to handle files in function
            allFiles = Directory.GetFiles(BWI_DATA_FACTIONS, "factions.hpp", SearchOption.AllDirectories);
            foreach (string file in allFiles)
            {
                string factionGroups = System.IO.File.ReadAllText(file);
                var dir = Directory.GetParent(file);
                allFactionGroups.AddRange(FactionGroupFactory.StringToList(factionGroups, dir.ToString()));
            }

            // Add debug faction group.
            allFactionGroups.Add(GetDebugFactionGroup(allFactionGroups));

            // Forces = BLUFOR, OPFOR, etc.
            List<Force> forces = new List<Force>();
            forces.Add(new Force(Side.BLUFOR));
            forces.Add(new Force(Side.OPFOR));
            forces.Add(new Force(Side.INDEP));
            forces.Add(new Force(Side.CIVIL));

            foreach (Force force in forces)
            {
                foreach (FactionGroup fg in allFactionGroups)
                {
                    if (force.Side == fg.Side)
                    {
                        force.FactionGroups.Add(fg);
                    }
                }
            }

            // Setup TreeView for Forces
            TreeListViewSides.CanExpandGetter = delegate (Object x)
            {
                return (x is Force || x is FactionGroup);
            };
            TreeListViewSides.ChildrenGetter = delegate (Object x)
            {
                if (x is Force)
                    return ((Force)x).FactionGroups;
                if (x is FactionGroup)
                    return ((FactionGroup)x).SubFactions;

                throw new ArgumentException("TreeListView side invalid child");
            };
            TreeListViewSides.Roots = forces;

            // Setup TreeView for Resupply
            TreeListViewResupply.CanExpandGetter = delegate (Object x)
            {
                return (x is SupplyGroup);
            };
            TreeListViewResupply.ChildrenGetter = delegate (Object x)
            {
                if (x is SupplyGroup)
                    return ((SupplyGroup)x).Supplies;

                throw new ArgumentException("TreeListViewResupply invalid child");
            };
            TreeListViewResupply.Roots = supplyGroups;

            // Print some stats
            PrintDebugStats();
            Console.WriteLine("Done!");

            // Select the first faction group right away.
            if (allFactionGroups.Count > 0)
            {
                //ListBoxFactionGroups.SelectedIndex = 0;
                TreeListViewSides.SelectedIndex = 0;
            }

            // TODO Debug Mk19 (can be cleaned up later)
            /*
            Console.WriteLine("\n* * * Debug: Vehicles with Mk19 M1001 Canister Magazines * * *");
            var mk19_48rnd = Magazine.Lookup("RHS_48Rnd_40mm_MK19_M1001");
            var mk19_96rnd = Magazine.Lookup("RHS_96Rnd_40mm_MK19_M1001");

            foreach (var group in allFactionGroups)
            {
                foreach (var faction in group.SubFactions)
                {
                    foreach (var vic in faction.GetAllVehicles(true))
                    {
                        if (vic.HasTurrets)
                        {
                            foreach (var turret in vic.Turrets)
                            {
                                if (turret.Magazines == null) { continue; }
                                if (turret.Magazines.Contains(mk19_48rnd))
                                {
                                    Console.WriteLine($"**{group} {faction}:** {vic} [{mk19_48rnd} x{turret.Magazines.Count(x => x.Equals(mk19_48rnd))}]");
                                }
                                if (turret.Magazines.Contains(mk19_96rnd))
                                {
                                    Console.WriteLine($"**{group} {faction}:** {vic} [{mk19_48rnd} x{turret.Magazines.Count(x => x.Equals(mk19_96rnd))}]");
                                }
                            }
                        }
                    }
                }
            }
            */

            // TODO Remember expanded branches. Need to store TreeListViewSides.ExpandedObjects
            foreach (Force branch in TreeListViewSides.Roots)
            {
                TreeListViewSides.Expand(branch);
            }
        }

        public static void LoadCfgData()
        {
            // Announce program title, and attempt to load the external .txt containing a bunch of ARMA 3 information.
            Console.WriteLine("Motorpool Tool");
            Console.WriteLine("Working directory is '{0}'.", BWI_DATA_VEHICLES);

            // Prepare files from raw exported script
            Cfg.Prepare(CFG_WEAPONS_PATH);
            Cfg.Prepare(CFG_MAGAZINES_PATH);
            Cfg.Prepare(CFG_MAGAZINE_WELLS_PATH);
            Cfg.Prepare(CFG_BASE_VEHICLES_PATH); // For InheritanceTests.cs
            Cfg.Prepare(CFG_VEHICLES_PATH);
            Cfg.Prepare(CFG_WORLDS_PATH);

            // Load Cfgs
            Console.WriteLine();
            MagazineFactory.InitializeLookupFromDataset(CFG_MAGAZINES_PATH);
            MagazineWellFactory.InitializeLookupFromDataset(CFG_MAGAZINE_WELLS_PATH);
            WeaponFactory.InitializeLookupFromDataset(CFG_WEAPONS_PATH);
            VehicleFactory.InitializeLookupFromDataset(CFG_VEHICLES_PATH);
            WorldFactory.InitializeLookupFromDataset(CFG_WORLDS_PATH);

            // Accessories
            accesoryGroups = AccessoryGroupFactory.FileToList(BWI_DATA_ACCESSORIES);

            // Resupply
            ResupplyFactory.InitializeLookupFromDirectory(BWI_RESUPPLY);
            supplyGroups = SupplyGroupFactory.DirectoryToList(BWI_DATA_SUPPLIES);            

            Console.WriteLine(DateTime.Now);
            Console.WriteLine();
        }

        private static void PrintDebugStats()
        {
            // Debug statistics
            Console.WriteLine("Read {0} CfgVehicles configs successfully.", Vehicle.VehicleDictionary.Count);
            Console.WriteLine("\tFirst entry '{0}' to last entry '{1}'\n", Vehicle.VehicleDictionary.First().Value, Vehicle.VehicleDictionary.Last().Value);

            Console.WriteLine("Read {0} BWI Motorpool entries successfully.", MotorpoolVehicle.Lookup.Count);
            Console.WriteLine("\tFirst entry '{0}' to last entry '{1}'\n", MotorpoolVehicle.Lookup.First().Value, MotorpoolVehicle.Lookup.Last().Value);

            Console.WriteLine($"Read {Magazine.MagazineDictionary.Count} CfgMagazines entries successfully.");
            Console.WriteLine("\tFirst entry '{0}' to last entry '{1}'\n", Magazine.MagazineDictionary.First().Value, Magazine.MagazineDictionary.Last().Value);

            Console.WriteLine($"Read {MagazineWellFactory.MagazineWellsLookup.Count} CfgMagazineWells entries successfully.");
            Console.WriteLine("\tFirst entry '{0}' to last entry '{1}'\n", MagazineWellFactory.MagazineWellsLookup.First().Value, MagazineWellFactory.MagazineWellsLookup.Last().Value);

            Console.WriteLine($"Read {Weapon.WeaponDictionary.Count} CfgWeapons entries successfully.");
            Console.WriteLine("\tFirst entry '{0}' to last entry '{1}'\n", Weapon.WeaponDictionary.First().Value, Weapon.WeaponDictionary.Last().Value);
        }

        private static FactionGroup GetDebugFactionGroup(List<FactionGroup> allFactionGroups)
        {
            List<string> vehiclesThatAreUsed = new List<string>();
            foreach (FactionGroup factionGroup in allFactionGroups)
            {
                foreach (Faction faction in factionGroup.SubFactions)
                {
                    foreach (MotorpoolVehicle vic in faction.GetAllVehicles())
                    {
                        if (!vehiclesThatAreUsed.Contains(vic.ClassName.ToLowerInvariant()))
                        {
                            vehiclesThatAreUsed.Add(vic.ClassName.ToLowerInvariant());
                        }
                    }
                }
            }

            List<MotorpoolVehicle> vehiclesThatAreNotUsed = new List<MotorpoolVehicle>();
            List<MotorpoolVehicle> allVehicles = new List<MotorpoolVehicle>();
            foreach (var vic in MotorpoolVehicle.Lookup)
            {
                allVehicles.Add(vic.Value);
                if (!vehiclesThatAreUsed.Contains(vic.Value.ClassName.ToLowerInvariant()))
                {
                    vehiclesThatAreNotUsed.Add(vic.Value);
                }
            }


            FactionGroup debugGroup = new FactionGroup("debug", "Debug Factions", "CIVIL", null);
            Faction debugUnusedVehiclesFaction = new Faction(debugGroup, "debug", "Vehicles not in any faction", 2020, "INFANTRY",
                Path.Combine(BWI_DATA_FACTIONS, @"urs_contractor\2015\uniform_w.sqf"),
                Path.Combine(BWI_DATA_FACTIONS, @"urs_contractor\2015\weapons.sqf"),
                Path.Combine(BWI_DATA_FACTIONS, @"urs_contractor\2015\grenades.sqf"),
                Path.Combine(BWI_DATA_FACTIONS, @"urs_contractor\2015\ammo.sqf"), null, null, null, null, null, null);
            Faction debugAllVehiclesFaction = new Faction(debugGroup, "debug", "All vehicles in motorpool", 2020, "INFANTRY",
                Path.Combine(BWI_DATA_FACTIONS, @"urs_contractor\2015\uniform_w.sqf"),
                Path.Combine(BWI_DATA_FACTIONS, @"urs_contractor\2015\weapons.sqf"),
                Path.Combine(BWI_DATA_FACTIONS, @"urs_contractor\2015\grenades.sqf"),
                Path.Combine(BWI_DATA_FACTIONS, @"urs_contractor\2015\ammo.sqf"), null, null, null, null, null, null);
            debugGroup.SubFactions.Add(debugUnusedVehiclesFaction);
            debugGroup.SubFactions.Add(debugAllVehiclesFaction);
            debugUnusedVehiclesFaction.MotorpoolRegulars = vehiclesThatAreNotUsed;
            debugAllVehiclesFaction.MotorpoolRegulars = allVehicles;

            

            return debugGroup;
        }

        private void TreeListViewSides_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TreeListViewSides.SelectedItem != null && TreeListViewSides.SelectedItem.RowObject is Faction)
            {
                Faction faction = TreeListViewSides.SelectedItem.RowObject as Faction;

                if (faction != null && faction.MotorpoolRegulars != null)
                {
                    // Update Roles
                    foreach (var element in allElements)
                    {
                        if (faction.HiddenElements != null && faction.HiddenElements.Contains(element.ClassName))
                        {
                            element.IsHidden = true;
                        }
                        else
                        {
                            element.IsHidden = false;
                        }
                        foreach (var role in element.Roles)
                        {
                            // Have to hide role if element is hidden.
                            // Tree list view will always show the parent element if there is a visible child.
                            role.IsHidden = element.IsHidden;
                            if (faction.HiddenRoles != null && faction.HiddenRoles.Contains(role.ClassName))
                            {
                                role.IsHidden = true;
                            }
                            else
                            {
                                role.IsHidden = false;
                            }
                        }
                    }
                    TreeListViewRoles.Roots = allElements;

                    // Tree List view will show filtered things if they are expanded.
                    // This collaspes hidden items automatically so they are properly filtered out.
                    foreach (var item in TreeListViewRoles.Roots)
                    {
                        if (item is Element && ((Element)item).IsHidden)
                        {
                            TreeListViewRoles.Collapse(item);
                        }
                    }

                    // Apply filter (hides HiddenElements and HiddenRoles)
                    SetTreeListViewRolesFilter(TextBoxSearch.Text);

                    // Update resupply list
                    SetTreeListViewResupplyFilter(faction);


                    // Update ListBox so user can click vehicles for more information.
                    ObjectListViewMotorpool.Items.Clear();
                    ObjectListViewMotorpool.SetObjects(faction.GetAllVehicles(false));

                    // Update textboxes.
                    TextBoxClass1.Text = faction.VehiclesInMotorpoolLevelToString(Constants.UTILITY);
                    TextBoxClass2.Text = faction.VehiclesInMotorpoolLevelToString(Constants.ANTI_CAR);
                    TextBoxClass3.Text = faction.VehiclesInMotorpoolLevelToString(Constants.ANTI_APC);
                    TextBoxClass4.Text = faction.VehiclesInMotorpoolLevelToString(Constants.ANTI_IFV);
                    TextBoxClass5.Text = faction.VehiclesInMotorpoolLevelToString(Constants.ANTI_MBT);

                    // Update Faction Information Section with some fun facts
                    LabelVehiclesClass1.Text = string.Format("CLASS I Utility Vehicles: {0}", faction.GetVehiclesInMotorpoolLevel(Constants.UTILITY).Count);
                    LabelVehiclesClass2.Text = string.Format("CLASS II Anti-Car Vehicles: {0}", faction.GetVehiclesInMotorpoolLevel(Constants.ANTI_CAR).Count);
                    LabelVehiclesClass3.Text = string.Format("CLASS III Anti-APC Vehicles: {0}", faction.GetVehiclesInMotorpoolLevel(Constants.ANTI_APC).Count);
                    LabelVehiclesClass4.Text = string.Format("CLASS IV Anti-IFV Vehicles: {0}", faction.GetVehiclesInMotorpoolLevel(Constants.ANTI_IFV).Count);
                    LabelVehiclesClass5.Text = string.Format("CLASS V Anti-MBT Vehicles: {0}", faction.GetVehiclesInMotorpoolLevel(Constants.ANTI_MBT).Count);

                    LabelSubFaction.Text = string.Format("Sub-faction - {0} - {1} Vehicles Total", faction.Type, faction.GetAllVehicles().Count);

                    // Update selected role
                    TreeListViewRoles_SelectedIndexChanged(sender, e);


                    // TODO Better place for tests
                    //foreach(Vehicle vehicle in f.GetAllVehicles(true))
                    //{
                    //    Testing.InhertianceTests.PrintIfInheritanceViolated(vehicle);
                    //}
                }
            }
        }

        private void UpdatePictureBox(PictureBox pictureBox, string path)
        {
            if (pictureBox.Image != MPT_VEHICLE_MISSING && pictureBox.Image != null)
            {
                pictureBox.Image.Dispose();
            }

            if (File.Exists(path))
            {
                pictureBox.Load(path);
            }
            else
            {
                pictureBox.Image = MPT_VEHICLE_MISSING; // No image found.
            }
        }

        private void ObjectListViewMotorpool_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                MotorpoolVehicle vic = ObjectListViewMotorpool.SelectedObject as MotorpoolVehicle;

                if (vic != null)
                {
                    

                    string specialText = string.Empty;
                    if (vic.Reammo > 0)
                    {
                        specialText += specialText.Length == 0 ? "Rearm" : ", Rearm";
                    }
                    if (vic.Refuel > 0)
                    {
                        specialText += specialText.Length == 0 ? "Refuel" : ", Refuel";
                    }
                    if (vic.Repair)
                    {
                        specialText += specialText.Length == 0 ? "Repair" : ", Repair";
                    }

                    string restriction = "Regulars";
                    if(vic.IsRecon)
                    {
                        restriction = "Recon";
                    }
                    else if(vic.IsSpecialForces)
                    {
                        restriction = "Special Forces";
                    }

                    // User textboxes
                    TextBoxVehicleInfoRestriction.Text = $"{restriction}";
                    TextBoxVehicleEscalation.Text = $"{vic.EscalationString}";
                    TextBoxVehicleInfoCategory.Text = specialText.Equals(string.Empty) ? vic.CategoryString : string.Format("{0} {1}", specialText, vic.CategoryString);
                    
                    TextBoxVehicleInfoCrew.Text = $"{vic.GetCrewCount()} crew";
                    TextBoxVehicleInfoPax.Text = $"{vic.GetPassengerCount()} passengers";
                    TextBoxVehicleInfoFloats.Text = $"{vic.Floats}";

                    TextBoxVehicleInfoCost.Text = $"{vic.Cost} points";
                    TextBoxVehicleInfoCargo.Text = $"{vic.CargoSpace} cargo space";
                    TextBoxVehicleInfoCOP.Text = $"{vic.Outpost}";


                    // Developer relevant textboxes.
                    TextBoxClassName.Text = vic.ClassName;
                    TextBoxInhertiance.Text = string.Join(", ", vic.Inhertiance);
                    TextBoxBWIClass.Text = string.Format("{0}", vic.BWIClass);

                    // Clear Pylon list, check if this vehicle has pylons.
                    ListBoxWeapons.Items.Clear();
                    TextBoxWeaponClassName.Text = string.Empty;
                    TextBoxWeaponDescription.Text = string.Empty;
                    TextBoxWeaponAmmoCount.Text = string.Empty;

                    if (vic.HasWeapons || vic.HasPylons)
                    {
                        ListBoxWeapons.Items.Add(vic.Category == Constants.ROTARY || vic.Category == Constants.FIXED ? "Pilot Weapons" : "Driver Equipment");
                    }

                    // Handle fixed weapons like plane autocannons.
                    if (vic.HasWeapons)
                    {
                        foreach (Weapon weapon in vic.Weapons)
                        {
                            weapon.Indent = true;
                            ListBoxWeapons.Items.Add(weapon);
                        }
                    }

                    // Handle pilot pylon weapons. Gunner pylons weapons handled later.
                    if (vic.HasPylons)
                    {
                        foreach (Magazine pylon in vic.Pylons)
                        {
                            pylon.Suffix = showVehicleAmmoAsSuffixToolStripMenuItem.Checked;
                            pylon.Filter = true;
                            if (pylon.Control == Constants.PILOT)
                            {
                                pylon.Indent = true;
                                ListBoxWeapons.Items.Add(pylon);
                            }
                        }
                        ListBoxWeapons.SelectedIndex = 0;
                    }

                    if ((vic.Turrets != null && vic.Turrets.Any(b => b.IsPrimaryTurret))
                        || (vic.Pylons != null && vic.Pylons.Any(b => b.Control == Constants.GUNNER)))
                    {
                        ListBoxWeapons.Items.Add("Gunner Weapons");
                    }

                    // Handle turret weapons like gunship turrets.
                    if (vic.HasTurrets)
                    {
                        foreach (Turret turret in vic.Turrets)
                        {
                            if(turret.HasWeapons)
                            {
                                ListBoxWeapons.Items.Remove("Gunner Weapons");
                                ListBoxWeapons.Items.Add(turret); // replace it with real turret
                                if (turret.Weapons == null) { continue; } // commander turret
                                foreach (Weapon weapon in turret.Weapons)
                                {
                                    weapon.Indent = true;
                                    ListBoxWeapons.Items.Add(weapon);
                                }
                            }
                            
                        }
                    }

                    // Handle gunner pylons.
                    if (vic.HasPylons)
                    {
                        foreach (Magazine pylon in vic.Pylons)
                        {
                            if (pylon.Control == Constants.GUNNER)
                            {
                                pylon.Indent = true;
                                ListBoxWeapons.Items.Add(pylon);
                            }
                        }
                    }

                    // Attempt to retrieve a thumbnail for this vehicle.
                    string thumbnailPath = string.Format(MPT_VEHICLE_THUMBNAILS, vic.ClassName);
                    UpdatePictureBox(PictureBoxVehicle, thumbnailPath);

                    // Attempt to select first pylon.
                    if (ListBoxWeapons.Items.Count > 0) { ListBoxWeapons.SelectedIndex = 0; }


                }
            }
            catch (Exception ex)
            {
                ShowCrashMessageBox(ex);
            }
        }

        private void ListBoxWeapons_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                object selectedWeapon = ListBoxWeapons.SelectedItem;
                MotorpoolVehicle vic = ObjectListViewMotorpool.SelectedObject as MotorpoolVehicle;

                TextBoxWeaponClassName.Text = string.Empty;
                TextBoxWeaponDescription.Text = string.Empty;
                TextBoxWeaponAmmoCount.Text = "N/A";

                if (selectedWeapon == null || vic == null)
                {
                    return;
                }

                if (selectedWeapon is Magazine pylon)
                {
                    TextBoxWeaponClassName.Text = pylon.ClassName;
                    TextBoxWeaponDescription.Text = pylon.Description;
                    TextBoxWeaponAmmoCount.Text = string.Format("{0}", pylon.Ammo.ToString());
                }
                else if (selectedWeapon is Weapon weapon)
                {
                    TextBoxWeaponClassName.Text = weapon.ClassName;
                    TextBoxWeaponDescription.Text = weapon.Description;

                    // Stuff like aircraft MASTERSAFE will never have compatible magazines.
                    if (weapon.CompatibleMagazines == null)
                    {
                        return;
                    }

                    // Get list of compatible magazines for this weapon.
                    IEnumerable<Magazine> data;
                    if (weapon.Parent != null)
                    {
                        data = weapon.Parent.Magazines.Where(x => weapon.CompatibleMagazines.Contains(x.ClassName, StringComparer.CurrentCultureIgnoreCase));
                    }
                    else
                    {
                        data = vic.Magazines.Where(x => weapon.CompatibleMagazines.Contains(x.ClassName, StringComparer.CurrentCultureIgnoreCase));
                    }

                    if (data.Count() == 0)
                    {
                        return; // no magazines :(
                    }

                    int totalAmmo = data.Sum(mag => mag.Ammo);

                    string ammoFormat = string.Empty;
                    var groupOfMagazines = data
                        .GroupBy(mag => mag.ClassName)
                        .Select(grp => grp.ToList())
                        .ToList();

                    foreach(var group in groupOfMagazines)
                    {
                        var magazine = group.First();

                        if (showVehicleAmmoAsSuffixToolStripMenuItem.Checked)
                        {
                            ammoFormat += string.Format("{0} ( x{1}{2} ){3}",
                                magazine,
                                group.Count(),
                                magazine.Ammo <= 1 ? string.Empty : $" × {magazine.Ammo}rnd",
                                Environment.NewLine);
                        }
                        else
                        {
                            //ammoFormat += $"(x{group.Count()}{(magazine.Ammo <= 1 ? string.Empty : $" × {magazine.Ammo}rnd")}) {magazine}{Environment.NewLine}";
                            ammoFormat += string.Format("( x{1}{2} ) {0}{3}",
                                magazine,
                                group.Count(),
                                magazine.Ammo <= 1 ? string.Empty : $" × {magazine.Ammo}rnd",
                                Environment.NewLine);
                        }
                    }

                    TextBoxWeaponAmmoCount.Text = $"{ammoFormat}";
                    if(groupOfMagazines.Count > 1)
                    {
                        TextBoxWeaponAmmoCount.Text += $"{totalAmmo} total round{(totalAmmo == 1 ? string.Empty : "s")}";
                    }
                }
                else if (selectedWeapon is Turret turret)
                {
                    TextBoxWeaponClassName.Text = turret.ClassName;
                }
            }
            catch (Exception ex)
            {
                ShowCrashMessageBox(ex);
            }
        }

        private void TextBoxClass1_Click(object sender, EventArgs e)
        {
            TextBoxClass1.SelectAll();
            TextBoxClass1.Copy();
        }

        private void TextBoxClass2_Click(object sender, EventArgs e)
        {
            TextBoxClass2.SelectAll();
            TextBoxClass2.Copy();
        }

        private void TextBoxClass3_Click(object sender, EventArgs e)
        {
            TextBoxClass3.SelectAll();
            TextBoxClass3.Copy();
        }

        private void TextBoxClass4_Click(object sender, EventArgs e)
        {
            TextBoxClass4.SelectAll();
            TextBoxClass4.Copy();
        }

        private void TextBoxClass5_Click(object sender, EventArgs e)
        {
            TextBoxClass5.SelectAll();
            TextBoxClass5.Copy();
        }

        private void TextBoxClassName_Click(object sender, EventArgs e)
        {
            TextBoxClassName.SelectAll();
            TextBoxClassName.Copy();
        }

        private void TextBoxInhertiance_Click(object sender, EventArgs e)
        {
            TextBoxInhertiance.SelectAll();
            TextBoxInhertiance.Copy();
        }

        private void ShowDeveloperDebugInfoToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            ToggleDeveloperInformation((sender as ToolStripMenuItem).Checked);
        }

        private void ToggleDeveloperInformation(bool shouldDisplay)
        {
            LabelBWIClass.Visible = shouldDisplay;
            TextBoxBWIClass.Visible = shouldDisplay;

            LabelClassName.Visible = shouldDisplay;
            TextBoxClassName.Visible = shouldDisplay;

            if (shouldDisplay && !TabControlMotorpoolInfo.TabPages.Contains(TabPageVehicleDeveloperInfo))
                TabControlMotorpoolInfo.TabPages.Insert(1, TabPageVehicleDeveloperInfo);
            else
                TabControlMotorpoolInfo.TabPages.Remove(TabPageVehicleDeveloperInfo);

            MPColumnMaximumLoad.IsVisible = shouldDisplay && !enableSmallPanelMode;
            MPColumnTransportAmmo.IsVisible = shouldDisplay && !enableSmallPanelMode;
            MPColumnTransportFuel.IsVisible = shouldDisplay && !enableSmallPanelMode;

            LabelWeaponClassName.Visible = shouldDisplay;
            TextBoxWeaponClassName.Visible = shouldDisplay;

            LabelInhertiance.Visible = shouldDisplay;
            TextBoxInhertiance.Visible = shouldDisplay;

            ObjectListViewMotorpool.RebuildColumns();

            // Console window show / hide
            if(this.Visible)
            {
                ShowWindow(GetConsoleWindow(), shouldDisplay ? SW_SHOW : SW_HIDE);
            }
            
        }

        private void ShowDeveloperVehicleInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ctrl = sender as ToolStripMenuItem;
            ctrl.Checked = !ctrl.Checked;
        }
        private void SuffixPylonStripMenuItem_Click(object sender, EventArgs e)
        {
            showVehicleAmmoAsSuffixToolStripMenuItem.Checked = !showVehicleAmmoAsSuffixToolStripMenuItem.Checked;
            // refresh vehicle
            ObjectListViewMotorpool_SelectedIndexChanged(sender, e);
        }

        private void GroupItemsInInventoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupItemsInInventoryToolStripMenuItem.Checked = !groupItemsInInventoryToolStripMenuItem.Checked;
            // refresh roles
            TreeListViewRoles_SelectedIndexChanged(sender, e);
        }

        private void ShowInventoryQuantityAsSuffixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showInventoryQuantityAsSuffixToolStripMenuItem.Checked = !showInventoryQuantityAsSuffixToolStripMenuItem.Checked;
            TreeListViewRoles_SelectedIndexChanged(sender, e);
        }

        // TODO more crash handlers
        private void StagingViewerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Start the map viewer.
            //try
            //{
                if (StagingViewer == null)
                {
                    StagingViewer = new FormStagingViewer();
                    StagingViewer.Show();
                }
                else if (StagingViewer.IsDisposed)
                {
                    StagingViewer = new FormStagingViewer();
                    StagingViewer.Show();
                }
                else
                {
                    StagingViewer.Show();
                    StagingViewer.BringToFront();
                }
            //}
            //catch (Exception ex)
            //{
            //    ShowCrashMessageBox(ex);
            //}
        }

        private void DumpVehicleDebugStringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MotorpoolVehicle vic = ObjectListViewMotorpool.SelectedObject as MotorpoolVehicle;
            if (vic == null)
            {
                return;
            }
            Console.Clear();
            Console.WriteLine(vic.ToDebugString());
        }

        private void ShowCrashMessageBox(Exception ex)
        {
            MessageBox.Show(string.Format("Motorpool Tool tried to crash for this reason:\n\n{0}\n{1}\n\n" +
                "Pleae take a screenshot of this, or click this message and press CTRL+C to copy it in a text format.", ex.Message, ex.StackTrace), "oepsie woepsie!");
        }

        private void LoadSettings(object sender)
        {
            // Set Table View style
            enableSmallPanelMode = Settings.Default.SetTableView;
            SetTableView(enableSmallPanelMode);

            // Show Developer Vehicle Info
            EnableDEVModeToolStripMenuItem.Checked = Settings.Default.ShowDeveloperVehicleInfo;
            ToggleDeveloperInformation(Settings.Default.ShowDeveloperVehicleInfo);

            // Sets user perference for pylons ammo count being suffixed or prefixed.
            showVehicleAmmoAsSuffixToolStripMenuItem.Checked = Settings.Default.ShowPylonAmmoAsSuffix;

            // Sets user preference if inventory items should be grouped up from "Cable Ties Cable Ties" to "(x2) Cable Ties)"
            groupItemsInInventoryToolStripMenuItem.Checked = Settings.Default.GroupInventoryItems;

            // Sets user preference if inventory items should list their quantity after their name, e.g. "Cable Ties (x2)"
            showInventoryQuantityAsSuffixToolStripMenuItem.Checked = Settings.Default.ShowInventoryQuantityAsSuffix;

            // Restore last window size.
            ((Form)sender).Size = Settings.Default.WindowSize;

            // Restore if the window was maximized. Don't restore a minimized window state.
            ((Form)sender).WindowState = Settings.Default.WindowState == FormWindowState.Minimized ? FormWindowState.Normal : Settings.Default.WindowState;

            // Restore splitter position.
            SplitContainerMPT.SplitterDistance = Settings.Default.MotorpoolSplitterDistance;

            // Restore the users's font choice.
            TypeConverter converter = TypeDescriptor.GetConverter(ObjectListViewMotorpool.Font);
            Program.MPTFont = (Font)converter.ConvertFromInvariantString(Settings.Default.UserFont);
            Program.UpdateProgramFont(this, Program.MPTFont);
        }

        private void FormMotorpoolTool_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.SetTableView = enableSmallPanelMode;
            Settings.Default.ShowDeveloperVehicleInfo = EnableDEVModeToolStripMenuItem.Checked;
            Settings.Default.ShowPylonAmmoAsSuffix = showVehicleAmmoAsSuffixToolStripMenuItem.Checked;
            Settings.Default.ShowInventoryQuantityAsSuffix = showInventoryQuantityAsSuffixToolStripMenuItem.Checked;
            Settings.Default.GroupInventoryItems = groupItemsInInventoryToolStripMenuItem.Checked;
            Settings.Default.WindowSize = (sender as Form).Size;
            Settings.Default.MotorpoolSplitterDistance = SplitContainerMPT.SplitterDistance;
            Settings.Default.WindowState = (sender as Form).WindowState;

            TypeConverter converter = TypeDescriptor.GetConverter(ObjectListViewMotorpool.Font);
            Settings.Default.UserFont = converter.ConvertToInvariantString(ObjectListViewMotorpool.Font);
            Settings.Default.Save();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UpdateDatasetStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!FormUpdateDataset.IsOpen)
            {
                var updateDataset = new FormUpdateDataset();
                updateDataset.FormClosed += new FormClosedEventHandler(RefreshMotorpool);
                updateDataset.Show();
            }
        }

        private void RefreshMotorpool(object sender, FormClosedEventArgs e)
        {
            LoadDatasets();
        }


        private void TreeListViewRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (TreeListViewRoles.SelectedItem != null && TreeListViewRoles.SelectedItem.RowObject is Role)
                {
                    var selectedRole = TreeListViewRoles.SelectedItem.RowObject as Role;

                    if (TreeListViewSides.SelectedItem != null
                        && TreeListViewSides.SelectedItem.RowObject is Faction)
                    {
                        // debug text
                        var faction = TreeListViewSides.SelectedItem.RowObject as Faction;
                        var role = faction.Roles[selectedRole.Code.ToLowerInvariant()];

                        // GUI
                        TextBoxFactionGroup.Text = faction.Group.Name;
                        TextBoxFaction.Text = faction.Name;

                        if (role.Callsign != null)
                        {
                            TextBoxCallsign.Text = role.Callsign;
                            ToolTipInfo.SetToolTip(this.TextBoxCallsign, null);
                        }
                        else
                        {
                            ToolTipInfo.SetToolTip(this.TextBoxCallsign, string.Join("\n", role.Parent.Callsigns));
                            TextBoxCallsign.Text = role.Parent.Callsigns.First();
                        }

                        TextBoxElementRole.Text = string.Format($"{role.Parent.Name} {role.Name}");

                        TextBoxMap.Text = role.HasMap ? "Yes" : "No";
                        TextBoxCompass.Text = role.HasCompass ? "Yes" : "No";
                        TextBoxUAVTerminal.Text = role.HasUAVTerminal ? "Yes" : "No";
                        TextBoxGPS.Text = role.HasGPS ? "Yes" : "No";
                        TextBoxBFT.Text = role.HasBFT ? "Yes" : "No";
                        if (role.Wrist == 2)
                        {
                            TextBoxWatch.Text = "Altimeter Watch";
                        }
                        else if (role.Wrist == 1)
                        {
                            TextBoxWatch.Text = "Watch";
                        }
                        else
                        {
                            TextBoxWatch.Text = "None";
                        }

                        TextBoxMedic.Text = role.IsMedic > 0 ? "Yes" : "No";
                        TextBoxEngineer.Text = role.IsEngineer > 0 ? "Yes" : "No";
                        TextBoxEOD.Text = role.IsEOD > 0 ? "Yes" : "No";
                        TextBoxInterpreter.Text = role.Interpreter ? "Yes" : "No";

                        TextBoxStaminaFactor.Text = string.Format($"{role.StaminiaFactor:F2}");
                        TextBoxGForce.Text = string.Format($"{role.GForceCoef:F2}");

                        TextBoxPrimary.Text = role.Primary == null ? "None" : role.Primary.ToString();
                        TextBoxSecondary.Text = role.Secondary == null ? "None" : role.Secondary.ToString();
                        TextBoxTertiary.Text = role.Tertiary == null ? "None" : role.Tertiary.ToString();
                        TextBoxBinocular.Text = role.Binocular == null ? "None" : role.Binocular.ToString();

                        TextBoxHeadgear.Text = role.Headgear == null ? "None" : role.Headgear.ToString();
                        TextBoxUniform.Text = role.Uniform == null ? "None" : role.Uniform.ToString();
                        TextBoxVest.Text = role.Vest == null ? "None" : role.Vest.ToString();
                        TextBoxBackpack.Text = role.Backpack == null ? "None" : role.Backpack.ToString();

                        ListBoxUniform.Items.Clear();
                        ListBoxVest.Items.Clear();
                        ListBoxBackpack.Items.Clear();
                        if (role.Uniform != null)
                            GroupBoxUniformLoad.Text = string.Format($"Uniform: {role.Uniform.Inventory.Sum(x => x.Key.Mass)}/{ Vehicle.Lookup(role.Uniform.ContainerClass).MaxmiumLoad}");
                        else
                            GroupBoxUniformLoad.Text = "Uniform: None";

                        if (role.Vest != null)
                            GroupBoxVestLoad.Text = string.Format($"Vest: {role.Vest.Inventory.Sum(x => x.Key.Mass)}/{ Vehicle.Lookup(role.Vest.ContainerClass).MaxmiumLoad}");
                        else
                            GroupBoxVestLoad.Text = "Vest: None";

                        if (role.Backpack != null)
                            GroupBoxBackpackLoad.Text = string.Format($"Backpack: {role.Backpack.Inventory.Sum(x => x.Key.Mass)}/{ Vehicle.Lookup(role.Backpack.ClassName).MaxmiumLoad}");
                        else
                            GroupBoxBackpackLoad.Text = "Backpack: None";

                        //this.ToolTipInfo.SetToolTip(this.TextBoxUAVTerminal, "Used to remotely connect to unmanned vehicles.");

                        ListBoxInventoryHelper(ListBoxUniform, role.Uniform);
                        ListBoxInventoryHelper(ListBoxVest, role.Vest);
                        ListBoxInventoryHelper(ListBoxBackpack, role.Backpack);

                        // TODO Allowed Accesories Test
                        var scopes = new List<Accessory>();

                        // get all scopes that use the TOP RAIL and are ALLOWED for this role
                        //List<AccessoryGroup> groups = accesoryGroups.Where(acc => acc.Slot == AccessorySlot.SLOT_TOP && acc.Level <= role.AllowedAccessories.TopRail).ToList();
                        //foreach(var accGrp in groups)
                        //{
                        //    if(role.Primary == null) { break; }
                        //    List<Accessory> listOfFoo = accGrp.Accessories.Where(acc =>
                        //        acc.Year <= faction.Year
                        //        && acc.CompatibleClasses.Contains(role.Primary.ClassName, StringComparer.InvariantCultureIgnoreCase)).ToList();
                        //    scopes.AddRange(listOfFoo);
                        //}

                        //Console.Clear();
                        ListBoxPrimaryAccessories.Items.Clear();
                        ListBoxHeadgearAccessories.Items.Clear();

                        var optics = Accessory.GetCompatibleAccessories(accesoryGroups, AccessorySlot.SLOT_TOP, role.AllowedAccessories.SideRail, faction.Year, role.Primary);
                        foreach(var optic in optics)
                        {
                            ListBoxPrimaryAccessories.Items.Add(optic);
                            foreach (var acc in optic.Accessories)
                            {
                                acc.Indent = true;
                                ListBoxPrimaryAccessories.Items.Add(acc);
                            }
                        }

                        List<AccessoryGroup> rails = Accessory.GetCompatibleAccessories(accesoryGroups, AccessorySlot.SLOT_SIDE, role.AllowedAccessories.SideRail, faction.Year, role.Primary);
                        foreach(var rail in rails)
                        {
                            ListBoxPrimaryAccessories.Items.Add(rail);
                            foreach (var acc in rail.Accessories)
                            {
                                acc.Indent = true;
                                ListBoxPrimaryAccessories.Items.Add(acc);
                            }
                        }

                        List<AccessoryGroup> rests = Accessory.GetCompatibleAccessories(accesoryGroups, AccessorySlot.SLOT_BOTTOM, role.AllowedAccessories.BottomRail, faction.Year, role.Primary);
                        foreach (var rest in rests)
                        {
                            ListBoxPrimaryAccessories.Items.Add(rest);
                            foreach (var acc in rest.Accessories)
                            {
                                acc.Indent = true;
                                ListBoxPrimaryAccessories.Items.Add(acc);
                            }
                        }

                        List<AccessoryGroup> barrels = Accessory.GetCompatibleAccessories(accesoryGroups, AccessorySlot.SLOT_BARREL, role.AllowedAccessories.Barrel, faction.Year, role.Primary);
                        foreach (var barrel in barrels)
                        {
                            ListBoxPrimaryAccessories.Items.Add(barrel);
                            foreach (var acc in barrel.Accessories)
                            {
                                acc.Indent = true;
                                ListBoxPrimaryAccessories.Items.Add(acc);
                            }
                        }

                        List<AccessoryGroup> headmounts = Accessory.GetCompatibleAccessories(accesoryGroups, AccessorySlot.HEADMOUNT, AccessoryLevel.NIGHTVISION, faction.Year, role.Headgear);
                        foreach (var mount in headmounts)
                        {
                            ListBoxHeadgearAccessories.Items.Add(mount);
                            foreach (var acc in mount.Accessories)
                            {
                                acc.Indent = true;
                                ListBoxHeadgearAccessories.Items.Add(acc);
                            }
                        }


                        // TODO Better place for unit tests.
                        if(!Testing.RoleTests.WeaponHasCompatibleAmmo(role.Primary, role))
                        {
                            Console.WriteLine($"{role} is missing ammo for weapon {role.Parent.Callsigns.First()} {role.Primary} {role.Primary.ClassName}");
                        }

                        if (!Testing.RoleTests.WeaponHasCompatibleAmmo(role.Secondary, role))
                        {
                            Console.WriteLine($"{role} is missing ammo for weapon {role.Parent.Callsigns.First()} {role.Secondary} {role.Secondary.ClassName}");
                        }

                        if (!Testing.RoleTests.WeaponHasCompatibleAmmo(role.Tertiary, role))
                        {
                            Console.WriteLine($"{role} is missing ammo for weapon {role.Parent.Callsigns.First()} {role.Tertiary} {role.Tertiary.ClassName}");
                        }
                    }
                }
            }
            catch (AccessViolationException ex)
            {
                ShowCrashMessageBox(ex);
            }
        }

        private void ListBoxInventoryHelper(ListBox lb, ARMAEntity container)
        {
            if (container != null)
            {
                if (groupItemsInInventoryToolStripMenuItem.Checked)
                {
                    foreach (var item in container.Inventory)
                    {
                        if (!showInventoryQuantityAsSuffixToolStripMenuItem.Checked)
                        {
                            lb.Items.Add(string.Format($"(x{item.Value}) {item.Key}"));
                        }
                        else
                        {
                            lb.Items.Add(string.Format($"{item.Key} (x{item.Value})"));
                        }
                    }
                }
                else
                {
                    foreach (var kv in container.Inventory)
                    {
                        for (int i = 0; i < kv.Value; i++)
                        {
                            lb.Items.Add(string.Format($"{kv.Key}"));
                        }
                    }
                }
            }
        }

        private void ObjectListViewMotorpool_FormatRow(object sender, FormatRowEventArgs e)
        {
            MotorpoolVehicle vic = e.Model as MotorpoolVehicle;

            if (vic.IsCarrierAirWing)
                e.Item.ForeColor = Color.Blue;
            else if (vic.IsSpecialForces)
                e.Item.ForeColor = Color.Green;
            else if (vic.IsRecon)
                e.Item.ForeColor = Color.Purple;
        }

        private void ObjectListViewMotorpool_BeforeCreatingGroups(object sender, CreateGroupsEventArgs e)
        {
            e.Parameters.PrimarySortOrder = SortOrder.Ascending;
        }

        private void ChangeFontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog f = new FontDialog();

            if (f.ShowDialog() != DialogResult.Cancel)
            {
                Program.MPTFont = f.Font;
                RefreshObjectListViews(ObjectListViewMotorpool, Program.MPTFont);
                TreeListViewSides.Font = Program.MPTFont;
                TreeListViewRoles.Font = Program.MPTFont;

                Program.UpdateProgramFont(this, Program.MPTFont);
                if (StagingViewer != null)
                {
                    Program.UpdateProgramFont(StagingViewer, Program.MPTFont);
                }
            }
        }



        private void RefreshObjectListViews(ObjectListView olv, Font font)
        {
            olv.Font = font;
            olv.AutoResizeColumns();
            foreach (OLVListItem i in olv.Items)
            {
                olv.RefreshItem(i);
            }
        }

        private void TextBoxSearch_KeyUp(object sender, KeyEventArgs e)
        {
            SetTreeListViewRolesFilter(TextBoxSearch.Text);
        }

        private void SetTreeListViewRolesFilter(string text)
        {
            this.TreeListViewRoles.ModelFilter = null;
            foreach (var element in TreeListViewRoles.Roots)
            {
                if (!((Element)element).IsHidden)
                {
                    try
                    {
                        TreeListViewRoles.Expand(element);
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        // Shouldn't crash because the ModelFilter is set to null before trying to expand.
                    }
                }
            }
            this.ObjectListViewMotorpool.ModelFilter = new ModelFilter(delegate (object x)
            {
                if (text.Length == 0 || text.Equals("Search..."))
                {
                    return true;
                }
                var vic = x as MotorpoolVehicle;
                return vic.ToString().ToLowerInvariant().Contains(text.ToLowerInvariant());
            });

            this.TreeListViewRoles.ModelFilter = new ModelFilter(delegate (object x)
            {
                var hide = x as Hideable;
                if (hide.IsHidden)
                {

                    return false;
                }

                if (text.Length == 0 || text.Equals("Search..."))
                {
                    return true;
                }
                var role = x as Role;
                if (role == null) { return true; }


                var search = text.ToLowerInvariant();

                bool callsignMatch = false;
                foreach (string s in role.Parent.Callsigns)
                {
                    if (s.ToLowerInvariant().Contains(search))
                    {
                        callsignMatch = true;
                        break;
                    }
                }

                if (role.Callsign != null && role.Callsign.ToLowerInvariant().Contains(search))
                {
                    callsignMatch = true;
                }

                return role.Name.ToLowerInvariant().Contains(search)
                || role.Parent.Name.ToLowerInvariant().Contains(search)
                || role.Code.ToLowerInvariant().Contains(search)
                || callsignMatch
                ;
            });
            TreeListViewRoles.SelectedIndex = 1;
        }

        private void SetTreeListViewResupplyFilter(Faction faction)
        {
            TreeListViewResupply.ModelFilter = null;

            this.TreeListViewResupply.ModelFilter = new ModelFilter(delegate (object x)
            {
                if (x is SupplyGroup)
                {
                    var supplyGroup = x as SupplyGroup;
                    if(supplyGroup.Supplies.Any(x => faction.Resupply.Contains(x.ClassName, StringComparer.OrdinalIgnoreCase)))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (x is Supply)
                {
                    var supply = x as Supply;
                    if (faction.Resupply.Contains(supply.ClassName, StringComparer.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
                
            });
        }

        private void TextBoxSearch_Enter(object sender, EventArgs e)
        {
            if (TextBoxSearch.Text.Equals("Search..."))
            {
                TextBoxSearch.Text = string.Empty;
                TextBoxSearch.ForeColor = Color.Black;
            }
        }

        private void TextBoxSearch_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxSearch.Text))
            {
                TextBoxSearch.Text = "Search...";
                TextBoxSearch.ForeColor = Color.Gray;
            }
        }

        private void ButtonToggleView_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            enableSmallPanelMode = !enableSmallPanelMode;
            SetTableView(enableSmallPanelMode);
        }

        private void SetTableView(bool smallPanelMode)
        {
            ButtonToggleView.Text = enableSmallPanelMode ? "Expand Table View" : "Collapse Table View";

            // Set IsVisible for all columns. 'Restriction' and 'Display Name' columns are not hidable.
            foreach (var column in ObjectListViewMotorpool.AllColumns)
            {
                if (column.Hideable)
                {
                    column.IsVisible = !smallPanelMode;
                }
            }

            // Respect 'Dev Mode' check when view is expanded..
            ToggleDeveloperInformation(EnableDEVModeToolStripMenuItem.Checked);

            int width = ObjectListViewMotorpool.AllColumns.Where(o => o.IsVisible).Sum(o => o.Width);
            if (smallPanelMode)
            {
                this.Size = new Size(630 + width, this.Size.Height);
            }
            else
            {
                this.Size = new Size(830 + width, this.Size.Height);
            }

            Console.WriteLine(ObjectListViewMotorpool.AllColumns.Where(o => o.IsVisible).Sum(o => o.Width));
        }

        private void FormMotorpoolTool_SizeChanged(object sender, EventArgs e)
        {
            ObjectListViewMotorpool.RebuildColumns();
        }
    }
}
