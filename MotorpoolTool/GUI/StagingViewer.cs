﻿using MotorpoolTool.ARMA;
using MotorpoolTool.BWI.Stagings;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using static MotorpoolTool.BWI.Stagings.Marker;
using Color = SFML.Graphics.Color;
using Font = SFML.Graphics.Font;
using View = SFML.Graphics.View;

namespace MotorpoolTool.GUI
{
    public partial class FormStagingViewer : Form
    {
        // For terrain rendering / prep.
        private const int TILE_SIZE = 2048;
        private static List<Texture> terrainTextures;
        private static List<Sprite> terrainTiles;
        private static Texture stagingTexture;
        private static Font stagingViewerFont;
        private static List<Marker> stagingMarkers;
        private static MapSurface map;

        // Handles rendering and relative scaling.
        private static RenderWindow renderWindow;
        private static View terrainViewport;
        private static View markerViewport;
        private static View overlayViewport;
        private static Stopwatch stopwatch;

        // Handles scaling math for terrains.
        private static World world;
        private static Vector2i terrainImageSize;
        private static Vector2f lastResize;

        // Marker size variables.
        private static float mapMarkerScale = 0.25f;

        // Camera zoom variables.
        private static float mainViewportZoom = 1.0f;
        private const float ZOOM_LERP = 0.2f;

        // Camera panning variables.
        private static Vector2f oldPos;
        private static bool isPanning = false;

        // Staging ownership variables.
        private Marker blufor_primary = null;
        private Marker blufor_secondary = null;
        private Marker opfor_primary = null;
        private Marker opfor_secondary = null;
        private Marker indep_primary = null;
        private Marker indep_secondary = null;

        // Construction constants.
        public const float CONSTRUCTION_TIER_1 = 1000f;
        public const float CONSTRUCTION_TIER_2 = 2000f;
        public const float CONSTRUCTION_TIER_3 = 3000f;

        // Debug
        private static Text GUIText;

        public FormStagingViewer()
        {
            InitializeComponent();
        }

        private void StagingViewer_Load(object sender, EventArgs e)
        {
            terrainTextures = new List<Texture>();
            terrainTiles = new List<Sprite>();
            stagingTexture = new Texture("markers\\staging.png") { Smooth = true };
            stagingViewerFont = new Font("fonts/lucon.ttf");
            GUIText = new Text(string.Empty, stagingViewerFont) { CharacterSize = 16, FillColor = Color.Black };
            stagingMarkers = new List<Marker>();
            stopwatch = new Stopwatch();

            map = new MapSurface();
            map.Size = PanelPlaceholder.Size;
            map.Location = PanelPlaceholder.Location;
            map.Anchor = PanelPlaceholder.Anchor;

            renderWindow = new RenderWindow(map.Handle);
            //renderWindow.SetFramerateLimit(60);
            renderWindow.MouseButtonPressed += new EventHandler<MouseButtonEventArgs>(MapViewerMousePressed);
            renderWindow.MouseButtonReleased += new EventHandler<MouseButtonEventArgs>(MapViewerMouseReleased);
            renderWindow.MouseMoved += new EventHandler<MouseMoveEventArgs>(MapViewerMouseMoved);
            renderWindow.MouseWheelScrolled += new EventHandler<MouseWheelScrollEventArgs>(MapViewerMouseScrolled);
            renderWindow.Resized += new EventHandler<SizeEventArgs>(MapViewerResized);
            renderWindow.KeyPressed += new EventHandler<SFML.Window.KeyEventArgs>(MapKeyPressed);

            terrainViewport = new View(new Vector2f(PanelPlaceholder.Size.Width / 2, PanelPlaceholder.Size.Height / 2), new Vector2f(PanelPlaceholder.Width, PanelPlaceholder.Height));
            lastResize = terrainViewport.Size;
            markerViewport = new View(terrainViewport);
            overlayViewport = new View(terrainViewport);

            this.Controls.Add(map);

            PopulateMapSelection(ComboxMapSelect);
            ComboxBLUFORPrimary.SelectedIndexChanged += (sender, e) => SetStagingSide(sender, ref blufor_primary, ref LabelBLUFORSpawns);
            ComboxBLUFORSecondary.SelectedIndexChanged += (sender, e) => SetStagingSide(sender, ref blufor_secondary, ref LabelBLUFORSpawns);

            ComboxOPFORPrimary.SelectedIndexChanged += (sender, e) => SetStagingSide(sender, ref opfor_primary, ref LabelOPFORSpawns);
            ComboxOPFORSecondary.SelectedIndexChanged += (sender, e) => SetStagingSide(sender, ref opfor_secondary, ref LabelOPFORSpawns);

            ComboxINDEPPrimary.SelectedIndexChanged += (sender, e) => SetStagingSide(sender, ref indep_primary, ref LabelINDEPSpawns);
            ComboxINDEPSecondary.SelectedIndexChanged += (sender, e) => SetStagingSide(sender, ref indep_secondary, ref LabelINDEPSpawns);

            // Disable scrollwheel on combo boxes, as it's too easy to accidentally mess them up during regular scrolling.
            ComboxMapSelect.MouseWheel += new MouseEventHandler(ComboxDisableMouseWheel);
            ComboxBLUFORPrimary.MouseWheel += new MouseEventHandler(ComboxDisableMouseWheel);
            ComboxBLUFORSecondary.MouseWheel += new MouseEventHandler(ComboxDisableMouseWheel);
            ComboxOPFORPrimary.MouseWheel += new MouseEventHandler(ComboxDisableMouseWheel);
            ComboxOPFORSecondary.MouseWheel += new MouseEventHandler(ComboxDisableMouseWheel);
            ComboxINDEPPrimary.MouseWheel += new MouseEventHandler(ComboxDisableMouseWheel);
            ComboxINDEPSecondary.MouseWheel += new MouseEventHandler(ComboxDisableMouseWheel);


            Program.UpdateProgramFont(this, Program.MPTFont);
            Stopwatch.StartNew();
        }

        internal class MapSurface : System.Windows.Forms.Control
        {
            protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
            {
                //base.OnPaint(e);
            }
            protected override void OnPaintBackground(PaintEventArgs pevent)
            {
                //base.OnPaintBackground(pevent);
            }
        }

        private void ComboxDisableMouseWheel(object sender, MouseEventArgs e)
        {
            ((HandledMouseEventArgs)e).Handled = true;
        }

        private void PopulateMapSelection(ComboBox combox)
        {
            combox.Items.Clear();
            foreach (var item in World.WorldDictionary)
            {
                if (Staging.StagingListDictionary.ContainsKey(item.Key))
                {
                    combox.Items.Add(item.Value);
                }
            }
        }

        private void PopulateStagingSelection(ComboBox combox)
        {
            combox.Items.Clear();
            combox.Items.Add(new Staging("none", "None", new Vector2f(0, 0), "", new string[] { }, new string[] { }, new int[] { }, new string[] { }));
            combox.SelectedIndex = 0;
            foreach (var item in Staging.StagingListDictionary[world.ClassName.ToLowerInvariant()])
            {
                combox.Items.Add(item);
            }
        }

        private static void SetStagingSide(object sender, ref Marker marker, ref Label label)
        {
            var combox = sender as ComboBox;
            var staging = combox.SelectedItem as Staging;

            if (staging == null) { return; }
            else if (staging.ClassName.Equals("none"))
            {
                label.Text = string.Empty;
                marker = null;
                return;
            }

            // Set marker
            foreach (var stagingMarker in stagingMarkers)
            {
                if (stagingMarker.Staging.Equals(staging))
                {
                    marker = stagingMarker;
                    marker.Changed = true;
                    break;
                }
            }
        }

        private void FormStagingViewer_Shown(object sender, EventArgs e)
        {
            while (this.Visible)
            {
                // Update
                Application.DoEvents();
                renderWindow.DispatchEvents();
                renderWindow.Clear(Color.White);

                if (backgroundWorker1.IsBusy)
                {
                    continue;
                }

                if (world != null)
                {

                    GUIText.DisplayedString =
                        $"Size: {terrainImageSize.X * (1 / GetWorldScale()) / 1000:F2}km" +
                        $" x {terrainImageSize.Y * (1 / GetWorldScale()) / 1000:F2}km" +
                        $"\nZoom: {Math.Min(50, GetZoom()):F2}x";

#if DEBUG
                    GUIText.DisplayedString += $"\nterrainViewport X:{terrainViewport.Size.X} Y:{terrainViewport.Size.Y}";
                    GUIText.DisplayedString += $"\nFrame time: {stopwatch.ElapsedMilliseconds}ms";
                    GUIText.DisplayedString += $"\n{stagingMarkers.Count}";
#endif
                }



                stopwatch.Restart();
                UpdateMarkers();


                renderWindow.SetView(terrainViewport);
                DrawMap(renderWindow);
                DrawConstructionExclusionRadiuses(renderWindow);

                renderWindow.SetView(markerViewport);
                DrawMarkers(renderWindow);

                renderWindow.SetView(overlayViewport);

                renderWindow.Draw(GUIText);


                // Display
                renderWindow.Display();
            }

            // Closing
            backgroundWorker1.CancelAsync();
            DisposeTiles();
            renderWindow.Dispose();
            terrainViewport.Dispose();
        }

        private static float GetZoom()
        {
            return lastResize.X / terrainViewport.Size.X;
        }

        private void DisposeTiles()
        {
            foreach (var m in stagingMarkers)
            {
                m.Dispose();
            }
            foreach (Sprite s in terrainTiles)
            {
                s.Dispose();
            }
            foreach (Texture t in terrainTextures)
            {
                t.Dispose();
            }
            stagingMarkers.Clear();
            terrainTiles.Clear();
            terrainTextures.Clear();
        }

        private void DrawMap(RenderWindow renderWindow)
        {
            foreach (Sprite s in terrainTiles)
            {
                renderWindow.Draw(s);
            }
        }

        private void UpdateMarkers()
        {
            foreach (var marker in stagingMarkers)
            {
                if (marker.Equals(blufor_primary) || marker.Equals(blufor_secondary))
                {
                    marker.Color = COLOR_BLUFOR;
                    if (marker.Changed || LabelBLUFORSpawns.Text.Length == 0)
                    {
                        LabelBLUFORSpawns.Text = string.Empty;
                        MarkerLabelHelper(ref blufor_primary, ref LabelBLUFORSpawns);
                        MarkerLabelHelper(ref blufor_secondary, ref LabelBLUFORSpawns);
                    }

                }

                if (marker.Equals(opfor_primary) || marker.Equals(opfor_secondary))
                {
                    marker.Color = COLOR_OPFOR;
                    if (marker.Changed || LabelOPFORSpawns.Text.Length == 0)
                    {
                        LabelOPFORSpawns.Text = string.Empty;
                        MarkerLabelHelper(ref opfor_primary, ref LabelOPFORSpawns);
                        MarkerLabelHelper(ref opfor_secondary, ref LabelOPFORSpawns);
                    }
                }

                if (marker.Equals(indep_primary) || marker.Equals(indep_secondary))
                {
                    marker.Color = COLOR_INDEP;
                    if (marker.Changed || LabelINDEPSpawns.Text.Length == 0)
                    {
                        LabelINDEPSpawns.Text = string.Empty;
                        MarkerLabelHelper(ref indep_primary, ref LabelINDEPSpawns);
                        MarkerLabelHelper(ref indep_secondary, ref LabelINDEPSpawns);
                    }
                }

                if (!marker.Equals(blufor_primary) && !marker.Equals(blufor_secondary)
                    && !marker.Equals(opfor_primary) && !marker.Equals(opfor_secondary)
                    && !marker.Equals(indep_primary) && !marker.Equals(indep_secondary))
                {
                    marker.Color = COLOR_CIVIL;
                }

                marker.Scale = new Vector2f(mapMarkerScale, mapMarkerScale);
                var windowCoords = renderWindow.MapCoordsToPixel(marker.OffsetRenderPosition, terrainViewport);
                var markerCoords = renderWindow.MapPixelToCoords(windowCoords, markerViewport);

                var zoomAdjustedCoords = new Vector2f(markerCoords.X - (marker.Sprite.Texture.Size.X * mapMarkerScale / 2), markerCoords.Y - (marker.Sprite.Texture.Size.Y) * mapMarkerScale / 2);
                marker.Position = zoomAdjustedCoords;
            }
        }

        private static void MarkerLabelHelper(ref Marker marker, ref Label label)
        {
            if (marker == null) { return; }
            if (label.Text.Length > 0)
            {
                label.Text += "\n\n";
            }
            label.Text += marker.Staging.DisplayName + "\n";
            label.Text += string.Join("\n", marker.Staging.MotorpoolSpawns);
            marker.Changed = false;
        }

        private void DrawMarkers(RenderWindow renderWindow)
        {
            foreach (var marker in stagingMarkers)
            {
                if (CheckBoxShowAllStagings.Checked)
                {
                    renderWindow.Draw(marker);
                }
                else
                {
                    if (marker.Equals(blufor_primary)
                        || marker.Equals(blufor_secondary)
                        || marker.Equals(opfor_primary)
                        || marker.Equals(opfor_secondary)
                        || marker.Equals(indep_primary)
                        || marker.Equals(indep_secondary))
                    {
                        renderWindow.Draw(marker);
                    }
                }
            }

        }

        private void DrawConstructionExclusionRadiuses(RenderWindow renderWindow)
        {
            if (world == null) { return; }

            /* radius * GetWorldSize to correct that some maps are not 1:1 for meters:pixel
             * Draw circles in reverse order for aesthetic reasons (looks nicer if largest circle is on the bottom
             */
            if (CheckBoxConstructionTier3.Checked)
            {
                DrawConstructionExclusionRadius(renderWindow, CONSTRUCTION_TIER_3 * GetWorldScale());
            }

            if (CheckBoxConstructionTier2.Checked)
            {
                DrawConstructionExclusionRadius(renderWindow, CONSTRUCTION_TIER_2 * GetWorldScale());
            }

            if (CheckBoxConstructionTier1.Checked)
            {
                DrawConstructionExclusionRadius(renderWindow, CONSTRUCTION_TIER_1 * GetWorldScale());
            }
        }

        private void DrawConstructionExclusionRadius(RenderWindow renderWindow, float radius)
        {
            if (blufor_primary != null)
            {
                DrawCircle(renderWindow, COLOR_BLUFOR_TRANS, radius, SolveStagingPosition(blufor_primary.Staging.Position));
            }
            if (blufor_secondary != null)
            {
                DrawCircle(renderWindow, COLOR_BLUFOR_TRANS, radius, SolveStagingPosition(blufor_secondary.Staging.Position));
            }

            if (opfor_primary != null)
            {
                DrawCircle(renderWindow, COLOR_OPFOR_TRANS, radius, SolveStagingPosition(opfor_primary.Staging.Position));
            }
            if (opfor_secondary != null)
            {
                DrawCircle(renderWindow, COLOR_OPFOR_TRANS, radius, SolveStagingPosition(opfor_secondary.Staging.Position));
            }

            if (indep_primary != null)
            {
                DrawCircle(renderWindow, COLOR_INDEP_TRANS, radius, SolveStagingPosition(indep_primary.Staging.Position));
            }
            if (indep_secondary != null)
            {
                DrawCircle(renderWindow, COLOR_INDEP_TRANS, radius, SolveStagingPosition(indep_secondary.Staging.Position));
            }
        }

        private void DrawCircle(RenderWindow renderWindow, Color color, float radius, Vector2f position)
        {
            // Center position on 
            position -= new Vector2f(radius, radius);
            var circle = new CircleShape() { FillColor = color, Radius = radius, Position = position };
            renderWindow.Draw(circle);
            circle.Dispose();
        }

        private Vector2f SolveStagingPosition(Vector2f armaPosition)
        {
            // ArmA Meters * GetWorldScale() = Pixel coordinates
            float x = armaPosition.X * GetWorldScale();

            /* Steps:
             * Multiple worldSize.Y by the inverse of GetWorldScale().
             * This gives us the worldSize in meters.
             * 
             * Subtract the ArmA position (which is in meters).
             * 
             * Multiple the final result by GetWorldScale().
             * This gives us the pixel coordinates we need for Y.
             */
            float y = (terrainImageSize.Y * (1 / GetWorldScale()) - armaPosition.Y) * GetWorldScale();

            return new Vector2f(x, y);
        }

        private static void MapViewerResized(object sender, SizeEventArgs e)
        {
            // TODO better math for less view snapping
            var resizePos = terrainViewport;
            lastResize = new Vector2f(e.Width, e.Height);
            terrainViewport = new View(resizePos.Center, new Vector2f(e.Width, e.Height));
            markerViewport = new View(terrainViewport);

            overlayViewport = new View(new Vector2f(e.Width / 2, e.Height / 2), new Vector2f(e.Width, e.Height));
        }

        private static void MapViewerMouseMoved(object sender, MouseMoveEventArgs e)
        {
            if (isPanning)
            {
                Vector2f newPos = renderWindow.MapPixelToCoords(new Vector2i(e.X, e.Y), terrainViewport);
                Vector2f deltaPos = oldPos - newPos;

                terrainViewport.Center = terrainViewport.Center += deltaPos;
                markerViewport.Center = terrainViewport.Center;
            }
        }

        private static void MapViewerMouseScrolled(object sender, MouseWheelScrollEventArgs e)
        {
            //Console.WriteLine($"Mousewheel delta {e.Delta}");
            if (!isPanning)
            {
                if (e.Delta > 0 && GetZoom() < 50)
                {
                    mainViewportZoom -= ZOOM_LERP;
                }
                else if (e.Delta < 0 && GetZoom() > 0.01)
                {
                    mainViewportZoom += ZOOM_LERP;
                }

                terrainViewport.Zoom(mainViewportZoom);

                mainViewportZoom = 1f;
            }
        }

        private static void MapViewerMousePressed(object sender, MouseButtonEventArgs e)
        {
            isPanning = true;
            oldPos = renderWindow.MapPixelToCoords(new Vector2i(e.X, e.Y), terrainViewport);

            /* Important: Some computers don't allow scrollwheel input when not focused. */
            map.Focus();
        }

        private static void MapViewerMouseReleased(object sender, MouseButtonEventArgs e)
        {
            isPanning = false;
        }

        private static void MapKeyPressed(object sender, SFML.Window.KeyEventArgs e)
        {
            switch (e.Code)
            {
                case Keyboard.Key.Add:
                    if (GetZoom() < 50)
                        mainViewportZoom -= ZOOM_LERP;
                    break;
                case Keyboard.Key.Subtract:
                    if (GetZoom() > 0.01)
                        mainViewportZoom += ZOOM_LERP;
                    break;
            }

            terrainViewport.Zoom(mainViewportZoom);
            mainViewportZoom = 1f;
        }

        private void FormatTerrainImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = @"terrains\";
                openFileDialog.Filter = "Terrain .png|*.png|All files (*.*)|*.*";
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    Dictionary<string, Bitmap> tiles = new Dictionary<string, Bitmap>();
                    Bitmap map = new Bitmap(openFileDialog.FileName);

                    Console.Write("Formatting Terrain Image...\n[");
                    for (int y = 0; y < map.Height; y += TILE_SIZE)
                    {
                        for (int x = 0; x < map.Width; x += TILE_SIZE)
                        {
                            // Image might not cleanly divide by 512
                            int tileWidth = Math.Min(map.Width - x, TILE_SIZE);
                            int tileHeight = Math.Min(map.Height - y, TILE_SIZE);

                            Rectangle rtc = new Rectangle(x, y, tileWidth, tileHeight);
                            Bitmap target = new Bitmap(tileWidth, tileHeight);

                            using (Graphics graphics = Graphics.FromImage(target))
                            {
                                graphics.DrawImage(map, new Rectangle(0, 0, tileWidth, tileHeight), rtc, GraphicsUnit.Pixel);
                            }
                            tiles.Add(string.Format($"{x}_{y}", Program.MPTCulture), target);
                            //Console.WriteLine($"x: {x} y: {y}");
                        }
                        Console.Write($"|");
                    }

                    string terrainName = openFileDialog.FileName.Split('.')[0];

                    Console.WriteLine($"] Tiles Made: {tiles.Count}");
                    if (!Directory.Exists(terrainName))
                    {
                        Directory.CreateDirectory(terrainName);
                    }

                    Console.Write("Saving tiles... ");
                    foreach (var kv in tiles)
                    {
                        kv.Value.Save(string.Format($"{terrainName}\\{kv.Key}.png", Program.MPTCulture));
                        kv.Value.Dispose();
                    }

                    string worldTextFile = string.Format($"{terrainName}\\{Path.GetFileName(openFileDialog.FileName).Split('.')[0]}.txt", Program.MPTCulture);
                    string worldTextContents = string.Format($"{map.Width}\n{map.Height}", Program.MPTCulture);
                    File.WriteAllText(worldTextFile, worldTextContents);

                    map.Dispose();
                    Console.WriteLine("done!");
                }
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void FormStagingViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
        }

        private void ComboBoxMapSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            world = ComboxMapSelect.SelectedItem as World;// Populate BLUFOR / OPFOR / INDEP staging selection

            // Reset camera to the center of the terrain.

            PopulateStagingSelection(ComboxBLUFORPrimary);
            PopulateStagingSelection(ComboxBLUFORSecondary);

            PopulateStagingSelection(ComboxOPFORPrimary);
            PopulateStagingSelection(ComboxOPFORSecondary);

            PopulateStagingSelection(ComboxINDEPPrimary);
            PopulateStagingSelection(ComboxINDEPSecondary);

            ComboxMapSelect.Enabled = false;
            backgroundWorker1.RunWorkerAsync();
        }

        private static float GetWorldScale()
        {
            return (Math.Min(terrainImageSize.X, terrainImageSize.Y) / world.Size);
        }

        private void CheckBoxRotateLabels_CheckedChanged(object sender, EventArgs e)
        {
            foreach (var marker in stagingMarkers)
            {
                marker.Rotation = CheckBoxRotateLabels.Checked ? -30f : 0;
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            blufor_primary = null;
            blufor_secondary = null;
            opfor_primary = null;
            opfor_secondary = null;
            indep_primary = null;
            indep_secondary = null;

            int maxSize = (int)SFML.Graphics.Texture.MaximumSize;
            string directoryPath = string.Format($"terrains\\{world.ClassName}", Program.MPTCulture);
            string worldTextFile = string.Format($"{directoryPath}\\{world.ClassName}.txt", Program.MPTCulture);

            if (!Directory.Exists(directoryPath))
            {
                Console.WriteLine($"{directoryPath} Directory not found.");
                MessageBox.Show("You're missing the optional terrains folder. Staging Viewer won't without the folder.\n\n" +
                    "Download the terrains folder and put it in the same directory as MotorpoolTool.exe");
                return;
            }

            if (!File.Exists(worldTextFile))
            {
                Console.WriteLine($"{worldTextFile} Text file not found.");
                return;
            }

            var worldData = File.ReadAllLines(worldTextFile);

            terrainImageSize = new Vector2i(int.Parse(worldData[0]), int.Parse(worldData[1]));

            if (world.Size == -1) // BIS_fnc_mapSize didn't figure out the map size
            {
                world.Size = Math.Min(terrainImageSize.X, terrainImageSize.Y);
            }
            //world.Size = Math.Max(worldSize.X, world.Size); 
            //world.Size = worldSize.X;
            var allImages = Directory.GetFiles(directoryPath, "*.png");
            DisposeTiles();

            float progress = 0;
            foreach (string image in allImages)
            {
                if (worker.CancellationPending)
                {
                    return;
                }
                Texture t = new Texture(image);

                // Prevents grainy images, costs more RAM.
                t.GenerateMipmap();

                var coords = image.Substring(image.LastIndexOf("\\") + 1).Split('.')[0].Split('_');
                Sprite s = new Sprite(t)
                {
                    Position = new Vector2f(float.Parse(coords[0], NumberStyles.Float, Program.MPTCulture), float.Parse(coords[1], NumberStyles.Float, Program.MPTCulture))
                };
                terrainTextures.Add(t);
                terrainTiles.Add(s);
                progress++;
                worker.ReportProgress((int)((progress / allImages.Length) * 100));
            }


            /* The image size of the map in pixels is often 1:1, but there are exceptions (e.g. Altis). 
             * worldScale is the factor to scale marker positions by to the map.
             */
            foreach (var staging in Staging.StagingListDictionary[world.ClassName.ToLowerInvariant()])
            {
                var solveMarkerPosition = SolveStagingPosition(staging.Position);

                Sprite sprite = new Sprite(stagingTexture)
                {
                    Rotation = CheckBoxRotateLabels.Checked ? -30f : 0
                };
                stagingMarkers.Add(new Marker(staging, sprite, stagingViewerFont, staging.DisplayName) { Color = COLOR_CIVIL, OffsetRenderPosition = solveMarkerPosition });
            }

            terrainViewport.Center = new Vector2f(world.Size * GetWorldScale() / 2, world.Size * GetWorldScale() / 2);

            // Thread safe enabling of map select.
            SetEnabled(this, ComboxMapSelect, true);
        }

        // TODO Move to a generic threadsafe helper class
        delegate void SetEnabledCallback(Form f, Control ctrl, bool enabled);
        public static void SetEnabled(Form f, Control ctrl, bool enabled)
        {
            if (ctrl.InvokeRequired)
            {
                SetEnabledCallback d = new SetEnabledCallback(SetEnabled);
                f.Invoke(d, new object[] { f, ctrl, enabled });
            }
            else
            {
                ctrl.Enabled = enabled;
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            ProgressBarTerrain.Value = e.ProgressPercentage;
        }
    }
}
