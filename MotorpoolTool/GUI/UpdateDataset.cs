﻿using MotorpoolTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MotorpoolTool.GUI
{
    public partial class FormUpdateDataset : Form
    {
        private const string CFG_MAGAZINES_SCRIPT = @"scripts/UpdateCfgMagazines.sqf";
        private const string CFG_MAGAZINEWELLS_SCRIPT = @"scripts/UpdateCfgMagazineWells.sqf";
        private const string CFG_WEAPONS_SCRIPT = @"scripts/UpdateCfgWeapons.sqf";
        private const string CFG_VEHICLES_SCRIPT = @"scripts/UpdateCfgVehicles.sqf";

        private const string TEXT_WAITING = "Waiting for script...";
        private const string TEXT_IDLE = "Dataset idle.";
        private const string TEXT_COMPLETE = "Dataset updated!";

        private const short NONE = -1;
        private const short CFG_WEAPONS = 0;
        private const short CFG_MAGAZINES = 1;
        private const short CFG_MAGAZINEWELLS = 2;
        private const short CFG_VEHICLES = 3;

        public static bool IsOpen { get; set; }

        private short expectedDatasetToUpdate = NONE;

        public FormUpdateDataset()
        {
            InitializeComponent();
        }

        private void FileToClipboard(string filePath)
        {
            if(!File.Exists(filePath))
            {
                Console.WriteLine("File {0} not found.", filePath);
                MessageBox.Show("Something went wrong trying to load this script. Send Andrew a screenshot of this: " + filePath);
            }
            else
            {
                Clipboard.SetText(File.ReadAllText(filePath));
            }
        }

        private void ResetLabels()
        {
            ResetLabel(LabelUpdateCfgWeapons);
            ResetLabel(LabelUpdateCfgMagazines);
            ResetLabel(LabelUpdateCfgMagazineWells);
            ResetLabel(LabelUpdateCfgVehicles);
        }

        private void ResetLabel(Label label)
        {
            if(label.ForeColor == Color.Green)
            {
                return;
            }
            label.ForeColor = Color.Black;
            label.Text = TEXT_IDLE;
        }

        private void ButtonUpdateCfgWeapons_Click(object sender, EventArgs e)
        {
            FileToClipboard(CFG_WEAPONS_SCRIPT);
            ResetLabels();
            SetLabelToWait(LabelUpdateCfgWeapons);

            expectedDatasetToUpdate = CFG_WEAPONS;
            ScriptTimer.Start();
        }

        private void ButtonUpdateCfgMagazines_Click(object sender, EventArgs e)
        {
            FileToClipboard(CFG_MAGAZINES_SCRIPT);
            ResetLabels();
            SetLabelToWait(LabelUpdateCfgMagazines);

            expectedDatasetToUpdate = CFG_MAGAZINES;
            ScriptTimer.Start();
        }

        private void ButtonUpdateCfgMagazineWells_Click(object sender, EventArgs e)
        {
            FileToClipboard(CFG_MAGAZINEWELLS_SCRIPT);
            ResetLabels();
            SetLabelToWait(LabelUpdateCfgMagazineWells);

            expectedDatasetToUpdate = CFG_MAGAZINEWELLS;
            ScriptTimer.Start();
        }

        private void ButtonUpdateCfgVehicles_Click(object sender, EventArgs e)
        {
            FileToClipboard(CFG_VEHICLES_SCRIPT);
            ResetLabels();
            SetLabelToWait(LabelUpdateCfgVehicles);

            expectedDatasetToUpdate = CFG_VEHICLES;
            ScriptTimer.Start();
        }

        private void SetLabelToWait(Label label)
        {
            label.Text = TEXT_WAITING;
            label.ForeColor = Color.Red;
        }

        private void SetLabelToComplete(Label label)
        {
            label.Text = TEXT_COMPLETE;
            label.ForeColor = Color.Green;
        }

        private bool IsDataset(string data)
        {
            // Fresh dataset from scripts will always start with an opening bracket.
            // TODO replace this with a better marker.
            if(data[0].Equals('['))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void WriteDataset(string filePath, string data)
        {
            // Cleanup done
            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(data);
            writer.Close();
        }

        private void ScriptTimer_Tick(object sender, EventArgs e)
        {
            if(!IsDataset(Clipboard.GetText()))
            {
                return;
            }

            switch(expectedDatasetToUpdate)
            {
                case CFG_WEAPONS:
                    WriteDataset(FormMotorpoolTool.CFG_WEAPONS_PATH, Clipboard.GetText());
                    SetLabelToComplete(LabelUpdateCfgWeapons);

                    expectedDatasetToUpdate = NONE;
                    ScriptTimer.Stop();
                    break;
                case CFG_MAGAZINES:
                    WriteDataset(FormMotorpoolTool.CFG_MAGAZINES_PATH, Clipboard.GetText());
                    SetLabelToComplete(LabelUpdateCfgMagazines);

                    expectedDatasetToUpdate = NONE;
                    ScriptTimer.Stop();
                    break;
                case CFG_MAGAZINEWELLS:
                    WriteDataset(FormMotorpoolTool.CFG_MAGAZINE_WELLS_PATH, Clipboard.GetText());
                    SetLabelToComplete(LabelUpdateCfgMagazineWells);

                    expectedDatasetToUpdate = NONE;
                    ScriptTimer.Stop();
                    break;
                case CFG_VEHICLES:
                    WriteDataset(FormMotorpoolTool.CFG_VEHICLES_PATH, Clipboard.GetText());
                    SetLabelToComplete(LabelUpdateCfgVehicles);

                    expectedDatasetToUpdate = NONE;
                    ScriptTimer.Stop();
                    break;
                default:
                    break;
            }
        }

        private void FormUpdateDataset_Load(object sender, EventArgs e)
        {
            FormUpdateDataset.IsOpen = true;
        }

        private void FormUpdateDataset_FormClosing(object sender, FormClosingEventArgs e)
        {
            FormUpdateDataset.IsOpen = false;
        }
    }
}
