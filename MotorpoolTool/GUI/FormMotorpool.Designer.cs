﻿namespace MotorpoolTool.GUI
{
    partial class FormMotorpoolTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMotorpoolTool));
            this.TextBoxClass2 = new System.Windows.Forms.TextBox();
            this.TextBoxClass3 = new System.Windows.Forms.TextBox();
            this.TextBoxClass4 = new System.Windows.Forms.TextBox();
            this.TextBoxClass1 = new System.Windows.Forms.TextBox();
            this.TextBoxClass5 = new System.Windows.Forms.TextBox();
            this.LabelVehiclesClass1 = new System.Windows.Forms.Label();
            this.LabelVehiclesClass2 = new System.Windows.Forms.Label();
            this.LabelVehiclesClass3 = new System.Windows.Forms.Label();
            this.LabelVehiclesClass4 = new System.Windows.Forms.Label();
            this.LabelVehiclesClass5 = new System.Windows.Forms.Label();
            this.LabelClassName = new System.Windows.Forms.Label();
            this.TextBoxClassName = new System.Windows.Forms.TextBox();
            this.TabControlMotorpoolInfo = new System.Windows.Forms.TabControl();
            this.TabPageVehicleInfo = new System.Windows.Forms.TabPage();
            this.GroupBoxVehicleInfoGeneral = new System.Windows.Forms.GroupBox();
            this.TableLayoutPanelVehicleInfo = new System.Windows.Forms.TableLayoutPanel();
            this.TextBoxVehicleInfoCOP = new System.Windows.Forms.TextBox();
            this.TextBoxVehicleInfoCargo = new System.Windows.Forms.TextBox();
            this.TextBoxVehicleInfoCost = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBoxVehicleInfoCrew = new System.Windows.Forms.TextBox();
            this.LabelVehicleInfoCrew = new System.Windows.Forms.Label();
            this.TextBoxVehicleInfoRestriction = new System.Windows.Forms.TextBox();
            this.LabelVehicleInfoRestriction = new System.Windows.Forms.Label();
            this.LabelVehicleInfoEscalation = new System.Windows.Forms.Label();
            this.LabelVehicleInfoCategory = new System.Windows.Forms.Label();
            this.LabelVehicleInfoPax = new System.Windows.Forms.Label();
            this.LabelVehicleInfoFloats = new System.Windows.Forms.Label();
            this.TextBoxVehicleEscalation = new System.Windows.Forms.TextBox();
            this.TextBoxVehicleInfoCategory = new System.Windows.Forms.TextBox();
            this.TextBoxVehicleInfoPax = new System.Windows.Forms.TextBox();
            this.TextBoxVehicleInfoFloats = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.GroupBoxVehicleInfoWeapons = new System.Windows.Forms.GroupBox();
            this.LabelWeaponsWeapons = new System.Windows.Forms.Label();
            this.TextBoxWeaponAmmoCount = new System.Windows.Forms.TextBox();
            this.LabelWeaponAmmoCount = new System.Windows.Forms.Label();
            this.ListBoxWeapons = new System.Windows.Forms.ListBox();
            this.LabelWeaponDescription = new System.Windows.Forms.Label();
            this.TextBoxWeaponDescription = new System.Windows.Forms.TextBox();
            this.TabPageVehicleDeveloperInfo = new System.Windows.Forms.TabPage();
            this.TextBoxInhertiance = new System.Windows.Forms.TextBox();
            this.LabelWeaponClassName = new System.Windows.Forms.Label();
            this.TextBoxWeaponClassName = new System.Windows.Forms.TextBox();
            this.LabelInhertiance = new System.Windows.Forms.Label();
            this.LabelBWIClass = new System.Windows.Forms.Label();
            this.TextBoxBWIClass = new System.Windows.Forms.TextBox();
            this.PictureBoxVehicle = new System.Windows.Forms.PictureBox();
            this.MenuStripMotorpoolTool = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateDatasetStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EnableDEVModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vehicleInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showVehicleAmmoAsSuffixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dumpVehicleDebugStringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.factionInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupItemsInInventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showInventoryQuantityAsSuffixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StagingViewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeFontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TabControlFactionInfo = new System.Windows.Forms.TabControl();
            this.TabPageMotorpool = new System.Windows.Forms.TabPage();
            this.SplitContainerVehicleView = new System.Windows.Forms.SplitContainer();
            this.ObjectListViewMotorpool = new BrightIdeasSoftware.ObjectListView();
            this.MPColumnReconSpecialLocked = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnDisplayName = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnCategory = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnEscalation = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnCrew = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnPassengers = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnTotalCapacity = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnCargo = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnCost = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnCanFloat = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnIsOutpost = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnMaximumLoad = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnTransportAmmo = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MPColumnTransportFuel = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.TabPageLoadouts = new System.Windows.Forms.TabPage();
            this.TableLayoutPanelRoleInformation = new System.Windows.Forms.TableLayoutPanel();
            this.GroupBoxRoleGeneralInformation = new System.Windows.Forms.GroupBox();
            this.TextBoxFactionGroup = new System.Windows.Forms.TextBox();
            this.LabelFactionGroup = new System.Windows.Forms.Label();
            this.LabelElementRole = new System.Windows.Forms.Label();
            this.TextBoxFaction = new System.Windows.Forms.TextBox();
            this.LabelStaminaFactor = new System.Windows.Forms.Label();
            this.TextBoxElementRole = new System.Windows.Forms.TextBox();
            this.TextBoxStaminaFactor = new System.Windows.Forms.TextBox();
            this.LabelFaction = new System.Windows.Forms.Label();
            this.TextBoxCallsign = new System.Windows.Forms.TextBox();
            this.LabelCallsign = new System.Windows.Forms.Label();
            this.GroupBoxSpecialRoleInformation = new System.Windows.Forms.GroupBox();
            this.TextBoxMedic = new System.Windows.Forms.TextBox();
            this.LabelMedic = new System.Windows.Forms.Label();
            this.TextBoxEngineer = new System.Windows.Forms.TextBox();
            this.LabelInterpreter = new System.Windows.Forms.Label();
            this.LabelEngineer = new System.Windows.Forms.Label();
            this.TextBoxInterpreter = new System.Windows.Forms.TextBox();
            this.TextBoxEOD = new System.Windows.Forms.TextBox();
            this.LabelGForce = new System.Windows.Forms.Label();
            this.LabelEOD = new System.Windows.Forms.Label();
            this.TextBoxGForce = new System.Windows.Forms.TextBox();
            this.TabControlRoleInfo = new System.Windows.Forms.TabControl();
            this.TabPageInventory = new System.Windows.Forms.TabPage();
            this.TableLayoutPanelInventory = new System.Windows.Forms.TableLayoutPanel();
            this.GroupBoxVestLoad = new System.Windows.Forms.GroupBox();
            this.TextBoxVest = new System.Windows.Forms.TextBox();
            this.ListBoxVest = new System.Windows.Forms.ListBox();
            this.GroupBoxUniformLoad = new System.Windows.Forms.GroupBox();
            this.TextBoxUniform = new System.Windows.Forms.TextBox();
            this.ListBoxUniform = new System.Windows.Forms.ListBox();
            this.GroupBoxBackpackLoad = new System.Windows.Forms.GroupBox();
            this.ListBoxBackpack = new System.Windows.Forms.ListBox();
            this.TextBoxBackpack = new System.Windows.Forms.TextBox();
            this.TabPageAccessories = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.GroupBoxMiscGear = new System.Windows.Forms.GroupBox();
            this.TextBoxSecondary = new System.Windows.Forms.TextBox();
            this.LabelUAVTerminal = new System.Windows.Forms.Label();
            this.LabelBinocular = new System.Windows.Forms.Label();
            this.LabelBFT = new System.Windows.Forms.Label();
            this.TextBoxBinocular = new System.Windows.Forms.TextBox();
            this.TextBoxBFT = new System.Windows.Forms.TextBox();
            this.LabelTertiary = new System.Windows.Forms.Label();
            this.TextBoxUAVTerminal = new System.Windows.Forms.TextBox();
            this.TextBoxTertiary = new System.Windows.Forms.TextBox();
            this.TextBoxWatch = new System.Windows.Forms.TextBox();
            this.TextBoxMap = new System.Windows.Forms.TextBox();
            this.LabelWatch = new System.Windows.Forms.Label();
            this.LabelSecondary = new System.Windows.Forms.Label();
            this.TextBoxCompass = new System.Windows.Forms.TextBox();
            this.LabelMap = new System.Windows.Forms.Label();
            this.LabelCompass = new System.Windows.Forms.Label();
            this.LabelGPS = new System.Windows.Forms.Label();
            this.TextBoxGPS = new System.Windows.Forms.TextBox();
            this.GroupBoxPrimary = new System.Windows.Forms.GroupBox();
            this.TextBoxPrimary = new System.Windows.Forms.TextBox();
            this.ListBoxPrimaryAccessories = new System.Windows.Forms.ListBox();
            this.GroupBoxHeadgear = new System.Windows.Forms.GroupBox();
            this.ListBoxHeadgearAccessories = new System.Windows.Forms.ListBox();
            this.TextBoxHeadgear = new System.Windows.Forms.TextBox();
            this.TreeListViewRoles = new BrightIdeasSoftware.TreeListView();
            this.RoleColumnName = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.TabPageResupply = new System.Windows.Forms.TabPage();
            this.TreeListViewResupply = new BrightIdeasSoftware.TreeListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.TreeListViewSides = new BrightIdeasSoftware.TreeListView();
            this.SideColumnName = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ToolTipInfo = new System.Windows.Forms.ToolTip(this.components);
            this.SplitContainerMPT = new System.Windows.Forms.SplitContainer();
            this.LabelSubFaction = new System.Windows.Forms.Label();
            this.ButtonToggleView = new System.Windows.Forms.Button();
            this.TextBoxSearch = new System.Windows.Forms.TextBox();
            this.TabControlMotorpoolInfo.SuspendLayout();
            this.TabPageVehicleInfo.SuspendLayout();
            this.GroupBoxVehicleInfoGeneral.SuspendLayout();
            this.TableLayoutPanelVehicleInfo.SuspendLayout();
            this.GroupBoxVehicleInfoWeapons.SuspendLayout();
            this.TabPageVehicleDeveloperInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxVehicle)).BeginInit();
            this.MenuStripMotorpoolTool.SuspendLayout();
            this.TabControlFactionInfo.SuspendLayout();
            this.TabPageMotorpool.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerVehicleView)).BeginInit();
            this.SplitContainerVehicleView.Panel1.SuspendLayout();
            this.SplitContainerVehicleView.Panel2.SuspendLayout();
            this.SplitContainerVehicleView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ObjectListViewMotorpool)).BeginInit();
            this.TabPageLoadouts.SuspendLayout();
            this.TableLayoutPanelRoleInformation.SuspendLayout();
            this.GroupBoxRoleGeneralInformation.SuspendLayout();
            this.GroupBoxSpecialRoleInformation.SuspendLayout();
            this.TabControlRoleInfo.SuspendLayout();
            this.TabPageInventory.SuspendLayout();
            this.TableLayoutPanelInventory.SuspendLayout();
            this.GroupBoxVestLoad.SuspendLayout();
            this.GroupBoxUniformLoad.SuspendLayout();
            this.GroupBoxBackpackLoad.SuspendLayout();
            this.TabPageAccessories.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.GroupBoxMiscGear.SuspendLayout();
            this.GroupBoxPrimary.SuspendLayout();
            this.GroupBoxHeadgear.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeListViewRoles)).BeginInit();
            this.TabPageResupply.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeListViewResupply)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeListViewSides)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerMPT)).BeginInit();
            this.SplitContainerMPT.Panel1.SuspendLayout();
            this.SplitContainerMPT.Panel2.SuspendLayout();
            this.SplitContainerMPT.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBoxClass2
            // 
            this.TextBoxClass2.Location = new System.Drawing.Point(3, 81);
            this.TextBoxClass2.Multiline = true;
            this.TextBoxClass2.Name = "TextBoxClass2";
            this.TextBoxClass2.ReadOnly = true;
            this.TextBoxClass2.Size = new System.Drawing.Size(272, 40);
            this.TextBoxClass2.TabIndex = 6;
            this.TextBoxClass2.Click += new System.EventHandler(this.TextBoxClass2_Click);
            // 
            // TextBoxClass3
            // 
            this.TextBoxClass3.Location = new System.Drawing.Point(3, 140);
            this.TextBoxClass3.Multiline = true;
            this.TextBoxClass3.Name = "TextBoxClass3";
            this.TextBoxClass3.ReadOnly = true;
            this.TextBoxClass3.Size = new System.Drawing.Size(272, 40);
            this.TextBoxClass3.TabIndex = 7;
            this.TextBoxClass3.Click += new System.EventHandler(this.TextBoxClass3_Click);
            // 
            // TextBoxClass4
            // 
            this.TextBoxClass4.Location = new System.Drawing.Point(3, 199);
            this.TextBoxClass4.Multiline = true;
            this.TextBoxClass4.Name = "TextBoxClass4";
            this.TextBoxClass4.ReadOnly = true;
            this.TextBoxClass4.Size = new System.Drawing.Size(272, 40);
            this.TextBoxClass4.TabIndex = 8;
            this.TextBoxClass4.Click += new System.EventHandler(this.TextBoxClass4_Click);
            // 
            // TextBoxClass1
            // 
            this.TextBoxClass1.Location = new System.Drawing.Point(3, 22);
            this.TextBoxClass1.Multiline = true;
            this.TextBoxClass1.Name = "TextBoxClass1";
            this.TextBoxClass1.ReadOnly = true;
            this.TextBoxClass1.Size = new System.Drawing.Size(272, 40);
            this.TextBoxClass1.TabIndex = 9;
            this.TextBoxClass1.Click += new System.EventHandler(this.TextBoxClass1_Click);
            // 
            // TextBoxClass5
            // 
            this.TextBoxClass5.Location = new System.Drawing.Point(3, 258);
            this.TextBoxClass5.Multiline = true;
            this.TextBoxClass5.Name = "TextBoxClass5";
            this.TextBoxClass5.ReadOnly = true;
            this.TextBoxClass5.Size = new System.Drawing.Size(272, 40);
            this.TextBoxClass5.TabIndex = 10;
            this.TextBoxClass5.Click += new System.EventHandler(this.TextBoxClass5_Click);
            // 
            // LabelVehiclesClass1
            // 
            this.LabelVehiclesClass1.AutoSize = true;
            this.LabelVehiclesClass1.Location = new System.Drawing.Point(3, 6);
            this.LabelVehiclesClass1.Name = "LabelVehiclesClass1";
            this.LabelVehiclesClass1.Size = new System.Drawing.Size(48, 13);
            this.LabelVehiclesClass1.TabIndex = 11;
            this.LabelVehiclesClass1.Text = "UTILITY";
            // 
            // LabelVehiclesClass2
            // 
            this.LabelVehiclesClass2.AutoSize = true;
            this.LabelVehiclesClass2.Location = new System.Drawing.Point(3, 65);
            this.LabelVehiclesClass2.Name = "LabelVehiclesClass2";
            this.LabelVehiclesClass2.Size = new System.Drawing.Size(60, 13);
            this.LabelVehiclesClass2.TabIndex = 12;
            this.LabelVehiclesClass2.Text = "ANTI_CAR";
            // 
            // LabelVehiclesClass3
            // 
            this.LabelVehiclesClass3.AutoSize = true;
            this.LabelVehiclesClass3.Location = new System.Drawing.Point(4, 124);
            this.LabelVehiclesClass3.Name = "LabelVehiclesClass3";
            this.LabelVehiclesClass3.Size = new System.Drawing.Size(59, 13);
            this.LabelVehiclesClass3.TabIndex = 13;
            this.LabelVehiclesClass3.Text = "ANTI_APC";
            // 
            // LabelVehiclesClass4
            // 
            this.LabelVehiclesClass4.AutoSize = true;
            this.LabelVehiclesClass4.Location = new System.Drawing.Point(4, 183);
            this.LabelVehiclesClass4.Name = "LabelVehiclesClass4";
            this.LabelVehiclesClass4.Size = new System.Drawing.Size(54, 13);
            this.LabelVehiclesClass4.TabIndex = 14;
            this.LabelVehiclesClass4.Text = "ANTI_IFV";
            // 
            // LabelVehiclesClass5
            // 
            this.LabelVehiclesClass5.AutoSize = true;
            this.LabelVehiclesClass5.Location = new System.Drawing.Point(4, 242);
            this.LabelVehiclesClass5.Name = "LabelVehiclesClass5";
            this.LabelVehiclesClass5.Size = new System.Drawing.Size(61, 13);
            this.LabelVehiclesClass5.TabIndex = 15;
            this.LabelVehiclesClass5.Text = "ANTI_MBT";
            // 
            // LabelClassName
            // 
            this.LabelClassName.AutoSize = true;
            this.LabelClassName.Location = new System.Drawing.Point(390, 6);
            this.LabelClassName.Name = "LabelClassName";
            this.LabelClassName.Size = new System.Drawing.Size(91, 13);
            this.LabelClassName.TabIndex = 17;
            this.LabelClassName.Text = "Config Classname";
            this.LabelClassName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxClassName
            // 
            this.TextBoxClassName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxClassName.Location = new System.Drawing.Point(487, 3);
            this.TextBoxClassName.Name = "TextBoxClassName";
            this.TextBoxClassName.ReadOnly = true;
            this.TextBoxClassName.Size = new System.Drawing.Size(278, 20);
            this.TextBoxClassName.TabIndex = 21;
            this.TextBoxClassName.Click += new System.EventHandler(this.TextBoxClassName_Click);
            // 
            // TabControlMotorpoolInfo
            // 
            this.TabControlMotorpoolInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControlMotorpoolInfo.Controls.Add(this.TabPageVehicleInfo);
            this.TabControlMotorpoolInfo.Controls.Add(this.TabPageVehicleDeveloperInfo);
            this.TabControlMotorpoolInfo.Location = new System.Drawing.Point(4, 299);
            this.TabControlMotorpoolInfo.Name = "TabControlMotorpoolInfo";
            this.TabControlMotorpoolInfo.SelectedIndex = 0;
            this.TabControlMotorpoolInfo.Size = new System.Drawing.Size(511, 315);
            this.TabControlMotorpoolInfo.TabIndex = 28;
            // 
            // TabPageVehicleInfo
            // 
            this.TabPageVehicleInfo.Controls.Add(this.GroupBoxVehicleInfoGeneral);
            this.TabPageVehicleInfo.Controls.Add(this.GroupBoxVehicleInfoWeapons);
            this.TabPageVehicleInfo.Location = new System.Drawing.Point(4, 22);
            this.TabPageVehicleInfo.Name = "TabPageVehicleInfo";
            this.TabPageVehicleInfo.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageVehicleInfo.Size = new System.Drawing.Size(503, 289);
            this.TabPageVehicleInfo.TabIndex = 1;
            this.TabPageVehicleInfo.Text = "Vehicle Information";
            this.TabPageVehicleInfo.UseVisualStyleBackColor = true;
            // 
            // GroupBoxVehicleInfoGeneral
            // 
            this.GroupBoxVehicleInfoGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxVehicleInfoGeneral.Controls.Add(this.TableLayoutPanelVehicleInfo);
            this.GroupBoxVehicleInfoGeneral.Location = new System.Drawing.Point(3, 6);
            this.GroupBoxVehicleInfoGeneral.Name = "GroupBoxVehicleInfoGeneral";
            this.GroupBoxVehicleInfoGeneral.Size = new System.Drawing.Size(494, 102);
            this.GroupBoxVehicleInfoGeneral.TabIndex = 45;
            this.GroupBoxVehicleInfoGeneral.TabStop = false;
            this.GroupBoxVehicleInfoGeneral.Text = "General Information";
            // 
            // TableLayoutPanelVehicleInfo
            // 
            this.TableLayoutPanelVehicleInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TableLayoutPanelVehicleInfo.ColumnCount = 6;
            this.TableLayoutPanelVehicleInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelVehicleInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TableLayoutPanelVehicleInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelVehicleInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TableLayoutPanelVehicleInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelVehicleInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.TextBoxVehicleInfoCOP, 5, 2);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.TextBoxVehicleInfoCargo, 5, 1);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.TextBoxVehicleInfoCost, 5, 0);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.label6, 4, 0);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.TextBoxVehicleInfoCrew, 3, 0);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.LabelVehicleInfoCrew, 2, 0);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.TextBoxVehicleInfoRestriction, 1, 0);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.LabelVehicleInfoRestriction, 0, 0);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.LabelVehicleInfoEscalation, 0, 1);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.LabelVehicleInfoCategory, 0, 2);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.LabelVehicleInfoPax, 2, 1);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.LabelVehicleInfoFloats, 2, 2);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.TextBoxVehicleEscalation, 1, 1);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.TextBoxVehicleInfoCategory, 1, 2);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.TextBoxVehicleInfoPax, 3, 1);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.TextBoxVehicleInfoFloats, 3, 2);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.label7, 4, 1);
            this.TableLayoutPanelVehicleInfo.Controls.Add(this.label8, 4, 2);
            this.TableLayoutPanelVehicleInfo.Location = new System.Drawing.Point(3, 19);
            this.TableLayoutPanelVehicleInfo.Name = "TableLayoutPanelVehicleInfo";
            this.TableLayoutPanelVehicleInfo.RowCount = 3;
            this.TableLayoutPanelVehicleInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TableLayoutPanelVehicleInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TableLayoutPanelVehicleInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TableLayoutPanelVehicleInfo.Size = new System.Drawing.Size(488, 83);
            this.TableLayoutPanelVehicleInfo.TabIndex = 50;
            // 
            // TextBoxVehicleInfoCOP
            // 
            this.TextBoxVehicleInfoCOP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxVehicleInfoCOP.Location = new System.Drawing.Point(376, 57);
            this.TextBoxVehicleInfoCOP.Name = "TextBoxVehicleInfoCOP";
            this.TextBoxVehicleInfoCOP.ReadOnly = true;
            this.TextBoxVehicleInfoCOP.Size = new System.Drawing.Size(109, 20);
            this.TextBoxVehicleInfoCOP.TabIndex = 61;
            // 
            // TextBoxVehicleInfoCargo
            // 
            this.TextBoxVehicleInfoCargo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxVehicleInfoCargo.Location = new System.Drawing.Point(376, 30);
            this.TextBoxVehicleInfoCargo.Name = "TextBoxVehicleInfoCargo";
            this.TextBoxVehicleInfoCargo.ReadOnly = true;
            this.TextBoxVehicleInfoCargo.Size = new System.Drawing.Size(109, 20);
            this.TextBoxVehicleInfoCargo.TabIndex = 60;
            // 
            // TextBoxVehicleInfoCost
            // 
            this.TextBoxVehicleInfoCost.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxVehicleInfoCost.Location = new System.Drawing.Point(376, 3);
            this.TextBoxVehicleInfoCost.Name = "TextBoxVehicleInfoCost";
            this.TextBoxVehicleInfoCost.ReadOnly = true;
            this.TextBoxVehicleInfoCost.Size = new System.Drawing.Size(109, 20);
            this.TextBoxVehicleInfoCost.TabIndex = 59;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(335, 3);
            this.label6.Margin = new System.Windows.Forms.Padding(3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 21);
            this.label6.TabIndex = 56;
            this.label6.Text = "Cost";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxVehicleInfoCrew
            // 
            this.TextBoxVehicleInfoCrew.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxVehicleInfoCrew.Location = new System.Drawing.Point(221, 3);
            this.TextBoxVehicleInfoCrew.Name = "TextBoxVehicleInfoCrew";
            this.TextBoxVehicleInfoCrew.ReadOnly = true;
            this.TextBoxVehicleInfoCrew.Size = new System.Drawing.Size(108, 20);
            this.TextBoxVehicleInfoCrew.TabIndex = 53;
            // 
            // LabelVehicleInfoCrew
            // 
            this.LabelVehicleInfoCrew.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelVehicleInfoCrew.AutoSize = true;
            this.LabelVehicleInfoCrew.Location = new System.Drawing.Point(180, 3);
            this.LabelVehicleInfoCrew.Margin = new System.Windows.Forms.Padding(3);
            this.LabelVehicleInfoCrew.Name = "LabelVehicleInfoCrew";
            this.LabelVehicleInfoCrew.Size = new System.Drawing.Size(35, 21);
            this.LabelVehicleInfoCrew.TabIndex = 48;
            this.LabelVehicleInfoCrew.Text = "Crew";
            this.LabelVehicleInfoCrew.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxVehicleInfoRestriction
            // 
            this.TextBoxVehicleInfoRestriction.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxVehicleInfoRestriction.Location = new System.Drawing.Point(66, 3);
            this.TextBoxVehicleInfoRestriction.Name = "TextBoxVehicleInfoRestriction";
            this.TextBoxVehicleInfoRestriction.ReadOnly = true;
            this.TextBoxVehicleInfoRestriction.Size = new System.Drawing.Size(108, 20);
            this.TextBoxVehicleInfoRestriction.TabIndex = 43;
            // 
            // LabelVehicleInfoRestriction
            // 
            this.LabelVehicleInfoRestriction.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelVehicleInfoRestriction.AutoSize = true;
            this.LabelVehicleInfoRestriction.Location = new System.Drawing.Point(3, 3);
            this.LabelVehicleInfoRestriction.Margin = new System.Windows.Forms.Padding(3);
            this.LabelVehicleInfoRestriction.Name = "LabelVehicleInfoRestriction";
            this.LabelVehicleInfoRestriction.Size = new System.Drawing.Size(57, 21);
            this.LabelVehicleInfoRestriction.TabIndex = 45;
            this.LabelVehicleInfoRestriction.Text = "Restriction";
            this.LabelVehicleInfoRestriction.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelVehicleInfoEscalation
            // 
            this.LabelVehicleInfoEscalation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelVehicleInfoEscalation.AutoSize = true;
            this.LabelVehicleInfoEscalation.Location = new System.Drawing.Point(3, 30);
            this.LabelVehicleInfoEscalation.Margin = new System.Windows.Forms.Padding(3);
            this.LabelVehicleInfoEscalation.Name = "LabelVehicleInfoEscalation";
            this.LabelVehicleInfoEscalation.Size = new System.Drawing.Size(57, 21);
            this.LabelVehicleInfoEscalation.TabIndex = 46;
            this.LabelVehicleInfoEscalation.Text = "Escalation";
            this.LabelVehicleInfoEscalation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelVehicleInfoCategory
            // 
            this.LabelVehicleInfoCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelVehicleInfoCategory.AutoSize = true;
            this.LabelVehicleInfoCategory.Location = new System.Drawing.Point(3, 57);
            this.LabelVehicleInfoCategory.Margin = new System.Windows.Forms.Padding(3);
            this.LabelVehicleInfoCategory.Name = "LabelVehicleInfoCategory";
            this.LabelVehicleInfoCategory.Size = new System.Drawing.Size(57, 23);
            this.LabelVehicleInfoCategory.TabIndex = 47;
            this.LabelVehicleInfoCategory.Text = "Category";
            this.LabelVehicleInfoCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelVehicleInfoPax
            // 
            this.LabelVehicleInfoPax.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelVehicleInfoPax.AutoSize = true;
            this.LabelVehicleInfoPax.Location = new System.Drawing.Point(180, 30);
            this.LabelVehicleInfoPax.Margin = new System.Windows.Forms.Padding(3);
            this.LabelVehicleInfoPax.Name = "LabelVehicleInfoPax";
            this.LabelVehicleInfoPax.Size = new System.Drawing.Size(35, 21);
            this.LabelVehicleInfoPax.TabIndex = 49;
            this.LabelVehicleInfoPax.Text = "Pax";
            this.LabelVehicleInfoPax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelVehicleInfoFloats
            // 
            this.LabelVehicleInfoFloats.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelVehicleInfoFloats.AutoSize = true;
            this.LabelVehicleInfoFloats.Location = new System.Drawing.Point(180, 57);
            this.LabelVehicleInfoFloats.Margin = new System.Windows.Forms.Padding(3);
            this.LabelVehicleInfoFloats.Name = "LabelVehicleInfoFloats";
            this.LabelVehicleInfoFloats.Size = new System.Drawing.Size(35, 23);
            this.LabelVehicleInfoFloats.TabIndex = 50;
            this.LabelVehicleInfoFloats.Text = "Floats";
            this.LabelVehicleInfoFloats.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxVehicleEscalation
            // 
            this.TextBoxVehicleEscalation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxVehicleEscalation.Location = new System.Drawing.Point(66, 30);
            this.TextBoxVehicleEscalation.Name = "TextBoxVehicleEscalation";
            this.TextBoxVehicleEscalation.ReadOnly = true;
            this.TextBoxVehicleEscalation.Size = new System.Drawing.Size(108, 20);
            this.TextBoxVehicleEscalation.TabIndex = 51;
            // 
            // TextBoxVehicleInfoCategory
            // 
            this.TextBoxVehicleInfoCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxVehicleInfoCategory.Location = new System.Drawing.Point(66, 57);
            this.TextBoxVehicleInfoCategory.Name = "TextBoxVehicleInfoCategory";
            this.TextBoxVehicleInfoCategory.ReadOnly = true;
            this.TextBoxVehicleInfoCategory.Size = new System.Drawing.Size(108, 20);
            this.TextBoxVehicleInfoCategory.TabIndex = 52;
            // 
            // TextBoxVehicleInfoPax
            // 
            this.TextBoxVehicleInfoPax.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxVehicleInfoPax.Location = new System.Drawing.Point(221, 30);
            this.TextBoxVehicleInfoPax.Name = "TextBoxVehicleInfoPax";
            this.TextBoxVehicleInfoPax.ReadOnly = true;
            this.TextBoxVehicleInfoPax.Size = new System.Drawing.Size(108, 20);
            this.TextBoxVehicleInfoPax.TabIndex = 54;
            // 
            // TextBoxVehicleInfoFloats
            // 
            this.TextBoxVehicleInfoFloats.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxVehicleInfoFloats.Location = new System.Drawing.Point(221, 57);
            this.TextBoxVehicleInfoFloats.Name = "TextBoxVehicleInfoFloats";
            this.TextBoxVehicleInfoFloats.ReadOnly = true;
            this.TextBoxVehicleInfoFloats.Size = new System.Drawing.Size(108, 20);
            this.TextBoxVehicleInfoFloats.TabIndex = 55;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(335, 30);
            this.label7.Margin = new System.Windows.Forms.Padding(3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 21);
            this.label7.TabIndex = 57;
            this.label7.Text = "Cargo";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(335, 57);
            this.label8.Margin = new System.Windows.Forms.Padding(3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 23);
            this.label8.TabIndex = 58;
            this.label8.Text = "COP";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GroupBoxVehicleInfoWeapons
            // 
            this.GroupBoxVehicleInfoWeapons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxVehicleInfoWeapons.Controls.Add(this.LabelWeaponsWeapons);
            this.GroupBoxVehicleInfoWeapons.Controls.Add(this.TextBoxWeaponAmmoCount);
            this.GroupBoxVehicleInfoWeapons.Controls.Add(this.LabelWeaponAmmoCount);
            this.GroupBoxVehicleInfoWeapons.Controls.Add(this.ListBoxWeapons);
            this.GroupBoxVehicleInfoWeapons.Controls.Add(this.LabelWeaponDescription);
            this.GroupBoxVehicleInfoWeapons.Controls.Add(this.TextBoxWeaponDescription);
            this.GroupBoxVehicleInfoWeapons.Location = new System.Drawing.Point(3, 114);
            this.GroupBoxVehicleInfoWeapons.Name = "GroupBoxVehicleInfoWeapons";
            this.GroupBoxVehicleInfoWeapons.Size = new System.Drawing.Size(494, 169);
            this.GroupBoxVehicleInfoWeapons.TabIndex = 39;
            this.GroupBoxVehicleInfoWeapons.TabStop = false;
            this.GroupBoxVehicleInfoWeapons.Text = "Weapon Information";
            // 
            // LabelWeaponsWeapons
            // 
            this.LabelWeaponsWeapons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelWeaponsWeapons.AutoSize = true;
            this.LabelWeaponsWeapons.Location = new System.Drawing.Point(249, 16);
            this.LabelWeaponsWeapons.Name = "LabelWeaponsWeapons";
            this.LabelWeaponsWeapons.Size = new System.Drawing.Size(53, 13);
            this.LabelWeaponsWeapons.TabIndex = 31;
            this.LabelWeaponsWeapons.Text = "Weapons";
            this.LabelWeaponsWeapons.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxWeaponAmmoCount
            // 
            this.TextBoxWeaponAmmoCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TextBoxWeaponAmmoCount.Location = new System.Drawing.Point(6, 32);
            this.TextBoxWeaponAmmoCount.Multiline = true;
            this.TextBoxWeaponAmmoCount.Name = "TextBoxWeaponAmmoCount";
            this.TextBoxWeaponAmmoCount.ReadOnly = true;
            this.TextBoxWeaponAmmoCount.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxWeaponAmmoCount.Size = new System.Drawing.Size(240, 52);
            this.TextBoxWeaponAmmoCount.TabIndex = 26;
            this.TextBoxWeaponAmmoCount.WordWrap = false;
            // 
            // LabelWeaponAmmoCount
            // 
            this.LabelWeaponAmmoCount.AutoSize = true;
            this.LabelWeaponAmmoCount.Location = new System.Drawing.Point(6, 16);
            this.LabelWeaponAmmoCount.Name = "LabelWeaponAmmoCount";
            this.LabelWeaponAmmoCount.Size = new System.Drawing.Size(36, 13);
            this.LabelWeaponAmmoCount.TabIndex = 30;
            this.LabelWeaponAmmoCount.Text = "Ammo";
            this.LabelWeaponAmmoCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ListBoxWeapons
            // 
            this.ListBoxWeapons.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListBoxWeapons.FormattingEnabled = true;
            this.ListBoxWeapons.Location = new System.Drawing.Point(252, 32);
            this.ListBoxWeapons.Name = "ListBoxWeapons";
            this.ListBoxWeapons.Size = new System.Drawing.Size(236, 134);
            this.ListBoxWeapons.TabIndex = 0;
            this.ListBoxWeapons.SelectedIndexChanged += new System.EventHandler(this.ListBoxWeapons_SelectedIndexChanged);
            // 
            // LabelWeaponDescription
            // 
            this.LabelWeaponDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelWeaponDescription.AutoSize = true;
            this.LabelWeaponDescription.Location = new System.Drawing.Point(6, 87);
            this.LabelWeaponDescription.Name = "LabelWeaponDescription";
            this.LabelWeaponDescription.Size = new System.Drawing.Size(60, 13);
            this.LabelWeaponDescription.TabIndex = 29;
            this.LabelWeaponDescription.Text = "Description";
            this.LabelWeaponDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxWeaponDescription
            // 
            this.TextBoxWeaponDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TextBoxWeaponDescription.Location = new System.Drawing.Point(6, 103);
            this.TextBoxWeaponDescription.Multiline = true;
            this.TextBoxWeaponDescription.Name = "TextBoxWeaponDescription";
            this.TextBoxWeaponDescription.ReadOnly = true;
            this.TextBoxWeaponDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxWeaponDescription.Size = new System.Drawing.Size(240, 62);
            this.TextBoxWeaponDescription.TabIndex = 25;
            // 
            // TabPageVehicleDeveloperInfo
            // 
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.TextBoxClass1);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.TextBoxInhertiance);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.TextBoxClass2);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.LabelVehiclesClass5);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.LabelWeaponClassName);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.LabelVehiclesClass1);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.TextBoxClass3);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.TextBoxWeaponClassName);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.LabelVehiclesClass2);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.LabelInhertiance);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.LabelVehiclesClass4);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.TextBoxClass5);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.TextBoxClass4);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.LabelBWIClass);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.LabelVehiclesClass3);
            this.TabPageVehicleDeveloperInfo.Controls.Add(this.TextBoxBWIClass);
            this.TabPageVehicleDeveloperInfo.Location = new System.Drawing.Point(4, 22);
            this.TabPageVehicleDeveloperInfo.Name = "TabPageVehicleDeveloperInfo";
            this.TabPageVehicleDeveloperInfo.Size = new System.Drawing.Size(503, 289);
            this.TabPageVehicleDeveloperInfo.TabIndex = 2;
            this.TabPageVehicleDeveloperInfo.Text = "Developer Info";
            this.TabPageVehicleDeveloperInfo.UseVisualStyleBackColor = true;
            // 
            // TextBoxInhertiance
            // 
            this.TextBoxInhertiance.Location = new System.Drawing.Point(296, 117);
            this.TextBoxInhertiance.Name = "TextBoxInhertiance";
            this.TextBoxInhertiance.ReadOnly = true;
            this.TextBoxInhertiance.Size = new System.Drawing.Size(200, 20);
            this.TextBoxInhertiance.TabIndex = 40;
            this.TextBoxInhertiance.Click += new System.EventHandler(this.TextBoxInhertiance_Click);
            // 
            // LabelWeaponClassName
            // 
            this.LabelWeaponClassName.AutoSize = true;
            this.LabelWeaponClassName.Location = new System.Drawing.Point(296, 6);
            this.LabelWeaponClassName.Name = "LabelWeaponClassName";
            this.LabelWeaponClassName.Size = new System.Drawing.Size(140, 13);
            this.LabelWeaponClassName.TabIndex = 33;
            this.LabelWeaponClassName.Text = "Vehicle Weapon Classname";
            this.LabelWeaponClassName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextBoxWeaponClassName
            // 
            this.TextBoxWeaponClassName.Location = new System.Drawing.Point(295, 22);
            this.TextBoxWeaponClassName.Name = "TextBoxWeaponClassName";
            this.TextBoxWeaponClassName.ReadOnly = true;
            this.TextBoxWeaponClassName.Size = new System.Drawing.Size(201, 20);
            this.TextBoxWeaponClassName.TabIndex = 32;
            // 
            // LabelInhertiance
            // 
            this.LabelInhertiance.AutoSize = true;
            this.LabelInhertiance.Location = new System.Drawing.Point(295, 101);
            this.LabelInhertiance.Name = "LabelInhertiance";
            this.LabelInhertiance.Size = new System.Drawing.Size(60, 13);
            this.LabelInhertiance.TabIndex = 39;
            this.LabelInhertiance.Text = "Inhertiance";
            this.LabelInhertiance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelBWIClass
            // 
            this.LabelBWIClass.AutoSize = true;
            this.LabelBWIClass.Location = new System.Drawing.Point(296, 49);
            this.LabelBWIClass.Name = "LabelBWIClass";
            this.LabelBWIClass.Size = new System.Drawing.Size(82, 13);
            this.LabelBWIClass.TabIndex = 38;
            this.LabelBWIClass.Text = "BWI Classname";
            this.LabelBWIClass.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxBWIClass
            // 
            this.TextBoxBWIClass.Location = new System.Drawing.Point(296, 65);
            this.TextBoxBWIClass.Name = "TextBoxBWIClass";
            this.TextBoxBWIClass.ReadOnly = true;
            this.TextBoxBWIClass.Size = new System.Drawing.Size(200, 20);
            this.TextBoxBWIClass.TabIndex = 37;
            // 
            // PictureBoxVehicle
            // 
            this.PictureBoxVehicle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PictureBoxVehicle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PictureBoxVehicle.Location = new System.Drawing.Point(0, 0);
            this.PictureBoxVehicle.Name = "PictureBoxVehicle";
            this.PictureBoxVehicle.Size = new System.Drawing.Size(510, 293);
            this.PictureBoxVehicle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxVehicle.TabIndex = 28;
            this.PictureBoxVehicle.TabStop = false;
            // 
            // MenuStripMotorpoolTool
            // 
            this.MenuStripMotorpoolTool.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.ViewToolStripMenuItem});
            this.MenuStripMotorpoolTool.Location = new System.Drawing.Point(0, 0);
            this.MenuStripMotorpoolTool.Name = "MenuStripMotorpoolTool";
            this.MenuStripMotorpoolTool.Size = new System.Drawing.Size(984, 24);
            this.MenuStripMotorpoolTool.TabIndex = 30;
            this.MenuStripMotorpoolTool.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateDatasetStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // updateDatasetStripMenuItem
            // 
            this.updateDatasetStripMenuItem.Name = "updateDatasetStripMenuItem";
            this.updateDatasetStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.updateDatasetStripMenuItem.Text = "Update Dataset...";
            this.updateDatasetStripMenuItem.Click += new System.EventHandler(this.UpdateDatasetStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // ViewToolStripMenuItem
            // 
            this.ViewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EnableDEVModeToolStripMenuItem,
            this.vehicleInformationToolStripMenuItem,
            this.factionInformationToolStripMenuItem,
            this.StagingViewerToolStripMenuItem,
            this.changeFontToolStripMenuItem});
            this.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem";
            this.ViewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.ViewToolStripMenuItem.Text = "View";
            // 
            // EnableDEVModeToolStripMenuItem
            // 
            this.EnableDEVModeToolStripMenuItem.Name = "EnableDEVModeToolStripMenuItem";
            this.EnableDEVModeToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.EnableDEVModeToolStripMenuItem.Text = "Enable DEV mode";
            this.EnableDEVModeToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ShowDeveloperDebugInfoToolStripMenuItem_CheckedChanged);
            this.EnableDEVModeToolStripMenuItem.Click += new System.EventHandler(this.ShowDeveloperVehicleInfoToolStripMenuItem_Click);
            // 
            // vehicleInformationToolStripMenuItem
            // 
            this.vehicleInformationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showVehicleAmmoAsSuffixToolStripMenuItem,
            this.dumpVehicleDebugStringToolStripMenuItem});
            this.vehicleInformationToolStripMenuItem.Name = "vehicleInformationToolStripMenuItem";
            this.vehicleInformationToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.vehicleInformationToolStripMenuItem.Text = "Vehicle Information";
            // 
            // showVehicleAmmoAsSuffixToolStripMenuItem
            // 
            this.showVehicleAmmoAsSuffixToolStripMenuItem.Name = "showVehicleAmmoAsSuffixToolStripMenuItem";
            this.showVehicleAmmoAsSuffixToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.showVehicleAmmoAsSuffixToolStripMenuItem.Text = "Show vehicle ammo as suffix";
            this.showVehicleAmmoAsSuffixToolStripMenuItem.Click += new System.EventHandler(this.SuffixPylonStripMenuItem_Click);
            // 
            // dumpVehicleDebugStringToolStripMenuItem
            // 
            this.dumpVehicleDebugStringToolStripMenuItem.Name = "dumpVehicleDebugStringToolStripMenuItem";
            this.dumpVehicleDebugStringToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.dumpVehicleDebugStringToolStripMenuItem.Text = "Dump vehicle debug string";
            this.dumpVehicleDebugStringToolStripMenuItem.Click += new System.EventHandler(this.DumpVehicleDebugStringToolStripMenuItem_Click);
            // 
            // factionInformationToolStripMenuItem
            // 
            this.factionInformationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.groupItemsInInventoryToolStripMenuItem,
            this.showInventoryQuantityAsSuffixToolStripMenuItem});
            this.factionInformationToolStripMenuItem.Name = "factionInformationToolStripMenuItem";
            this.factionInformationToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.factionInformationToolStripMenuItem.Text = "Faction Information";
            // 
            // groupItemsInInventoryToolStripMenuItem
            // 
            this.groupItemsInInventoryToolStripMenuItem.Name = "groupItemsInInventoryToolStripMenuItem";
            this.groupItemsInInventoryToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.groupItemsInInventoryToolStripMenuItem.Text = "Group items in inventory";
            this.groupItemsInInventoryToolStripMenuItem.Click += new System.EventHandler(this.GroupItemsInInventoryToolStripMenuItem_Click);
            // 
            // showInventoryQuantityAsSuffixToolStripMenuItem
            // 
            this.showInventoryQuantityAsSuffixToolStripMenuItem.Name = "showInventoryQuantityAsSuffixToolStripMenuItem";
            this.showInventoryQuantityAsSuffixToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.showInventoryQuantityAsSuffixToolStripMenuItem.Text = "Show inventory quantity as suffix";
            this.showInventoryQuantityAsSuffixToolStripMenuItem.Click += new System.EventHandler(this.ShowInventoryQuantityAsSuffixToolStripMenuItem_Click);
            // 
            // StagingViewerToolStripMenuItem
            // 
            this.StagingViewerToolStripMenuItem.Name = "StagingViewerToolStripMenuItem";
            this.StagingViewerToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.StagingViewerToolStripMenuItem.Text = "Staging Viewer";
            this.StagingViewerToolStripMenuItem.Click += new System.EventHandler(this.StagingViewerToolStripMenuItem_Click);
            // 
            // changeFontToolStripMenuItem
            // 
            this.changeFontToolStripMenuItem.Name = "changeFontToolStripMenuItem";
            this.changeFontToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.changeFontToolStripMenuItem.Text = "Change font...";
            this.changeFontToolStripMenuItem.Click += new System.EventHandler(this.ChangeFontToolStripMenuItem_Click);
            // 
            // TabControlFactionInfo
            // 
            this.TabControlFactionInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControlFactionInfo.Controls.Add(this.TabPageMotorpool);
            this.TabControlFactionInfo.Controls.Add(this.TabPageLoadouts);
            this.TabControlFactionInfo.Controls.Add(this.TabPageResupply);
            this.TabControlFactionInfo.Location = new System.Drawing.Point(0, 29);
            this.TabControlFactionInfo.Name = "TabControlFactionInfo";
            this.TabControlFactionInfo.SelectedIndex = 0;
            this.TabControlFactionInfo.Size = new System.Drawing.Size(773, 643);
            this.TabControlFactionInfo.TabIndex = 33;
            // 
            // TabPageMotorpool
            // 
            this.TabPageMotorpool.Controls.Add(this.SplitContainerVehicleView);
            this.TabPageMotorpool.Location = new System.Drawing.Point(4, 22);
            this.TabPageMotorpool.Name = "TabPageMotorpool";
            this.TabPageMotorpool.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageMotorpool.Size = new System.Drawing.Size(765, 617);
            this.TabPageMotorpool.TabIndex = 0;
            this.TabPageMotorpool.Text = "Motorpool";
            this.TabPageMotorpool.UseVisualStyleBackColor = true;
            // 
            // SplitContainerVehicleView
            // 
            this.SplitContainerVehicleView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SplitContainerVehicleView.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.SplitContainerVehicleView.IsSplitterFixed = true;
            this.SplitContainerVehicleView.Location = new System.Drawing.Point(0, 0);
            this.SplitContainerVehicleView.Margin = new System.Windows.Forms.Padding(0);
            this.SplitContainerVehicleView.Name = "SplitContainerVehicleView";
            // 
            // SplitContainerVehicleView.Panel1
            // 
            this.SplitContainerVehicleView.Panel1.Controls.Add(this.ObjectListViewMotorpool);
            // 
            // SplitContainerVehicleView.Panel2
            // 
            this.SplitContainerVehicleView.Panel2.Controls.Add(this.TabControlMotorpoolInfo);
            this.SplitContainerVehicleView.Panel2.Controls.Add(this.PictureBoxVehicle);
            this.SplitContainerVehicleView.Size = new System.Drawing.Size(765, 617);
            this.SplitContainerVehicleView.SplitterDistance = 259;
            this.SplitContainerVehicleView.SplitterWidth = 3;
            this.SplitContainerVehicleView.TabIndex = 29;
            // 
            // ObjectListViewMotorpool
            // 
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnReconSpecialLocked);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnDisplayName);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnCategory);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnEscalation);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnCrew);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnPassengers);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnTotalCapacity);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnCargo);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnCost);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnCanFloat);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnIsOutpost);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnMaximumLoad);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnTransportAmmo);
            this.ObjectListViewMotorpool.AllColumns.Add(this.MPColumnTransportFuel);
            this.ObjectListViewMotorpool.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ObjectListViewMotorpool.CellEditUseWholeCell = false;
            this.ObjectListViewMotorpool.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.MPColumnReconSpecialLocked,
            this.MPColumnDisplayName});
            this.ObjectListViewMotorpool.Cursor = System.Windows.Forms.Cursors.Default;
            this.ObjectListViewMotorpool.FullRowSelect = true;
            this.ObjectListViewMotorpool.GridLines = true;
            this.ObjectListViewMotorpool.HideSelection = false;
            this.ObjectListViewMotorpool.Location = new System.Drawing.Point(-1, -2);
            this.ObjectListViewMotorpool.Margin = new System.Windows.Forms.Padding(0);
            this.ObjectListViewMotorpool.Name = "ObjectListViewMotorpool";
            this.ObjectListViewMotorpool.ShowGroups = false;
            this.ObjectListViewMotorpool.Size = new System.Drawing.Size(260, 619);
            this.ObjectListViewMotorpool.TabIndex = 18;
            this.ObjectListViewMotorpool.UseCompatibleStateImageBehavior = false;
            this.ObjectListViewMotorpool.UseFiltering = true;
            this.ObjectListViewMotorpool.View = System.Windows.Forms.View.Details;
            this.ObjectListViewMotorpool.FormatRow += new System.EventHandler<BrightIdeasSoftware.FormatRowEventArgs>(this.ObjectListViewMotorpool_FormatRow);
            this.ObjectListViewMotorpool.SelectedIndexChanged += new System.EventHandler(this.ObjectListViewMotorpool_SelectedIndexChanged);
            // 
            // MPColumnReconSpecialLocked
            // 
            this.MPColumnReconSpecialLocked.AspectName = "ReconSpecialLocked";
            this.MPColumnReconSpecialLocked.Hideable = false;
            this.MPColumnReconSpecialLocked.Text = "Restrictions";
            this.MPColumnReconSpecialLocked.ToolTipText = "Is this vehicle locked behind SF / Recon?";
            this.MPColumnReconSpecialLocked.Width = 70;
            // 
            // MPColumnDisplayName
            // 
            this.MPColumnDisplayName.AspectName = "Name";
            this.MPColumnDisplayName.FillsFreeSpace = true;
            this.MPColumnDisplayName.Groupable = false;
            this.MPColumnDisplayName.Hideable = false;
            this.MPColumnDisplayName.MaximumWidth = 360;
            this.MPColumnDisplayName.MinimumWidth = 140;
            this.MPColumnDisplayName.Text = "Name";
            this.MPColumnDisplayName.ToolTipText = "Name of the vehicle as it appears in the motorpool";
            this.MPColumnDisplayName.Width = 140;
            // 
            // MPColumnCategory
            // 
            this.MPColumnCategory.AspectName = "CategoryString";
            this.MPColumnCategory.DisplayIndex = 2;
            this.MPColumnCategory.IsVisible = false;
            this.MPColumnCategory.Text = "Category";
            // 
            // MPColumnEscalation
            // 
            this.MPColumnEscalation.AspectName = "EscalationString";
            this.MPColumnEscalation.DisplayIndex = 3;
            this.MPColumnEscalation.IsVisible = false;
            this.MPColumnEscalation.Text = "Escalation";
            this.MPColumnEscalation.Width = 70;
            // 
            // MPColumnCrew
            // 
            this.MPColumnCrew.AspectName = "GetCrewCount";
            this.MPColumnCrew.DisplayIndex = 4;
            this.MPColumnCrew.Groupable = false;
            this.MPColumnCrew.IsVisible = false;
            this.MPColumnCrew.Text = "Crew";
            this.MPColumnCrew.ToolTipText = "Crew capacity";
            // 
            // MPColumnPassengers
            // 
            this.MPColumnPassengers.AspectName = "GetPassengerCount";
            this.MPColumnPassengers.DisplayIndex = 5;
            this.MPColumnPassengers.Groupable = false;
            this.MPColumnPassengers.IsVisible = false;
            this.MPColumnPassengers.Text = "Pax";
            this.MPColumnPassengers.ToolTipText = "Passenger capacity";
            this.MPColumnPassengers.Width = 80;
            // 
            // MPColumnTotalCapacity
            // 
            this.MPColumnTotalCapacity.AspectName = "GetTotalCapacity";
            this.MPColumnTotalCapacity.DisplayIndex = 6;
            this.MPColumnTotalCapacity.IsVisible = false;
            this.MPColumnTotalCapacity.Text = "Total";
            this.MPColumnTotalCapacity.ToolTipText = "Total carrying capacity";
            // 
            // MPColumnCargo
            // 
            this.MPColumnCargo.AspectName = "CargoSpace";
            this.MPColumnCargo.DisplayIndex = 7;
            this.MPColumnCargo.IsVisible = false;
            this.MPColumnCargo.Text = "Cargo";
            this.MPColumnCargo.ToolTipText = "ACE3 cargo capacity";
            this.MPColumnCargo.Width = 50;
            // 
            // MPColumnCost
            // 
            this.MPColumnCost.AspectName = "Cost";
            this.MPColumnCost.AspectToStringFormat = "{0} pts";
            this.MPColumnCost.DisplayIndex = 8;
            this.MPColumnCost.Groupable = false;
            this.MPColumnCost.IsVisible = false;
            this.MPColumnCost.MinimumWidth = 50;
            this.MPColumnCost.Text = "Cost";
            this.MPColumnCost.ToolTipText = "Motorpool points cost";
            this.MPColumnCost.Width = 51;
            // 
            // MPColumnCanFloat
            // 
            this.MPColumnCanFloat.AspectName = "Floats";
            this.MPColumnCanFloat.DisplayIndex = 9;
            this.MPColumnCanFloat.IsVisible = false;
            this.MPColumnCanFloat.Text = "Floats";
            this.MPColumnCanFloat.ToolTipText = "Can this vehicle float on water?";
            this.MPColumnCanFloat.Width = 41;
            // 
            // MPColumnIsOutpost
            // 
            this.MPColumnIsOutpost.AspectName = "Outpost";
            this.MPColumnIsOutpost.DisplayIndex = 10;
            this.MPColumnIsOutpost.IsVisible = false;
            this.MPColumnIsOutpost.Text = "COP";
            this.MPColumnIsOutpost.ToolTipText = "Can this vehicle be used as a mobile COP?";
            // 
            // MPColumnMaximumLoad
            // 
            this.MPColumnMaximumLoad.AspectName = "MaxmiumLoad";
            this.MPColumnMaximumLoad.DisplayIndex = 11;
            this.MPColumnMaximumLoad.IsVisible = false;
            this.MPColumnMaximumLoad.Text = "MaximumLoad";
            this.MPColumnMaximumLoad.Width = 90;
            // 
            // MPColumnTransportAmmo
            // 
            this.MPColumnTransportAmmo.AspectName = "TransportAmmo";
            this.MPColumnTransportAmmo.DisplayIndex = 12;
            this.MPColumnTransportAmmo.IsVisible = false;
            this.MPColumnTransportAmmo.Text = "TransportAmmo";
            this.MPColumnTransportAmmo.Width = 90;
            // 
            // MPColumnTransportFuel
            // 
            this.MPColumnTransportFuel.AspectName = "TransportFuel";
            this.MPColumnTransportFuel.DisplayIndex = 13;
            this.MPColumnTransportFuel.IsVisible = false;
            this.MPColumnTransportFuel.Text = "TransportFuel";
            this.MPColumnTransportFuel.Width = 90;
            // 
            // TabPageLoadouts
            // 
            this.TabPageLoadouts.Controls.Add(this.TableLayoutPanelRoleInformation);
            this.TabPageLoadouts.Controls.Add(this.TabControlRoleInfo);
            this.TabPageLoadouts.Controls.Add(this.TreeListViewRoles);
            this.TabPageLoadouts.Location = new System.Drawing.Point(4, 22);
            this.TabPageLoadouts.Name = "TabPageLoadouts";
            this.TabPageLoadouts.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageLoadouts.Size = new System.Drawing.Size(765, 617);
            this.TabPageLoadouts.TabIndex = 1;
            this.TabPageLoadouts.Text = "Loadouts";
            this.TabPageLoadouts.UseVisualStyleBackColor = true;
            // 
            // TableLayoutPanelRoleInformation
            // 
            this.TableLayoutPanelRoleInformation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TableLayoutPanelRoleInformation.ColumnCount = 2;
            this.TableLayoutPanelRoleInformation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanelRoleInformation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanelRoleInformation.Controls.Add(this.GroupBoxRoleGeneralInformation, 0, 0);
            this.TableLayoutPanelRoleInformation.Controls.Add(this.GroupBoxSpecialRoleInformation, 1, 0);
            this.TableLayoutPanelRoleInformation.Location = new System.Drawing.Point(237, 0);
            this.TableLayoutPanelRoleInformation.Margin = new System.Windows.Forms.Padding(0);
            this.TableLayoutPanelRoleInformation.Name = "TableLayoutPanelRoleInformation";
            this.TableLayoutPanelRoleInformation.RowCount = 1;
            this.TableLayoutPanelRoleInformation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanelRoleInformation.Size = new System.Drawing.Size(505, 164);
            this.TableLayoutPanelRoleInformation.TabIndex = 80;
            // 
            // GroupBoxRoleGeneralInformation
            // 
            this.GroupBoxRoleGeneralInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxRoleGeneralInformation.Controls.Add(this.TextBoxFactionGroup);
            this.GroupBoxRoleGeneralInformation.Controls.Add(this.LabelFactionGroup);
            this.GroupBoxRoleGeneralInformation.Controls.Add(this.LabelElementRole);
            this.GroupBoxRoleGeneralInformation.Controls.Add(this.TextBoxFaction);
            this.GroupBoxRoleGeneralInformation.Controls.Add(this.LabelStaminaFactor);
            this.GroupBoxRoleGeneralInformation.Controls.Add(this.TextBoxElementRole);
            this.GroupBoxRoleGeneralInformation.Controls.Add(this.TextBoxStaminaFactor);
            this.GroupBoxRoleGeneralInformation.Controls.Add(this.LabelFaction);
            this.GroupBoxRoleGeneralInformation.Controls.Add(this.TextBoxCallsign);
            this.GroupBoxRoleGeneralInformation.Controls.Add(this.LabelCallsign);
            this.GroupBoxRoleGeneralInformation.Location = new System.Drawing.Point(3, 3);
            this.GroupBoxRoleGeneralInformation.Name = "GroupBoxRoleGeneralInformation";
            this.GroupBoxRoleGeneralInformation.Size = new System.Drawing.Size(246, 158);
            this.GroupBoxRoleGeneralInformation.TabIndex = 78;
            this.GroupBoxRoleGeneralInformation.TabStop = false;
            this.GroupBoxRoleGeneralInformation.Text = "General Role Information";
            // 
            // TextBoxFactionGroup
            // 
            this.TextBoxFactionGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxFactionGroup.Location = new System.Drawing.Point(100, 19);
            this.TextBoxFactionGroup.Name = "TextBoxFactionGroup";
            this.TextBoxFactionGroup.ReadOnly = true;
            this.TextBoxFactionGroup.Size = new System.Drawing.Size(140, 20);
            this.TextBoxFactionGroup.TabIndex = 24;
            // 
            // LabelFactionGroup
            // 
            this.LabelFactionGroup.Location = new System.Drawing.Point(4, 22);
            this.LabelFactionGroup.Name = "LabelFactionGroup";
            this.LabelFactionGroup.Size = new System.Drawing.Size(90, 13);
            this.LabelFactionGroup.TabIndex = 23;
            this.LabelFactionGroup.Text = "Faction Group";
            this.LabelFactionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelElementRole
            // 
            this.LabelElementRole.Location = new System.Drawing.Point(4, 74);
            this.LabelElementRole.Name = "LabelElementRole";
            this.LabelElementRole.Size = new System.Drawing.Size(90, 13);
            this.LabelElementRole.TabIndex = 60;
            this.LabelElementRole.Text = "Element && Role";
            this.LabelElementRole.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxFaction
            // 
            this.TextBoxFaction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxFaction.Location = new System.Drawing.Point(100, 45);
            this.TextBoxFaction.Name = "TextBoxFaction";
            this.TextBoxFaction.ReadOnly = true;
            this.TextBoxFaction.Size = new System.Drawing.Size(140, 20);
            this.TextBoxFaction.TabIndex = 26;
            // 
            // LabelStaminaFactor
            // 
            this.LabelStaminaFactor.Location = new System.Drawing.Point(4, 126);
            this.LabelStaminaFactor.Name = "LabelStaminaFactor";
            this.LabelStaminaFactor.Size = new System.Drawing.Size(90, 13);
            this.LabelStaminaFactor.TabIndex = 37;
            this.LabelStaminaFactor.Text = "Stamina Factor";
            this.LabelStaminaFactor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxElementRole
            // 
            this.TextBoxElementRole.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxElementRole.Location = new System.Drawing.Point(100, 71);
            this.TextBoxElementRole.Name = "TextBoxElementRole";
            this.TextBoxElementRole.ReadOnly = true;
            this.TextBoxElementRole.Size = new System.Drawing.Size(140, 20);
            this.TextBoxElementRole.TabIndex = 59;
            // 
            // TextBoxStaminaFactor
            // 
            this.TextBoxStaminaFactor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxStaminaFactor.Location = new System.Drawing.Point(100, 123);
            this.TextBoxStaminaFactor.Name = "TextBoxStaminaFactor";
            this.TextBoxStaminaFactor.ReadOnly = true;
            this.TextBoxStaminaFactor.Size = new System.Drawing.Size(140, 20);
            this.TextBoxStaminaFactor.TabIndex = 38;
            this.ToolTipInfo.SetToolTip(this.TextBoxStaminaFactor, "Represents physical training beyond a regular soldier.\r\nHigher values will slow s" +
        "tamina drain in all situations.");
            // 
            // LabelFaction
            // 
            this.LabelFaction.Location = new System.Drawing.Point(4, 48);
            this.LabelFaction.Name = "LabelFaction";
            this.LabelFaction.Size = new System.Drawing.Size(90, 13);
            this.LabelFaction.TabIndex = 25;
            this.LabelFaction.Text = "Subfaction";
            this.LabelFaction.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxCallsign
            // 
            this.TextBoxCallsign.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxCallsign.Location = new System.Drawing.Point(100, 97);
            this.TextBoxCallsign.Name = "TextBoxCallsign";
            this.TextBoxCallsign.ReadOnly = true;
            this.TextBoxCallsign.Size = new System.Drawing.Size(140, 20);
            this.TextBoxCallsign.TabIndex = 28;
            // 
            // LabelCallsign
            // 
            this.LabelCallsign.Location = new System.Drawing.Point(4, 100);
            this.LabelCallsign.Name = "LabelCallsign";
            this.LabelCallsign.Size = new System.Drawing.Size(90, 13);
            this.LabelCallsign.TabIndex = 27;
            this.LabelCallsign.Text = "Callsign";
            this.LabelCallsign.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GroupBoxSpecialRoleInformation
            // 
            this.GroupBoxSpecialRoleInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxSpecialRoleInformation.Controls.Add(this.TextBoxMedic);
            this.GroupBoxSpecialRoleInformation.Controls.Add(this.LabelMedic);
            this.GroupBoxSpecialRoleInformation.Controls.Add(this.TextBoxEngineer);
            this.GroupBoxSpecialRoleInformation.Controls.Add(this.LabelInterpreter);
            this.GroupBoxSpecialRoleInformation.Controls.Add(this.LabelEngineer);
            this.GroupBoxSpecialRoleInformation.Controls.Add(this.TextBoxInterpreter);
            this.GroupBoxSpecialRoleInformation.Controls.Add(this.TextBoxEOD);
            this.GroupBoxSpecialRoleInformation.Controls.Add(this.LabelGForce);
            this.GroupBoxSpecialRoleInformation.Controls.Add(this.LabelEOD);
            this.GroupBoxSpecialRoleInformation.Controls.Add(this.TextBoxGForce);
            this.GroupBoxSpecialRoleInformation.Location = new System.Drawing.Point(255, 3);
            this.GroupBoxSpecialRoleInformation.Name = "GroupBoxSpecialRoleInformation";
            this.GroupBoxSpecialRoleInformation.Size = new System.Drawing.Size(247, 158);
            this.GroupBoxSpecialRoleInformation.TabIndex = 79;
            this.GroupBoxSpecialRoleInformation.TabStop = false;
            this.GroupBoxSpecialRoleInformation.Text = "Special Role Stats";
            // 
            // TextBoxMedic
            // 
            this.TextBoxMedic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxMedic.Location = new System.Drawing.Point(102, 19);
            this.TextBoxMedic.Name = "TextBoxMedic";
            this.TextBoxMedic.ReadOnly = true;
            this.TextBoxMedic.Size = new System.Drawing.Size(139, 20);
            this.TextBoxMedic.TabIndex = 30;
            this.ToolTipInfo.SetToolTip(this.TextBoxMedic, "Only medics can use surgery kits and administer IV bags.");
            // 
            // LabelMedic
            // 
            this.LabelMedic.Location = new System.Drawing.Point(6, 22);
            this.LabelMedic.Name = "LabelMedic";
            this.LabelMedic.Size = new System.Drawing.Size(90, 13);
            this.LabelMedic.TabIndex = 29;
            this.LabelMedic.Text = "Is Medic";
            this.LabelMedic.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxEngineer
            // 
            this.TextBoxEngineer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxEngineer.Location = new System.Drawing.Point(102, 45);
            this.TextBoxEngineer.Name = "TextBoxEngineer";
            this.TextBoxEngineer.ReadOnly = true;
            this.TextBoxEngineer.Size = new System.Drawing.Size(139, 20);
            this.TextBoxEngineer.TabIndex = 32;
            this.ToolTipInfo.SetToolTip(this.TextBoxEngineer, "Only engineers can use tool kits to repair vehicles.");
            // 
            // LabelInterpreter
            // 
            this.LabelInterpreter.Location = new System.Drawing.Point(6, 126);
            this.LabelInterpreter.Name = "LabelInterpreter";
            this.LabelInterpreter.Size = new System.Drawing.Size(90, 13);
            this.LabelInterpreter.TabIndex = 39;
            this.LabelInterpreter.Text = "Is Interpreter";
            this.LabelInterpreter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelEngineer
            // 
            this.LabelEngineer.Location = new System.Drawing.Point(6, 48);
            this.LabelEngineer.Name = "LabelEngineer";
            this.LabelEngineer.Size = new System.Drawing.Size(90, 13);
            this.LabelEngineer.TabIndex = 31;
            this.LabelEngineer.Text = "Is Engineer";
            this.LabelEngineer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxInterpreter
            // 
            this.TextBoxInterpreter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxInterpreter.Location = new System.Drawing.Point(102, 123);
            this.TextBoxInterpreter.Name = "TextBoxInterpreter";
            this.TextBoxInterpreter.ReadOnly = true;
            this.TextBoxInterpreter.Size = new System.Drawing.Size(139, 20);
            this.TextBoxInterpreter.TabIndex = 40;
            this.ToolTipInfo.SetToolTip(this.TextBoxInterpreter, "Interpreter units speak more than one language.\r\nAllows this unit to speak in oth" +
        "er ACRE languages.");
            // 
            // TextBoxEOD
            // 
            this.TextBoxEOD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxEOD.Location = new System.Drawing.Point(102, 71);
            this.TextBoxEOD.Name = "TextBoxEOD";
            this.TextBoxEOD.ReadOnly = true;
            this.TextBoxEOD.Size = new System.Drawing.Size(139, 20);
            this.TextBoxEOD.TabIndex = 34;
            this.ToolTipInfo.SetToolTip(this.TextBoxEOD, "Only EOD units are trained to defuse explosives.");
            // 
            // LabelGForce
            // 
            this.LabelGForce.Location = new System.Drawing.Point(6, 100);
            this.LabelGForce.Name = "LabelGForce";
            this.LabelGForce.Size = new System.Drawing.Size(90, 13);
            this.LabelGForce.TabIndex = 35;
            this.LabelGForce.Text = "G-Force Coef";
            this.LabelGForce.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelEOD
            // 
            this.LabelEOD.Location = new System.Drawing.Point(6, 74);
            this.LabelEOD.Name = "LabelEOD";
            this.LabelEOD.Size = new System.Drawing.Size(90, 13);
            this.LabelEOD.TabIndex = 33;
            this.LabelEOD.Text = "Is EOD";
            this.LabelEOD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxGForce
            // 
            this.TextBoxGForce.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxGForce.Location = new System.Drawing.Point(102, 97);
            this.TextBoxGForce.Name = "TextBoxGForce";
            this.TextBoxGForce.ReadOnly = true;
            this.TextBoxGForce.Size = new System.Drawing.Size(139, 20);
            this.TextBoxGForce.TabIndex = 36;
            this.ToolTipInfo.SetToolTip(this.TextBoxGForce, "Represents pilot High-G training.\r\nThe smaller the value, the harder it is to G-L" +
        "OC.");
            // 
            // TabControlRoleInfo
            // 
            this.TabControlRoleInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControlRoleInfo.Controls.Add(this.TabPageInventory);
            this.TabControlRoleInfo.Controls.Add(this.TabPageAccessories);
            this.TabControlRoleInfo.Location = new System.Drawing.Point(233, 163);
            this.TabControlRoleInfo.Margin = new System.Windows.Forms.Padding(0);
            this.TabControlRoleInfo.Name = "TabControlRoleInfo";
            this.TabControlRoleInfo.SelectedIndex = 0;
            this.TabControlRoleInfo.Size = new System.Drawing.Size(509, 414);
            this.TabControlRoleInfo.TabIndex = 77;
            // 
            // TabPageInventory
            // 
            this.TabPageInventory.Controls.Add(this.TableLayoutPanelInventory);
            this.TabPageInventory.Location = new System.Drawing.Point(4, 22);
            this.TabPageInventory.Name = "TabPageInventory";
            this.TabPageInventory.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageInventory.Size = new System.Drawing.Size(501, 388);
            this.TabPageInventory.TabIndex = 0;
            this.TabPageInventory.Text = "Inventory Contents";
            this.TabPageInventory.UseVisualStyleBackColor = true;
            // 
            // TableLayoutPanelInventory
            // 
            this.TableLayoutPanelInventory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TableLayoutPanelInventory.ColumnCount = 3;
            this.TableLayoutPanelInventory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TableLayoutPanelInventory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TableLayoutPanelInventory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TableLayoutPanelInventory.Controls.Add(this.GroupBoxVestLoad, 1, 2);
            this.TableLayoutPanelInventory.Controls.Add(this.GroupBoxUniformLoad, 0, 2);
            this.TableLayoutPanelInventory.Controls.Add(this.GroupBoxBackpackLoad, 2, 2);
            this.TableLayoutPanelInventory.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanelInventory.Margin = new System.Windows.Forms.Padding(0);
            this.TableLayoutPanelInventory.Name = "TableLayoutPanelInventory";
            this.TableLayoutPanelInventory.RowCount = 1;
            this.TableLayoutPanelInventory.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanelInventory.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanelInventory.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanelInventory.Size = new System.Drawing.Size(501, 388);
            this.TableLayoutPanelInventory.TabIndex = 78;
            // 
            // GroupBoxVestLoad
            // 
            this.GroupBoxVestLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxVestLoad.Controls.Add(this.TextBoxVest);
            this.GroupBoxVestLoad.Controls.Add(this.ListBoxVest);
            this.GroupBoxVestLoad.Location = new System.Drawing.Point(170, 3);
            this.GroupBoxVestLoad.Name = "GroupBoxVestLoad";
            this.GroupBoxVestLoad.Size = new System.Drawing.Size(161, 394);
            this.GroupBoxVestLoad.TabIndex = 78;
            this.GroupBoxVestLoad.TabStop = false;
            this.GroupBoxVestLoad.Text = "Vest:";
            // 
            // TextBoxVest
            // 
            this.TextBoxVest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxVest.Location = new System.Drawing.Point(6, 16);
            this.TextBoxVest.Name = "TextBoxVest";
            this.TextBoxVest.ReadOnly = true;
            this.TextBoxVest.Size = new System.Drawing.Size(149, 20);
            this.TextBoxVest.TabIndex = 68;
            // 
            // ListBoxVest
            // 
            this.ListBoxVest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListBoxVest.FormattingEnabled = true;
            this.ListBoxVest.Location = new System.Drawing.Point(6, 42);
            this.ListBoxVest.Name = "ListBoxVest";
            this.ListBoxVest.Size = new System.Drawing.Size(149, 342);
            this.ListBoxVest.TabIndex = 72;
            // 
            // GroupBoxUniformLoad
            // 
            this.GroupBoxUniformLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxUniformLoad.Controls.Add(this.TextBoxUniform);
            this.GroupBoxUniformLoad.Controls.Add(this.ListBoxUniform);
            this.GroupBoxUniformLoad.Location = new System.Drawing.Point(3, 3);
            this.GroupBoxUniformLoad.Name = "GroupBoxUniformLoad";
            this.GroupBoxUniformLoad.Size = new System.Drawing.Size(161, 394);
            this.GroupBoxUniformLoad.TabIndex = 77;
            this.GroupBoxUniformLoad.TabStop = false;
            this.GroupBoxUniformLoad.Text = "Uniform:";
            // 
            // TextBoxUniform
            // 
            this.TextBoxUniform.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxUniform.Location = new System.Drawing.Point(6, 15);
            this.TextBoxUniform.Name = "TextBoxUniform";
            this.TextBoxUniform.ReadOnly = true;
            this.TextBoxUniform.Size = new System.Drawing.Size(149, 20);
            this.TextBoxUniform.TabIndex = 66;
            // 
            // ListBoxUniform
            // 
            this.ListBoxUniform.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListBoxUniform.FormattingEnabled = true;
            this.ListBoxUniform.Location = new System.Drawing.Point(6, 41);
            this.ListBoxUniform.Name = "ListBoxUniform";
            this.ListBoxUniform.Size = new System.Drawing.Size(149, 342);
            this.ListBoxUniform.TabIndex = 71;
            // 
            // GroupBoxBackpackLoad
            // 
            this.GroupBoxBackpackLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxBackpackLoad.Controls.Add(this.ListBoxBackpack);
            this.GroupBoxBackpackLoad.Controls.Add(this.TextBoxBackpack);
            this.GroupBoxBackpackLoad.Location = new System.Drawing.Point(337, 3);
            this.GroupBoxBackpackLoad.Name = "GroupBoxBackpackLoad";
            this.GroupBoxBackpackLoad.Size = new System.Drawing.Size(161, 394);
            this.GroupBoxBackpackLoad.TabIndex = 79;
            this.GroupBoxBackpackLoad.TabStop = false;
            this.GroupBoxBackpackLoad.Text = "Backpack:";
            // 
            // ListBoxBackpack
            // 
            this.ListBoxBackpack.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListBoxBackpack.FormattingEnabled = true;
            this.ListBoxBackpack.Location = new System.Drawing.Point(6, 42);
            this.ListBoxBackpack.Name = "ListBoxBackpack";
            this.ListBoxBackpack.Size = new System.Drawing.Size(149, 342);
            this.ListBoxBackpack.TabIndex = 73;
            // 
            // TextBoxBackpack
            // 
            this.TextBoxBackpack.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxBackpack.Location = new System.Drawing.Point(6, 19);
            this.TextBoxBackpack.Name = "TextBoxBackpack";
            this.TextBoxBackpack.ReadOnly = true;
            this.TextBoxBackpack.Size = new System.Drawing.Size(149, 20);
            this.TextBoxBackpack.TabIndex = 70;
            // 
            // TabPageAccessories
            // 
            this.TabPageAccessories.Controls.Add(this.tableLayoutPanel1);
            this.TabPageAccessories.Location = new System.Drawing.Point(4, 22);
            this.TabPageAccessories.Name = "TabPageAccessories";
            this.TabPageAccessories.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageAccessories.Size = new System.Drawing.Size(501, 388);
            this.TabPageAccessories.TabIndex = 1;
            this.TabPageAccessories.Text = "Weapons & Accessories";
            this.TabPageAccessories.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.GroupBoxMiscGear, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.GroupBoxPrimary, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.GroupBoxHeadgear, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(501, 388);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // GroupBoxMiscGear
            // 
            this.GroupBoxMiscGear.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxMiscGear.Controls.Add(this.TextBoxSecondary);
            this.GroupBoxMiscGear.Controls.Add(this.LabelUAVTerminal);
            this.GroupBoxMiscGear.Controls.Add(this.LabelBinocular);
            this.GroupBoxMiscGear.Controls.Add(this.LabelBFT);
            this.GroupBoxMiscGear.Controls.Add(this.TextBoxBinocular);
            this.GroupBoxMiscGear.Controls.Add(this.TextBoxBFT);
            this.GroupBoxMiscGear.Controls.Add(this.LabelTertiary);
            this.GroupBoxMiscGear.Controls.Add(this.TextBoxUAVTerminal);
            this.GroupBoxMiscGear.Controls.Add(this.TextBoxTertiary);
            this.GroupBoxMiscGear.Controls.Add(this.TextBoxWatch);
            this.GroupBoxMiscGear.Controls.Add(this.TextBoxMap);
            this.GroupBoxMiscGear.Controls.Add(this.LabelWatch);
            this.GroupBoxMiscGear.Controls.Add(this.LabelSecondary);
            this.GroupBoxMiscGear.Controls.Add(this.TextBoxCompass);
            this.GroupBoxMiscGear.Controls.Add(this.LabelMap);
            this.GroupBoxMiscGear.Controls.Add(this.LabelCompass);
            this.GroupBoxMiscGear.Controls.Add(this.LabelGPS);
            this.GroupBoxMiscGear.Controls.Add(this.TextBoxGPS);
            this.GroupBoxMiscGear.Location = new System.Drawing.Point(337, 3);
            this.GroupBoxMiscGear.Name = "GroupBoxMiscGear";
            this.GroupBoxMiscGear.Size = new System.Drawing.Size(161, 382);
            this.GroupBoxMiscGear.TabIndex = 80;
            this.GroupBoxMiscGear.TabStop = false;
            this.GroupBoxMiscGear.Text = "Other Role Equipment";
            // 
            // TextBoxSecondary
            // 
            this.TextBoxSecondary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxSecondary.Location = new System.Drawing.Point(65, 19);
            this.TextBoxSecondary.Name = "TextBoxSecondary";
            this.TextBoxSecondary.ReadOnly = true;
            this.TextBoxSecondary.Size = new System.Drawing.Size(90, 20);
            this.TextBoxSecondary.TabIndex = 44;
            // 
            // LabelUAVTerminal
            // 
            this.LabelUAVTerminal.Location = new System.Drawing.Point(-1, 178);
            this.LabelUAVTerminal.Name = "LabelUAVTerminal";
            this.LabelUAVTerminal.Size = new System.Drawing.Size(60, 13);
            this.LabelUAVTerminal.TabIndex = 61;
            this.LabelUAVTerminal.Text = "UAV";
            this.LabelUAVTerminal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelBinocular
            // 
            this.LabelBinocular.Location = new System.Drawing.Point(-1, 74);
            this.LabelBinocular.Name = "LabelBinocular";
            this.LabelBinocular.Size = new System.Drawing.Size(60, 13);
            this.LabelBinocular.TabIndex = 47;
            this.LabelBinocular.Text = "Binoculars";
            this.LabelBinocular.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelBFT
            // 
            this.LabelBFT.Location = new System.Drawing.Point(-1, 230);
            this.LabelBFT.Name = "LabelBFT";
            this.LabelBFT.Size = new System.Drawing.Size(60, 13);
            this.LabelBFT.TabIndex = 57;
            this.LabelBFT.Text = "BFT";
            this.LabelBFT.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxBinocular
            // 
            this.TextBoxBinocular.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxBinocular.Location = new System.Drawing.Point(66, 71);
            this.TextBoxBinocular.Name = "TextBoxBinocular";
            this.TextBoxBinocular.ReadOnly = true;
            this.TextBoxBinocular.Size = new System.Drawing.Size(90, 20);
            this.TextBoxBinocular.TabIndex = 48;
            // 
            // TextBoxBFT
            // 
            this.TextBoxBFT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxBFT.Location = new System.Drawing.Point(65, 227);
            this.TextBoxBFT.Name = "TextBoxBFT";
            this.TextBoxBFT.ReadOnly = true;
            this.TextBoxBFT.Size = new System.Drawing.Size(90, 20);
            this.TextBoxBFT.TabIndex = 58;
            this.ToolTipInfo.SetToolTip(this.TextBoxBFT, "Only units with a DAGR or microDAGR have BFT.\r\nUnits that pick up either DAGR wil" +
        "l gain BFT.");
            // 
            // LabelTertiary
            // 
            this.LabelTertiary.Location = new System.Drawing.Point(-1, 48);
            this.LabelTertiary.Name = "LabelTertiary";
            this.LabelTertiary.Size = new System.Drawing.Size(60, 13);
            this.LabelTertiary.TabIndex = 45;
            this.LabelTertiary.Text = "Tertiary";
            this.LabelTertiary.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxUAVTerminal
            // 
            this.TextBoxUAVTerminal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxUAVTerminal.Location = new System.Drawing.Point(66, 175);
            this.TextBoxUAVTerminal.Name = "TextBoxUAVTerminal";
            this.TextBoxUAVTerminal.ReadOnly = true;
            this.TextBoxUAVTerminal.Size = new System.Drawing.Size(90, 20);
            this.TextBoxUAVTerminal.TabIndex = 62;
            this.ToolTipInfo.SetToolTip(this.TextBoxUAVTerminal, "Used to remotely connect to unmanned vehicles.");
            // 
            // TextBoxTertiary
            // 
            this.TextBoxTertiary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxTertiary.Location = new System.Drawing.Point(65, 45);
            this.TextBoxTertiary.Name = "TextBoxTertiary";
            this.TextBoxTertiary.ReadOnly = true;
            this.TextBoxTertiary.Size = new System.Drawing.Size(90, 20);
            this.TextBoxTertiary.TabIndex = 46;
            // 
            // TextBoxWatch
            // 
            this.TextBoxWatch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxWatch.Location = new System.Drawing.Point(66, 123);
            this.TextBoxWatch.Name = "TextBoxWatch";
            this.TextBoxWatch.ReadOnly = true;
            this.TextBoxWatch.Size = new System.Drawing.Size(90, 20);
            this.TextBoxWatch.TabIndex = 54;
            // 
            // TextBoxMap
            // 
            this.TextBoxMap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxMap.Location = new System.Drawing.Point(66, 149);
            this.TextBoxMap.Name = "TextBoxMap";
            this.TextBoxMap.ReadOnly = true;
            this.TextBoxMap.Size = new System.Drawing.Size(90, 20);
            this.TextBoxMap.TabIndex = 50;
            // 
            // LabelWatch
            // 
            this.LabelWatch.Location = new System.Drawing.Point(-1, 126);
            this.LabelWatch.Name = "LabelWatch";
            this.LabelWatch.Size = new System.Drawing.Size(60, 13);
            this.LabelWatch.TabIndex = 53;
            this.LabelWatch.Text = "Wrist";
            this.LabelWatch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelSecondary
            // 
            this.LabelSecondary.Location = new System.Drawing.Point(-1, 22);
            this.LabelSecondary.Name = "LabelSecondary";
            this.LabelSecondary.Size = new System.Drawing.Size(60, 13);
            this.LabelSecondary.TabIndex = 43;
            this.LabelSecondary.Text = "Secondary";
            this.LabelSecondary.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxCompass
            // 
            this.TextBoxCompass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxCompass.Location = new System.Drawing.Point(65, 97);
            this.TextBoxCompass.Name = "TextBoxCompass";
            this.TextBoxCompass.ReadOnly = true;
            this.TextBoxCompass.Size = new System.Drawing.Size(90, 20);
            this.TextBoxCompass.TabIndex = 52;
            // 
            // LabelMap
            // 
            this.LabelMap.Location = new System.Drawing.Point(-1, 152);
            this.LabelMap.Name = "LabelMap";
            this.LabelMap.Size = new System.Drawing.Size(60, 13);
            this.LabelMap.TabIndex = 49;
            this.LabelMap.Text = "Map";
            this.LabelMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelCompass
            // 
            this.LabelCompass.Location = new System.Drawing.Point(-1, 100);
            this.LabelCompass.Name = "LabelCompass";
            this.LabelCompass.Size = new System.Drawing.Size(60, 13);
            this.LabelCompass.TabIndex = 51;
            this.LabelCompass.Text = "Compass";
            this.LabelCompass.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelGPS
            // 
            this.LabelGPS.Location = new System.Drawing.Point(-1, 204);
            this.LabelGPS.Name = "LabelGPS";
            this.LabelGPS.Size = new System.Drawing.Size(60, 13);
            this.LabelGPS.TabIndex = 55;
            this.LabelGPS.Text = "GPS";
            this.LabelGPS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxGPS
            // 
            this.TextBoxGPS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxGPS.Location = new System.Drawing.Point(65, 201);
            this.TextBoxGPS.Name = "TextBoxGPS";
            this.TextBoxGPS.ReadOnly = true;
            this.TextBoxGPS.Size = new System.Drawing.Size(90, 20);
            this.TextBoxGPS.TabIndex = 56;
            this.ToolTipInfo.SetToolTip(this.TextBoxGPS, "GPS does *not* give you BFT.");
            // 
            // GroupBoxPrimary
            // 
            this.GroupBoxPrimary.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxPrimary.Controls.Add(this.TextBoxPrimary);
            this.GroupBoxPrimary.Controls.Add(this.ListBoxPrimaryAccessories);
            this.GroupBoxPrimary.Location = new System.Drawing.Point(3, 3);
            this.GroupBoxPrimary.Name = "GroupBoxPrimary";
            this.GroupBoxPrimary.Size = new System.Drawing.Size(161, 382);
            this.GroupBoxPrimary.TabIndex = 78;
            this.GroupBoxPrimary.TabStop = false;
            this.GroupBoxPrimary.Text = "Primary && Attachments";
            // 
            // TextBoxPrimary
            // 
            this.TextBoxPrimary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxPrimary.Location = new System.Drawing.Point(6, 19);
            this.TextBoxPrimary.Name = "TextBoxPrimary";
            this.TextBoxPrimary.ReadOnly = true;
            this.TextBoxPrimary.Size = new System.Drawing.Size(149, 20);
            this.TextBoxPrimary.TabIndex = 42;
            // 
            // ListBoxPrimaryAccessories
            // 
            this.ListBoxPrimaryAccessories.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListBoxPrimaryAccessories.FormattingEnabled = true;
            this.ListBoxPrimaryAccessories.Location = new System.Drawing.Point(6, 45);
            this.ListBoxPrimaryAccessories.Name = "ListBoxPrimaryAccessories";
            this.ListBoxPrimaryAccessories.Size = new System.Drawing.Size(149, 329);
            this.ListBoxPrimaryAccessories.TabIndex = 72;
            // 
            // GroupBoxHeadgear
            // 
            this.GroupBoxHeadgear.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxHeadgear.Controls.Add(this.ListBoxHeadgearAccessories);
            this.GroupBoxHeadgear.Controls.Add(this.TextBoxHeadgear);
            this.GroupBoxHeadgear.Location = new System.Drawing.Point(170, 3);
            this.GroupBoxHeadgear.Name = "GroupBoxHeadgear";
            this.GroupBoxHeadgear.Size = new System.Drawing.Size(161, 382);
            this.GroupBoxHeadgear.TabIndex = 79;
            this.GroupBoxHeadgear.TabStop = false;
            this.GroupBoxHeadgear.Text = "Headgear && NVGs";
            // 
            // ListBoxHeadgearAccessories
            // 
            this.ListBoxHeadgearAccessories.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListBoxHeadgearAccessories.FormattingEnabled = true;
            this.ListBoxHeadgearAccessories.Location = new System.Drawing.Point(6, 45);
            this.ListBoxHeadgearAccessories.Name = "ListBoxHeadgearAccessories";
            this.ListBoxHeadgearAccessories.Size = new System.Drawing.Size(149, 329);
            this.ListBoxHeadgearAccessories.TabIndex = 73;
            // 
            // TextBoxHeadgear
            // 
            this.TextBoxHeadgear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxHeadgear.Location = new System.Drawing.Point(6, 19);
            this.TextBoxHeadgear.Name = "TextBoxHeadgear";
            this.TextBoxHeadgear.ReadOnly = true;
            this.TextBoxHeadgear.Size = new System.Drawing.Size(149, 20);
            this.TextBoxHeadgear.TabIndex = 64;
            // 
            // TreeListViewRoles
            // 
            this.TreeListViewRoles.AllColumns.Add(this.RoleColumnName);
            this.TreeListViewRoles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TreeListViewRoles.CellEditUseWholeCell = false;
            this.TreeListViewRoles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.RoleColumnName});
            this.TreeListViewRoles.Cursor = System.Windows.Forms.Cursors.Default;
            this.TreeListViewRoles.HideSelection = false;
            this.TreeListViewRoles.Location = new System.Drawing.Point(0, 0);
            this.TreeListViewRoles.Margin = new System.Windows.Forms.Padding(0);
            this.TreeListViewRoles.Name = "TreeListViewRoles";
            this.TreeListViewRoles.ShowGroups = false;
            this.TreeListViewRoles.Size = new System.Drawing.Size(233, 577);
            this.TreeListViewRoles.TabIndex = 0;
            this.TreeListViewRoles.UseCompatibleStateImageBehavior = false;
            this.TreeListViewRoles.UseFiltering = true;
            this.TreeListViewRoles.View = System.Windows.Forms.View.Details;
            this.TreeListViewRoles.VirtualMode = true;
            this.TreeListViewRoles.SelectedIndexChanged += new System.EventHandler(this.TreeListViewRoles_SelectedIndexChanged);
            // 
            // RoleColumnName
            // 
            this.RoleColumnName.AspectName = "Name";
            this.RoleColumnName.FillsFreeSpace = true;
            this.RoleColumnName.Text = "Name";
            this.RoleColumnName.Width = 125;
            // 
            // TabPageResupply
            // 
            this.TabPageResupply.Controls.Add(this.TreeListViewResupply);
            this.TabPageResupply.Location = new System.Drawing.Point(4, 22);
            this.TabPageResupply.Name = "TabPageResupply";
            this.TabPageResupply.Size = new System.Drawing.Size(765, 617);
            this.TabPageResupply.TabIndex = 2;
            this.TabPageResupply.Text = "Resupply";
            this.TabPageResupply.UseVisualStyleBackColor = true;
            // 
            // TreeListViewResupply
            // 
            this.TreeListViewResupply.AllColumns.Add(this.olvColumn1);
            this.TreeListViewResupply.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TreeListViewResupply.CellEditUseWholeCell = false;
            this.TreeListViewResupply.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1});
            this.TreeListViewResupply.Cursor = System.Windows.Forms.Cursors.Default;
            this.TreeListViewResupply.HideSelection = false;
            this.TreeListViewResupply.Location = new System.Drawing.Point(0, 0);
            this.TreeListViewResupply.Margin = new System.Windows.Forms.Padding(0);
            this.TreeListViewResupply.Name = "TreeListViewResupply";
            this.TreeListViewResupply.ShowGroups = false;
            this.TreeListViewResupply.Size = new System.Drawing.Size(233, 577);
            this.TreeListViewResupply.TabIndex = 1;
            this.TreeListViewResupply.UseCompatibleStateImageBehavior = false;
            this.TreeListViewResupply.UseFiltering = true;
            this.TreeListViewResupply.View = System.Windows.Forms.View.Details;
            this.TreeListViewResupply.VirtualMode = true;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "ToString";
            this.olvColumn1.FillsFreeSpace = true;
            this.olvColumn1.Text = "Name";
            this.olvColumn1.Width = 125;
            // 
            // TreeListViewSides
            // 
            this.TreeListViewSides.AllColumns.Add(this.SideColumnName);
            this.TreeListViewSides.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TreeListViewSides.CellEditUseWholeCell = false;
            this.TreeListViewSides.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.SideColumnName});
            this.TreeListViewSides.Cursor = System.Windows.Forms.Cursors.Default;
            this.TreeListViewSides.HideSelection = false;
            this.TreeListViewSides.Location = new System.Drawing.Point(0, 29);
            this.TreeListViewSides.Margin = new System.Windows.Forms.Padding(0);
            this.TreeListViewSides.Name = "TreeListViewSides";
            this.TreeListViewSides.ShowGroups = false;
            this.TreeListViewSides.Size = new System.Drawing.Size(207, 643);
            this.TreeListViewSides.TabIndex = 33;
            this.TreeListViewSides.UseCompatibleStateImageBehavior = false;
            this.TreeListViewSides.View = System.Windows.Forms.View.Details;
            this.TreeListViewSides.VirtualMode = true;
            this.TreeListViewSides.SelectedIndexChanged += new System.EventHandler(this.TreeListViewSides_SelectedIndexChanged);
            // 
            // SideColumnName
            // 
            this.SideColumnName.AspectName = "ToString";
            this.SideColumnName.FillsFreeSpace = true;
            this.SideColumnName.Text = "Faction Name";
            this.SideColumnName.Width = 150;
            // 
            // ToolTipInfo
            // 
            this.ToolTipInfo.AutoPopDelay = 32767;
            this.ToolTipInfo.InitialDelay = 1;
            this.ToolTipInfo.ReshowDelay = 500;
            this.ToolTipInfo.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.ToolTipInfo.ToolTipTitle = "Info";
            // 
            // SplitContainerMPT
            // 
            this.SplitContainerMPT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SplitContainerMPT.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.SplitContainerMPT.Location = new System.Drawing.Point(0, 27);
            this.SplitContainerMPT.Name = "SplitContainerMPT";
            // 
            // SplitContainerMPT.Panel1
            // 
            this.SplitContainerMPT.Panel1.Controls.Add(this.LabelSubFaction);
            this.SplitContainerMPT.Panel1.Controls.Add(this.TreeListViewSides);
            this.SplitContainerMPT.Panel1MinSize = 200;
            // 
            // SplitContainerMPT.Panel2
            // 
            this.SplitContainerMPT.Panel2.Controls.Add(this.ButtonToggleView);
            this.SplitContainerMPT.Panel2.Controls.Add(this.TabControlFactionInfo);
            this.SplitContainerMPT.Panel2.Controls.Add(this.TextBoxSearch);
            this.SplitContainerMPT.Panel2.Controls.Add(this.TextBoxClassName);
            this.SplitContainerMPT.Panel2.Controls.Add(this.LabelClassName);
            this.SplitContainerMPT.Panel2MinSize = 700;
            this.SplitContainerMPT.Size = new System.Drawing.Size(984, 672);
            this.SplitContainerMPT.SplitterDistance = 207;
            this.SplitContainerMPT.TabIndex = 34;
            // 
            // LabelSubFaction
            // 
            this.LabelSubFaction.AutoSize = true;
            this.LabelSubFaction.Location = new System.Drawing.Point(3, 6);
            this.LabelSubFaction.Name = "LabelSubFaction";
            this.LabelSubFaction.Size = new System.Drawing.Size(42, 13);
            this.LabelSubFaction.TabIndex = 35;
            this.LabelSubFaction.Text = "Faction";
            // 
            // ButtonToggleView
            // 
            this.ButtonToggleView.Location = new System.Drawing.Point(241, 2);
            this.ButtonToggleView.Name = "ButtonToggleView";
            this.ButtonToggleView.Size = new System.Drawing.Size(120, 20);
            this.ButtonToggleView.TabIndex = 36;
            this.ButtonToggleView.Text = "ButtonToggleView";
            this.ButtonToggleView.UseVisualStyleBackColor = true;
            this.ButtonToggleView.Click += new System.EventHandler(this.ButtonToggleView_Click);
            // 
            // TextBoxSearch
            // 
            this.TextBoxSearch.ForeColor = System.Drawing.Color.Gray;
            this.TextBoxSearch.Location = new System.Drawing.Point(4, 3);
            this.TextBoxSearch.Name = "TextBoxSearch";
            this.TextBoxSearch.Size = new System.Drawing.Size(233, 20);
            this.TextBoxSearch.TabIndex = 35;
            this.TextBoxSearch.Text = "Search...";
            this.TextBoxSearch.Enter += new System.EventHandler(this.TextBoxSearch_Enter);
            this.TextBoxSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBoxSearch_KeyUp);
            this.TextBoxSearch.Leave += new System.EventHandler(this.TextBoxSearch_Leave);
            // 
            // FormMotorpoolTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 711);
            this.Controls.Add(this.SplitContainerMPT);
            this.Controls.Add(this.MenuStripMotorpoolTool);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MenuStripMotorpoolTool;
            this.MinimumSize = new System.Drawing.Size(1000, 750);
            this.Name = "FormMotorpoolTool";
            this.Text = "Motorpool Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMotorpoolTool_FormClosing);
            this.Load += new System.EventHandler(this.FormPlatoonPlanner_Load);
            this.SizeChanged += new System.EventHandler(this.FormMotorpoolTool_SizeChanged);
            this.TabControlMotorpoolInfo.ResumeLayout(false);
            this.TabPageVehicleInfo.ResumeLayout(false);
            this.GroupBoxVehicleInfoGeneral.ResumeLayout(false);
            this.TableLayoutPanelVehicleInfo.ResumeLayout(false);
            this.TableLayoutPanelVehicleInfo.PerformLayout();
            this.GroupBoxVehicleInfoWeapons.ResumeLayout(false);
            this.GroupBoxVehicleInfoWeapons.PerformLayout();
            this.TabPageVehicleDeveloperInfo.ResumeLayout(false);
            this.TabPageVehicleDeveloperInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxVehicle)).EndInit();
            this.MenuStripMotorpoolTool.ResumeLayout(false);
            this.MenuStripMotorpoolTool.PerformLayout();
            this.TabControlFactionInfo.ResumeLayout(false);
            this.TabPageMotorpool.ResumeLayout(false);
            this.SplitContainerVehicleView.Panel1.ResumeLayout(false);
            this.SplitContainerVehicleView.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerVehicleView)).EndInit();
            this.SplitContainerVehicleView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ObjectListViewMotorpool)).EndInit();
            this.TabPageLoadouts.ResumeLayout(false);
            this.TableLayoutPanelRoleInformation.ResumeLayout(false);
            this.GroupBoxRoleGeneralInformation.ResumeLayout(false);
            this.GroupBoxRoleGeneralInformation.PerformLayout();
            this.GroupBoxSpecialRoleInformation.ResumeLayout(false);
            this.GroupBoxSpecialRoleInformation.PerformLayout();
            this.TabControlRoleInfo.ResumeLayout(false);
            this.TabPageInventory.ResumeLayout(false);
            this.TableLayoutPanelInventory.ResumeLayout(false);
            this.GroupBoxVestLoad.ResumeLayout(false);
            this.GroupBoxVestLoad.PerformLayout();
            this.GroupBoxUniformLoad.ResumeLayout(false);
            this.GroupBoxUniformLoad.PerformLayout();
            this.GroupBoxBackpackLoad.ResumeLayout(false);
            this.GroupBoxBackpackLoad.PerformLayout();
            this.TabPageAccessories.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.GroupBoxMiscGear.ResumeLayout(false);
            this.GroupBoxMiscGear.PerformLayout();
            this.GroupBoxPrimary.ResumeLayout(false);
            this.GroupBoxPrimary.PerformLayout();
            this.GroupBoxHeadgear.ResumeLayout(false);
            this.GroupBoxHeadgear.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeListViewRoles)).EndInit();
            this.TabPageResupply.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TreeListViewResupply)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeListViewSides)).EndInit();
            this.SplitContainerMPT.Panel1.ResumeLayout(false);
            this.SplitContainerMPT.Panel1.PerformLayout();
            this.SplitContainerMPT.Panel2.ResumeLayout(false);
            this.SplitContainerMPT.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerMPT)).EndInit();
            this.SplitContainerMPT.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox TextBoxClass2;
        private System.Windows.Forms.TextBox TextBoxClass3;
        private System.Windows.Forms.TextBox TextBoxClass4;
        private System.Windows.Forms.TextBox TextBoxClass1;
        private System.Windows.Forms.TextBox TextBoxClass5;
        private System.Windows.Forms.Label LabelVehiclesClass1;
        private System.Windows.Forms.Label LabelVehiclesClass2;
        private System.Windows.Forms.Label LabelVehiclesClass3;
        private System.Windows.Forms.Label LabelVehiclesClass4;
        private System.Windows.Forms.Label LabelVehiclesClass5;
        private System.Windows.Forms.Label LabelClassName;
        private System.Windows.Forms.TextBox TextBoxClassName;
        private System.Windows.Forms.TabControl TabControlMotorpoolInfo;
        private System.Windows.Forms.TabPage TabPageVehicleInfo;
        private System.Windows.Forms.PictureBox PictureBoxVehicle;
        private System.Windows.Forms.MenuStrip MenuStripMotorpoolTool;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewToolStripMenuItem;
        private System.Windows.Forms.Label LabelBWIClass;
        private System.Windows.Forms.TextBox TextBoxBWIClass;
        private System.Windows.Forms.ToolStripMenuItem vehicleInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StagingViewerToolStripMenuItem;
        private System.Windows.Forms.TabPage TabPageVehicleDeveloperInfo;
        private System.Windows.Forms.Label LabelWeaponAmmoCount;
        private System.Windows.Forms.Label LabelWeaponDescription;
        private System.Windows.Forms.TextBox TextBoxWeaponAmmoCount;
        private System.Windows.Forms.TextBox TextBoxWeaponDescription;
        private System.Windows.Forms.ListBox ListBoxWeapons;
        private System.Windows.Forms.Label LabelWeaponClassName;
        private System.Windows.Forms.TextBox TextBoxWeaponClassName;
        private System.Windows.Forms.ToolStripMenuItem dumpVehicleDebugStringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateDatasetStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showVehicleAmmoAsSuffixToolStripMenuItem;
        private System.Windows.Forms.TabControl TabControlFactionInfo;
        private System.Windows.Forms.TabPage TabPageMotorpool;
        private System.Windows.Forms.TabPage TabPageLoadouts;
        private BrightIdeasSoftware.TreeListView TreeListViewRoles;
        private BrightIdeasSoftware.OLVColumn RoleColumnName;
        private BrightIdeasSoftware.TreeListView TreeListViewSides;
        private BrightIdeasSoftware.OLVColumn SideColumnName;
        private BrightIdeasSoftware.ObjectListView ObjectListViewMotorpool;
        private BrightIdeasSoftware.OLVColumn MPColumnDisplayName;
        private BrightIdeasSoftware.OLVColumn MPColumnCategory;
        private BrightIdeasSoftware.OLVColumn MPColumnEscalation;
        private BrightIdeasSoftware.OLVColumn MPColumnCrew;
        private BrightIdeasSoftware.OLVColumn MPColumnPassengers;
        private BrightIdeasSoftware.OLVColumn MPColumnCargo;
        private BrightIdeasSoftware.OLVColumn MPColumnCost;
        private BrightIdeasSoftware.OLVColumn MPColumnCanFloat;
        private BrightIdeasSoftware.OLVColumn MPColumnMaximumLoad;
        private BrightIdeasSoftware.OLVColumn MPColumnTransportAmmo;
        private BrightIdeasSoftware.OLVColumn MPColumnTransportFuel;
        private BrightIdeasSoftware.OLVColumn MPColumnReconSpecialLocked;
        private System.Windows.Forms.ToolStripMenuItem changeFontToolStripMenuItem;
        private System.Windows.Forms.Label LabelFactionGroup;
        private System.Windows.Forms.TextBox TextBoxFactionGroup;
        private System.Windows.Forms.Label LabelBinocular;
        private System.Windows.Forms.TextBox TextBoxBinocular;
        private System.Windows.Forms.Label LabelTertiary;
        private System.Windows.Forms.TextBox TextBoxTertiary;
        private System.Windows.Forms.Label LabelSecondary;
        private System.Windows.Forms.TextBox TextBoxSecondary;
        private System.Windows.Forms.TextBox TextBoxPrimary;
        private System.Windows.Forms.Label LabelInterpreter;
        private System.Windows.Forms.TextBox TextBoxInterpreter;
        private System.Windows.Forms.Label LabelStaminaFactor;
        private System.Windows.Forms.TextBox TextBoxStaminaFactor;
        private System.Windows.Forms.Label LabelGForce;
        private System.Windows.Forms.TextBox TextBoxGForce;
        private System.Windows.Forms.Label LabelEOD;
        private System.Windows.Forms.TextBox TextBoxEOD;
        private System.Windows.Forms.Label LabelEngineer;
        private System.Windows.Forms.TextBox TextBoxEngineer;
        private System.Windows.Forms.Label LabelMedic;
        private System.Windows.Forms.TextBox TextBoxMedic;
        private System.Windows.Forms.Label LabelCallsign;
        private System.Windows.Forms.TextBox TextBoxCallsign;
        private System.Windows.Forms.Label LabelFaction;
        private System.Windows.Forms.TextBox TextBoxFaction;
        private System.Windows.Forms.Label LabelElementRole;
        private System.Windows.Forms.TextBox TextBoxElementRole;
        private System.Windows.Forms.Label LabelBFT;
        private System.Windows.Forms.TextBox TextBoxBFT;
        private System.Windows.Forms.Label LabelGPS;
        private System.Windows.Forms.TextBox TextBoxGPS;
        private System.Windows.Forms.Label LabelWatch;
        private System.Windows.Forms.TextBox TextBoxWatch;
        private System.Windows.Forms.Label LabelCompass;
        private System.Windows.Forms.TextBox TextBoxCompass;
        private System.Windows.Forms.Label LabelMap;
        private System.Windows.Forms.TextBox TextBoxMap;
        private System.Windows.Forms.ListBox ListBoxUniform;
        private System.Windows.Forms.TextBox TextBoxBackpack;
        private System.Windows.Forms.TextBox TextBoxVest;
        private System.Windows.Forms.TextBox TextBoxUniform;
        private System.Windows.Forms.TextBox TextBoxHeadgear;
        private System.Windows.Forms.Label LabelUAVTerminal;
        private System.Windows.Forms.TextBox TextBoxUAVTerminal;
        private System.Windows.Forms.ListBox ListBoxBackpack;
        private System.Windows.Forms.ListBox ListBoxVest;
        private System.Windows.Forms.ToolTip ToolTipInfo;
        private System.Windows.Forms.SplitContainer SplitContainerMPT;
        private System.Windows.Forms.Label LabelSubFaction;
        private System.Windows.Forms.ToolStripMenuItem factionInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupItemsInInventoryToolStripMenuItem;
        private System.Windows.Forms.TextBox TextBoxSearch;
        private BrightIdeasSoftware.OLVColumn MPColumnTotalCapacity;
        private System.Windows.Forms.ToolStripMenuItem showInventoryQuantityAsSuffixToolStripMenuItem;
        private BrightIdeasSoftware.OLVColumn MPColumnIsOutpost;
        private System.Windows.Forms.TextBox TextBoxInhertiance;
        private System.Windows.Forms.Label LabelInhertiance;
        private System.Windows.Forms.TabControl TabControlRoleInfo;
        private System.Windows.Forms.TabPage TabPageInventory;
        private System.Windows.Forms.TabPage TabPageAccessories;
        private System.Windows.Forms.ListBox ListBoxHeadgearAccessories;
        private System.Windows.Forms.ListBox ListBoxPrimaryAccessories;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanelInventory;
        private System.Windows.Forms.GroupBox GroupBoxUniformLoad;
        private System.Windows.Forms.GroupBox GroupBoxVestLoad;
        private System.Windows.Forms.GroupBox GroupBoxBackpackLoad;
        private System.Windows.Forms.GroupBox GroupBoxHeadgear;
        private System.Windows.Forms.GroupBox GroupBoxPrimary;
        private System.Windows.Forms.GroupBox GroupBoxMiscGear;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox GroupBoxRoleGeneralInformation;
        private System.Windows.Forms.GroupBox GroupBoxSpecialRoleInformation;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanelRoleInformation;
        private System.Windows.Forms.ToolStripMenuItem EnableDEVModeToolStripMenuItem;
        private System.Windows.Forms.TabPage TabPageResupply;
        private BrightIdeasSoftware.TreeListView TreeListViewResupply;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private System.Windows.Forms.SplitContainer SplitContainerVehicleView;
        private System.Windows.Forms.Button ButtonToggleView;
        private System.Windows.Forms.GroupBox GroupBoxVehicleInfoWeapons;
        private System.Windows.Forms.Label LabelWeaponsWeapons;
        private System.Windows.Forms.GroupBox GroupBoxVehicleInfoGeneral;
        private System.Windows.Forms.TextBox TextBoxVehicleInfoRestriction;
        private System.Windows.Forms.Label LabelVehicleInfoRestriction;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanelVehicleInfo;
        private System.Windows.Forms.Label LabelVehicleInfoCrew;
        private System.Windows.Forms.Label LabelVehicleInfoEscalation;
        private System.Windows.Forms.Label LabelVehicleInfoCategory;
        private System.Windows.Forms.Label LabelVehicleInfoPax;
        private System.Windows.Forms.Label LabelVehicleInfoFloats;
        private System.Windows.Forms.TextBox TextBoxVehicleEscalation;
        private System.Windows.Forms.TextBox TextBoxVehicleInfoCategory;
        private System.Windows.Forms.TextBox TextBoxVehicleInfoCOP;
        private System.Windows.Forms.TextBox TextBoxVehicleInfoCargo;
        private System.Windows.Forms.TextBox TextBoxVehicleInfoCost;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TextBoxVehicleInfoCrew;
        private System.Windows.Forms.TextBox TextBoxVehicleInfoPax;
        private System.Windows.Forms.TextBox TextBoxVehicleInfoFloats;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}

