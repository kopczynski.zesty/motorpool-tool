﻿namespace MotorpoolTool.GUI
{
    partial class FormStagingViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboxMapSelect = new System.Windows.Forms.ComboBox();
            this.PanelPlaceholder = new System.Windows.Forms.Panel();
            this.MenuStripStagingViewer = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatTerrainImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CheckBoxRotateLabels = new System.Windows.Forms.CheckBox();
            this.GroupBoxBLUFOR = new System.Windows.Forms.GroupBox();
            this.LabelBLUFORSpawns = new System.Windows.Forms.Label();
            this.ComboxBLUFORSecondary = new System.Windows.Forms.ComboBox();
            this.ComboxBLUFORPrimary = new System.Windows.Forms.ComboBox();
            this.GroupBoxOPFOR = new System.Windows.Forms.GroupBox();
            this.LabelOPFORSpawns = new System.Windows.Forms.Label();
            this.ComboxOPFORSecondary = new System.Windows.Forms.ComboBox();
            this.ComboxOPFORPrimary = new System.Windows.Forms.ComboBox();
            this.GroupBoxINDEP = new System.Windows.Forms.GroupBox();
            this.LabelINDEPSpawns = new System.Windows.Forms.Label();
            this.ComboxINDEPSecondary = new System.Windows.Forms.ComboBox();
            this.ComboxINDEPPrimary = new System.Windows.Forms.ComboBox();
            this.GroupBoxConstruction = new System.Windows.Forms.GroupBox();
            this.CheckBoxConstructionTier3 = new System.Windows.Forms.CheckBox();
            this.CheckBoxConstructionTier2 = new System.Windows.Forms.CheckBox();
            this.CheckBoxConstructionTier1 = new System.Windows.Forms.CheckBox();
            this.GroupBoxWorldSelect = new System.Windows.Forms.GroupBox();
            this.ProgressBarTerrain = new System.Windows.Forms.ProgressBar();
            this.CheckBoxShowAllStagings = new System.Windows.Forms.CheckBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.MenuStripStagingViewer.SuspendLayout();
            this.GroupBoxBLUFOR.SuspendLayout();
            this.GroupBoxOPFOR.SuspendLayout();
            this.GroupBoxINDEP.SuspendLayout();
            this.GroupBoxConstruction.SuspendLayout();
            this.GroupBoxWorldSelect.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ComboxMapSelect
            // 
            this.ComboxMapSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboxMapSelect.FormattingEnabled = true;
            this.ComboxMapSelect.Location = new System.Drawing.Point(6, 19);
            this.ComboxMapSelect.Name = "ComboxMapSelect";
            this.ComboxMapSelect.Size = new System.Drawing.Size(185, 21);
            this.ComboxMapSelect.Sorted = true;
            this.ComboxMapSelect.TabIndex = 1;
            this.ComboxMapSelect.SelectedIndexChanged += new System.EventHandler(this.ComboBoxMapSelect_SelectedIndexChanged);
            // 
            // PanelPlaceholder
            // 
            this.PanelPlaceholder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelPlaceholder.Location = new System.Drawing.Point(227, 27);
            this.PanelPlaceholder.Name = "PanelPlaceholder";
            this.PanelPlaceholder.Size = new System.Drawing.Size(595, 572);
            this.PanelPlaceholder.TabIndex = 2;
            this.PanelPlaceholder.Visible = false;
            // 
            // MenuStripStagingViewer
            // 
            this.MenuStripStagingViewer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.MenuStripStagingViewer.Location = new System.Drawing.Point(0, 0);
            this.MenuStripStagingViewer.Name = "MenuStripStagingViewer";
            this.MenuStripStagingViewer.Size = new System.Drawing.Size(834, 24);
            this.MenuStripStagingViewer.TabIndex = 3;
            this.MenuStripStagingViewer.Text = "MenuStripStagingViewer";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formatTerrainImageToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // formatTerrainImageToolStripMenuItem
            // 
            this.formatTerrainImageToolStripMenuItem.Name = "formatTerrainImageToolStripMenuItem";
            this.formatTerrainImageToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.formatTerrainImageToolStripMenuItem.Text = "Format terrain image...";
            this.formatTerrainImageToolStripMenuItem.Click += new System.EventHandler(this.FormatTerrainImageToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // CheckBoxRotateLabels
            // 
            this.CheckBoxRotateLabels.AutoSize = true;
            this.CheckBoxRotateLabels.Location = new System.Drawing.Point(6, 98);
            this.CheckBoxRotateLabels.Name = "CheckBoxRotateLabels";
            this.CheckBoxRotateLabels.Size = new System.Drawing.Size(131, 17);
            this.CheckBoxRotateLabels.TabIndex = 4;
            this.CheckBoxRotateLabels.Text = "Rotate Staging Labels";
            this.CheckBoxRotateLabels.UseVisualStyleBackColor = true;
            this.CheckBoxRotateLabels.CheckedChanged += new System.EventHandler(this.CheckBoxRotateLabels_CheckedChanged);
            // 
            // GroupBoxBLUFOR
            // 
            this.GroupBoxBLUFOR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxBLUFOR.AutoSize = true;
            this.GroupBoxBLUFOR.Controls.Add(this.LabelBLUFORSpawns);
            this.GroupBoxBLUFOR.Controls.Add(this.ComboxBLUFORSecondary);
            this.GroupBoxBLUFOR.Controls.Add(this.ComboxBLUFORPrimary);
            this.GroupBoxBLUFOR.Location = new System.Drawing.Point(3, 137);
            this.GroupBoxBLUFOR.Name = "GroupBoxBLUFOR";
            this.GroupBoxBLUFOR.Size = new System.Drawing.Size(197, 103);
            this.GroupBoxBLUFOR.TabIndex = 5;
            this.GroupBoxBLUFOR.TabStop = false;
            this.GroupBoxBLUFOR.Text = "BLUFOR Stagings";
            // 
            // LabelBLUFORSpawns
            // 
            this.LabelBLUFORSpawns.AutoSize = true;
            this.LabelBLUFORSpawns.Location = new System.Drawing.Point(7, 74);
            this.LabelBLUFORSpawns.Name = "LabelBLUFORSpawns";
            this.LabelBLUFORSpawns.Size = new System.Drawing.Size(10, 13);
            this.LabelBLUFORSpawns.TabIndex = 8;
            this.LabelBLUFORSpawns.Text = "-";
            // 
            // ComboxBLUFORSecondary
            // 
            this.ComboxBLUFORSecondary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboxBLUFORSecondary.FormattingEnabled = true;
            this.ComboxBLUFORSecondary.Location = new System.Drawing.Point(6, 46);
            this.ComboxBLUFORSecondary.Name = "ComboxBLUFORSecondary";
            this.ComboxBLUFORSecondary.Size = new System.Drawing.Size(185, 21);
            this.ComboxBLUFORSecondary.TabIndex = 7;
            // 
            // ComboxBLUFORPrimary
            // 
            this.ComboxBLUFORPrimary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboxBLUFORPrimary.FormattingEnabled = true;
            this.ComboxBLUFORPrimary.Location = new System.Drawing.Point(6, 19);
            this.ComboxBLUFORPrimary.Name = "ComboxBLUFORPrimary";
            this.ComboxBLUFORPrimary.Size = new System.Drawing.Size(185, 21);
            this.ComboxBLUFORPrimary.TabIndex = 6;
            // 
            // GroupBoxOPFOR
            // 
            this.GroupBoxOPFOR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxOPFOR.AutoSize = true;
            this.GroupBoxOPFOR.Controls.Add(this.LabelOPFORSpawns);
            this.GroupBoxOPFOR.Controls.Add(this.ComboxOPFORSecondary);
            this.GroupBoxOPFOR.Controls.Add(this.ComboxOPFORPrimary);
            this.GroupBoxOPFOR.Location = new System.Drawing.Point(3, 246);
            this.GroupBoxOPFOR.Name = "GroupBoxOPFOR";
            this.GroupBoxOPFOR.Size = new System.Drawing.Size(197, 103);
            this.GroupBoxOPFOR.TabIndex = 8;
            this.GroupBoxOPFOR.TabStop = false;
            this.GroupBoxOPFOR.Text = "OPFOR Stagings";
            // 
            // LabelOPFORSpawns
            // 
            this.LabelOPFORSpawns.AutoSize = true;
            this.LabelOPFORSpawns.Location = new System.Drawing.Point(7, 74);
            this.LabelOPFORSpawns.Name = "LabelOPFORSpawns";
            this.LabelOPFORSpawns.Size = new System.Drawing.Size(10, 13);
            this.LabelOPFORSpawns.TabIndex = 8;
            this.LabelOPFORSpawns.Text = "-";
            // 
            // ComboxOPFORSecondary
            // 
            this.ComboxOPFORSecondary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboxOPFORSecondary.FormattingEnabled = true;
            this.ComboxOPFORSecondary.Location = new System.Drawing.Point(6, 46);
            this.ComboxOPFORSecondary.Name = "ComboxOPFORSecondary";
            this.ComboxOPFORSecondary.Size = new System.Drawing.Size(185, 21);
            this.ComboxOPFORSecondary.TabIndex = 7;
            // 
            // ComboxOPFORPrimary
            // 
            this.ComboxOPFORPrimary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboxOPFORPrimary.FormattingEnabled = true;
            this.ComboxOPFORPrimary.Location = new System.Drawing.Point(6, 19);
            this.ComboxOPFORPrimary.Name = "ComboxOPFORPrimary";
            this.ComboxOPFORPrimary.Size = new System.Drawing.Size(185, 21);
            this.ComboxOPFORPrimary.TabIndex = 6;
            // 
            // GroupBoxINDEP
            // 
            this.GroupBoxINDEP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxINDEP.AutoSize = true;
            this.GroupBoxINDEP.Controls.Add(this.LabelINDEPSpawns);
            this.GroupBoxINDEP.Controls.Add(this.ComboxINDEPSecondary);
            this.GroupBoxINDEP.Controls.Add(this.ComboxINDEPPrimary);
            this.GroupBoxINDEP.Location = new System.Drawing.Point(3, 355);
            this.GroupBoxINDEP.Name = "GroupBoxINDEP";
            this.GroupBoxINDEP.Size = new System.Drawing.Size(197, 103);
            this.GroupBoxINDEP.TabIndex = 8;
            this.GroupBoxINDEP.TabStop = false;
            this.GroupBoxINDEP.Text = "INDEP Stagings";
            // 
            // LabelINDEPSpawns
            // 
            this.LabelINDEPSpawns.AutoSize = true;
            this.LabelINDEPSpawns.Location = new System.Drawing.Point(7, 74);
            this.LabelINDEPSpawns.Name = "LabelINDEPSpawns";
            this.LabelINDEPSpawns.Size = new System.Drawing.Size(10, 13);
            this.LabelINDEPSpawns.TabIndex = 8;
            this.LabelINDEPSpawns.Text = "-";
            // 
            // ComboxINDEPSecondary
            // 
            this.ComboxINDEPSecondary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboxINDEPSecondary.FormattingEnabled = true;
            this.ComboxINDEPSecondary.Location = new System.Drawing.Point(6, 46);
            this.ComboxINDEPSecondary.Name = "ComboxINDEPSecondary";
            this.ComboxINDEPSecondary.Size = new System.Drawing.Size(185, 21);
            this.ComboxINDEPSecondary.TabIndex = 7;
            // 
            // ComboxINDEPPrimary
            // 
            this.ComboxINDEPPrimary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboxINDEPPrimary.FormattingEnabled = true;
            this.ComboxINDEPPrimary.Location = new System.Drawing.Point(6, 19);
            this.ComboxINDEPPrimary.Name = "ComboxINDEPPrimary";
            this.ComboxINDEPPrimary.Size = new System.Drawing.Size(185, 21);
            this.ComboxINDEPPrimary.TabIndex = 6;
            // 
            // GroupBoxConstruction
            // 
            this.GroupBoxConstruction.AutoSize = true;
            this.GroupBoxConstruction.Controls.Add(this.CheckBoxConstructionTier3);
            this.GroupBoxConstruction.Controls.Add(this.CheckBoxConstructionTier2);
            this.GroupBoxConstruction.Controls.Add(this.CheckBoxConstructionTier1);
            this.GroupBoxConstruction.Location = new System.Drawing.Point(3, 464);
            this.GroupBoxConstruction.Name = "GroupBoxConstruction";
            this.GroupBoxConstruction.Size = new System.Drawing.Size(197, 102);
            this.GroupBoxConstruction.TabIndex = 8;
            this.GroupBoxConstruction.TabStop = false;
            this.GroupBoxConstruction.Text = "Construction Exclusion Visualizer";
            // 
            // CheckBoxConstructionTier3
            // 
            this.CheckBoxConstructionTier3.Location = new System.Drawing.Point(6, 66);
            this.CheckBoxConstructionTier3.Name = "CheckBoxConstructionTier3";
            this.CheckBoxConstructionTier3.Size = new System.Drawing.Size(185, 17);
            this.CheckBoxConstructionTier3.TabIndex = 2;
            this.CheckBoxConstructionTier3.Text = "Tier 3 (3000m)";
            this.CheckBoxConstructionTier3.UseVisualStyleBackColor = true;
            // 
            // CheckBoxConstructionTier2
            // 
            this.CheckBoxConstructionTier2.Location = new System.Drawing.Point(6, 43);
            this.CheckBoxConstructionTier2.Name = "CheckBoxConstructionTier2";
            this.CheckBoxConstructionTier2.Size = new System.Drawing.Size(185, 17);
            this.CheckBoxConstructionTier2.TabIndex = 1;
            this.CheckBoxConstructionTier2.Text = "Tier 2 (2000m)";
            this.CheckBoxConstructionTier2.UseVisualStyleBackColor = true;
            // 
            // CheckBoxConstructionTier1
            // 
            this.CheckBoxConstructionTier1.Location = new System.Drawing.Point(6, 20);
            this.CheckBoxConstructionTier1.Name = "CheckBoxConstructionTier1";
            this.CheckBoxConstructionTier1.Size = new System.Drawing.Size(185, 17);
            this.CheckBoxConstructionTier1.TabIndex = 0;
            this.CheckBoxConstructionTier1.Text = "Tier 1 (1000m)";
            this.CheckBoxConstructionTier1.UseVisualStyleBackColor = true;
            // 
            // GroupBoxWorldSelect
            // 
            this.GroupBoxWorldSelect.Controls.Add(this.ProgressBarTerrain);
            this.GroupBoxWorldSelect.Controls.Add(this.CheckBoxShowAllStagings);
            this.GroupBoxWorldSelect.Controls.Add(this.ComboxMapSelect);
            this.GroupBoxWorldSelect.Controls.Add(this.CheckBoxRotateLabels);
            this.GroupBoxWorldSelect.Location = new System.Drawing.Point(3, 3);
            this.GroupBoxWorldSelect.Name = "GroupBoxWorldSelect";
            this.GroupBoxWorldSelect.Size = new System.Drawing.Size(197, 128);
            this.GroupBoxWorldSelect.TabIndex = 8;
            this.GroupBoxWorldSelect.TabStop = false;
            this.GroupBoxWorldSelect.Text = "Template Selection";
            // 
            // ProgressBarTerrain
            // 
            this.ProgressBarTerrain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProgressBarTerrain.Location = new System.Drawing.Point(6, 46);
            this.ProgressBarTerrain.Name = "ProgressBarTerrain";
            this.ProgressBarTerrain.Size = new System.Drawing.Size(185, 23);
            this.ProgressBarTerrain.TabIndex = 6;
            // 
            // CheckBoxShowAllStagings
            // 
            this.CheckBoxShowAllStagings.AutoSize = true;
            this.CheckBoxShowAllStagings.Location = new System.Drawing.Point(6, 75);
            this.CheckBoxShowAllStagings.Name = "CheckBoxShowAllStagings";
            this.CheckBoxShowAllStagings.Size = new System.Drawing.Size(111, 17);
            this.CheckBoxShowAllStagings.TabIndex = 5;
            this.CheckBoxShowAllStagings.Text = "Show All Stagings";
            this.CheckBoxShowAllStagings.UseVisualStyleBackColor = true;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.GroupBoxWorldSelect);
            this.flowLayoutPanel1.Controls.Add(this.GroupBoxBLUFOR);
            this.flowLayoutPanel1.Controls.Add(this.GroupBoxOPFOR);
            this.flowLayoutPanel1.Controls.Add(this.GroupBoxINDEP);
            this.flowLayoutPanel1.Controls.Add(this.GroupBoxConstruction);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 27);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(221, 572);
            this.flowLayoutPanel1.TabIndex = 9;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // FormStagingViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 611);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.PanelPlaceholder);
            this.Controls.Add(this.MenuStripStagingViewer);
            this.MinimumSize = new System.Drawing.Size(850, 650);
            this.Name = "FormStagingViewer";
            this.Text = "Staging Viewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormStagingViewer_FormClosing);
            this.Load += new System.EventHandler(this.StagingViewer_Load);
            this.Shown += new System.EventHandler(this.FormStagingViewer_Shown);
            this.MenuStripStagingViewer.ResumeLayout(false);
            this.MenuStripStagingViewer.PerformLayout();
            this.GroupBoxBLUFOR.ResumeLayout(false);
            this.GroupBoxBLUFOR.PerformLayout();
            this.GroupBoxOPFOR.ResumeLayout(false);
            this.GroupBoxOPFOR.PerformLayout();
            this.GroupBoxINDEP.ResumeLayout(false);
            this.GroupBoxINDEP.PerformLayout();
            this.GroupBoxConstruction.ResumeLayout(false);
            this.GroupBoxWorldSelect.ResumeLayout(false);
            this.GroupBoxWorldSelect.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox ComboxMapSelect;
        private System.Windows.Forms.Panel PanelPlaceholder;
        private System.Windows.Forms.MenuStrip MenuStripStagingViewer;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formatTerrainImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.CheckBox CheckBoxRotateLabels;
        private System.Windows.Forms.GroupBox GroupBoxBLUFOR;
        private System.Windows.Forms.ComboBox ComboxBLUFORSecondary;
        private System.Windows.Forms.ComboBox ComboxBLUFORPrimary;
        private System.Windows.Forms.GroupBox GroupBoxOPFOR;
        private System.Windows.Forms.ComboBox ComboxOPFORSecondary;
        private System.Windows.Forms.ComboBox ComboxOPFORPrimary;
        private System.Windows.Forms.GroupBox GroupBoxINDEP;
        private System.Windows.Forms.ComboBox ComboxINDEPSecondary;
        private System.Windows.Forms.ComboBox ComboxINDEPPrimary;
        private System.Windows.Forms.GroupBox GroupBoxConstruction;
        private System.Windows.Forms.GroupBox GroupBoxWorldSelect;
        private System.Windows.Forms.CheckBox CheckBoxConstructionTier3;
        private System.Windows.Forms.CheckBox CheckBoxConstructionTier2;
        private System.Windows.Forms.CheckBox CheckBoxConstructionTier1;
        private System.Windows.Forms.CheckBox CheckBoxShowAllStagings;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ProgressBar ProgressBarTerrain;
        private System.Windows.Forms.Label LabelBLUFORSpawns;
        private System.Windows.Forms.Label LabelOPFORSpawns;
        private System.Windows.Forms.Label LabelINDEPSpawns;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}