﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MotorpoolTool.GUI
{
    public static class Cfg
    {

        /// <summary>
        /// Cleans up a CfgXYZ file that has been freshly dumped via a script. This removes a lot of excess characters that are not required for this tool.
        /// </summary>
        /// <param name="filePath">filePath to CfgVehicles, CfgWeapons, CfgMagazines, etc, text file.</param>
        /// <returns>True if file changed, false if file not changed.</returns>
        public static bool Prepare(string filePath)
        {
            // THIS FUNCTION WILL ONLY RUN IF IT THINKS THE FILE NEEDS CLEANUP
            // THIS IS DETERMINED BY CHECKING IF THE FIRST CHARACTER IS AN OPEN BRACKET [
            // If this open bracket exists, it means the file has not been cleaned up, and is a raw array from ARMA 3.

            if(!File.Exists(filePath))
            {
                throw new FileNotFoundException(string.Format("File '{0}' was not found.", filePath));
            }

            var cfg = File.ReadAllLines(filePath);
            if (cfg[0].Length == 0 || !cfg[0][0].Equals('['))
            {
                return false;
            }


            // Remove the bracket at the start and end of the file.
            cfg[0] = cfg[0].Remove(0, 1);
            cfg[cfg.Length - 1] = cfg[cfg.Length - 1].Remove(cfg[cfg.Length - 1].Length - 1, 1);

            // Go through file and remove " chars, and remove the comma at the end of the string.
            for(int index = 0; index < cfg.Length; index++)
            {
                cfg[index] = cfg[index].Replace("\"", string.Empty);
                if (cfg[index].Length == 0) { continue; }
                if (cfg[index][cfg[index].Length - 1] == ',')
                {
                    cfg[index] = cfg[index].Remove(cfg[index].Length - 1, 1);
                }

                // Special case, replace stacked empty arrays [[]] to make life easier.
                cfg[index] = ReplaceEmptyArray(cfg[index]);
                cfg[index] = ReplaceEscapedHTMLChars(cfg[index]);

            }

            // Cleanup done
            StreamWriter writer = new StreamWriter(filePath);
            foreach(string line in cfg)
            {
                // skip empty lines
                if(line.Length == 0) { continue; }
                writer.WriteLine(line);
            }
            writer.Close();
            Console.WriteLine(string.Format("Pre-processed {0} lines in {1}", cfg.Length, filePath));

            return true;
        }

        /// <summary>
        /// Some ARMA 3 configs (looking at you, RHS) use HTML characters instead of regular characters in their config descriptions. These need to be converted for my sanity.
        /// </summary>
        /// <param name="line"></param>
        /// <returns>Line with real characters</returns>
        private static string ReplaceEscapedHTMLChars(string line)
        {
            line = Regex.Replace(line, "&lt;", "<");
            line = Regex.Replace(line, "&gt;", ">");
            return line;
        }

        /// <summary>
        /// Sometimes ARMA 3 configs contain empty arrays instead of nil arrays. This leads to stacked empty arrays. Easier to remove them in pre-processing than handling them down the line.
        /// </summary>
        /// <param name="line"></param>
        /// <returns>Empty array instead of stacked empty array.</returns>
        private static string ReplaceEmptyArray(string line)
        {
            line = Regex.Replace(line, @"\[\[\]\]", "[]");
            //line = Regex.Replace(line, @"\[\]", string.Empty);
            return line;
        }

        /// <summary>
        /// Converts a string from the config to an array.
        /// </summary>
        /// <param name="s">String to be converted</param>
        /// <returns>2D string array in format <code>[MainTurret,[Weaponn,Ammo,Ammo],OtherTurret,[Weapon,Ammo,Ammo]]</code></returns>
        public static string[][] StringToArray(string s)
        {
            if (s == null)
            {
                return null;
            }

            string[] array = s.Split(new string[] { "]," }, StringSplitOptions.RemoveEmptyEntries);
            return array.Select(x => x.Split(new string[] { "[", "]" }, StringSplitOptions.RemoveEmptyEntries).ToArray()).ToArray();
        }
    }
}
