﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MotorpoolTool.GUI
{
    static class Program
    {
        public static readonly string MPTVersion = "Motorpool Tool v2021-MAY-28";
        public static Font MPTFont;

        // Cultures that use commas as a decimal point make life hard.
        public static CultureInfo MPTCulture = new CultureInfo("en-US");
        public static Form MotorpoolTool;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            MotorpoolTool = new FormMotorpoolTool();
            MotorpoolTool.Text = MPTVersion;
            Application.Run(MotorpoolTool);
        }

        public static void UpdateProgramFont(Form form, Font font)
        {
            foreach (Control control in form.Controls)
            {
                if (control.HasChildren)
                {
                    foreach (Control child in control.Controls)
                    {
                        // not scaling UI to font sizes outside of tables
                        child.Font = new Font(font.FontFamily, child.Font.Size);
                    }
                }
            }
        }
    }
}
